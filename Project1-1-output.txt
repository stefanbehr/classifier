devtest:

Confusion Matrix, row=true, column=predicted  accuracy=0.986335403726708
    label   0   1  |total
  0 right 400   7  |407
  1  left   4 394  |398

test:

Confusion Matrix, row=true, column=predicted  accuracy=0.9676616915422885
    label   0   1  |total
  0 right 391  16  |407
  1  left  10 387  |397
