import math
import operator
import re
import sys

text    = sys.stdin.read()
lines   = re.split('[\r\n]+', text)
cat_cat = {}
for line in lines:
  if re.match('array:[0-9]+ [a-zA-Z]+( [a-zA-Z]+:[0-9eE\-\.]+)+ ?', line) == None:
    continue
  
  toks = line.split(' ')
  
  if toks[1] not in cat_cat:
    cat_cat[toks[1]] = []
  
  for tok in toks[2:len(toks)]:
    cat_prob = tok.split(':')
    if cmp(cat_prob[0], "right") != 0:
      continue
    
    cat_cat[toks[1]].append(cat_prob[1])

cats = cat_cat.items()
cat_probs = {}
for cat in cats:
  
  prob = 0.0
  for _prob in cat[1]:
    prob += float(_prob)
  
  prob = (prob/len(cat[1]))
  
  cat_probs[cat[0]] = math.floor(100000.0*prob)/100000.0 # round to 5 digits

sorted_cat_probs = sorted(cat_probs.iteritems(), key=operator.itemgetter(1))

print "\tAvg. Score\tRight Rank\tLeft Rank"
cats = sorted_cat_probs
for i in range(len(cats)):
  print cats[i][0] + "\t" + str(cats[i][1]) + "\t" + str(len(cats) - i) + "\t" + str(i + 1)
