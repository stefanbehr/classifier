This morning, President Obama proclaimed the 2012 Greek Independence Day a national day of celebration of Greek and American democracy: 

"On Greek Independence Day, we commemorate the proud traditions that tie our nations together and honor all those who trace their lineage to the Hellenic Republic. Nearly 200 years after the Greek people won their war to return democracy to their homeland and become a sovereign state, we reaffirm the warm friendship and solidarity that will guide our work together in the years ahead." 

Read the full White House statement here . 
