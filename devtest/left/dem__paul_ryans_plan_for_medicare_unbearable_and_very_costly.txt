Conspicuously absent from Florida? Romney's new running mate, Paul Ryan. As Democratic Party chair and Florida Rep. Debbie Wasserman Schultz put it at a stop in Miami Gardens, Florida, ''I'm not very surprised Paul Ryan decided to skip the bus tour today with Mitt Romney. If I were the architect of a plan to end Medicare as we know it, I wouldn't show my face in Florida either.'' 

Our Romney-Ryan Economics: Middle Class Under the Bus tour is criss-crossing Florida today. Mitt Romney and his campaign bus are here too. 

Conspicuously absent? Romney s new running mate, Paul Ryan. 

As Democratic Party chair and Florida Rep. Debbie Wasserman Schultz put it at a stop in Miami Gardens, Florida, "I m not very surprised Paul Ryan decided to skip the bus tour today with Mitt Romney. If I were the architect of a plan to end Medicare as we know it, I wouldn t show my face in Florida either." 

Unlike Ryan, seniors are out in full force. Rose, a Miami Gardens senior, says Romney s choice of running mate makes it clearer than ever that President Obama is "the only choice" that we have. "He s put forth programs like Obamacare that I truly need because I have all kinds of ailments," she says. Even though she is assisted by a walker ("my Cadillac") and a respirator, Rose came out to send a message to the Republican ticket and Florida voters alike: There s just too much at stake to elect Romney and Ryan. "Without that kind of help from Medicare in the future and now, it would be really unbearable and very costly for people like me." 

Rose is concerned about Paul Ryan s budget plan, which would end Medicare as we know it and turn the nation s guaranteed promise to its seniors into a risky voucher program. "I m opposed to the entire idea of vouchers," she says. "The burden of the expense of medical care would then fall on me primarily. My medical care is extensive and I m not atypical. He really is wrong about the voucher program." 

"That s why I m supporting President Obama, why I believe that this entire country needs to rally around the President. I ll walk the streets and just shout, Vote, vote, vote for President Obama! " 
