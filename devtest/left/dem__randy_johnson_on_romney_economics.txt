Taking the mic at the Democratic National Convention tonight, Randy Johnson spoke about being the victim of Romney economics: 

Mitt Romney once said, I like being able to fire people. Well I can tell you from personal experience: He does. 

What I fault him for is making money without a moral compass. I fault him for putting profits before people like me. But that s just Romney economics. America cannot afford Romney economics. Mitt Romney will stick it to working people. Barack Obama? Sticking up for working people. It s as simple as that. That s why I m supporting him for a second term for president. 
