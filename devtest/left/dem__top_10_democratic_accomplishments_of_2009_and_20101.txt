During the past two years, President Obama and Congressional Democrats have been fighting hard on behalf of the American people. Working to secure economic stability, fairness, and opportunity and overcoming challenges largely inherited when President Obama took office. 



As 2010 and the 111th Congress near a close, Democrats can reflect on the past two years with pride. In keeping with America s age-old, year-end tradition to list the best of, today s post marks the first in a series of year-end superlatives. 



Due to the vigorous productivity of the 111th Congress, this list is not comprehensive and may differ for different people this is only one version. 



Without further ado, Democrats Top 10 Accomplishments of the past two years. 10. Passing Credit Card Reform (CARD Act) 

Protects Americans from unfair and misleading credit card practices 



9. Lilly Ledbetter Fair Pay Act 

Ensures that all workers in America are paid what they deserve, regardless of race, gender, or age 



8. Supreme Court Justices Sonia Sotomayor and Elena Kagan 

President Obama nominated and the Senate confirmed Associate Justices Sonia Sotomayor and Elena Kagan to the Supreme Court 



7. Student Loan Reform 

Helps make college more affordable for students and families 



6. Wall Street Reform 

Holds Wall Street accountable , ends "too big to fail" bailouts, and enacts the strongest consumer protections in history 



5. The New START Treaty 

Keeps America safe and strengthens our global leadership on nuclear weapons issues 



4. Don t Ask, Don t Tell Repeal 

Ends the discriminatory policy preventing gays and lesbians from serving in America s armed forces 



3. Ending combat operations in Iraq 

Effectively ends Operation Iraqi Freedom and withdraws 100,000 troops 



2. The Recovery Act 

Saves and creates millions of jobs, investing unprecedented resources in building a new foundation for our country, and preventing a second Great Depression 



1. The Affordable Care Act 

Reforms our country s broken health care system by holding insurance companies accountable, lowering costs, ensuring greater choice, and improving the quality of care for all Americans 

America is making steady progress toward recovery. Although our country still has a long way to go, President Obama and Democrats in the 111th Congress have enacted essential changes that pulled America from the brink of economic and financial meltdown and invested in a new economic foundation for the long run. 



What would be your top 10 list of accomplishments during the past two years? Tell us in the comments below. 
