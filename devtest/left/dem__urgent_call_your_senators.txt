In the following message to supporters, Chairman Tim Kaine urges people to call their senators and ask them to support the repeal of "Don t Ask, Don t Tell" and the passage of the DREAM Act: 

Friend -- 



Your senators need to hear from you right away. 



Tomorrow, they will cast important votes on two huge issues: the repeal of "Don t Ask, Don t Tell" and passage of the DREAM Act. 



When the clerk of the Senate calls the roll, the outcome for both these bills could go either way. The only thing that we know with certainty is that each vote will be extremely close. And because there are so few days left in 2010, this might be our final opportunity to move forward on either of these issues. 

Right now, we need you to call your senators and ask them to support both the repeal of "Don t Ask, Don t Tell" and passage of the DREAM Act. 

President Obama and the leadership of our armed forces have together asked Congress to repeal "Don t Ask, Don t Tell." It s obvious that the time for this change has come -- simple fairness demands it. This vote is our chance to do so. 



The DREAM Act is a critical piece of immigration reform -- it will offer a pathway to citizenship for young people who are willing to serve in our armed forces or attend college. 



If your lawmakers are supporting these measures, let them know they have your thanks. If your senators are undecided or opposed to these measures, please ask them to do what is right. 



Time is running out to see progress on these critical issues . 



The single best thing you can do to help support the President is to call your senators right now. For lawmakers who have yet to make up their minds on how they ll vote on the DREAM Act or "Don t Ask, Don t Tell" repeal, calls from constituents could make all the difference. 



Call now: 



http://my.democrats.org/DADTDREAMCalls-Dblog 

Thank you, 



Governor Tim Kaine 

Chairman 
