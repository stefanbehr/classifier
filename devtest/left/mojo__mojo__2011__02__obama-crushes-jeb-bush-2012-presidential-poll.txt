Apparently unsatisfied with the current crop of GOP presidential hopefuls, some big names on the right have been pushing Jeb Bush , former Florida governor and younger brother to George W., to launch a 2012 bid for the White House. But according to a new Fox News poll ( PDF ), Bush would get crushed by President Barack Obama in a 2012 presidential match-up. 

Fox's bipartisan pollsters found that Obama would defeat Jeb Bush by a whopping 20-point margin, 54 percent to 34 percent. That's a healthy gain for Obama since last September, when the same pollsters put Obama ahead 45 percent to 37 percent. The Fox poll results, then, throw some cold water on all the Jeb hype of late, suggesting that Jeb might be smarter to look to 2016 rather than 2012. 

Unfortunately for the Republicans, the rest of the party's possible presidential field didn't fare much better against Obama. The Fox poll shows Obama beating Mitt Romney by 7 percentage points, Mike Huckabee by eight, and Newt Gingrich by 20. The only possible GOP candidate whose loss margin is greater than Bush's or Gingrich's? Sarah Palin, who the poll finds would get trounced by Obama by 21 points, 56 to 35. 

It's very early in the president race, to be sure, but the poll shows that the Republican Party faces an uphill battle.
