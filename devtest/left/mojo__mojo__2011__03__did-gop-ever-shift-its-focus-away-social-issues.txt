The normally excellent Jeff Zeleny takes a close look at the 2012 Iowa caucuses and discovers that conservatives care about social issues: 

The ailing economy and the Tea Party's demand for smaller government have dominated Republican politics for two years, but a resurgent social conservative movement is shaping the first stage of the presidential nominating contest... 

Has the ailing economy and demand for smaller government really dominated Republican politics for two years ? 

Let's recap: The debate against health-care reform featured not only the false claim that the new law would budget taxpayer funding to pay for abortions one member of Congress even called another member of Congress a babykiller on the House floor but also the false claim that the new law would target senior citizens and people with disabilities for death. Then, the entire month of August, 2010, was spent debating the small government issue of whether a religious group should build a house of worship in Lower Manhattan. 

Glenn Beck's case against Barack Obama which provided the fuel for FOX's amped-up full-team-coverage of 9/12 and tea party rallies over the last two years has its foundation in the idea that the United States was founded as a Christian nation, rooted in divine principles, and that any effort to alter the Founders' immaculate construction more or less conflicts with the will of God. It's a popular idea, endorsed by grassroots activists and elected officials alike. 

Meanwhile, the first order of business for members of the landslide GOP class of 2011 has been to introduce a set of anti-choice legislation at the state and federal level that would redefine rape ; defund Planned Parenthood ; force women to listen to ultrasounds prior to getting an abortion; omit exceptions for rape, incest, and the health of the mother; and provide funding for organizations that tell women (falsely) that getting an abortion can lead to breast cancer or suicide. You know, small government stuff. 

The dominant theme in Republican politics for the last two years hasn't been jobs and small government; it's been opposition to Barack Obama, period. The grassroots conservative critique of the Obama administration stems from a set of social and economic values that are deeply intertwined. The notion that religious conservatives are suddenly resurgent rests on the flawed assumption that they ever really went away.
