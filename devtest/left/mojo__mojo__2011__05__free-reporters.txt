In the rare bit of news unrelated to Osama bin Laden, today is World Press Freedom Day! Which means that the United Nations is holding a shindig in Washington, and people are giving speeches noting that press freedom is at its lowest level in 12 years, and there's a new report out on the top 10 tools used by online censors and oppressors. 

For our part, we'll take this day to remember the many journalists who have lost their freedom--journalists whose suffering isn't making headlines the way the ordeals of Lara Logan and the New York Times Four did, but who are equally deserving of our sympathy and outrage. No fewer than 16 reporters are detained or missing in Libya alone right now (and four more, including photographers Tim Hetherington and Chris Hondros, have been killed); hundreds share their fate around the world. One of them is Shane Bauer, who was detained in August 2009 while hiking in a remote, scenic part of Iraqi Kurdistan near the Iranian border. He remains in prison in Iran along with his friend Josh Fattal, an environmental educator. Sarah Shourd, Shane's fiancee, was detained with the two but has since been freed. 

Shane wasn't on assignment at the time of his arrest (which according to a Nation investigation took place inside Iraq), but he had done terrific reporting from the Middle East including a Mother Jones expose on US payments to corrupt contractors in Iraq. Below is a statement by a number of the US journalists who have had the good fortune of working with him, ourselves included, urging Iran to end Shane and Josh's unjust captivity. It's been far too long. 

A JOURNALISTS APPEAL TO IRAN ON WORLD PRESS FREEDOM DAY 

On the occasion of World Press Freedom Day we call on the government and judicial authorities of the Islamic Republic of Iran to release our colleague Shane Bauer and his friend Josh Fattal, an environmental educator, after more than 21 months of detention. 

Shane, 28, is a talented freelance reporter and photographer whose work for a variety of news organizations has helped Americans better understand the impact of U.S. policy in the Middle East. While based in Damascus, Syria, for a year before his arrest, Shane, a fluent Arabic speaker, reported sensitively and incisively from Iraq, Syria and Yemen. Previously, he d reported from Darfur and Ethiopia. At the time of his arrest, he was preparing a report about the Israeli military s suspected misuse of nonlethal weapons in the West Bank. 

We have no doubt that the charges of espionage Iranian prosecutors have leveled against Shane and Josh are entirely unfounded. Shane and Josh were on vacation with Shane s fianc e Sarah Shourd when the three were arrested during a hiking trip in Iraqi Kurdistan near the border with Iran. Sarah was compassionately released last fall, but Shane and Josh are still being wrongfully denied their freedom. 

Shane is not being held prisoner because of his work as a journalist. But Shane was traveling in Iraq because he had previously done extensive and revelatory reporting there, exposing, for example, large-scale U.S. bribery of influential sheikhs in Iraq and human rights abuses by Iraq s U.S.-trained Special Operations Forces. 

As editors and reporters who have worked closely with Shane and admire his work, we firmly believe that his detention is unjust. We call on Iran to release Shane and Josh immediately. 

Sincerely, 

Monika Bauerlein and Clara Jeffery, co-editors, Mother Jones, San Francisco, CA 

Sandy Close, executive editor and director, New America Media, San Francisco, CA 

Jack Epstein, foreign editor, San Francisco Chronicle 

Esther Kaplan, editor, The Investigative Fund at The Nation Institute, New York, NY 

Vlae Kershner, news director, SFGate, the website of San Francisco Chronicle, San Francisco, CA 

Richard Kim, executive editor, The Nation, New York, NY 

Tim Redmond, executive editor, San Francisco Bay Guardian, San Francisco, CA 

Robert Rosenthal, executive director, Center for Investigative Reporting, Berkeley, CA 



Joel Simon, executive director, Committee to Protect Journalists, New York, NY A.C. Thompson, staff reporter, ProPublica, New York, NY
