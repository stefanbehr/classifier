A top Congressional Democrat, joined by two leading campaign finance reform groups here in Washington, has raised the ante on his demand that a federal court close a gaping loophole in the nation's laws against dark money in politics. In April, Rep. Chris Van Hollen (D-Md.) and two campaign finance groups announced that they were filing suit against the Federal Election Commission, after more than 90 percent of the funders behind election ads went unnamed in the 2010 elections. On Tuesday, they submitted a new brief and asked the judge to consider the challenge with haste. 

The legal challenge zeroes in on a decision made by the FEC, the nation's underwhelming watchdog for campaign finance , that dramatically undercut federal disclosure requirements for what are called electioneering advertisements ads that outright support or oppose a candidate. Here's what happened in a nutshell: In 2007, the FEC essentially told corporations and labor unions that unless donors said outright that they wanted their money to fund electioneering ads, those donors could stay secret disclosure rules be damned. The decision flew in the face of the McCain-Feingold campaign finance reform law of 2002, which said that any union or corporation funding electioneering ads must reveal all contributors of $1,000 or more. 

The FEC's decision quickly became the loophole that ate the rule. According to Tara Malloy, associate counsel at the pro-reform Campaign Legal Center, In 2010, groups making electioneering communications disclosed the funders of less than 10 percent of the $79.9 million spent on electioneering communications. 

Van Hollen's suit essentially argues that by creating this crater-sized disclosure loophole, the FEC overstepped its bounds and the limits of what it can and can't do. Now Van Hollen and the reformers have filed a motion for summary judgment in the suit. The disclosure loophole opened by the FEC has already allowed millions of secret dollars to influence our elections and the anonymous spending is only likely to increase in 2012, Malloy said. But the American people deserve to have disclosure about the sources of the money being spent by corporations and other special interest groups to buy influence over government decisions. 

Print Email Tweet Obama or GOP: Who Has the Edge in Debt Fight? We re Still at War: Photo of the Day for July 6, 2011 Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

What the FEC? 

The federal elections watchdog makes Congress look like a model of efficiency. FEC Lawyers: GOP Super-Duper PAC Is Illegal 

The nation's campaign finance watchdog tosses cold water on the latest scheme from the conservative lawyer behind Citizens United. The FEC Brings Down the Hammer (Belatedly) 

Problematic FEC Nominee Withdraws Name... Finally 

Hans von Spakovsky, the major hold-up in seating the FEC and the GOP's point man in disenfranchising minority voters, may... Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
