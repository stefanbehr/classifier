Rainbow Runners 

Cadets and staff assigned to Cadet Field Training finished their summer detail with a run-back from Camp Buckner to the steps of Washington Hall, July 22, West Point N.Y. After the runners came through Washington Gate they were cooled down by the West Point Fire Department. Lt. Gen. David H. Huntoon Jr. ran with the group and spoke to the CFT detail at the completion of the run. ( Photo by Tommy Gilligan/West Point Public Affairs)
