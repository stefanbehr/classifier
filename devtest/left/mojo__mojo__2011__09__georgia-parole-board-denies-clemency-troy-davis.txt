Seven recanting witnesses, a Pope, a former FBI Director, and an ex-US president couldn't persuade the Georgia Board of Pardons and Paroles that enough doubt had accumulated about Troy Davis' culpability in the 1989 murder of Georgia police officer Mark MacPhail to call off his execution. For the fourth time in the past four years, Davis has been given an execution date Wednesday at 7 p.m., accordin g to the Atlanta-Journal Constitution . 

The Davis case has been one of several recent death penalty cases in which exculpatory evidence that has emerged post-conviction has highlighted significant flaws in the criminal justice system, particularly when it comes to eyewitness testimony, which according to the Innocence Project, has played a role in 75 percent of convictions overturned by DNA testing. 

Doubts about Davis' guilt however, aren't exactly unanimous. They are not, for example, shared by the family of the slain police officer whom Davis was convicted of killing. 

He s guilty, MacPhail's widow, Joan MacPhail-Harris, said. We need to go ahead and execute him. 

For all the questions raised about the evidence and lack thereof that led to Davis' original conviction, the Georgia Board of Pardons and Paroles agreed.
