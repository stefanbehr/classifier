Last week, a federal appeals court blocked the part of Alabama's new immigration law one of the harshest in the nation that required public schools to check on their students' immigration statuses. A win for immigration advocates? Not so fast. The court preserved a section of the law that allows police officers to check a person's immigration status during traffic stops, and another making it a felony for illegal immigrants to conduct state business (like getting a driver's license). 

But those victories are ringing pretty hollow for farmers. In response to the new law, much of Alabama's migrant workforce is expected to leave the state, the AP reports . Part of Republican Gov. Robert Bentley's thinking: That unemployed American citizens will step in to fill the breach, in exchange for free transportation and steady pay. 

So far, not so good: 

After two weeks, [Jerry Spencer, a chief executive at Grow Alabama, a company that markets Alabama-grown produce] said Monday, the experiment is a failure. Jobless resident Americans lack the physical stamina and the mental toughness to see the job through, he said, and there's not much of a chance a new state program to fill the jobs will fare better. 

Lana Boatwright, another tomato farmer near Steele, said many of the people she has tried to hire since the law went into effect were concerned about losing their government disability payments if they worked in the fields. . . . 

Spencer said that of more than 50 people he recruited for the work, only a few worked more than two or three days, and just one stuck with the job for the last two weeks. 

It's pretty discouraging, said Spencer, chief executive of the Birmingham-based Grow Alabama, which sells and promotes produce grown in the state. 

According to Spencer, a member of a four-man tomato-picking crew can earn about $150 a day during the peak harvest time. But, at the lower, more realistic end, the figure is much closer to $25 a day, apparently not making it worth many Americans' while. 

It might not have been a bad idea for Gov. Bentley to float his idea past Georgia Governor Nathan Deal, who signed a similarly harsh immigration law earlier this year, with similar results. As the AP reports , that law was also blamed for scaring off over 11,000 workers during the spring and summer harvest. The jury is still out on Deal's plan to fill the gap with people on probation . 

None of this is to say that unemployed Alabamians aren't grateful for an opportunity to work, or that they're not capable of doing work traditionally reserved for migrant labor. But it's important to occasionally remind anti-immigrationistas that planting and picking tomatoes 20 hours a day, seven days a week, is really hard work. No one does it because they want to.
