Way before he served up the 9-9-9 Plan , Herman Cain was best known as the CEO of Godfather's Pizza. Which means that anyone doing oppo research on the GOP presidential front-runner du jour is going to have to dig through his past as a junk-food magnate, from his start as a business analyst at Coke and his rise through the ranks at Burger King to his eventual breakthrough as the first black executive to head a leveraged buyout of a fast-food company. 

A quick perusal of his old press clippings didn't turn up anything as potentially embarrassing as, say, this photo of a cash-hungry Mitt Romney , just a few morsels that are more cheesy than saucy. Enjoy! 



Cain bubbles up at Coke. Jet , April 25, 1974 





Cain as a Burger King veep. Ebony , April 1984 





This Pizza Man Delivers. Cain, after taking over at Godfather's Pizza. Black Enterprise , February 1988 





In 1986, Cain was named president of Godfather's Pizza, and by most accounts, it was an offer he could have easily refused. Ebony , April 1988 





Sampling a pizza is never a problem for Godfather's president. Cain is a frequent visitor to the company's test kicthens. Ebony , April 1988 





Herman Cain pieces together a hot deal. Black Enterprise, August 1988
