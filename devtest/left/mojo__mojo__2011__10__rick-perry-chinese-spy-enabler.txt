Over the past year, Gov. Rick Perry helped pave the way for China's largest telecom company a firm with ties to the Chinese military and intelligence services that have sparked concerns among defense officials and senior lawmakers to relocate some of its operations to Texas. 

Earlier this month, a report from the Office of the Director of National Intelligence on the Chinese military confirmed that the Huawei, the company in question, maintains close ties with the Chinese army. From The Washington Times : 

[The] report states that Huawei s 2010 annual report failed to mention that [Huawei chairwoman Sun Yafang], considered the most trusted aide to Huawei founder Ren Zhengfei, has ties to [the Ministry of State Security], fueling suspicions of potential close links between Huawei and the Chinese government. 

Mr. Ren was identified in the report as having worked for China's military from 1974 to 1983 in the engineering corps. The report says that Mr. Ren is purportedly China s most influential business leader who seldom mentions his military background in public. 

In April, a publication sponsored by China s State Council newspaper reported that Huawei received $36.8 million and $63.2 million in 2009 and 2010, respectively, from the government for domestic development, innovation, and research. 

The company also received $48.2 million and $80 million in 2009 and 2010 for completing certain research projects. 

There's more to this story, though. The Washington Times piece makes no mention of Perry's open courting of Huawei CEO Ren during a 12-day bridge-building trip to China this past summer. At the ribbon-cutting ceremony at the company's new headquarters last October (see video below), Perry had kind words for Ren: What a really interesting man he is. Rather straight-spoken. If you didn t know any better, you'd say he grew up out in West Texas. He truly is a very powerful chief executive officer and a very focused, very hard driven individual, which, in the world we live in today, is a great attribute. 

Huawei has been a serious concern for US national security officials for some time . In 2009, the National Security Agency warned AT T not to purchase Huawei equipment over concerns that Chinese intelligence could use it to secretly eavesdrop on Americans. And in 2010, eight Republican senators asked the Obama administration to investigate Huawei s effort to sell equipment to Sprint Nextel. Both deals ultimately fell through. 

You might dismiss China's potential infiltration into US communications networks via Huawei as Conversation -style conspiracy talk, but it's not totally unreasonable: Huawei has already reportedly tried to take over Iran's telecom system , and India has accused Huawei employees of selling spy technology to the Taliban. 

How does Perry, whose grave warnings about the national security threats on the campaign trail, explain his open embrace of Huawei? Eli Lake asked veteran Perry adviser Dave Carney that very question: 

Dave Carney, a strategist who has been with the governor for 14 years and would play a major role on a presidential campaign, defended these moves when I asked him about them. He said that it's Washington's job to vet corporations for national security reasons. If this Chinese Company is as evil as has been reported, then the federal government should step in to deal with it. 

Carney has a point: National security isn't Perry's purview. But as governor of a state that shares a 1,200 mile border with a foreign country, you'd hope that he'd be more critical of a company with a troublesome track record like Huawei. 

UPDATE: A spokesperson for Huawei contacted Mother Jones , and denied that the company has any long-standing ties to the Chinese military or intelligence services. The spokesperson added that the company is in the process of reaching out to the media to refute such allegations.
