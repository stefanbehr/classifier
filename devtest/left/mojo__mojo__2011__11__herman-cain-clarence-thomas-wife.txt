When in the course of human events a long-shot presidential candidate surges in the polls and finds himself battling multiple allegations of sexual harassment, it becomes necessary for said candidate to sit down with the one woman in America who can best understand his plight: Clarence Thomas' wife, Ginni. 

So naturally, that's what Herman Cain did on Wednesday, the same day a third woman stepped forward to allege that she had been sexually harassed by Cain while working at the National Restaurant Association. Thomas, who recently left a voice message for Anita Hill, the woman who accused her husband of sexual harassment, asking Hill to apologize, sat down with Cain for a Daily Caller exclusive. In the interview, Thomas peppered Cain with questions like, Are reporters setting you up to be guilty until proven innocent? and Is campaigning in Washington, DC a disorienting experience? Here's a characteristic exchange: 

GINNI THOMAS: 30 congressmen are calling for A.G. Eric Holder to resign over Operation Fast and Furious . Will you join them? 

HERMAN CAIN: I'm disappointed in all of the conflicting stories. I have not followed it closely enough to say that I want to pile on, but I happen to believe that 30 congressmen can't be wrong, in terms of the determination that they have made, that suggests that it may be better for him to step down. I trust those congressmen and the analysis that they made. 

To be clear: 30 congressmen can be very wrong, very easily. On any given issue, the odds are quite high that 30 congressmen are calling for something that Cain completely disagrees with. To choose a subject of concern for Cain: 220 congressmen voted for the Affordable Care Act or to put it another way, 7.33 groups of 30 congressmen voted for the Affordable Care Act. 

The bigger picture here is that Fast and Furious is another serious news story that Cain, by his own admission, hasn't paid any attention to. On Monday, he told an audience at the National Press Club he didn't have a position on student loans . It would be a lot easier for Cain to change the subject away from harassment if there was any other subject he was actually comfortable talking about.
