Things could be going better for Rick Perry's presidential campaign. He probably wishes he hadn't gone into a total free-fall in the national polls (and in Iowa), for instance. Maybe he regrets giving a speech in New Hampshire on Friday in which he sounded like he'd just shotgunned a bottle of Robitussin. But there's a reason Republicans still believe he has a shot to beat Mitt Romney: The Texas governor has a lot of money in the bank, and just as importantly, he's got a lot of friends with a lot of money in the bank. 

Perry has not one but two super-PACs working on his behalf dark money groups that can accept unlimited donations (including from corporate sources) the most notable of which is Make Us Great Again, founded by Perry's former chief of staff and long-time friend , Austin mega-lobbyist Mike Toomey. Make Us Great Again has a goal of raising and spending $55 million on Perry's behalf during the primaries alone, which is a lot. And now it's on the air with its first set of television ads making it the first super PAC to hit airwaves during the Republican primary: 



We're betting they won't be the last. 

Print Email Tweet The Elizabeth Warren-Scott Brown Proxy War Kentucky Governor s Race Devolves Into Debate Over Hinduism Tim Murphy 

Reporter 

Tim Murphy is a reporter at Mother Jones . Email him with tips and insights at tmurphy [at] motherjones [dot] com. RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Report: Perry Super PAC Expects to Raise $55 Million 

Meet Rick Perry s Favorite Lobbyist 

Rick Perry Figures Out How to Improve His Debate Performance 

Is This the Weirdest Rick Perry Speech Ever? 

It certainly has to be in the conversation. Chart of the Day: The Cost of the Perry Tax Plan 

Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
