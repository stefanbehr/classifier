More than 55 percent of Mississippi voters rejected an amendment to the state constitution that would have redefined legal personhood as beginning at conception. The so-called personhood measure was the most radically anti-abortion effort in the country, and would have outlawed all abortions and many forms of birth control. 

With 63 percent of precincts reporting, 57 percent of voters rejected the amendment, according to the Associated Press tally . 

The Center for Reproductive Rights called the amendment's defeat a tremendous victory for women in the state and across the country. Outlawing medical services commonly used and relied upon by Americans in their personal lives runs completely counter to the U.S. Constitution, not to mention some of our most deeply held American political traditions and values, said Nancy Northup, the group's president, in a statement. 

This is the third time voters have rejected this kind of measure at the polls: Colorado defeated similar efforts in 2008 and 2010. Abortion foes and pro-choice groups were both watching Mississippi avidly this year. Anti-abortion groups in a number of other states are at work on getting their own ballot initiatives in place for the 2012 election, as are some Republican congressmen . Mississippi, one of the most conservative states in the country, was seen as a test case for other measures.
