President Barack Obama's strategy for winning the Latino vote is to let Republicans keep talking. 

Elise Foley writes that in an interview with Latino reporters yesterday, Obama said that the GOP debates were winning the Latino vote for him. 

I don't think it requires us to go negative in the sense of us running a bunch of ads that are false, or character assassinations. It will be based on facts We may just run clips of the Republican debates verbatim. We won't even comment on them, we'll just run those in a loop on Univision and Telemundo, and people can make up their own minds. 

Why does the president sound so confident? A recent Latino Decisions poll showed that Obama was poised to capture the Latino vote in similar margins as in 2008, between 65 and 70 percent. Latino Decision's Gabriel R. Sanchez, however, says Obama's problem isn't so much that Latinos will vote Republican. It's that they won't vote. 

The majority of Latinos are saying 'not so much,' in terms of being enthusiastic about his candidacy, says Sanchez, who notes that 53 percent of Latinos polled said they are less enthusiastic about Obama after his first few years in office, and 48 percent were more excited about voting in 2008 than they are now. Thousands of disillusioned Latino voters staying home in a given state could mean the difference between defeat and a second term. The numbers might stay roughly the same in terms of vote share, but if turnout drops, that's problematic [for Obama], Sanchez says. 

Obama's other remarks highlight the contrast in rhetoric with his Republican opponents. The president told the group of reporters that Alabama's strict anti-immigrant law, the harshest in the country , was a bad law and not simply anti-immigrant, but I think it does not match our core values as a country. 

With little to show in the way of progress on immigration reform, that contrast may be the best argument the president has to offer Latino voters going forward. Whether it actually moves Latino voters to the polls is an open question. After all, Latino voters could say to themselves, what is a Republican president going to do, deport a million people ?
