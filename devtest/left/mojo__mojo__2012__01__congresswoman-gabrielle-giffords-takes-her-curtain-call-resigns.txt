On Wednesday, Gabrielle Giffords (D-Ariz.) stepped down from her seat in the House of Representatives. In her farewell speech read on the House floor by her colleague and close friend Debbie Wasserman Schultz (D-Fla.) Giffords thanked her colleagues and constituents, and briefly touched on key issues that have been close to her heart while in office: 

In public service, I found a venue for my pursuit of a stronger America by ensuring the safety and security of all Americans, by producing clean energy here at home instead of importing oil from abroad, and by honoring our brave men and women in uniform with the benefits they earned. I found a way to care for others. And in the past year, I have found a value that is unbreakable even by the most vicious of attacks. 

Here is C-SPAN footage of the emotional goodbye ( click here to read the full text of her speech, as well as her letter of resignation to Arizona Gov. Jan Brewer ): 



Giffords a respected Blue Dog Democrat has been recovering for the past year from a gunshot wound to the head she sustained during a 2011 shooting in Tucson, Arizona. At the Congress on Your Corner public event held in front of a Safeway on January 8, 2011, gunman Jared Lee Loughner opened fire, killing six attendees and wounding another thirteen . 

Unsurprisingly, an outpouring of support came from both sides of the aisle on Wednesday. House Minority Leader Nancy Pelosi called Giffords the brightest star among us, and said that she brought the word 'dignity' to new heights in her recovery from the assassination attempt. Rep. Mike Rogers (R-Mich.) tweeted : Rep. Giffords is an inspirational leader who will be missed [in] #Congress. R's, D's wish her nothing but the best as she continues to recover. 

On Tuesday night, reverence for Giffords was also on full bipartisan display as she attended President Obama's State of the Union address. Her arrival inside the House chamber was especially poignant. 

In her formal letter of resignation, also submitted to House Speaker John Boehner on Wednesday, Giffords wrapped up her farewell by promising, I will recover and will return.
