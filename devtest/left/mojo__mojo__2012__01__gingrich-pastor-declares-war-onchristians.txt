With revelations of Newt Gingrich's zeal for the sanctity of open marriage now dominating the news cycle, the former House Speaker may soon need to consult a higher power to help him field the tough family values questions that South Carolina's bedrock conservative Republican voters are likely to ask him. 

Luckily, Gingrich has someone much closer to home to help out: long-time friend Michael Youssef, an Atlanta-based televangelist who was just named the national co-chair of Gingrich's Faith Leaders Coalition . Youssef is an ardent pro-lifer, a virulent Islamophobe , and a gay-hater who has compared Mormonism to Islam (unfavorably). 

It's unclear if Gingrich's people did all their homework on Youssef , who has a history of attacking not only Muslims and gay people, but also Presbyterians and Episcopalians. Via the People for the American Way, here's an excerpt from a column Youssef wrote for the American Family Association last January: 

Can anybody in his/her right mind believe that the Episcopal Church is the Church of Jesus the Jesus who left the glories of heaven, came to our broken and dark world, died on the cross to redeem us and give us power over sin, and then rose again to assure us of eternal life with Him? The answer has to be a resounding, No! 

The Episcopal Church is not Jesus' church. The few...very few faithful ones left within this Church need to run for their lives lest they be held accountable for complacency on the Day of Judgment. 

In the pages of the AFA's One News Now back in May, Youssef urged Presbyterians and Episcopalians to leave behind this Chinese water torture method of homosexual lobbying i.e., their support for gay rights: 

Today, I'm appealing to all faithful Presbyterians in the PC(USA) and Episcopalians to vote with your feet and get out of these churches as fast as you can. As I mention in my latest book The Greatest Lie, this type of preaching is now invading many mainline and evangelical churches. 

There can be no excuse. 

No, you cannot stay and be a witness. 

No, you cannot stay and try to change things. 

No, you cannot stay and hope that you will be a light. 

These denominations have chosen darkness, and they need to experience what true darkness is all about by not having any believers inside their walls. 

To modify slightly the words of the apostle Paul, the faithful believers ought to deliver these institutions to Satan by walking out as fast as they can. (1 Corinthians 5:4-5) 

Putting anti-Muslim, anti-gay, anti-abortion Youssef front and center could be just what Gingrich needs to warm the hearts of some Palmetto State voters. But the televangelist's inflammatory judgments about Presbyterians and Episcopalians could also cause problems for the candidate: after all, Youssef's attacking Christians !
