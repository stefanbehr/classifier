At Thursday's GOP presidential debate in Charleston, Rick Santorum pulled out what he thought was the perfect anecdote for bureaucratic overreach. As he explained it, he'd talked to a state health official in Iowa and been informed that the state had actually been fined by the federal government because it didn't cover enough people under Medicaid. It was an example of well-intentioned big government gone bad and passing the burden onto a cash-strapped state. But was it true? 

As it happens, ABC's Huma Khan looked into this when Santorum first brought it up earlier in January: 

First, there is no Department of Public Welfare in Iowa, as Santorum stated. It s the Department of Human Services that disburses Medicaid grants. 

Second, it is unclear to what fine Santorum was referring. Iowa, like other states, receives federal reimbursement for the money it disburses in Medicaid fees. There is no quota system or target that the state has to meet in order to be eligible for federal money. The amount of money that each state receives is dependent on its economy. 

The formula is based on how well that state is doing economically and since Iowa is improving its economic status, we are soon to lose a couple percentage points, said Roger Munns, a spokesman for the Iowa Department of Human Services. This is not a punishment. This is a recognition that Iowa s economy is improving relative to other states. 

So: Not exactly the nightmare scenario Santorum warned us of. But it could be worse; when Michele Bachmann wanted an anonymous expert to back up her allegation that under Obamacare, IRS agents would be forced to approve every medical procedure, she claimed to have heard it first-hand from a seven-foot-tall doctor .
