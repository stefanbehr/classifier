Apple caved sort of. For the first time in its history, the beloved electronics company has started allowing labor inspectors with the Fair Labor Association into the factories that churn out Apple's most popular products, the company said Monday. 

The announcement is clearly a reaction to This American Life 's searing narrative on workers and working conditions in Chinese factories (including Apple's), Mr. Daisey and the Apple Factory, as well as the New York Times ' devastating iEconomy series , which sparked protests at Apple stores nationwide. One of the stories in the series, on the human costs of manufacturing the iPad , revealed dangerous, if not inhumane, working conditions at Apple suppliers. The story begins with this anecdote: 

The explosion ripped through Building A5 on a Friday evening last May, an eruption of fire and noise that twisted metal pipes as if they were discarded straws. 

When workers in the cafeteria ran outside, they saw black smoke pouring from shattered windows. It came from the area where employees polished thousands of iPad cases a day. 

Two people were killed immediately, and over a dozen others hurt. As the injured were rushed into ambulances, one in particular stood out. His features had been smeared by the blast, scrubbed by heat and violence until a mat of red and black had replaced his mouth and nose. 

That individual, a 22-year-old college graduate named Lai Xiaodong, would die in the hospital two days later. 

It was revelations like this that ignited an outcry about working conditions at Apple suppliers, and pushed Apple into allowing the FLA to inspect suppliers such as Foxconn , the Taipei-based mega-manufacturer that makes iPads and iPhones. But how seriously should we take Apple's announcement? 

The FLA, after all, has its own set of critics, who point out that the group is funded by the same companies Apple, Adidas, and Nike whose factories the group claims to be independently inspecting. The group also counts 200-plus universities as partners. Times labor reporter Steven Greenhouse quotes labor rights experts who call the FLA largely a fig leaf and hardly independent from the companies they're inspecting. FLA chief Jorge Perez-Lopez told Greenhouse the FLA's work isn't swayed by its funders. 

There are also larger, more systemic problems here that won't be solved by letting FLA inspectors into Apple factories. Suppliers cooking their books to pass inspections is all-too-common in China, as is the use of off-the-books, secretive shadow factories that inspectors never even see. And the auditing industry itself, even self-styled independent auditors, is plagued by bribery (factory bosses paying off inspectors) and corruption, according to labor and manufacturing experts. 

Apple's problems in China aren't the result of a few bad suppliers that can be rooted out by inspectors, either. As the group China Labor Watch wrote in a letter to Apple CEO Timothy Cook , the company's problems speak to a conflict at the heart of how Apple does business: 

We believe the most basic cause of the problems at your supplier factories is the low price Apple insists on paying them, leaving next to no room for them to make a profit. The demand for astronomically high production rates at an extremely low price pushes factories to exploit workers, since it is the only way to meet Apple's production requirements and make its factory owners a profit at the same time. 

To be fair, Apple's problems are not unique. They are faced by the entire electronics industry and its customers as they attempt to manage a global manufacturing system that locates factories wherever the cost of production is cheapest. The key choice Apple has to make as a company is whether it will try to shift the attention of journalists and the public towards the individual factories that make their products, or will sincerely acknowledge its responsibility for these factories' deplorable working conditions and make systemic changes to its supply chain. 

Dispatching the FLA into supplier factories won't fix these deep-rooted problems. In a recent email to Apple employees, Cook said the company's reforms would reach deeper into the supply chain. That's more along the lines of what it will take to truly fix labor problems in countries like China. Anything less is lip service. 



Front page image: Workers on the production line at the Foxconn factory in Shenzhen, China. 

ChinaFotoPress/Zuma
