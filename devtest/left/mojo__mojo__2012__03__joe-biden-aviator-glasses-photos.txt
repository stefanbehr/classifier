In early March, Team Obama announced it had decided to release the Biden : The pugnacious, filter-free VP would be returning to the campaign trail. Soon after, the campaign posted this photo to their website next to the headline Welcome Joe back to the trail : 



Yep. That's the 47th vice president of the United States at a 2012 campaign event, acting out a one-liner from CSI: Miami . Or preparing to gun down a yak from 200 yards away with the power of his mind bullets . Or simply striking a pose that might be described as Bidening. The Internet jubilantly had its way with the image . Grist blogger David Roberts dubbed it the most Joe Biden-y photo ever taken. 

Here are some more photos of Joe Biden being Biden or at least doing a pretty good impression of The Onion version of himself: 

Biden, at his part-time job as vice president of the United States of I-Wear-These-Aviators-Better-Than-You-Do. 



In May 2011, probably saying something like, Hey remember that time four days ago when we annihilated Bin Laden? That was pretty sweet. 



Yet another in a series of photos of Biden cracking up the president. Here, he hangs up his Aviators to play Angry Birds last July, debt-ceiling crisis be damned. 



Showing up in Kandahar looking exactly like Steven Seagal in Machete while Sen. Lindsey Graham (R-S.C.) cowers behind him. 



A bromantic high-five. 



In Iraq. Wearing the Aviators. Of course. 



Ignoring a small child. And possibly telling the president a dirty joke. 



Biden and Obama, blowing off running the country for a few minutes of taxpayer-funded dicking around. 



Oh hey Biden. Are you just bidin' your time? Or is that a UFO you see out there? 

Image credits: TaraGiancaspro /Flickr; Pete Souza /The White House; Pete Souza /The White House; US Navy Petty Officer Aramis X. Ramirez /isafmedia ; Pete Souza /The White House; The US Army /Flickr; Pete Souza /The White House; Pete Souza /The White House; Pete Souza /The White House
