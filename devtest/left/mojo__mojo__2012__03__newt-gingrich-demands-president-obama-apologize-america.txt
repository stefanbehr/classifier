Last Wednesday, GOP presidential candidate and aspiring sloth-watcher Newt Gingrich lamented the inability to have a serious discussion about America's future because the news media and his opponents can't comprehend the enormity of his ideas. 

On Tuesday, he demanded President Obama apologize for Robert DeNiro. Via Benjy Sarlin : 

Newt Gingrich is incensed about a joke by actor Robert DeNiro at a fundraiser attended by Michelle Obama for the president s re-election, in which the Academy Award-winning star used the word white to describe the Republican field s spouses. 

Callista Gingrich. Karen Santorum. Ann Romney. Now do you really think our country is ready for a white first lady? DeNiro said. Too soon, right? 

... 

What DeNiro said last night was inexcusable and the president should apologize for him. It was at an Obama fundraiser, it is exactly wrong, it divides the country, [Gingrich] said. If people on the left want to talk about talk show hosts, then everybody in the country should hold the president accountable when someone at his event says something that is utterly and terribly unacceptable as what Robert DeNiro said. 

No word yet on whether Gingrich thinks Obama should apologize for Little Fockers . 

Meanwhile, in more serious Newt Gingrich news, Jonathan Martin and Ginger Gibson report that the former speaker's campaign is actually kind of a wreck, and is slowly expiring in all the usual ways of terminal campaigns at the end-stage: Cash is running low, supporters are griping about not getting paid and aides are valiantly trying to convince themselves as much as the press that, really, there is a path forward.
