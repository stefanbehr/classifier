Hitler does not have a patent on adverbs. 

It all started right after Team Obama debuted their new campaign's slogan: Forward the long-awaited sequel to 2008's Hope and Change. And in those seven letters, members of the conservative commentariat detected a whiff of totalitarianism. 

On Tuesday, ThinkProgress editor Alex Seitz-Wald threw together a primer on the bizarre, petty, and not entirely unexpected freak-out. For example, Bill Kristol of the Weekly Standard criticized the president for having signed off on a word so closely linked to Chairman Mao's mass-murder-tastic Great Leap Forward. ( [P]erhaps President Obama might rethink this slightly creepy slogan, Kristol pondered earnestly.) Breitbart.com 's Joel Pollak ( this guy ) wrote about how the seven-letter slogan is further proof that Obama's political heritage belongs to a long line of Communist tyrants. Jim Hoft at Gateway Pundit took Forwardgate as his cue to yet again draw the Obama-Hitler connection . 

There you have it: The 44th President of the United States and his campaign staff like to use words. Communists and fascists throughout history were also known to have used words. 

It's the same kind of bulletproof logic you'd get from Dave Chappelle's Conspiracy Brother in Undercover Brother . 

Here are some other conclusions that follow the same line of reasoning that begot the Forward backlash. You can apply the formula to anyone, really. 

Obama: 

The White House /Flickr You know who else liked dogs, don't you? 

German Federal Archive Supermodels: 

You know who else really loved horsies? 

Biggie: 

Wikimedia You know who else knew where Brooklyn at? 



I think we're done here.
