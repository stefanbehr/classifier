A quick look at the week that was in the world of political dark money ... the money shot 









quote of the week 



Your defense of Crossroads' legal position last night on Fox News was both mystifying and revealing. 

Robert Bauer, the Obama campaign's chief legal counsel, in his second snarky letter to Karl Rove this week. He's demanding that Rove disclose the donors to his dark-money nonprofit Crossroads GPS, which claims to be a social welfare group. ( Read about the first letter here .) 

attack ad of the week 



Earlier this month, President Obama told reporters at a White House press conference that the private sector is doing fine. By that, he meant that the private sector has fared better as of late than the flailing public sector. But the comment makes for a great attack-ad sound bite and could become a headache for Obama. The Romney campaign was the first to pounce on the gaffe with an ad last week . Earlier this week, the pro-Romney super-PAC Restore Our Future came out with with its own ad; it was followed by this ad by the Koch brothers-affiliated Americans for Prosperity: 



Fake attack ad of the week 



Courtesy of the Mother Jones DC news team, a cautionary look at what could happen if dark money invaded the land of Westeros . Watch the Crossbows GPS spot below. (Warning: the videos contain mild spoilers from season two of Game of Thrones .) 

STAT of the week 



$94.8 million: The amount of money spent by dark-money social welfare groups like Crossroads GPS in the 2010 election, according to research by the Center for Responsive Politics and the Center for Public Integrity. That's 45 percent more than super-PACs spent during the same cycle. 

more mojo dark-money coverage 



Follow the Dark Money : The down and dirty history of secret spending, PACs gone wild, and the epic four-decade fight over the only kind of political capital that matters. 

3 Companies, 1 PO Box, and a $1 Million Super-PAC Gift : If Houston computer magnate Robert T. Brockman wants to cover his tracks, he's not trying hard enough. 

How to Sweep Dark Money Out of Politics : Undoing Citizens United , the DIY guide. 

Citizens United vs. Sideboob : The court is more worried about a little skin on TV than unlimited corporate money in elections. 

Connecticut's Governor A Democrat! Sides With Dark Money : Gov. Dannel Malloy vetoed the disclosure bill over policy and constitutional worries, but reformers say he's way off mark. 

more must-reads 



Thanks to outside spending groups, Barack Obama may be the first incumbent president to be out-raised by his opponent. AP 

Mitt Romney plans to mingle with deep-pocketed donors at Republicanpalooza this weekend. New York Times 

Will Karl Rove's donor disclosure dodges lead to Crossroad GPS' downfall? Huffington Post 

The US Chamber of Commerce works with Republicans to block a donor disclosure bill. iWatch News 

Stephen Colbert's super-PAC barely raised any money last month, but it's inspired copycats like My Cat Xavier for a Better Tomorrow, Tomorrow. Politico 

Print Email Tweet Corn on MSNBC: Romney s Super PAC Ski Trip Big Waffle Goes All in for American Crossroads Gavin Aronsen 

Writing Fellow 

Gavin Aronsen is a writing fellow at Mother Jones . For more of his stories, click here . Follow him on Twitter or send an email to garonsen [at] motherjones [dot] com. RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Your Weekend Longreads List on Dark Money 

Some of MoJo's favorite in-depth stories on money and American politics from the New Yorker, Texas Monthly, and our own reporters. Meet the Bald-Headed Bastard Who Battles Dark Money 

Fred Wertheimer's four-decade crusade against secret money in US politics began with an interrupted nap. Follow the Dark Money 

The down and dirty history of secret spending, PACs gone wild, and the epic four-decade fight over the only kind of political capital that matters. How to Sweep Dark Money Out of Politics 

Undoing Citizens United, the DIY guide. Monika Bauerlein and Clara Jeffery Talk Dark Money With Bill Moyers 

On the war for Wisconsin, the work of "journo-detectives," and the law that underlies all other law. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
