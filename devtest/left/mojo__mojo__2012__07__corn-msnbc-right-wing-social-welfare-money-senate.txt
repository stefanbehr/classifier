David Corn joins The Washington Post 's Eugene Robinson and Democratic strategist Julian Epstein on MSNBC's Martin Bashir to discuss how conservatives are planning to funnel hundreds of millions of dollars into politically-driven social welfare organizations, possibly tipping the balance of the Senate. 



David Corn is Mother Jones' Washington bureau chief. For more of his stories, click here . He's also on Twitter .
