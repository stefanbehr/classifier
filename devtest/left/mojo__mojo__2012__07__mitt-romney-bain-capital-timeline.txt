Since his first run for office in 1994, Mitt Romney has been dogged by charges that the private equity firm he founded profited from outsourcing and shuttered companies. But over the last two weeks, new documents emerged to call into question the GOP presidential candidate's narrative about his time at the company. At issue are two primary questions: Was Mitt Romney responsible for decisions made by Bain Capital between 1999 and 2002? And did the company's behavior in those years building huge profits by squeezing companies dry reflect Romney's broader mission at Bain? The answer to the first question depends on your definition of the term managing director ; the answer to the second question is a subject of fierce debate. 

Here, as best as we can figure, is Romney's timeline at Bain: 



Open-source timeline tool by Balance Media and WNYC/John Keefe . Try it yourself here!
