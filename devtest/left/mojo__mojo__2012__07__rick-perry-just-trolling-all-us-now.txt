On Tuesday, Texas governor Rick Perry kind-of-sort-of called on Mitt Romney to release his tax returns (and President Obama to release his college transcripts), telling reporters, I'm all about transparency . This has been taken as a sign that Republicans are increasingly frustrated with Romney's secrecy regarding his tax returns, which is probably true. But the larger takeaway here is that Rick Perry is trolling all of us because Rick Perry is absolutely not all about transparency. 

As my colleague Andy Kroll explained last August: 

Perry and his administration have withheld information in 100 public-records requests during his time in Austin, and on occasion failed to respond on time to other records requests as required by state law. His administration has also refused to hand over notes and records about how the state's two honeypots for economic growth, the Emerging Technology Fund and the Texas Enterprise Fund, decided to dole out grant money, including on one occasion to a company owned by a Perry donor. The Chronicle went so far as to sue the Perry administration for refusing to hand over notes on its decision not to grant clemency to Cameron Todd Willingham, a man who was executed in 2004 after being convicted of multiple murders on the basis of flawed arson pseudoscience. 

He didn't just walk the walk, he talked the talk. In October 2010, when he was pressed for more details on his public schedule, Perry told reporters : I think we give so much information already that it is boring. 

Oh, and he's still blocking me on Twitter .
