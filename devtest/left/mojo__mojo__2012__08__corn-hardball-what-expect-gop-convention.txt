David Corn and Erin McPike joined host Chris Matthews on MSNBC's Hardball live at the Republican national convention to discuss how the speaker lineup illuminates the fault lines in the GOP and the Romney campaign's strategy in Tampa. Read more MoJo coverage of the convention here . 



David Corn is Mother Jones' Washington bureau chief. For more of his stories, click here . He's also on Twitter .
