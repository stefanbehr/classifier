David Corn and Howard Fineman joined Chris Matthews on MSNBC's Hardball to discuss the wimpy bounce in the polls the accompanied the GOP convention, the campaigns' use of microtargeting technologies to turn out the vote, and how the presidential contest will play out in Virginia and other swing states. 



David Corn is Mother Jones' Washington bureau chief. For more of his stories, click here . He's also on Twitter .
