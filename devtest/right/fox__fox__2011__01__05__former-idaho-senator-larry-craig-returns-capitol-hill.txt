Disgraced former Sen. Larry Craig , R-Idaho, was spotted in the tunnel running between the Longworth and Cannon House Office Buildings on Capitol Hill . 

Few have seen Craig after he left the Senate following his arrest in the bathroom of the Minneapolis-St. Paul airport in a 2007 sex sting. 

Fox News stopped Craig and asked him why he was in town. 

Craig said he was here to help welcome and congratulate Idaho's new Congressman, Rep. Raul Labrador, R-Idaho. Labrador defeated former-Rep. Walt Minnick, D-Idaho, in November's midterm election.
