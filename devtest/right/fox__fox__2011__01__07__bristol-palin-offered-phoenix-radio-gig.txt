She may be moving from snowy Alaska to the Valley of the Sun, and now a Phoenix radio station has offered Bristol Palin a job co-hosting a morning show in what could be her new hometown. 

In a letter posted on its website , KMXP-FM, known as Mix 96.9, offers Palin the job. 

"Because of your appeal to the Mix audience, your upcoming move to the Valley, and your natural talent, I would like to offer you a salaried on-air position as a co-host on our morning show," the letter says. 

It goes on to state that compensation for the job is negotiable and that the station could be flexible with scheduling, even suggesting she could work from other cities. 

"We are not looking for a political commentator, nor are we looking to push a political agenda on Mix 96.9," the letter says. "Along with providing a great career experience, this is an opportunity that can grow as big as you would like it to grow for yourself." 

The eldest Palin daughter is reported to have bought a house in Maricopa, Arizona, about 25 miles south of downtown Phoenix.
