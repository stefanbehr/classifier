Rep. Louise Slaughter, D-N.Y., said in a Monday afternoon conference call that the Federal Communications Commission (FCC) should work harder to sanction broadcasts that could incite people to violence. 

"No one owns the airwaves," Slaughter said, "They are owned by the people." 

Noting that she was not up on the current state of FCC regulations, Slaughter thought that during the recent health care debate, "she really heard" exhortations to violence in the nation's political dialogue. 

"I think that the dialogue has changed," Slaughter said, "Don't retreat, reload. Someone in Nevada saying we may need to use Second Amendment remedies. There's only one way to read this." 

Slaughter also voiced concerns about James Lee Loughner's ability to purchase a firearm in the state of Arizona. Loughner is accused of killing six and wounding fourteen people at a "Congress on Your Corner" event in Tucson, Arizona on Saturday. The host of that event, Rep. Gabrielle Giffords, D-Ariz., remains in the University of Arizona Medical Center with a gunshot wound to the head. 

"He was dismissed from a community college for not being well," Slaughter noted, "Yet he was able to go to a store in Tucson in November and buy a Glock 9 (millimeter handgun) which he put an extender on that allowed him to shoot 30 times."
