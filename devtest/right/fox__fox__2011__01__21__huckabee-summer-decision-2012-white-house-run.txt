As the 2012 presidential election cycle nears, former Arkansas Governor Mike Huckabee , who is among top potential Republican candidates, said Friday that he will wait to make a decision on a White House bid until the summer. 

In an interview with FOX News' Martha MacCallum, Huckabee indicated that he did not want to announce early because he didn't want prospective voters to see him as a "stale loaf of bread on the shelf." 

"I think people get sick of us if we're out there for too long a period of time," said Huckabee, who came in second to Sen. John McCain , R-Ariz., during the 2008 Republican primary. 

"By the time it starts to matter, when you're getting closer to the caucuses and the primaries, you've been so, just, essentially, exposed in a campaign mode that there's nothing new," he added. 

Huckabee announced his 2008 presidential campaign in late January of 2007. At the time, many political experts put his quest in the long-shot category. 

However, Huckabee's underdog campaign ended up winning the Iowa Caucus, spurring an 18 month bid that left him as the last man standing against McCain, the eventual GOP nominee. 

"For me, having done this four years ago, I realize that very few people can sustain the burn rate of a campaign if it's going to have to last 18 months. You can't raise and go through that much money and the infrastructure necessary to carry the campaign, "said Huckabee. 

Mike Huckabee is the Host of "Huckabee" on FOX News Channel and is a FOX News contributor.
