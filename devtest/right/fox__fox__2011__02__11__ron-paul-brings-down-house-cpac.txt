If anything can be gauged by applause in main ballroom, Ron Paul will once again win the Presidential Straw Poll here at CPAC. 

Paul won the contest last year, much to the chagrin of many of the party faithful who attended. 

This year Paul was the only potential presidential candidate to directly tackle the tricky subject of Egypt . 

"We've invested $70 Billion in Egypt ... and all we get is chaos for it," said Paul. "I'm still against foreign aid for everybody." 

Paul hit on the idea of isolationism repeatedly. "The Founding Fathers talked about staying out of foreign entanglements when it's none of our business." 



Paul also revisited many of his favorite subjects, including ending the Federal Reserve Bank, protecting civil liberties and repealing the Patriot Act. 

"The Patriot Act is literally the destruction of the fourth amendment." 

Ron Paul supporters have been the loudest supporters over the course of CPAC, picking a fight with Donald Trump and trying to boo Donald Rumsfeld and Dick Cheney off the stage Thursday. 

Though it would surely enrage many Republicans if he wins the straw poll Saturday it should surprise no one.
