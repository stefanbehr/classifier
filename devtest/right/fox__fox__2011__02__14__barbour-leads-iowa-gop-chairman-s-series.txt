Mississippi's Republican Governor Haley Barbour says it'll be April before he decides whether he'll run for President in 2012. 

But while Barbour considers the pros and cons of a White House bid, he'll be spending some time in Iowa. 

The state GOP has announced Barbour will lead-off the "Chairman's Series," a string of political events involving "national political figures." 

Chairman Matt Strawn sees the series as an opportunity to bolster the Iowa Republican Party statewide. It's also a chance for potential presidential candidates to test-drive Iowa's political climate. 

Barbour's appearance is set for March 15th in the Quad Cities area. Iowa GOP communication director Casey Mills says the event will likely be in Davenport, but the venue and time have not been determined yet.
