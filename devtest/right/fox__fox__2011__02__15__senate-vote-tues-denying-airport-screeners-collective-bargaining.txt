UPDATE: The Wicker Amendment to deny TSA agents the ability to collectively bargain was defeated by a vote of 47 to 51. It failed to meet the 60 vote threshold needed for passage. 

As the Senate continues its debate on the FAA Authorization bill, a key amendment barring Transportation Security Administration (TSA) agents from collectively bargaining will receive a vote later Tuesday afternoon (aprox 230pm). The ban, sponsored by Sen Roger Wicker , R-Miss., seeks to nullify recent action by the Administration to allow the nation's 43,000 airport screeners to negotiate for nonsecurity-related work measures and benefits, like pay scale and vacation. 

Republicans fear that any union influence (typically collective bargaining agreements are negotiated by a union) could jeopardize national security and disrupt airport operations. 

Supporters of the right for TSA workers note that many other first responders have not been hampered by their unions, citing the many law enforcement and emergency medical personnel who rushed to Ground Zero in Manhattan on September 11, 2001. 

The Wicker amendment must overcome a high hurdle, 60 votes for passage. It is unclear if he and his backers, including the top Republican on the Senate Homeland Security panel, Susan Collins of Maine, can muster enough support. But the two modified the amendment Monday to help boost its chances, allowing TSA screeners to fall under the Whistleblower Protection Act, and allowing the security officials, should they choose, to join a union. 

TSA chief John Pistole told lawmakers last Thursday he would be "willing" to fire TSA employees en masse should they go on strike or cause a slowdown in operations.
