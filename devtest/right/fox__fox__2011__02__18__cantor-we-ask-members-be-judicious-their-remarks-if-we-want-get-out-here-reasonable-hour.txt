Conversation on the floor between House Majority Leader Eric Cantor (R-VA) and House Minority Whip Steny Hoyer (D-MD) about the schedule. 

Cantor: We ask members to be judicious in their remarks if we want to get out of here at a reasonable hour. 

Cantor: We have been at this for at least 90 hours. 

The headline here is that they agreed via unanimous consent to cut back the time of debate on some amendments from ten minutes to six minutes. There are currently 50 amendments on the GOP side and three on the Democratic side pending...plus one colloquy. 

So that would be about 5 hours and 20 minutes of debate from this point forward. Tack onto that a couple of hours of voting plus "injury and booking time... 

Sooo..Unless they yield back time...this easily gets us close to 6 am. 

Rep. Norm Dicks (D-WA), the top Democrat on the House Appropriations Committee implored Republicans to try to chop down their amendments. 

"I hope that a lot members on your side would consider yielding back their amendments so we can get the hell out of here," Dicks said. 

House Appropriations Committee Chairman Hal Rogers (R-KY) concurred. 

"The shorter we can make our speeches, the better off we are," Rogers said.
