Highlighting the unrest in Africa and the Middle East along with increasing energy prices, Alaska's governor says the solution to America's energy security and economic woes is simple but officials in Washington refuse to budge when it comes to increasing oil exploration and production in his home state. 

"We stand ready to create these jobs, improve our nation's energy security, our economy and our national security," Republican Sean Parnell said Friday in advance of meetings with fellow governors who've gathered in Washington D.C. 



His speech at the National Press Club was billed as a major address on energy policy and was an unapologetic pitch for increased drilling in Alaska. Parnell said bureaucrats in Washington have been too willing to issue uninformed edicts that purportedly protect the environment but are done with little regard to the concerns of Alaskans who are directly impacted by the federal government's actions. 

Parnell likened his cause to those of other oil-producing states and says he wonders why "the federal government has become openly hostile to a sector of our economy that has created hundreds of thousands of jobs, kept the country on an even keel during the recession, and produces a commodity that we all use every day." 

Alaska voters in November elected Parnell to a full-term in the office that he inherited in 2009 when now-former governor and FOX News contributor Sarah Palin stepped down. 

Parnell specifically pointed to policies from the Interior Department and the Environmental Protection Agency that he says are driving U.S. foreign policy in the Middle East. But Parnell struck a cooperative tone when he said a change in leadership at the White House isn't necessary to change what he calls Washington's "no new wells" policies. 

"[President Obama] can look at what is happening overseas and say, 'we can take control of this at home and we can have more exploration and production here at home.' But it's going to take regulatory agencies that are given that kind of mandate." 

Parnell also noted the calamity of the 1970's energy crisis with gas lines and rations. He said his history lesson wasn't meant to suggest that such scenes would play out again today but rather called it an "economic disruption" similar to what's happening now. 

As often happens when gas prices go on the rise, there's been increasing talk that President Obama should release oil from the nation's strategic reserves. Parnell said it wasn't his place to give advice to the president on what to do but said, "if we had more domestic production we would need less from the strategic petroleum reserve."
