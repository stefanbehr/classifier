Congressman Allen West, R-Fla., says he has his constituents' support, even if the debate over spending leads to the first government shutdown since 1995. 

During a Saturday interview with Fox News, the freshman congressman said he met with constituents this week and they don't want him to back down on spending cuts. 

"They want us to stay the course," said West. "They don't want us to be the first to blink." 

West defeated incumbent Democrat Ron Klein to win Florida's 22nd Congressional District in the 2010 midterm, thanks to considerable Tea Party support. West says he was elected to tackle wasteful spending and growing debt. 

"The people sent us up here for fiscal responsibility. That's what we're promoting," says West. 

New York Senator Chuck Schumer disagrees with such hard-handed tactics, and says forcing a government shutdown is "reckless and irresponsible." 

When asked about Schumer's comments, West told Fox News, "If [Schumer] wants to talk about reckless and irresponsible' he needs to look at himself in the mirror." 

West didn't stop there, telling Fox News, "I would say to Senator Schumer that reckless and irresponsible' is the 1.42 trillion dollar deficit, the 1.29 trillion dollar deficit, the 1.65 trillion dollar deficit over the past three years, the five trillion dollars of increased debt that we've seen in the past four years, the fact that we have been handed a 2012 budget that does nothing but increase spending, increase borrowing and increase taxes." 

West says he supports a short-term continuing resolution that would keep the government running for two weeks past the March 4th deadline. But that doesn't mean House Republicans are going soft on their push for major spending cuts. 

"This continuing resolution, solution, that we're offering to Harry Reid and the President does make sure that the government continues on, but also makes sure we begin to tackle the problem of spending in Washington, D.C." says West. 

The Republican congressmen added that President Obama is to blame if the government shuts down, not the GOP. 

"If [Obama] is trying to play a political gamesmanship to try to once again re-create what happened in back in 1995, because he believes that will help his efforts toward winning the presidency in 2012, that's not being a president," says West. "That's not being a leader, that's not standing up and making the hard decisions that need to be made for this country."
