UPDATE: 4:30p.m. 

Fox News has learned Newt Gingrich will actually travel to the First in the Nation Presidential Caucus state of Iowa on March 7th. 

Gingrich will address the Iowa Faith and Freedom Coalition. This is Newt's third trip to the Hawkeye State in 2011 and his ninth visit since 2010. 

2:05p.m. Monday 

Sources close to former House Speaker Newt Gingrich tell Fox News they expect Newt to make a decision this week about whether he will form an exploratory committee. 

Newt himself has long said that during the month of February he would make up his mind about a possible 2012 presidential run, and would follow that up with some sort of announcement in March. 

The Atlanta Journal-Constitution's Jim Galloway reports "I was on a State Bar panel Saturday afternoon with Randy Evans, the Atlanta lawyer in charge of Newt Gingrich's business interests. ... Before we parted, Evans told me this: 'In 10 days, Newt Gingrich will be in Georgia, announcing his exploratory committee." 

However, while sources close to Gingrich tell us the decision will likely be made this week they are waving us off any imminent announcement in Atlanta, "You don't need to be booking plane tickets to Atlanta... When and how to make any official announcement would be a whole other decision making process." 

Newt Gingrich is a Fox News contributor.
