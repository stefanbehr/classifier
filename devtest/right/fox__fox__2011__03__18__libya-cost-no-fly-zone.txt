According to new report by the Center for Strategic and Budgetary Assessments, three options for a No-Fly Zone over Libya would have three very different costs: 



"Full No-Fly Zone" covering all of Libya 

- $100 million to $300 million per week 

- Initial strike to secure airspace: $500 million and $1 billion 

- Six month total: $3.1 billion - $8.8 billion 

- Similar to no-fly zone imposed over Iraq (Operation Northern and Southern Watch) 



Limited No-Fly Zone focusing on the northern third of Libya 

- $30 million to $100 million per week 

- Initial strike to secure airspace: $400 million to $800 million 

- Six month total $1.18 billion - $3.4 billion 



Stand-off No-Fly Zone focusing on costal Libya with only air and naval assets beyond Libyan territory 

- $15 million to $25 million per week 

- Because this is strictly a stand-off operation with no assets in Libya, CSBA suggests no "initial cost." 

- Six month total $0.39 billion - $0.65 billion 

- This No-Fly zone would be enforced by three aegis-equipped destroyers. 

- These ships, supported by radar monitoring planes (AWACS), and land-based fighter aircraft would intercept violating aircraft from a distance with "over-the-horizon" missiles. 

- There is no historical precedent for this sort of no-fly zone. 



The group also estimates a 6 month No-Fly Zone could cost as much as $9 Billion. Here's a look at the costs of previous No-Fly Zones: 



3 months of air superiority over Serbia cost $2.4 billion 

No Fly Zone over Iraq cost $1.3 billion per year 

Libya is 6.5 times larger than the No Fly Zone over Iraq. 

CSBA (Center for Strategic and Budgetary Assessments) estimates a 6 month No Fly Zone could cost as much as $9 billion.
