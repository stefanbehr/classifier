The most recent Chief of Staff for Minnesota Congresswoman Michele Bachmann says he intends to support former Minnesota Governor, Tim Pawlenty in the GOP race for President. 

Ron Carey served as Bachmann's fifth Chief of Staff before resigning last summer during Bachmann's reelection campaign. 

Carey, former chairman of the Minnesota Republican Party , says he is not surprised by Bachmann's decision to launch a presidential exploratory committee before June, citing her numerous visits to Iowa to meet with supporters. 

But Carey feels Tim Pawlenty is the best candidate to make it across the finish line in November 2012. 

"My experience with Pawlenty , the more people get to know him, he's a very sharp guy. He's very likable," Carey said. "I see him walk into a room of 200 people and everyone he meets leaves the room and says, 'he's a great guy.' He has that Midwest charm and ability to connect with people." 

Carey adds, "His [Pawlenty's] roots are so humble and main street America. It's easy for people to connect with him and say, 'he's one of us, he's just like me.' " 

Carey points out, it's one thing to be able to give a speech and fire up a big crowd, but it's quite another to win enough votes to get elected. "Electability is a very, very high attribute you have to have this year to win. I don't want to have an emotionally filled endeavor only to get 35 percent [of the vote] in November [2012]." 

"I hope the Republican faithful connect their heads with their hearts. You have candidates to get people fired up, but can they actually build a coalition? That's why I'm supporting Pawlenty, " Carey says.
