MIAMI- In a surprise move, Republican Florida Congressman Connie Mack IV has decided not to run for the U.S. Senate seat that was formerly occupied by his father, and currently held by U.S. Senator Bill Nelson , a Democrat up for re-election in 2012 according to a source close to Mack. He made the announcement in Fort Myers Friday morning. 

"I've got two small children and it's hard enough to get to spend a lot of good quality time now. I have a wife. They are all very important to me and at the end of the day family has to be number one,'' Mack told the St. Petersburg Times. 

The four term congressman was expected to join a growing GOP field of senatorial prospects who hope to oust Nelson, an incumbent who has served 2 terms in the Senate, and the only Democrat left in Florida to hold prominent office. 

Currently, State Senate President Mike Haridopolos, the early favorite, is the only Republican that has offiically entered the race, but several other prominent Republicans including former U.S. Senator George Lemieux, and Adam Hasner, former Florida House Majority Leader , are expecting to join the primary battle soon. 

Mack is married to current California Congresswoman Mary Bono Mack. His father Connie Mack III remains an influential political figure in Florida who served as both U.S. Senator and congressman.
