New unemployment numbers being lauded by the Obama administration aren't impressing everyone and one skeptic is sounding an alarm from the heart of organized labor. 

"Nearly two years after one of the worst recessions in our history -- certainly the worst one in our lifetimes -- our economy is showing signs of real strength," the president said Friday. "Today we learned that we added 230,000 private sector jobs last month. That's good news." 

But Mark Brenner, a labor economist and the director of labornotes.org , a website dedicated to voicing the concerns of organized labor, says Friday's numbers aren't something the administration should be "shouting from the rooftops." More than the number of jobs created, Brenner argues, the White House should be more focused on the quality of new jobs. 

"If you dig into the numbers, I think there should be a lot more hand-wringing than backslapping," Brenner said. "If you think you can build a recovery around more Home Depot cashiers and Wal-Mart greeters you need to have your head examined." 

With organized labor typically staunchly loyal to Democrats, Brenner says he knows he is an "outlier;" a voice of organized labor criticizing a Democratic administration as it touts labor statistics. But he thinks recent warm relations between the White House and the business community aren't being used to address job quality, even with the uptick in creation. 

"Almost half of the new jobs in the private sector are in low-wage occupations like retail trade, food service, and temporary services, with few signs of life in higher wage occupations. If you're looking for jobs that can help you pay for a mortgage or send your kids to college, you're out of luck." 

Brenner believes going back to the 1990's, most Democrats and Republicans have been more concerned about free trade and professional classes rather than the working class. And he sees more economic trouble ahead for the working class as state and municipal budgets shrink. 

"Once the ax starts falling on state and local budgets, private sector growth is likely to stall, if not completely reverse in some sectors, most notably in healthcare where government funds account for one out of every two dollars spent," Brenner says.
