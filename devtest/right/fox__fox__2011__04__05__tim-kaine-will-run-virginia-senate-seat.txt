Tim Kaine , chairman of the Democratic National Committee , ended the speculation Tuesday by announcing that he will run for the open Senate seat in Virginia. Over the past several weeks, the former popular governor of the Old Dominion has hinted at a run but he made official today with a web video. 

"I'm running for the United States Senate because America has big challenges and I'm convinced Virginia has answers to help strengthen our nation," said Kaine in the video. 

Mr. Kaine's decision to run is a victory for Democrats who are working to hold on to the seat being vacated by freshman Democratic Senator Jim Webb . President Obama has publicly encouraged Kaine to run, and Democratic sources tell Fox News that behind the scenes there was significant pressure coming from the White House for Kaine to run. 

The most likely opponent for Kaine in the general election is former Senator George Allen , the Republican who is hoping to reclaim the seat he lost to Senator Webb in 2006. 

Following Kaine's Tuesday afternoon announcement, Allen's campaign was quick to go on offense. "While Chairman Kaine may try to paint a different picture of his tenure as Governor, it was maked by his proposals calling for staggering tax increases and by substantial job losses for Virginians," said Allen campaign communications director Katie Wright. 

The race was always likely to be one of the most competitive races in the 2012 cycle, but now that it may pit two well known politicians against each other right across the Potomac from the Capitol , it has the potential to be among the most closely watched contests in the nation.
