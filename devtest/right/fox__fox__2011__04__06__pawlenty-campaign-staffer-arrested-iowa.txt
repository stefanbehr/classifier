The very first Iowa staffer hired by Republican presidential candidate Tim Pawlenty has been suspended without pay from the campaign after his arrest overnight. 

24-year-old Ben Foster was arrested by the Polk County Sheriff's Department on charges of public intoxication and trespassing. 

Des Moines television station KCCI reports Foster was arrested at about 3:00 a.m. Wednesday after a 15-year-old girl allegedly discovered Foster banging on the door of her Ankeny home. 

Through the Pawlenty campaign, Foster issued the following statement: 

"Last night, I made a very serious mistake. I take full responsibility for my actions. I want to apologize to all affected by my poor judgment. I especially apologize to the people who were disturbed during the incident and the arresting officers. I give my word that it will never happen again." 

Eric Woolson, a spokesman for Pawlenty's Iowa campaign says Foster is expected to "bear the consequences for his action."
