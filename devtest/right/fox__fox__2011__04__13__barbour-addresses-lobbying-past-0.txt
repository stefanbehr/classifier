If anyone's wondering whether Mississippi Governor Barbour can win the Republican nomination given his previous work as an influential DC lobbyist, here's his response: I'm not afraid to make them mad. 

At least that's what he indicated on Capitol Hill Tuesday, the featured speaker at a forum hosted by the Congressional Health Care Caucus. 

Barbour slipped in his defense while talking about his effort to promote the use of generic drugs in Mississippi's Medicaid program, an initiative he said increased usage rates from 46 percent to 78 percent over six years. 

"The patients were fine, everybody's happy, except my old clients," Barbour deadpanned to laughter, in what appeared to be an effort to deflate criticism of his K Street ties. "Back when I was around town more..some of our biggest clients were brand name pharmaceutical companies." 

At the health care forum, Barbour reiterated his support for Medicaid block grants, which is one of the proposals featured in Rep. Paul Ryan's FY 2012 budget. Emphasizing his belief that states can run Medicaid more efficiently than the federal government can, Barbour said Republican governors are currently working on a series of reforms to offer to Congress which would control costs in a way that would be consistent with Ryan's budget. 

Barbour predicted that "Obamacare" -- he said the term was more preferable to "Puh-pak-uh" (the acronym for the Patient Protection and Affordable Care Act) -- could only result in a "big fat tax increase" in Mississippi because it would add 400,000 new individuals to the state's Medicaid rolls, putting one in three residents on the Medicaid program. 

"You can save money on entitlements," Barbour said. "Medicaid is the biggest entitlement state governments deal with. You can save money on entitlements by running programs right." 

He is expected to announce a decision on whether to pursue a presidential bid by the end of this month, and peppered with questions about the 2012 race, Barbour demonstrated a preference for subtle digs over pointed attacks. 

Take for instance, Barbour's response to a question about Nick Ayers, his star aide when he chaired the Republican Governor's Association, who set political observers atwitter by agreeing to be Tim Pawlenty's campaign manager: Barbour downplayed the 28-year-old's political experience -- "He was Governor Perdue's campaign manager" -- while at the same time offering encouragement -- " But that's great, you know, good. He wanted to be the campaign manager of a big campaign, he's gonna get a chance, he's a good guy." 

And then there was the question about Mitt Romney , who announced Monday he had filed the FEC paperwork for a presidential exploratory committee. Asked to compare Romney's health care program in Massachusetts to the Affordable Care Act, Barbour first dodged the question and then drew a connection between the two without saying one overtly negative word about his former RGA colleague. 

"If Massachusetts wants it, more power to them. that's their business," Barbour said, adding, "It would not be a good system for us. We would not choose it in Mississippi, and frankly, we oppose the Obama administration trying to force it on us." 

Pressed further, Barbour said "the similarities are obvious" but declined to dig into his potential opponent, saying, "Mitt announced yesterday, I'm gonna let him have his day." 

Registering in the low digits in early primary polls, Barbour's visit to Capitol Hill , teeming with reporters, gave him a burst of national political coverage ahead of his trips to the early primary states of New Hampshire and South Carolina this week.
