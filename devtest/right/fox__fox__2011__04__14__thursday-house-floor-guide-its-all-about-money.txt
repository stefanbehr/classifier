The long-term spending bill, known as the Continuing Resolution, will be on the House floor Thursday for a final vote. 

This bill, agreed to late last week by both leaders in the House and Senate, cuts $38.5 billion and funds the federal government for the rest of the fiscal year. 

The Senate is expected to take up the CR later Thursday night. 

The House will also start consideration of House Budget Committee Chairman Paul Ryan's budget, along with several other alternative budgets. 

Here's how this works: 

Late Thursday morning, the House will begin debate on the CR. This will be straightforward. In early to mid-afternoon, sometime between 1-3 pm ET, the House will go to a straight vote on the CR. 

We expect a number of conservative Republicans to peel off here. House Speaker John Boehner , R-Ohio, lost 28 of his own members on the vote Saturday morning and 54 on the last big CR vote in March. 

The magic number is 217 these days to pass something in the House, because there are two vacancies. Former Reps. Chris Lee , R-N.Y., and Jane Harman, D-Calif., resigned their posts and Rep. Gabby Giffords, D-Ariz., is not voting. 

With 241 Republicans, Boehner can only lose 24 of his own without needing Democratic support. Conservatives are very skittish about voting for this bill after seeing one Congressional Budget Office metric that showed this CR only cuts $352 million. That's about what is needed to slash Planned Parenthood and $80 million less than is necessary to eliminate the Corporation for Public Broadcasting - two of the policy riders staunch conservatives were most attached to. 

The GOP is not out of the woods yet. This could be an "organic" vote on the House floor with more and more Republicans voting "no" on the bill once they see how their colleagues vote. 

Republicans insist they will be okay. But, this bill has some potential to meltdown on the floor like the first TARP vote in September 2008. 

What to watch for: 

See how many Republicans, especially those with Tea Party support, vote no. That number could be much higher than the zenith of 54 - climbing upwards of 70. 

That said Democrats aren't enamored with this bill either. Expect less than one-third of all Democrats to vote for this CR. 

Watch to see a split vote in the Democratic leadership. Minority Leader Nancy Pelosi , D-Calif., and Minority Whip Steny Hoyer, D-Md., still haven't announced how they will vote. 

Keep in mind, despite the cry from Republicans, Boehner will probably need Democratic support to pass this bill and keep the government running past 11:59:59 pm Friday. 

After that vote, there are two technical -- but important -- matters to move through.There will be two short debates of 20 minutes apiece on "Enrollment Corrections." The deal cut between the president, Boehner and Senate Majority Leader Harry Reid, D-Nev., called for straight Senate votes to defund health care for this year and cut out money for Planned Parenthood. But that process has to start in the House. 

So, member will first spend 20 minutes debating the health care provision and then 20 minutes on Planned Parenthood. 

After that, there will be two individual votes on both of those issues, prepping everything for Senate consideration. 

And still there's more! Next up is the fiscal year 2012 budget, crafted by Ryan. 

The House will not finish the budget debate Thursday. That will happen Friday. But as indicated earlier, they will debate a number of alternative budgets as well: 

-- One from the Congressional Black Caucus 

-- One from Blue Dog Rep. Jim Cooper , D-Tenn. 

-- One from the Progressive Caucus 

-- One from the Republican Study Committee 

-- And one from Rep. Chris Van Hollen, D-Md., the top Democrat on the Budget Committee 

Final vote on the budgets come Friday.
