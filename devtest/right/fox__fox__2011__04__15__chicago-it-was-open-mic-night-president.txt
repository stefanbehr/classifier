Although they were meant for a private audience, the White House says President Obama stands by some of the candid thoughts he shared - inadvertently with a large group - Thursday night. 

"That was meant to be a closed-press event. He was taking questions from supporters," White House Press Secretary Jay Carny told reporters aboard Air Force One . "But there's nothing -- nothing he said that contradicts anything he said in public." 

Mr. Obama was in Chicago for a series of fundraisers Thursday, and as is common for such events, the traveling press was brought into the room to hear the opening remarks and then escorted out so the president could speak more freely with his paying guests. But an open microphone at one of the fundraisers caught the president discussing the negotiations he had with Republicans on the 2011 budget - after the press had left the room. The mic picked up the president criticizing the GOP for their attempts to roll back portions of health care reform, as well as their efforts restrict Planned Parenthood from receiving federal funding 

"I remember at one point in the negotiations," Obama shared, with what he thought was a small audience, "One of Boehner's staff people pipes up and says, 'You don't understand Mr. President, you know, we've lost on you know, healthcare, we've lost on the EPA we've given that up, we've got to have something for our--to sell to our caucus.' And I said to them, I said let me tell you something - I spent a year and a half getting healthcare passed. I had to take that issue across the country and I paid significant political costs to get it done. The notion that I'm going to let you guys undo that in a 6 months spending bill, you want to repeal healthcare, go at it, we'll have that debate. But you won't be able to do that by nickel and dimming me in the budget. You think we're stupid?" 

The audio of Mr. Obama's remarks, in their entirety, was still being relayed back to the White House press room, and were picked up by CBS News' Mark Knoller . Carney says the mistake was merely a "miscommunication" and that the president is "not at all" embarrassed about anything he'd said. 

"It's not a problem, not an issue," said Carney. 

The president also had some tough words for House Budget Committee Chairman Rep Paul Ryan, R-Wis., whose 2012 budget blueprint just passed the House Friday. 

"When Paul Ryan says his priority is to make sure, you know, he's just being America's accountant, and you know trying to be responsible," the president pondered with his allegedly private audience, "This is the same guy who voted for two wars that were unpaid for, voted for the Bush tax cuts that were unpaid for, voted for the prescription drug bill that cost as much as my healthcare bill, but wasn't paid for." 

Carney defended the remarks. "I think what he said in that session you're talking about and the things he's said in more public forums have been entirely consistent. And you can't in one breath criticize him for being pointed in his comments about the House Republican budget plan in public and then say, my gosh, he was pointed and so different in private, because he is making clear that the visions are quite different." 

Congressman Ryan did not respond directly to the president's critical comments, but late last night on the House floor, Ryan expressed the concern that leaders here in Washington are more worried about the next election than the next generation, and called for making tough decisions today. 

"I hope that leaders in this town change their tune so that we can fix this problem, but it's going to require them to change their tune. We don't need good politicians, we don't need clever politics- we need real leadership and real solutions to fix this country's problems," said Ryan 

Mike Emanuel contributed to this report
