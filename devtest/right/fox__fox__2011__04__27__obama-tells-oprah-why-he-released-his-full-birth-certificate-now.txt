In an interview with Oprah Winfrey set to air Monday, May 2nd, the daytime host asked President Obama why he released his birth certificate now. The president explained that in these serious times, "we can't be distracted by sideshows". 

The President told the TV host that while he was dealing with the budget debate, "the biggest news was this birth certificate thing." 

And so he was prompted to release the long form of his birth certificate, to put the "silliness" to rest. President Obama released a "certificate of live birth" during his 2008 race for presidency. But the so-called "birther" movement developed when critics of the now-president questioned his citizenship. 

Most recently, potential 2012 challenger Donald Trump raised the issue in media interviews calling for the President to come forth with the full document. 

Mr. Obama had to get Hawaii to waive a law that prevents the long form birth certificate from being photocopied or released to anyone, including himself. 

Today he told Oprah, "And so at that point I said to my team, look, even though this is not usually what the State of Hawaii does, even though the Republican Governor of Hawaii, the Democratic Governors of Hawaii, all the various officials had confirmed that I was born here, let's ask them for a special dispensation where they will go ahead and provide us with the original to see if we can put this to rest." 

The president also shared a laugh with his friend Oprah joking, "Can I just say? I was there, so I knew that I had been born."
