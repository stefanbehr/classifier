"Representing the People, Not the Powerful." Sounds sort of like a campaign slogan, but it's not. It's the motto of Morgan Morgan, the personal injury law firm that now employs former Florida Governor Charlie Crist . 

"If you need help sorting out your legal issues as the result of an accident or insurance dispute, visit me," the former governor says in a new TV advertisement that identifies him on-screen as "Attorney Charlie Crist ." 

The firm has offices throughout Florida, in addition to locations in Mississippi, Georgia, and Tennessee. Their website's homepage is currently displaying a large photo of Crist standing beside his new boss, John Morgan. 

"It's really such a natural transition for me; to be with Morgan Morgan and work with my dear friend John Morgan," Crist explains on the site. "He really is all about the people. That's what I've always tried to do and aspire to do as a public servant. Now I'll do it in the private sector." 

At least one Floridian has posted Crist's commercial to Youtube, and it marks the former governor's second noteworthy appearance on the site in the last month. In April, Crist posted a video apology on the site , directed at singer David Byrne from the Talking Heads. The apology was in response to a $1 million lawsuit Byrne filed against Crist, for using his music in a campaign ad without permission. 

CLICK HERE TO SEE CRIST'S NEW TV AD
