A bipartisan group of lawmakers travels down Pennsylvania Ave for a 10:00 a.m. ET meeting on deficit reduction with Vice President Biden. Capitol Hill conservatives have called for sharp reductions in spending as a condition for raising the nation's debt limit. On the other side of the ledger, Senate Finance Chairman Max Baucus , D-Mont., one of the meeting's participants, appeared open to the idea of a "trigger" that would cut spending if the government exceeded preset limits at a committee hearing Wednesday. However, Democrats have repeatedly said that deficit reduction cannot occur through spending cuts alone. 

The death of Usama bin Laden at a compound 30 miles from the capital of Pakistan has raised concerns over how much the government there did or did not know about the terror mastermind's location. A Senate Foreign Relations Committee hearing at 10:00 a.m. ET seeks to evaluate the current U.S. policy towards the Islamic republic. 

Whether the Pakistani government knew of bin Laden's home address or not, one of the principal reasons for the war in Afghanistan is now gone. Accordingly, a bipartisan collection of lawmakers plans to introduce legislation to speed the military withdrawal from the south-central Asian nation at a 9:45 a.m. ET press conference. 

House Republicans plan to unveil a series of energy bills in the coming weeks that they say will help ease Americans' pain at the pump. On the floor Thursday will be a bill to ease limits on offshore drilling permits. 

We'll be covering all these stories and more, so stay with Fox News for all the latest.
