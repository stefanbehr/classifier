The first GOP Presidential Primary debate gave South Carolina Republican voters a good taste of what's to come in the months ahead. Minutes after the debate ended, South Carolina Governor Nikki Haley told reporters in the Spin Room, "It's 'go time' in South Carolina. It's a touch of what we're going to see. We are getting excited. " 

Gov. Haley says the big issue facing South Carolina, a right to work state, is simple: labor unions. Haley was glad to hear the candidates discuss the issue during the debate. 

"I appreciate they are talking about unions, it affects every state," she said. "I will fight the unions every step of the way and I want to hear what every candidate has to say on the issue." 

Out of the field of candidates exploring a run for President, Gov. Haley says she's met personally with several, including Tim Pawlenty , Rick Santorum and Michele Bachmann . 

"Anyone who comes to SC, we will welcome them and meet with them." 

Haley had this piece of advice for Republicans campaigning across the Palmetto state, "You have to hit every corner of our state. It's not about hiring consultants." 

When faced with the question of which candidate won tonight's debate, Gov. Haley played it safe, telling reporters, "there's no winner tonight. It's too soon. The dinner looks great, but we got to get to what it tastes like."
