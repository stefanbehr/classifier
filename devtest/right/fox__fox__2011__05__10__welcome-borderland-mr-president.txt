El Paso, Texas -- President Obama is heading to El Paso, Texas Tuesday afternoon to address the nation on, what the White House is calling, America's broken immigration system. 

Fixing immigration is an issue Obama has addressed since his 2008 campaign in speeches and the like, but so far has been unable to push any legislation through Congress. With the race for 2012 just around the corner, Obama and the White House say immigration "remains a priority." Tuesday Obama will speak at Chamizal National Park, the first president to do so since Lyndon Johnson, standing just yards from Juarez, Mexico along the border fence. 

White House Press Secretary Jay Carney gave reporters a small glimpse Monday of what the president will be touching on, saying Obama is committed to comprehensive reform. "We weren't able to achieve it in the first part of this term - the president's term, but it remains a priority of the president." 

Of top concern is border security, among the points Obama is expected to hit on during his visit. There are now twice as many border agents than there were in 2004. Rail shipments traveling across the border are screened to make sure money, drugs, and weapons are not being smuggled. But as wars between competing cartels rage in Mexico - frequently catching Americans in its crosshairs - border security still needs to be addressed. 

Obama is also expected to touch on economic issues surrounding the shared border. Monday Carney referenced a statement by New York's Mayor Michael Bloomberg that 25 percent of all high-tech startups in the United States are founded by immigrants, which has led to 450,000 jobs. 

The White House said that in order to get immigration reform achieved, it's going to require bipartisan support. "The president's speech tomorrow will be about trying to continue to build public support," said Carney. 

El Pasoans definitely have opinions on how Obama has handled immigration so far. Some tell FOX News they are happy with how Obama has handled immigration since taking office and think nothing should change. Others thought he would have accomplished more by now. And one man, a Mexico City native, who immigrated to El Paso 24 years ago, said he's not impressed and will not vote for Obama again in 2012. 

While most in the area agree that hardworking immigrants who contribute meaningfully to the U.S. deserve some rights, there were mixed feelings on issues like public education for the children of illegal immigrants. One father of four said it's frustrating to see his tax dollars go to children who are here illegally, but local university students said an education is so valuable that everyone should be able to go to school even if they're not paying into the system. 

An argument by Republicans calling out Obama for supporting a provision that could have cut temporary legal workers while in the Senate is gaining traction ahead of Tuesday's visit in the blogosphere. But the same blogs also say Obama voted for other parts of a bipartisan plan put forward by Sens. John McCain and Ted Kennedy in 2007, which ultimately fell short of the votes needed to pass.
