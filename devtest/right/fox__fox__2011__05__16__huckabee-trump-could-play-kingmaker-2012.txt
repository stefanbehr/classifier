As the list of official Republican presidential contenders grows, so does the list of influential party kingmakers. Criticism that the GOP field is lackluster means the likes of Mike Huckabee and Donald Trump could play an important role in helping voters decide who to back come primary season. 



Gov. Mike Huckabee 

The former Arkansas governor and 2008 presidential candidate could be one of the hottest endorsements of the 2012 season. Huckabee is a great Republican story and is one that a few of the current contenders wouldn't mind emulating. The Baptist minister was virtually unknown when he broke out on the scene during the last presidential election, even though many insiders thought he was a viable contender. Less than a year after announcing his candidacy, Huckabee was consistently polling near the top of national polls and then won the Iowa Republican Caucuses. His evangelical Christian background was a strong selling point with the Deep South and social conservatives around the country. 

Four years later, the Fox News contributor's name is one of his strongest selling points, and the fundraising trouble that contributed to Huckabee's failed bid in '08 is no longer an issue. Still, Fox News Senior Political Analyst Brit Hume thinks Huckabee's faith could be the biggest help if he decides to endorse someone. "He could help a candidate get a hearing from Christian conservatives in Iowa," says Hume. Larry Sabato of the University of Virginia agrees. "This is an endorsement worth having," he says. "Not only was he a co-frontrunner in the polls, but he resonates with social conservatives who are critical to success in early states." 



Gov. Chris Christie 

Current New Jersey Gov. Chris Christie has already met with a few Republican hopefuls and Sabato crowns this endorsement the most valuable to have. The no-nonsense, confrontational Christie has become a darling of the GOP for standing up to the state's teachers unions and for slashing spending in a traditionally blue state. Christie has repeatedly said he's not running for president, yet many Republicans still cry out for him to jump in the race. And while Christie recognizes that "short of suicide" there's no way he can stop people from speculating on his candidacy, he's all but assured to finish out his current obligation. 

"A governor can always help in his home state," Hume explains. That's the draw of a Christie endorsement. If he can be the first Republican to win statewide in the last 12 years, maybe New Jersey - and the Northeast - could be in play for the GOP come 2012. At the very least, a Christie endorsement would be a sign to the party faithful that his choice is a straight-shootin' budget hawk. "I wouldn't be surprised if Christie made the case for his choice better than the candidate had been able to make it," Sabato warns. 

Along the same line as Christie, current governors Nikki Haley of South Carolina and Rick Perry of Texas offer some of the same benefits. "Endorsements have little history of moving voters, but a politician with a political organization can certainly help a candidate," says Hume. "Nikki Haley, for instance, could be a big help to someone running in the South Carolina primary. Her organization can turn out voters." Sabato says Haley's importance can be summed up in two words. "South Carolina, South Carolina, South Carolina. No Republican has won the nomination without first winning the Palmetto State in recent decades. Haley will have great influence here." 



Gov. Haley Barbour 

There were many things Mississippi Gov. Haley Barbour considered before officially deciding against a presidential run. One of those things is the perception that he would have been the ultimate insider. The 2010 election was widely considered to be an election against establishment candidates, and it was going to be pretty tough for the two-term governor, former head of the Republican National Committee and Washington lobbyist to paint himself any differently. But therein lays one of greatest assets to a Republican candidate. "Barbour has a voluminous rolodex so he could help with fundraising," says Hume. "And he would be wise counsel." 

But Hume points out that unlike South Carolina, Mississippi is not an important primary state. Neither is it likely to be seriously contested in the general election. Many Republican hopefuls probably would like to have Barbour's help, but how publically remains to be seen. 



Donald Trump 

Few potential candidates are able to gin up media attention like Donald Trump. "Trump would attract a forest of cameras so the endorsee could make his or her case," Sabato says. The famous entrepreneur was at the top of his polling game a few weeks ago as he consistently questioned whether or not President Obama was actually born in Hawaii. After Obama largely squashed that rumor, Trump began to fall in the polls and out of favor with voters. There's always the risk of controversy with Trump next to you. 

"Trump will probably be no help, unless he gives money," Hume says. But Obama's camp has already said he hopes to raise $1 billion this election cycle, so money might end up being a pretty important thing. 



Gov. Sarah Palin 

Sarah Palin has yet to decide whether she'll jump into the race for the White House . The former Alaska governor and Tea Party favorite has one of the most recognizable names in American politics, so it could be awhile before we know her decision. If she decides not to give it a go, you can bet there will be a clamoring for her endorsement. "If Sarah Palin is willing to campaign for a candidate, she could turn out crowds nearly anywhere," Hume predicts. 

Her star status could rake in the money during the primary season and make a huge difference to lesser known candidates. But voters have a love-hate relationship with Palin. "Her endorsement cuts both ways," Sabato warns, "though with Republicans, it would be mainly positive." 



Tune into Special Report w/Bret Baier Monday night at 6p ET to hear more of Brit Hume's thoughts on the 2012 race. If you miss it, check out the show's homepage for the video.
