The White House provided this statement from spokesman Nicholas Shapiro: 

"The President called Missouri Governor Jay Nixon to personally extend his condolences and to tell all of the families of Joplin affected by the severe tornadoes that they are in his thoughts and prayers. The President assured the governor that FEMA will remain in close contact and coordination with state and local officials. 

"The President has directed FEMA Administrator Craig Fugate to travel to Missouri to ensure the state has all the support it needs. In addition, in anticipation of requests for assistance, a FEMA Incident Management Assistance Team (IMAT) is en route to Joplin. This self-sustaining team will work with FEMA officials already in Missouri to coordinate with state and local officials to identify needs and any shortfalls impacting disaster response and recovery. 

"The Federal government will continue to support our fellow Americans during this difficult time." 



President Obama arrived in Dublin, Ireland Monday kicking off his six-country swing through Europe but is getting briefed on the deadly tornado damage in Missouri. 

An administration official tells Fox that the president received regular updates on the damage while on Air Force One . Obama has directed his staff to keep him posted on the latest news from the disaster zone and to coordinate with state and local officials for their assistance needs. 

One of the updates came while Obama, travelling with the first lady, was having some down time at his hotel following meetings with Irish President Mary McAleese and Taoiseach Enda Kenny. 

Earlier, the president released a statement and said, "Michelle and I send our deepest condolences to the families of all those who lost their lives in the tornadoes and severe weather that struck Joplin, Missouri as well as communities across the Midwest today." 

Authorities in Joplin, Missouri say that more than 80 people were killed and destruction hit almost six miles long and half-mile wide, destroying much of the city. 

Fox Business Network's Rich Edson contributed to this report.
