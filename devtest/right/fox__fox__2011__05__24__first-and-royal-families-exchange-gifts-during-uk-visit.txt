The president and first lady exchanged a series of gifts with Queen Elizabeth II and Prince Philip as the royal family welcomed the Obamas at the beginning of their visit to London. 

The Queen gave the Obamas copies of Royal archive letters between the royal family and U.S. presidents ranging from John Quincy Adams to William McKinley. Included were letters between Queen Victoria and Mary Todd Lincoln marking the assassination of Abraham Lincoln. All of the letters were bound in a red leather book and placed in a presentation box and the first lady was given a gold and coral broach in a red leather jewel box. 

The Obamas gave Queen Elizabeth II a leather-bound book featuring memorabilia and photos from a 1939 trip visit her parents, King George VI and Queen Elizabeth, made to the U.S. in 1939. That trip was the first to the U.S. by a reigning British monarch. 

For Prince Philip, a carriage racing connoisseur, the Obamas brought a set of Fell Pony bits and shanks emblazoned with the presidential seal, and horseshoes worn by a Jamaican carriage champion horse. The Obamas gave Prince Charles and Camilla, the Duchess of Cornwall, a collection of seedlings and seeds from Mount Vernon, George Washington's Virginia home. The seeds were presented in a wooden box made from a magnolia tree that fell on the White House lawn during a 2009 snow storm. 

It has been tradition to exchange gifts with foreign heads of state. The president and frst lady accept gifts, but most gifts are later turned over to the archives since ethics rules preclude the First Family from keeping them.
