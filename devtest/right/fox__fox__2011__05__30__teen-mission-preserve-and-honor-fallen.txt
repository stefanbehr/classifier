Quiet, soft-spoken 17- year-old Ricky Gilleland spends most weekends surrounded by tombstones, as he walks through Arlington National Cemetery just outside Washington, D.C. looking for the burial sites of those individuals who have died in the line of duty since September 11, 2001. Gilleland has taken on the job that the historic cemetery has not been able to do itself. 

Through his website, preserveandhonor.com , Gilleland has cataloged the thousands who are laid to rest in Section 60 of Arlington Cemetery. With a camera in hand, Gilleland shoots a photo of both the front and back of the headstone, "to provide a virtual place for loved ones and friends to both locate the graves of the fallen and reflect on the memory of their sacrifice." 

Gilleland explains it's a way for those who cannot return to the cemetery to see a loved one's final resting place. "They can go back (on the website) and they can find their loved one...and find a little more comfort," he told Fox News on a recent Sunday afternoon. 

The Stafford, Va. teenager estimates he's spent over 400 hours during the last year cataloging almost a thousand names. It's a project he decided to undertake last June after learning Arlington National Cemetery had mismanaged many sites. It was discovered that one service member had been buried on top of an existing grave, and other graves had been mismarked. These blunders cost two top administrators their jobs. The scandal tarnished the reputation of the cemetery. 

Gilleland decided there had to be a better way. "I figured this day and age there should be an electronic record of all of this information." So with $200 of his own money, he decided to create a website. 

Coming from a military family dating back to his great-great-great grandfather who fought in the battle of Gettysburg during the Civil War, and with two brothers currently serving in the U.S. Air Force, Gilleland says the graves represent something personal for him. 

But it's his efforts that represent something very important for others. Paula Davis and Xiomara Mena sit side by side in Section 60 on the weekends. Their sons' respective graves are just a few feet apart. 

Davis' son, Private First Class Justin Ray Davis, was killed in Afghanistan June 6, 2006. She wears her only child's dog tags around her neck and understands the need for families to stay connected to the memories of those they've lost. "It just makes a big difference when you can't be here to see a physical picture," Davis explains. 

Having had other families ask her to take photos of a loved one's grave, Davis says it means a lot that Gilleland has taken on this "huge responsibility." 

She's heard about the young man with the camera before. 

"I was hoping one day, I'd run into you to thank you," she tells him, jumping up to give him a hug. 

The cemetery scandal last year weighs on her mind also. "I'm just glad that someone is making sure that forever, 100 years from now, someone will be able to see where the tombstone is and who's in that place," Davis says. 

Gilleland is a high school junior and hopes to enter the U.S. Naval Academy when he graduates. Documenting gravesites has given him a unique understanding of the perils he might encounter. "A lot of the people buried here, unfortunately, are very young, not much older than me," he says. But serving his country is too important. 

To search for a loved one or to learn more about Project Preserve and Honor, please go to preserveandhonor.com. Jennifer Griffin will have more on Ricky and his efforts Memorial Day on Special Report w/ Bret Baier at 6p ET.
