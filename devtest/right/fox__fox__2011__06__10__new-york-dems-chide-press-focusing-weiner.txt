Representative Charles Rangel did not call on fellow New York Democrat Anthony Weiner to resign like a growing number of others in the party. 

Instead, at a press conference today on the steps of City Hall in New York, he saved his criticism for the media, stating the "whole idea of resignation is something hyped up by the press more than [Weiner's] ability to serve," continuing on to say Rep. Weiner is smart and respected and could still be an effective Congressman only if "the press gets off his back." 

Rangel said he has a hard time passing judgment on Representative Weiner due to the "high tech" nature of the scandal, involving sexually explicit pictures and messages sent over Twitter, Facebook, and text messages to six women online. 

"I don't even understand that type of stuff to be honest with you, but he apologizes for it, so I, like everyone else, have to assume it's not the proper conduct, but I don't know what he did, who he offended." 

Rep. Rangel continued, "He wasn't going out with prostitutes, he wasn't going out with little boys, he wasn't going into men's rooms with broad stances, I mean, all of those things I understand, I may be 80-years-old, but high tech stuff like this I can't respond." 

At least 12 Democratic members have publicly condemned Rep. Weiner and have urged him to step down. But Rep. Rangel, who is no stranger to scandal having been found guilty on a series of ethics violations and failure to comply with tax laws, pointed to other offending members and questioned why Weiner was being singled out. 



"Certainly I know immoral sex when I hear it from other members and no one has screamed for their resignation, so I don't know why they're selecting Anthony." 

As to the fate of Anthony Weiner's political career and the impact his scandal could have on the Democratic party as a whole, Rep. Rangel said it depends on whether or not the media will forgive and forget. 

"If indeed members -- and especially Democratic members -- every time they go out to campaign... people like you come asking about Anthony Weiner , it becomes a very difficult thing and quite frankly while they may love Anthony Weiner, it's abundantly clear that they love themselves politically a lot better." 

New York State Assembly Representative Sheldon Silver and U.S. Representative Carolyn Maloney, both Democrats, kept comments about their fellow New Yorker to a minimum, but agreed with Rangel that his future should be decided by his constituents. 

Assemblyman Silver chided the press for derailing a press conference held to honor prominent Iranian activists who have dedicated years to efforts bringing basic human rights to Iran . 

"Iran, nuclear weapons, people here who are heroes -- that should be the story that you are really writing about today, people who are defying that regime and being real heroes, that's the real story today," he said.
