"This is going to kill the ability for businesses to create jobs and to promote ... economic recovery throughout our country." That's what Republican South Carolina Attorney General Alan Wilson says will happen if the National Labor Relations Board wins its fight to force Boeing to build a new plant in Washington state instead of the Palmetto state. 

The NRLB argues in a complaint filed in Seattle April 20 that Boeing's decision to build the plant in South Carolina is retaliation for a worker strike in the company's home state of Washington, but Wilson says that's not the case. 

"Since the announcement that Boeing was going to open a plant in Charleston, South Carolina, Boeing has actually created 2,000 new jobs in Washington state. So it's hard to say you are retaliating against the union when you create 2,000 members to their role," said Wilson on Fox News. 

Attorneys general from 15 other states have added their names to a counter-suit filed against the NRLB, saying a ruling in the labor board's favor could hurt their states as well. 

"I am afraid that companies like Boeing and smaller [firms] are not only going to be afraid to go to union states, they're going to be afraid to stay in the United States and they are going to go abroad. That's my greatest concern," Wilson said. 

When asked what he thought would happen with this case in the end, Wilson said, "NLRB loses, [and] South Carolina, Boeing and people who favor job creation will win."
