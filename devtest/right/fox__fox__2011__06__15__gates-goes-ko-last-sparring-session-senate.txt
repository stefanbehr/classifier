There was a tense exchange Wednesday between Sen. Patrick Leahy , D-Vt., and outgoing Defense Secretary Robert Gates during a Senate hearing to discuss the defense budget. 

Referencing reports Pakistan arrested CIA informants that helped lead to the death of Usama bin Laden, Leahy asked Gates how long the U.S. will be willing to "support governments that lie to us?" 

GATES : Well, first of all, I would say, based on 27 years in CIA and four and a half years in this job, most governments lie to each other. That's the way business gets done. 

LEAHY : Do they also arrest the people that help us when they say they're allies? 

GATES : Sometimes. 

LEAHY : Not often. 

GATES : And -- and sometimes they send people to spy on us, and they're our close allies. So... 

LEAHY : And we give aid to them. 

GATES : ... that's the real world that we deal with. 

Pakistani sources confirmed to Fox News that five CIA informants who helped with the Bin Laden raid have been arrested in Pakistan in the last four weeks, including a doctor in the Pakistani army who provided the CIA with license plate numbers of those vehicles entering the Bin Laden compound. 

These new revelations, first reported by The New York Times, have led to increased calls for Washington to cut the billions of dollars in counterterrorism funding it provides Pakistan each year. 

Senator Lindsey Graham, a member of the Senate Armed Services Committee, suggested to reporters Wednesday the Pakistanis did far worse than to just arrest the Pakistani raid informants. 

"They did more than arrest them, but I'm sure that'd come out later" 

"Tell us more," a reporter shouted. "I can't," Graham responded. 

In addition to expressing frustration with the Pakistanis, Sen. Leahy pushed Gates on Afghan President Hamid Karzai , who he said "can't seem to make up his mind if he's on our side or the Taliban ." 

In his final testimony to the Senate before retiring at the end of the month, Gates began to raise his voice and speak with more emotion about the importance of a stable Afghanistan -- and then took another swipe at Leahy. 

GATES : First of all, I think the prospects of having a more stable Afghanistan in terms of a country that can defend itself -- I'm not talking about a Vermont democracy here, but a country that can defend itself. 

LEAHY : Neither am I, Mr. Secretary, and you know that. 

GATES : I know. But what I'm talking about is we are not in the business of nation building. What we are trying to do is build the Afghan national security forces to the point where they have the ability to defend that country and so that the Taliban and Al Qaida cannot reconstitute themselves in that country. 

And I think we are making considerable headway in that respect. So I think that -- I know people are frustrated. The country's been at war for 10 years. I know people are tired. But people also have to think in terms of stability and in terms of the potential for reconstitution. What's the cost of failure?
