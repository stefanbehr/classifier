As a former House speaker, college professor and now Republican presidential candidate, Newt Gingrich is eager to talk about "big issues." But after a barrage of stories about dispirited staffers leaving the campaign en masse, and another line of credit at Tiffany's , this time for up to $1 million, policy discussions have taken a back seat to questions on campaign strategy. 

"I'm not running to talk about the nuances of campaigns," Gingrich said during a morning speech at the Atlanta Press Club. 

During the wide-ranging speech Gingrich called for the repeal of the Dodd-Frank bill, with its sweeping regulations on Wall Street, and for a full scale audit of the Federal Reserve. But at a post-event news conference reporters were more interested in pressing the former speaker on the recent campaign staff departures in the context of his ability to lead. 

"Philosophically, in a way that many of you who've known me for a long time understand, I am very different from normal politicians," Gingrich said. "And normal consultants found that very hard to deal with." 

Gingrich also took the opportunity to lash out at some of his former campaign advisers , calling them "so-called professional consultants who have deliberately and maliciously kept feeding things to the press to create stories... I assume that is because they left saying I couldn't possibly win and they want to try to make sure that's true." 

The Gingrich campaign is having difficulties raising money, to the extent that two top fundraisers stepped down on Tuesday, but the candidate insists he will go forward with a grassroots campaign, based on "substantive issues" and "inexpensive" by presidential campaign standards. 

The former speaker compared the rocky start of his campaign to the early days of Ronald Reagan's initial bid for president. He predicted that in the months ahead, the national media would ask not what went wrong with the campaign but, "How did you survive?"
