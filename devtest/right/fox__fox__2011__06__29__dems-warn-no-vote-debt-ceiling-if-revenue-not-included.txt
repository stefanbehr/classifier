Congressional Democrats have been calling for revenue-increasing measures to be included in any final deficit reduction package negotiated with Republicans and the White House , but on Wednesday Senate Democrats ratcheted up the pressure. 

When asked if they were prepared to vote against any measure that does not contain provisions that bring more revenue into the federal government for deficit reduction, like closing tax loopholes and eliminating subsidies for certain industries, Sen. Bob Menendez , D-N.J., warned, "There are those of us who will have difficulty in supporting a debt ceiling increase without revenue, and when I talk about revenue - ending these tax loopholes and tax breaks that companies in America are getting at the expense of the American taxpayer." 

Sen. Chuck Schumer , D-N.Y., went even further. The number three Democrat, in reacting to a question about an insistence by House Speaker John Boehner , R-Ohio, that the votes do not exist in his conference for any package with tax increases, blasted, "He doesn't have the votes if he doesn't have revenues, because he's not going to get Democratic votes without revenues and he doesn't have enough Republican votes to pass it." 

Armed with a recent vote, supported by 34 Republicans, including GOP Leader Mitch McConnell of Kentucky, ending ethanol-blending subsidies and using the revenue for deficit reduction, Democrats said this should open the door to a compromise "to ensure that the sacrifice is shared." 

Republicans balked. 

McConnell has said that these matters should be dealt with later in the context of overall tax code reform, a problem far too complex to solve in the ongoing debt talks. 

A senior Senate GOP leadership aide also scoffed at the amount on which Democrats are focused, noting that ending ethanol subsidies only brings in roughly $2 billion annually. Ending taxpayer subsidies for corporations using commercial jets, another Democratic target, would bring in another $3 billion over the next decade. 

Schumer refused to say how much revenue Democrats are seeking in closed-door talks with the president. 

Lawmakers are fast-approaching the August 2 deadline after which the U.S. Is expected to start defaulting on its debt, according to the Treasury Department. 

It is unclear if an agreement can be reached in time, as any agreement will need to be written into legislative language and analyzed and scored by the nonpartisan number-crunchers at the Congressional Budget Office.
