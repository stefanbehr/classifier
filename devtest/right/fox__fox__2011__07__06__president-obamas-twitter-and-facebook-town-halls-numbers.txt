Even though Wednesday's Twitter town hall was about the same length as the Facebook one back April (approximately 68 minutes and 70 minutes, respectively), there were several differences in the two social media events. 



The Twitter town hall was held at the White House in the East Room and President Obama responded to 22 tweets and posted one himself. Most tweet comments and questions came from average Twitter users, but also included one from House Speaker John Boehner and New York Times columnist Nick Kristoff. 

In contrast, the Facebook one was held at the company's headquarters in Palo Alto, California consisted of a lot less - just eight total questions - with four from Facebook employees in the audience and four from Facebook users. The president himself never used a computer or posted a status update or message on the site. 

In the Twitter town hall, the messages were displayed on a large television screen on the stage, and in the Facebook one, the questions were just read out loud. 

Both town halls were moderated by the company's executives, Facebook CEO Mark Zuckerberg and Twitter co-founder and Executive Chairman Jack Dorsey. 

Methods of choosing the questions varied as well. Twitter partnered with Mass Relevance to "curate, visiual, and integrate conversations" according to a White House release. 

"Algorithms behind Twitter search will identify the Tweets that are most engaged via Retweets, Favorites and Replies," the release also stated. Twitter also used a team of eight regional users, made up of mostly local journalists, who helped to pick questions from their communities by gauging retweets. 

By noon, there were more than 60,000 tweets about the event with the hashtag #AskObama. 

The total number of Facebook questions were never released, but they did give some guidance on selection. 

"The Facebook employee questions from the audience were submitted to an internal company tool that enables other employees vote questions up. Relevant questions with lots of votes were chosen. The online questions were reviewed by a team at Facebook. Individual online questions were chosen to represent the most common relevant questions received. None of these questions were shared with the White House prior to the event," Facebook told Fox News back in April. 

Wardrobe was also different in the town halls. In a funnier moment in the Facebook one, the president joked "My name is Barack Obama, and I'm the guy who got Mark to wear a jacket and tie." He then prompted the two to take off their jackets. During Twitter's town hall, both Obama and Dorsey kept their jackets on.
