The White House released its annual salary report to Congress and like anything in Washington, it depends on who you ask if they went up too much, or are an adequate reflection of the tough economic times and have moved down. 

The salaries, which can seen here show that about a third of the employees make more than $100,000 per year and the lowest earn $41,000, except for three people who are working for no compensation, or $0 annual salary. Twenty-one employees made the maximum $172,000. 

The White House backs the figures, saying that salaries went down an average of $150 per person and that total salary spending decreased in part due to the total number of staffers going down as well. 

"The President Obama is deeply committed to continuing to reduce costs in government," said White House Spokesman Eric Schultz. 

However, some critics say they are spending too much, like Rep. Louie Gohmert, R-Texas. 

"[I]n the White House , in looking at it, this administration's got over 450 employees. Now, under the Bush administration, there were over 100, about a fourth of the employees, made less than $40,000," he told Fox Business on Tuesday. 

Fox News fact-checked, and the congressman's statements do pan out, with 102 of the 447 employees on the 2008 list having salaries of less than $40,000. 

"I guess, you know, there's so much greatness when you associate with this White House you deserve to be paid more, I don't know," he said. 

Gohmert added another sarcastic jab, "Don't forget the 34 -- the 34 czars that are out there dictating policy and let's face it...when you're a dictator you need to be paid more." 

As the economy faltered, President Obama enacted a pay freeze earlier in his administration for top wage-earners. 

Wednesday at a Twitter town hall, he referenced the freeze. 

"So they haven't had a raise in two and a half years, and that's appropriate, because a lot of ordinary folks out there haven't, either. In fact, they've seen their pay cut in some cases," Obama said. 

An analysis by the gossip website Gawker, that was widely circulated and posted on the internet, compared the salary increases to those of what staffers got last year. The site found that 75 percent of staffers who stayed on got raises from 2009 to 2010. 



And this year, the figure isn't quite as big - but of 270 staffers who have been at the White House for more than a year, more than 50 percent got raises with an average increase of 8 percent. 

Fox double-checked Gawker's claim on how many got raises and found 267 staffers on both lists, indicating they had worked for more than one year. Of those staffers, 144 had received a raise in 2011 (54 percent). 



It's worth noting that some of those raises were for promotions, not just for the regular yearly increases. 

"To be clear, in the past year, the average salary of a White House employee went down, the total number of White House staffers went down, and the total amount spent on White House salaries went down. If pay increases were issued, they were given for a variety of reasons, ranging from promotions to additional work responsibilities," Schultz said. 

Most employee survey data, like these by The Conference Project projected about 3 percent raises on average for employees nationwide this year. 

The White House , is of course a different entity than the private sector so it's hard to exactly do an apples to apples comparison.
