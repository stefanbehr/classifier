A retreat to Camp David famously helped forge an agreement between Israel and Egypt in 1978, but as debt limit negotiations escalate to a higher pitch with each passing day, leaders from both sides of the House are snubbing the storied presidential woodland retreat. 

"The only thing I hope he doesn't ask us to do is go to Camp David," House Minority Leader Nancy Pelosi , D-Calif., said in a press conference Thursday. "That goes beyond the pale. Driving down the street for these meetings is one thing." 

Daily debt meetings have brought congressional leaders to the White House , and with negotiations stalling, rumors surfaced late Wednesday night that President Obama was mulling over inviting congressional leaders to Camp David this weekend in hopes of fleshing out an agreement. 

The White House would not confirm any plans for a bipartisan pow-wow at the presidential retreat, located about 60 miles northwest of Washington in Frederick County, Maryland. Though President Obama has spent much less time at the retreat than his predecessor, he floated the idea of a congressional summit at the Camp during the contentious lame duck session of Congress in late 2010. An invitation there traditionally has been considered an honor. 

Delaware Democratic Sen. Chris Coons warned Thursday that defaulting could put U.S. "global leadership," at "real risk," but Pelosi implied debt dealings don't rise to the level of Camp David-style international peace talks. 

"I want that to be his preserve, a place a president can go to renew, to study, to prepare for the next week," Pelosi explained. "I want it to be a place where the president takes heads of state to close out all other concerns, and stay focused on resolving a global problem." 

"I don't want it to be a place where the president has to continue to listen to some of this stuff," she said of ongoing negotiations. 

Asked if she wouldn't like the chance to have s'mores at Camp David over the weekend with her fellow lawmakers, Pelosi laughed, "I have five children, nine grandchildren. S'mores are big for us - I'd rather have them at home." 

On a more serious note Pelosi added, "Let's not say, well, we don't have to make any decisions today, on Thursday or Friday, because we're going to have s'mores at Camp David over the weekend." 

That sentiment echoes Speaker of the House John Boehner's,R-Ohio, apparent view on forging a lasting debt limit peace at the Camp. "The White House discussed with the Speaker the possibility of going to Camp David this weekend. And in response, the Speaker said he sees no need for that," Brendan Buck, spokesman for the Speaker, said Wednesday night. 

Pelosi had an explanation for the bipartisan distaste for sharing gooey marshmallow treats in the woods. "He probably likes s'mores at home too," she said of Boehner.
