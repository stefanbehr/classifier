Like the phoenix, the most misunderstood, convoluted Congressional procedure of all time appears to be rising form the ashes of the debt ceiling debate. 

You may remember "reconciliation" as the vehicle used to finally approve the health care reform package last year. 

Well, now Rep. Rob Andrews, D-N.J., indicates that reconciliation, which curbs debate in the Senate and blows out any chance of a filibuster, may be the only route to passing a final debt ceiling package. 

"Every piece of major economic legislation since the 1970s has been by reconciliation," Andrews said. "You can get 40 senators to block anything." 

Senate Majority Leader Harry Reid , D-Nev., has been insistent that the process on a final debt deal originate in the House. 

Article 1, Section 7 of the Constitution requires that all bills that raise revenue--or have tax implications--start in the House of Representatives. That means that any sort of package must initiate here. 

Democrats used reconciliation to pass the final version of the health care bill last year as it became clear that Republicans had the votes to filibuster the package after the upset election of Sen. Scott Brown , R-Mass. 

Using reconciliation in the Senate limits the chance for defeat by short-circuiting the filibuster option and limiting debate. 

Andrews predicts the following: 

The president and the Congressional leadership will eventually assemble an "interim" 3-part bill that raises the debt ceiling, makes some sort of an enforceable down payment on spending cuts and creates a reconciliation option to successfully navigate the rocky Senate shoals. 

It will be interesting to watch whether House Republicans sign off on this route because they fought reconciliation so ardently last year on health care.
