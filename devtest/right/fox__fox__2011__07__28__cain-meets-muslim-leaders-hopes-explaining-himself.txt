A few weeks ago, Herman Cain said he wouldn't want any Muslims in his cabinet. Now, he's saying sorry. 

The Republican presidential candidate held court Wednesday with a small group of Muslims at the All Dulles Area Muslim Society Center in Sterling, Virginia. He apologized for his previous comments critical of the Islamic faith, but he refrained from taking anything back. 

"While I stand by my opposition to the interference of Sharia law into the American legal system, I remain humble and contrite for any statements I have made that might have caused offense to Muslim Americans and their friends," said Cain. 

"I am truly sorry for any comments that may have betrayed my commitment to the U.S. Constitution and the freedom of religion guaranteed by it. Muslims, like all Americans, have the right to practice their faith freely and peacefully." 

Sharia law became an issue in the Cain campaign after his most recent appearance on FOX News Sunday, where he enthusiastically supported the residents of Murfreesboro, Tenn. who are trying to prevent a large Mosque's construction in their town. 

"Our Constitution guarantees the separation of church and state," Cain told Chris Wallace . "Islam combines church and state. They're using the church part of our First Amendment to infuse their morals in that community, and the people of that community do not like it. They disagree with it." 

Cain is polling at 9 percent nationally in the latest Fox News poll, well behind front-runner Mitt Romney who gets 26 percent. But Cain still comes out on top of other candidates like Tim Pawlenty and Jon Huntsman .
