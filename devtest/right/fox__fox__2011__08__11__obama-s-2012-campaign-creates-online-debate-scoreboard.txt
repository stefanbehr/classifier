While President Obama spent much of his time Thursday afternoon in Michigan criticizing Congress , his 2012 re-election campaign spent the evening countering arguments made by candidates in the Fox News, Washington Examiner Iowa GOP presidential debate. 

A few minutes after the debate started, Obama 2012 sent an email to supporters linking them to a debate scoreboard that featured photos of each of the eight candidates. Alongside those photos were candidate quotes and positions on issues from the debt ceiling to health care, immigration to education. If the Obama campaign picked up on what it viewed as a change in position on one of those issues, it pointed the issue out. 

"The Republican presidential candidates are debating in Iowa tonight, and we're keeping score," the web page reads. "During the debate, the panels below will keep track of where the candidates double down, where they backtrack, and what they fail to account for at all. Watch along, and share with your friends." 

Many of the GOP candidates attacked the president's record on many of those issues. Former Minnesota Gov. Tim Pawlenty used the president's name three times in four sentences in an early debate answer.
