Despite his army of grassroots supporters and a strong finish in the Iowa straw poll, the media are ignoring the " Ron Paul Revolution," the presidential hopeful says - and a new study suggests he may be right. 

Nine of the twelve potential presidents surveyed attracted more media attention this year than Texas Rep. Ron Paul , a study from the Pew Research Center's Project for Excellency in Journalism found. 

Paul has been the lead subject of only 27 campaign stories* between January 1 and August 14, according to Pew's research. That's less than a quarter of the coverage former Massachusetts governor Mitt Romney has attracted, and a small fraction of the 221 stories written about President Obama's reelection campaign. 

Paul beat out former Senator Rick Santorum and businessman Herman Cain in amount of media coverage he generated, but Donald Trump , who dropped out of the race, and Sarah Palin , who has not announced her intentions, both outpaced him. Stories about former Speaker of the House Newt Gingrich's limping campaign also blew past the amount of Paul coverage. 

"It disturbs me," Paul said on FOX News Tuesday when asked about his meager play in the press. "I have done quite well. I'm quite willing to match my name up against Obama any time of the day." 

It's a frustration Paul has voiced frequently since finishing a close second in the Iowa straw poll a week ago. Though Paul's showing in national polls has been less than stellar, the outspoken libertarian has a powerful grassroots fundraising operation, a flock of engaged supporters, and a good showing in local straw polls. 

"We showed we did well in Iowa, and we have good organization, and we can raise money," he said. "But they don't want to discuss my views, because I think they are frightened by us challenging the status quo and the establishment." 

Paul, a three-time veteran of the presidential campaign trail, says that voters eventually will discover that the more covered-candidates are just interested in maintaining the "status quo" - and media attention will taper off. 

"There's many that have entered this race, you know, over the past year," he said when asked about the popularity of Texas Governor Rick Perry on FOX News Friday morning. "They come for a few months or so and then they leave, and they get a lot of publicity." 

Officially entering the race less than a week ago, Perry edged out his fellow Texan in the Pew study, attracting 33 stories. Paul officially announced his candidacy in May. 

*Pew considers a candidate to be a "dominant newsmaker" in an article when they are featured in at least 50 percent of the story. Articles written between January 1 - August 14 were taken into account.
