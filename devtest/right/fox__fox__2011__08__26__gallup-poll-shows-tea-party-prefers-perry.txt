The migration of Tea Party support to Texas Gov. Rick Perry that many expected after his entry into the race for the 2012 GOP presidential nomination is becoming a reality according to a new Gallup poll. 

The poll shows 35 percent of Republicans who say they support the Tea Party also support Perry. Rep. Michele Bachmann and Mitt Romney are tied for second at 14 percent. Just behind them is Rep. Ron Paul at 12 percent. 

Among those Republicans who say they don't support the Tea Party , Romney leads the pack at 23 percent with Perry polling at 20 percent, Bachmann at 6 percent and Paul pulling 16 percent. 

Tea Party leaders have long insisted that despite polls showing Perry's popularity with their members, the group hasn't formally endorsed a candidate. FreedomWorks leader and former House Majority Leader Dick Army, a leading voice in the Tea Party said recently that the group's support isn't guaranteed. 

Mark Meckler of the Tea Party Patriots says his group hasn't picked a candidate and that he thinks members are waiting the campaign out a little longer before making a decision. 

"(It) seems like most everyone is taking a wait and see attitude," he said. "The "Tea Party Patriots" don't support any candidate. No one is the "inevitable" candidate of any kind." 

The same Gallup poll shows the Tea Party is a formidable part of the Republican Party . Fifty-eight percent of Republicans and Republican-leaning independents say they support the group.
