In the aftermath of Hurricane Irene and on the sixth anniversary of hurricane Katrina, FEMA administrator Craig Fugate says when it comes to government preparation, "we can't wait to know how bad it is before we get ready." 

Addressing reporters at the White House Monday, Fugate praised his team for working alongside the coastal states and Governors' offices over the last week in anticipation of Hurricane Irene. FEMA prepositioned resources in the track of the storm, monitoring its changes for days ahead of its landfall. Fugate, who served as the Director of Florida's Division of Emergency Management prior to joining the Obama administration in 2009, calls the anticipatory efforts "the mechanics we learned from Katrina." 

Irene was the first hurricane to make landfall during the Obama administration, and the FEMA official stressed the preparation and lessons learned from the devastating storm that claimed the lives of more than 1,800 Americans in the summer of 2005. 

Fugate also credited Congress for passing the Post-Katrina Emergency Management Reform Act which served to clarify to role of FEMA. He says one thing the government has improved upon is that "when we know there's a disaster that could occur, is not to wait until the state says 'we're going to need help'." 

While Irene did not turn out to be the monster that Katrina was, the research and preparation on FEMA's part ensured that the government is better prepared to manage storms. "You don't get time back in a disaster," said Fugate, recalling the hours after Katrina made landfall six years ago. 

As of this writing, the death toll for Hurricane Irene stands at 21. Several people are still missing.
