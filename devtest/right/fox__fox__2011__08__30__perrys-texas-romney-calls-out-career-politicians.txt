Speaking to the VFW national convention in San Antonio, Texas Tuesday morning, Mitt Romney took a not so subtle shot at rival Rick Perry without actually naming the Texas governor who is battling him for GOP front-runner status ahead of the 2012 election. 

"I am a conservative businessman," Romney said in his prepared remarks. 

"I have spent most of my life outside of politics, dealing with real problems in the real economy. Career politicians got us into this mess and they simply don't know how to get us out!" 

Perry has spent over two decades as an elected public official in the Lone Star state. Romney, in contrast, served four years as Massachusetts governor - but did run and lose elections to the Senate and presidency. 

Romney will also criticize President Obama's handling of Libya , calling it "mission muddle." 

"Our involvement in Libya was marked by inadequate clarity of purpose before we began the mission, mission muddle during the operation, and ongoing confusion as to our role in the future," Romney says in his remarks to this convention of military veterans of foreign wars. 

"Qaddafi is on the run and we congratulate the Libyan people and the extraordinary professionalism of our men and women in the armed services. But when a president sends our men and women into harm's way, he must first explain their mission, define its success, plan for their victorious exit, provide them with the best weapons and armor in the world, and properly care for them when they come home! " 

"As America's veterans, you understand better than anyone that weakness invites aggression," Romney will add. 

This is the 112th national VFW convention. Thousands of veterans are expected to congregate this week in San Antonio.
