Vice President Biden sharpened the Obama campaign's "us versus them" theme for the battle of 2012 at Tulsa fundraiser Tuesday, aiming directly at the Tea Party . 

He did, however, openly exclude a frequent administration target from his remarks at a Democratic National Committee fundraiser at a private home. 

The White House usually doesn't shy away from talking about the sour economy as something that began under President George W. Bush's watch, but Mr. Biden gave Obama's predecessor somewhat of a pass. According to the local reporter who covered the event for the national media, Mr. Biden said the former president should not be blamed for all of the country's economic problems, noting it has taken a long time for the economy to get as bad as it is. 

Biden instead set his sights on the Tea Party, reiterating the view of many Democrats that the organization was responsible for the derailing of an early debt ceiling deal. 

"This wasn't about money," the vice president said. "This was about a fundamental change in social policy. It's a legitimate question. It's a legitimate question. But the problem is, the Republican Party , we're not sure who it is." 

Although not all of the presidential candidates in the GOP field are backed by the Tea Party, the administration has continually framed the group as the Obama counter-point. 

In fundraising speeches, the president has said the differences between his campaign and the other side are much more defined than they were the last time around. 

Mr. Biden echoed that view Tuesday, "This is going to be the clearest-cut election... this is going to be the clearest choice of where we want to be. These guys have laid it out where they want us to be. And we're going to debate it."
