Senate Majority Leader Harry Reid , announced Thursday that the Senate would vote next week on a nearly $6 billion disaster relief aid bill to replenish the coffers of the financially-strapped Federal Emergency Management Agency, which the Nevada Democrat said was "flat broke." 

"We've had string of natural disasters that have been just awful," Reid noted, saying he would schedule a vote next week "just as soon as I can arrange it." 

Approval of disaster aid is traditionally nonpartisan, but this time could be different. Normally, such spending is deemed an emergency and not offset with spending cuts or revenue increases, but concerns about rising deficits have some Republicans changing their tune. 

"These are different times. We have got to figure out how to pay for these things," Sen. John Thune , R-SD, told reporters. 

House Majority Leader Eric Cantor, R-Va., took heat last week for suggesting that offsets be found, and on Wednesday said, instead, "When we are talking about offsets, that only has to do with this ad hoc sort of spending that has taken place in the past, which is what we tried to correct." The leader said firmly, "I am for making sure people get their money. I have never, never said that I'm holding anything hostage or would be playing politics with this." 

Democrats are prepared to pounce on any Republican that suggests cuts must be made in the budget to counter increased disaster spending, sensing that any opposition or delay could look callous. 

Reid, noting that the wars in Iraq and Afghanistan have not been paid for to date, dared Republicans to refuse his effort. "I hope my Republican colleagues have put politics aside and work with us to get relief to the American people who need it now," Reid said Wednesday. 

Under the recent agreement to raise the country's debt ceiling, lawmakers are cleared to approve up to $11.3 billion for disaster relief without offset. The Senate Appropriations Committee on Wednesday approved a fraction of that. 

"Our subcommittees marking up today have reviewed the known requirements caused by tornadoes, floods, and other disasters and will recommend that a total of $5.5 billion be allocated under the debt ceiling authority for disaster relief," the panel's chairman, Daniel Inouye of Hawaii, said. "We recognize that additional funds may be required to cover damages which have not yet been estimated based on recent flooding in the Midwest, the Northeast, and the South. Accordingly, the amounts could be adjusted as the affected bills continue to move through the legislative process if and when additional cost estimates are confirmed." 

And though the funding is part of a larger spending bill to fund the Department of Homeland Security, in which FEMA resides, Reid said he would move the disaster aid in a standalone bill, a move to designed to highlight GOP opposition.
