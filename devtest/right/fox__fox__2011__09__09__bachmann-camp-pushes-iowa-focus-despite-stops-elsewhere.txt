The campaign of GOP presidential candidate Michele Bachmann tells Fox News that the Minnesota Congresswoman will spend "a majority of her time" in Iowa going forward. 

But while the Associated Press reports Bachmann will campaign almost exclusively in Iowa, there are only two events currently slated over the next ten days. 

Campaign spokeswoman Alice Stewart notes that tomorrow, Bachmann will be hitting the tailgate crowd at the Iowa versus Iowa State football game in Ames. 

Bachmann has recently been urged by supporters to mix and interact more with Iowans coming off her Ames Straw Poll win last month. Some Republican activists, including Bachman-backers have been critical of the candidate's reliance on photo-ops and speeches, often omitting an Iowa campaign staple, taking questions from voters. 

With Texas Governor Rick Perry in the race, Bachmann is under pressure to win the Iowa Caucuses , the first state in the gauntlet of GOP contests. But, after the football game, Bachmann is off to Florida for a Tea Party debate in Tampa. Later next week, Bachmann is in California for the state GOP convention and an appearance on Jay Leno's show. 

Bachmann is slated to return to Iowa during the week of September 18-24. But that week includes the Fox News, Google Debate in Orlando on the September 22. 

Plus, Stewart adds, "we plan on being in New Hampshire soon." 

So, Bachmann's Iowa opportunities will be limited.
