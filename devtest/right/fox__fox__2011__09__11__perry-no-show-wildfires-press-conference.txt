Gov. Rick Perry , R-Texas, may have added fuel to the firestorm surrounding his handling of wildfires in his state. On Saturday, Perry was scheduled to hold a press conference to talk about the fires, but the event came and went with no sign of him. The explanation from his office didn't come until after the event was over. 

A spokesperson for the Governor said there was a last minute change in the location of the event, and the governor didn't want to make people wait around. The press was given a heads up about the change of location, but not about the governor's absence. 

His office added that officials at that press conference "reiterated the same message that the governor would have delivered." They changed the location because the first location was still restricted to the public. 

Gov. Perry has been receiving some criticism recently due to time spent on the campaign trail while there were fires burning in his state. However, earlier this week, Perry cut a campaign stop in South Carolina short to return to Texas and address the wildfires. 

Perry attended a 9/11 memorial service at the Texas State Cemetery in Austin on Sunday morning. He is scheduled to return to the campaign trail in Florida on Monday.
