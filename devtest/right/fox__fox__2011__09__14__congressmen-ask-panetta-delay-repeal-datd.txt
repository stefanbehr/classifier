House Armed Services Chairman Rep. Buck McKeon, R-Calif., and Rep. Joe Wilson , R-S.C., who chairs the Military Personnel subcommittee have written a letter to Defense Secretary Leon Panetta asking him to delay the repeal of Don't Ask Don't Tell on Sept. 20 because certain regulations regarding benefits to same sex couples have not been revised. 

Mr. Secretary, we trust that you will see the risk of moving forward with repeal without giving service members and their leaders adequate time to study, understand and prepare themselves to implement the revised policies and regulations they will need to be successful," the letter reads. 

The pair also requests that the Defense Department share with the committee the service chiefs and the views of senior military leaders in memoranda form, a request that has been denied by the Office of the Secretary of Defense in the past. 

"The Department is not ready to implement the repeal because all the policies and regulations necessary for the transition are not yet final," the letter that's dated Sept. 12 reads. "We would ask that the senior military leaders' memoranda immediately be made public and transmitted to the Committee on Armed Services." 

Despite the letter, the Pentagon intends to implement the policy on Sept. 20 as originally planned. 

"The repeal of Don't Ask Don't Tell will occur, in accordance with the law and after a rigorous certification process, on September 20," a Pentagon spokesman said. "Senior Department of Defense officials have advised Congress of changes to regulations and policies associated with repeal. We take that obligation seriously." 

And a senior defense official says the time has come to implement the repeal. 

"The nearly two-decade debate on this issue is over. The service secretaries, service chiefs, and combatant commanders submitted their recommendations months ago, and none of them suggested that repeal be postponed." 

Click Here To Read The Full Letter
