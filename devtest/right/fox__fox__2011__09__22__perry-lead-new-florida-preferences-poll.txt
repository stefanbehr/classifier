ORLANDO, Fla. - On the day of the Fox News/ Google Republican presidential debate, a new poll is showing that Texas Gov. Rick Perry has taken the lead over former Massachussetts Gov. Mitt Romney and the rest of the GOP field in the key battleground state of of Florida. 

Perrry is ahead of Romney with 28 percent to 22 percent, according to the Quinnipiac University poll released Thursday. If former Alaska Gov. Sarah Palin does not jump into the race, the landscape improves for Perry with his lead increasing to 31-22 percent over Romney. 

However, if Romney would face President Obama in a hypothetical match-up, the Republican would lead 47-40 percent but Perry would be behind Obama 44-42 percent. 

By a margin of 57 percent to 39 percent, voters polled from across the poltical spectrum disapprove of Obama's job performance-- his "worst score in any Quinnipiac University poll in any state," according to a statement released by the survey taker. 

With 29 electoral votes, Florida is a crucial state for Obama's 2012 map towards keeping the White House . In 2008, he won the Sunshine State in his victory over then-GOP presidential nominee Sen. John McCain .
