Senior Senate leaders are mulling over a couple of different options here to avoid a government shutdown. 

It hinges on how flush FEMA is with cash and whether it can make it through the rest of the week, which happens to be the end of the fiscal year. 

In short, House Republicans were mostly concerned with offsetting disaster money for this fiscal year. Since the new fiscal year starts Saturday, there is a lot more agility there to find pay-fors, et al. 

So, if FEMA can hang on for this week, one option would be for the Senate to pass a short-term Continuing Resolution (CR)...which is a stopgap spending bill...to keep the government running past 11:59:59 pm Friday night and into early next week. 

Ostensibly, this bill would only fund general government operations and not add more money to FEMA's budget for this year. 

I'm hearing the "straight" CR could also fund the government through November 18...not just next week. 

The Senate could conceivably vote on the short-term bill today or tomorrow and the House could do it by unanimous consent. 



The House is technically not "here" right now. But they are in pro forma session the next few days. And they could okay it by unanimous consent with only a skeleton crew here. 

Sen. Chuck Schumer (D-NY), said, "It's hard to understand that given the fact that FEMA is funded through Thursday or Friday that Republicans couldn't agree to a compromise." 

Sen. Ben Cardin (D-MD), however, offered a word of caution, saying "This hasn't gelled yet." 

Adding to this... 

In addition, Senate Democrats may caucus at 6:30 pm tonight. 

When asked about a straight CR, Senate Minority Leader Mitch McConnell (R-KY) responded "We'll be in touch."
