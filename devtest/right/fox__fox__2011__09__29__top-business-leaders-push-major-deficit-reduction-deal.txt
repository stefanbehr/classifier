WASHINGTON -- A coalition of about 150 top business groups is urging the Super Committee on Capitol Hill to go big on a deficit deal, pushing the panel in a letter first obtained by Fox News to put both entitlement cuts and major tax reform on the table in order to squeeze out far bigger savings than is in the panel's mandate. 

While the letter organized by the powerful U.S. Chamber of Commerce, the Business Roundtable, and the National Association of Manufacturers does not cite a specific dollar figure, a senior official involved in the effort told Fox News the leaders would like the panel to shoot for a deal in excess of $4 trillion in deficit reduction. 

President Obama recently unveiled a deficit reduction package in the range of $3 trillion to $4 trillion, which included about $1.5 trillion in tax increases. Republicans have shown little or no support for Obama's plan and top aides in both parties privately say they do not currently expect that a deal of that size can be ironed out. 

But the business groups have a significant amount of clout on Capitol Hill and, in the letter dated Thursday, they urged the 12 members of the panel to "go beyond the legislative mandate of the Balanced Budget Control Act of 2011 to achieve savings of $1.2 to $1.5 trillion to ensure that we stabilize our nation's debt and put the debt's share of the economy on a downward path. This is essential for long-term economic growth in our nation." 



Rep. Chris Van Hollen (Md.), a Democrat who serves on the Super Committee, told Fox News he's still hopeful that a deal of this magnitude can get done but is skeptical all sides can come together. 

"I've always been in the camp where I hope we can go big, get some sort of an overarching agreement that tackles a lot of these issues," said Van Hollen, who also serves as the top Democrat on the House Budget Committee. 

But, Van Hollen added, "I don't know if we'll be able to get there or not. Only time will tell. I think there is a seriousness of purpose of getting everyone to work very hard in a very short period of time to get the job done." 

Rep. Andy Harris (R-Md.) noted there is a huge challenge looming over the Super Committee, which has to get both chambers of Congress to reach a final deal right before Christmas, based on the law that created the panel during this summer's debt ceiling negotiations. 

If the panel fails to reach a bipartisan agreement, the law mandates there will be a process known as "sequestration" in which painful cuts to the Pentagon as well as entitlement programs like Medicare will kick in to reach the $1.2 trillion target. 

"I suspect we may be delaying our holiday break, our holiday recess for a while we go over at that issue," said Harris. "But then again, we have statutory deadlines to meet. And we have automatic sequestration if we don't meet those deadlines. So one way or the other, there will be $1.2 trillion cut from the budgets in the next ten years. The question is how will we get to that $1.2 trillion." 

The letter from the largely Republican-friendly business groups may put pressure on GOP leaders to put major tax reform on the table in the negotiations, something they have been reluctant to do. 

Speaker John Boehner (R-Ohio) though recently gave a speech suggesting the panel should lay out principles and a process for dealing with comprehensive tax reform to lower corporate and individual rates. 

While the business leaders do not mention tax increases in the letter, they seem to suggest they would go further than Boehner by pushing the panel to go beyond just principles and actually get a more specific tax reform deal. 

"This letter reinforces the Speaker's message that American job creators need us to lift the cloud of uncertainty caused by our deficit and reform our tax code to support private sector job growth," Brendan Buck, spokesman for Boehner told Fox. 

During their so-called "grand bargain" negotiations, Obama and Boehner discussed eliminating some tax deductions in exchange for lowering corporate tax rates in order to try and spur economic growth and thus bring in more tax revenue to the Treasury. But the talks broke down, in part because Obama was also pushing specific tax increases and Boehner balked. 

"Put simply, Congress must reform entitlement programs and comprehensively restructure the U.S. tax code," the groups wrote to the Super Committee, though they did not outline anything close to a specific plan for action. "The U.S. tax system is in desperate need of simplification and fundamental, comprehensive reform that encourages investment and employment." 

The letter was signed by dozens of groups, from the American Gas Association to the National Lime Association.
