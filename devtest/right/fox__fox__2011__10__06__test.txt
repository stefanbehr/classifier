When Fox News Correspondent Ed Henry was in Dallas with President Obama this week, he bumped into a little boy who had memorized the president's famous 2008 election night speech in Grant Park in Chicago. Watch as he delivers some of the highlights from the speech. 

Watch the latest video at FoxNews.com
