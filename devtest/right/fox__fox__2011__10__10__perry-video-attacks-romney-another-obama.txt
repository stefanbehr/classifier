Rick Perry's presidential campaign has released another Web video claiming there are striking similarities between Mitt Romney and President Obama on a key issue -- this time, health care. 

The video purports that Mitt Romney s government mandated health care plan in Massachusetts was the model for what it calls Obamacare. 

It includes news clips of Obama saying he agrees with the former Massachusetts governor s health care plan and other clips claiming Romney has since changed his position on his state s plan when he says what was right for Massachusetts may not be right for America. 

The Romney campaign says this video is just one of at least 19 distortions the Texas governor has made since entering the race. 

Rick Perry is a desperate candidate who will say and do anything to prop up his sinking campaign," Romney s communication director Gail Gitcho said. 

Gitcho added, In trying to deflect attention from his liberal in-state tuition policy for illegal immigrants, he has resorted to repeated dishonesty, distortions and fabrications about Mitt Romney . 

Romney and Obama have been the focus of most of Perry s web videos. Mark Miner, Perry s press secretary, said that s because Mitt Romney and President Obama are similar on many issues. 

As for this latest video, the Romney campaign claims the Perry campaign did some selective editing. It points to a years-old "Meet the Press" interview featuring Tim Russert saying, Why, if it's good for Massachusetts and it's working in Massachusetts, wouldn't you apply it to the rest of the country? Romney replies, I would. 

The Romney camp says the former Massachusetts governor went on to say he wouldn t support a mandate. Romney also said, I like what we did in Massachusetts. I think it's a great plan. But I'm a federalist. I don't believe in applying what works in one state to all states if different states have different circumstances. 

Among the other 18 distortions claimed by the Romney campaign are Perry's alleged backtrack from his endorsement of Al Gore in 1988 and Perry s perceived back and forth on the idea of moving Social Security and federal pensions back to the states. 

This latest Perry ad comes ahead of the candidate s first big policy speech of the 2012 campaign. The campaign says it will be Friday in Pittsburgh and focus on energy and jobs.
