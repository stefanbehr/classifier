The campaign for Republican U.S. Rep. Allen West of Florida confirms that he has raised nearly $2 million dollars for his-re-election bid, dwarfing his nearest Democratic rival by more than double in the same fundraising period. 

"The people of the 22nd Congressional District of Florida along with Americans from across the United States have heard our message and support a strong constitutional conservative in the House of Representatives," West said in a news release about his $ 1.9 million gain. 

Tuesday, West's Democratic opponents Lois Frankel and Patrick Murphy announced that they had raised a total of $415,000 and $313,000 respectively, according to a report by George Bennett at the Palm Beach Post. 

West's campaign says that the average contribution to the West for Congress campaign during the third quarter, was approximately $47 per donation, with 99.9% of contributions coming from individual donors. 

West has raised over $ 4 million for his 2012 re-election campaign.
