Cain train meet the Cain plane. 

As GOP presidential candidate Herman Cain ascends up the national polls, one airline has introduced a fare sale based on his 9-9-9 plan to reform the U.S. economy. 

"Let's get real... America can't wait until 2012, you need a vacation now!", says a Spirit Airlines advertisement promoting the 9-9-9 Vacation Plan . The Florida-based low-cost carrier is offering fares for as little as $9 each way on a round-trip ticket. 

Cain says, if elected, he will turnaround the economy by instituting a 9 percent business flat tax, individual flat tax, and national sales tax. 

"We love it," responds Cain Spokesperson JD Gordon. "9-9-9 is the way to go." 

The latest NBC News/Wall Street Journal Poll has Herman Cain leading the pack with 27 percent of the vote, up from just five percent in August. 

This isn't the first time Spirit Airlines has tied its advertisements to hot topics. Previous ads include: "The Weiner Sale: With Fares Too HARD To Resist," making light of the sex scandal that forced New York Democratic Congressman Anthony Weiner to resign, "Check Out The Oil On Our Beaches," in reference to the Deepwater Horizon oil spill, and its "Eye of the Tiger Sale" after pro golfer Tiger Woods crashed his SUV outside of his Florida home after a dispute with his wife.
