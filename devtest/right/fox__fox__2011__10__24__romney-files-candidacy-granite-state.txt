Mitt Romney made it official this morning, making the traditional visit to the New Hampshire secretary of state's office to formally sign his candidacy papers for the first-in-the-nation primary. 

Dozens of cameras jockeyed for position around the historical wooden desk as the former Massachusetts governor signed his name, thanking well wishers who lined the second floor hallway. 

Following the event, Romney held a brief rally in front of the State House, walking down the steps with former New Hampshire governor John Sununu, who despite much fanfare proceeded to give a somewhat tepid endorsement for Romney's presidential bid during his brief introductory remarks. 



Romney addressed the crowd in a short rally speech, saying New Hampshire will remain the "first primary in the nation as it ought to be" -- a reference to Nevada's failed efforts to hold their primary earlier, a move that would have changed the entire political calendar. New Hampshire state law requires it be the first state to hold a primary, thereby ensuring its continued relevance in the presidential election process. 

Romney hit key campaign talking points, telling the small crowd that "the American people realize it's time for real change," adding President Obama has failed to create jobs and has increased the size of government. 

With a home in New Hampshire, Romney can almost claim native status, and given that the state borders Massachusetts, voters here know him well. He continues to do well in Granite State polls, and this morning's event was carefully staged. Romney bypassed the traditional round table with local reporters and answered no questions as he moved through the gathered media to his waiting car.
