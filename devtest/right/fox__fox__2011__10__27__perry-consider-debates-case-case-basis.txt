Texas governor Rick Perry will not be participating in all the GOP debates. His campaign has committed to the next debate in Michigan on November 9th, but that's the only one the campaign says it has decided on. 

Perry spokesman Ray Sullivan tells Fox News,"We will take part in future debates, but have not made final decisions on which. By our count there are 16 debates planned between now and Jan 31st. 16 debates in 12 weeks planned." 

The campaign is considering each debate on a "case by case basis." 

Several debates have been announced in the past weeks. In November alone there are three debates already on the calendar.
