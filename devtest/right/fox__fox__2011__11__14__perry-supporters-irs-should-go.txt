Commerce, education and ... the Internal Revenue Service. In an attempt to put the recent Michigan debate gaffe behind him, Texas Gov. Rick Perry took to the internet to ask his supporters what agency or department they would eliminate. 

The campaign says more than 20-thousand visits were made to their web page last week when the question was posted, and the answer was overwhelmingly the IRS, which is a part of the Treasury department. 

Who can forget what Perry couldn't remember at the CNBC GOP Presidential debate Michigan last week? For his part, Perry chose commerce, education, and eventually about 15 minutes later, the department of energy. 

But ending the energy department only polled 11-percent with Perry's supporters. Energy fell after education at 16-percent, and the Environmental Protection Agency at 14-percent in the campaign's online poll. 

Perry has said many times that his 20-percent optional flat tax and simplified tax codes will "end the IRS as we know it." 

Supporters of Texas Congressman Ron Paul will likely be happy to read the IRS should go. For his part Paul, the candidate Perry looked to for help remembering during the debate, wants to get rid of five agencies. 

In a recent interview with AP, Paul said he wasn't trying to make Perry look bad during the debate when he held up his five fingers and said five agencies should go. Paul described Perry's stumble as "a human reaction," which is also how the Perry campaign has described it. 

Paul, a huge proponent of limited government, wants to get rid of the same three agencies and departments Perry does. But, Paul also wants to throw out the income tax and IRS, and close down the departments of Housing and Urban Development and Interior.
