Still no consensus from The FAMiLY Leader, one of several social conservative groups in Iowa, on which GOP presidential candidate to back. 

Board members continued deliberating Tuesday, but Bob Vander Plaats, the CEO of the group says that despite a conference call yesterday, there is "nothing new to report". Vander Plaats says The FAMiLY Leader board is still working on narrowing its list of four candidates to back: Michele Bachmann , Newt Gingrich , Rick Perry and Rick Santorum. 

"I think the board is where 80% of Iowans are, they can be persuaded," explains Vander Plaats. The Iowa Faith Freedom Coalition has announced it will not endorse any candidate. The president of the group, Steve Scheffler, who is also an RNC committee member, says he will not personally endorse a candidate either. 

Similarly, Iowa Right-to-Life's board of directors have decided not to back a single candidate, and its executive director Jenifer Bowen will also abstain from endorsing a presidential candidate. 

There is a persistent concern among social conservatives, which includes the state's politically active Evangelical Christians, that without consensus self-described "pro-family" candidate Mitt Romney may win the Iowa Caucuses, and perhaps roll-up the Republican nomination early. 

Social conservatives in Iowa have been openly critical of Romney for his pro-choice past on the issue of abortion.
