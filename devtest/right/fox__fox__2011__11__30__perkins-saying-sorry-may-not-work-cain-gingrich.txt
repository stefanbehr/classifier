Evangelical Christians may forgive the personal indiscretions of GOP presidential candidates, but Family Research Council President Tony Perkins says that does not mean they will vote for them. Perkins claims voters, "want to know they are electing a politician with character, not character as a politician." 

Perkins told Fox News' Megyn Kelly that while Evangelicals embrace the idea of forgiveness, there is a difference between accepting an apology, and "placing your trust in him as president and Commander-in-Chief." 

In Perkins' view, Herman Cain's lawyer Lin Wood may have done the candidate real damage with social conservatives on Tuesday, when he said Ginger White's claim of a long term affair was "a private consensual matter" and "beyond the scrutiny of the public." 

Perkins said an apology would be a good idea if these allegations are true, but it may not be enough for the Atlanta business man. "I think at this point there are so many allegation that have been made, it might be beyond the ability of Herman Cain to pull this out," said Perkins. 

As for Newt Gingrich , Perkins thinks the next two weeks are telling for whether or not he can be the alternative to Mitt Romney for social conservatives. 

Perkins claims Gingrich is not an "outsider" and will face scrutiny on both his personal and professional record as Speaker of the House. "If you look at his new Contract with America it is devoid of social issues. What he makes a priority of is often absent from that." 

For his part, Perkins says there are no "moral qualms" with the way Romney has been conducting himself. The problem for Perkins lies in "his professional life," where Perkins says you find inconsistencies in his policies.
