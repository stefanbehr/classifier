GOP presidential candidate Mitt Romney has decided not to participate in The Newsmax ION Presidential Debate scheduled for December 27th in Des Moines, Iowa which will be moderated by business tycoon, and reality TV celebrity Donald Trump . 

Appearing a short time ago on FOX News, Romney told anchor Neil Cavuto that he chose against the event because there were already two debates in Iowa this month and he wanted to spend the rest of the time before the primaries campaigning and " doing work you have to do." 

Romney said he called Trump to inform him of his decision. 

" He understood my perspective and wished me well, " Romney said. 

Amongst the rest of the GOP presidential field, Iowa front-runner Newt Gingrich , and Rick Santorum have both said they will participate while Ron Paul , and John Huntsman have also rejected the offer. 

Rick Perry and Michele Bachmann are still weighing their decisions. 

" Trump called Michele this morning. We have not confirmed the debate," Bachmann Campaign spokeswoman Alice Stewart said to FOX.
