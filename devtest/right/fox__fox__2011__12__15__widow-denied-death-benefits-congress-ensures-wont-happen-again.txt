When the Senate passes the National Defense Authorization Act (NDAA) Thursday, funding for weapons programs and troop pay raises will be touted across the country, but one widow in Arkansas will not be celebrating. 

Miranda Luke has been fighting for nearly two years to obtain her late husband's military benefits, benefits the Army abruptly denied when he died at the couple's home in 2010. 

Captain Samson Luke, a Bronze-star active-duty officer who deployed for combat twice in Iraq , later served as an Arkansas National Guardsman. During a mandatory weekend training assignment at Fort Chaffee, Luke was authorized to return to his home, just 12 miles from base, to sleep for the evening after a grueling day of heavy labor in freezing conditions. His heart stopped. He was just 34 years old, leaving behind a wife and four young children. 

Upon Luke's death, his widow was assigned a casualty assistance officer. Three weeks later, the Army pulled that officer out and told Miranda that she would not be receiving the $100,000 death gratuity or up to $8,000 in funeral expense benefits that he would have automatically received, no questions asked, had he died on base or "in the vicinity" of the training facility, as required by law. 

Miranda Luke sought out the help of her senator, Mark Pryor , who set about clarifying any ambiguity in the law. Pryor got that clarification in the bill that will soon be signed by President Obama, but members of the House and Senate Armed Services Committees who hammered out the differences between the two chambers' bills, stripped out the retroactive clause in the Senate bill that would have helped the Luke family. 

Still, Pryor, in a statement, said he is optimistic the Luke family will soon receive the benefits. An aide to Pryor tells Fox a meeting is currently being set with the Secretary of the Army for next week (the secretary is currently overseas). 

According to the statement from Pryor's office, Miranda Luke said, "I would like them to do what's right. I would like them to not only give me and my four children back the benefits that my husband died for. I would also like them to change the verbiage, the language, so that no one else has to go through this again." 

With the passage of the defense bill, this situation should not happen ever again. 

Pryor's provision in the final bill clearly states that death benefits are available to any reservist who dies during an authorized stay at their home during inactive duty training. 

"Miranda Luke and her four kids have dealt with a lot over the past two years, from losing their husband and father to facing ongoing uncertainty over military benefits they are due," Pryor said in the statement. "It's been an honor to team up with Miranda so we can ensure families facing similar situations are taken care of financially. I'd also like to see a better result for the Luke family, and I think we'll get there soon."
