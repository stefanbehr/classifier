In statements, some of the Republican presidential candidates reacted to the death of North Korean dictator Kim Jong Il. 

" Kim Jong-il was a ruthless tyrant who lived a life of luxury while the North Korean people starved. He recklessly pursued nuclear weapons, sold nuclear and missile technology to other rogue regimes, and committed acts of military aggression against our ally South Korea , " Mitt Romney , the former Massachusetts governor said in a release. "He will not be missed. His death represents an opportunity for America to work with our friends to turn North Korea off the treacherous course it is on and ensure security in the region." 

"America must show leadership at this time. The North Korean people are suffering through a long and brutal national nightmare. I hope the death of Kim Jong-il hastens its end," Romney concludes. 

Jon Huntsman , a former U.S. ambassador to China , also described Jong IL as a "tyrant." 

"Kim Jong Il was a conscienceless tyrant. His death closes a tragic chapter for the people of North Korea and offers them the best opportunity to get on a path towards a more free and open society and political reform, " Huntsman said. 

"In the short-term, the United States must pay extremely close attention to the disposition of North Korea's nuclear bombs, nuclear materials, and other elements of their WMD program and use all available pressure points to prevent rogue activities and proliferation," Huntsman adds."This must include close consultation and coordination with our allies in the region, South Korea and Japan , and maintaining an open dialogue with other influential parties such as China and Russia to ensure stability in the region." 

Texas Governor RIck Perry said the totalitarian leader's death could serve as an opportunity to "to reunify the peninsula." 

"The death of vicious dictator Kim Jong Il provides some cause for hope but does not automatically end the reign of inhumane tyranny he and his father constructed, " P erry stated." Twenty-three million people still live under North Korea's isolationist, inhumane and tyranical policies. North Korea remains a nuclear power, and there is a great threat that those weapons might fall into the wrong hands if civil war breaks out." 

"The United States must now strongly reaffirm our commitment to Asian allies, particularly South Korea , and maintain a strong military, diplomatic, and economic presence in the Pacific region during this period. We should also engage with China , and encourage Beijing to work towards a peaceful transition from a grim dictatorship to a free Korea, " Perry said.
