MANCHESTER, N.H. - Republican presidential candidate Newt Gingrich said the race to win the Iowa caucuses has become a "real mess." 

Speaking at a town hall to voters in the first in the primary state of New Hampshire, which is one week after the Iowa caucuses, he brought up the slew of advertisements that have been airing in the state. 

"The Iowa race has gotten to be a real mess," Gingrich said.Adding the amount of money dropped, 

"I think my friends have bought about $7 [million] or $8 million of negative advertising so far." 

He vowed that he would "cheerfully" keep running. 

Gingrich made the made remarks in the Granite state as Romney also makes his final push to voters. Romney did a bus tour through New Hampshire, making what the campaign called his "closing arguments" to voters who will cast the first primary votes in the 2012 race. 

Romney said on Fox earlier in the day that maybe Gingrich is complaining a little too much. 

"If you can't stand the relatively modest heat in the kitchen right now, wait until Obama's hells kitchen shows up, "Romney said on "Fox and Friends." 

Gingrich and Romney have been taking jabs at each other for the last couple weeks, Gingrich saying he wants a positive campaign, and that he'd only responded when attacked. 

Romney has had to no issue taking swipes at his opponent, especially after Gingrich started surging in the polls a couple weeks ago. 

A so-called SuperPAC, Restore our Future, has been releasing attack ads slamming Gingrich, to which the former House speaker said were false. 

The ad said in part, "Gingrich not only teamed up with Nancy Pelosi on global warming but together they cosponsored a bill that gave $60 million a year to a UN program supporting China's brutal one-child policy. As speaker, Gingrich even supported taxpayer funding of some abortions." To that he said it's untrue because the UN money was never OK'd and support for federally funded abortion was just in the case of lower-income women dealing when the mother's life was danger, rape or incest. 

In an interview with Fox News' Chief Political Correspondent Carl Cameron aboard his campaign bus, Romney said he has nothing to do with them and hadn't viewed "all" of them. 

"The question of whether I judge them or not is not relevant because I can't direct what ads they run," Romney said. 

A new Rasmussen poll puts Romney back in the lead in Iowa, with Ron Paul in second and Gingrich slipping to third. 

Romney has held a commanding lead in New Hampshire, having been governor in nearby Massachusetts, but Gingrich still got the backing of the influential newspaper, "The Union Leader."
