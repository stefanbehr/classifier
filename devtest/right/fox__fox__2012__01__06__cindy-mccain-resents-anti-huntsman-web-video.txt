Cindy McCain , a victim of dirty campaigning herself in the 2000 presidential election, is weighing in on a controversial campaign ad made by a purported Ron Paul supporter that uses images of Jon Huntsman's adopted daughters in an attempt to prove Huntsman is "un-American." 

Senator John McCain's wife tweeted: "I deeply resent the video made using the adopted daughters of @johnhuntsman. @ronpaul shame on you. This has shades of 2000 all over it." 

She is referring back to the 2000 GOP primary when push polls in South Carolina asked voters what they thought of McCain "fathering an illegitimate black child." McCain's adopted daughter Bridget is from Bangladesh . 

In the Huntsman video, footage is used that show his two adopted daughters, one of whom is from India and one of whom is from China. Ron Paul's campaign has disavowed the Huntsman ad calling it "disgusting." 

The Congressman himself says "I haven't looked at it, but I understand it is an ugly ad and I have disavowed it, and obviously all campaigns have to suffer these consequences when somebody goes up and they put the candidates name on there and you get bad press for it, but obviously it was way out of order for them to do that." 

Both Cindy and John McCain have endorsed Mitt Romney and have been campaigning for him this week.
