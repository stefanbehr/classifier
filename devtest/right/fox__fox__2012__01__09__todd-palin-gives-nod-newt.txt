Todd Palin, the husband of 2008 Republican vice presidential candidate Sarah Palin , is throwing his support behind 2012 GOP presidential hopeful Newt Gingrich . 

Sources close to the campaign say Gingrich spoke to Todd Palin this morning, but he hasn t spoken to Sarah Palin recently. 

This is "a tremendous boost from an influential conservative that will help advance Newt's case in NH as a Reagan conservative," Gingrich spokesman R.C. Hammond told Fox News. "It's evidence that those who want to break up the status quo in Washington are putting their support behind Newt Gingrich ." 

This obviously raises questions about whether the former GOP Vice-presidential nominee will endorse, especially considering Palin's 2008 running mate, Arizona Sen. John McCain, announced his backing for Mitt Romney last week. 

Sarah Palin dodged the endorsement question on Fox News recently, saying, "I'm still in that process with probably 70 percent of Americans trying to decide."
