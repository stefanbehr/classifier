BLYTHEWOOD, South Carolina - Texas Governor Rick Perry is now taking aim at one of his former GOP rivals. Responding to Herman Cain's comments that Perry "wasn't very deep on the issues," Perry fired back. 

"I hope Herman Cain doesn't think running Godfather's Pizza is the same as running the state of Texas," Perry said Thursday during an interview on Laura Ingraham's radio show.Cain dropped out of the race back in December after a series of missteps and sexual harassment allegations torpedoed his campaign. 

Despite abysmal finishes in both Iowa and New Hampshire, Perry insists he can still pull off a victory in the Palmetto State. 

"We're here to win and think there is a real possibility for us to win this election in South Carolina," Perry maintained during an interview later Thursday with Martha MacCallum on Fox News Channel. 

He has a lot of ground to cover. An average of the latest polls put him in the back of the GOP pack here. Yet, those are not the numbers Perry is focusing on. 

"About sixty percent of the people [here] are either undecided or can still be swayed," Perry told MacCallum. 

Perry has been trying to sway some of those people by hammering Mitt Romney over his record as CEO of Bain Capital, saying the former Massachusetts governor was not a venture capitalist, but a "vulture capitalist," and highlighting companies in South Carolina where hundreds of workers lost their jobs after Bain took them over. 

"In those cases where they've come in and basically taken the profits out of these companies, and sold them for a quick profit, I'm not for that, I don't think most people in South Carolina are," Perry told Fox News. 

"I think we are making a point that the Republican Party should always be about creating a climate where jobs can be created."
