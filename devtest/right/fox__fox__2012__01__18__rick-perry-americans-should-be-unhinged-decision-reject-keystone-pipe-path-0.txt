Asked about the imminent decision expected from the State Department , which sources say will call for a rejection of the Keystone XL Pipeline from Canada to Texas, Republican presidential candidate and Texas Gov. Rick Perry said Wednesday it's no surprise, but it doesn't make the decision any better. 

"It doesn't surprise me but it's again -- the president's focused more on the next election than on the next generation. 

"Getting this country independent of foreign sources of crude from countries that are not our friends is really problematic. So this Canadian oil, there's a possibility we could lose it to China with that decision so I hope Americans will really become unhinged with that decision because it is a really bad decision for our country, for energy independence and sends a horrible message at a time that were headed to $4 and $5 oil or excuse me $4 or $5 gasoline, to have a neighbor who's willing to sell us crude that is available."
