Mitt Romney , whose wealth is under scrutiny as he hasn't yet released his tax records -- something he pledged Sunday he would do this week -- said he hopes that people don't hold it against him that he has spent 10 percent of his fortune on tithes to his church. 

Romney is a Mormon. He has long argued his religion shouldn't be a reason for anyone to not vote for him. But after being asked whether it could be a political problem that he has given millions of dollars to the Church of Latter Day Saints, Romney said Sunday he'd be "surprised" if that came up. 

"Gee, I hope not. If people want to discriminate against someone based upon their commitment to tithe, I'd be very surprised. This is a country that believes in the Bible. The Bible speaks about providing tithes and offerings. I made a commitment to my church a long, long time ago that I would give 10 percent of my income to the church. And I followed through on that commitment," he told "Fox News Sunday." 

"And, hopefully, as people look at various individuals running for president, they'd be pleased with someone who made a promise to God and kept that promise. So, if I had given less than 10 percent, then I think people would have had to look at me and say, hey, what's wrong with you, fella, don't you follow through on the promises?" 

Exit polls from Saturday's Republican presidential primary in South Carolina showed that for voters who said it matters that a candidate shares their religious beliefs, Newt Gingrich , who converted to Catholicism last decade and raised Romney's religion on the campaign trail, beat Romney 46 percent to 20 percent. 

Rick Santorum is also Catholic. Ron Paul is a Baptist. President Obama was a member of the Trinity United Church of Christ before moving to Washington, D.C., from Chicago . 

Romney said that he doesn't think faith is going to be an issue in this contest. "The great majority of people chose the candidate who they think should be the next president as opposed to someone who maybe in the same faith that they're in," Romney said of the votes so far in the primary season. 

"I don't think in the final analysis that religion is going to play a big factor in selecting our nominee. I do think that conservative values do play an enormous role and I think the speaker has some explaining to do for sitting down on the sofa with Nancy Pelosi and arguing for climate change regulation, for calling the Paul Ryan plan right wing social engineering," he said.
