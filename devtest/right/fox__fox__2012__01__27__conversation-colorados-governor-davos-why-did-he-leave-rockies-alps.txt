DAVOS -- This is Colorado Gov. John Hickenlooper's first Davos. He paid his own way. Even though the big sport in these parts is skiing, Hickenlooper says the fishing at the World Economic Forum is great. 

Fishing for business, anyway. 

"We're trying to create jobs in Colorado. Not just make jobs, but create what we call sustainable jobs that will be there in 10 years. That requires you have customers. You are either creating a product or providing a service. There are so many wonderful executives here who are trying to grow their companies. They want to be in a place that is relentlessly pro-business and has high standards," he said. 

Hickenlooper said while companies don't want excessive regulation, they don't mind being held to high standards, particularly when it comes to the environment. So he's been trying to paint Colorado as a great place for investment, one that ticks all the boxes. 

And there's been some serendipity in the snow. He met lots of people here who are planning ski trips to Aspen and Vail in the not-too-distant future. "That's an asset I never utilized before," he said. He said he plans to try to visit some of them when they head to Colorado, and give them a little guidance and whatever assistance he can. "It's about building relationships." He said trust leads to commitments. 

Davos is known as a jet-setters mecca. But it is a lot more diverse than many people think. 

"I wouldn't say it's a melting pot, but it's a mixing bowl -- non-profits, NGO's, titans of industry, political leadership from all over the world," he said. 

Besides making lots of contacts here -- from wind farm manufacturers to other industries that may seek out opportunities in Colorado -- Hickenlooper said his eyes have been opened. He had lunch with the U.N. Secretary General Ban Ki-moon and Bishop Desmond Tutu, who were talking about the plight of millions girls around the world who are forced into unwanted marriages. The Colorado governor said it is important that these issues get exposed, and he, for one plans to share the stories like this that he has heard. 

Americans are nervously watching the Euro drama play out, and Treasury Secretary Tim Geithner told crowds here on Friday that a U.S. recovery will be dependent to a large extent on how the Eurozone muddles through its crisis. But Hickenlooper said the international delegates assembled here in Davos are looking to America for hope. 

One, from Indonesia , told him, "We can't do it alone. We can't come out of this without the United States to lead us." 

And people here are keen to know what will happen in the U.S. presidential election. 

"Everybody wants to know what is going to happen. Is Romney going to win? Is Gingrich going to win? Is President Obama waging a class war? Is he trying to hurt the rich in some way? People have asked me how can Americans possibly call Barack Obama a socialist? The man's an incredible capitalist! He thinks the rich should pay more taxes, but he's not anywhere near a socialist," he said.
