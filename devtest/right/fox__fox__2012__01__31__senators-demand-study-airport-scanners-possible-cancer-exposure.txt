Chances are, if you have been at an U.S. airport lately, you've been inside a tubular body scanning machine called a "backscatter," named for the way it scatters electrons. On Tuesday, a bipartisan group of senators introduced legislation to determine whether the scanners emit harmful levels of cancer-causing radiation, as some experts believe. 

The bill would also require signs at airports to clearly alert travelers of alternative screening options available in lieu of the X-ray devices. 

Primary bill sponsor Sen. Susan Collins , top Republican on the Homeland Security and Governmental Affairs Committee, told of a once-pregnant constituent who believes she miscarried after unknowingly entering a backscatter machine. 

"Time and time again I have expressed my concern over their use, particularly since there is an alternative screening technology available," she said. "While the ( Transportation Security Administration ) has repeatedly told the public that the amount of radiation emitted from these machines is extremely small, passengers and some scientific experts have raised legitimate questions about the impact of repeated exposure to this radiation." 

According to the TSA's website, "One scan is equivalent to approximately 10 microRem of radiation. This is equivalent to the exposure each person receives in about two minutes of airplane flight at altitude or each person receives every 15 minutes from naturally occurring background radiation." 

A TSA official told Fox News on Tuesday, "While we do not comment on pending legislation, TSA is committed to working with Congress to explore options for an additional study to further prove these machines are safe for all passengers." 

The agency notes that "the Food and Drug Administration (FDA) and NCRP (National Council on Radiation Protection) have advised that the (backscatter) is safe to use on anyone ages 5 and up regardless of gender or any medical condition." 

Meanwhile, in November, the European Commission announced that it would ban the use of X-ray machines from all airports in the 27-member European Union "in order not to risk jeopardizing citizens' health and safety." 

The Collins bill, co-sponsored by panel members Daniel Akaka , D-Hawaii, Carl Levin , D-Mich., Tom Coburn , R-Okla., and Scott Brown , R-Mass., would "require the Department of Homeland Security's Science and Technology Directorate, in consultation with the National Science Foundation , to commission an independent study on the possible health effects of the X-ray radiation emitted by some of the scanning machines in airports." 

"We simply do not truly know the risk of this radiation exposure over multiple screenings for frequent fliers, those in vulnerable groups, or TSA employees themselves who were operating these machines," Collins warned. 

It is unclear when the bill will be brought up for a vote.
