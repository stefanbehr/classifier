The answer depends on who you ask.The Indiana Democratic Party has combed through records going all the way back to Lugar's first year in the Senate, 1977. 

The hundreds of pages of itemized travel expenses were passed along to Fox News. Among the findings offered by the party is that the six-term US Senator has spend almost $50,000 in travel expenses just visiting his home state. 

The expenses are necessary, in part, because Lugar has no home in Indiana. The Indiana Senator lives in the Washington DC suburb of McLean, Virginia. So, virtually every time Lugar stays in Indiana there's an added cost."At the very least, Senator Lugar should be compelled to better explain the more than twenty years worth of taxpayer dollars used to fund his travel expenses in Indiana," says state Democratic party chairman Dan Parker. 

Another of the Indiana Democratic Party's findings puts a finer point on the issue. It says Lugar appears to have expensed 325 nights in Indiana over a 21-year period, from 1990 through 2011. 

The suggestion is that Lugar's time in the state has been limited to less than a year over the course of two-plus decades. The Lugar campaign says that suggestion is completely false."(Senator Lugar) spends a quarter of the year, every year in the state," says campaign spokesman David Wilkie. 

Wilkie notes from February 1st of 2011 and 2012, Lugar has spent 89-days in Indiana. And while Lugar is not "domiciled" in Indiana, he does own a farm in the state which he still manages. 

The issue of Lugar's time spent in Indiana is a sore one. 

Lugar's challenger in the May primary, State Treasurer Richard Mourdock has won over dozens of county Republican Party chairmen, some saying they've never even met the man who's been representing Indiana for 36-years. 

That's a sign of potential trouble for Lugar. Knowing and developing relationships with local party leaders is usually considered critical in statewide contests. 

Supporters say Lugar has developed his own web of supporters in every corner of the Indiana and they dismiss talk of an upset, either by Mourdock in the primary or by Democratic candidate Joe Donnelly in the November general election. Lugar's campaign is equally confident. 

"The entire state is his home," says Wilkie.
