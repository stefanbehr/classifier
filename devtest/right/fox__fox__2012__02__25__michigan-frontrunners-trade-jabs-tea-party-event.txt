In what's quickly becoming a contentious race, presidential candidates Mitt Romney and Rick Santorum traded barbs during separate speeches at the same Tea Party event Saturday. The back and forth is setting the stage for what will likely be a bitter battle for Michigan and the mantle of GOP front-runner. 

"It's absolutely laughable for a liberal Governor to suggest that I am not conservative," former Senator Santorum fumed this morning to a local Michigan chapter of Americans For Prosperity, a national tea party organization. "He repeatedly gets up and says all these things that he didn't do, that he did do. Folks, this is an issue of trust. Imagine what he's going to do in a general election." 

A few hours later, Romney fired back before the same audience, using Santorum's own words against him. 

"Senator Santorum was kind of enough to say on the Laura Ingraham show-- he said Mitt Romney , this is a guy who is really conservative and who we can trust," Romney said about Santorum endorsing him for President in 2008. "And when he came out and endorsed me he said these words - he said he is the clear conservative candidate. He's right. I'm the conservative candidate." 

And the jabs didn't stop there. 

The former Massachusetts Governor also challenged Santorum's conservative credentials, accusing him of not sticking to his principles, and for backing former Pennsylvania Senator Arlen Specter - a liberal Republican who crossed over and ran as a Democrat in 2010 - in his 1996 Presidential bid. 

"There were other conservatives running like Bob Dole-- he didn t support them. He supported the pro-choice candidate, Arlen Specter. 

This taking one for the team, that s business as usual in Washington. We have to have principled, conservative leadership and I have demonstrated that through my life and demonstrated it as a governor." 

Romney and Santorum are locked in a tight battle in Michigan. Santorum surged ahead in national polls after sweeping a trio of contest in early February, but recent polls have seen a Romney rebound. The state is especially important to Romney, who was born there and whose father George was a popular 3-term governor. 

*Written by Chris Laible and Nick Kalman
