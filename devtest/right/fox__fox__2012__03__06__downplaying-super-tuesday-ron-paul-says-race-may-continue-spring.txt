Ron Paul is downplaying the idea that any kind of finality should be associated with Super Tuesday , "We're going to find out something very special today, but this won't be the end all, this is a long term operation," the Texas Congressman told supporters on Tuesday. 

Paul made the remarks while campaigning in Idaho, one of the 10 states holding presidential primary elections or caucuses today. The libertarian leaning candidate also maintains that in some states, such as Maine and Nevada, which have already conducted straw polls and had him finishing in second or third place, he may actually end up winning the most delegates. "There are several where we may well win the delegation because we have a lot of organization and they're going to work their way through and they're going to go to the county convention and the state convention..." 

If Mitt Romney pulls out wins in the big prizes of Ohio and Tennessee tonight, along with his expected victories in Vermont, Massachusetts and Virginia, his supporters will be once again be pushing the image of the former Massachusetts Governor as the inevitable GOP nominee. 

There will also be many pushing the idea that the primary process has turned into an ugly fight that is hurting the Republican brand. 

However, Ron Paul says he is ready for the long haul , "...it's going to be May, I think before you really start picking or choosing success or lack of success." 

Fox News Producer Faith Mangan contributed to this report
