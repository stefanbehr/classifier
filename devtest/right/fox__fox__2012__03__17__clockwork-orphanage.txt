In time, it could have been so much more.The time has nothing to show. - "Time (Clock of the Heart)" by Culture Club 

On Capitol Hill , you'll find them jutting from the walls down long, ornate corridors. They're perched in hearing rooms. They adorn Congressional office suites. They're present in the press galleries. 

They're clocks. But these aren't typical temporal sentinels. These are special Congressional clocks, designed not only to tell time, but to communicate the parliamentary traffic on the House and Senate floors to the Capitol Hill community. 

And a sad irony surrounds these clocks. 

They may keep time. But time has passed them by. 

The clocks appear as though they were lifted straight from the set of "Mad Men." The numbers are embossed with a Gill Sans font, lacking serifs. The hands rotate around the dial, sweeping past each marker denoting the seconds between the numbers. Most of the clocks are crowned with a halo of seven, tiny, diodes which stretch between the ten and the two. A variation of the clocks post the lights on the odd numbers, with the seventh light spotted on the six. 

Sometimes the lights are illuminated. Sometimes they're not. Sometimes the clocks buzz and clang. And therein lies the mysterious cipher of the Congressional clocks. 

This is the "legislative call system," a schedule of lights and bells created to alert lawmakers when the House and Senate gavel into session, recess, adjourn for the day, vote and conduct quorum calls. 



Tourists inevitably ask Congressional regulars what odd lights and bells mean. Yet virtually no one on Capitol Hill has a clue how this demoded system works any more. 

Why would they? It's a construct before the internet. Before email and Twitter. Before cellphones and iPhones. Predating pagers. 

Capitol Hill is a big campus. A lawmaker or Congressional staff could find themselves deep in the bowels of the Rayburn House Office Building when a vote is called. A senator could be in his office on the fifth floor of Hart, nearly a quarter mile away from the Senate floor. So before the age of instant communication, the lights on the clocks and bells served as a Capitol Hill town crier, informing everyone what the House and Senate was doing. 

This practice seems odd today. When the House calls a vote, the Republican and Democratic floor staffs blast emails to massive distribution lists. The messages detail the vote cluster, what the votes are on and how long the votes will last. In fact, many offices have a TV or two turned to C-SPAN or the House floor feed. They see when the House launches a vote. That's when an aide gets the lawmaker and dispatches him or her to the floor. 

The Republican and Democratic cloakrooms began sending pages to lawmakers when pagers hit the scene in the 1980s. The nasally voice of a cloakroom staffer would echo around the marble hallways, dictating how long members had to report to the floor to cast their votes. 

But before these technological evolutions, only the clocks and bells were the only way to inform everyone about what was going on. 

In fact, the development of the special clocks was an enhancement of the bell alerts. 

Every high school in America has some sort of bell system. They ring to signal when school starts. What's the start of third period. When lunch is served. Engineers set up Capitol Hill like a high school, placing bells at strategic places around the campus. 

If you know the bell schematic, you can tell what's happening in the House and Senate chambers without even being there. 

The House and Senate are notoriously unique institutions, each steeped with their own histories and arcane traditions. So it should come as no surprise that the House and Senate bell systems are as different as the bodies themselves. And the meanings behind the bells and lights are as esoteric as Congressional procedure. 

In the Senate, a single bell indicates the beginning of a vote. Most votes in the Senate last for 15 minutes. Five bells signals the Senate is halfway through that vote. 

Six rings dictates that a parliamentary time of the day called "Morning Business" is ending. It could also mean that the Senate has recessed but is coming back. Four rings reveals the Senate is packing up for the day or beginning a long recess. 

Two rings is a quorum call. You hear the double ring in the Senate a lot. The Senate rarely executes a true quorum call (known as a "live" quorum call) where they actually require senators to come to the floor to establish whether they have enough people present to conduct business. Instead, most Senate quorum calls are simply time-outs. No one is seeking to speak on the Senate floor, but senators don't want to adjourn for the day. The Senate may also order a quorum call so floor leaders can huddle offstage to forge an agreement on votes or amendments. 

The House system is more complex. 

Two rings denotes a vote on the House floor. But only if it is an "electronically recorded vote." Again, that's antiquated. Nearly every vote conducted in the House these days is "electronic." Members come to the chamber with magnetized voting cards which they insert into machines stationed around the House floor. They then press buttons to record their votes. 

Prior to the electronic system, a House clerk called the names of all 435 members to determine how they were voting. It was utterly tedious. Just in case anyone wants to resurrect that anachronistic practice, listen for the bells to ring twice, pause and then ring twice again. 

Then listen for moans of agony emanating from the House floor as lawmakers summarily beat their colleague who demanded such an arduous vote. 

The two ring bell bursts are also complicated by the types of votes called on the floor. For instance, if the House rings the bells two times, then pauses and then rings the bells five times, the first vote is on a measure handled as a "suspension." A suspension vote means the House needs a supermajority to pass the bill or resolution. Two rings plus five rings also means the House is not just taking one vote, but beginning a cluster of votes. 

When the House adjourns, the bells clang four times. Six buzzes means the House is in recess. 

Now here's where the lights on the clocks come in: 

If you look at a clock on the House or Senate side when the bells sound, notice how the flashing diodes generally correspond to the rings. In other words, if the Senate recesses but intends to return to session later, you'll hear six bells and six lights will appear on the clocks. If the House bells sound five times and you spot five lights on a clock, they're in the middle of a five-minute vote. 

A seventh light on the House clock (often in a different color) signifies the House is in session. 

What you don't want to hear are 12 rings every two seconds and see six lights on the left of the clock face strobing. That's a "civil defense" warning. 

But again, like anyone would know what a "civil defense" warning is these days. Besides, they'd probably learn about it first on Twitter. And even if people did know what 12 bells and six lights on the left meant, would they scurry to the basement of the Capitol? That's the site of George Washington's tomb. Washington isn't buried there. But this location deep in the recesses of the Capitol features a faded, magenta and black "Fallout Shelter" sign. The aluminum strip dates back to the early 60s, complete with the three, inverted triangles ensconced inside a circle. Signs like these are still around. And are as antiquated as the Capitol's clock and bell systems. 

The clocks and bells remind me of a staple of rural life when I grew up in Ohio the 70s and '80s. The township's volunteer fire department was just down the road from our house. And periodically, a loud, long siren would begin to wail from a gigantic post positioned atop the fire house. Minutes later, a group of local neighbors would speed past our house in their cars en route the fire station. Moments after that, the pumper truck, the ambulance or water wagon would scream down the road with our neighbors at the wheel, now decked out in flame-retardant coats and firefighter helmets. 

The water wagons were necessary because many locations in the township didn't have fire plugs. They had to transport the water to the scene to fight a blaze. 

No siren bawls atop that fire station now to summon volunteer firefighters. They're now notified by radios, pagers, email and Twitter. 

But at the Capitol, the bells and flashing clocks live on, even as most lawmakers, aides and journalists get their information via email or off the TV screen. 

The special clocks and bells were once cutting-edge technology. But so were House and Senate pages. After all, the original purpose of Congressional pages was to bring lawmakers to the floor for votes. The House or Senate would call a vote and teams of teenaged pages roamed the halls (and often the local watering holes) to find members. 

Then the bells and clocks came. But it took until last year for the House eliminate its page program. The Senate page program still exists. 

In late 2010, House Speaker John Boehner (R-OH) commissioned Rep. Greg Walden (R-OR) to lead the House's transition between Democratic and Republican control. Walden recalled that when Republicans came to power in the House in 1994, former House Speaker Newt Gingrich (R-GA) wanted to demonstrate Congress was under new management. Gingrich aimed to eliminate waste and redundancy. And as a symbol of that, the Georgia Republican demanded that the House kick the bucket. 

The ice bucket, that is. 

The program existed to deliver buckets of ice to every House office each day. The delivery service employed nearly two dozen people and cost Congress $400,000 annually. The practice dated back to a period before refrigeration. Yet no one ever questioned why buckets of ice sat outside Congressional offices on sweltering July days, forming pools of water underneath. 

When Republicans against seized control of the House in November, 2010, Walden sought to emulate Gingrich's practices of bringing Congress into the 21st Century. 

"What's our bucket of ice?" Walden asked at the time. 

No one's discussed eliminating the specialized clocks which adorn the Capitol. But it's clear they are relics. And like the ice deliveries, some might argue it's a custom that's outlived its usefulness - especially if few can even interpret what the flashing lights on the clocks mean. 

Amid the email blasts, Tweets and C-SPAN, the clocks are curios from another time. Strays with no home. Yet they tick on, perhaps worthy of a clockwork orphanage.
