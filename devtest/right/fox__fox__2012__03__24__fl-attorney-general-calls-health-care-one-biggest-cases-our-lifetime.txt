"It's truly one of the biggest cases of our lifetime." That is how Florida Attorney General Pam Bondi (R) describes the lawsuit her state and 25 others filed that will be heard before the Supreme Court on Monday. 

The lawsuit was originally filed back in 2010 and has been ruled on by several lower courts. Bondi says that the case boils down to whether or not Congress has the power to force people to buy insurance. 

"If they can force us to do this, they could force us to do anything," Bondi told Fox News Saturday. 

"I can tell you that the Obama administration, they're going to try to focus on health care policy and that's not what it is about. This is about our rights of the United States Constitution," added Bondi. 

Bondi also says that she is surprised by the amount of time court has given them to make their case. 

"A modern day United States Supreme Court, they only give you an hour of oral argument. Here, we have been given six hours over three days." said Bondi. 

Bondi along with several other attorneys general will be in the Supreme Court to hear the arguments. A decision on the case is expected in late June.
