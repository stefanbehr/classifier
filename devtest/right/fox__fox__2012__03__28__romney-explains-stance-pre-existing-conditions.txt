Mitt Romney , under fire from Rick Santorum for allegedly crafting the precursor to ObamaCare in Massachusetts, explained Tuesday how his approach to overhauling health care at the federal level would be different than the president's. 

Romney, who adamantly opposes the federal health care law, said on " The Tonight Show " that he would want those who have been continuously insured to be able to get insurance going forward. 

But he added the security would not be extended to people who apply for insurance once they fall ill. 

"If they are 45 years old and they show up and say 'I want insurance because I have heart disease,' it's like, hey guys -- we can't play the game like that," Romney said. 

Romney spoke as the Supreme Court prepared to head into the third and final day of debate on the Obama health care law. 

Santorum has used the Supreme Court hearing as an opportunity to attack "RomneyCare," saying it is the "blueprint" on which ObamaCare is based. He says Romney can't fight Obama on the issue in the general election. 

Solicitor General Donald Verrilli, the federal government's attorney in the case, also told the justices that Congress saw how health care worked "in the state of Massachusetts" and that "it had every reason to think it could work on a national basis." 

The Massachusetts state-run health care system functions similar to ObamaCare in that both include employer-provided insurance and Medicare for seniors -- and seek to reduce the number of uninsured by expanding Medicaid. Also, they both include an individual mandate that requires people to buy insurance or pay a penalty. 

But Romney has said states should be free to craft their own health care solutions, and not be told what to do by Washington.
