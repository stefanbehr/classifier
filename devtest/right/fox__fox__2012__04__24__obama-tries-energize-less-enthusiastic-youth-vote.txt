BOULDER, Colorado - President Obama is trying to jazz up college students and younger voters as he travels on a two-day battleground state swing where he'll push preventing a federal student loan rate hike. 

A voting bloc that is notoriously unreliable, the president beat expectations of youth voters in 2008 and shared a large majority - two to one over rival Senator John McCain , R-Ariz - but is facing a harder sell to galvanize the youth vote this time. 

Recent data shows the rough job market for recent grads at play and they may be less likely to actually show up and vote. A recent Fox News poll show that the president's favorability among voters under 30 has dropped 5 percent while his unfavorability has gone up 9 percent. 

"In 2008 President Obama was a very unique candidate. He really captured the imagination of a lot of people who either had never been energized by politics or it had been a very long time before they had been excited about a candidate," said Nathan Gonzales of Rothenberg Political Report and Politicsinstereo.com founder. 

[N]ow that he's an incumbent, he's had to make some decisions that aren't necessarily popular, and he's also responsible for the direction of the country, some of that shine has worn off with younger voters," Gonzales added. 

The president is spending Tuesday and Wednesday in key political states he won last time, but still needs wins in - North Carolina, Colorado and Iowa. 

In all three speeches he's pushing for Congress to stop a student loan rates from doubling and jumping to 6.8 percent on July 1. It's actually an issue that presumptive nominee Mitt Romney on Monday said he agrees with the president. White House Press Secretary Jay Carney when asked for reaction on that, said they would take all the support they can get. 

"That's basically a tax hike for more than seven million Americans," Obama said Tuesday. 

The president played off the crowd in his first speech at University of Chapel Hill in Raleigh-Durham. where he interjected lines against rising student loans with lines like "can I get an AMEN?!" 

While in North Carolina, Obama continued the pitch to younger voters by also taping an appearance with late-night comedian Jimmy Fallon . 

Obama's also continuing to frame himself as the one who's in touch with the middle class, adding on to his long-time campaign strategy to contrast to Republicans and what he calls are two-competing visions for America. 

"I didn't just get some talking points about this I didn't just get a policy briefing on this. Michelle and I - we've been in your shoes, like I said - we didn't come from wealthy families," Obama said. 

Some recent lines in speeches had some political observers saying the president's doing a general-election positioning that he's trying to say he's more of a regular guy, standing up for the average folks, and Republicans and Romney are rich, privileged and therefore out of touch.Republicans charged he was again using taxpayer dollars for what they see as campaign stops and not official visits. They were also quick to circulate and note that Obama as a senator in 2007 was on the campaign trail and actually missed two key votes on the college cost reduction bill that he's now demanding Congress to extend. 

Monday Romney slammed Obama's job performance on jobs, also trying to appeal to younger voters, "Particularly with the number of college graduates that can't find work, or that can only find work well beneath their skill level, I fully support the effort to extend the low interest rate on student loans," Romney said. 

White House aides noted even though Romney may support keeping the rates low, he also backs Rep. Paul Ryan's budget, that locks in higher rates.Tuesday is also a primary day, where Romney will look to nail down more delegates and get a step closer to securing his standing as the nominee, at least mathematically. 

It also means both sides are now more in general election mode, instead of just appealing to their bases. 

"I think the Obama campaign has realized for months that Mitt Romney would be their opponent. I think they were probably dreaming and wishing they had one of the other Republicans, but it's no surprise to them and now it's just about finding the right message against him that they're going to use for the next five months," Gonzales said. 

Fox News' Chief White House Correspondent Ed Henry contributed to this report.
