The Obama re-election team is distancing the president from comments made by a key surrogate on Tuesday. 

During a cable television interview Rep. Jim Clyburn , a Democrat from South Carolina, said "There's something about raping companies and leaving them in debt and setting up Swiss bank accounts and corporate businesses in the Grand Caymans. I have a serious problem with that." 

The Obama campaign's response to Clyburn's comment was terse. "We strongly disagree with Congressman Clyburn's choice of words- they have no place in this conversation," said Obama Campaign spokeswoman Lis Smith. 

However the Obama Camp isn't backing off the Bain Capital line of attack. Smith added, "But we do believe that Mitt Romney should come clean about his record as a corporate buyout specialist and how, contrary to his claims of creating jobs, his focus was on reaping quick profits for investors at the expense of workers and middle class families." 

Private equity and Mitt Romney's time as the CEO of Bain Capital has been dominating the presidential campaign trail lately. On Monday President Obama defended attacks on Mr. Romney's time at Bain Capital, saying the subject "is not a distraction. This is part of the debate that we're going to be having this election campaign."
