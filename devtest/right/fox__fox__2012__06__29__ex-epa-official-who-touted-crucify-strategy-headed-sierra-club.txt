The former Environmental Protection Agency regional administrator who once said that his regulatory enforcement philosophy was to "crucify" and "make an example of" non-compliant companies has taken a job with the Sierra Club. 

Al Armendariz quickly resigned from the EPA after his videotaped comments were made public. He will now work as part of Sierra Club's "Beyond Coal" campaign based in Texas. 

The group says Armendariz's experience as an environmental scientist will help, "move Texas, Oklahoma, and Arkansas off coal-fired electricity and toward an economy powered by job-generating clean energy sources such as wind and the sun." 

Sen. Jim Inhofe , R-Okla, whose office first publicized the "crucify" comments released a tongue-in-cheek statement congratulating Armendariz. 

"At least at the Sierra Club he won't get into so much trouble for telling the truth that their true agenda is to kill oil, gas and coal," Inhofe said, "I was however, surprised not to have been asked to provide a reference -- I would have been happy to tell the Sierra Club about his steadfast commitment to regulating fossil fuels out of existence." 

Armendariz skipped a House hearing where he was scheduled to testify in June in favor of a meeting at the Sierra Club office in Washington, D.C.
