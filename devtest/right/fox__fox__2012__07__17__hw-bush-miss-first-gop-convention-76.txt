When Republicans gather in Tampa for their convention in late August, former President George H.W. Bush will not be joining them -- making it the first convention he's missed since 1976. 

To be clear, the former president is still supporting Mitt Romney , but it has gotten more difficult for him to move around. 

"President and Mrs. Bush are more convinced than ever today that we need to get behind Mitt and Ann Romney to turn this country around, but sadly the situation around President Bush's mobility will prevent them from attending the Tampa Convention," Bush spokesman Jim McGrath told Fox News. 

In a recent PARADE magazine interview, Bush spoke about his medical condition. "They call it vascular Parkinsonism. It just affects the legs. It's not painful. You tell your legs to move and they don't move. It's strange, but if you have to have some bad-sounding disease, this is a good one to get," he told presidential historian Mark Updegrove. 

Even at 88 years old, the former president says it is tough to accept. " It is hard, because I love being active, (playing) sports, being in the game. ... But you just face the reality and make the best of it."
