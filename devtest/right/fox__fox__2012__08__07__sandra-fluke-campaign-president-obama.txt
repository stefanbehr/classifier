Sandra Fluke , the Georgetown law student who became the epicenter of a political firestorm regarding federally mandated contraception coverage, will introduce President Obama during his visit to Denver Wednesday, a campaign official confirms. 

Fluke became a public figure when Rush Limbaugh called her a "slut" and "prostitute" after she testified before Congress supporting the administration's position that insurance companies should be federally required to cover contraception costs; Limbaugh's choice of language, which he later apologized for , escalated the national debate over the Obama Administration's so-called contraception mandate. 

Democrats seized Limbaugh's language to accuse Republicans of engaging in an ongoing "war on women" while critics of the mandate, including Catholic University of America, charged the White House with violating religious liberty. 

The Obama campaign has been actively courting the women's vote and Fluke's appearance with the president is part of a continuing effort to depict Romney as "out of touch" with women. Fluke has written an op-ed endorsing the president's re-election.
