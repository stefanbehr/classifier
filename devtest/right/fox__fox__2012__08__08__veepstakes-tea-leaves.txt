What can you read into a Wikipedia update? In the world of presidential politics, apparently quite a bit. 

Tech President, a website that describes itself as a "hub for the conversation between political practitioners and technologists" notes that there's a possible link between the number of updates to the Wikipedia page of potential GOP presidential running mate and the VP eventual pick. 

The Tech President data comes from Cyveillance, an Arlington, Virginia based Internet monitoring company that found multiple changes were made to the pages of Sarah Palin and Joe Biden in the days prior to their being announced as VP candidates in 2008. 

Who Mitt Romney will choose as a running mate is one of the best kept secrets in this presidential campaign and the Wikipedia count adds to the so-called 'Veepstakes' intrigue. 

IF (and that's a mighty big if) the hypothesis held true then freshman Republican Senator Kelly Ayotte of New Hampshire leads the pack of presumptive nominees with 153 total updates to her page. Senator Marco Rubio of Florida has 100, Senator Rob Portman of Ohio has 81 and Congressman Paul Ryan of Wisconsin has 57. Former Minnesota Governor Tim Pawlenty , often mentioned as a front-runner has just 18. 

The data had a chance be skewed - especially after Stephen Colbert told his audience to get involved in the process. 

"We could be looking at Vice President Season Six of Buffy the Vampire Slayer. So, Nation, let your voice be heard in this history decision. Go on Wikipedia and make as many edits as possible to your favorite VP contender." 

But Wikipedia tightened security on the accounts of several potential candidates Wednesday, including former Secretary of State Condoleezza Rice and Rep. Paul Ryan, R-Wis., by updating their pages to a semi-protected mode. 

Wikipedia is based on an open platform that allows anonymous users to edit and update pages, but in what the website calls "limited cases" editing is restricted to prevent disruption or vandalism. 

Tim Pawlenty has reportedly locked his site to protect against such shenanigans and according to Tech President, Portman and Rubio have "semi-protected" their sites. Tech President Editorial Director Micah Sifry told FOX News it's not unusual for Wikipedia to limit access to registered users, those who have contributed in the past and have made multiple edits. 

"The game is over," he said with regards to looking at Wikipedia numbers for VP insight, adding "I guess we all just pushed the needle further into the haystack." 

For his part Mitt Romney is keeping his decision a secret, directing media and voters to download an app called "Who Will Be Mitt's VP." That's where the campaign says Romney's running mate will be revealed. 

The latest from Romney himself came this past weekend, when he told reporters he will absolutely decide and announce his running mate before the third night of the Republican convention, which is traditionally the night the vice presidential candidate addresses the delegates.
