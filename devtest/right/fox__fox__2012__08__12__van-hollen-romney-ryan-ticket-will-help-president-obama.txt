Now that Mitt Romney has chosen Rep. Paul Ryan , R-Wis., to join his presidential ticket, the ranking Democrat on the House Budget Committee says Americans will have a clear, fundamental choice in November. 

"This pick will sharpen the choice in the election," Rep. Chris Van Hollen, D-Md., told "America's News Headquarters." "It will sharpen it in a way that will help the president. But I do think we are in for a very good debate." 

Van Hollen is Ryan's Democratic counterpart on the House Budget Committee. He says his fellow Democrats are not opposed to tax reform, but it depends on the type of tax reform. 

Gov. Romney and Rep. Ryan "say they will do (tax reform) in deficit neutral ways. That means you have to remove a lot of the deductions. A lot of the deductions help middle income taxpayers, mortgage interest and the special treatment of health care costs," he said. 

Van Hollen suggested the Romney-Ryan budget would put a chokehold on the middle class and hit seniors like a hammer, saying, "In the Ryan-Romney Medicare plan, as health care costs rise, seniors will actually not get support from medicare that keeps pace." 

Van Hollen says only the wealthy benefit. 

"If you are dropping the top rate from 35 percent to 25 percent ... great for folks at the high end of the income scale. But they are making up for it by increasing the burden on middle class taxpayers," he said. 

While stumping in North Carolina on Sunday, Romney and Ryan highlighted their own economic message, saying the president's " 'hope and change' has turned to attack and blame.' " 

Ryan says it's the threat of tax increases, Obamacare' and choking red tape that is stifling job creation. 

He also says, "We owe you a choice. Stay on a path: nation in debt, doubt, despair, or we can change this thing and get this country back on the right track." 

-- Regina Bratton
