The problem is not in your set. 

But television viewers can't be faulted if they mistakenly thought C-SPAN looks a lot like ESPN. 

That's because Congress sure burns a lot of time digging around in professional and collegiate sports. 

To wit: 

The acquittal of Roger Clemens on charges that he lied to lawmakers about using steroids. Alleged oping in horseracing. Questionable boxing matches. The Bowl Championship Series. Bounties in the National Football League. 

Cutting spending? Restoring jobs? Syria? Yeah, Congress wrestles with those issues, too. But it seems as though the world of sports owns box seats on Capitol Hill. 

It's always been that way. 

When it comes to sports, Congress has historically spent the most time on the national pastime. That may only be fitting. As far back as the 1890s, there were debates in Congress about whether the Sherman Antitrust Act applied to professional baseball. A landmark 1922 Supreme Court case ruled that baseball was exempt from antitrust rules as the sport was not "interstate commerce." And since then, Congress periodically threatens to revoke baseball's antitrust exemption when things go awry. Such was the case when Congress investigated steroid use in the game in 2005 and during the 1994 labor stoppage which cancelled the World Series. 



The High Court reaffirmed the antitrust exemption in the 1953 case Toolson v. New York Yankees . Justices determined that Congress's lack of action in applying antitrust statutes equally granted the game a special status. 

In 1998, both Major League Baseball and the players successfully petitioned Congress to rescind the antitrust exemption when it came to labor laws. 

President Bill Clinton signed the Curt Flood Act of 1998 into law. It was an homage to all-star St. Louis Cardinals outfielder Curt Flood who challenged the game's reserve clause. That provision bound players to their teams unless they were traded. The late outfielder lost at the Supreme Court. But Flood's maneuver opened the door to free agency a few years later. 

Free agency then changed pro sports forever. 

When Flood's case reached the Supreme Court, the citation was Flood v. Kuhn . "Kuhn" referred to Bowie Kuhn, who was the Major League Baseball Commissioner. 

But baseball didn't always have a commissioner. In fact the creation of a commissioner partly stems from Congressional pressure. 

The 1919 "Black Sox" scandal was a pivotal moment in American sports. Eight Chicago White Sox players testified that they threw games to the eventual champion Cincinnati Reds. Congress threatened to conduct an inquiry and hold more sway over the game. But to stave off Congressional intervention, team owners hired federal judge Kennesaw "Mountain" Landis to become its "commissioner." Landis expelled the Chicago players in question. 

But the creation of a commissioner didn't fully satisfy lawmakers. Rep. Benjamin Welty (D-OH) questioned whether it was appropriate for Landis to continue as a judge and simultaneously serve as baseball commissioner. The House Judiciary Committee launched a hearing. 

"What is to prevent (owners) from going into the Supreme Court now and hiring every member on the bench?" asked Welty at the 1921 hearing. 

Lawmakers tried to ban federal judges from holding other jobs. But the measure failed. Nonetheless, Landis stepped down from the bench and continued as commissioner. 

Baseball had no commissioner in 1994 as the game was on the precipice of an historic labor dispute. Fay Vincent resigned in 1992, triggering the interregnum. 

Then Sen. Connie Mack III (R-FL), the father of current Rep. Connie Mack IV (R-FL) and grandson of legendary Philadelphia Athletics' manager Connie Mack, had some choice words for Major League Baseball in the early 1990s. The Tampa-St. Petersburg area tried to lure either the White Sox or the San Francisco Giants to its new domed stadium (now called Tropicana Field and home of the Tampa Bay Rays). 

"I think (the failure to name a commissioner) just adds some momentum to what we're trying to do. There is a clear sense of defiance on their part," said Mack. 

A few months later, the Senate Judiciary Committee, chaired no less by then Sen. Joe Biden (D-DE), held a field hearing in St. Petersburg about the absence of a permanent commissioner. Then-interim commissioner Bud Selig, who at the time also owned the Milwaukee Brewers, tried to placate skeptical senators. Late Ohio Sen. Howard Metzenbaum (D-OH) then characterized most of what Selig had to say as "BS." 

But that wasn't all from Metzenbaum. 

"Congress should move to reclaim our national pastime for the fans before the barons of baseball become too cozy, too comfortable and too cocky," lectured the Ohio Democrat. 

The hearing so dismayed the late Sen. Daniel Patrick Moynihan (D-NY) that he promised to introduce a bill to repeal the antitrust exemption. 

The House Oversight Committee planned an inquest into steroid use in baseball in March, 2005. The threat was that Congress may strip the antitrust shelter if baseball didn't shape up. 

Former House counsel Stanley Brand described the effort as "an absolutely excessive and unprecedented misuse of Congressional power." 

But lawmakers disagreed. 

"I don't want to say it's something we're going to hold over their heads," said Rep. Lynn Westmoreland (R-GA) at the time. "But if it's something they're aware of and it's something you're aware of, it might start affecting your position. It might be time to review why they're exempt from antirust laws." 

President George W. Bush, the former owner of the Texas Rangers, even mentioned steroid use in Major League Baseball during his State of the Union address that year. 

Selig initially declined to appear at the hearing and then changed his mind. The House Oversight Committee asked all-stars Sammy Sosa, Mark McGwire, Rafael Palmeiro, Curt Schilling, Frank Thomas and Jason Giambi to testify. For a while, it appeared that former star Jose Canseco and Schilling were the only ones who would attend. Attorneys for Palmeiro argued that "to require that (their client) come to answer baseless charges is unfair." 

At the hearing, Palmeiro shook his finger at lawmakers and declared "Let me start by telling you this: I have never used steroids, period. I don't know how to say it any more clearly than that. Never." 

Later that summer, Palmeiro became the first big star to fail a doping test. Major League Baseball suspended Palmeiro ten games and lawmakers pondered whether he lied under oath. 

Roger Clemens wasn't so fortunate. 

After House investigators privately deposed Clemens in 2008, the pitcher requested an open Congressional hearing. That's where lawmakers felt Clemens lied and referred him to the Justice Department for prosecution. A jury cleared Clemens on all counts in June. 

Six television satellite trucks parked along Pennsylvania Avenue outside the E. Barrett Prettyman Courthouse in shadow of the U.S. Capitol for the trial. Reporters waited daily to learn Clemens' fate. 

The jury exonerated Clemens who choked up as he addressed onlookers and the press just outside the courthouse. In fact, Clemens had to sprint to make it to the courtroom when the verdict came down. He was off playing catch with his sons near the Washington Monument. They were about to drive to Virginia to hit the batting cages. 

By the time the case worked its way through the courts, it was well out of the hands of Rep. Henry Waxman (D-CA), one of the lawmakers who referred Clemens to the Justice Department. 

"We had significant doubts about the truthfulness of (Clemens) testimony in 2008," said Waxman. "The decision whether Mr. Clemens committed perjury is a decision the jury had to make and I respect its decision." 

But the Clemens verdict didn't halt lawmakers from immediately tinkering with another professional sport. 

Senate Majority Leader Harry Reid (D-NV) and Sen. John McCain (R-AZ) introduced a bill this summer to create a regulatory authority to oversee boxing. This came after a controversial, split decision which awarded Timothy Bradley a victory over Manny Pacquaio. Viewers of the Bradley/Pacquaio match in Las Vegas presumed Pacquaio won, extending his seven-year-long winning streak. But that wasn't the case. 

Congress has nibbled around the edges of boxing since 1960. The late Sen. Estes Kefauver (D-TN) held a series of antitrust hearings on the issue. The legendary 1982 bout between Ray "Boom Boom" Mancini and Duk Koo Kim sparked additional Congressional interest after Kim died from brain injuries after the match. Then-Reps. Jim Florio (D-NJ), Pat Williams (D-MT) and Bill Richardson (D-NM) introduced a bill to create a federal boxing agency. 

President Bill Clinton singed the Professional Boxing Safety Act into law in 1996. It increased oversight of the industry and bolstered assistance for state boxing commissions. 

Muhammad Ali testified before the Senate Commerce Committee in 1999 in favor of a McCain bill to reform the boxing contract system. 

When it comes to sports, safety seems to pique the interest of lawmakers the most. 

NFL Commissioner Roger Goodell told a House panel in 2009 that the league was "changing the culture of our game for the better." But that was before former superstars Junior Seau and Dave Duerson committed suicide earlier this year. 

Their deaths coupled with news that the New Orleans Saints placed bounties on the heads of opposing players. Senate Majority Whip Dick Durbin (D-IL) threatened to convene a hearing on the issue until Goodell crafted a series of reforms to eliminate bounties. Bounties were banned in the players handbook. Plus, the league set up an anonymous hotline where players could call to report bounties. 

Goodell chatted with Durbin at the Capitol in May, pledging to take serious steps. 

"Bounties are not a part of football. The integrity of the game and the safety of players is paramount," Goodell said. 

Around the same time as the Goodell-Durbin conclave, Congress met the call to the post in horseracing. I'll Have Another won the first two legs of the Triple Crown. He then lost his shot at becoming the first Triple Crown winner in 34 years when he withdrew from the Belmont Stakes due to tendonitis. 

Questions swirled around I'll Have Another's trainer Doug O'Neill. O'Neill attracted scrutiny for 14 steroid violations in 14 states during his career. The New York Times indicated that O'Neill's horses were twice as likely to become injured as other thoroughbreds. 

Rep. Ed Whitfield (R-KY) considered revamping a bill to increase penalties for doping in horse racing. Whitfield originally introduced a package in 2011 alongside Sen. Tom Udall (D-NM) to authorize race-day medication standards and sanctions for repeat offenders. 

In July, Senate Commerce Committee Chairman Jay Rockefeller (D-WV) convened a hearing that studied performance enhancing drugs in horseracing. 

Horseracing dominates the spring sports calendar. And late August means many sports fan turn their attention to college football. Some fans are ecstatic that schools are trashing the Bowl Championship Series (BCS) in 2014 in favor of a limited playoff system. 

"Call it the BS System," admonished Rep. Joe Barton (R-TX) at a 2009 House hearing on the BCS, later characterizing the BCS as "communism." 

The criticism was that "elite" conferences like the Big Ten, ACC, Big East, Big 12, Pac-12 and SEC were guaranteed slots if they finished high in the BCS rankings. The BCS defended the system, saying schools playing in those conferences were traditional football powers. 

"Using that logic, Delaware, which is the first state in the nation, should have 50 votes in the House," reasoned Barton. 

Calls for a playoff intensified after Florida defeated Oklahoma in 2009 for the national title. Both schools lost but once all year. Meantime, Southern California wasn't in contention despite having lost only once. And Utah was shut out despite being undefeated. As a result, Sen. Orrin Hatch (R-UT) asked the Justice Department to investigate the BCS. 

The Summer Olympics just wrapped up in London. So there's chatter that Congress should waive taxes assessed on medals earned by America's Olympians. 

And expect this debate to pre-occupy Congress - until another crisis threatens to paralyze the sports world. 

Which will inevitably invite yet another round of Congressional scrutiny.
