Following the release of Friday's jobs report, Republican presidential nominee Mitt Romney spent two days insisting the figures didn t tell the entire story. 

The unemployment rate dropped in September to its lowest level since President Obama took office, 7.8 percent, after peaking at 10 percent in 2009. 

But while explaining the sudden decrease, Romney seemed to go further Saturday night, insinuating the drop was due to a difference in accounting. 

If we calculated, by the way, our unemployment rate, in a way that was consistent with the way it was calculated when he came into office it would be a different number, Romney told a crowd of more than 6,500 in Apopka, Florida , a conservative section of the state. 

You see if the number of people if the percentage of the American population who were in the workforce were the same today as the day he was elected, our unemployment rate would be above 11 percent, he continued. 

Romney was referring to the labor force participation rate, the number of people either employed or looking for a job, which has steadily declined over the last decade. 

He argued there are millions of people who have given up looking for jobs that are not factored into the labor force participation rate, as people who become disillusioned or do not actively look for work within the previous four weeks are not counted. 

While this is a contributing factor, economists attribute about half of the decrease to demographic patterns, like the Baby Boomer population approaching retirement age. 

But the labor force participation rate is not incorporated into the monthly unemployment figures, nor has it been since before Obama took office four years ago. And the rate actually ticked up last month, as unemployment fell 0.3 percent. 

Some conservatives, including former General Electric CEO Jack Welch and Congressman Allen West, who campaigned with Romney Sunday in Port St Lucie, have cried foul on the latest statistics, saying the Bureau of Labor Statistics, a famously independent and secretive department, is conspiring to re-elect President Obama 

On Friday, Welch set off a digital maelstrom after tweeting, "Unbelievable jobs numbers.. these Chicago guys will do anything.. can't debate so change numbers." 

The Romney campaign has so-far denied these theories, but Romney s comments Saturday night could be seen as stoking reaction from this group, hoping their anger would turn into votes on November 6th. 

When asked if Romney was giving credence to the theorists, a senior adviser denied it, saying he was simply emphasizing that American workers have suffered under President Obama, and if as many people were working today as when he took office, the unemployment rate would be close to 11 percent. 

Asked for comment, the Obama campaign did not respond to repeated e-mail requests. But Secretary of Labor Hilda Solis has called accusations that members of the Bureau of Labor Statistics manipulated the numbers ludicrous , calling the economists tasked with calculating the figure our best trained and best skilled employees.
