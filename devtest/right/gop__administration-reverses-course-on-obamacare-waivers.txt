Administration Reverses Course On ObamaCare Waivers 





Since ObamaCare Was Signed Into Law, 1,433 Waivers Have Been Granted.  "As of the end of May 2011, a total of 1,433 one-year waivers have been granted.  This update includes 62 new approvals."  (Helping Americans Keep The Coverage They Have And Promoting Transparency,"  HHS.gov , Accessed 6/20/11) "The Number Of Enrollees In Plans With Annual Limits Waivers Is 3.2 Million..."  (Helping Americans Keep The Coverage They Have And Promoting Transparency,"  HHS.gov , Accessed 6/20/11) 

Obama Administration To Stop Granting New Waivers To ObamaCare In September.   "The Obama administration on Friday said it would stop granting new waivers to the health-care overhaul in September following sharp opposition from Republicans who cited the waivers in their bit to undermine the law." (Janet Adamy, "No New Health-Law Waivers To Be Given" ,  The  Wall Street Journal ,  6/18/11) 

By Ending Waivers, Obama Administration Is Hoping To Avoid The Public Criticism It Received Each Time New Waivers Were Issued.   "By cutting off applications, the administration will avoid bursts of attention each time it granted a new batch.  Opponents of the law contended that the administration had shown favoritism in granting the waivers, prompting federal health officials to disclose the names of recipients and the application process for granting them."  (Janet Adamy, "No New Health-Law Waivers To Be Given , "   The  Wall Street Journal ,  6/18/11) 

1.5 Million Union Workers Have Plans That Received Waivers From ObamaCare.  ("Helping Americans Keep the Coverage They Have and Promoting Transparency,"  HHS.gov , Accessed 5/16/11)   Unions Say They Need Relief From ObamaCare So That Employees Can Keep Their Coverage.  "Some of its chapters have obtained waivers, the union concedes, but notes the waivers were anticipated by Democrats who passed the law. 'The waiver process is a key part of healthcare reform because it helps ensure that workers won't lose their employer-provided health coverage,' SEIU states on its website." (Alexander Bolton, "SEIU Fights Healthcare Repeal After Obtaining Waivers From Law,"  The Hill ,  2/2/11)
