Cost Of Auto Bailouts Jumps Over 50 Percent 



In A New Report, The Treasury Department Reveals It Will Lose Billions More In The Auto Bailouts Than Originally Projected 

The Government Now Expects To Lose $23.6 Billion In The Auto Bailouts, $9 Billion More Than Its Previous Estimate, An Increase Of Over 50 Percent.  "The Treasury Department dramatically boosted its estimate of losses from its $85 billion auto industry bailout by more than $9 billion in the face of General Motors Co. s steep stock decline. In its monthly report to Congress, the Treasury Department now says it expects to lose $23.6 billion, up from its previous estimate of $14.33 billion." (David Shepardson, "U.S. Boosts Estimate Of Auto Bailout Losses To $23.6B,"  The Detroit News , 11/14/11) The Total Cost Of The Auto Bailouts Now Totals $79.6 Billion.  "The Treasury now pegs the cost of the bailout of GM, Chrysler Group LLC and the auto finance companies at $79.6 billion. It no longer includes $5 billion it set aside to guarantee payments to auto suppliers in 2009." (David Shepardson, "U.S. Boosts Estimate Of Auto Bailout Losses To $23.6B,"  The Detroit News , 11/14/11) 

In 2010 Obama Said American Taxpayers Would Recover More Than The Total Cost Of The Bailouts 

President Obama Said, We Are Going To Get Back All The Money That We Invested In Those Car Companies. (ABC's, " The View ," 7/28/10) 



In 2010, President Obama Said "American Taxpayers Are Now Positioned To Recover More Than My Administration Invested In GM."  OBAMA:   "General Motors relaunched itself as a public company, cutting the government's stake in the company by nearly half.  What's more, American taxpayers are now positioned to recover more than my administration invested in GM." (President Barack Obama, Remarks On The United States Auto Industry,  WhiteHouse.gov , 11/18/10) 

Obama Is Trying To Make The Bailouts A Core Message Of His Campaign But His Wild Claims Have Left Him Compared To "A Used Car Salesman" 

Obama Is Trying To "Capitalize On The Bailout To Gain Support For His Re-election Bid."  "The Democratic president has tried to capitalize on the bailout to gain support for his re-election bid, recently visiting a GM factory just a short distance from the university where the debate took place." (Kathy Barks Hoffman, "Presidential Debate Brings Little Focus On Mich.,"  The Associated Press , 11/10/11) Obama Has Argued That The "The Investment Paid Off."  "The administration and President Barack Obama have argued that any losses on the auto bailout were worth the hundreds of thousands of jobs saved. 'The investment paid off. The hundreds of thousands of jobs that have been saved made it worth it,' he said at an appearance last month at GM s Orion Assembly plant." (David Shepardson, "U.S. Boosts Estimate Of Auto Bailout Losses To $23.6B,"  The Detroit News , 11/14/11) 

"Michigan Is Anything But In The Bag For Obama."  "If there is one state in the union President Obama might think is secure for him in next year s election, it would be Michigan. After all, he led the tax-payer assisted bankruptcy reorganizations of General Motors and Chrysler, and turned Michigan from a complete economic basket-case to one of the top job creators in the country. But Michigan is anything but in the bag for Obama." (David Kiley, "Michigan GOP Debate: A Swing State Despite Obama's Auto Bailout,"  The Huffington Post , 11/9/11) 

The Washington Post 's Fact Checker:   "Virtually Every Claim By The President Regarding The Auto Industry Needs An Asterisk, Just Like The Fine Print In That Too-Good-To-Be-True Car Loan."  (Glenn Kessler, "President Obama's Phony Accounting On The Auto Industry Bailout,"  The Washington Post 's Fact Checker , 6/7/11) The Washington Post 's Fact Checker Gave President Obama's Claims On The Chrysler Bailout THREE Pinocchios.  (Glenn Kessler, "President Obama's Phony Accounting On The Auto Industry Bailout,"  The Washington Post 's Fact Checker , 6/7/11)   

FactCheck.org : The President Is "Sounding Very Much Like A Used Car Salesman" When He Describes The Success Of The Auto Bailouts.  "Notice the president -- sounding very much like a used-car salesman -- used the phrases 'during my watch' and 'under my watch' when describing the TARP loans as being 'completely repaid.'" (Eugene Kelly, "Chrysler Paid In Full?"  FactCheck.org , 6/6/11) FactCheck.org : "Read The Fine Print" The Obama Administration Is Not Getting Back $1.3 Billion It Gave To Chrysler.  "Taxpayer beware: You have to read the fine print to know what the president means when he says Chrysler has paid back 'every dime' of loans it received  'during my watch.'  The company got $12.5 billion in bailout funds under the Bush and Obama administrations, but -- despite what the president said -- isn t expected to pay about $1.3 billion of it." (Eugene Kelly, "Chrysler Paid In Full?,"  FactCheck.org , 6/6/11) The Value Of GM's Stock Would Have To "Greatly Exceed Levels It Had Traded At Before The Company's Bankruptcy ... For Taxpayers To Make A Profit."  "'Even if GM goes public again, the value of its stock would have to greatly exceed levels it had traded at before the company s bankruptcy in order for the stock to be worth enough for taxpayers to make a profit." (Chris Isidore, "GM CEO Promises Profit For Taxpayers,"  CNN Money , 1/11/10) 

 
