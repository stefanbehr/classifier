Durbin Put On Spot Over Democrat Budget Dodges 



On "Fox News Sunday" this morning, Senate Majority Whip Dick Durbin (D-IL) was put on the spot over past Democrat dodges over the budget and debt ceiling. Check out the video below: 

Durbin Tries To Deflect Question On Why Dems Haven't Passed Budget 





  

FOX NEWS' BRET BAIER : "Senator Durbin, why haven t the Senate Democrats passed a budget?" 

SEN. DICK DURBIN:   "It s called 60 votes. What it boils down to is this: we have 53 Democrats. But I can tell you when we get through all the procedural tangles that we face getting through this budget resolution, it is not just a matter of finding some agreement, but getting it executed on the floor. The point I want to make is this. This, as I understand it, the negotiation we re talking about will include some budget targets for at least the next fiscal year. So, we won t revisit this kind of crisis politics when it comes to next year." 

Durbin Tries To Deflect Question On Why Dems Dodged Debt Ceiling In December 

  



  

BAIER: "Let me go back to 2010 quickly, Senator Durbin. Senator Majority Leader Harry Reid had many opportunities to raise the debt ceiling with a Democratic Senate, a Democratic House and a Democratic president at the end of 2010, when it would have been sure to pass. He was blunt that really what it was about was politics. Here is what he said in December 2010 about raising the debt ceiling: 

SEN MAJORITY LEADER HARRY REID (VIDEO): "Once the Republicans have buy-in on the debt, they will have a majority in the House, they will have some sort of a a buy-in on the debt. It shouldn t be a heavily Democratic Senate, heavily Democratic House and Democrat president." 

BAIER : "So, was that a mistake?" 

DURBIN : "Well, I can tell you it reflected the reality of the situation. Bret let me tell you, give you an example of what we face. Many of the most conservative members of Congress on the Republican side, House and Senate, have been urging us to stay in Afghanistan, to continue to spend $10 billion a month. We know we borrow 40 cents for every dollar we spend. So it means $4 billion of the $10 billion each month they want us to spend needs to be borrowed. Now when the president comes in and says here is the debt ceiling, now we have to borrow the money to continue the war. That s one example that you re in favor of. They re saying we re not going to touch it. We re opposing the debt ceiling. What Harry Reid is saying is if you're going to stand for policies that spend the money, for goodness sake, responsibly stand up and vote for the extension of the debt ceiling." 

BAIER: "Sure, but when you are upset about the situation you re in now and look back to December 2010, you could have taken care of it then." 

DURBIN:   "Maybe. I don t know if that could have been part of the grand deal that extended the tax cuts across America and unemployment compensation benefits. That's Monday morning quarterbacking."
