In Lieu of Gift, Send Social Security Check 



After telling CBS News that seniors may not get their Social Security checks on August 3 rd , Obama has certainly found time to send invitations to his big August 3 rd  birthday fundraising bash. Will Obama be collecting campaign checks while threatening seniors with the loss of their Social Security checks? If so, America's seniors might not be in the partying mood as Obama hobnobs with big campaign donors in celebration of his birthday. Could he look more out-of-touch? 

CAMPAIGN CHECKS: 

Aug. 3 birthday fund-raiser. President Obama returns to Chicago on  Aug. 3  to mark his 50th birthday with fund-raisers at the Aragon Ballroom, with tickets ranging from $50 a person to $35,800 per couple, which includes VIP seating at a Birthday Concert where celebs will be performing and a dinner with the president. ( Chicago Sun Times ,  7/13/11) 

  

NO SOCIAL SECURITY CHECKS: 

Mr. Obama:  I cannot guarantee that those checks go out on  August 3rd  if we haven t resolved this issue. Because there may simply not be the money in the coffers to do it. ( CBS Evening News , 7/12/11) 



###
