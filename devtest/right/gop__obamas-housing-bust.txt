Obama's Housing Bust 



PROMISE: President Obama Promised That His Housing Program Would Prevent 7 to 9 Million Families From Foreclosure.  "And we will pursue the housing plan I m outlining today. And through this plan, we will help between 7 and 9 million families restructure or refinance their mortgages so they can afford--avoid foreclosure." (President Barack Obama,  Remarks On The Home Mortgage Industry In Mesa, Arizona , 2/18/09) HERE IS A ROUNDUP OF STORIES FROM THIS WEEKEND THAT ILLUSTRATE JUST HOW FAR SHORT OF THAT GOAL THE ADMINISTRATION FELL 



The Huffington Post : "How Failed Obama Foreclosure Relief Plan Contributes To Jobs Crisis" 

"[T]he Tumbling Real Estate Market Threatens To Drag The Economy Back Into Recession."  "The Obama administration s inability to stem the foreclosure crisis ricocheted dramatically on Friday, as the Labor Department released unexpectedly low job-growth numbers that pushed the unemployment rate back over 9 percent. The jobs report comes on the heels of both a devastating report that found housing prices hit new lows in March and warnings from economists that the tumbling real estate market threatens to drag the economy back into recession." (Zach Carter and Jeremy Bendery, "How Failed Obama Foreclosure Relief Plan Contributes To Jobs Crisis,"  The Huffington Post , 6/3/11) IMF Report Suggests That Rampant Foreclosures Added 1.25 Percent To The Unemployment Rate.  "The connection between the foreclosure crisis and rampant unemployment is well known by economists and the administration. Diving home values and heavy debt burdens force cutbacks in both consumer spending and tax revenue for local governments. These reduced spending levels and lower government revenues force layoffs in both the public and private sector. And those layoffs, in turn, spur more foreclosures. A July 2010 report from the International Monetary Fund suggested that foreclosure problems added 1.25 points to the unemployment rate or more than 10 percent." (Zach Carter and Jeremy Bendery, "How Failed Obama Foreclosure Relief Plan Contributes To Jobs Crisis,"  The Huffington Post , 6/3/11) 

The New York Times : "For Jobless, Little U.S. Help On Foreclosure" 

The Treasury Department Has Only Spent $1.85 Billion Of The $46 Billion It Was Given To Prevent Foreclosures.  "Critics of the Obama administration's approach to preventing foreclosures have pressed for two years to get officials to focus more of their attention on unemployed homeowners, with meager results. As part of the bank bailout, the Treasury Department was given $46 billion to spend on keeping homeowners in their houses; to date, the agency has spent about $1.85 billion." (Andrew Martin, "For Jobless, Little U.S. Help On Foreclosure,"  The New York Times , 6/4/11) 

"The Money Was There And They Didn't Spend It."  "Morris A. Davis, a former Federal Reserve economist, estimates that as many as a million homeowners slipped into foreclosure because of insufficient help for the unemployed. 'The money was there and they didn't spend it,' said Mr. Davis, an associate real estate professor at the University of Wisconsin. 'I don't mean to sound outraged, but I am pretty outraged.'" (Andrew Martin, "For Jobless, Little U.S. Help On Foreclosure,"  The New York Times , 6/4/11) 

The Washington Post :   "Swing States Still Struggling After Housing Bust Pose Challenges To Obama Reelection" 

In Swing States, "Obama Will Face Pointed Questions From Voters Who Think The Administration's Policies Have Done Little To Make Things Better."  "Reversing the economic decline fueled by the housing bust is a paramount test for President Obama as he campaigns for reelection. The president's challenge is particularly pressing in potential swing states such as Florida, Nevada and Arizona, where stubborn joblessness and the pain from the collapse in real estate is most acute. It is in these places where Obama will face pointed questions from voters who think the administration's policies have done little to make things better." (Michael A. Fletcher, "Swing States Still Struggling After Housing Bust Pose Challenges To Obama Reelection,"  The Washington Post , 6/4/11) 

Reuters :   "White House Says Needs To Deal With Housing Problems" 

The Administration's Foreclosure Prevention Programs "Have Had Little Impact On The Overall Housing Sector."  "The administration is already using taxpayer funds from its $700 billion bank bailout program to help prevent foreclosures and give struggling Americans a reprieve on their mortgage payments. But the programs have had little impact on the overall housing sector. March home prices slumped below lows reached during the financial crisis in April 2009. Goolsbee did not elaborate on what the administration could do to help the housing market recover." ("White House Says Needs To Deal With Housing Problems,"  Reuters , 6/5/11)
