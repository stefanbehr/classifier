OWS Anti-Semitism: Where's the Outrage? 



Memorandum 

To:           Interested Parties 

From:      Sean Spicer, @seanspicer 

Re:          OWS Anti-Semitism: Where s the Outrage? 

Date:       October 18, 2011 



Democratic leaders have spent the last week championing the "Occupy Wall Street" movement, yet in the midst of protestors' extreme anti-Semitic, anti-Israel  comments , they've been silent. 

Where's the outrage?  While protestors are seen spewing hate against Jewish Americans, President Obama, Nancy Pelosi, and DNC Chairwoman Debbie Wasserman  Schultz have declared their support for the demonstrations.  Democratic Congressional Campaign Committee Chair Steve Israel even circulated a petition saying he's "standing with" Occupy Wall Street. 

Is Steve Israel standing with those calling for the killing of Jews?  Does Debbie Wasserman Schultz agree with the various  calls  for Jews to be "run out of this country" and to  oppose and destroy  the state of Israel?  

Democrats were quick to single out any instances of perceived extremism among Tea Party supporters, but with Occupy Wall Street, they turn a blind eye.  President Obama claimed last weekend that Martin Luther King would support the demonstrations.  But surely Dr. King would have called out these ugly displays of bigotry. 
