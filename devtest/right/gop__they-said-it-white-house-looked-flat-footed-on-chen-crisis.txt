They Said It! White House Looked "Flat Footed" On Chen Crisis 



    

Click To Watch   

NBC's Chuck Todd : The New York Times  reported this morning that senior U.S. officials privately are acknowledging missteps in the handling of the case. And yesterday the White House, frankly, looked flat-footed." 

WH Spokesman Jay Carney : "All of our diplomacy was directed at putting him in the best possible position to achieve his objectives." 

Todd : "But was the diplomacy rushed because Clinton and Geithner were coming to China? Now the full story isn t clear." (MSNBC'S " Daily Rundown ," 5/4/12)
