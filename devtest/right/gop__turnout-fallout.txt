Turnout Fallout 





Democrat Primary Turnout Falls Sharply From Last Election With Unopposed Incumbent 

On Saturday, Less Democrats Voted In The Louisiana Democratic Primary Than In 1996, The Last Time A Democratic President Ran For Renomination Unopposed.  ( Federal Election Commission , Accessed 3/25/12;  Politico , Accessed 3/25/12) 

In 8 Of 10 States That Have Held Democratic Presidential Primaries This Year, Turnout Has Been Lower Than Turnout In 1996.  (Aaron Blake, "Obama May Have A Turnout Problem, Too,"  The Washington Post's  The Fix , 3/13/12;  Federal Election Commission , Accessed 3/25/12;  Politico , Accessed 3/25/12) "Turnout In Ohio, New Hampshire, Iowa, Oklahoma And Tennessee Is Down At Least 29 Percent From 16 Years Ago."  "Turnout in Ohio, New Hampshire, Iowa, Oklahoma and Tennessee is down at least 29 percent from 16 years ago, and in Massachusetts, it was down 8 percent, according to a Fix review. The numbers paint a picture that might lead some to ask whether Democrats are also facing an enthusiasm gap." (Aaron Blake, "Obama May Have A Turnout Problem, Too,"  The Washington Post's  The Fix , 3/13/12) 

"The Two States Where It Has Gone Up Are Somewhat Special Cases" That Include A Large Population Increase And A 1996 Primary Election In Which President Clinton Was Not On The Ballot.  "The two states where it has gone up are somewhat special cases. In Michigan, where turnout was up 37 percent from 1996, Clinton wasn't on the ballot that year, and in Georgia, where turnout was up 46 percent, the state's population has grown by about a third over the same span. (By comparison, GOP turnout in Georgia was up 61 percent from 1996.)" (Aaron Blake, "Obama May Have A Turnout Problem, Too,"  The Washington Post's  The Fix , 3/13/12) 

"The Numbers Do Lend Credence To The Idea That Democrats Are Also Not Terribly Excited About Voting In This Year's Presidential Election"  (Aaron Blake, "Obama May Have A Turnout Problem, Too,"  The Washington Post's  The Fix , 3/13/12)
