WH: Americans Want Us To Move Forward...To Plan Obama's B-Day Fundraiser 



As Washington races to the finish line to solve the debt ceiling deadline, the president is still busy planning his birthday big money fundraising bash in Chicago on Wednesday. When asked if the president should postpone the party/fundraiser due to the country's economic realities, WH advisor Valerie Jarrett said the American people want "us" to "move forward." Advice to the White House, Americans may want to move forward, but with 9.2 percent unemployment that might be the next thing to tackle...not a fundraiser. 



Andrea Mitchell:  Do you think the president should go forward with his birthday celebration which is basically a fundraiser...  Or should that be postponed given the economic realities?   

Valerie Jarrett:  I think what the American people want to see is for us to move this debate behind us and move forward. 







###
