White House Senior Advisor Admits Economy Won't Change 



White House senior adviser David Plouffe was asked today on ABC's "Good Morning America" if he agreed with Vice President Biden's comments last week that the Republicans could defeat Obama in 2012.  Plouffe admitted in his response that the election will be close because the economy isn't going to change much between now and November 2012. 

 PLOUFFE: "Well, we're going to have a close election. I mean some things aren't going to change between now and next November. We're obviously in a tough economy."
