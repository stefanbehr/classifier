White House Throws Schumer Under The Bus 



Sen. Schumer's latest ' extreme'  talking point has been accusing Republicans of  stifling the economy for political gain . That couldn't be further from the truth with  numerous jobs bills passed by the House sitting idle in the Democrat-controlled Senate . Perhaps so far from the truth that even the White House had to throw him under the bus? Watch: 

Special Asst. to the President Stephanie Cutter Throws Schumer Under The Bus 



TRANSCRIPT 

CHRIS MATTHEWS: Do you think there is a Republican strategy of slow walking this effort to have the debt ceiling vote passed in time to avoid a catastrophe? 

STEPHANIE CUTTER: Well I'm not going to get into what their motivations are. I do know that we have to come to an agreement. Congress has an obligation on the debt limit to avoid the government defaulting on the debt. It's never happened before. Congress has always met its obligations, and we're expecting that to happen this time.  We don't think they're actively working against the economy. Too many people are hurting out there, struggling to make ends meet. We need to show some leadership in Washington and work for those people. I think Republicans understand that and I don t think it s a very good political strategy to talk down the economy or to take actions that hurt the economy. 

MATTHEWS: Well maybe you re being too nice. I hate to say that about a partisan spokesperson 

###
