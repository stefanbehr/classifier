from nltk.tokenize import PunktSentenceTokenizer as SentTok
from nltk.tokenize import TreebankWordTokenizer as TreeWordTok

import os, re, string
 
sent = SentTok() # nltk sentence tokenizer instance
word = TreeWordTok() # nltk penn treebank word tokenizer instance

# for escaping punctuation
punc_map = {'!': 'bang',
            ';': 'semicolon',
            ':': 'colon',
            '|': 'pipe',
            '#': 'pound'}

def escape(token):
    """
    Replace string consisting of punctuation character
    with escape sequence.
    """
    if token in punc_map:
        return '!{0};'.format(punc_map[token])
    else:
        return token

def tokenize(blob, punc=True):
    """
    Given a blob of text, returns list of tokens.
    """
    blob = ' '.join(blob.split()) # normalize whitespace to single spaces
    sentences = sent.tokenize(blob) # sentence-tokenize the blob first
    tokens = []
    for s in sentences: # word-tokenize each sentence
        tokens.extend(word.tokenize(s))
    if not punc:
        return [token for token in tokens if token not in string.punctuation]
    else:
        return [escape(token) for token in tokens]

def extract_unigrams(blob, punc):
    """
    Return list of a text blob's unigrams,
    with w1 tags prepended.
    """
    tokens = tokenize(blob, punc)
    return ['w1_{0}'.format(token) for token in tokens]

def extract_bigrams(blob, punc):
    """
    Return list of a text blob's bigrams,
    formatted into the following format:

        "w2_<unigram1>_<unigram2>"
    """
    tokens = tokenize(blob, punc)
    bigrams = zip(tokens[:-1], tokens[1:])
    return ['w2_{0}_{1}'.format(*bigram) for bigram in bigrams]

# for checking what punctuation is present in the training/test files

def extract_punctuation(dpath):
    punc_list = []
    if os.path.isdir(dpath):
        os.path.walk(dpath, process_dir, (dpath, punc_list))
    punc_freq = {}
    for punc in punc_list:
        punc_freq[punc] = punc_freq.get(punc, 0) + 1
    punc_sorted = [(k, v) for v, k in sorted(((v, k) for k, v in punc_freq.items()), reverse=True)]
    print '\n'.join("{0}: {1}".format(*pair) for pair in punc_sorted)

def process_dir(args, dirname, files):
    base, store = args
    if dirname != base:
        alphanum = string.digits + string.letters[:26]
        for fname in files:
            with open(os.path.join(dirname, fname)) as fhandle:
                tokens = tokenize(fhandle.read().strip())
                store.extend(token for token in tokens
                             if len(token) == 1 and token.lower() not in alphanum)
