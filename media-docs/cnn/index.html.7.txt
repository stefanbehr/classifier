Washington (CNN) -- Why wait until Election Day?
A majority of Americans think President Barack Obama will win re-election, according to a Gallup poll on Wednesday.
By a 54%-34% margin, the public thinks Obama will defeat Republican nominee Mitt Romney on November 6.
But the actual race for the White House appears to be a much, much closer contest.
National surveys of those likely to vote in the presidential contest indicate the race is basically tied, with surveys in the crucial battleground states suggesting that Obama has the slightest of edges over his GOP challenger.
A CNN Poll of Polls, which averages the eight live operator, non-partisan, national surveys of likely voters conducted over the past nine days (nearly entirely after the final presidential debate) indicates Romney at 48% and the president at 47%.
But it's the numbers behind the topline figures that tell the real story. And one of them is race.
"I think what we're really seeing in the national polling is that the president is right at the do-or-die line among white voters," said CNN Senior Political Analyst and National Journal Editorial Director Ron Brownstein.
Brownstein says that Obama needs to carry 40% of the white vote and 80% of the minority vote to win a second term.
"He's right at that tipping point," added Brownstein.
While the national polls are revealing, the race for the White House is a battle for the states and their electoral votes, with 270 being the magic number.
The contest will be won or lost in the battleground states and no state is getting more attention than Ohio, where 18 electoral votes are up for grabs.
A CNN Poll of Polls that averages the five non-partisan, live operator surveys conducted in Ohio following last week's debate indicates Obama at 49% and Romney at 46% among likely voters in the Buckeye State.
No Republican in modern times has won the White House without carrying Ohio. While the latest public opinion polls give Obama the slight edge, the Romney campaign remains confident.
"We believe we can and will win Ohio. I think many of these polls if you take them together and average them, the trend line has been in our favor and the race in Ohio is very close," Romney campaign spokesman Kevin Madden told reporters on Wednesday.
The swing state of Virginia's also getting a ton of attention. A CNN Poll of Polls that averages the four non-partisan, live operator surveys conducted in Virginia since the debate indicates Romney at 48% and Obama at 47%.
And in Florida, a CBS News/New York Times/Quinnipiac University poll also indicates a race that's tied up, with the president at 48% and Romney at 47%. A CNN/ORC International poll released Monday also showed the race for Florida's 29 electoral votes deadlocked with Romney at 50% and Obama at 49%.
The Obama campaign likes the numbers coming out of the public opinion polls in the swing states.
"We feel very very good about the numbers that we're mounting up in those states," Obama campaign senior adviser David Axelrod told reporters on Wednesday.
"We're maintaining double-digit leads among women," added Axelrod, discussing the all-important and much-discussed female vote.
The Romney campaign also says it's in a very good place with a week to go.
"This race is exactly where we had hoped it would be a week out. It is a close race," said senior Romney adviser Russ Schriefer on a conference call on Wednesday.
And the Romney team points to the number 50.
"We have an incumbent president that is stuck well below the 50% threshold in almost every mark that you can take, whether it's the national polls or the state polls. And as you know, under 50%, or well under 50% is not a very happy place to be if you are an incumbent a week out," Schriefer added.
Most news organizations, including CNN, rate Pennsylvania as a "lean-Obama" state. But a Franklin and Marshall College poll released Wednesday indicates Obama with a 49%-45% advantage over Romney, which is within the survey's sampling error and is down from a nine-point lead he had last month. The new Franklin and Marshall poll is the fifth non-partisan, live operator survey released this month in Pennsylvania to indicate a close contest for the state's 20 electoral votes.
The Romney campaign and its allies are going up with ad buys in the Keystone State as well as Minnesota and Michigan, two other states that have been shaded blue in recent cycles.
The Obama campaign shoots back at talk that any of those states may actually be in play.
"You see the Romney campaign and their allies heading into three states they are simply not going to win: Michigan, Pennsylvania, and Minnesota. I am so confident of that, that I've put my mustache on the line," Axelrod said.
But the Obama campaign is also putting up spots in those states, prompting the Romney camp to say the Obama campaign is playing defense.
"We feel like they're defending territory while we're on offense," Madden added.
There's been a heavy dose of state polls released already this week, with many more to come the rest of this week and into the final weekend before the election. But as with the national polls, sometimes the numbers behind the state survey toplines are more revealing.
"In the battlegrounds, Obama's running two very different campaigns and he's mobilizing two very different coalitions," Brownstein said.
"In the Sun Belt states, the president's putting most of his chips on cultural liberalism, aimed mostly at younger voters, mainly white, college-educated white voters.
But in the Rust Belt, Obama's running better among blue-collar white voters due to his economic populism. They are his last line of defense, and he's running significantly better among blue-collar white voters in Ohio, Wisconsin and Iowa than anywhere else."
But in the end, there's only one number that will count. And that's the number of electoral votes each ticket has won.

