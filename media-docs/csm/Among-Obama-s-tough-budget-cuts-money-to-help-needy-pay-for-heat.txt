Reports suggest that President Obama's federal budget, to be released next week, will propose cutting in half the budget for the Low Income Home Energy Assistance Program. It's one of many popular programs on the chopping block.
President Obama speaks at Northern Michigan University in Marquette, Mich., Thursday, Feb. 10.
As the Obama administration gets ready to unveil its budget on Monday, it appears that the president - like Republicans - will be calling for cuts to programs that are popular.
According to supporters of the Low Income Home Energy Assistance Program (LIHEAP), President Obama will propose cutting in half the budget for the program, which helps people pay their heating and cooling bills. This calendar year, some 8.9 million people are expected to ask for help with their utility bills, at a cost of $5.1 billion.
Funding for LIHEAP is not likely to be the only discretionary spending pared by the administration. There are also reports that the Obama administration will propose sharp budgets cuts in community-action programs, which fund such things such as Head Start, as well as community block grants, which are federal monies that can be used for almost anything a locality needs.
Cutting in these areas is not surprising, says Stan Collender, a budget expert and partner at Qorvis Communications in Washington.
"They really don't have anyplace to go except to cut discretionary spending in lower-priority items like these," Mr. Collender says. "In his heart, the president may not want to do this, but if you want to reduce the deficit, this is where the money is."
Some cuts may kick in before the next budget year, which starts in October. Newly elected House Republicans have talked about slicing as much as $100 billion from the current year's budget.
"People will be horrified at the scale of the cuts the House is considering, if they are implemented," says Elizabeth Lower-Basch, senior policy analyst at the Center for Law and Social Policy, a Washington group that works to help the poor. "What we are hearing is that the cuts will be for the most vulnerable."
In the case of LIHEAP, any cuts for this fiscal year will be relatively minor since the bulk of the funding has already been shipped to the states, which administer the program, says Mark Wolfe of the National Energy Assistance Directors' Association, which lobbies for the funding.
At risk for this year could be $349 million in emergency funds, which are used in the case of heat waves or unexpected cold snaps.
"If worst comes to worst, we can live with it," Mr. Wolfe says.
Wolfe and others will be urging Congress to nix any cuts proposed by the president. According to the National Journal, which first reported the proposed cuts, the LIHEAP budget would be cut by $3 billion.
On Wednesday, responding to the news, Sen. John Kerry (D) of Massachusetts said in a statement, "I've always supported serious efforts to restore fiscal sanity, but in the middle of a brutal, even historic, New England winter, home heating assistance is more critical than ever to the health and welfare of millions of Americans, especially senior citizens."
According to Wolfe, demand this winter for help in paying heating bills is up 64 percent in Vermont, 21.1 percent in Massachusetts and 20 percent in Wyoming. Even in Florida, which has seen stretches of below-freezing weather this winter, demand is up 49.7 percent.
If the LIHEAP funding is cut to the 2008 level, it will mean dropping 3.1 million people from the program.
Whether Congress actually slices spending for any of the social services is another matter.
"Federal spending is unpopular. Federal services are not," says Mr. Collender.

