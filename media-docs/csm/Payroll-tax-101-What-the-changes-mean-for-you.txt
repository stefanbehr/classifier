The House and Senate passed a payroll tax cut extension for 160 million Americans Friday. Among other provisions, the bill adjusts downward the duration of unemployment benefits.
Speaker of the House John Boehner walks to the House Chamber for a vote on the payroll tax cut in Washington Friday. The House of Representatives passed legislation to extend a tax cut for 160 million workers through December and continue long-term unemployment benefits.
The tax-cut bill passed by Congress Friday will affect ordinary Americans in some important ways.
It means a temporary reduction in the payroll tax paid by workers will remain in place for the final 10 months of the year.
It also adjusts the duration of unemployment benefits, so that the extended benefits available since the recession will become a bit less generous.
RECOMMENDED: Why 'temporary' tax cuts never die: Payroll tax and 3 other examples
After votes in the House and Senate Friday, President Obama is expected to sign the bill soon.
"This tax cut is common-sense," Mr. Obama said in his weekly address to the nation last Saturday. "If you're a family making about $50,000 a year, this tax cut amounts to about $1,000 a year."
Many Republicans argue that the nation needs longer-term growth strategies, including tax reform, rather than a focus on temporary tax cuts. But enough Republicans supported the measure for its passage.
Here's an overview of what Congress calls The Middle Class Tax Relief and Job Creation Act of 2012:
A reduction of 2 percentage points in the payroll tax will stay in place through the end of the year. This provision, designed to help buoy the economy by letting the typical household keep an extra $20 per week, was Obama's top legislative priority. Normally workers would have 6.2 percent of their paychecks taxed to fund the Social Security program. Since the start of 2011, the temporary tax cut has reduced that to 4.2 percent of wages.
Self-employed people also will see a payroll tax cut of 2 percentage points, through year's end. The payroll tax cut has meant that since the start of 2011, they've been paying 10.4 percent of self-employment income for Social Security, rather than 12.4 percent.
The estimated cost of the payroll tax cut for the 10 months ahead is $93 billion.
Money lost to the Social Security trust funds as a result of the payroll tax cut is fully replaced from Treasury's general fund.
A House summary of the bill says unemployment insurance benefits will be reduced to "levels typically seen in a recession," phasing out a 20-week extended benefits program that has been in place since the recession that ended in mid-2009.
By the end of the year the maximum in most states would be 63 weeks, down from the 93 weeks currently received in some 46 states. In states with high joblessness, extended benefits have run for as long as 99 weeks.
The formulas provide that jobless benefits would be shortest (as little as 40 weeks, down from 60 weeks) in states with below-average unemployment. Benefits could run for as long as 73 weeks in states with a jobless rate of 9 percent or higher.
The measure requires those drawing jobless benefits at both the state and federal level to look for a job.
It allows states to drug screen anyone who lost his or her job by failing or refusing an employer drug test, or who is seeking a job that generally requires a drug test.
The bill extends the nation's primary welfare program, Temporary Assistance for Needy Families, for the current fiscal year.
It closes a so-called "strip club loophole" so that welfare funds cannot be accessed at teller machines in strip clubs, liquor stores, and casinos.
The measure requires civilian federal employees and members of Congress to contribute more toward their pension plan, starting with people who join the federal payroll after the end of 2012.
Get daily or weekly updates from CSMonitor.com delivered to your inbox. Sign up today.
How much do you know about taxes? Take the quiz.

