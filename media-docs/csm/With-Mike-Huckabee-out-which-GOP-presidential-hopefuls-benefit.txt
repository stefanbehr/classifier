Former Arkansas Gov. Mike Huckabee announced on his Fox News show Saturday night that he would not seek the Republican presidential nomination in 2012, adding uncertainty to the race to pick a challenger to President Obama.
Mike Huckabee's exit from the 2012 presidential race continues the inevitable winnowing process among likely and talked about challengers to President Obama's billion-dollar reelection effort.
So who among the field of possible GOP White House prospects is helped by Mr. Huckabee's decision not to run?
Some already have announced their decision not to jump into the race - Haley Barbour, Mike Pence, and John Thune. Others who once (or briefly) enjoyed high expectations - Sarah Palin and Donald Trump - are down in the polls, marked by image problems and seen by most observers as without much of a chance of winning the nomination.
Huckabee's decision could tempt the fence-sitting Indiana Gov. Mitch Daniels or perhaps former Utah Gov. Jon Huntsman. Or it may cause them to take a harder look at the reasons not to run - especially the cost to family of a year spent on the road and under constant personal scrutiny.
As moderates, Governor Daniels and Mr. Huntsman might expect to have a tough fight in Iowa and South Carolina - early nominating states where Huckabee did very well in 2008, and where social conservatives will have much to say about who succeeds in the race's first days.
Former Minnesota Gov. Tim Pawlenty has been playing up his social conservative chops; Rep. Michele Bachmann already has them. Former Sen. Rick Santorum does, too, although his connection to the John Ensign sex scandal - however tenuous - won't help. Newt Gingrich is dogged by past marital infidelities.
What about the tea party? Does Huckabee's exit have any bearing on that movement's influence in 2012?
There's always Ron Paul, of course. But Michele Bachmann could benefit there as well.
"Bachmann is the darling of the Tea Party crowd as well as a strong social conservative," writes Jonathan Tobin, senior online editor of Commentary magazine.

