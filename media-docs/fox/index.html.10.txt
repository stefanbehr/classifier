FILE: Oct 16, 2012: Mitt Romney and President Obama walks past each other at the end of their last debate at Lynn University, in Boca Raton, Fla.AP
Gallup said Tuesday that for the second straight night it will not conduct its daily tracking poll for the presidential race, as a result of superstorm Sandy.
The polling firm said it will assess the situation on a daily basis and provide an update Wednesday.
Frank Newport, editor in chief of the Washington-based firm, said Monday: "The ultimate effect on the overall picture of polling between now and this weekend, including election polling, will depend on what happens as a result of the storm, about which we will have a better understanding on Tuesday and Wednesday."
On Sunday, Gallup had candidates President Obama and Mitt Romney locked at 48 percent among registered voters and Romney leading 51-46 percent among likely voters, based on a seven-day rolling average.
The daily tracking poll has become a fixture in U.S. presidential election cycles.
Gallup is more than 75 years old and conducts continuous polling in 160 countries, according to the company's website.

