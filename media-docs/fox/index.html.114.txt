FILE: April 15, 2005: Tax filers fill out a form for certified mail at the General Post Office in New York before sending in her tax returns.AP
Nearly 2,400 Americans who received unemployment insurance in 2009 lived in households with income of at least $1 million, according to the Congressional Research Service.
The service's 10-page report focuses on unemployment following the longest recession since World War II and efforts by Congress to reform the federal unemployment system amid several years of $1 trillion-plus budget deficits.
The Labor Department requires states to pay unemployment compensation to eligible beneficiaries regardless of their income levels because individual or household income does not impact the "fact or cause of unemployment," according to the report.
The Republican-controlled House passed a bill this session that included a provision to impose an income tax on unemployment benefits for high-income earners. However, the bill signed Feb. 22 by President Obama did not include that provision.
The 112th Congress introduced a total of five bills related to restricting or putting a large tax on unemployment payments for workers who had high incomes.
The report was released after about 1.1 million people exhausted their jobless benefits during the second quarter of 2012, when more than 4.6 million filed initial unemployment claims, according to Bloomberg News, which first reported the survey.
"Sending millionaires unemployment checks is a case study in out-of-control spending," Sen. Tom Coburn, R-Okla., told Bloomberg. "Providing welfare to the wealthy undermines the program for those who need it most while burdening future generations with senseless debt."
In 2009, there were 2,362 people in so-called "millionaire" homes who reported unemployment insurance income. They represented 0.02 percent of the 11.3 million U.S. tax filers that year. In addition, 954,000 others in households earning more than $100,000 reported receiving unemployment benefits.
The number in 2008 was slightly higher, 2,840 people. They represented 0.03 percent of 9.5 million tax filers that year. And 807,000 others in households earning more than $100,000 reported receiving unemployment benefits, according to the report dated Aug.2.
Eliminating the federal share of unemployment benefits for millionaires would save $20 million in the next decade, according to the report.
Coburn introduced legislation in February 2011 that essentially would have prohibited federal funding of unemployment benefits for people who had at least $1 million in assets.
Bloomberg reports the Democrat-controlled Senate voted unanimously for the measure, but it was added to another bill that has yet to pass the chamber.

