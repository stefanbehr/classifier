With the Supreme Court giving President Obama's new health care law a green light, federal and state officials are turning to implementation of the law -- a lengthy and massive undertaking still in its early stages, but already costing money and expanding the government.
The Health and Human Services Department "was given a billion dollars implementation money," Republican Rep. Denny Rehberg of Montana said. "That money is gone already on additional bureaucrats and IT programs, computerization for the implementation."
"Oh boy," Stan Dorn of the Urban Institute said. "HHS has a huge amount of work to do and the states do, too. There will be new health insurance marketplaces in every state in the country, places you can go online, compare health plans."
The IRS, Health and Human Services and many other agencies will now write thousands of pages of regulations -- an effort well under way:
"There's already 13,000 pages of regulations, and they're not even done yet," Rehberg said.
"It's a delegation of extensive authority from Congress to the Department of Health and Human Services and a lot of boards and commissions and bureaus throughout the bureaucracy," Matt Spalding of the Heritage Foundation said. "We counted about 180 or so."
There has been much focus on the mandate that all Americans obtain health insurance, but analysts say that's just a small part of the law -- covering only a few pages out of the law's 2700.
"The fact of the matter is the mandate is about two percent of the whole piece of the legislation," Spalding said. "It's a minor part."
Much bigger than the mandate itself are the insurance exchanges that will administer $681 billion in subsidies over 10 years, which will require a lot of new federal workers at the IRS and health department.
"They are asking for several hundred new employees," Dorn said. "You have rules you need to write and you need lawyers, so there are lots of things you need to do when you are standing up a new enterprise."
For some, though, the bottom line is clear and troubling: The federal government is about to assume massive new powers.
According to James Capretta of the Ethics and Public Policy Center, federal powers will include designing insurance plans, telling people where they can go for coverage and how much insurers are allowed to charge.
"Really, how doctors and hospitals are supposed to practice medicine," he said.
The health department is still writing regulations, which can be controversial in and of themselves. One already written, for instance, requires insurance plans to cover contraception. It has been legally challenged by Catholic groups in a case likely to end up in the Supreme Court.
So, there are likely to be many more chapters to go in the saga of Obama's health care law.

