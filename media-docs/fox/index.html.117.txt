Aug. 30, 2012: Actor Clint Eastwood talks to an empty chair during his address to the Republican National Convention in Tampa, Fla. (AP)
LOS ANGELES -  Clint Eastwood said the idea to use an empty seat as a prop at the Republican National Convention was a spur-of-the-moment decision when someone backstage asked him if he wanted to sit down.
In his first interview since he attended the convention to pledge his support for Mitt Romney, Eastwood told the Carmel Pine Cone, a small California weekly newspaper, that his speech was not only unscripted, it was pretty much spontaneous.
"There was a stool there, and some fella kept asking me if I wanted to sit down," Eastwood told the newspaper, which published the article Friday. "When I saw the stool sitting there, it gave me the idea. I'll just put the stool out there and I'll talk to Mr. Obama and ask him why he didn't keep all of the promises he made to everybody."
Eastwood's peculiar, sometimes rambling conversation with an imaginary President Barack Obama in an empty chair set the blogosphere and social media ablaze. His appearance was intended to be a ringing endorsement for Romney, but the esteemed 82-year-old actor and director opened himself up to ridicule.
Eastwood said he achieved what he set out to do and got across three points.
"That not everybody in Hollywood is on the left, that Obama has broken a lot of the promises he made when he took office, and that the people should feel free to get rid of any politician who's not doing a good job," Eastwood said. "But I didn't make up my mind exactly what I was going to say until I said it."
Eastwood's longtime manager, Leonard Hirshan, told The Associated Press he was not aware of the Pine Cone newspaper article. "You're telling me something for the first time," he said. Hirshan stressed that as a manager, he wouldn't necessarily know about Eastwood's dealings with the media. The actor has no publicist.
While Eastwood said his presentation was "very unorthodox," that was his intent from the outset and he had plenty of people giving him advice on what to say.
"Everybody had advice for me, except the janitor," Eastwood said.
Eastwood said he was told to speak for five minutes but he said it was difficult to gauge time and there weren't any signals or cues telling him to wrap up.
Romney and his running mate, Paul Ryan, later came backstage to thank him.
"They were very enthusiastic, and we were all laughing," Eastwood said.
Eastwood, who stars in the upcoming movie "Trouble with the Curve," maintains Obama doesn't deserve a second term as president.
"President Obama is the greatest hoax ever perpetrated on the American people," Eastwood said. "Romney and Ryan would do a much better job running the country, and that's what everybody needs to know. I may have irritated a lot of the lefties, but I was aiming for people in the middle."

