President Obama's post-convention "bounce" may be shrinking.
New Gallup survey results out Tuesday show Obama leading Mitt Romney by a single percentage point.
The 47-46 point lead represents a much tighter race than just a week ago. Poll results last week showed Obama leading by 7 points, on the heels of the Democratic National Convention in Charlotte, N.C.
Many polls over the past two weeks appeared to show Obama doing better than Romney, suggesting Romney did not benefit from much of a bounce out of his party's convention in Tampa.
But the race may be tightening back up again. The latest Gallup results are based on a seven-day rolling average of results based on interviews with more than 3,000 registered voters.
The margin of error is 2 percentage points.

