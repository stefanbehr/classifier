WASHINGTON -  Two congressmen voiced concerns Monday following a Wall Street Journal report that Facebook was exploring ways to let kids join the social network without lying about their age.
Massachusetts Democrat Ed Markey and Texas Republican Joe Barton fired off a letter to Facebook boss Mark Zuckerberg with 14 paragraphs' worth of questions about technology the network is developing to give kids their own accounts.
"We acknowledge that more and more children under the age of 13 are using Facebook, and this is a problem that needs to be addressed," they wrote.
"However, we believe strongly that children and their personal information should not be viewed as a commodity to be bought and sold to the highest bidder."
The Wall Street Journal reported late Sunday that Facebook is developing technology that would allow children younger than 13 years old to use the site under parental supervision.
Sources said mechanisms being tested include connecting children's accounts to their parents' and controls that would allow adults to decide who their kids can "friend" and what applications they can use.
The under-13 features could enable Facebook and its partners to charge parents for games and other entertainment accessed by their children.
Any attempt to give younger kids access to the site would be extraordinarily sensitive, given regulators' already heightened concerns about how Facebook protects user privacy.
Click for more from The Wall Street Journal.

