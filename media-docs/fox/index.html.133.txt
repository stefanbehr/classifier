May 23, 2012: Joint Chiefs Chairman Gen. Martin Dempsey testifies on Capitol Hill in Washington.AP
The escalating "atrocities" in Syria could end up triggering a military intervention, Chairman of the Joint Chiefs of Staff Gen. Martin Dempsey told Fox News on Monday -- following the massacre that left more than 100 dead.
The international community has scrambled to respond to the violence over the weekend, with the recognition that an international peace plan has failed to stem the fighting. The U.N. Security Council called an emergency session Sunday, with its members unanimously supporting a statement condemning the killings and blaming the Bashar al-Assad regime. Meanwhile, U.N. envoy Kofi Annan traveled to Syria for talks.
Dempsey said Monday, as he has in the past, that military options are being crafted.
"Of course -- there is always a military option," Dempsey said.
But he added that while military leaders are "cautious" about the use of force, the situation in Syria could demand it.
"You'll always find military leaders to be somewhat cautious about the use of force, because we're never entirely sure what comes out on the other side," he said. "But that said, it may come to a point with Syria because of the atrocities."
Asked whether the Libya model -- in which the U.S. joined with other allies to provide support to anti-regime forces -- could be applied in Syria, Dempsey said it's "risky to apply a template" anywhere.
"I'm sure there are some things that we did in Libya that could be applicable in a Syria environment or Syria scenario. But I'm very cautious about templates," he said.
It's unclear how seriously military options are being discussed. All along, administration officials have pushed more for sanctions, diplomacy and international pressure to compel Assad to leave power -- warning that a military intervention in Syria could be far harder to control than one in Libya, which was relatively isolated both diplomatically and geographically.
But the massacre in Houla on Friday renewed calls for international action, particularly after dozens of children were killed. The U.N. estimates 49 children and 34 women were killed in the attack.
Though the Assad government denied responsibility, the U.N. Security Council said the attacks "involved a series of government artillery and tank shellings on a residential neighborhood."
"Such outrageous use of force against civilian population constitutes a violation of applicable international law," the council said. "Those responsible for acts of violence must be held accountable. "
Sen. John McCain was vehement over the weekend in his call for greater international involvement.
"This is a shameful episode in American history," McCain, R-Ariz., told "Fox News Sunday," criticizing the Obama administration's policies as "feckless."

