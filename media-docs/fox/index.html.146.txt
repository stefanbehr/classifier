Stockton, Calif., is set to declare bankruptcy as early as this week, according to local officials, a move that would make it one of the largest U.S. cities ever to file for reorganization.
On Monday, a state-required mediation with creditors to find a fiscal solution is scheduled to expire. Stockton's City Council is then slated to meet Tuesday to decide whether to adopt a budget for operating in bankruptcy, a move widely considered the last step before the city formally submits a Chapter 9 petition to federal bankruptcy court.
Stockton is required to file a budget before its new fiscal year begins July 1.
"The budget I'm sending to the council assumes we will file for bankruptcy," Stockton City Manager Bob Deis said in an interview. "We've been working very hard in trying to negotiate a major financial-restructuring package, but the timeline is close to ending."
Click for more from The Wall Street Journal.

