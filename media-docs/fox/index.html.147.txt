House Judiciary Committee Chairman Lamar Smith, R-Texas, has sent a "member to member" letter to Rep. John Conyers, D-Mich., the top Democrat on the panel, to take him up on his offer to potentially subpoena witnesses tied to intelligence leaks.
This sets in concrete a move to formalize an inquiry into the leaks.
House lawmakers had signaled Wednesday that they planned to launch their own investigation into recent national security leaks -- and use subpoena power to call witnesses.
The move comes after some Republicans complained that Attorney General Eric Holder has not assigned a special prosecutor to the case, instead relying on two of his U.S. attorneys for the job.
Conyers knocked down the complaints Wednesday, noting Congress can do that work.
"We have our investigative capacities and so why don't we inquire ourselves? We also have the regular power of subpoena. If there's somebody (Republicans think) we should talk to, we should talk to them," he said.
Smith, appeared to be in agreement. As for whom might be subpoenaed, it's unclear. But Republican Rep. Dan Lungren said the committee should start with senior administration officials and those in the Situation Room.
One witness before the committee, retired Col. Ken Allard, said leaks to the New York Times about a cyber-attack against Iran's nuclear program were tantamount to having a KGB agent in the White House.
As for timing, Smith said the current Justice Department investigation should wrap by November. He said if it doesn't, then politics are at play, and he said Congress can judge whether the administration is willing to conduct a serious investigation.
Holder has appointed two U.S. attorneys to the leak probe -- one regarding the Iran report, the other regarding leaks about an operation to foil an Al Qaeda in the Arabian Peninsula bomb plot.
Fox News' Catherine Herridge contributed to this report.

