Shown here is the map on the Chicago city website, left, and the map produced by Christopher Devane's company Big Stick.Chicago/Big Stick
The last thing you want to do in Chicago is cross City Hall.
But a local mapmaker says he's fed up and preparing to sue Mayor Rahm Emanuel's government after the city allegedly "stole" his maps nearly a decade ago and has refused to pay him royalties.
Christopher Devane, founder of Big Stick/Neighborhood Ties, told FoxNews.com that the city repeatedly has rebuffed his appeals. Though maps might seem a matter of public domain at first blush, Devane claims that the city -- and subsequently other companies -- have gradually plucked away at his work.
"They just refuse to acknowledge the fact that they plagiarized my map," he said.
A cursory glance at the map Devane produced and the map used on the city's Geographic Information Systems website shows striking similarities. Though the color scheme and design is different, each map contains similar neighborhood boundaries and labels.
Devane said his firm was the first to do that. Though many major cities have long had a rough idea of their own neighborhoods, Devane set out in 1992 to start defining those neighborhoods and mapping them. He did so after 12 years of service with the Illinois Air National Guard.
Devane, 52, described the painstaking process of going block-by-block in Chicago, Boston and other cities -- talking to residents, gathering a consensus -- in order to define where one neighborhood ends and another begins.
The maps now retail for $25 to $60.
But Devane said he hasn't been able to convince the city to pay him ever since noticing what he says is his work on the city website dating back to 2003. He likened what the city has done to copying a published work verbatim.
"Once they draw my borders, it's the same thing as lifting the passages out of my book," he said.
The city denies Devane's claims, though. In a letter from Corporation Counsel Stephen Patton to Devane last year, Patton claimed city personnel created the maps by drawing neighborhood boundaries on their own electronic map. The boundaries, he wrote, came from "various sources, including factual information" in a book Devane published and "their own knowledge."
"Copyright protects creative expression, but not facts," he wrote. "Because the city did not copy your map, the city was and is under no duty to ask your permission or to pay any royalty."
The letter came after Devane wrote to Emanuel's office complaining that Chicago's use of the map has created "collateral problems" -- he claimed other companies have, since seeing the city's map, used it by claiming "public domain."
Devane says he's got proof his maps were stolen. First, a 2008 email from a city project manager acknowledging the city "relies on your maps." Second, an old city Web page that attributed the information to his company. Patton, though, described those claims as errors.
A representative with the Chicago city government has not returned requests from FoxNews.com for comment. The alleged lifting dates back to the prior administration, under Richard M. Daley.

