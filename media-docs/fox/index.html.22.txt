The Obama and Romney camps made some last-minute pitches and attacks Monday before cancelling events through Tuesday as superstorm Sandy neared landfall.
Mitt Romney at a rally in Davenport, Iowa, repeated his closing message that he could lead Washington to the bipartisanship President Obama failed to reach.
"Brace yourself to higher taxes, particularly on small businesses," Romney also said, warning undecided voters about such an increase should they re-elect Obama.
He also urged supporters to donate to the Red Cross or other relief agencies and called on supporters in the path of the hurricane to remove yard signs that could become dangerous projectiles in windy conditions.
The Romney campaign has cancelled all events through Tuesday "out of sensitivity to those in harm's way," in the latest drastic schedule change in the final week of the presidential campaign due to the hurricane.
However, officials said late Monday there will be a storm-relief event Tuesday in Ohio and that Romney might hold "tele-townhall meetings" should campaign events be suspended into Wednesday. Romney also was briefed Monday by Federal Emergency Management Agency officials.
President Obama remains at the White House monitoring the storm -- a massive system that threatened some 60 million people along the East Coast and was poised to make landfall on the New Jersey coast.
However Vice President Joe Biden and former President Bill Clinton criticized Romney for saying that Chrysler is shifting production of its Jeep brand autos from Ohio to China.
Campaigning in Youngstown, Ohio, Clinton called Romney's claim "the biggest load of bull in the world."
Biden said it was "bizarre."
Clinton, who had been scheduled to appear with Obama, said the president was personally offended by Romney's claim.  Obama canceled scheduled appearances with Clinton in Florida and Ohio on Monday, citing Hurricane Sandy.
The storm has turned into an unexpected curveball in the final days of the presidential race. Both campaigns were preparing to make their closing arguments at a series of stops in battleground states. Those schedules are now significantly cut back and remain in flux.
Earlier in the day, Obama also had cancelled a campaign event set for Tuesday in Green Bay, Wis.
The Romney campaign, too, had a rally scheduled for Wisconsin later Monday night which was cancelled. Scheduled stops later Monday for Paul Ryan in Florida were also nixed, as were all events for both running mates on Tuesday.
However, Republican Vice Presidential nominee Paul Ryan held a rally Monday in Fernandina Beach, Fla., which he started with a call for prayers for those in the path of the storm.
"Governor Romney believes this is a time for the nation and its leaders to come together to focus on those Americans who are in harm's way," Romney spokeswoman Gail Gitcho said.
At a stop in Ohio Monday morning, Romney said "our hearts and prayers" go out to those in the storm's path. He urged people to make a donation to the American Red Cross.
Obama addressed the public on the storm early Monday afternoon from the White House. He urged people to follow the guidance of local officials, and evacuate without "delay" for those living in areas under evacuation orders.
"Because of the nature of this storm, we are certain that this is going to be a slow-moving process through a wide swath of the country, and millions of people are going to be affected," Obama said.
He said he's worried about safety, when asked about how the storm impacts the election. "The election will take care of itself next week," the president said.
White House spokesman Jay Carney said they changed plans because the storm picked up speed and intensity overnight, making it necessary for the president to leave earlier if he hoped to get back to Washington.
"The president's priority right now is the safety and security of Americans who are in the path of the storm," Carney said. "It's essential in his view that he be in Washington ... to oversee that effort and to be updated on it."
While the president is balancing his White House duties with his campaign schedule, both candidates are keeping a watchful eye on the storm. Both the Obama and Romney campaigns were suspending fundraising emails Monday to New Jersey, North Carolina, Pennsylvania, Virginia and the District of Columbia -- all expected to be hit by the storm's high winds and heavy rains.
On a conference call Monday morning, Obama adviser David Axelrod and campaign manager Jim Messina said the president's goal is the safety of Americans -- and they are taking scheduling decisions day by day.
Meanwhile, early voting in both Maryland and the District of Columbia was suspended Monday.
And in a rare move, the Supreme Court canceled Tuesday arguments and pushed them off until Thursday.
The Associated Press contributed to this report.

