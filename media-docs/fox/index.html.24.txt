Nov. 1, 2012: Republican presidential candidate, former Massachusetts Gov. Mitt Romney gestures as he speaks during a campaign stop at Meadow Event Park in Doswell, Va.AP
DOSWELL, Va. -  Republican Mitt Romney will campaign in Pennsylvania on Sunday.
Campaign officials on Thursday confirmed that Romney plans to hold a rally in the state two days before the election. The appearance would come shortly after his campaign began airing ads in the state.
Romney officials say the push into Pennsylvania is aimed at expanding the map as polls show a closer race in a state that has backed Democrats in recent presidential elections.
Democrats dismiss those claims. They say the push is evidence that Romney won't win other battleground states, like Ohio, and that he needs to look elsewhere for the 270 electoral votes needed to win the White House.
Romney's plans were first reported by the iPad newspaper The Daily.

