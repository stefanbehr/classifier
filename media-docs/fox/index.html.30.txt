FILE: October 16, 2011: Church Minister Joe Lowery speaks at the Martin Luther King, Jr. memorial dedication on the National Mall in Washington.REUTERS
The reverend and civil rights advocate who gave the benediction at President Obama's inauguration suggested at a recent Obama re-election rally that he thinks white people are going to hell -- though he later said it was just a joke.
The Rev. Joseph Lowery spoke at a rally Saturday in Georgia. According to an account in the Monroe County Reporter, "Lowery said that when he was a young militant, he used to say all white folks were going to hell. Then he mellowed and just said most of them were.
"Now, he said, he is back to where he was," according to the newspaper.
"I don't know what kind of a n----- wouldn't vote with a black man running," he also told the audience in the St. James Baptist Church in Forsyth, Ga., according to the paper.
The 91-year-old Lowery, though, told an Atlanta-area TV station and the Daily Caller that the monologue was a joke and from the perspective of a young militant.
Lowery also said he made clear at the time that the comments -- at the event reportedly attended by hundreds of African Americans -- were intended as a joke.
Still, Forsyth Mayor John Howard said he was "pretty shocked" by the comments, according to the newspaper.
Lowery told the Daily Caller he doesn't remember making the n-word comment.
Lowery was awarded the Presidential Medal of Freedom in 2009 by President Obama. His benediction in 2009 for Obama's inauguration included one racially charged, though somewhat whimsical, line.
He closed with the following passage: "Lord, in the memory of all the saints who from their labors rest, and in the joy of a new beginning, we ask you to help us work for that day when black will not be asked to get in back, when brown can stick around, when yellow will be mellow, when the red man can get ahead, man -- and when white will embrace what is right."

