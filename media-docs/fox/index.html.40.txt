Two Navy guided missile destroyers are being deployed off the coast of Libya, after attacks Tuesday on the U.S. Consulate in Benghazi left four Americans dead, including the U.S. ambassador, military officials told Fox News.
The destroyers are for "contingency purposes," a military official said.
In addition, officials said a "fast team" of 50 Marines was being sent from the U.S. Naval base in Rota, Spain. They are expected to go to Tripoli. According to a U.S. official, there are no U.S. personnel left at the consulate in Benghazi which was attacked.
A U.S. military aircraft is also expected to leave Libya soon with the dead and wounded onboard.
The move comes after President Obama ordered "all necessary resources" provided to Libya to support the security of U.S. personnel in the country.
In a statement, Obama also said he's directed his administration to "increase security at our diplomatic posts around the globe."
Ambassador J. Christopher Stevens and three other American staff members were killed in the attacks Tuesday.
U.S. posts are on alert amid protests, so far in Libya and Egypt, over a film that ridiculed Islam's Prophet Muhammad.
A senior military official clarified Wednesday that despite some reports, no Marines were killed in the Tuesday attack.
Fox News' Jennifer Griffin contributed to this report.

