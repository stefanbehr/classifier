WASHINGTON -  U.S. embassies in at least seven countries in the Middle East, Africa and the Caucuses are warning of possible anti-American protests following the attack on the consulate in Benghazi, Libya that killed the U.S. ambassador to Libya and three other Americans.
The embassies in Armenia, Burundi, Kuwait, Sudan, Tunisia and Zambia, along with the embassy in Egypt, which was hit by a protest on Tuesday, all issued warnings on Wednesday advising Americans to be particularly vigilant.
The warnings, posted on the embassies' websites, do not report any specific threat to Americans but note that demonstrations can become violent.
The protest in Cairo and the attack in Benghazi appear to have been responses to an inflammatory anti-Muslim video posted on the Internet.

