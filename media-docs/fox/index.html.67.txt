June 3, 2012: U.S. Secretary of Defense Leon Panetta, second from left, walks with Commander Air Power Generation Command Col. Sarbjit Singh, left, Defense and Naval Attache for Singapore Capt. John Wood, second from right, and Singapore Defense Attache to the U.S. Brig Gen. Chee Wee Tan before departing Paya Lebar Airfeild, Singapore.AP
PEARL HARBOR, Hawaii -  The commander of the U.S. Pacific Fleet said Monday the Navy will be sending its most advanced vessels and aircraft to the Asia-Pacific region as it builds up its presence by assigning most of its fleet there.
Adm. Cecil Haney said a policy recently outlined by Defense Secretary Leon Panetta to deploy 60 percent of the Navy's ships fleet to the Pacific by 2020 is about capabilities as well as quantity.
"It's not just numbers -- it's also what those platforms, what those units, bring to the table," Haney told The Associated Press in an interview at his headquarters in Pearl Harbor.
Haney cited as an example the Littoral Combat Ship which can operate in shallower waters than other vessels. The U.S. plans to begin deploying one of the ships to Singapore next year.
The EA-18G plane -- which can jam enemy air defenses and fly faster than the speed of sound -- is another. Haney said squadrons of these aircraft would be coming through the region.
There's also the Navy's most advanced submarine -- the Virginia-class. Several of these subs are based at Pearl Harbor.
"Yes, it's about having numbers in that 60-40 split, but also about having the right capability," he said.
The policy offers further details to the Obama administration's announcement earlier this year of a new defense strategy that places greater emphasis on a U.S. military presence in the region in response to Asia's growing economic importance and China's rise as a military power.
The Navy now has about 285 ships about evenly divided among the Atlantic and Pacific oceans. The total number of ships will decline in coming years as some vessels are retired and not replaced.
It has 11 aircraft carriers. A majority -- six -- are already assigned to the Pacific.
The policy is an extension of a 2006 Bush Administration-era policy that had the Navy base 60 percent of its submarines in the Pacific.
The service had for years split its submarines evenly between the Atlantic and Pacific. During the Cold War, the Navy kept 60 percent of its subs in the Atlantic as a deterrent to the Soviet Union.

