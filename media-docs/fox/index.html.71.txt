A North Carolina high school teacher reportedly has been suspended with pay after she was captured on video shouting at a student who questioned President Obama and suggesting he could be arrested for criticizing a sitting president.
The Salisbury Post reported Monday that district school officials suspended the teacher in the course of their investigation into the incident.
The newspaper, which first reported on the YouTube video, did not identify the teacher in question, who is reportedly on staff at North Rowan High School. The video does not show faces, but the heated argument in the classroom can clearly be heard.
"Do you realize that people were arrested for saying things bad about Bush?" the teacher said toward the end of the argument, telling the student, "you are not supposed to slander the president."
The student told the teacher that one can't be arrested "unless you threaten the president."
The argument started when the classroom began discussing news reports that Mitt Romney bullied a fellow student when he was in high school. At the time, The Washington Post had recently published a lengthy article alleging that Romney, as a teenager, had cut off another student's hair.
"Didn't Obama bully somebody though?" a student in the North Carolina classroom asked when the report was brought up, referring to an incident Obama described in his memoir "Dreams From My Father." In the book, Obama wrote that, as a child, he once pushed a female classmate after other students taunted them -- the only two black students in their grade -- and called Obama her boyfriend.
The teacher, in the video, said she didn't know whether Obama bullied anyone -- but the argument quickly escalated, as the teacher yelled at the student, telling him "there is no comparison."
"He's running for president," she said of Romney. "Obama is the president."
The student argued that both candidates are "just men," but the teacher took issue with the statement.
"He's just a man. Obama is no god," the student said.
The teacher responded: "Let me tell you something ... you will not disrespect the president of the United States in this classroom."
The teacher went on to say the two candidates are "not equal."
According to the Salisbury Post, the teacher is still employed and has not been suspended.
"The Rowan-Salisbury School System expects all students and employees to be respectful in the school environment and for all teachers to maintain their professionalism in the classroom. This incident should serve as an education for all teachers to stop and reflect on their interaction with students," the school said in a statement, published by the Post. "Due to personnel and student confidentiality, we cannot discuss the matter publicly."
The video was first posted online last week.

