President Obama never fails to mention the billions in federal dollars pumped into the U.S. auto industry when campaigning in Ohio.
At a recent campaign stop in Dayton, the president describes the bailout this way: "I bet on American workers. I bet on American manufacturing. I would do it again because that bet has paid off for Ohio and for America in a big way."
Former Delphi plant manager Tom Rose has a different opinion about the bailout: "It's good press. It's good politics, but it's only one side of the story."
Rose and tens of thousands of other Delphi retirees were nervous in 2009 when the company was trudging its way through bankruptcy. So a lot of questions were asked, like how's our pension looking?
The answer, says Rose, was a thumbs-up from management, which said the pension was well funded.
And then, it wasn't.
Delphi abruptly cancelled its employee pensions and turned over what remained in them to the federal government to administer.
It was the nightmare scenario for Delphi retirees. The monthly payments that were to be their financial bedrock through retirement years were slashed. The cuts were staggering, ranged from 30 to 70 percent.
After 39 years on the job, Rose's pension was clipped 40 percent.
"You spend years and years making your financial plan for your retirement ... and going through all the numbers," Rose said, "and it gets taken away from you in a heartbeat. ... and I feel absolutely betrayed."
But it was at this time that a floundering General Motors was seeking emergency help from the federal government. The company was in needs of billions. Chrysler was also at the White House door looking for assistance.
The Obama administration was willing but demanded a rapid restructuring of the companies. Typically, in these kinds of bankruptcies or bankruptcy-like circumstances, concessions are made or financial commitments are broken. But what GM needed besides the infusion of cash from the federal government was labor peace.
Any deal between the U.S. government and GM was not binding on the United Auto Workers. That gave the union leverage. What it wanted was its retired Delphi members taken care of.
GM already had an agreement with the UAW to "top-off" its union pensions at Delphi, if that company went into bankruptcy.
Steven Rattner, point-man for the White House on the bailout, notes that these types of side agreements typically get tossed out in bankruptcies. To Rattner, it was important for
GM to honor the side-deal about Delphi members.
"It was not easy to decide where to draw the line," says Rattner, "but ultimately we decided that GM should honor its prior agreement (with the UAW)."
But that was it. Salaried Delphi retiree pensions were not made whole. The 20,000 who had banked their future on their pensions were left to figure it out on their own.
Tom Rose thinks he knows why. "The union had political connections and we did not," he said.
In Rose's opinion, and that of many more salaried Delphi retirees, the pension decision was a political payoff. It was nothing more than a Democratic president and his administration taking care of an ally, organized labor.
Rattner insists that not the case.
"We had lots of discussions about whether to top off all of the pensions," Rattner said.
It came down to necessity. Rattner says GM could not survive without people on the floors of the factories working. That and the side agreement with the UAW cinched it.
To Rose, it all comes down to fairness.
"You've got two groups of employees working for the same company that are in the identical situation and yet they are treated distinctly different by our own federal government," he said.

