A new poll shows Mitt Romney 2 points ahead of President Obama in Ohio, in the latest survey showing the vital battleground contest tightening and too close to call.
The Rasmussen Reports poll of likely voters had Romney slightly ahead at 50-48 percent. The lead, though, was well within the 4-point margin of error -- the results effectively reflect a virtual tie in the Buckeye State.
Another poll released over the weekend, by the Ohio News Organization, showed the candidates knotted up at 49 percent each.
The polling, though, appears to show Ohio falling well into toss-up territory -- despite Obama having the edge in most surveys leading up to October. The RealClearPolitics average of polls shows Obama with a 1.9 percentage point lead there.
Ohio and its 18 electoral votes are considered vital to victory on Election Day, and particularly to Romney's campaign. The Republican nominee attended a rally in the state Monday morning, while Vice President Biden has scheduled one later in the day in Youngstown, Ohio, while Obama stays in Washington to deal with Hurricane Sandy.
The Rasmussen poll showed a big split between those who voted early and those who plan to vote on or closer to Election Day. For the one-third of voters who already cast their ballots, Obama was leading 62-36 percent. Romney, though, had a big lead among those who haven't voted yet.
The poll of 750 likely voters was conducted Oct. 28.
Nationally, the Gallup polling firm shows Romney leading 50-46 percent among likely voters.

