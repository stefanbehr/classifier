In excerpts of an interview scheduled to air on Thursday night, Marianne Gingrich claims that her ex-husband, Newt Gingrich, sought an "open marriage" before their own relationship ended. 
In the interview with ABC News, Marianne Gingrich claims that when the then-House speaker told her of a long-term, six-year affair he was having with a congressional staffer (Callista Bisek, now his third wife), he asked if he could remain married to Marianne and continue the affair. 
"I just stared at him and he said, 'Callista doesn't care what I do,'" Marianne Gingrich told ABC News. "He wanted an open marriage and I refused."
 
Marianne Gingrich, the former speaker's second wife, also alleged that her ex-husband conducted his affair "in my bedroom in our apartment in Washington."
She also said Gingrich moved to divorce her just months after she was diagnosed with multiple sclerosis.  "He also was advised by the doctor when I was sitting there that I was not to be under stress," she said. "He knew."
According to Marianne Gingrich, her relationship with him began while Newt Gingrich was still married, but in divorce proceedings, with his first wife, Jackie. At the time, Jackie Gingrich was being treated for cancer.
In a second interview with The Washington Post, Marianne Gingrich said her ex-husband asked for a divorce in May 1999, shortly before giving a speech on family values.
She asked in that conversation with The Washington Post, "How could he ask me for a divorce on Monday and within 48 hours give a speech on family values and talk about how people treat people?"
In an interview with NBC's TODAY on Thursday, before the ABC News excerpts were released, Newt Gingrich vowed that he won't say "anything negative" about his former wife.
According to The Washington Post, at a campaign stop in South Carolina on Thursday, he called the statements by Marianne Gingrich, "tawdry and inappropriate," but wouldn't go further. His third wife, Callista Gingrich, stood a few feet behind him.
As word spread Wednesday about the interview with Marianne Gingrich, the campaign responded with something of a "prebuttal."
In advance of that interview, the campaign released a statement Wednesday night from Gingrich's daughters from his first marriage, Kathy Lubbers and Jackie Cushman.
They wrote:
 

