Few glitches were reported with new voter ID laws Tuesday in Tennessee and Oklahoma, two Super Tuesday states where picture identification was required for those casting ballots.
The Nashville Tennessean reported that only a handful of provisional ballots related to the new law were needed across the state. A voter in Tennessee without a picture ID could cast a ballot and then get a free picture ID and return it to a county election commission by Thursday for the ballot to be counted.
One registered voter, Tim Thompson, 55, of Inglewood, Tenn., refused to show his photo identification and then refused to cast a provisional ballot, officials told the Tennessean.
"We just fought a war to bring democracy to Iraq,'' Thompson told the Tennessean. "Now, we're passing laws that restrict and bring conditions to our right to vote. I sacrificed my right to vote in order to make this statement,'' said the ex-Marine.
In Davidson County, one voter chose to cast a provisional ballot rather than show a photo ID, officials told the Tennessean.
"People are prepared when they come to the poll," said Blake Fontenay, spokesman for the Tennessee secretary of state's office.  
Oklahoma officials reported no complaints about their state's new voter-ID law, local news organizations and The Associated Press reported.
In Wisconsin, which holds its presidential primary on April 3, Dane County Judge David Flanagan on Tuesday issued a temporary injunction against the state's new voter ID law as part of a lawsuit brought by the NAACP.
Voter ID laws passed last year in Alabama, Kansas, Rhode Island, South Carolina, Tennessee, Texas and Wisconsin. Advocates say they are needed to combat voter fraud. Opponents say the laws disenfranchise voters who may not be able to easily or freely get photo-identification cards.
Other problemsOhio officials reported minor disruptions at two polling places, The Associated Press reported.
An Allen County Board of Elections official in northwest Ohio says a polling location was moved Tuesday morning from a high school to a nearby church after a bomb threat was called in to the school.
No bomb was found. The official said there was about a 15-minute delay while voters were directed to the new site.
The Franklin County elections board in Columbus said some voters at multiple-precinct locations left their polling places because workers were confused over which ballot to give them. Officials don't believe a large number of voters were affected. They were calling those who left to let them know they could return to vote.
Ohio voters were asked to vote twice, which caused a bit of confusion for some, NBC station WLWT reported.
When voters voted twice in the presidential race, they voted once for their favorite candidate for president, which counts toward the popular vote, and once for a slate of delegates assigned to a delegate even though the delegates are not obligated to vote for that candidate at the party convention.
In South Toledo, Ohio, voters in one precinct were given the wrong ballot. The Lucas County elections board said five people received ballots for the 5th congressional district instead of the 9th district. Kaptur's staff said up to 70 people received the wrong ballot.
The Toledo Blade reported late Tuesday that the Ohio Secretary of State's office reversed an earlier decision and said residents could vote again.
Two problems were reported at polling places in Memphis, Tenn., Shelby County Election Commission Chairman Robert Meyers told the Commercial Appeal.
Lifelink Church lost power due to a blown fuse, but voters could cast ballots because voting machines are battery operated, he said.
At Springdale Baptist Church a ruckus ensued after a poll worker asked which party's ballot a voter wanted. The voter, who was not identified, claimed not to have enough money to be a Republican and the poll worker replied that the voter never would if he or she kept pulling Democratic ballots, Meyers said.
"That's not the kind of conduct we want to condone on election day," Meyers told the Commercial Appeal. "We reminded (poll workers) that we have to keep our personal views to ourselves."

