 
Updated at 12:36 a.m. ET: The Santorum campaign announced that Bella Satorum was released from the hospital Monday night.
In a news release, GOP presidential candidate Rick Santorum and his wife Karen announced that their daughter returned home after being hospitalized for pneumonia over the weekend.
The campaign announced changes in Santorum's schedule, including an added stop in Gettysburg, Pa. Tuesday afternoon.
Ealier story: GOP presidential candidate Rick Santorum's hospitalized daughter could be released Monday, his campaign said.
Santorum spokesman Hogan Gidley said 3-year-old Bella was doing much better and could be discharged from a northern Virginia hospital by the end of the day.
"The family hopes to take her home from the hospital very soon," he said.
Related: Santorum to remain with ailing daughter on Monday
Bella was hospitalized Friday as her father began a brief holiday break from campaigning. He did not campaign Monday so he could be with his daughter, who suffers from a rare genetic condition called Trisomy 18.
Santorum faces an uphill battle against front-runner Mitt Romney in the race for the Republican presidential nomination. The contest next turns to Santorum's home state of Pennsylvania, where both men plan to campaign heavily and where Romney is airing $2.9 million in TV ads against Santorum.
Gidley said Santorum's campaign schedule Tuesday would depend on his daughter's health, but that "we have a full day of events planned tomorrow, so he should be back on the campaign trail."
In deference to Bella's illness, Romney's campaign pulled down a harsh ad that was running against the former senator in Pennsylvania. Romney spokeswoman Andrea Saul said Monday the campaign asked TV stations over the weekend to pull the ad and replace it with a positive, pro-Romney spot instead.
Romney is far ahead of Santorum in the race for delegates to the Republican National Convention and is the likely GOP nominee. Santorum has said he won't drop out of the race, though he's acknowledged he will have to win Pennsylvania if his campaign is to survive.
Bella was hospitalized in January with pneumonia and Santorum left the campaign trail, ahead of the Florida primary, to care for her. The campaign has not provided details of her latest hospitalization.
While Bella doesn't travel with her father, she is a frequent presence in his campaign.
Santorum carries a photo of her and often says she wasn't expected to live beyond her first birthday. Supporters frequently ask how she is doing or mention her when they meet him. Her story is well-known to religious conservatives who back Santorum because of his strong position against abortion. 

