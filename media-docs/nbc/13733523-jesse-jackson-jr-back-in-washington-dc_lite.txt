Rep. Jesse Jackson Jr., D-Ill., returned to Washington, D.C., Friday after nearly three months away from his job.
"I have just learned that Congressman Jackson is at home in DC with his wife and family convalescing," his communications director, Frank Watkins, said Friday.
Jackson, 47, suddenly left his post  June 10 and checked into an Arizona substance abuse clinic. He represents Illinois' 2nd Congressional District. About a month later he checked into the Mayo Clinic.
Asked if Jackson would be back at work Monday, his chief of staff, Rick Bryant, told NBC News: "I hope so."
The Mayo Clinic revealed last month that Jackson, the  son of civil rights leader and former presidential candidate Jesse Jackson, was  being treated for bipolar depression, a condition that affects the parts of the  brain controlling emotion, thought and drive. 
Millions of people have  bipolar disorder. It is marked by highs and  lows of mood, and can be treated by medication and psychological counseling,  according to the Mayo Clinic's website.
It's not clear when Jackson checked out of the clinic. Doctors there would not give specifics and referred all questions to his congressional office.
Jackson faces a reelection battle this November.
 

