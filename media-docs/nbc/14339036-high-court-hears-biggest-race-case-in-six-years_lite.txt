Updated at 1:30 pm ET The United States Supreme Court heard arguments Wednesday in the most important civil rights case to come before the justices in the past six years: a challenge to the use of race as a factor in admissions at the University of Texas.
The case involves Abigail Fisher, a white woman who applied to the school in 2008.
She was not entitled to automatic admission under Texas' "top 10 percent rule," which by law requires the university to admit all in-state high school seniors who rank academically in the top 10 percent of their classes.
Instead, Fisher competed for admission with others who weren't in the top 10 percent of their graduating class. And though her academic credentials were superior to some minority applicants who were admitted to the University of Texas, Fisher was not.
"There were people in my class with lower grades, who weren't in all the activities I was in, who were accepted into UT. And the only difference between us was the color of our skin," she said.
She argues that the university is not using racial classification of applicants to pursue a compelling state need, since Texas's top 10 percent rule has already succeeded in giving the university a racially diverse student body.
She contends the university is using a form of racial balancing that Supreme Court rulings have banned.
The university has rebutted her on that point, saying her racial balancing claim "is refuted by her own concession that UT has not set any 'target' for minority admissions."
NBC News Justice Correspondent Pete Williams reported after the one-hour oral argument that the justices were confronting the questions of "How do you know when there's enough diversity?" and "How do you know when you no longer need affirmative action"
He said the University of Texas had argued to the court that it wanted not only diversity in overall numbers of students but "it wants African-American students who are interested in fencing and speaking Greek and studying architecture" and Latino students who are fencers or ballet dancers.
Texas's argument was, Williams said "We want diversity, in other words, within the mere racial numbers."
In Wednesday's argument, Fisher was represented by Washington attorney Bert Rein, who is also representing Shelby County, Ala. in its challenge to the constitutionality of a key section of the Voting Rights Act. The high court has not yet agreed to hear the Shelby County case.
The University of Texas was represented by Gregory Garre, who briefly served as solicitor general in the Bush administration and who has argued 34 cases before the Supreme Court.
Solicitor General Donald Verrilli argued for Obama administration in support of the Texas program.
The last time the high court considered racial preferences in university admissions was in 2003, when it decided a case involving the University of Michigan Law School.
In that decision the court, ruling five to four, held that race or ethnicity could be used as one factor in a school's effort to ensure racial diversity in its student body. 
The court approved the University of Michigan's "narrowly tailored" use of race in admissions decisions, saying that it helped to advance the state's interest in getting the educational benefits that come from a diverse student body. The program did not violate the Equal Protection Clause of the Fourteenth Amendment, the court ruled.
Williams said it seemed pretty clear from what the justices said in Wednesday's oral argument that they were not likely to overrule the 2003 University of Michigan decision that allowed "narrowly tailored" racial preferences to be used in university admissions.
The author of the opinion in the Michigan case, Justice Sandra Day O'Connor, has since retired and been replaced by Justice Samuel Alito, who is opposed to racial preferences.
Dissenting in the Michigan case, Justice Anthony Kennedy said race can be considered as "one modest factor among many others to achieve diversity," but a school must ensure that each applicant receives individual consideration and that "race does not become a predominant factor" in admissions decisions.
As is often the case, the likely swing vote in the Texas case will be that of Kennedy.
Washington lawyer Tom Goldstein, who teaches Supreme Court litigation at Stanford and Harvard Law Schools, said some observers have wondered how Kennedy can be sympathetic to gay rights - a "liberal" stance, but seem skeptical of the use of racial preferences, a "conservative" stance.
"Justice Kennedy has a vision of the law and of the Constitution which is very much about individuals - he wants people to be thought of as people not as (members of) groups. He's concerned, I think, that the root of affirmative action is that you are treating people as black, you're treating them as Hispanic, and not as individuals" and also that by having state law which treat gays differently than heterosexuals, states are treating them as member of a group and not as individuals.
Justice Elena Kagan, a member of the court's liberal wing, will not participate in the Texas case. She served as President Barack Obama's solicitor general in 2009 and 2010 when the Justice Department became involved in the case in the lower courts. She joined the court in August of 2010.
With Kagan out of the Texas case, a four-to-four tie is possible, which would mean that the Fifth Circuit appeals court ruling in favor of the Texas program would stand, but that no binding precedent would be set for other circuits.
In its most recent high-profile case involving the use of race in education, the high court in 2007 invalidated public school programs in Seattle and Louisville, Ky., that used students' race as a "tiebreaker" for admission to certain high schools and kindergartens.
Chief Justice John Roberts said in his majority opinion on that case: "The way to stop discrimination on the basis of race is to stop discriminating on the basis of race."
 
 

