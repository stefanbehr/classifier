Monday's final debate between President Obama and former Gov. Mitt Romney was to be focused on foreign policy, and largely was, but the place that horses, bayonets, aircraft carriers, submarines and other technology occupy in the nation's defense became the talk of Twitter after Obama brushed aside Romney's critique of his military spending priorities. 
 
Scroll down for a social recap of the debate with the help of Storify. 

