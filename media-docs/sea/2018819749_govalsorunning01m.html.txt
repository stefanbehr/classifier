OLYMPIA -- If you had thought Democrat Jay Inslee and Republican Rob McKenna were the only candidates in Washington state's governor's race, you'd be wrong.
Seven other Washingtonians are also vying to succeed Democratic Gov. Chris Gregoire, and while they're not reflected in the polls or most media coverage, a few of these unknown-to-most candidates are running campaigns -- some more active than others -- even though they're likely to be electoral footnotes after the top-two primary on Aug. 7.
One is a pastor who led a successful fight against scantily clad baristas at coffee kiosks in Snohomish County, and is the only one among the "also running" group who has an organized campaign and is raising significant money and regularly holding campaign events. One candidate's sole platform is to raise taxes on cigarettes by $10 a pack. Another wants to see a "holistic governorship."
All are registered voters and paid a fee of nearly $1,700, the only requirements necessary to run for governor in Washington state.
Here's a look at those other candidates seeking the top office in the state.
Shahram Hadian, a pastor from Everett originally from Iran who often speaks of his conversion to Christianity from Islam.
Party: Republican.
Platform: He says he will stand up to public-sector unions, will oppose efforts to legalize gay marriage, and will fight the implementation of the federal health-care law that was recently upheld by the U.S. Supreme Court.
Prior elected experience: None. Ran for the Legislature in 2010, but lost in the primary.
Motivation to run: Hadian said his experience with the campaign of citizens who were opposed to pasty and thong-wearing coffee baristas in Snohomish County in 2009 "was an affirmation of the ability of the impact I was able to have." (The Snohomish County Council ultimately passed an ordinance that effectively banned the practice, requiring that bikini-barista stands with partially nude employees or sexually suggestive signs be licensed as adult entertainment). He referred to his decision to run for governor as a calling. "Do I admit this is David and Goliath? Absolutely. I believe we are going to see a miracle."
What he feels he offers that the mainstream candidates don't: He argues that he is the true conservative candidate in the race, and specifically mentions McKenna's stand on abortion (McKenna says he supports a woman's right to have an abortion) and McKenna's recent statements that he wouldn't fight implementation of the health-care law now that the U.S. Supreme Court has upheld it.
Rob Hill, owner and operator of commercial real-estate properties in Washington and Alaska. Lives in Shoreline.
Party: Democrat.
Platform: Raise taxes on pack of cigarettes by $10 by 2016 ($5 would be added in 2015 and an additional $5 in 2016).
Prior elected experience: None.
Motivation to run: His father died of a smoking-related cancer in 1995 and "I want cigarettes to be taxed at a level that makes it too expensive for young children to start smoking."
What he says he offers that the mainstream candidates don't: "A simple way to stop a product that kills over 400,000 people every year."
Money raised: None.
L. Dale Sorgen, a computer programmer from Sultan.
Platform: To "encourage people to think independently from the two ruling political parties and to think independently about ideas in a new and creative way that emphasizes personal liberties and excellence in government services that we are forced to pay for."
Motivation to run: Sorgen said he decided to run because he believes that the two main political parties are no longer fighting for taxpayers, but "instead they are only fighting to get, and keep, control for their side."
What he feels he offers that the mainstream candidates don't: He says he offers a "brighter, more prosperous tomorrow" for taxpayers. "In my opinion, there is not a nickel's worth of difference between any candidate from either of the two ruling political parties."
Money raised: $2,469.
Christian Joubert, formerly directed a holistic-health-retreat center. Splits his time living in Bellingham and Edmonds.
Platform: He says he wants a "proactive holistic-health-care system," and to help the state's economy he wants to focus on industries including hemp, organic agriculture, eco-housing and tree planting. He says his first priority is "equal access to holistic health, because without well-being, life has little appreciative value." He also calls for a reduction of the workweek to 35 hours and retirement at age 60.
Prior elected experience: None. Unsuccessfully ran for governor in 2008.
Motivation to run: "My inner voice."
What he feels he offers that the mainstream candidates don't: He says one of the differences he has with Inslee and McKenna is on fiscal and budget issues. He says he supports a "holistic governorship" that would see more taxes on "those business which disproportionately enrich the one percent while impoverishing the people, destroying our health and poisoning our resources."
Platform: He wants to address the issue of excessive force by police, and have mental and physical evaluations done on all police and check their backgrounds on any past complaints about excessive use of force.
Prior elected experience: None. He says if he doesn't win the governor's race, he will run for mayor of Black Diamond.
Motivation to run: His son's death. Sampson's 19-year-old son, Eric, was killed last year after he was shot by King County sheriffs after they say he wouldn't drop a machete, something that Max Sampson disputes. "I do not want this to happen to anyone else's family member," Sampson said.
What he feels he offers that the mainstream candidates don't: Sampson said he didn't believe the other candidates would go against the police departments on the issue of excessive force. "I would take on all the police in the state of Washington."
James White, airplane inspector for Boeing and volunteer child advocate. Lives in Marysville.
Party: Independent.
Platform: Family-court reform, job creation and education.
Prior elected experience: Treasurer for The Moose Lodge. Unsuccessfully ran for governor in 2008.
Motivation to run: His work in child advocacy and personal experience with an ongoing custody battle over his now 17-year-old son.
What he feels he offers that the mainstream candidates don't: A new perspective that's "not being driven by a party agenda."
Javier O. Lopez, retired, lives in Bucoda, Thurston County, says he is "one of the greatest artists of today's world."
Platform: Website discusses transportation, education and equity in taxation. When asked for specifics, he railed against the media and said, "I have nothing to tell you, because you are an usurper."
Prior elected experience: Community Action Committee, Los Angeles. Unsuccessfully ran for governor in 2008, and Bucoda Town Council in 2007.
Motivation to run: "Christianity."
What he feels he offers that the mainstream candidates don't: Wouldn't answer. In the voter's pamphlet, he writes: "I only regret having one life to give to my country and it has already been given to the people."

