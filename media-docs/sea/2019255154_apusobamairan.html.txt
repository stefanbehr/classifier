President Barack Obama is telling the U.N. General Assembly that he wants to resolve the dispute over Iran's nuclear program through diplomacy but that the time to do that is not unlimited.
President Barack Obama is telling the U.N. General Assembly that he wants to resolve the dispute over Iran's nuclear program through diplomacy but that the time to do that is not unlimited.
Obama says Iran has failed time after time to demonstrate that its nuclear program is peaceful and has failed to meet its obligations to the United Nations. He also says the Iranian government has been propping up the dictatorship in Syria and supporting terrorist groups abroad.
The president says, quote, "the United States will do what we must to prevent Iran from obtaining a nuclear weapon."
This is Obama's final international address before the November elections.

