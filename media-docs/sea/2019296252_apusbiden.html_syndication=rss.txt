Vice President Joe Biden says that he and President Barack Obama had been on the job less than a week when a top economic adviser told them the country was facing a trillion-dollar budget deficit.
Vice President Joe Biden says that he and President Barack Obama had been on the job less than a week when a top economic adviser told them the country was facing a trillion-dollar budget deficit.
Biden says that Obama replied, "But I haven't done anything yet."
Biden blames the deficit on the previous Bush administration, adding that it "put two wars on a credit card" and gave tax cuts to the wealthy after inheriting a balanced budget and revenue surplus from the Clinton administration.
Biden hammered GOP presidential candidate Mitt Romney for comments on a leaked video in which Romney describes 47 percent of Americans as paying no federal income tax and believing "they are victims."
Biden made his remarks Saturday to more than 2,000 people in Fort Myers, Fla.

