Forget about a date night. President Barack Obama and wife Michelle will be marking their wedding anniversary with a debate night.
Forget about a date night. President Barack Obama and wife Michelle will be marking their wedding anniversary with a debate night.
The couple's 20th anniversary is Wednesday, the same day the president will hold his first debate with Republican rival Mitt Romney. "Go figure," the first lady said in a recent television interview.
The president says he and his wife have planned a belated anniversary celebration on Saturday. His campaign is also asking supporters to sign an electronic anniversary card for the first couple.
The Obamas will at least spend their anniversary in the same city, a rarity given that both are frequently in battleground states in the campaign's final weeks. Mrs. Obama plans to be in the audience in Denver when her husband takes the debate stage.

