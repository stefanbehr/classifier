The mother of a former Navy SEAL killed in Libya has called on Mitt Romney to stop talking about her son during his political campaign.
The mother of a former Navy SEAL killed in Libya has called on Mitt Romney to stop talking about her son during his political campaign.
A spokesman for the Republican presidential candidate says Romney will respect her wishes.
Romney in recent days has been telling voters of chance encounter with the former SEAL, Glen Doherty, at a Christmas party two or three years ago. Doherty was among four Americans killed in the attacks in Benghazi.
Romney told the story of his chance encounter with Doherty at least twice in the last two days as part of a larger push to show a more personal side and criticize President Barack Obama's foreign policy. Romney, like other Republicans, have repeatedly raised questions about the president's handling of the Sept. 11 attack.
Indeed, the Benghazi terrorist strike that left U.S. Ambassador Christopher Stevens dead was a centerpiece of Romney's high-profile foreign policy speech at the Virginia Military Institute earlier in the week.
On Wednesday, Doherty's mother, Barbara Doherty, told Boston television station WHDH that she wanted Romney to stop citing her son.
"I don't trust Romney. He shouldn't make my son's death part of his political agenda. It's wrong to use these brave young men, who wanted freedom for all, to degrade Obama," Barbara Doherty said.
Romney spokesman Rick Gorka said: "Gov. Romney was inspired by the memory of meeting Glen Doherty and shared his story and that memory, but we respect the wishes of Mrs. Doherty."
Romney told the Doherty story at his first stop in Ohio on Wednesday, but did not at his next two public appearances.

