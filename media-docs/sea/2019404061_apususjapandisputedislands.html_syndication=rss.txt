Japan said Thursday its security alliance with the U.S. is an important deterrent against conflict breaking out between China and Japan over disputed islands.
Japan said Thursday its security alliance with the U.S. is an important deterrent against conflict breaking out between China and Japan over disputed islands.
Tensions between the Asian powers have spiked since Japan last month nationalized some of the tiny islands in waters between the two countries. Japan's ambassador to Washington Ichiro Fujisaki said that he trusts the leaders of China and Japan will avoid a war.
Fujisaki said the issue should be dealt with calmly and without resorting to force or coercion, or allowing nationalist sentiment to get out of control.
"It's important that we should not make an emotional issue out of this. We should calmly discuss where we can on these issues and always should respect law," the ambassador told the Brookings Institution think tank. He reiterated Japan's stance that the sovereignty of the island is not in dispute.
The dispute over the uninhabited islands, called Senkaku in Japan and Diaoyu in China, set off violent protests in China that targeted Japanese-owned businesses. A widespread call in China to boycott Japanese goods threatens its economic recovery after last year's devastating earthquake.
Fujisaki acknowledged China had controlled the unrest, but said the dispute should not prompt "business or economic action by one country against another."
The U.S. has called for "cooler heads" to prevail in the dispute. It has nearly 50,000 forces based in Japan. Washington takes no position over the islands' sovereignty but says they are covered by a 1960 security treaty requiring the U.S. to aid Japan if attacked.
Fujisaki said the security arrangements constitute "an important deterrence."

