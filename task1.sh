#!/usr/bin/env bash

# filepaths

train_dir=/opt/dropbox/12\-13/570/project1/training # training files
test_dir=/opt/dropbox/12\-13/570/project1/test # test files
dev_dir=/opt/dropbox/12\-13/570/project1/devtest # devtest files

# mallet stuff

MALLET="/NLP_TOOLS/tool_sets/mallet/latest/bin/mallet"

# extract features

echo "Processing training data..."
python2.7 trainer.py --base-dir=$train_dir >train.txt

echo "Processing devtest data..."
python2.7 trainer.py --base-dir=$dev_dir >devtest.txt

echo "Processing test data..."
python2.7 trainer.py --base-dir=$test_dir >test.txt

# convert to binary

echo "Converting training vectors..."
$MALLET import-svmlight --input train.txt --output train.vectors

echo "Converting devtest vectors..."
$MALLET import-svmlight --input devtest.txt --output devtest.vectors --use-pipe-from train.vectors

echo "Converting test vectors..."
$MALLET import-svmlight --input test.txt --output test.vectors --use-pipe-from train.vectors

# train models

echo "Training model, testing, and saving output..."

# testing against devtest
$MALLET train-classifier --training-file train.vectors --testing-file devtest.vectors --trainer MaxEnt --output-classifier model >devtest-results.txt

# testing against test
$MALLET train-classifier --training-file train.vectors --testing-file test.vectors --trainer MaxEnt --output-classifier model >test-results.txt

echo "Done."