#!/usr/bin/env bash

# filepaths

EXPECTED_ARGS=1

if [ $# -ne $EXPECTED_ARGS ]
then
    echo "Please provide media directory path"
    exit
fi

media_dir=$1 # media files
train_dir=/opt/dropbox/12\-13/570/project1/training # training files

# mallet stuff

MALLET="/NLP_TOOLS/tool_sets/mallet/latest/bin/mallet"

# extract features

echo "Processing media data..."
python2.7 trainer.py --base-dir=$media_dir --ct-min=4 >media.txt

if [ ! -e train.txt ]
then
    echo "Processing training data..."
    python2.7 trainer.py --base-dir=$train_dir --ct-min=4 >train.txt
else
    echo "Training data already processed."
fi

# convert to binary

echo "Converting training vectors..."
$MALLET import-svmlight --input train.txt --output train.vectors

echo "Converting media vectors..."
$MALLET import-svmlight --input media.txt --output media.vectors --use-pipe-from train.vectors

# classify media data

echo "Running classification..."

$MALLET train-classifier --training-file train.vectors --testing-file media.vectors --trainer MaxEnt --output-classifier modelMedia >media-report.txt

echo ""
echo "Generating average scores and rankings..."

$MALLET train-classifier --training-file train.vectors --testing-file media.vectors --trainer MaxEnt --output-classifier modelMedia --report test:raw >media-report-raw.txt

python2.7 avger.py <media-report-raw.txt >scores-and-ranks.txt

if [ ! -e Project1-2-output.txt ]
then
    cp scores-and-ranks.txt Project1-2-output.txt
fi

echo "Average scores, left rank and right rank dumped to scores-and-ranks.txt"

echo "Done."