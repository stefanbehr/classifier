It s really no surprise that recent polls show that women are driving President Obama s nationwide leads. Just yesterday, CNN/ORC released a poll that shows that women back President Obama over Mitt Romney by 16 points. That s because women are paying attention to the stark choice we face in this election, and we know that we need a leader who understands and is committed to shattering the glass ceiling that still exists for so many women in the workplace and in every aspect of their lives. 

We have that leader in President Obama. 

When asked, Mitt Romney s campaign didn t even know where their candidate stands on Lilly Ledbetter and fair pay. That should be a no brainer and for President Obama, it is. The Lilly Ledbetter Fair Pay Act was the first bill President Obama signed into law. It goes to the heart of the equal pay issue, giving women the power to challenge discrimination where they work. That s change that matters for all women. 

Change is also signing the Recovery Act into law to reverse our economic free-fall. Through this law, the Obama administration has issued more than 2,300 microloans and invested more than $3 billion in 12,000 grants to women-owned small businesses. That s a big deal. Romney thinks it made our economy worse. 

Change is nominating two women to the Supreme Court including the first Latina and seven women to cabinet-rank positions. 

Change is the President s steadfast defense of a woman s right to choose. 

Today, Romney is campaigning in Pennsylvania but no amount of Etch-A-Sketching will win back the female voters he s so thoroughly alienated. We ve seen Romney s true colors. And for women in Pennsylvania and across the country, the choice in this election is obvious. 

On a recent trip to Philadelphia, I joined Rep. Allyson Schwartz and fired-up women to stand up in support of the change President Obama has brought about for the women of this country. I want to make sure you hear directly from the Pennsylvania women I met on why they back our President. 

Take a look at this short video, and pass it along to women you know. 
