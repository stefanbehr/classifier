Jason Rae, Chair of the DNC Youth Council and DNC member from Wisconsin, issued the following statement regarding the Supreme Court s decision on the Affordable Care Act: 

Today s decision by the U.S. Supreme Court is a victory for all young people in this country. Unfortunately, there are people like Mitt Romney and John Boehner who don t understand the issues that face the Millennial generation. 

The Affordable Care Act ensures that young people never have to choose between going in debt and medical care. Among the components that truly benefits young people is a provision that allows young Americans to remain on their families health insurance until the age of 26 while they finish school, look for a job, or if their employer doesn t offer coverage. Just this month, the US Department of Health and Human Services released numbers showing that over 3.1 million young adults gained access to insurance because of this provision. 

In addition to providing coverage for young Americans until age 26, the Affordable Care Act also prohibits insurance companies from denying coverage to children because of pre-existing conditions. And by 2014, insurance companies will be prohibited from denying coverage to anyone, regardless of age, on the basis of a pre-existing condition. 

It s sad that Mitt Romney, author of a similar plan in Massachusetts while he was governor, and his Republican friends in Congress, think they need to do away with a piece of legislation that has already helped millions of Americans and will help millions more in the years to come. Today s victory in the Supreme Court shows that they are on the wrong side of this issue and ultimately will be on the wrong side of history. 

On behalf of young Americans all over this country, we express our sincere gratitude to President Obama for enacting and implementing this historic piece of legislation. 

The young people of the Democratic Party are proud of the Affordable Care Act and will continue to fight with President Obama and Democrats in Congress to make sure the law is fully implemented and that young Americans continue to benefit in the years ahead. We won t stand by and let Mitt Romney and Republicans in Congress do away with these important advancements in health care. 
