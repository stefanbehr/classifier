President Obama released a statement today commemorating National HIV Testing Day: 

National HIV Testing Day highlights the importance of HIV testing and the fight against HIV/AIDS. Of the over 1.1 million Americans living with HIV, more than 200,000 are unaware of their infection, and may unknowingly be transmitting the virus to others. Knowing your HIV status is a vital step toward accessing life-extending treatment for HIV, and thanks to ongoing research, that treatment is more effective than ever. 

Read the rest of President Obama s statement here . 

In 2010, the President and his administration released the nation s first National HIV/AIDS Strategy. The strategy aims to reduce infections with special emphasizes on testing and knowing your HIV status. Two years into its implementation, the strategy is also expanding services for those living with HIV/AIDS and continuing to reduce HIV-related health disparities for many Americans and their families. 

For more information on National HIV Testing Day and the Administration s efforts to fight HIV/AIDS, visit http://www.aids.gov/ . 
