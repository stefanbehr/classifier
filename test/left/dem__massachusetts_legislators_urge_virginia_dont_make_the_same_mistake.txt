David Linsky has represented Massachusetts s 5th Middlesex District for nearly 14 years. When Mitt Romney was elected to the governorship in 2002, Linsky says he was optimistic. But it didn t take long for disappointment to set in both as a legislator and a member of the middle class. 

"I thought he would be nonpartisan and issue-oriented," Linsky says. "I think that very quickly changed because he almost immediately began running for president and he became very conservative, particularly on social issues. After a very short period of time, he also showed an amazing lack of interest in being governor. He was not interested in the details of running our state." 

What Romney did manage to do in office, however, was notable in all the wrong ways. For all his so-called business experience, Romney failed to translate his personal success in the private sector to success in governing. "I think that, by and large, he thought that he could be governor the same way he was CEO of the company: directing other officials as if they were his employees," says Linsky. "And that s not the way government works." 

Romney had campaigned on more jobs and less debt but in the four years he spent in office, he broke every one of those pledges. As Linsky noted in his press conference in Richmond, Virginia, Massachusetts had dropped to 47th out of 50 in job creation. "That s not a record to be proud of," he says. 

Kathi-Anne Reinstein, who has also served in the Massachusetts legislature for nearly 14 years, adds, "He left us in all kinds of debt. He shifted where money was allocated to make it look like things were OK, but he left us in a mess he really did." In fact, by the time Romney left office, Massachusetts ranked first in per capita debt. And while Romney vowed not to raise taxes, he found a loophole: Reinstein notes he raised fees on just about everything from EMTs to plumber licenses. "Now you ve hurt the people who need help the most and that was his record," she says. 

What surprised and upset both legislators was how conservatively Romney governed on social issues. Reinstein cites this story as an example she ll never forget: "When we worked on marriage quality, he sent us all a copy of the Constitution with an incredibly condescending letter saying, in so many words, that you re not as educated as me, so here s a copy of the Constitution. But here s the thing: The Constitution has only been amended to add rights for the people not to discriminate. I remember that very well." 

So today these Massachusetts legislators are on a bus criss-crossing Virginia in advance of Mitt Romney s arrival with an urgent message for the American voting public: Don t repeat Massachusetts s mistake. Don t send Romney to the White House. Romney Economics didn t work in Massachusetts, and it won t work now. 

Learn more at RomneyEconomics.com . 
