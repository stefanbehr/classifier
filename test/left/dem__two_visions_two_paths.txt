This afternoon at a community college in Cleveland, Ohio, President Obama delivered a speech about our nation s economy and the choices we have to make this November. The President explained in black-and-white terms the key differences between himself and Governor Romney and the real choices Americans are making when casting their votes in November. 

With Governor Romney, Americans can choose the same path of tax cuts for the wealthy and less spending on programs that keep our country on the cutting edge the same path that landed us in the worst recession since the Great Depression. Or they can choose four more years of job growth, support for the middle class, and innovation. 

In his speech, the President advocated growing our economy through a strong, educated, and well-trained middle class, rather than trying the same trickle-down theories that provided benefits for the wealthy and detriment to the rest of the nation. President Obama demonstrated his strong conviction that through hard work and a growing middle class, our nation can pay down its debt, grow our economy, and create more jobs. 

Watch the entire speech now by clicking here . 
