Well, that didn't take long. Just one week after calling the birther conspiracy theory nonsense , probable GOP presidential candidate Mike Huckabee raised concerns of his own about the president's upbringing, in an interview with a conservative New York talk show host. In an appearance Monday on the Steve Malzberg show, the Fox News personality and former Arkansas governor appeared to sympathize with his host's questions about President Obama's citizenship, and then floated a theory of his own: Obama was raised in Kenya. Per Media Matters : 

I would love to know more. What I know is troubling enough. And one thing that I do know is his having grown up in Kenya, his view of the Brits, for example, very different than the average American. 

But don't worry, he's not a birther: 

The only reason I'm not as confident that there's something about the birth certificate, Steve, is because I know the Clintons [inaudible] and believe me, they have lots of investigators out on him, and I'm convinced if there was anything that they could have found on that, they would have found it, and I promise they would have used it. 

Huckabee went on to explain how Obama's Kenyan upbringing imbued him with an anti-British worldview radically different than most Americans. (You know, like the guys who wrote this anti-British screed .) Media Matters has the full audio here . 

Just to be clear: Obama was not raised in Kenya. So what exactly does he think the President is hiding? I contacted Huckabee through his PAC for a response; we'll let you know if we hear back. 

Huckabee's assertion about Obama's childhood haunts is decidely fringey, but his comments about the President's attitude toward the British should sound familiar. He's parroting the argument made in Forbes last fall by Dinesh D'Souza : that Obama's decision-making is informed by a distinct Kenyan, anti-colonialist worldview. The piece earned praise from one of Huckabee's likely primary challengers, Newt Gingrich, who announced the formation a presidential exploratory committee this week. 

Update: Huckabee spokesman Hogan Gidley tells Ben Smith The governor meant to say the President grew up in Indonesia. But it's worth noting Huckabee did more than just misidentify Obama's childhood residence; he misidentified everything about Obama's childhood. As Huckabee explained : [Obama's] perspective...growing up in Kenya with a Kenyan father and grandfather, their view of the Mau Mau Revolution in Kenya is very different than ours because he probably grew up hearing that the British were a bunch of imperialists who persecuted his grandfather. Whether it was Kenya or Indonesia, Obama didn't grow up with his Kenyan father or his Kenyan grandfather. Huckabee's not a birther, but he's either playing fast and loose with the facts or he doesn't really know them.
