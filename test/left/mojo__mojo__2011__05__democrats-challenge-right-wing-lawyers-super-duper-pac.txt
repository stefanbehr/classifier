A pair of Democratic strategists have challenged right-wing lawyer James Bopp and his new scheme to use members of Congress to drum up unlimited cash for what you might call the GOP's new super-duper PAC . 

In a letter to the Federal Election Commission (FEC) sent today, Monica Dixon and Ali Lapp, the directors of two new super PACs intended to bolster congressional Democrats in 2012 , have questioned the legality of Bopp's new venture, simply called Republican Super PAC. While federal law caps campaign donations directly to candidates at $2,500 a year, Bopp's plan would harness the fundraising prowess of politicians to funnel donations to Bopp's outfit the donors could even tell Republican Super PAC to earmark their money for particular race. The key, Bopp told my colleague Stephanie Mencimer , is that coordination only applies to spending, not to the fundraising. What Bopp's saying is that while PACs like his cannot directly coordinate with candidates or elected officials on TV ads, mailers, or other types of campaigning, it's perfectly legal to ask candidates to raise money for his PAC. 

Dixon and Lapp, however, want the FEC to take a look at Bopp's strategy and declare if it's legal or not. Pointing to federal statute, their attorneys say that Bopp's plan would appear to prohibit [federal elected officials, candidates for federal office, and national party committee members] from soliciting unlimited individual, corporate, and union contributions on behalf of PACs like Bopp's. In an accompanying statement Dixon and Lapp said: We are seeking immediate clarification from the FEC in order to ensure that our organizations operate fully within the law and in order to assure operational equivalency between Republicans and Democrats. 

Which is to say, if the FEC approves of what the other guys are doing with their super-duper PAC, we should be able to do it as well. 

Here's the full letter: 

Advisory Opinion Request - IE PAC Solicitations 

Print Email Tweet Jon Huntsman s Climate Problem Did ICE Intentionally Mislead? Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

The GOP s Super-Duper PAC 

If you thought big corporations owned Congress, just wait until candidates start raising unlimited money from them. The Man Behind Citizens United Is Just Getting Started 

Meet the lawyer who could turn our elections upside down. Consumer Protection s Citizens United 

In a high stakes case over your right to sue for corporate abuses, guess which side the Chamber of Commerce is on? Inside the Democrats Outside Money Machine 

Can this team of political operatives match Karl Rove, the Koch brothers, and the right wing's dark-money juggernaut? Crashing the Covert Campaign Spending Spree 

What do secretive independent expenditure groups hate more than disclosure rules? When we show up at their doors with a camera rolling. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
