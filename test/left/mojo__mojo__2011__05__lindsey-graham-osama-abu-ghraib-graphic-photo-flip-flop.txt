Now beginneth the backlash against the White House for its decision not to release photos of Osama bin Laden's bloated, bullet-busted corpse. According to ABC News : 

Sen. Lindsey Graham, R-SC, believes President Obama's decision not to release the Osama bin Laden photos is a mistake that will unnecessarily prolong this debate over the death of the world's most wanted man. 

I respectfully disagree with President Obama's decision not to release the photos. It's a mistake, Graham said today. 

Well, at least it's a respectful disagreement. But as long as Graham's demanding government accountability, perhaps he can explain why he was against releasing graphic US military photos before he was for it. 

Back in 2009, he fought almost singlehandedly to keep additional photos of harsh inmate abuse in Iraq's Abu Ghraib prison out of public view. Every photo would become a bullet or IED used by terrorists against our troops, he said, while threatening to block funding for the wars in Afghanistan and Iraq if the pics were released. He and Sen. Joe Lieberman (I-Conn.) put out a joint release explaining their desire to suppress the pictures: 

The photos do not depict anything that is not already known. Transparency, and in this case needless transparency, should not be paid for with the lives of American citizens, let alone the lives of our men and women in uniform fighting on our behalf in Iraq, Afghanistan and elsewhere...Such a release would be tantamount to a death sentence to some who are serving our nation in the most dangerous and difficult spots like Iraq and Afghanistan. 

Strangely, that's similar to what President Obama said about the Abu Ghraib photos and the bin Laden shots. Obama's desire to suppress evidence of American service members' handiwork, good and bad, may be disheartening to transparency advocates, but at least he's being consistent. Graham, on the other hand, would do well to explain his photographic flip-flop.
