With nearly a year-and-a-half to go before election day, Priorities USA , the new presidential super PAC founded by two ex-Obama aides, is out with its first advertisement of the nascent 2012 presidential campaign. The ad targets Mitt Romney , accusing the former Massachusetts governor of flip-flopping on the issue of health care on the eve of his visit to South Carolina this weekend. 

The ad opens with fellow 2012 contender Newt Gingrich's recent criticism of Rep. Paul Ryan's budget plan that would, among other things, eviscerate Medicare. (On NBC's Meet the Press , Gingrich called Ryan's plan right-wing social engineering a description that Gingrich quickly retracted in the face of massive criticism from conservatives.) Then the ad highlights Romney's support for Ryan's plan, which is followed by an image of Romney during his recent health care speech in Michigan, in which he defended his universal reform plan in Massachusetts a plan that is anathema to GOPers like Paul Ryan. The ad concludes by asking, With Mitt Romney, you have to wonder: which page is he on today? 

Here's the ad in full, with a script afterward: 



The script: 

Newt Gingrich says the Republican plan that would essentially end Medicare is too radical. Governor Haley thinks the plan is courageous, and Gingrich shouldn't be cutting conservatives off at the knees. Mitt Romney says he's on the same page as Paul Ryan, who wrote the plan to essentially end Medicare. But with Mitt Romney, you have to wonder: which page is he on today? Priorities USA Action is responsible for the content of this advertisement. 

Bill Burton, a former press secretary in the Obama White House who co-founded Priorities USA , declined to put a price on the ad buy, but described it in an email as a statewide, election-year level buy over the course of his short trip. He added, If you are watching the news this weekend in South Carolina, you will see this ad. 

Priorities USA is hardly the first group to hit Romney for his mushy position on health care. Indeed, as Mother Jones reported , Romney's 2008 New Hampshire campaign director, Bruce Keough, ruled out working for Romney in 2012 because of the former governor's wishy-washy political identity. Romney, Keough told me , manages to say things that cause people to think, 'Wait a second: I thought I knew him, and now I'm not so sure.' I think he can be successful. But I don't think he will be successful if he runs his campaign like he did in 2008. 

In the months to come, expect plenty more ads like this one hitting Romney for his changing positions, especially when it comes to health care. 

Print Email Tweet As Need Grows, States Slash Welfare Benefits Bachmann s Anti-Gay Ally: Obama s Not a Christian Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Ex-Romney Campaign Chair: I m Done With Mitt 

A key Romney '08 campaigner jumps ship, saying he can't handle the candidate's ever-changing persona. Mitt Romney and the Al Gore Problem 

Inside the Democrats Outside Money Machine 

Can this team of political operatives match Karl Rove, the Koch brothers, and the right wing's dark-money juggernaut? Issa Stacks the Deck Against Obama s Dark Money Directive 

Congress' top GOP watchdog shuts out a leading campaign finance reformer from a crucial hearing. The Left s American Crossroads? 

Democrats are gearing up for the 2012 shadow spending wars. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
