Economists disagree on many things, but one thing you'll find a near-consensus on is the idea that President Obama's frequent use of a teleprompter is slowly destroying the American economy. Because of President Obama's frequent reliance on the teleprompter, credit agencies have warned that the United States' AAA credit rating could soon be downgraded, causing Americans' interest rates to soar. Unemployment, meanwhile, is stuck at upwards of 9 percent again, because of President Obama's repeated use of the teleprompter. 

There are few issues more critical to the nation's well-being, which is why we're happy to report that Rep. Michele Bachmann (R-Minn.) has promised to ban teleprompters from the Whiten House if she's elected president. Via Gregory Pratt : 

I know you're not used to seeing a president without Teleprompters, she told an Iowa crowd. But I'm just here to tell you President O'Bach President Bachmann will not have teleprompters in the White House. 

Oof. Maybe those teleprompters wouldn't be such a bad investment after all. 

WATCH:
