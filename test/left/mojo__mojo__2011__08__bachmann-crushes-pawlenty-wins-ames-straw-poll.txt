So it begins. Two months after kicking off her presidential campaign in her reclaimed Iowa hometown, Rep. Michele Bachmann (R-Minn.) scored a resounding victory in the Ames Straw Poll , a non-binding event that doubles as a fundraiser for the Iowa Republican Party. Texas Congressman Ron Paul finished just 150 votes back in second place (out of nearly 17,000 cast), but it was Bachmann's drubbing of her fellow Minnesotan Tim Pawlenty that immediately stands out and raises questions about whether there's still room in the race for the formerly mulleted, former governor of Minnesota. 

While today's event is technically meaningless the votes won't count until the caucuses are held in December it was the first serious test of the candidates' appeal and manpower. You could think of Ames as a sort of highly concentrated get-out-the-vote contest : The Bachmann and Pawlenty campaigns both bused in supporters from as far away as Davenport on the Illinois border and covered the $30/per ticket fee for supporters to cast their votes. Pawlenty spent the past week drumming up support across the state, but Bachmann's organizational dominance was obvious from the minute I parked my car. She employed a fleet of golf carts to shuttle elderly attendees to-and-from the parking lot and the voting booths, and an army of volunteers focused intently on sheparding supporters toward the polls. 

Registering for the event was a prerequisite for entering Bachmann's cavernous (if somewhat pungent-smelling ) tent, where she entertained voters with live Christian rock and a special performance from country music star Randy Travis (at that point, the crowd spilled well out of the tent and brought in dozens of supporters of other candidates). Bachmann's win also owes something to the underlying qualities that go unmentioned in her speech but which have made her a hit among Iowa conservatives. As I noted earlier , Wallbuilders co-founder Rick Green, a Christian Reconstructionist who believes Christians have an obligation to take over government, spoke on Bachmann's stage in the morning and revved up the crowd with a message that offered the audience a glimpse of the candidate's roots as a proponent of Biblical Constitutionalism. Christian values: good; Moral relativism: Bad. 

The dreadlocked Bachmann supporter in the Jesus is my Rock t-shirt might beg to difer, but in Iowa, Bachmann demonstrated that, at least in this truncated version of the GOP field, she is a rock star. Bachmann commanded a crowd twice as large as anyone else's when she showed up at the Iowa State Fair to speak from the Des Moines Register soapbox, and she was the only one who had a state police escort. At her speech in Ames, she whipped the crowd into a frenzy (Only Paul, who was an anti-government crusader before it was cool, could come anywhere close). This, from Sandra Beak of Illinois, was a typical reaction from the congresswoman's supporters: I saw Michele Bachmann last night Oh my gosh! That woman is energetic! She never stops! It's amazing! 

Those are four things no one has ever said about Tim Pawlenty. Leading up to the poll, Pawlenty continued to do all the little things that candidates are supposed to do serving up the best barbecue and handing out Dairy Queen blizzards; fine-tuning his stump speech over the course of the week into one that, at least on paper, finally seemed to work; showing up on time and staying on message but the enthusiasm just wasn't there. 

On the day that Texas Gov. Rick Perry finally stepped into the ring , the Ames results carry with them a serious disclaimer: We're about to hit restart on the whole race. Mitt Romney, the presumed front-runner from day one, chose to spend the day in New Hampshire and skip the straw poll entirely. The results don't mean everything, but they weren't meaningless either. If nothing else, consider this: two-thirds of Ames voters chose candidates Paul and Bachmann who were decidedly in the fringe of the party in 2008. This is your new GOP. 

Update: Here's the final tally. Out of 16,892 votes: 

4,823 for Bachmann 

4671 for Paul 

2293 for Pawlenty 

1657 for Santorum 

1456 for Cain 

718 for Perry (write-in) 

567 for Romney (skipped) 

385 for Newt (skipped) 

69 for Huntsman (skipped) 

35 for McCotter
