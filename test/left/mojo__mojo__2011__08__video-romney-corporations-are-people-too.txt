At an Iowa State Fair appearance Thursday morning, former Massachusetts Gov. Mitt Romney responded to a heckler who was calling for increased taxes on corporations by summoning his inner John McCain. Corporations are people, my friend, the GOP presidential candidate said. (The Washington Post 's Greg Sargent has a complete transcript of the exchange .) 



Romney, who won the Ames Straw Poll in 2007 but is skipping it this year, spoke for about 20 minutes at the Des Moines Register's state fair soapbox. In line with conventional wisdom that the New Hampshire primary which, unlike the Iowa caucuses, is widely regarded as a must-win for Romney is more focused on fiscal policy than social issues, Romney spent the entire time focused on the economy. 

I'm not going to raise taxes, Romney said, as the hecklers chanted Scrap the cap! (on Social Security) and Wall Street greed! He said he could get behind closing loopholes exploited by big banks if it didn't involve a tax hike, and promised a peacock farmer among the crowd of several hundred Iowans and reporters that he would make the business climate more conducive to his feather-export trade. 

He also insisted that the United States has the highest corporate tax rate in the world a questionable claim, at best . 

Afterwards, Romney refused to take questions from a swarm of reporters who encircled him as he walked toward the fair's agriculture building, home to the famed butter cow, where he met up with Sen. Chuck Grassley (R-Iowa) on his way to the Iowa Pork Tent. 

Asked by Mother Jones if he planned to focus more on social issues in the future, and particularly if he stood behind his promise to appoint a presidential commission to investigate the harassment of gay marriage opponents, Romney said only, What I don't do is discuss important issues on the fly at a fair as I get the chance to say hi to fairgoers. (Yesterday, Rick Santorum told us that he stood firmly behind the pledge, penned by the National Organization for Marriage for GOP candidates, to launch such a commission.) 

Despite all the political heat, Romney had no complaints. You can't get much better than this for weather, Grassley said, to which Romney replied, It's not too hot, nice sun, no rain, before wandering off to pose for a photo with last year's Iowa State Fair Queen.
