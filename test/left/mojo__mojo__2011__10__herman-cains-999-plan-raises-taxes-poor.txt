If you watched Tuesday evenings' GOP presidential debate, you heard a lot about Herman Cain's 9-9-9 plan . For the unfamiliar, it's pretty straightforward: 9 percent corporate tax, 9 percent sales tax, and 9 percent income tax. Win-win-win! Or maybe not. Cain was asked at the debate to explain away the charge that his 9-9-9 plan would effectively raise taxes on low-income workers. (Among other things, Cain's plan would implement a sales tax on groceries, which only two states currently do.) 

Cain rejected the notion, but the facts are pretty clearly not on his side. Don't take it from me, though. None other than Bruce Bartlett, a former economic adviser to Ronald Reagan and George H.W. Bush, says so. Here's how he explained it at the New York Times ' Economix blog earlier this week: 

It's important to understand that the 9 percent rates on personal and business income would apply to very different tax bases than now exist. For individuals, the tax would apply to gross income less only the deduction for charitable contributions. No mention is made of a personal exemption. 

This means that the 47 percent of tax filers who now pay no federal income taxes will pay 9 percent on their total income. And elimination of the payroll tax won t even help half of them because the earned income tax credit, which Mr. Cain would abolish, offsets both their income tax liability and their payroll tax payment as well. 

Additionally, everyone would now pay a 9 percent sales tax on all purchases. No mention is made of any exemptions from this tax, so we may assume that it will apply to food, medical care, rent, home and auto purchases and a wide variety of other expenditures now exempt from state sales taxes. This would increase their cost of living by 9 percent while, at the same time, the poor would pay income taxes. 

Bartlett, no lefty, calls Cain's plan a distributional monstrosity. The Center for American Progress, meanwhile, found that the lowest quintile of earners would pay nine times more under the 9-9-9 plan 18 percent of their total income, versus 2 percent today.
