The first phase of Monday's Occupy Oakland-led West Coast port shutdown was, by protesters' accounts, a success: Port terminals were shut down in Oakland and in Longview, Washington the site of an ongoing contract fight with a subsidiary of agribusiness giant Bunge. In Long Beach, San Diego, and Vancouver, attempts to shut down the respective ports were less successful, with protesters blocking access to the three ports for about an hour before police forced them to disperse. Police arrested five demonstrators in San Diego and at least two in Long Beach . 

In Oakland, protesters exchanged heated words with angry port workers who were anxious to be paid. Among these workers were members of the International Longshore and Warehouse Union (ILWU), whose leadership had spoken out in opposition to the shutdown. 

At one blockade, a trucker sparred verbally with a protester as the two recorded video of each other. The trucker called the protesters hooligans who were hurting the working class. Nearby, a union electrical worker laughed and called protesters morons. 

Mark Hebert, a Utah trucker working as an independent contractor, was heckled by protesters as he complained to the media about the shutdown. Hebert said he typically dropped cargo off at the port once a week and risked losing $200 to $400 in pay. 

But several truckers honked in support of Occupy Oakland, and some ILWU workers strongly supported the port's second shutdown. (Oakland's general strike on November 2 resulted in a port shutdown, costing the city a reported $4 million and putting more than 10,000 port workers temporarily out of work.) One, a business agent watching the blockade from a company van who said his name was L.T., told me that protesters need to stay your ground. I'm not crossing the picket line. Responding to ILWU president Robert McEllrath's letter criticizing the shutdown , he said, We don't give a damn. 

Oakland protesters will reconvene later for an evening march to set up a blockade ahead of a 7 p.m. port shift change, and occupiers in other cities plan evening actions to keep their terminals out of service as well. 

That didn't sit well with Hebert, who cut our conversation short to return to his truck for a breather. On his way, he kicked over a sign reading truckers have rights to union wages a good reminder that Occupy's success today may depend upon winning the public-relations battle for working-class sympathy.
