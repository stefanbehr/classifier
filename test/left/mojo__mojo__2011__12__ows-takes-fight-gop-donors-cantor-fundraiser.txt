Photo by Stephanie Mencimer Occupy Wall Street and other protesters in DC for a big Take Back the Capitol action Tuesday took their fight for the 99 percent right to the Republican power base: expensive lobbyists-fueled fundraisers. On Tuesday night, about 100 mostly unemployed activists rallied outside the swank Lincoln restaurant downtown, where House Majority Leader Eric Cantor (R-Va.) was holding a $2,500 a plate fundraiser for his leadership PAC. 

In no small bit of irony, Cantor had themed the fundraiser a Festivus event . Seinfeld fans may recall that Festivus is a fictional holiday created for the show. It involves an undecorated aluminum Festivus pole and rituals including the airing of grievances. (On the Seinfeld episode, Frank Costanza declares , The tradition of Festivus begins with the Airing of Grievances. I got a lot of problems with you people! And now, you're gonna hear about it. ) 

The invitation to Cantor's fundraiser asked attendees to come and air your grievances. So the protesters did. In between heckling donors and GOP members of Congress and chanting for millionaires to pay their fair share, the protesters stopped for the occasional mic check, in which they had an unemployed person step forward to tell his or her tale of woe. ( I used to be a science teacher... ) Many of the people in the crowd had been flown in from Idaho by the Service Employees International Union, which helped organize the protest. 

You can watch the video footage of Tuesday night's Cantor protest below, including a scene where a Cantor staff member talks to a Channel 4 reporter with her back turned before having him tossed out: 




The Lincoln restaurant has big plate-glass windows looking out on to the street, which would have given diners a good view of all the ruckus outside, but the restaurant staff drew the curtains so that the protesters had to make due with several bullhorns. The noise was significant, and despite the curtains, the protest had its intended effect. According to admittedly unreliable sources on the scene, Cantor lasted only seven minutes at the fundraiser before taking off, prompting the protesters to declare victory. An SEIU organizer on hand was quick to observe that, while Cantor might still have raised significant funds at the event, at least the donors didn't get access to him in exchange for their donations. 

Cantor has been a regular target of such protests as Occupy Wall Street activists have focused on his lucrative ties to Wall Street and the financial industry, which has donated hundreds of thousands of dollars to his campaigns and leadership PAC. In October, Cantor abruptly canceled a speech shortly before it started at the University of Pennsylvania after discovering that it would be open to the public. Hundreds of protesters had shown up for the event. And now, the Occupy movement is honing in on his donors. 

Cantor isn't the only member of Congress whose fundraisers have been disrupted by protests. Earlier in the day, protesters had crashed a fundraiser for Rep. Sam Johnson (R-Texas) at the Charlie Palmer Steakhouse. Such actions are likely to continue throughout the week. Given the reaction to last night's event, the OWS folks might be on to something. 

Do you appreciate fair and factual reporting on Occupy Wall Street? Please donate a few bucks to help us expand our coverage . Print Email Tweet Rick Perry: Kids Can t Even Celebrate Christmas Anymore Perry Slams Obama For Supporting Human Rights For Gays Stephanie Mencimer 

Reporter 

Stephanie Mencimer is a staff reporter in Mother Jones ' Washington bureau. For more of her stories, click here . You can also follow her on Twitter . RSS | Twitter If You Liked This, You Might Also Like... 

House GOP Leader Eric Cantor Calls Occupy Protests Mobs 

Another conservative joins the chorus of Occupy Wall Street bashers. Eric Cantor Aide Forms Super-PAC--Is He Angling for VP? 

In 2011, if you don't have a third party outfit raising money for you, you're doing it wrong. Quote of the Day: Cantor Bails 

Obama Meets Occupy Wall Street 

Guess Who s Coming to the GOP Fundraiser? 

One of the GOP hard-hitting political campaign managers in California is a punk musician and one-time druggie who disappears for... Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
