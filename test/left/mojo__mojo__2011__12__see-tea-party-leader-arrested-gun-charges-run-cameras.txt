Tea Party Patriots leader Mark Mecker has never seen a TV camera he didn't love. He seems to relish his Fox News appearances and hamming it up before the cameras at press conferences, rallies, and any other place he can get his mug on the big screen. But yesterday, Meckler was caught on tape literally running through oncoming traffic in New York City to avoid reporters, after he was arrested for illegally possessing a gun and charged with a felony. Meckler had gone to LaGuardia airport to catch a flight to LA and checked a locked case carrying a box that contained a Glock handgun and 19 cartridges of 9mm ammo. 

A local CBS news station, which shot the footage, describes the chase : 

As he left the courthouse after his arraignment he was desperate to avoid news cameras. A man accompanying Meckler put his hand over the lens of CBS 2 s camera and repeatedly tried to interfere with our photographer as Meckler raced through the courthouse to a side door. 

As two photographers gave chase, Meckler ran to Queens Boulevard, the infamous Boulevard of Death, and ran across one lane of traffic, jumped over a wrought iron barricade and then high-tailed it to the other side before disappearing into the night. 

Meckler has a concealed carry permit to carry the weapon in California, but that doesn't make it legal in New York City, where Meckler had apparently been tooling around for a few days (on a trip that his lawyer diplomatically described as temporary transit through the state). He claimed he brought the gun because he's gotten death threats, which raises the question of whether Meckler was actually packing heat during his entire stay in New York, which would be a big violation of the law there. After all, if he needs a gun because he's gotten death threats, it's not going to do him much good locked in a TSA-approved travel box. 

Which may be why Meckler was so eager to avoid reporters. Watch his amazing fence-leaping skills as he navigates the Boulevard of Death here:
