Rep. Joe Walsh (R-Ill.), a tea-party-backed freshman lawmaker, recently branded President Barack Obama a tyrant for announcing that the Department of Homeland Security would stop deporting certain young illegal immigrants who entered the United States as children. Then, doubling down on his criticism of the president, Walsh took back his use of the word tyrant because, he said, Obama really isn't smart enough to know what that means. 

Walsh's remarks were captured by Credo Super-PAC, a political arm of the phone company Credo Mobile. The Huffington Post first reported Walsh's tyrant rant. 

Here's the video and text of Walsh's comments, which he made last weekend at a town hall meeting in Elmhurst, Ill.: 



And again, fair is fair, you want a debate on the law, fine, have that debate. But right now it's a law on the books and you just told your law enforcement people don't enforce it. I was on one radio station and I said my god he's a tyrant. I don't know what else you call him. I don't want to give him that credit because I don't think he's smart enough. I think he's only doing this because he's campaigning, that's all the guy knows. So I don't want to call him a tyrant, because he really isn't smart enough to know what that means. But in one fell swoop he just made 800,000 illegal immigrants, let's call it legal, and gave them the ability to work here legally. 

Walsh is one of ten tea-party-affiliated members of Congress that Credo Super-PAC has targeted in this election year. Credo volunteers are following and recording the remarks of the Tea Party Ten in their districts. 

This isn't the first time Credo caught Walsh stuffing his foot into his mouth. Last month, Walsh was caught on camera saying that the Democratic Party wants Hispanics to be dependent on government just like African Americans. Activist and former presidential candidate Jesse Jackson, Walsh said, would be out of work if [African Americans] weren't dependent on government.
