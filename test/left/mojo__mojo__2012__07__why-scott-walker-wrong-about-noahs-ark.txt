Wisconsin Republican Gov. Scott Walker, fresh off a landslide recall victory , took to Twitter on Wednesday evening to ponder the conservative equivalent of What if we're all colorblind, and the people we thought were colorblind are the ones with normal vision? In what appeared to be a response to President Obama's recent riff on the importance of public institutions, Walker wondered: Imagine if Noah had needed help from the government to build the Ark. It might have never been built. 

Imagine if Noah had needed help from the government to build the ark. It might have never been built. Scott Walker (@ScottKWalker) July 19, 2012 

That's quite the thought experiment. In Walker's scenario, the government is so inept it would have scuttled the construction of the Ark, bringing about the end of mankind. (As it was, the Ark managed to only save eight people out of the entire global population so it wasn't exactly a huge victory for the private sector.) 

But the real story behind Noah's Ark isn't dependency, it's red tape. GAO reports from the pre-Flood era are understandably hard to come by, so the best records we have come from Genesis. In that telling, risk-takers like Noah were saddled (by Job's creator, no less) with cumbersome restrictions on everything from the kind of wood they could use to the size and breadth of the vessel and who would be allowed on: 

Make thee an ark of gopher wood; rooms shalt thou make in the ark, and shalt pitch it within and without with pitch. 

And this is the fashion which thou shalt make it of: The length of the ark shall be three hundred cubits, the breadth of it fifty cubits, and the height of it thirty cubits. 

A window shalt thou make to the ark, and in a cubit shalt thou finish it above; and the door of the ark shalt thou set in the side thereof; with lower, second, and third stories shalt thou make it. 

... 

And of every living thing of all flesh, two of every sort shalt thou bring into the ark, to keep them alive with thee; they shall be male and female. 

Of fowls after their kind, and of cattle after their kind, of every creeping thing of the earth after his kind; two of every sort shall come unto thee, to keep them alive. 

And take thou unto thee of all food that is eaten, and thou shalt gather it to thee; and it shall be for food for thee, and for them. 

Given the tight regulations, it's no wonder Noah only built one.
