# notes:
# 
# i marked a line with a comment that begins with "# DEBUG"; i think it could
# be useful to throw debugging stuff in and mark it like that so that we can
# find it easily later and take out
#
# TODO:
# 
# ignore words that occur fewer than 4 or so times.  we should pick a
# good limit.
# 
# TODON'T:
# 
# sort.  it doesn't matter what order features are in.  (someone asked
# on gopost.)

import re
import sys
import os

import extract

def print_help():
  print "options:"
#  print ############################################################################## <-- make sure messages here don't go pas teh end of the #s
  print "  --base-dir=<directory name> (mandatory) the script will read files from here"
  print "  --ct-min=[0-9]+ (optional; default 1) tokens with a count less than the"
  print "                             given number won't be included."
  print "  --include-punct=true|false (optional; default true) punct == punctuation."
  print "      (adding --exclude-punct to the arg list is the same as adding"
  print "       --include-punc=false to the arg list.)"
  sys.exit()

# deal w/ incoming args :-\
if len(sys.argv) < 2:
  print_help()

base_dir = ""
ct_min = 1 # this won't be used until the script goes to print stuff out
include_punct = True

for arg in sys.argv:
  tmp = arg.split("=")
  if len(tmp) == 2:
    if cmp(tmp[0], "--base-dir") == 0:
      base_dir = tmp[1]
    elif cmp(tmp[0], "--ct-min") == 0:
      ct_min = int(tmp[1])
    elif cmp(tmp[0], "--include-punct") == 0:
      if cmp(tmp[1].lower(), "false") == 0:
        include_punct = False
      elif cmp(tmp[1].lower(), "true") != 0:
        print "invalid value for --include-punct.  (valid values are 'true' and 'false'.)"
        sys.exit()
  elif len(tmp) == 1:
    if cmp(tmp[0], "--help") == 0:
      print_help()
    elif cmp(tmp[0], "--exclude-punct") == 0:
      include_punct = False

# make sure base_dir a valid dir
if len(base_dir) == 0 or not os.path.exists(base_dir) or not os.path.isdir(base_dir):
  print "please give the name of the training directory as the 1st parameter. (you gave '" + base_dir +"'.)"
  sys.exit()

# done dealing w/ incoming args :)
  
# keep track of a dict that maps words -> occurances  within each
# classification, so the structure of this thing is something like this:
# 
# tok_ct = {classification1: {word1: word1_ct, word2: word2_ct, ...},
#           classification2: {wordn: wordn_ct, wordn+1: wordn+1_ct, ...},
#           ...}
# 
# ...yay for nested dicts

tok_ct      = {} # (ct == 'count')

# pulling this out into its own function so that i can call it more than once - once for each type of n-gram we're gonna use
def add_tok_cts(cat, toks):
  global tok_ct
  for tok in toks:
    # tokens are like threats.  empty ones can be ignored.
    if len(tok) == 0:
      continue
    
    # process tokens a little
    tok = tok.lower()
    
    if tok in tok_ct[cat]:
      tok_ct[cat][tok] += 1
    else:
      tok_ct[cat][tok] = 1

# define a function that can be passed to os.path.walk
def visit_dir(base_dir, current_dir, file_names):
  global tok_ct
  
  # if this is the base_dir, do nothing; it's the subdirs of base_dir that we're interested in here
  if cmp(base_dir, current_dir) == 0:
    return
  
  tmp = current_dir.split('/')
  cat = tmp[len(tmp) - 1] # new_cat for new_category
  
  file_num = 0
  for file_name in file_names:
    new_cat = cat + "_" + str(file_num)
    file_num += 1
    
    tok_ct[new_cat] = {}
    full_name = current_dir + "/" + file_name
    
    # if file_name isn't actually a file name, ignore it and move to the next one
    if not os.path.isfile(full_name):
      print "not a file: " + full_name # DEBUG not really necessary to print out an error; we could just ignore anything that's not a file.
      continue
    
    f_hnd = open(full_name, 'r') # f_hnd is short for 'file handle'
    text  = f_hnd.read()
    f_hnd.close() # for good measure; don't need the handle any more anyways - i already read the entire file.
    
    # tokenize, grab n-grams, & count 'em
    toks = extract.extract_unigrams(text, include_punct)
    add_tok_cts(new_cat, toks)
    
    toks = extract.extract_bigrams(text, include_punct)
    add_tok_cts(new_cat, toks)
# end visit_dir function

# run that code i just wrote
os.path.walk(base_dir, visit_dir, base_dir)

# print out tok_ct in a nice, mallet-friendly kind of manner
items = tok_ct.items()
for item in items:
  print_me = ""
  cat      = ""
  
  tmp = item[0].split("_")
  for i in range(len(tmp) - 1):
    cat += tmp[i] + "_"
  
  cat = cat[0:len(print_me) - 1]
  
  toks = item[1].items()
  for tok in toks:
    val = tok[1]
    
    # a little post processing?
    
    # done post processing
    
    if val >= ct_min:
      print_me += " " + tok[0] + ":" + str(val)
  
  # only print something if there were toks whose ct was at least ct_min
  if len(print_me) > 0:
    print cat + print_me
