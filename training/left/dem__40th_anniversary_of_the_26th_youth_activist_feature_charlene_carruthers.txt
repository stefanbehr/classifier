July 1, 2011 marks the 40th anniversary of the ratification of the 26th Amendment to the Constitution, lowering the voting age from 21 to 18. Democrats are marking that occasion by shining a spotlight on young activists dedicated to improving the lives of Americans and bridging cultural divides. 

Charlene Carruthers was born and raised on the Southside of Chicago in the "Back of the Yards" neighborhood, famously organized by an emerging progressive movement in the 1930s. Charlene didn t learn of the neighborhoods famed lineage until she entered college, but nevertheless credits her upbringing for shaping the organizing work she does today. She spoke briefly about her activist roots: 

I ve been involved in the political process as an activist since the age of 18. I started community organizing while in St. Louis as a graduate student. For the past two years I ve worked on civic engagement and grassroots organizing campaigns. My work focuses on women, the African Diaspora and youth leadership development, I also write about and am highly engaged in the reproductive justice movement. 

Charlene began her activism organizing with the Black Student Union and the Student Senate at her university around diversity issues and the Darfur conflict. A trip to South Africa was a turning point for her-witnessing two segregated neighborhoods opened her eyes to a bigger world of conflict. She returned to the states with a renewed resolve and began graduate school, studying social work. 

Following Republican attacks on affirmative action, she began to educate her peers about what the damning anti-affirmative action legislation being proposed really meant-and how it would affect them. 

In 2008, Charlene found herself working on local races for Democrats in the Chicago area. That work led to two more years of field organizing, where she mobilized young people to advocate for health care reform, comprehensive immigration reform, and election protection. Today, she is a youth leadership developer-training the next generation of progressive leaders how to organize online and on the ground. 

When asked to reflect on the past 40 years of youth voters, Charlene said: 

The country has undergone major transition and young people have always been at the forefront of defining what change is. Having the right to vote is an essential piece of building power and allows you to harness that power in exponential ways. 

This weekend Charlene is celebrating the 40th anniversary of the 26th by training a group of youth to maximize their power by organizing both on and offline. 
