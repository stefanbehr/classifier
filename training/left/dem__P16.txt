President Obama's investments in clean energy and energy efficiency have supported nearly a quarter of a million jobs. 

President Obama s investments in clean energy and energy efficiency have supported nearly a quarter of a million jobs. In President Obama, we have a commander-in-chief who understands that our commitment to our veterans doesn't end when they return home. 

In President Obama, we have a commander-in-chief who understands that our commitment to our veterans doesn t end when they return home. As President Obama's personal aide during the 2008 campaign and for two years at the White House, Reggie Love's job was to have the President's back. Now, he says, it's time for all of us to have the President's back. 

As President Obama s personal aide during the 2008 campaign and for two years at the White House, Reggie Love s job was to have the President s back. 

Now, he says, it s time for all of us to have the President s back. 

"This is it. It s game time. And we all know how much of a competitor the President is on and off the basketball court which is where I spent a lot of time with him, and the guy hates to lose. He knows that our ground game is where we win this election. And none of us wants to wake up on November 7 thinking man, I wish I had done just a little bit more." 

So sign up to volunteer for the Obama campaign in your community. Chip in a few dollars. Register to vote . Commit to vote for President Obama. And when you ve done all those things, make sure your friends and family do too. There s too much at stake not to. Gov. Deval Patrick knows what you're going to ask: ''Yes, I am from the commonwealth of Massachusetts. That is where Mitt Romney lives, votes, and governed. He will lose Massachusetts. Enough said.'' 

Gov. Deval Patrick knows what you re going to ask: "Yes, I am from the commonwealth of Massachusetts. That is where Mitt Romney lives, votes, and governed. He will lose Massachusetts. Enough said." 

He s not on the Gotta Vote bus in Virginia today to talk about his predecessor in the Massachusetts statehouse. He s here to connect with Virginia voters about President Obama, "the one candidate in this race who cares about leaving a better country for generations to come" and make sure they re fired up and ready to vote. 

"I m a great believer that we get the government we deserve," he tells voters at stops in Norfolk and Williamsburg. "Sit it out, leave it to the highly energized people who just want government to help a few people, then we we will get that government we deserve. But if we show up, if we make the phone calls, knock the doors, talk to friends, talk to neighbors, talk to that cranky uncle you re not supposed to discuss politics with, if we have that conversation and invite people to become a part of their civic future, that s how we re-elect Barack Obama." 

Volunteer Ngozi is a freshman at Hampton University in Virginia. While most of her classmates will cast their first ballots in this year's election, Ngozi can't: She's just 17 years old. But her age isn't stopping her from making sure President Obama gets a second term. 

Ngozi is a freshman at Hampton University in Virginia. While most of her classmates will cast their first ballots in this year s election, Ngozi can t: She s just 17 years old. But her age isn t stopping her from making sure President Obama gets a second term she s been dorm-storming all semester, registering students to vote ahead of Monday s registration deadline. "It s so important to me that I want to help anyway," she says. "I canvassed in 2008, and this year, even though I can t vote, I want to help by doing all that I can and that means registering people to vote." Today, she and fellow student Ambur are registering voters outside the football stadium as the Hampton Pirates take on Norfolk State. Mitt Romney would raise middle-class taxes to pay for huge tax cuts for the wealthy. Share this if you'd prefer four more years of the Obama tax plan. 

Mitt Romney would raise middle-class taxes to pay for huge tax cuts for the wealthy. Share this if you d prefer four more years of the Obama tax plan. 

DNC Chair Debbie Wasserman Schultz released the following statement on tonight s vice presidential debate in Danville, Kentucky: 

Tonight s debate which Vice President Biden clearly and decisively won showcased two very different approaches to facts, details and core convictions: Vice President Biden understands they matter, and Congressman Paul Ryan obviously does not. 

At no point tonight was Ryan able to offer a straightforward answer or specific explanation as to how he and Mitt Romney s tax plan would be anything but devastating to the middle class, how doubling down on trickle-down will result in the creation of one single job now, how the Romney-Ryan ticket s policy on Iran would be different from President Obama s, or exactly what their plan is for our troops serving in Afghanistan. And women across the country watching tonight s debate were not given any guarantee by Paul Ryan that they would continue to have control over decisions affecting their own health, their own lives, and their own bodies. 

Meanwhile, Vice President Biden demonstrated that honest answers and concrete details do make a difference and he consistently used them tonight to underscore the fact that he s spent his entire life fighting for America s middle-class families. He and President Obama will proudly continue to do so in a second term, as they keep moving our country forward with an economy built from the middle out. At Thursday's debate, Vice President Biden said, 'Look at my record--it's been all about the middle class. They're the people who grow this country.''
