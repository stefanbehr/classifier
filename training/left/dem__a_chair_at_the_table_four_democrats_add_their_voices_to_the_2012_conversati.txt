Four lucky Democrats from across the country joined our chair, Debbie Wasserman Schultz, for lunch in Washington, D.C. They stopped by Democratic National Headquarters after lunch to talk about why they re standing with President Obama once again and what s at stake in this election. 

Tracy, a retired college professor and sales rep from Florida, kicks off the conversation by getting straight to the point: "Just because I m a lifelong Democrat doesn t mean it s the only reason I m voting for President Obama. This is the most important election of my lifetime. The Supreme Court, health reform, education, and immigration it s all at stake." 

Health care is a particular concern for everyone around the table. From free preventive care to Medicare and small-business benefits, everyone present has already benefited from the President s health reform bill and they re worried about what would happen if Republicans get their way and repeal it. 

Carol, a mother of five from Texas, tells her Affordable Care Act story. "My husband and I have a small business we run from our home. My husband had been unable to get on our policy. But thanks to the Affordable Care Act, he s been able to get on the policy now. And this year was the first time in my lifetime my premiums went down. I recently received a rebate check from my insurance company for $628 and for us, that s a big deal. There s also an overall security of knowing that there s not going to be a cap, knowing, God forbid, that something catastrophic could happen. We always lived in fear with five kids. No more." 

Roger, from North Carolina, says that as a skin cancer survivor, he no longer has to worry about losing coverage because of pre-existing conditions. And Pat, a retired insurance worker from Maryland, adds that she s worried about what will happen to women s health care under Mitt Romney. "My generation, we fought to have what we have now. We know what it s like not to have it." 

That s why Carol says it s so important no matter where you live for Democrats to get involved in this election. She says: "It s really important to me that my children see that if you believe in something, you need to invest the time to do it. You can t just sit around and wait for someone to do something about it. I would just take a little money each month that I would put aside. That was our contribution, what we could do. Because what we have at stake is so monumentally important." 

Donate now 
