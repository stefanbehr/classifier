A candidate for United States senator from Massachusetts, Elizabeth Warren used her convention debut to remind Americans exactly who Barack Obama is. 

I m Elizabeth Warren and this is my first Democratic convention. 

A candidate for United States senator from Massachusetts, Elizabeth Warren used her convention debut to remind Americans exactly who Barack Obama is: 

President Obama believes in a level playing field. He believes in a country where nobody gets a free ride or a golden parachute. A country where anyone who has a great idea and rolls up their sleeves has a chance to build a business, and anyone who works hard can build some security and raise a family. 

President Obama believes in a country where billionaires pay their taxes just like their secretaries do, and I can t believe I have to say this in 2012, a country where women get equal pay for equal work. 

He believes in a country where everyone is held accountable. Where no one can steal your purse on Main Street or your pension on Wall Street. President Obama believes in a country where we invest in education, in roads and bridges, in science, and in the future, so we can create new opportunities, so the next kid can make it big, and the kid after that, and the kid after that. 

That s what President Obama believes. And that s how we build the economy of the future. An economy with more jobs and less debt. We root it in fairness. We grow it with opportunity. And we build it together. 

Join Elizabeth and show President Obama you re with him tonight: 

Donate 
