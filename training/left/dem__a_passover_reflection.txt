From BarackObama.com : 

In his Passover message last year, President Obama encouraged us to embrace the lessons of this holiday and "work to alleviate the suffering, poverty, injustice and hunger of those who are not yet free." This Passover, we turn to the symbols of past hardships to remind us of our responsibility to serve others. President Obama reminds us of this important value every day with his belief that America prospers when we re all in it together. 



Today, our community lives in freedom and we hope to eat a Seder meal that reflects that freedom. But the HagGadah reminds us over and over again that we should not forget that our ancestors were once slaves and strangers in Egypt. It is the essence of Passover to consider our blessings as a reminder to stand together with strangers and those less fortunate. President Obama s vision of a free and just society compels us to apply the lessons of Passover to our country at this critical time. 

Now is the time for us to stand with President Obama and ensure four more years of continued progress. Join me by signing up for Jewish Americans for Obama . 
