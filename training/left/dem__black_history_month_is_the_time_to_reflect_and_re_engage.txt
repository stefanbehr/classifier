Four years ago, this country did something historic; something few thought was possible in electing Barack Obama. And two million more African Americans were inspired to come out and vote than in the previous Presidential election 96 percent of whom supported the President. 

When he took office, there was a sense that America had lost its moral compass; that we needed to restore the basic values that made our country great. And from day one, President Obama took action to restore the sense that we re all in this together; that hard work will pay off; and that everyone who does their fair share and plays by the same rules will be rewarded. 

He s laid the foundation for a vibrant and prosperous America by investing in education, infrastructure, and reforming the health care system to improve access to health care for all Americans. And the President has a vision for moving the country forward and maintaining our position as that shining city upon a hill. But he can t do it alone. 

If we are going to finish the job that began on a blustery day in the shadow of the Old State Capitol in Springfield, Illinois; if we are going to protect the change that we have made in the President s first term; then we are going to have to work even harder than we did in 2008. 

That s why we have re-examined every tactic from four years ago, honed our strategy, and launched new initiatives that will give all Americans, including African Americans, more ownership of this campaign. 

During Black History Month, we launched Greater Together Summits on the campuses of 28 HBCUs across the country, including marquee events at the AUC in Atlanta and North Carolina Central University. Nearly 1,000 students came out to the Atlanta University Center on the campus of Clark Atlanta to hear from Dr. Michael Eric Dyson, Keshia Knight Pullium, and Janelle Mona talk about why they re supporting the President. 

We ve started the Black Business Captains Program aimed at incorporating and cultivating business leaders into a larger program of empowerment and engagement for the African American community. These captains will take the lead on educating local businesses on participation importance and opportunities; reach out to key business leaders and involve their personal networks; and provide validation for organizers doing voter registration and volunteer outreach efforts within their businesses. 

And along with several other initiatives, we launched BarackObama.com/AfricanAmericans , giving supporters more ownership over the campaign, information about the President s accomplishments, and how they can get involved. 

However, Republicans want to turn back the clock on the President s accomplishments and have taken action in 30 states to make voting harder. It is clear that these laws are designed to keep tens of millions of Americans from being able to cast their vote. That s why earlier this year we launched a new website, protectingthevote.org . This will educate voters about their right to vote, and expose the Republican Party s attempts to limit our rights for their political gain. 

As the President said, Voting is a fundamental right and when we make our voices heard, we can keep moving our nation forward, building a fairer, stronger and more just America. And this campaign will be about inclusiveness. We re trying to get as many Americans involved as possible. That s the true spirit of our elections. We ve never solved anything in America with less democracy, and we won t now. 

And so as we close out Black History Month, it s important to remember the President s words when we embarked on this great journey. Five years ago in Springfield, he said, In the face of impossible odds, people who love their country can change it. This country has come a long way, but there s still work to do. We know we can count on the President to lead us to better days. Let s stand with him and keep this country moving in the right direction. 
