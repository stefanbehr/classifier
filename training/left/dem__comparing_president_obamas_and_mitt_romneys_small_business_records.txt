Small businesses are the engines of our economy, creating 2 out of every 3 net new jobs in this country. President Obama and Mitt Romney take starkly different approaches to supporting America s small business owners. While the President aggressively pursues policies that support their growth, Mitt Romney has championed policies that undermine the ability of entrepreneurs to expand and hire. 

Just take a look at what happened to small businesses in Massachusetts while Romney was governor: 

Start-ups fell: Under Romney, the number of business start-ups fell by 10%, hitting their lowest point in his last year in office. Each year Romney was in office, start-up growth in Massachusetts lagged behind the national average. 

Small businesses shut their doors: When Romney took office, more entrepreneurs were starting small businesses than shutting them down. When he left, the opposite was true the number of small businesses shrank during his term. 

Job creation suffered: With fewer new businesses starting, and existing small businesses closing shop, Massachusetts fell to 47th of 50 states in job creation during Romney s term. 

Click here to read more about Gov. Romney s small business record in Massachusetts. 

President Obama, on the other hand, has a long record of support for small businesses. Here s just a few things he s done: Signed 18 tax cuts to support small businesses Supported a record volume of small business loans last year, and more than 150,000 loans since he took office, allowing business owners to expand and hire more workers Invested $2 billion in Small Business Administration funding for early-stage businesses and businesses in underserved communities or emerging business sectors 

Click here to learn more about how the candidates compare on support for small businesses. 

President Obama has put forward a plan that would support small businesses by cutting taxes for entrepreneurs who hire and expand, and investing in infrastructure, innovation, and education. Romney s plan, on the other hand, would slash investments that support small business growth while putting millions of small businesses at risk of a tax increase to pay for tax breaks for millionaires and billionaires. 

To distract Americans from what his own plan would do, Romney is deliberately editing the President s words to make it appear as if the President was insulting small business owners. The facts are clear and, no matter how much Romney tries to distort the truth, his record reveals exactly what his policies would mean for small businesses. Watch Deputy Campaign Manager Stephanie Cutter lay out what s at stake this year, and then share the facts with your friends. 


