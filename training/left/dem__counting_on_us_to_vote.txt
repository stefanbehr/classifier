As Rep. Debbie Wasserman Schultz told Waterloo, Iowa, ''President Obama is counting on us to make sure we continue to move forward together.'' 

Rep. Debbie Wasserman Schultz: 

"President Obama is counting on us to make sure we continue to move forward together. 

"Students who can t afford to go to college unless we keep making college affordability a top priority are counting on us. 

"Seniors who can t afford to buy their life-saving prescription medications without Obamacare are counting on us. 

"Middle-class families who are working hard every day to put a roof over their families heads and put food on the table are counting on us. 

"Remember: It was the hard work, the incredible passion and the amazing energy of Americans like you that helped put President Obama over the top in 2008. 

"Now we need you to channel that momentum and give everything you ve got to doing it again on November 6." 

Gotta vote. 
