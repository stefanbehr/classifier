Yesterday, we announced a new effort to protect every eligible citizen s right to vote, including a new report from our Voting Rights Institute: A Reversal in Progress: Restricting Voting Rights for Electoral Gain. 

Republicans across the country have engaged in a full-scale attack on the right to vote, seeking ways to restrict or limit voters ability to cast their ballots for their own partisan advantage. 

DNC counsel and voter protection director Will Crossley appeared on Al Sharpton s MSNBC show last night to reiterate the Democrats belief that our democracy is stronger, not weaker, when voters are able to cast their ballot and have their vote counted. 



Watch and share last night s appearance on PoliticsNation. 
