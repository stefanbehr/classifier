From the second floor of an Italian restaurant in downtown Manchester the Democratic Party s war room in New Hampshire for the week DNC communications director Brad Woodhouse and laid-off worker Randy Johnson responded to Romney s latest incredibly out-of-touch comments. 

Here s a quick recap: Yesterday, Mitt Romney told a New Hampshire crowd that he knows what it feels like to fear a pink slip, though he and the campaign have been unable to cite any specific examples of when that might be the case. That s because when Romney took the job at Bain Capital, he negotiated himself a pretty sweet deal with zero risk to himself. As Woodhouse put it: "The only reason Mitt Romney knows what a pink slip is is that he s handed out so many." 

But then this morning, the man who claimed he knew the fear of getting laid off told supporters at a campaign event that he "like[s] being able to fire people." Johnson, who was actually laid off when Bain Capital took over his office-supply plant in Indiana, spoke from the heart: "It brought back memories," he said, of hearing Romney joke about firing. "That is exactly what they told me: You re fired. Firing people and making light of it is wrong. I m really going to call him out on it." 

You can help us call him out. Check out this video and pass it along to your friends and family. Don t let Mitt Romney get away with being so callous about people s livelihoods. 


