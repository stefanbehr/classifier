Jackie from Lorain, Ohio, voted early--in fact, she was so excited to cast her ballot for President Obama, she arrived before the polls even opened. Pointing to the "for all" button she wore to today's Gotta Vote event, Jackie says she supports the President because he doesn't divide people into 47 percent and the 53 percent. 

Jackie from Lorain, Ohio, voted early in fact, she was so excited to cast her ballot for President Obama, she arrived before the polls even opened. Pointing to the "for all" button she wore to today s Gotta Vote event, Jackie says she supports the President because he doesn t divide people into 47 percent and the 53 percent. 

"Mitt Romney is just so far away from the people like me. President Obama represents everybody. I was fortunate to be able to see him at Kent State, and I don t think he was politicking when he said, for those people who don t support him, who are against him, I ll be your president too. Not their president, like he s separating people. We re all in this together. He s for everybody, and I just appreciate how hard he works for all of us." 

Gotta Vote 
