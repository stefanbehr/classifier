Mitt Romney wants to cut $1 trillion from Medicaid, denying coverage to 60 million people, including low-income kids. 

PresidentObama says, "We leave no one behind. We pull each other up." Apparently, Mitt Romney disagrees. 
