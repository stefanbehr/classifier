The Democratic Party will make history today, as delegates are preparing to vote to approve a party platform that includes marriage equality and equal treatment under the law for same-sex couples. 

Jan, an LGBT delegate from Michigan, says the 2012 platform is something she never expected to see: "When I first heard about it, I started crying. I couldn t believe it. I m going to be 60 years old soon, and I didn t think that I was ever going to see it in my lifetime. It s so exciting." 

Today s meeting of the Democratic Party s LGBT caucus was historic in other regards: The standing-room only crowd hosted a record number of LBGT delegates as well as a record number of transgender delegates. Every state in the nation was represented in the caucus, a far cry from the days when the caucus was getting off the ground with a small number of people. 

The stakes are high this year for LGBT Americans and what President Obama has accomplished on their behalf achievements that, in the words of an LGBT delegate from New Hampshire, "Romney and Ryan would wipe out in a matter of days." That s why LGBT Americans are making their voices heard in full force, standing up for the President who ended "Don t Ask, Don t Tell," and became the first sitting president to publicly announce his belief in marriage equality. Jan says, "I think Barack Obama is historically a fantastic candidate for the LGBT community, and we need to get that message out loud and clear." 

There s more to do, but we ve come a long way and the LGBT caucus is ready to stand with the President and finish the fight. 
