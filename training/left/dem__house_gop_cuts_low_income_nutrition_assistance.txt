House Republicans have made it clear that even in times of economic recovery, they re not willing to end things like tax cuts for the wealthiest Americans. 

But here s what they are willing to do: cut nutrition assistance for women, infants, and children. 

At the end of last month, the Republican-led House Appropriations Committee approved more than $830 million in cuts to three programs that help low-income families keep food on the table -- the Special Supplemental Nutrition Program for Women, Infants, and Children (WIC), the Emergency Food Assistance Program, and the Commodity Supplemental Food Program. 

Not only is this unjust but it s also bad fiscal policy. Think Progress reports that economists estimate that each dollar invested in WIC "saves between $1.77 and $3.13 in health care costs in the first 60 days after an infant s birth by reducing the instance of low-birthweight babies and improving child immunization rates." 

Read more at Think Progress . 
