A new report released by the Department of Health and Human Services (HHS) last week shows just how important the Affordable Care Act is to seniors and those on disability: In 2011 alone, 3.6 million people with Medicare have saved $2.1 billion on their prescription drugs, an average of $604 per person. Medicare Advantage premiums are down by 16 percent since 2010, and enrollment is up by about 10 percent. By 2021 the average person with Medicare will save nearly $4,200 because of the Affordable Care Act. 

For older Americans and people on disability living on a fixed income, these discounts are invaluable, as are the even bigger savings to come in the years ahead. 

You can find out more about these and future discounts in the full HHS report . Also, be sure to read Kathleen Sebelius s blog about the report as well as HHS Assistant Secretary on Aging Kathy Greenlee s blog for the White House. 
