On Monday night, hundreds of supporters came together at a reception hosted by DNC Chair, Debbie Wasserman Schultz at the AIPAC Policy Conference where they reaffirmed their commitment to President Obama and their party. 

The enthusiastic crowd cheered loudly as the Chair gave her address where she affirmed President Obama s unwavering support for Israel and commitment to preventing Iran from obtaining a nuclear weapon. 

In addition to the hundreds of supporters in attendance Congresswoman Wasserman Schultz was joined at the reception with National Jewish Democratic Council President and CEO David Harris, Rep. Joe Baca, Rep. Howard Berman, Rep. Ted Deutch, Rep. Eliot Engel, Rep. Rush Holt, Rep. Paul Tonko, Rep. Larry Kissell and Rep. Nita Lowey. 

Congresswoman Lowey praised President Obama and his relationship with Israel as well as the great leadership provided by DNC Chair Wasserman Schultz. Mr. Harris also emphasized that our focus is to keep Israel secure and provide indefatigable support. 

With a crowd that overflowed the room, the overwhelming spirit was one of excitement and optimism. Just a day earlier, President Obama addressed the AIPAC crowd and stated: 

Four years ago I stood before you and said that Israel s security is sacrosanct; it is not negotiable. That belief has guided my actions as president. The fact is, my administration s commitment to Israel s security has been unprecedented. Our military and intelligence cooperation has never been closer. Our joint exercises and training have never been more robust. Despite a tough budget environment, our security assistance has increased every single year. 

This administration s unwavering support and commitment to Israel s security has been a driving force in bringing out supporters to our reelection efforts. President Obama said it best: So there should not be a shred of doubt by now. When the chips are down, I have Israel s back. 
