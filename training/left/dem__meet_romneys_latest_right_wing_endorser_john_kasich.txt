Mitt Romney has added another Tea Party governor to his list of endorsements, and this time, it s Ohio Gov. John Kasich. Kasich gained national notoriety last year when he led the charge to strip Ohio workers of their collective bargaining rights an anti-union effort to which Romney gladly lent his support. Issue 2 wasn t as popular with Ohio voters, however, as it was with Romney, and they overwhelmingly rejected their governor s plan. 

The extremism doesn t end there: Romney and Kasich share the same agenda to weaken the middle class in order to help the wealthiest few. Take education. Romney s campaigning on gutting the Department of Education, and Kasich slashed funds to Ohio s local school districts all to fund major tax cuts for millionaires. The middle class can, however, look forward to a whopping $167 tax cut from President Romney ("$167 isn t zero," he says). 

It s no wonder Kasich held the distinction of being the most unpopular governor in America. His pal Mitt can commiserate: Polls show he s the most unpopular candidate in decades. 

Watch and share our new video on what Ohio voters already know: Romney and Kasich s mutual agenda is bad for the middle class. 
