As Paul Ryan campaigns in Iowa today, bringing his distortions and failed policies to the Hawkeye State, the people of Davenport have a few things they d like to say to him. 

Judy: 

"I wish Paul Ryan would stop telling lies and stop exaggerating and start thinking about what the American people really want. I am sure he s not going to impress the people of Iowa. We re smarter than that. We re smarter than to vote for him and Mitt Romney. I m on Medicare. Their plan would definitely affect me because I d have to start thinking about a voucher program. And taxes: Even though I m retired, I still pay income tax. And I need to see some tax cuts for people like me." 

Rene: 

"I would tell him he doesn t represent me. I m a single parent. I have an elderly mom. She lives on Social Security relies on it. What Ryan and Mitt Romney plan to do would hurt my mom and people like me. He s not representing my family, and that s why I m supporting Barack Obama." 

Don: 

"Paul Ryan should get real about what their program would really do for America. It is tragic what would happen to just small things, like Meals on Wheels, Pell Grants, the food stamp program, the things that may not matter to the people he knows but matter a lot to the people who are in the 47 percent." 

Ann: 

"I m going to community college in the spring. Barack Obama s the only candidate who s on the side of students and education. He capped student loan rates and raised Pell Grants. It might not seem like a lot, but it matters so much to someone who barely has anything and still wants to go to college. It s hard when I see my mom struggle to make ends meet when she s still trying to pay off her student loans and my dad s student loans and hasn t been able to save for me. Barack Obama s the only one who s doing something about it. I m excited about my future now." 

Gotta Vote 
