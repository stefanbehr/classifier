Our chair, Debbie Wasserman Schultz, will be on hand in New Hampshire this evening to lead the Democratic rebuttal to the Republican presidential debate. Congresswoman Wasserman Schultz will emphasize the GOP candidates out-of-touch messages, including plans to deregulate Wall Street and give tax breaks to the wealthy. 

The American Jobs Act has gained considerable public support after just three weeks of advocacy by President Obama. A new ABC/Washington Post poll shows 52 percent of Americans support the AJA 10 percent more than in early September with 36 percent opposed, a clear indication that the public supports the plan that would put hundreds of thousands of Americans back to work. 

The latest Gallup poll of the GOP presidential race shows Herman Cain surpassing Rick Perry and approaching front-runner Mitt Romney . The poll conducted October 3 7 has Romney receiving 20 percent of Republican support with Cain close behind at 18 percent. 

The Koch brothers revealed they will spend more than $200 million to bankroll right-wing causes ahead of the 2012 election. This puts the conservative billionaire brothers in the company of Republican operatives Karl Rove and Ed Gillespie, who plan to raise $240 million. 

Senior Obama campaign strategist David Axelrod urged Congress to put country ahead of party and support the American Jobs Act . Axelrod sees public opinion swinging in Democrats direction in light of recent polls that indicate growing support for the jobs bill. 
