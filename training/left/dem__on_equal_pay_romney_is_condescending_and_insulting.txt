This November, something no woman should have to ask for, much less fight for equal pay is at stake. 

Last month, a study by the Institute for Women s Policy Research found that on average, women earn 77 cents to every dollar earned by men. In some industries, the disparities are greater still: Women make between 55 and 62 cents for each dollar their male counterparts do. 

Equal pay isn t just a women s issue. It s good for families, and it s good for our economy as a whole. President Obama knows that, and upon taking office, he signed into law the Lilly Ledbetter Fair Pay Restoration Act. The law is named after an Alabama woman who worked at a tire factory for 17 years but was denied the pay that her male counterparts received. Ledbetter took her fight all the way to the Supreme Court, but it wasn t till President Obama s signature put her name in the history books that she received some measure of justice. 

The fact that the Lilly Ledbetter Act was the first law of his presidency sent an unmistakable signal that in President Obama, American women have a leader who is fighting on their behalf every single day. 

But this November, something no woman should have to ask for, much less fight for equal pay is at stake as voters face a choice between, as DNC Chair Debbie Wasserman Schultz puts it, "a guy who gets it and a guy who doesn t." The Republicans presumptive nominee, Mitt Romney, won t say whether he would have voted for basic legislation that would ensure women earn equal pay for equal work. And to add insult to injury, over the weekend, a top Republican strategist dismissed the issue entirely, saying it s just not true that women are paid less than men. 

Lilly Ledbetter has a few words for these Republican men: 

"It s condescending, and it s insulting. 

"It s unfortunate that Romney and the Republicans want to dismiss the facts and write their own version of reality. But I will tell you what reality looks like: Reality is when a woman works day in and day out just as hard as everybody else but makes just 77 percent of what her co-workers make because of the mere fact that she is a woman." 

For Ledbetter and the thousands of women who share a similar story the choice in this election is clear. It s up to all of us to support President Obama and the policies that make a difference in the lives of women across the country. Sign up to volunteer here . 
