As a home for business, our country has tremendous appeal. We have a diverse and educated workforce, intellectual property laws that protect business owners rights, reliable infrastructure, and easy access to a global consumer market. That s part of why more than five million Americans are already employed by foreign companies operating in the U.S. 

But in recent years international competition for business investment has escalated to unprecedented levels. So President Obama has initiated this new program to leverage the influence and resources of the federal government to make sure more and more of these investments take place in America. 

To accelerate this process, the President issued an Executive Order to create a new initiative, SelectUSA , to aggressively pursue new business investment right here on American shores. This program will: 

[L]leverage existing resources of the federal government to ramp up promotion of the U.S. as a prime investment destination to create jobs at home and to keep jobs from going overseas. 

The federal government will be more empowered to work with governors, mayors, and policymakers in all sectors and levels of government to help attract great business investment in our communities. 

Additionally, Secretary of Commerce Gary Locke described how SelectUSA will also help to reduce red tape and unnecessary hurdles to winning those investments: 

We will also be a trouble shooter for those dealing with red tape and federal hurdles to bringing or keeping business operations and investment in their communities. 

Too often, businesses and local governments have difficulty navigating federal agencies in pursuit of permits, tax information and directives on regulatory compliance, delaying new investment. We know that, because we ve heard directly from businesses about it. 

SelectUSA will help turn this around. 

Coordinated by the Commerce Department, with expert personnel in nearly 80 countries promoting American exports and investment opportunities, SelectUSA will lead the federal effort to address business investment attraction and retention issues, both logistical and policy related, in the U.S. 

President Obama has said time and again that he is not satisfied with the pace of our country s economic recovery, and that is doing everything in his power to help create jobs and strengthen our economy. 

SelectUSA is part of that effort. 

Learn more about this innovative new program by visiting SelectUSA.gov . 
