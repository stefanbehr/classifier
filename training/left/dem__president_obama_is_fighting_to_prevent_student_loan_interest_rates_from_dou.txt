President Obama believes that keeping interest rates on student loans low is key to growing the economy, so that more Americans get a fair shot at an affordable college education and the skills they need to find a good job. 

Keeping college affordable is a critical part of the President s blueprint for an economy that s built to last one that prepares Americans for the jobs of the future, restores middle-class security, and rewards hard work and responsibility. 

Interest rates for new subsidized student loans are set to double from 3.4 to 6.8 percent on July 1 unless Congress acts. The rates were set by Congress in 2007. Did you know that Americans owe more on student loans than credit cards? If Congress fails to act, more than 7 million students would pay on average more than $1,000 in costs over the life of that loan. 

President Obama has a strong record of helping America s students pay for college. He doubled our investments in Pell Grants, helping 3.7 million more students afford college. He fought for a college tax credit that s worth up to $10,000 over four years of college. He capped federal student loan payments at 10 percent of monthly income. That will help 1.6 million students manage their monthly payments and pursue the career of their dreams as soon as this year. 

Call your Member of Congress today and let them know that we have to keep college affordable, giving all Americans a fair shot at receiving an affordable education. They need to know that passing this bill is not only good for our nation s students and their families it s vital to keeping America competitive for the long haul. 
