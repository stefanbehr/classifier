Yesterday, President Obama met with 40 high school students who are finalists for the Intel Science Talent Search . This event was an opportunity to recognize some of the brightest science and math students across the country students who will become the next generation s scientists, engineers, and technological leaders. These talented young men and women are examples of how investments in education today will help America win the future. 

President Obama s budget plan includes crucial investments in America s education system that will prepare the next generation for the challenges ahead it aims to train 100,000 new science, technology, engineering, and math teachers. 

Phil Larson writes on the White House blog : 

The Intel Science Talent Search is one of the most highly-regarded science research competitions in the country. Many past Science Talent Search participants have gone on to distinguished careers, including seven Nobel laureates, four National Medal of Science winners, 11 MacArthur Foundation Fellows, and two Fields Medalists (as well as one Academy Award winner Natalie Portman!). 

This year s finalists include 16 girls and 24 boys who represent 39 schools in 15 states and were selected from over 1,700 applications tied to cuttingedge research. President Obama applauds the work of these young innovators and of pioneering minds throughout the Nation. 

Learn about President Obama s recent efforts to reform the education policy No Child Left Behind . 
