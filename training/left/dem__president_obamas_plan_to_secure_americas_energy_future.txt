As turmoil around the world continues and gas prices climb higher, President Obama today called for a renewed effort to secure our country s energy future. For too long, America has been beholden to unstable and fluctuating oil-markets, an addiction that affects prices at the pump, the cost of groceries, and the ability of families to heat their homes. 

Today, the President outlined a plan to shake America s age-old dependence on oil and usher in a new era of cleaner, sustainable, and affordable sources of energy. And the time to act is now: 

We cannot keep going from shock to trance on the issue of energy security, rushing to propose action when gas prices rise, then hitting the snooze button when they fall again. The United States of America cannot afford to bet our long-term prosperity and security on a resource that will eventually run out. Not anymore. 

President Obama set a new goal for our country: reduce the amount of oil imported by one-third by 2025. The administration released A Blueprint for A Secure Energy Future that establishes two bottom-line goals: Find and produce more oil at home; Reduce our dependence on oil with cleaner alternative fuels and greater efficiency. 

Part of the short-term way to address America s energy challenge, the administration is encouraging greater offshore oil exploration and production taking careful precaution to ensure that companies meet standards of safety and responsibility. 

However, President Obama made it clear that additional oil production is not a sustainable solution. The President s plan calls for added collaboration with international partners to increase natural gas supplies and bioenergy production: 

[N]ot just ethanol, but biofuels made from things like switchgrass, wood chips, and biomass. 

If anyone doubts the potential of these fuels, consider Brazil. Already, more than half half of Brazil s vehicles can run on biofuels. And just last week, our Air Force used an advanced biofuel blend to fly an F-22 Raptor faster than the speed of sound. In fact, the Air Force is aiming to get half of its domestic jet fuel from alternative sources by 2016. 

America will work to phase out oil as a primary energy source, but in the meantime, we can accelerate those efforts by making our cars and trucks more efficient as well 70 percent of oil consumption goes to transportation. That means higher fuel-efficiency standards, which will increase gas mileage, save billions of barrels of oil, and putting thousands of dollars back in the pockets of families. 

And the President is ensuring that the federal government will lead by example. He is directing agencies to purchase only alternative fuel, hybrid, or electric vehicles by 2015 the federal government s fleet is among the largest in the country. The administration has also made record investments in high-speed rail and mass transit, providing an efficient and cleaner alternative for urban, suburban, and rural Americans. 

However, these efforts will not reach their full potential without opening the markets for clean energy. The administration is working with businesses and entrepreneurs to achieve the goal of generating 80 percent of America s electricity from clean energy sources by 2035. 

It also means making residential, commercial, and industrial buildings more energy efficient. In fact, the administration is already on track to weatherize 600,000 low-income homes through the Recovery Act and implementing initiatives, such as the HOMESTAR program to help homeowners finance retrofits and the Better Buildings Initiative to make commercial facilities 20 percent more efficient by 2020. 

In his closing remarks, President Obama spoke directly to the Georgetown students in the audience about the energy challenges confronting our country and the role they must play in helping to find the solutions: 

I don t want to leave this challenge for future presidents. I don t want to leave it for my children. And I do not want to leave it for yours. Solving it will take time and effort. It will require our brightest scientists, our most creative companies, and, most importantly, all of us Democrats, Republicans, and everyone in between to do our part. But with confidence in America, in ourselves, and in one another I know it is a challenge we will solve. 

Click here to read the President s full remarks. 
