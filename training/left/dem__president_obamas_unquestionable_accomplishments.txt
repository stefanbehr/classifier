My President and Commander and Chief, Barack Obama, has accomplished more in four years than the preceding administration did in eight years. He ended a war in Iraq, destroyed al Qaeda and help remove Gaddafi from power in Libya. President Obama has made this country safer. He has the economy going in the right direction and 32 million more American citizens now have health insurance. Those are just a few of the outstanding achievements he has accomplished. 

President Obama has already made a positive impact on American history with his accomplishments. 

As a 91-year-old World War II Disabled Veteran, I am tired of people complaining about my great President and the Commander-in-Chief. I know this could possibly be the last presidential election I see. I yearn for the chance and the honor to be a part of this presidential campaign and work with President Obama on his re-election. As a senior citizen and honorable veteran of America, I look forward to doing my part in this effort. 
