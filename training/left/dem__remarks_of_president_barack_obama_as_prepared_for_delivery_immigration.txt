Remarks of President Barack Obama As Prepared for Delivery Immigration and Border Security Tuesday, May 10, 2011 El Paso, Texas. Hello, El Paso! It s great to be back here with all of you, and to be back in the Lone Star State. I love coming to Texas. Even the welcomes are bigger down here. So, to show my appreciation, I wanted to give a big policy speech outdoors right in the middle of a hot, sunny day. 



I hope everyone is wearing sunscreen. 



Now, about a week ago, I delivered the commencement address at Miami Dade Community College, one of the most diverse schools in the nation. The graduates were proud that their class could claim heritage from 181 countries around the world. Many of the students were immigrants themselves, coming to America with little more than the dreams of their parents and the clothes on their backs. A handful had discovered only in adolescence or adulthood that they were undocumented. But they worked hard and gave it their all, and they earned those diplomas. 



At the ceremony, 181 flags one for every nation represented was marched across the stage. Each was applauded by the graduates and relatives with ties to those countries. But then, the last flag the American flag came into view. And the room erupted. Every person in the auditorium cheered. Yes, their parents or grandparents or the graduates themselves had come from every corner of the globe. But it was here that they had found opportunity, and had a chance to contribute to the nation that is their home. 



It was a reminder of a simple idea, as old as America itself. E pluribus, unum. Out of many, one. We define ourselves as a nation of immigrants a nation that welcomes those willing to embrace America s precepts. That s why millions of people, ancestors to most of us, braved hardship and great risk to come here so they could be free to work and worship and live their lives in peace. The Asian immigrants who made their way to California s Angel Island. The Germans and Scandinavians who settled across the Midwest. The waves of the Irish, Italian, Polish, Russian, and Jewish immigrants who leaned against the railing to catch that first glimpse of the Statue of Liberty. 



This flow of immigrants has helped make this country stronger and more prosperous. We can point to the genius of Einstein and the designs of I. M. Pei, the stories of Isaac Asimov and whole industries forged by Andrew Carnegie. 



And I think of the naturalization ceremonies we ve held at the White House for members of the military, which have been so inspiring. Even though they were not yet citizens, these men and women had signed up to serve. One was a young man named Granger Michael from Papua New Guinea, a Marine who deployed to Iraq three times. Here s what he said about becoming an American citizen. I might as well. I love this country already. Marines aren t big on speeches. Another was a woman named Perla Ramos. She was born and raised in Mexico, came to the United States shortly after 9/11, and joined the Navy. She said, I take pride in our flag and the history we write day by day. 



That s the promise of this country that anyone can write the next chapter of our story. It doesn t matter where you come from; what matters is that you believe in the ideals on which we were founded; that you believe all of us are equal and deserve the freedom to pursue happiness. In embracing America, you can become American. And that enriches all of us. 



Yet at the same time, we are standing at the border today because we also recognize that being a nation of laws goes hand in hand with being a nation of immigrants. This, too, is our heritage. This, too, is important. And the truth is, we ve often wrestled with the politics of who is and who isn t allowed to enter this country. At times, there has been fear and resentment directed toward newcomers, particularly in periods of economic hardship. And because these issues touch on deeply held convictions about who we are as a people, about what it means to be an American these debates often elicit strong emotions. 



That s one reason it s been so difficult to reform our broken immigration system. When an issue is this complex and raises such strong feelings, it s easier for politicians to defer the problem until after the next election. And there s always a next election. So we ve seen a lot blame and politics and ugly rhetoric. We ve seen good faith efforts from leaders of both parties fall prey to the usual Washington games. And all the while, we ve seen the mounting consequences of decades of inaction. 



Today, there are an estimated 11 million undocumented immigrants in the United States. Some crossed the border illegally. Others avoid immigration laws by overstaying their visas. Regardless of how they came, the overwhelming majority of these folks are just trying to earn a living and provide for their families. But they ve broken the rules, and have cut in front of the line. And the truth is, the presence of so many illegal immigrants makes a mockery of all those who are trying to immigrate legally. 



Also, because undocumented immigrants live in the shadows, they re vulnerable to unscrupulous businesses that skirt taxes, pay workers less than the minimum wage, or cut corners with health and safety. This puts companies who follow those rules, and Americans who rightly demand the minimum wage or overtime or just a safe place to work, at an unfair disadvantage. 



Think about it. Over the past decade, even before the recession, middle class families were struggling to get by as costs went up but incomes didn t. We re seeing this again with gas prices. Well, one way to strengthen the middle class is to reform our immigration system, so that there is no longer a massive underground economy that exploits a cheap source of labor while depressing wages for everyone else. I want incomes for middle class families to rise again. I want prosperity in this country to be widely shared. That s why immigration reform is an economic imperative. 



And reform will also help make America more competitive in the global economy. Today, we provide students from around the world with visas to get engineering and computer science degrees at our top universities. But our laws discourage them from using those skills to start a business or power a new industry right here in the United States. So instead of training entrepreneurs to create jobs in America, we train them to create jobs for our competition. That makes no sense. In a global marketplace, we need all the talent we can get not just to benefit those individuals, but because their contributions will benefit all Americans. 



Look at Intel and Google and Yahoo and eBay these are great American companies that have created countless jobs and helped us lead the world in high-tech industries. Every one was founded by an immigrant. We don t want the next Intel or Google to be created in China or India. We want those companies and jobs to take root in America. Bill Gates gets this. The United States will find it far more difficult to maintain its competitive edge, he s said, if it excludes those who are able and willing to help us compete. 



It s for this reason that businesses all across America are demanding that Washington finally meet its responsibility to solve the immigration problem. Everyone recognizes the system is broken. The question is, will we summon the political will to do something about it? And that s why we re here at the border today. 



In recent years, among the greatest impediments to reform were questions about border security. These were legitimate concerns; it s true that a lack of manpower and resources at the border, combined with the pull of jobs and ill-considered enforcement once folks were in the country, contributed to a growing number of undocumented people living in the United States. And these concerns helped unravel a bipartisan coalition we forged back when I was a United States Senator. In the years since, borders first has been a common refrain, even among those who previously supported comprehensive immigration reform. 



Well, over the past two years we have answered those concerns. Under Secretary Napolitano s leadership, we have strengthened border security beyond what many believed was possible. They wanted more agents on the border. Well, we now have more boots on the ground on the southwest border than at any time in our history. The Border Patrol has 20,000 agents more than twice as many as there were in 2004, a build up that began under President Bush and that we have continued. 



They wanted a fence. Well, that fence is now basically complete. 



And we ve gone further. We tripled the number of intelligence analysts working the border. I ve deployed unmanned aerial vehicles to patrol the skies from Texas to California. We ve forged a partnership with Mexico to fight the transnational criminal organizations that have affected both of our countries. And for the first time we are screening 100 percent of southbound rail shipments to seize guns and money going south even as we go after drugs coming north. 



So, we have gone above and beyond what was requested by the very Republicans who said they supported broader reform as long as we got serious about enforcement. But even though we ve answered these concerns, I suspect there will be those who will try to move the goal posts one more time. They ll say we need to triple the border patrol. Or quadruple the border patrol. They ll say we need a higher fence to support reform. 



Maybe they ll say we need a moat. Or alligators in the moat. 



They ll never be satisfied. And I understand that. That s politics. 



But the truth is, the measures we ve put in place are getting results. Over the past two and a half years, we ve seized 31 percent more drugs, 75 percent more currency, and 64 percent more weapons than before. Even as we ve stepped up patrols, apprehensions along the border have been cut by nearly 40 percent from two years ago that means far fewer people are attempting to cross the border illegally. 



Also, despite a lot of breathless reports that have tagged places like El Paso as dangerous, violent crime in southwest border counties has dropped by a third. El Paso and other cities and towns along the border are consistently rated among the safest in the nation. Of course, we shouldn t accept any violence or crime, and we have more work to do. But this progress is important. 



Beyond the border, we re also going after employers who knowingly exploit people and break the law. And we are deporting those who are here illegally. Now, I know that the increase in deportations has been a source of controversy. But I want to emphasize: we are not doing this haphazardly; we are focusing our limited resources on violent offenders and people convicted of crimes; not families, not folks who are just looking to scrape together an income. As a result, we increased the removal of criminals by 70 percent. 



That is not to ignore the real human toll. Even as we recognize that enforcing the law is necessary, we don t relish the pain it causes in the lives of people just trying to get by. And as long as the current laws are on the books, it s not just hardened felons who are subject to removal; but also families just trying to earn a living, bright and eager students; decent people with the best of intentions. I know some here wish that I could just bypass Congress and change the law myself. But that s not how a democracy works. What we really need to do is keep up the fight to pass reform. That s the ultimate solution to this problem. And I d point out, the most significant step we can take now to secure the borders is to fix the system as a whole so that fewer people have incentive to enter illegally in search of work in the first place. This would allow agents to focus on the worst threats on both of our borders from drug traffickers to those who would come here to commit acts of violence or terror. 



So, the question is whether those in Congress who previously walked away in the name of enforcement are now ready to come back to the table and finish the work we ve started. We have to put the politics aside. And if we do, I m confident we can find common ground. Washington is behind the country on this. Already, there is a growing coalition of leaders across America who don t always see eye-to-eye, but who are coming together on this issue. They see the harmful consequences of this broken system for their businesses and communities. They understand why we need to act. 



There are Democrats and Republicans, including former-Republican Senator Mel Martinez and former-Bush administration Homeland Security Secretary Michael Chertoff; leaders like Mayor Michael Bloomberg; evangelical ministers like Leith Anderson and Bill Hybels; police chiefs from across the nation; educators and advocates; labor unions and chambers of commerce; small business owners and Fortune 500 CEOs. One CEO had this to say about reform. American ingenuity is a product of the openness and diversity of this society Immigrants have made America great as the world leader in business, science, higher education and innovation. That s Rupert Murdoch, the owner of Fox News, and an immigrant himself. I don t know if you re familiar with his views, but let s just say he doesn t have an Obama bumper sticker on his car. 



So there is a consensus around fixing what s broken. Now we need Congress to catch up to a train that s leaving the station. Now we need to come together around reform that reflects our values as a nation of laws and a nation of immigrants; that demands everyone take responsibility. 



So what would comprehensive reform look like? 



First, we know that government has a threshold responsibility to secure the borders and enforce the law. Second, businesses have to be held accountable if they exploit undocumented workers. Third, those who are here illegally have a responsibility as well. They have to admit that they broke the law, pay their taxes, pay a fine, and learn English. And they have to undergo background checks and a lengthy process before they can get in line for legalization. 



And fourth, stopping illegal immigration also depends on reforming our outdated system of legal immigration. We should make it easier for the best and the brightest to not only study here, but also to start businesses and create jobs here. In recent years, a full 25 percent of high-tech startups in the U.S. were founded by immigrants, leading to more than 200,000 jobs in America. I m glad those jobs are here. And I want to see more of them created in this country. We need to provide farms a legal way to hire the workers they rely on, and a path for those workers to earn legal status. Our laws should respect families following the rules reuniting them more quickly instead of splitting them apart. Today, the immigration system not only tolerates those who break the rules, it punishes the folks who follow the rules. While applicants wait for approval, for example, they re often forbidden from visiting the United States. Even husbands and wives may have to spend years apart. Parents can t see their children. I don t believe the United States of America should be in the business of separating families. That s not right. That s not who we are. And we should stop punishing innocent young people for the actions of their parents by denying them the chance to earn an education or serve in the military. That s why we need to pass the Dream Act. Now, we passed the Dream Act through the House last year. But even though it received a majority of votes in the Senate, it was blocked when several Republicans who had previously supported the Dream Act voted no. It was a tremendous disappointment to get so close and then see politics get in the way. And as I gave the commencement at Miami Dade, it broke my heart knowing that a number of those promising, bright students young people who worked so hard and who speak to what s best about America are at risk of facing the agony of deportation. These are kids who grew up in this country, love this country, and know no other place as home. The idea that we would punish them is cruel and it makes no sense. We are a better nation than that. So we re going to keep up the fight for the Dream Act. We re going to keep up the fight for reform. And that s where you come in. I will do my part to lead a constructive and civil debate on these issues. We ve already held a series of meetings about this at the White House in recent weeks. And we ve got leaders here and around the country helping to move the debate forward. But this change has to be driven by you to help us push for comprehensive reform, and to identify what steps we can take right now like the Dream Act and visa reform areas where we can find common ground among Democrats and Republicans to begin fixing what s broken. I am asking you to add your voices to this debate and you can sign up to help at whitehouse.gov. We need Washington to know that there is a movement for reform gathering strength from coast to coast. That s how we ll get this done. That s how we can ensure that in the years ahead we are welcoming the talents of all who can contribute to this country; and that we are living up to that basic American idea: you can make it if you try. That idea is what gave hope to Jos Hern ndez, who is here today. Jos s parents were migrant farm workers. And so, growing up, he was too. He was born in California, though he could have just as easily been born on the other side of the border, had it been a different time of year, because his family moved with the seasons. Two of his siblings were actually born in Mexico. They traveled a lot and Jos joined his parents picking cucumbers and strawberries. He missed part of the school year when they returned to Mexico each winter. He didn t learn English until he was 12. But Jos was good at math, and he liked it. The great thing about math was that it s the same in every school, and it s the same in Spanish. So he studied hard. And one day, standing in the fields, collecting sugar beets, he heard on a transistor radio that a man named Franklin Chang-Diaz a man with a name like his was going to be an astronaut for NASA. Jos decided that he could be an astronaut, too. So he kept studying, and graduated high school. He kept studying, earning an engineering degree and a graduate degree. He kept working hard, ending up at a national laboratory, helping to develop a new kind of digital medical imaging system. And a few years later, he found himself more than 100 miles above the surface of the earth, staring out the window of the Shuttle Discovery, remembering the boy in the California fields with a crazy dream and an unshakable belief that everything was possible in America. That is what we are fighting for. We are fighting for every boy and girl like Jos with a dream and potential just waiting to be tapped. We are fighting to unlock that promise, and all that it holds not just for their futures, but for the future of this great country.Thank you. God bless you. And may God bless the United States of America. 


