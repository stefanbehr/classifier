Illinois s Rep. Jan Schakowsky sent a letter to Mitt Romney ahead of his fundraising trip to Chicago, calling on him to answer major questions her constituents and all Americans want to know about where the famously flip-flopping Romney really stands when it comes to issues affecting the middle class. 

Rep. Schakowsky wrote: 

Dear Governor Romney, 

I want to welcome you to the great state of Illinois. Throughout your trip to the state, I m confident you will get to know the hardworking, entrepreneurial and resilient people that I am privileged to represent every day as a Member of Congress. I hope you take some time to talk with them about many of the challenges facing our state and our country, but also about the great faith that they have in getting our economy working again and creating jobs for middle class families. 

As part of your visit, I want to pose a few questions about your plans our constituents would be interested in the answers to: 

1) Why do you feel like the wealthiest Americans and corporations need more tax breaks while at the same time criticizing President Obama s plan to extend tax breaks to the middle class as little Band-Aids a plan that would result in a tax cut of $1,640 for a typical Illinois family? 

2) In Nevada, you said, Don t try to stop the foreclosure process. Let it run its course and hit the bottom. Do you think it is fair to propose more tax breaks for Wall Street CEOs whose decisions led to the collapse of the housing market, while telling homeowners in Illinois and elsewhere who were scammed, cheated or saw their home s value plummet because of the risky bets of Wall Street that they have to fend for themselves? 

3) In August you said, I love a flat tax. Is this still the case? And if so, what would a flat tax mean for middle-class families in Illinois? 

4) Your groundbreaking work on health reform in Massachusetts, including the notion of individual responsibility, was a model for the nation and for the Affordable Care Act which President Obama signed in to law. Why then do you keep distancing yourself from the very reforms that you said were a model for the nation? 

Thank you for taking some time to consider these important questions. 

Sincerely, 

Jan 
