As part of their ideological crusade against women s rights, Indiana Republicans passed a law in May that cut off Medicaid funding to Planned Parenthood in their state, a nationwide women s health care provider. Now that private donations have elapsed, Planned Parenthood is closing several locations and roughly 9,300 low-income women will no longer have the same access to health care. 

Indiana Republicans targeted Planned Parenthood because one of the services they provide is abortion services. Though it s only 3 percent of what they do and no federal dollars are spent on that procedure, Republicans made funding for the organization an ideological touchstone and now low-income women are paying the price. 

Think Progress reports that Planned Parenthood focuses on contraception, sexually transmitted diseases (STDs) testing and treatment, and cancer screening and prevention. But now that private donations have run out, the organization is closing several locations and leaving thousands of Indiana women without care: 

According to Indiana Planned Parenthood president Betty Cockrum, the clinics will stop treating Medicaid patients today. Our 9,300 Medicaid patients , including those who had appointments Tuesday, are going to see their care disrupted. 

To reduce costs, all 28 clinics will close tomorrow and employees will be sent home without pay. Only one clinic in Indianapolis will stay open Wednesday but will close Thursday. 

Should the law remain in place, Planned Parenthood stated that it must close eight of its clinics : two Indianapolis locations and clinics in Bedford, Hammond, Michigan City, New Albany, Terre Haute and Muncie. The shuttered clinics would significantly restrict help for the 85,000 Hoosiers the states clinics currently serve. 

Washington Post blogger Ezra Klein explains the net costs Planned Parenthood saves the federal government: 

The services Planned Parenthood provides save the federal government a lot of money. It s somewhat cold to put it in these terms, but taxpayers end up bearing a lot of the expense for unintended pregnancies among people without the means to care for their children. The same goes for preventable cancers and sexually transmitted diseases such as HIV/AIDS. You can find a lot more information about Planned Parenthood and its services here . 

Planned Parenthood filed an injunction to block the Indiana law. A federal judge is expected to rule on that suit by July 1 st . 
