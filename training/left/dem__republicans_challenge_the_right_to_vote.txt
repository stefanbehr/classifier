Across the country, in states ranging from Ohio to Florida to Wisconsin, Republican policymakers are pushing legal changes that make it harder for citizens to vote. 

Over the weekend, the New York Times covered the growing controversy : 

Most of the measures would require people to show a form of official, valid identification to vote. While driver s licenses are the most common form, voters can also request free photo IDs from the Department of Motor Vehicles or use a passport or military identification, among other things. 

But Democrats say thousands of people in each state do not have these. The extra step, they add, will discourage some voters who will have to pay to retrieve documents, like birth certificates, for proof to obtain a free card. If voters do not have the proper identification on Election Day, they can cast provisional ballots in most states but must return several days later to a local board of elections office with an ID. 

A few state bills and laws also shave the number of early voting days, a move that Democrats say would impact Democratic voters once again. In the 2008 presidential election, a majority of those who cast early votes did so for President Obama. 

Creating barriers to the ballot box is a duplicitous way to win an election, and taken together, these laws suppress the votes of millions of eligible citizens 

We ve been studying the effects of Republican voter suppression proposals, and as part of that effort, the Voting Rights Institute has released a report that highlights the real cost of these laws . 
