Mitt Romney and Paul Ryan share a clear vision for the future - ending Medicare as we know it so that millionaires and billionaires can get a tax cut, and paying for it with deep budget cuts that cost jobs and hurt average Americans, especially seniors, veterans, and children. 

The Romney-Ryan proposals would cut over $1 trillion from Medicaid over the next ten years. Both plans would result in over 50 million people being denied coverage, including low-income children, pregnant women, nursing home patients and people with disabilities. Watch our video and share with your friends. 
