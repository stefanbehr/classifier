The Atlantic has a good explanation of why Romney s Nevada victory over the weekend is "less than it appears." 

On Saturday, Romney took 50 percent of the fewer than 33,000 votes cast in the Republican presidential caucuses. Compare that to 2008, when Romney earned 51 percent of 44,000 votes cast in the Republican caucuses. This year, Romney not only received numerically fewer votes, but he also earned a smaller percentage of the votes cast. The enthusiasm s clearly not there. 

Molly Ball writes: 

"This year, with more attention paid, Nevada was supposed to be the state that would give Romney a resounding victory and begin to seal his own unstoppable status. A 29-point victory is resounding enough, and this time, Romney had a bit more competition in the state. But the lower enthusiasm for his candidacy could be a bad sign for Romney, and for Republicans, in the general election. " 
