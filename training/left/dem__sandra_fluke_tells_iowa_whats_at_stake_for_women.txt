Sandra Fluke hit the campaign trail with the Gotta Vote bus to spread the message to the women--and men--of Iowa City just how high the stakes are in this election. 

Earlier this year, Sandra Fluke drew the wrath of conservatives when she testified before Congress arguing that her law school s lack of contraception coverage hurt women, who often use birth control for preventive care. Rush Limbaugh led the right-wing charge, verbally attacking her as a "slut" for speaking out on an issue she believed in: access to women s health care. 

Fluke has become an outspoken women s rights advocate, and today, she hit the campaign trail with the Gotta Vote bus to spread the message to the women and men of Iowa City just how high the stakes are in this election. 

"This November, we have a real choice between candidates with two visions. There s a lot at stake. Mr. Romney believes that a woman s employer should be the one who decides what kind of health care she should have. President Obama believes that trust belongs in a woman, that she can make that decision with her doctor and family. 

"Mr. Romney has refused to stand up for equal pay. Our generation thought we wouldn t have to keep pushing on this. President Obama has been a clear leader. The Lilly Ledbetter Act was the first bill that he signed, and he s not done yet. He s called for the passage of the Paycheck Fairness Act. 

"And when we ve needed him most, President Obama has stood with women and defended our access to health care when he defended Planned Parenthood. Mr. Romney, by contrast, has said he wants to defund Planned Parenthood. That means taking away breast cancer screenings, cervical cancer screenings, and care for moms and babies. That s not the leadership or the vision that women deserve. 

"So if you believe in a woman s right to fair pay and her ability to enforce that, you gotta vote. If you believe in an America where a woman can make decisions about her own body you gotta vote, and you gotta vote for President Obama." 

Gotta Vote 
