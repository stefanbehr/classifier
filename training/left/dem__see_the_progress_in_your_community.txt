During the past several months, we ve talked a lot about the ways Democrats are moving America forward. Today, we re launching a new website called PROGRESS that helps you see first-hand what this means for you and the people in your community. 

Progress.Democrats.org provides a localized view of what these reforms have meant for Americans state by state, community by community. 

For curious voters, or volunteers on the ground, this website helps tell the story of the progress made over the last 20 months. 



A volunteer in Columbus, Ohio, can let his or her neighbors know that 13,300 small businesses received tax cuts in their congressional district. Parents in St. Louis can tell their college-aged children that they are among the 51,000 young adults in Missouri s 2nd District that can stay on their parents health care coverage because Democrats fought for them. 

If you re in Wisconsin, you can tell your friends and neighbors that the Recovery Act provided a tax cut to 285,000 families in Wisconsin s 2nd Congressional District alone. You can tell them the story of ZBB energy, an energy company in Menomonee Falls that s using a Recovery Act grant to research new batteries for the renewable energy industry. 

If you re going door-to-door in Lancaster, Pennsylvania, you can talk to voters about how 14,440 small businesses in their congressional district are now eligible for health care tax credits under the Affordable Care Act, or about the 5,900 teachers jobs that were saved there by the state-aid bill passed by Democrats and signed into law by President Obama. 

The numbers are impressive. But behind each of these numbers are stories of real people whose lives and communities have been made better by the changes Democrats have made children who now have access to health insurance, students who now have a shot at a college education, workers who now have a job, middle-class families who now have a little extra security. 

We know we have a long way to go to dig out of the hole we found ourselves in, but step by step, our country is moving forward. 

Take a look at the progress in your community and then share the information with your friends and neighbors. With 19 days until the election, it s more important than ever that voters see what s at stake, and understand that the choice this November is to build on this progress, or to turn our backs on the families and communities that are still recovering from the policies that led to this recession. 

The story of the past 20 months is the story of progress, and it s a story that needs to be told now more than ever. 
