DNC Chair Debbie Wasserman Schultz just sent out this email blasting Mitt Romney and the GOP presidential candidates for saying they would start our foreign aid to Israel at "zero": 

Here s something I never thought I d hear an American presidential candidate pledge to do: 

Cut all foreign aid for Israel to zero. 

But that s exactly what happened at the Republican debate on Saturday, when Mitt Romney, Newt Gingrich, and Rick Perry raced each other to the extremes of Tea Party isolationism, saying they d "start everything at zero" in the foreign aid budget and force Israel and every other ally to make their case for receiving American assistance. 

It s outrageous and dangerous and it shows a critical ignorance of how a president needs to act. It is never responsible to raise doubts about our commitment to the security of a key ally like Israel. 

Stand up to the extreme elements of the Republican Party join the call to reject "zeroing out" our foreign aid to Israel . 

Mitt Romney and other Republican candidates have spent a lot of time lately saying how much they support Israel and openly questioning President Obama s commitment to the Jewish state. 

But a stance like this tells us two really important things: 

1) These guys are so eager to please the most extreme elements of their Tea Party base that they d forget about one of the most loyal allies our country has. 

2) They fundamentally don t understand our current foreign policy agreements, like the commitments we ve made to Israel that establish certain levels of aid for years to come. 

At the end of the day, foreign aid is a tiny fraction of the federal budget less than 1 percent that goes a long way to support our national security and economic goals abroad. The cuts these candidates propose wouldn t make a dent in the deficit, but they would wreak absolute havoc on our foreign policy and America s standing in the world. 

In typical fashion, the Romney campaign tried to say two different things to two different audiences, releasing a statement to try to walk back his words saying he was referring only to Pakistan. But one look at the transcript shows otherwise: "One of the things we have to do with our foreign aid commitments, the ongoing foreign aid commitments, I agree with Governor Perry. You start everything at zero." 

While his campaign is already trying to wiggle out of it, Romney himself has been conspicuously silent on the matter. He might be hoping he can get away with pandering to the Tea Party isolationists, but we won t let him. 

Tell Romney, Gingrich, and Perry that it s absolutely unacceptable for any presidential candidate to suggest zeroing out our foreign aid to Israel add your name today to fight back. 

Thanks, 

Debbie 
