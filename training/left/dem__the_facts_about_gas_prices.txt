While Americans are hurting at the pump, Republicans have been trying to score political points off rising gas prices. They re placing the blame on President Obama, but as even Rupert Murdoch s Wall Street Journal and the Koch brother funded Cato Institute note, the facts are simply not on the GOP s side. 

Here s the condensed version: Oil and gas development has increased every year since President Obama has been in office. Domestic oil production, in particular, is now at an eight-year high, and our dependence on foreign oil is at a 16-year low. But we can t just drill our way to lower gas prices. One reason is the price of crude oil is set on the world market, and it s affected by other things we can t control. Global demand is soaring, thanks to countries like China, India, and Brazil. And world events like the instability in the Middle East cause unpredictable price spikes. 

The fact is, "drill, baby, drill" will never meet our energy needs completely. The U.S. has 2 percent of the world s oil reserves, but we use 20 percent of the world s oil. 

This isn t a problem we can solve overnight. That s why the President is focused on developing all of our natural resources domestic oil, gas, wind, solar, and biofuels and encouraging fuel efficiency so we can reduce our dependence over time. Already, his "all-of-the-above" strategy is increasing our energy independence so families can save at the pump. 

We ve cut net oil and petroleum imports by 1 million barrels a day. President Obama has expanded domestic oil production by speeding up the leasing process and improving safety to prevent future spills. And we ve made record gains in clean energy development, including nearly doubling renewable wind, solar, and geothermal energy since 2008. And the Obama administration reached an agreement with American automakers that will nearly double fuel efficiency standards for passenger cars which could save families more than $8,000 per vehicle at the pump. 

Compare that to the Republican Party, led by presidential front-runners Mitt Romney and Rick Santorum. Romney increased a tax on gas by 400 percent as Massachusetts governor. As a candidate, Romney has no plans to stop the massive taxpayer subsidies for the oil and gas companies who are making record profits. And he and Santorum both opposed increasing fuel-efficiency standards at the pump. 

These Republicans records just don t match their rhetoric and neither do the facts. 
