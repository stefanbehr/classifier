President Obama explains how the most dire warnings about the Citizens United case are being proven valid, even as Republicans in Congress have blocked legislation to fix it: 



Back in January, in my State of the Union Address, I warned of the danger posed by a Supreme Court ruling called Citizens United. This decision overturned decades of law and precedent. It gave the special interests the power to spend without limit and without public disclosure to run ads in order to influence elections. 

Now, as an election approaches, it s not just a theory. We can see for ourselves how destructive to our democracy this can become. We see it in the flood of deceptive attack ads sponsored by special interests using front groups with misleading names. We don t know who s behind these ads or who s paying for them. 

...the special interests want to take Congress back, and return to the days when lobbyists wrote the laws. And a partisan minority in Congress is hoping their defense of these special interests and the status quo will be rewarded with a flood of negative ads against their opponents. It s a power grab, pure and simple. They re hoping they can ride this wave of unchecked influence all the way to victory. 

What is clear is that Congress has a responsibility to act. But the truth is, any law will come too late to prevent the damage that has already been done this election season. That is why, any time you see an attack ad by one of these shadowy groups, you should ask yourself, who is paying for this ad? Is it the health insurance lobby? The oil industry? The credit card companies? 

But more than that, you can make sure that the tens of millions of dollars spent on misleading ads do not drown out your voice. Because no matter how many ads they run no matter how many elections they try to buy the power to determine the fate of this country doesn t lie in their hands. It lies in yours. It s up to all of us to defend that most basic American principle of a government of, by, and for the people. What s at stake is not just an election. It s our democracy itself. 
