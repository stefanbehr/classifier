Last Wednesday night, more than 85 members of the American Jewish and African American communities crowded the DNC for the first Voting Rights Passover Seder. The seder, inspired by the Passover celebration of the Jewish people s liberation from slavery in Egypt, brought together the two communities for a discussion of voting rights and how to get President Obama re-elected for four more years. 

Voting rights will be a major issue in the 2012 presidential election. A slew of new voting laws across the country aim to make it harder to vote. States that have ended early voting make it difficult for voters who are unable to leave work early to make it to the polls. Stricter photo ID laws disproportionately affect communities that are less likely to have current valid government IDs; more than 21 million American citizens lack access to accepted ID. In the face of tactics like these, Quincey Gamble, the DNC s voter protection field director, led a discussion on specific actions the two communities could take together to fight back against voter suppression. 



African Americans and American Jews share strong historical connections, including, most notably, the Civil Rights Movement. On the 44th anniversary of the assassination of Dr. Martin Luther King Jr., Aaron Jenkins of the Operation Understanding D.C., a nonprofit aimed at strengthening relations between the two communities, spoke about the long-standing friendship between Rabbi Abraham Joshua Heschel and Dr. King. In fact, before he died, Dr. King was planning to attend his first Passover seder with Rabbi Heschel. 



Now is the time to stand with the President against voter suppression efforts. Join our work to ensure four more years of continued progress. This group was fired up and ready to go. Are you? 
