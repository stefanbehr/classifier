Last week, a video revealed what Mitt Romney really thinks of half the country: "dependent upon government" and "victims." Not surprisingly, those people--the middle class, seniors, veterans, students, and low-income Americans--were outraged that someone running to be president for all Americans could write them off so casually. So Romney's trying to make amends through a new ad. 

Last week, a video revealed what Mitt Romney really thinks of half the country: "dependent upon government" and "victims." He told a group of wealthy donors at a closed-door fundraiser that it s not his responsibility to worry about those people because he ll "never convince them to take personal responsibility and care for their lives." 

Not surprisingly, those people the middle class, seniors, veterans, students, and low-income Americans were outraged that someone running to be president for all Americans could write them off so casually. So Romney s trying to make amends through a new ad. Despite everything he s said and proposed to the contrary, Romney looks straight into the camera and says, "President Obama and I both care about the middle class and poor. My policies will make things better for them." (Not you, them .) 

Here s what he doesn t seem to understand: When Election Day rolls around 41 days from now, those people won t remember an ad where a penitent Romney offers vague promises and refers to them in the third person. They ll remember the candidate who s "not concerned about the very poor," who announced to the world that he "likes being able to fire people," and who, when he doesn t think the cameras are rolling, says it s not his job to fight for 47 percent of Americans. 

They ll remember the real Romney. 
