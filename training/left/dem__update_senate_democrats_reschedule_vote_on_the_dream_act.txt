LATER UPDATE: The White House press office released the following statement in light of Senator Reid s decision to table today s vote on the Senate version of the DREAM Act in favor of the House version that passed last night. 

Last night s approval of the DREAM Act in the House of Representatives was a historic and important step. We agree with the Senate leadership s decision to table the version under consideration in that chamber in favor of taking up the version approved in the House. Eight Republicans voted together with Democrats to approve this important bill in the House last night. It should get bipartisan support in the Senate as well, and in light of the vote in the House, this is the right way to move forward to get that. The DREAM Act is the right thing to do for our nation, our economy, and our security and even reduces the deficit by $2.2 billion over the next decade. The President looks forward to seeing the Senate approve it so that he can sign it into law. 

Senate Democrats agreed to briefly postpone today s planned vote on their own version of the DREAM Act, and instead likely will consider the version passed last night by the House . 

The delay could strengthen the measure s chance of passing, as it now gives supporters more time to persuade undecided Senators, and potentially prevents the need for a follow-up vote in the House. 

While a future vote on the DREAM Act has not yet been scheduled, it s now expected to come sometime next week. President Obama, Congressional Democrats, committed volunteers, and DREAM Act and immigration reform activists will continue working toward passing this important legislation. 

Following the House vote to pass the DREAM Act, the President praised the bill as a step toward correcting one of the most egregious flaws of a badly broken immigration system, and pledged to continue to do everything we can to move forward on immigration reform. 
