Last Friday President Obama hosted the third White House Tribal Nations Conference, bringing together leaders from 565 federally recognized tribes. During the conference, tribal leaders met with administrative officials such as Interior Secretary Ken Salazar, Agriculture Secretary Tom Vilsack, Education Secretary Arne Duncan, and others to continue to build upon the President s commitment to strengthen the government-to-government relationship with Indian Country. 

Earlier that day, the President signed an executive order that establishes an initiative to help expand educational opportunities and improve educational outcomes for all American Indian and Alaska Native students. 

You can read the Improving American Indian and Alaska Native Educational Opportunities and Strengthening Tribal Colleges and Universities executive order here . 
