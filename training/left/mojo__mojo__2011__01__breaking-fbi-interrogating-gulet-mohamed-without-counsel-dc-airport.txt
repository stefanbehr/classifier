UPDATE : Gulet Mohamed was released over two hours after he was detained at Dulles airport by government agents. He left Dulles for his home in Alexandria, Virginia, without saying much about the questioning (at his lawyer's suggestion). But as he was entering a taxi, a reporter asked, What everyone wants to know is, are you a terrorist? Mohamed replied, I am not a terrorist. Here's a photo of him in the cab. 

UPDATE 2: Here's a video of some of the events of this morning: 



The video was shot by me and produced by the estimable Siddhartha Mahanta . Gulet Mohamed's lawyer, Gadeir Abbas, has a shaved head and a red tie. Gulet has closely-shorn hair and a beard and is wearing a greyish sweatshirt with an emblem on it. 

ORIGINAL POST : FBI agents have detained and are interrogating Gulet Mohamed, an American teen who was detained in Kuwait for a month, without counsel at Dulles International Airport outside Washington, DC, Mohamed's lawyer said Friday morning. 

Mohamed's entire family was waiting for the teen, who says he was beaten and harshly interrogated in Kuwait by unknown officials and intimidated and repeatedly interrogated by the FBI despite asking repeatedly for his lawyer and invoking his right to remain silent . But a customs official called Mohamed's lawyer, Gadeir Abbas, on an airport phone around 7:30 a.m. and informed him that Mohamed would not be emerging. When Abbas demanded to represent his client, he says the customs official suggested he call the FBI's 1-800 number and then hung up. (I witnessed Abbas' end of the phone conversation.) 

It's outrageous that after the manifestly objectionable treatment that the US government has visited upon Gulet that they continue to violate his rights and cause his family distress, Abbas tells Mother Jones . 

The FBI did not immediately return calls and emails seeking comment. Airport officials, TSA officers, and airport police present on the scene declined to explain the situation further. 

Mohamed's family and lawyer claim that Mohamed has asked FBI officials for counsel multiple times during previous questioning. US legal and constitutional restrictions generally require that custodial interrogations stop when a subject asks for his lawyer. That rule does not seem to have been followed in this case. Mohamed traveled to Yemen and Somalia, two hotbeds of anti-American extremism, in 2009 (to visit family and learn Arabic, his family says). But he has not been charged with a crime in any country. 

Airport officials are now trying to force television and other media to move their setup out of the main arrival area and down to the ground transportation floor.
