David Corn and CNBC's Maria Bartiromo joined Chris Matthews on MSNBC's Hardball to discuss the new labor statistics showing a small drop in the unemployment rate and what it will take from Obama to get big business hiring again . 



David Corn is Mother Jones ' Washington bureau chief. For more of his stories, click here . He's also on Twitter and Facebook . Get David Corn's RSS feed .
