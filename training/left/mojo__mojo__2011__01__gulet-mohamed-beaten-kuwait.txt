UPDATE, January 7, 7:30 p.m. : Gulet Mohamed was not detained at the behest of the United States government, Philip Crowley, a State Department spokesman, told reporters on Friday. We are aware of his detention. We have provided him consular services, and we are ensuring his well-being, as we would for any citizen in detention. Mother Jones will have more as this story continues to develop. Keep reading below for why Mohamed's family and lawyer think the US was involved. 

Gulet Mohamed, an American teenager detained in Kuwait who claims to have been brutally interrogated there, was arrested and questioned by Kuwaiti security on behalf of the US government, his lawyer and family members charged on Thursday. 

Mohamed, a 19-year-old from Alexandria, Virginia, called the New York Times ' Mark Mazzetti and Salon 's Glenn Greenwald this week via a cell phone another inmate smuggled into the prison where he is being held. In the interviews, Mohamed recounted being severely beaten. He said he was forced to stand for hours, and that interrogators threatened to torture him with electricity and imprison his mother. 



Questions that Kuwaiti interrogators asked Mohamed indicated a level of knowledge about his family and actions that could only have been obtained from American law enforcement, the teen's lawyer, Gadeir Abbas, told the two reporters at a sparsely attended press conference Thursday afternoon. In fact, he added, interrogators mentioned a specific, off-the-cuff conversation Mohamed had at a mosque in the US some time ago a conversation that he claimed they could only have learned about through surveillance. Since the idea that Kuwaiti intelligence forces are spying on US mosques strains credulity, Abbas and Mohamed's family believe American officials were passing information to the Kuwaitis. 

Mohamed's case is an example of proxy detention, Abbas said. Instead of the US detaining and interrogating Mohamed, or using extraordinary rendition to send him to be tortured in Egypt or Syria, the government is taking one step back and trying to accomplish the same goal: the unlawful torture and detention abroad of an American citizen by a country that is known to engage in human rights abuses, Abbas argued. 

Salon 's Greenwald has suggested that Kuwaiti interrogators' questions about Anwar al-Awlaki, the American cleric and Al Qaeda propagandist who is in hiding in Yemen, are further evidence of American involvement in Mohamed's detention. Al-Awlaki has become an obsession of the Obama administration, Greenwald wrote Thursday, and the idea that [Kuwait] would do this to an American citizen without the American government's knowledge, if not its assent and participation, is implausible in the extreme. 

In a letter to the Justice Department sent Thursday, Abbas wrote that the manner of his detention and the questions asked of Mr. Mohamed indicate to him that he was taken into custody at the behest of the United States. The Justice Department did not respond to a request for comment. 



Zahra Mohamed, one of Gulet's six older siblings, offered a tearful defense of her brother at Thursday's press conference and urged the US government to allow him to return home. In many ways, his family says, Gulet was a normal American kid. He played basketball, had an iPhone, and obsessed with the game Madden NFL. But like many American teenagers, Gulet had a bad case of wanderlust. He wanted to travel abroad to learn more about his heritage, Zahra explained. He begged his mother to let him leave: after all, he'd never known his father, and he wanted to learn Arabic. Traveling to the Middle East would let him get to know his father's side of the family, get in touch with his roots, and learn the language of the Quran. 

He visited Yemen's capital, Sanaa, sometime around March 2009, and stayed with family there for three weeks. But his family (especially his mother) feared for him, his sister told Mother Jones , and eventually convinced him to leave. He then went to Somalia, where he stayed with another part of his late father's side of the family. 

Gulet didn't like Somalia it was too hot, and he kept getting food poisoning. So he went to stay with another uncle in Kuwait, and buckled down to finish his study of Arabic. He never met with militants like al-Awlaki, he told the Times: I am a good Muslim. I despise terrorism. He went to renew his three-month Kuwaiti visa several times, his relatives said, each time with no problem. But in December, when he applied for another, he was seized and thrown in prison. After hearing from Gulet every day for months (he'd sometimes ask his sister how Carmelo Anthony, his favorite basketball player, was doing), his family didn't hear from him for a week. 

In a late December conference call using the smuggled cell phone, Gulet told his family he had been tortured. They were shocked. He is a kid, he hasn't done anything, Zahra said Thursday. All we want is for him to come home; that's it. 

Kuwaiti officials have told Gulet's older brother, Mohed Mohamed, that they are ready to release Gulet, have no interest in him, and are only holding him at the behest of the United States government, Abbas said Thursday. Gulet told the Times that FBI agents visited him in prison to tell him that he could not return to the United States until he gave truthful answers about his travels. And American officials told the Times that Mohamed is on the no-fly list and cannot return to the US at this time. 

Prohibiting a US citizen from returning to the US or conditioning a citizen's right to return on answering questions is absolutely illegal, Ben Wizner, a staff attorney at the American Civil Liberties Union, told Mother Jones on Thursday. 

But this isn't the first time that US citizens have been detained abroad and questioned about their travels to Yemen and other Muslim countries, Wizner said. Often, these Americans are told they are on the no-fly list and forbidden from returning to the US. Those people do have a way out, however. The ACLU is in the midst of a long legal fight over the no-fly list. As part of that lawsuit, the group sought to obtain a court order preventing the government from using the list in these types of situations. After that, the government backed down, capitulated, and arranged for our clients abroad to be repatriated, Wizner says. Ever since that, every time someone has contacted us on behalf of a citizen stranded abroad we've been able to arrange for that citizen to get back using the threat of litigation. 



Gulet Mohamed has not been charged with any crimes. I went to school, studied the US Constitution, his sister says. What happened to the Constitution? I feel like that has been lost. He's such a kid, a little kid.
