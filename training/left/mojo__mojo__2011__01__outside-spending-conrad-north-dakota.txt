Never mind that the 2012 elections, in which a resurgent GOP will try to topple Barack Obama and reclaim a Senate majority, are nearly two years away. Never mind that the 112th Congress is just days old. Already the spending wars for the next election season have begun in an unlikely place: the political backwater that is North Dakota. 

A left-leaning independent advocacy group, Commonsense Ten, is doling out $30,000 for radio ads in North Dakota in defense of Sen. Kent Conrad, the Washington Post reports . The ads describe Conrad as a deficit hawk and lifelong North Dakotan, champion for our ranches, and family farms and fiscal conservative. Why now, and why Conrad? As it turns out, C10's new ad buy is a response to recent attacks on Conrad by the conservative American Future Fund. Based in Iowa, AFF recently bought $60,000 worth of air time for commercials that criticize Conrad for backing wasteful stimulus, massive Wall Street bailouts, and the budget-busting health care bill that Americans didn't want. 

Here's more from the Post : 

We saw last cycle what the Republican dominance in outside spending meant, said [C10 co-founder Jim] Jordan. We're going to do everything we can to play that to a draw, at least, in 2011 and 2012. 

In the last election, North Dakota Democrats took a major hit, with Republicans winning an open Senate seat and defeating former Rep. Earl Pomeroy (D). Given those gains, Republicans believe 2012 is the cycle where they finally can beat Conrad and have ramped up public pressure on him in the early stages of the race. 

Conrad has been mentioned as a possible retiree but has said little publicly about his future political plans. Unlike in 2010, when popular Gov. John Hoeven (R) ran and won a Senate race, Republicans have no obvious candidate to take on Conrad at the moment. 

Democrats should be encouraged that C10 is already wading into the 2012 fray. With 23 senators up for re-election (that includes independents Joe Lieberman of Connecticut and Bernie Sanders of Vermont) and only 10 GOP seats up for grabs, 2012 could very well be the year the Senate flips back to the Republicans. Which is to say, Senate Democrats are going to need all the help they can get. 

Print Email Tweet Bloggingheads: Corn and Pinkerton on Bill Daley, John Boehner, and Michele Bachmann Few Anti-Reform Dems Riding the Repeal Wave Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Kent Conrad on Healthcare 

Outside Spending: The Final Tally 

Following the Outside Money 

Crashing the Covert Campaign Spending Spree 

What do secretive independent expenditure groups hate more than disclosure rules? When we show up at their doors with a camera rolling. Outside Election Spending Doubles in Two Weeks 

Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
