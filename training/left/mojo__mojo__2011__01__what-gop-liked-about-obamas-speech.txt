Listening to Republicans' immediate reaction to President Obama's State of the Union address Tuesday night, you'd almost think they didn't hear the same speech. I thought he did a good job he almost sounded like a Republican, freshmen Rep. Raul Labrador (R-Idaho), said minutes after exiting the House floor. Another freshman, Rep. Cory Gardner (R-Colo.), told Mother Jones , It sounded like a lot more of the same a lot more government spending. Rep. Allen West (R-Florida) hit both points at once: It was a president caught between two worlds the world of trying to be a fiscal conservative and the world of trying to appease his base. 

Though Republicans were quick to slam the key pillars of Obama's speech his call for greater innovation, infrastructure spending, and education investments as just another stimulus, but the president's biggest critics were willing to admit there was a lot to like as well. 

Obama's promise to revamp the tax code garnered praise from Rep. Jeb Hensarling (R-Texas), a top-ranking conservative. If he wants to work with Republicans to fundamentally flatten the tax code of all the various loopholes and credits and deductions, [we're] happy to work with him on that, Hensarling said, as he waited in line for a cable appearance. 

Firebrand conservative Rep. Steve King (R-Iowa) said that he was pleasantly surprised by the president's nod to clean coal. It was couched in the language subtly, he said. But maybe he is giving us some openings for all energy all the time, which I'm for. 

West, the Florida freshmen, even showed off his copy of Obama's speech: In the margins, he'd scribbled, Good point! near the president's lines praising teachers and addressing illegal immigration. 

To be sure, the parts of the speech that drew the most Republican cheers also have the dimmest chance of becoming law: there's little political drive on either side for a comprehensive energy bill, immigration refrom, or tax-reform legislation. And Republicans are still bent on beating up Obama on the budget. Though the president's call for a freeze on all discretionary government spending drew a few gasps, King noted that that the freeze would only be a good thing if Obama were to roll back spending to 2008 levels a massive cut that House Republicans are demanding. 

In King's view, even the much-ballyhooed across-the-aisle seating arrangement showed the limits of bipartisanship: Members of each party couldn't applaud as a block, he noted, and so there wasn't much response from the crowd. I've never been a SOTU address and seen such a flat response.
