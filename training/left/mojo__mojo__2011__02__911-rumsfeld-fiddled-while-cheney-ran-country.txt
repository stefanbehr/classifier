In her interview last night with former Defense Secretary Donald Rumsfeld, author of a new autobiography, Diane Sawyer asked him about a tough decision he had to make on the morning of 9/11. Was it not difficult, she asked, to order military pilots to shoot down passenger jets that the government believed to be hijacked and headed toward targets in Washington maybe the White House, maybe the Capitol. For a moment, Rumsfeld dropped his generally arrogant stance, and instead looked as if he were about to cry as he recalled the agony he went through in making the decision. 

It might have been a poignant moment, were it not for the fact that Rumsfeld didn t make the decision. It was Vice President Dick Cheney who made it. And it was Cheney who was running the country that morning, with a confused Rumfeld watching from the sidelines. 

When the nation is threatened, it is the President, the Commander-in-Chief who must make the decision to engage the military. Under the law, he orders the Secretary of Defense to implement his commands down through the military chain of command. While President Bush was being shuttled around from bunker to bunker on the morning of September 11, 2001, supposedly out of cell phone contact at times, Rumsfeld was next in line. But Rumsfeld s role on 9/11 has always been a mystery. In his new book, on page 339, the former secretary of Defense casts a little light on what he did that morning . 

Feeling the Pentagon shake when American Airlines Flight 77 hit at 9:38, and seeing the smoke, Rumsfeld, by his own report, rushed into the Pentagon parking lot, which was in chaos amid frantic rescue efforts and treating the wounded. Then he returned to his office. He spoke briefly to Bush, who was on Air Force One flying around somewhere in the southeast, who wanted to know about the damage to the Pentagon. From there Rumsfeld went to the military command post in the basement. And there, he writes, heeding the advice of General Dick Myers, the vice chairman of the Joint Chiefs, who was also in the room, he raised the threat level to a state of alert, and launched fighters to protect Air Force One. Rumsfeld was supposed to be removed to a secret site, but he says he was unwilling to be out of touch during the time it would take to relocate me to the safe site. 

Shortly afterwards, he writes, the Vice President reached me by phone. Cheney reportedly told Rumsfeld, There s been at least three instance here where we ve had reports of aircraft approaching Washington A couple were confirmed hijacked. And pursuant to the President s instructions I gave authorization for them to be taken out. 

In fact, there is considerable doubt as to when Cheney actually received the President s instructions, and considerable evidence that he acted on his own volition, as even the timid 9/11 Commission report makes clear . But in any case, his orders clearly violated the military chain of command something Rumsfeld failed to point out, according to his own account of the subsequent conversation. 

Yes, I understand, I replied. Who did you give that direction to? 

It was passed from here through the [operations] center at the White House, Cheney answered. 

Has that directive been transmitted to the aircraft? 

Yes, it has, Cheney replied. 

So we ve got a couple of aircraft up there that have those instruction at this present time? I asked. 

That is correct, Cheney answered. Then he added, [I]t s my understanding they ve already taken a couple of aircraft out. 

We can t confirm that, I told him. We had not received word that any US military pilots had even contemplated engaging and firing on a hijacked aircraft. 

We re told that one aircraft is down, I added, but we do not have a pilot report 

As it turned out the only other aircraft that crashed had not been shot down. It was United Airlines Flight 93, a hijacked plane that went down in a field near Shankville, Pennsylvania. 

This from the man directly charged under the law with putting into action the orders from the Commander-in-Chief. The Vice President is nowhere listed in the chain of command and has no authority to act. In the above passage, Rumsfeld himself describes how he essentially was a bystander that morning, with little or no input in the crisis. Our multi-billion-dollar Defense Department and its chief were unprepared, incompetent, and ignored as Cheney seized the reins and ran the country. 

Later, before the 9/11 commission, Rumsfeld provided a rather astonishing explanation for his behavior: 

The Department of Defense did not have responsibility for the borders. It did not have responsibility for the airports And the fact that I might not have known something ought not to be considered unusual. Our task was to be oriented out of this country and to defend against attacks from abroad. And a civilian aircraft was a law enforcement matter to be handled by law enforcement authorities and aviation authorities. And that is the way our government was organized and arranged. So those questions you re posing are good ones. And they are valid and they ought to be asked. But they ought to be asked of people who had the statutory responsibility for those things. 

In his book, Rumsfeld laments the fact he did not resign after Abu Ghraib. In truth, he should have resigned or been fired for failing to protect the nation in the face of the worst attack since Pearl Harbor.
