On Friday, I reported here that Democrats in Congress and campaign finance reform groups were plotting a strategy to legally challenge the tax-exempt status of powerful right-wing outside groups, including Crossroads GPS, American Action Network, and the American Future Fund. Earlier today, Citizens for Responsibility and Ethics in Washington did just that, formally requesting that the Internal Revenue Service investigate the American Future Fund to determine whether the Iowa-based group violated its tax-exempt status, which allows AFF to hide the identity of its donors. 

As I explained , challenges to 501(c)4 outside groups' tax status hinge upon one particular clause in the tax law. According to the IRS, 501(c)4 groups, otherwise known as social welfare organizations, must be primarily issue-driven outfits. A group like American Future Fund can engage in political advocacy, running ads that attack or express support for a particular candidate, so long as that political advocacy is not its primary activity. In a letter sent to the IRS on Tuesday, CREW alleges that AFF in fact made politicking its primary purpose. 

AFF's initial filings with the IRS claim the group's dealings were strictly issue based and non-partisan and that AFF does not support or oppose any candidate for public office. In the 2010 midterm elections, however, AFF spent upwards of $7.3 million expressly backing a particular candidate or calling for the defeat of another candidate. Another $2.2 million was spent on what are called electioneering communications, or election-time ads name-dropping a particular candidate. This evidence, CREW says, is reason to believe that AFF might have violated its IRS status. And if the IRS agrees, AFF would be forced to register as a political action committee and open up its books to the public. Given the amount of money the American Future Fund spent on ads in the 2010 congressional elections, it seems clear the primary if not only goal of the group is to elect Republicans to Congress, Melanie Sloan, CREW's executive director, said in a statement. 

A request for comment with the American Future Fund wasn't immediately returned on Tuesday. 

Here's more from CREW: 

The list of AFF's officers reads like a who's who of Iowa Republican politics. Nicole Schlinger, AFF's sole board member when it was organized, is the former finance director for the Republican Party of Iowa. The current president, Sandra Greiner, served as a Republican in the Iowa House of Representatives from 1992-2008, was elected to the state Senate in 2010. AFF reportedly got its initial seed money from Bruce Rastetter, CEO of Hawkeye Energy Holdings, who has been described by Iowa newspapers as a long-time power broker in Iowa politics. ... 

How can an organization that has a sitting Republican legislator as its chief executive claim to be non-partisan? said Ms. Sloan. It is clear the American Future Fund is a front for wealthy Republicans to anonymously spend millions electing Republicans to Congress. 

And here's CREW's complete letter to the IRS: 

2-1-2011 AFF IRS Letter 

Print Email Tweet Did Democrats Unwittingly Sabotage Health Reform? We re Still at War: Photo of the Day for February 2, 2011 Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Crashing the Covert Campaign Spending Spree 

What do secretive independent expenditure groups hate more than disclosure rules? When we show up at their doors with a camera rolling. Dems Eye Legal Attack on Shadow Spending Groups 

Lawmakers are crafting a plan to challenge the tax status of dark money outfits like Karl Rove's Crossroads GPS. House Votes to Gut Presidential Public Financing 

In a symbolic move, House Republicans open the door to corporate-controlled elections. The GOP s Campaign Finance Sneak Attack 

Boehner and Co. fast-track a bill to kill the presidential public finance system. Outside Spending Wars Reignite in Great Plains 

In North Dakota, 2012 attack ads are already hitting the airwaves. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
