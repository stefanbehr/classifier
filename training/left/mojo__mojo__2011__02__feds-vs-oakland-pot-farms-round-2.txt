In the most strongly worded warning to date, a federal attorney has threatened to crack down on industrial-scale pot farms should Oakland move ahead with a plan to permit and tax them. 

The Justice Department is carefully considering civil and criminal legal remedies regarding those who seek to set up industrial marijuana growing warehouses in Oakland pursuant to licenses issued by the city, US Attorney Melinda Haag of the Northern District of California warned in a letter sent to Oakland's city attorney on Tuesday. 

Since last year, when Oakland garnered national attention for its scheme to become the first city in the country to tax and regulate medical marijuana growers, it has repeatedly delayed the proposed law over legal concerns. 

It's still too early to say that the pot farm plan has completely gone to pot. Oakland City Council Member Desley Brooks, who makes a cameo in my recent feature on hempreneurs, has written a new draft of the Oakland ordinance that she thinks will pass muster. From today's Oakland Tribune : 

The new draft has specific language establishing a closed-loop relationship between cultivators and distributors which would keep the marijuana only in the hands of patients as well as making the patient relationships more explicit, which Brooks said address some concerns under state law.
