Facebook / Mike Hogan for Mayor February's been a busy month in the war on reproductive rights. Last week, MoJo 's Kate Sheppard broke the story about an effort in South Dakota to classify the murder of abortion doctors as a justifiable homicide (the bill was scrapped); this morning we told you about a similar effort in Nebraska, which the Omaha Police Department says could incite violence; and in Georgia, lawmakers are considering a bill that would conceivably permit the state to execute women who have miscarriages. 

The legislators behind these efforts have generally deflected criticism by arguing that their bills are being misinterpreted. But Jacksonville, Florida mayoral candidate Mike Hogan doesn't really have that option. Participating at a candidate forum at a Catholic church on Monday, Hogan emphasized his long-standing opposition to Roe v. Wade , which is to be expected from a conservative Republican. But then he went one step further : 

Hogan added that the only thing he wouldn't do was bomb an abortion clinic, then the law-and-order advocate added, with a laugh, but it may cross my mind. 

The Mandarin crowd applauded. 

In a follow-up interview with the Florida Times-Union , Hogan emphasized that his comments shouldn't be taken seriously, because he was only pandering. I mean, I'm not going to be politically correct, he told the paper. That was a joke. This was an audience for this. This is a Catholic Church. I guarantee you they are 110 percent pro-life. 

Opponents from both parties have slammed the comments, and Hogan, a tax collector and former city councilman, has backtracked somewhat. As he explained on Wednesday , Maybe I was thinking it was humorous, but it was not something I should have brought up. Since 1993, eight abortion doctors have been killed by pro-life activists. 

Like Phil Jensen , the author of South Dakota's since-scuttled justifiable homicide bill, and Nebraska state senator Mark Christensen, Hogan has also taken the initiative in combatting Sharia (Islamic law) from being implemented in his community. According to the Times-Union , Hogan warned that such extremism was already a problem in New England, and that he'd take a strong stand against it as mayor. That position, too, comes against the backdrop of violence: Last spring a pipe bomb exploded outside an Islamic center Jacksonville, prompting an FBI investigation.
