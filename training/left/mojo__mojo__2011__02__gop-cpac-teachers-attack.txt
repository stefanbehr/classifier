The fierce battle over federal spending playing out on Capitol Hill right now is dividing not only Democrats and Republicans but factions within the GOP itself. But one vocal House Republican says the real threat to the party comes not from within, or from folks across the aisle, but from gasp! teachers and their unions. 

On the second morning of CPAC, the annual gathering of the conservative movement, Rep. David Schweikert (R-Ariz.) told a packed ballroom that Republicans are under siege for their efforts to slash $100 billion or more from their federal budget in their first year in office. (The $100 billion number was part of the GOP's 2010 midterm campaigning.) An animated Schweikert went on, I'm already starting to see my personal Facebook page, my personal email accounts, being attacked by every schoolteacher union group, by groups that live on government money. 

What CPAC attendees need to do, the Arizona congressman continued, is fight back against the menace of teachers' unions by filling up your Twitter account, your Facebook account, your friends' emails with defending us to explain that $100 billion dollars is a drop in the bucket when next year's shortfall is one and a half trillion. 

Here's Schweikert's comments: 

How many of you have a Twitter account? How many of you have a Facebook account? How many of you have friends? 

Look, look. A lot of you gave money and time and bled to get this Congress back. But the war is going to explode this weekend when the bad guys start to see what reality is in the numbers. I'm already staring to see my personal Facebook page, my personal email accounts, being attacked by every schoolteacher union group, by groups that live on government money. If you aren't filling up your Twitter account, your Facebook account, your friends' emails with defending us, and explain that 100 billion dollars is a drop in the bucket when next year's shortfall is one and a half trillion, we're gonna get run over. We're gonna lose the messaging game... 

Whether you like it or not, you're the army. We gotta go to battle. 

Schweikert's comments wouldn't be the first time the right targeted teachers' unions. Who can forget conservative radio host Neal Boortz's comment from 2007 that teachers' unions are destroying a generation and are much more dangerous than al Qaeda ? Fox's Sean Hannity chimed in, too: They are ruining our school system. Great stuff.
