UPDATE: Jeff Cox has been fired . 

ORIGINAL POST: Wednesday morning, Mother Jones reported that Jeff Cox, an Indiana deputy attorney general, had called for using live ammunition against Wisconsin protesters . Cox's bosses have issued a statement noting that they are conducting an immediate review of the prolific tweeter and blogger and that the state attorney general will take appropriate personnel action when the review of the serious matter is complete. The statement: 

The Indiana Attorney General's Office does not condone the inflammatory statements asserted in the Mother Jones article and we do not condone any comments that would threaten or imply violence or intimidation toward anyone. Civility and courtesy toward all constituents is very important to this agency. We take this matter very seriously. 

An immediate review of this personnel matter is now under way to determine whether the assertions made in the Mother Jones article about an employee are accurate. When that review is complete, appropriate personnel action will be taken. 

The reporter who wrote the Mother Jones article informs us that the offensive postings over the weekend were made using a personal Twitter account and personal email, not a state government email account. 

As public servants, state employees should strive to conduct themselves with professionalism and appropriate decorum in their interactions with the public. This is a serious matter that is being addressed. 

Meanwhile, People for the American Way, a national progressive advocacy organization, has called for Cox to resign . The group says Cox's call for violence is beyond the pale and adds that he should step down immediately.
