Governor Mitch Daniels isn't the only Indianan taking a cue from his northern neighbors in Wisconsin: Protesting Hoosiers swarmed the statehouse in Indianapolis Tuesday, while House Democrats reportedly fled the state, and the GOP-led Senate has passed SB 575 [ PDF ], which would eliminate state teachers' collective bargaining rights. 

Several quorum calls have been attempted in the House, but there are not enough representatives to take a vote on the Right To Work bill that's currently before the Legislature. The body's Democrats, who are in an indefinite caucus, according to spokesperson Peg McLeish, are rumored to be out of state. An approaching deadline is at hand, and if bills aren't passed this week, they are dead. 

Meanwhile, at the anti-bill statehouse protests, ministers prayed; families and children milled; gray-haired, jean-clad union members held signs; students staged sit-ins; and guitarists' notes weaved through teachers' chants. The NAACP , along with many unions, showed up to protest the Right To Work bill. Our organization is part of the fabric of the Indiana tapestry, said NAACP representative George VanSickles when asked why their group showed. When anyone is oppressed or experiences injustice or segregation or isn't able to get a decent job or wage, the fight for freedom is activated automatically. 

Nevertheless, VanSickles downplayed comparisons to the labor protests that were playing out in Wisconsin's capital. We have our own mind, he said. The Dems have walked out before, when things went terribly wrong. 

A quick return for those Democratic representatives seemed unlikely. They will stay in caucus until these bills are removed from the agenda, Indiana's Democratic Party chairman, Dan Parker, told the Washington Post . They are not coming back. Under state law, either party can call a caucus a private meeting that cannot be interrupted and has no time limit which appears to doom the the right-to-work bill's prospects in the House. That didn't stop Senate Republicans, however, from passing their version of the bill, in 30-to-19 vote right along partisan lines. 

Although Mitch Daniels has encouraged legislators to hold off on the hot-button items, his sentiment was made clear in an interview with NPR 's Diane Rehm: The most powerful special interests in America today are the government unions, he said in a recent radio program. 

The bill would eliminate teachers' ability to bargain over anything besides wages and wage-related benefits. It would force parties in a dispute to submit to an arbitration process in which the school board would serve as fact-finder and arbitrator. It would prevent instructors from appealing arbitration decisions; allow the board to fire teachers and revoke their licenses at will; and repeal the existing minimum-salary provisions for teachers. 

Rick Muir, president of the Indiana Federation of Teachers , told Mother Jones in a weary voice that he was deeply saddened as he left the statehouse today. There's nothing good about this bill, he said. But then he corrected himself. 

If there's anything good, it's the spark that is getting more people to take a stand, he said. The laborers are strong and growing in intensity.
