Senate appropriations committee chairman Daniel Inouye (D-Hawaii) announced that his committee will implement an earmark moratorium for the remainder of the current session of Congress. 

From his statement: 

The President has stated unequivocally that he will veto any legislation containing earmarks, and the House will not pass any bills that contain them...The Appropriations Committee will thoroughly review its earmark policy to ensure that every member has a precise definition of what constitutes an earmark. To that end, we will send each member a letter with the interpretation of Rule XLIV (44) that will be used by the Committee. If any member submits a request that is an earmark as defined by that rule, we will respectfully return the request. 

Next year, when the consequences of this decision are fully understood by the members of this body, we will most certainly revisit this issue and explore ways to improve the earmarking process. 

Inouye's point: earmarked legislation, either in the House or on the president's desk, has no shot, so what's the point? But this represents a serious about-face for the eight-term senator. Back in fiscal year 2009, Inouye pulled in almost $220 million for projects in Hawaii. Translation: he could be looking at a tough two years.
