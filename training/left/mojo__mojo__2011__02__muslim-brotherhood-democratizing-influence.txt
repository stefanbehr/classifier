Is the Muslim Brotherhood, the Islamist party that's gained political recognition in Egypt, an antidemocratic force? Many US conservatives think so: The Muslim Brotherhood doesn't care about democracy, said John Bolton, the Bush administration s ambassador to the UN, in a recent interview with Fox News . Western-allied Arab dictators in Tunisia, Libya, and Yemen have made similar charges against Islamists in their countries. 

But Muslim Brotherhood scholar Joshua Stacher says his field research in Egypt indicates otherwise. An Arabic-speaking political scientist who teaches at Kent State University, Stacher has specialized in studying the Brotherhood and argues that since 2005, the group has functioned as Egypt's only real political party. 

During Egypt's 2005 parliamentary elections, Stacher set out to shadow candidates of Mubarak's National Democratic Party (NDP), but found them just so difficult to work with because they are the ones who are defending this untenable position. They have the policies that are unpopular, and they get upset when you ask about them. 

So instead, he turned to Brotherhood candidates, who offered open doors. (While the Muslim Brotherhood party was officially banned in Egypt, its supporters have gained seats in parliament by running as independents.) 

Stacher sat in on campaign meetings, attended rallies, hung out in campaign offices for days on end, and spent time with programmers who were creating online campaigns tailored to specific local concerns. One focused on stamping out corruption, for example, while another emphasized improving water filtration. Every candidate had their own website, and every governorate had their own candidate list, he recalls. It was amazing. 

Stacher was also impressed that Muslim Brotherhood candidates always lived in the communities they sought to represent and offered weekly office hours for constituents. Everybody came through those doors, he says. Poor laborers. Unemployed people. Women whose husband was in jail. And they had all sorts of demands from 'the cars are driving too fast on the street and there need to be speed bumps,' to 'my son is a university graduate and needs a job.' 

Stacher concluded that the Brotherhood's grassroots political machine represented a stark contrast to the largely top-down campaigning of Mubarak's party and to the weak infrastructure of other opposition groups. That's the secret to their success, Stacher says. They are connected and do outreach in the communities in which they live even in the face of routine intimidation and harassment. 

One candidate Stacher followed was teargassed on the way to his polling station along with a crowd of supporters. Despite similar intimidation across Egypt, 88 Muslim Brotherhood-affiliated candidates won seats in the 454-member parliament gaining a historic 20 percent minority. 

The following year, Stacher and a colleague, Samer Shehata, set out to research how the newly minted Brotherhood MPs were conducting themselves in Mubarak s traditionally rubber stamp parliament. Despite many analysts' predictions that, as Stacher puts it, they would simply deliver bombastic speeches from the floor, he came away struck by the group's professionalism and action on issues of substance. 

When the government published its annual Statement on Budgetary and Policy Priorities, the Muslim Brotherhood had a 300-page response ready. They also found ways to score points on the fly: When the discovery of bird flu in Egypt nearly destroyed the poultry industry in early 2006, the Mubarak government was criticized for dragging its feet. Meanwhile dozens of Brotherhood-affiliated parliamentarians set up a photo op outside Parliament munching grilled chicken. 

Several other researchers I spoke to concurred that Muslim Brotherhood elected officials have exerted a democratizing influence; that much is consensus not just with Egypt scholars but with scholars from across the Arab world, according to Bruce Riedel, a Middle East analyst at the Brookings Institution. 

Academics disagree, however, on the degree to which a Brotherhood-led government would protect Egypt's secular freedoms. The Brotherhood is sharply divided between pragmatic, open-minded moderates and hard-line conservatives bent on spreading fundamentalist Islamic teachings. 

Stacher maintains that continued repression would only empower the hardliners. By contrast, he says, if everyone has free range to participate, what we'll see from the Muslim Brotherhood is an increasing pragmatism. And this will drown out those conservative voices.
