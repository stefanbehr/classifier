Newt Gingrich has made abolishing the Environmental Protection Agency a central theme in what many believe to be the early days of a presidential bid. He first referenced the idea in a speech last week in Iowa, then elaborated on the plan in an email to his supporters. But if Gingrich is serious about the White House, he may went to throttle back on the EPA bashing. Doing away with the EPA is pretty unpopular with Americans even with Republicans, according to a poll released Wednesday . 

The poll was conducted by Opinion Research Center International and commissioned by the Natural Resources Defense Council and Health Care Without Harm. It found that 67 percent of Americans including 61 percent of Republicans opposed the idea of abolishing the agency. 

The poll also asked about efforts to strip the EPA's authority to act on greenhouse gases, the more realistic threat to the agency's mission right now, as both Democrats and Republicans in Congress have floated plans that would strip that authority to varying degrees. Yet another bill is expected on Wednesday from Rep. Fred Upton (R-Mich.) and Sen. Jim Inhofe (R-Okla.) that would eliminate that authority. 

According to the poll, 77 percent of Americans oppose efforts to restrict the EPA's efforts on air pollution, including 61 percent of Republicans. Democrats, Republicans, and Independents want politicians to protect the health of children and adults rather than protecting polluters, said Pete Altman, climate campaign director at the Natural Resources Defense Council. 

Only 25 percent of Republicans they polled actually supported Gingrich's call to abolish the agency outright by far the more extreme example of EPA-bashing we've seen in recent weeks. 

Not to be outdone, Gingrich's group American Solutions for Winning the Future released its own polling data in the midst of the NRDC's call with reporters. There was just one problem. Its data was from a 2007 survey that asked vague questions about whether America can have both economic growth and environmental protection, and whether innovation is a good thing. Not a single question dealt with extinguishing the EPA.
