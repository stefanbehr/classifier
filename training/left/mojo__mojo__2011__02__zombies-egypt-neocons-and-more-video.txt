For your viewing pleasure, here's a Bloggingheads diavlog between Daniel Drezner, professor of international relations at Tufts' Fletcher School of Law and Diplomacy and author of Theories of International Politics and Zombies , and an effete pasty round-faced dude who sounds like me. BHTV went whole-hog on Thriller music, after-effects, and hidden zombies. They're also offering a chance to win a copy of Drezner's book; check out the brain-sucking details below. Otherwise, enjoy the back-and-forth on who's the real walking undead: neocons, supply-siders, or Egypt protesters. Bonus: One (1) John Bolton mustache joke is hidden within! 



ZOMBIE HUNT! 

Win a copy of Theories of International Politics and Zombies ! Hidden in this diavlog are five different images from well-known zombie features (four movies, one TV show). The first reader of this blog to correctly identify when those zombie scenes appear in the diavlog and from what movie or TV show they were taken, gets a copy of Dan Drezner's new book. For a chance to win: Send an email to bloggingheadszombiehunt@gmail.com . In the body of your email, include a link to this blog post, the five different times (minute and second) in the diavlog when the zombie images appear, and the movie/show from where the images were taken. Contest ends at midnight on March 1. Happy hunting!
