Since the midterm elections this past November, a reenergized Republican party has forged ahead with plans to dismantle abortion rights on every front, at both state and federal levels. The developments are coming so fast and furious that it can be a little overwhelming, but here's a recap of some of the recent highlights, in order of publication date. 

1. The Man Who Loved Women Too Much Contributor Sara Blustain profiles Harold Cassidy, the lawyer behind a legal strategy that reframes abortion restrictions not as simply protecting the unborn, but rather as protecting women from the consequences of their decisions in other words, chipping away at a woman's right to choose in the name of women's rights. ( January/February issue ) 

2. Are You Sure You Want an Abortion? Using information provided by the Guttmacher Institute, I put together these maps showing which states have imposed abortion restrictions such as waiting periods, obligatory ultrasounds, or mandatory counseling that includes discredited medical information. ( January/February issue ) 

3. The House GOP's Plan to Redefine Rape Pretty much all abortion restrictions, in this case a ban on the use of federal money for abortions, contain a rape exemption. But DC-based staff reporter Nick Baumann exposed a recent Republican attempt to redefine rape as only forcible rape. Boy, did that piss people off. Baumann's story spread like wildfire even showing up (hilariously) on Jon Stewart . The GOP caved on that provision. (Jan. 28, 2011) 

4. Is Providing Abortions Creating a 'Nuisance'? In Wichita, Kansas, ground zero in the abortion wars, Dr. Mila Means wants to replace the murdered Dr. George Tiller as the area's last remaining abortion provider. But thanks to threats from anti-abortion groups, and the ruling of a judge who had previously donated to pro-life causes, nobody will rent Means any office space. Kate Sheppard reports from MoJo 's DC bureau. (Feb. 4, 2011) 

5. If You Thought the GOP's 'Rape Redefinition' Bill Was Bad... MoJo editorial fellow Maddie Oatman reports on a proposal that would let doctors refuse to abort a woman's fetus even if an abortion was necessary to save the life of the mother. (Feb. 8, 2011) 



6. South Dakota Moves To Legalize Killing Abortion Providers Kate Sheppard reports on a bill under consideration in the Mount Rushmore State that would have made preventing harm to a fetus a justifiable homicide in many cases. Her story caused a national uproar, forcing state legislators to table the bill. Nick Baumann later reported on similar bills introduced in Nebraska and Iowa . (Feb. 15, 2011) 



7. Revealed: The Group Behind the Bills that Could Legalize Killing Abortion Providers Nick Baumann and Dan Schulman, our DC-based senior editor, show us who's pushing all these justifiable homicide bills. (Feb. 28, 2011) 

8. Texas Considers Bill to Ban Almost All Abortions DC staff reporter Tim Murphy reports on a mind-bogglingly restrictive bill that was penned by anti-abortion activists and introduced in the Lone Star state without even the usual exemptions for rape and incest. Christ. (March 11, 2011) 

9. GOP Bill Would Force IRS to Conduct Abortion Audits Were you raped? Was it incest? These are the types of questions the government's tax police would have to ask women who've terminated pregnancies if Congressional Republicans have their way, Nick Baumann reports. (March 18, 2011) 

10. The Limits of Tax Jihadism Citing the article above, political blogger Kevin Drum makes the case that Republicans are willing to push their anti-abortion agenda even at the expense of their anti-tax orthodoxy. (March 18, 2011) 

Late-breaking honorable mention: Ohio's 'Heartbeat' Abortion Bill Moves Forward Jen Phillips digs into the details of a pending Ohio bill that would outlaw abortions after six weeks of gestation, a point at which point many women haven't even confirmed that they're pregnant. In other words, it more or less outlaws abortion. This one is even making anti-abortion activists nervous, because they're afraid the courts will smack it down, setting a precendent that might come back to haunt them. (March 29, 2011) 

Click here for more Mother Jones coverage of reproductive rights.
