Anti-choice state senators in Arkansas passed a bill on Thursday that could limit access to abortions for women in the state by subjecting clinics to the same standards as outpatient surgical centers. 

That bill would have the anticipated impact of making non-surgical abortions much harder to obtain in a state where it's already fairly difficult. The law would force clinics or doctors that provide women with abortion pills like RU-486 or Mifeprex to follow more stringent rules applied to outpatient surgical centers. The bill's Republican sponsor dubbed it the Abortion Patients' Enhanced Safety Act creating the impression that the bill is only designed to protect women. 

The state's Planned Parenthood branch says the measure is specifically designed to target them, since their two clinics in Fayetteville and Little Rock are the only ones that offer medical abortions in the state. They already only offer them up to 8 weeks after conception, and they don't provide surgical abortions, which make the new rules arbitrary, says the group. They would now be required to make major changes to their facilities, including providing recovery rooms and additional bathrooms, with no practical reason to do so. 

Abortion rights groups often refer to this type of law as Targeted Regulation of Abortion Providers or a TRAP law . The idea is that you make the regulations so burdensome that it becomes difficult, if not impossible, to actually provide legal abortion services. A similar effort was passed last month in Virginia that would make outpatient facilities subject to the same regulations as hospitals. 

It's a burden and it targets us specifically, Murry Newbern, director of community affairs at Planned Parenthood of Arkansas and Eastern Oklahoma, told Mother Jones . This is just a tactic that people that want to reduce access to safe, legal abortion use make it more expensive. 

Anti-abortion groups are praising the bill, with Americans United for Life Action claiming in a statement Friday that the measure will protect women and address the problem of dangerous conditions for women in abortion clinics.
