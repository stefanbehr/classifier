Courtesy of State Sen. Mae Beavers Last month, Tennessee state Sen. Mae Beavers introduced SB 1091 , a bill that would require presidential candidates to present a long-form birth certificate in order to qualify for the ballot in the Volunteer State. Beavers, a Republican, is in good company : Nearly a dozen states have now introduced similar legislation part of national campaign mounted by the birthers , those conservatives who believe that President Barack Obama was not born in the United States. To date they haven't had much luck; a bill proposed in Arizona looked the most promising but was scuttled in committee; on Wednesday, New Hampshire GOPers knocked down a similar proposal . 

It's a far-fetched goal, and it turns out that Beavers, who recently discussed her bill on Reality Check , a radio show devoted to debunking birther legislation, still has some research to do. From the transcript : 

RC: What are the specific requirements in the bill? 

MB: That they have to have the long form birth certificate. 

RC: What is the long form birth certificate? 

MB: Now, you're asking me to get into a lot of things that I haven't really looked into yet. 

The host then asked the obvious follow-up: why put a term into the bill, if you don't know what it means? Beavers responded, Well, we are following some of the bills that have been filed in lots of other states, and you know how it is, you file your bill and, you know, you prepare before you go to committee. 

File first, understand later? 

Beavers went on to state more clearly, I'm not entirely sure what long form means. She seemed genuinely surprised by the news that not all states even print long-form birth certificates anymore. I only know about Tennessee, she explained. As for her motives for introducing the bill, Beavers didn't declare herself as an outright birther, but she noted, I think people have raised questions about [Obama's birth] enough to make everybody wonder. Although the state of Hawaii has produced a certificate of live birth for Obama that has been been widely distributed , Beavers said proof of Obama's citizenship must have gotten buried in her inbox: I get emails all the time with things in them, you know; I can't honestly tell you that I read all of them, because I get so many. 

Beavers' long-form slip-up fits a trend of Republican state lawmakers amping up extreme right-wing legislation with dubious supporting evidence. As we reported last month, South Dakota state Rep. Phil Jensen floated a measure banning Islamic Sharia law that would have also undone child custody protections , and another bill that could have provided an opening for the killing of abortion providers . Alabama state Sen. Gerald Allen borrowed his own anti-Sharia bill from Wikipedia , and when asked by a reporter what Sharia actually is, said, I don't have my file in front of me. Texas state Rep. Leo Berman, who introduced both an anti-Sharia bill and a birther bill, recently explained that he got most of his political information on YouTube because YouTubes are infallible . 

Beavers did not respond to multiple requests for comment.
