Charles G. Koch, the right-wing titan of industry, is a very tight-lipped guy, just like his billionaire brother David Koch. But today Charles Koch has gone public with a Wall Street Journal op-ed titled Why Koch Industries Is Speaking Out. In it, Koch decries the years of overspending that have brought us face-to-face with an economic crisis. He blames this crisis on both Democrats and Republicans who've done a poor job managing our finances. Koch explains how he, his family, and his multi-billion-dollar company, Koch Industries, have tried to support politicians like Wisconsin Republican Governor Scott Walker who are working to solve these problems. 

But here's where it gets interesting. Koch goes on to rail against businesses who have successfully lobbied for special favors and treatment like government subsidies and regulations. For starters, Koch Industries has benefited plenty from government subsidies in the past. As the New York Observer reported, Koch companies have received subsidies from the Venezuelan government as part of a deal to sell Venezuelan-made fertilizer in the US; used US land subsidies for its Matador Cattle Company; and profited from private logging of US forests that wouldn't have been possible if the US Forestry Service hadn't built new roads with taxpayer money to un-logged lands, among other examples. (For much more on the Kochs' use of subsidies, check out this ThinkProgress post .) 

And while Charles Koch criticizes crony capitalism, his company is one of the biggest players in the nation when it comes to lobbying and political donations. According to the Center for Responsive Politics, Koch Industries has spent more than $40 million lobbying the federal government in the past three years alone. Koch Industries, company executives, and the company's political action committee have doled out $11 million since 1989 to federal candidates, political parties, and political committees; Charles and David Koch and their wives contributed $2.8 million of that, a mere $1,500 of which went to Democrats, according to the Public Campaign Action Fund (PCAF). Much of that spending has gone toward fighting new regulations of the oil and gas industry, which would hurt Koch Industries' profits. Not surprisingly, then, lawmakers on the influential House energy and commerce committee have pocketed $630,950 in Koch-connected donations. 

Koch's concerns about the fiscal health of the US, as voiced in his op-ed, are not unfounded. But his criticism of lobbying and crony capitalism flies in the face of his own actions and those of companies, critics say. Koch Industries is the perfect example of absolutely everything Charles claims to hate about our current political system, David Donnelly, national campaigns director for Public Campaign Action Fund, said in a statement. The hypocrisy is palpable. 

Print Email Tweet Memo to Americans United for Life: Our Questions Still Stand Barbour on Gingrich: I m Crazy About Him Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Watchdog Wants Probe of Gov. Walker s State Patrol Visit 

Did the Wisconsin governor break the law by sending troopers to a Democrat's house? Did Gov. Scott Walker Break the Law During Prank Call? 

Campaign watchdogs are investigating Walker's statements to a caller pretending to be billionaire David Koch. Wisconsin Gov. Scott Walker: Funded by the Koch Bros. 

The politician trying to eviscerate public-sector unions is in sync with one of his largest financial backers--the right's infamous billionaire brothers. Did Scott Walker Get Crank-Call Pwned? (AUDIO) UPDATE: YES 

The Wisconsin governor, talking to a guy posing as a right-wing Koch brother, spills on the protesters and more. Wait till you hear the tape. The Koch Brothers Vast Right-Wing Media Conspiracy 

Watched any conservative programs lately? Chances are, you've seen an oil-funded pundit. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
