In the past few weeks, Haley Barbour has been snapping up top GOP political strategists for his likely presidential campaign. Barbour hired former Republican National Committee communications guru Jim Dyke to work for his political action committee, Haley PAC, perhaps drawing on Dyke's experience at the powerhouse outside spending group American Crossroads. 

Barbour's latest hire to his PAC is Sally Bradshaw, a Florida politico who has advised former Florida Governor Jeb Bush and worked on Mitt Romney's failed presidential campaign in 2008. Despite her ties to Romney, who's still considered a frontrunner in the 2012 race, Bradshaw told the Miami Herald that Barbour is the right fit for me. 

Here's more from the Herald : 

Barbour has said he won't announce his White House intentions until April, after the Mississippi legislature ends it session. He ruffled feathers last week when he suggested to a conservative audience in Iowa that the U.S. should consider cuts to the defense budget in an effort to streamline government spending... 

Bradshaw said that kind of candor is what attracts her to Barbour. We need a president who is not afraid to be bold,'' she said. Haley is not afraid of speaking his mind. 

Bradshaw has worked for the Mississippi native once before, in the mid-1980s, when she was a Washington intern for Barbour when he was President Reagan's political director. 

By hiring Bradshaw, a veteran strategist in Florida, Barbour is all but signaling that he's going to run in 2012. On Tuesday, he travels to Nevada , another battleground primary in the presidential race, to meet with Republican Governor Brian Sandoval and some state legislators. Put simply, Barbour is quietly amassing support in crucial states Florida, Nevada and laying the groundwork for a strong presidential bid. He may not officially be in the race, but it's only a matter of time before he puts his name in the hat.
