The church-going, plainspoken, socially conservative voters of Iowa: They're the key electorate for GOP candidates in that state's curtain-raising presidential caucus. Unlike in years past, when the GOP field featured an obvious favorite of the Iowa evangelical crowd Mike Huckabee in 2008, George W. Bush in 2000 and 2004 the 2012 Republican caucus is shaping up to be a wide-open battle. Huckabee has yet to say if he's in or out, while Sarah Palin, who boasts a strong evangelical backing, remains on the bubble, watching her public support plummet . 

Here's Politico 's Maggie Haberman with a smart look at how the 2012 GOP field might fare out in the heartland come January: 

Both Huckabee and Palin skipped the Iowa Faith and Freedom Coalition confab on Monday night, yielding the stage to a second tier that included Newt Gingrich, Tim Pawlenty, Rick Santorum, Buddy Roemer and Herman Cain, all of whom are seeking the votes of the state s influential evangelicals. 

Because many of the likely-to-run hopefuls are close together on social issues, conservatives have one of their biggest candidate pools to chose from in some time... 

National top-tier hopeful Mitt Romney will face struggles with some evangelicals, but he has a base to start from thanks to his competition in the caucuses in 2008, although how hard the former Massachusetts governor plans to compete in Iowa remains a question mark. 

Other options for evangelical voters are Mississippi Gov. Haley Barbour, who many believe will fare strongly in Iowa's retail-based political system, and Minnesota Rep. Michele Bachmann, a tea party favorite who seems poised to score voters who might have been with Palin.
