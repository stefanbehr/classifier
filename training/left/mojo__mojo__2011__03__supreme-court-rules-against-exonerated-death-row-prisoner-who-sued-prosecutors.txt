Last October, Mother Jones published a long piece about the case of John Thompson, who spent 14 years on death row before he was exonerated--based on evidence that had been purposefully withheld by prosecutors in the office of New Orleans DA Harry Connick Sr. A Louisiana jury found the DA's office culpable for Thompson's ordeal (which included coming within weeks of execution before the exculpatory evidence was revealed), and awarded him $14 million in compensatory damages. 

The state appealed the jury's verdict all the way to the U.S. Supreme Court, which yesterday ruled against Thompson and stripped him of his award. As reported by the Washington Post : 

Conservative justices prevailed in the 5 to 4 ruling , which shielded the district attorney s office from liability for not turning over evidence that showed John Thompson s innocence. 

Justice Clarence Thomas said Thompson could not show a pattern of deliberate indifference on the part of former district attorney Harry Connick Sr. in training his staff to turn over evidence to the defense team. 

It was the first decision of the court term that split the justices into ideological camps, and Justice Ruth Bader Ginsburg emphasized her disagreement by reading a summary of her dissent from the bench. 

I would uphold the jury s verdict awarding damages to Thompson for the gross, deliberately indifferent and long-continuing violation of his fair trial right, she said, adding that she was joined by Justices Stephen G. Breyer, Sonia Sotomayor and Elena Kagan. 

She said the actions of prosecutors under the control of Connick, who left office in 2003 and is the father of the famous singer of the same name, dishonored the obligation to turn over evidence favorable to the accused established in Brady v. Maryland nearly 50 years ago. 

Ginsburg also wrote that Connick s deliberately indifferent attitude created a tinderbox in which Brady violations were nigh inevitable. As we wrote in October, many other convictions secured by the office have also been overturned, all due to suppression of evidence. They all try to portray it as rogue prosecutor; a fluke, said New Orleans Defense Attorney Nick Trenticosta, but Harry Connick used to give awards to prosecutors for successfully convicting people. 

Connick, Trenticosta said, created a culture where convictions were won at any cost. The office's zeal for sending people to death row was such that a New Orleans prosecutor kept on his desk a model electric chair holding photos of five condemned men--John Thompson among them. Trenticosta has called the prosectors actions calculated measures to take people s lives away . 

Findings by the Innocence Project of New Orleans back up this assessment. In a 2008 report , the Project reviewed the record of Connick s 28-year-tenure, and found that the practice of suppressing evidence was so prevalent that it could be called a legacy in New Orleans. 

According to available records, favorable evidence was withheld from 9 of the 36 (25%) men sentenced to death in Orleans Parish from 1973-2002. Four of those men were eventually exonerated, having been released only after serving a collective 43 years on death row. In other words, one in every four men sent to death row by the New Orleans District Attorney s office from 1973-2002 was convicted after evidence that could have cast doubt on their guilt was withheld from them at trial. Four men, about 11%, were completely innocent. 

None of this kept Justice Antonin Scalia from concluding that Thompson spent 14 years on death row at Angola Prison because of the actions of a single miscreant prosecutor. 

Thompson, who went on to found the organization Resurrection After Exoneration , has called what New Orleans prosecutors tried to do to him an attempt at premeditated murder . In an interview with the Times-Picayune today, Thompson said, This fight started for me 18 years ago, and it feels like it's going to go on some more. There's a lot of innocent guys who's still on death row because of this corrupt system. 

As Kevin Drum pointed out this morning, the implications of the Supreme Court's decision in Connick v. Thompson reach far beyond John Thompson s life and even beyond New Orleans. A majority of the justices have concurred that a district attorney s office cannot be held responsible for the misconduct of its own prosecutors. As it stands, there are no deterrents to these prosecutors, said Nick Trenticosta. If they get caught withholding evidence so what? Nothing happens to them.
