For years, the big drugstore chains have stoutly denied selling prescription information patient names, contact information, doctors' names, and prescription details to pharmaceutical companies for marketing use. Now, that charade has come to an end with two class action suits, accusing CVS and Walgreen of doing just that. 

In a civil suit in Philadelphia County Court, as Courthouse News reports , the city s teachers union charged that consumers got unsolicited sales pitches after CVS allegedly sold customers private information to Eli Lilly and Co., Merck, AstraZeneca, Bayer, and other drug manufacturers. The union s claim states: 

Specifically, in exchange for the receipt of funds, direct promotional letters were sent to physicians of consumers by defendant CVS Caremark in order to promote and tout specific prescription drugs of pharmaceutical manufacturers who contracted with defendant CVS Caremark for use of prescription information, according to the complaint. 

While touted as an 'RXReview Program' by defendant CVS Caremark, in reality, the physician communications were nothing more than a profit-making opportunity, the class claims. 

CVS's scheme contradicts its public pronouncements as to the sanctity of both consumers' privacy and the physician-patient relationship, according to the complaint. 

Courthouse News goes on to report: In 2009, CVS settled related claims from the Department of Health and Human Services and Federal Trade Commission. Under the agreement with the health department, CVS paid $2.25 million to settle claims related to media reports that its pharmacies were throwing pill bottles with customers' personal information into open dumpsters. 

A second law suit, in California, alleges that Walgreen unlawfully sold medical information gleaned from patient prescriptions, according to a Reuters report . In this case the plaintiffs accuse Walgreen of depriving them of the commercial value of their own prescription information. 

According to the suit, brought by Todd Murphy on behalf of his two daughters and the rest of the class of consumers, Walgreen sells the prescription information to data mining companies who resell it to pharmaceutical companies for marketing purposes. The practice allows drugmakers to target physicians considered high-volume prescribers and those most willing to prescribe new medications, it said. 

So what's the point of all this? If the facts in these suits prove to be correct, then when you take a prescription down to CVS or Walgreen, you are effectively releasing the information to just about everybody in the drug and related industries. It most certainly opens you to the possibility of a blizzard of pitches. And it opens the door for pitches to doctors to prescribe the same or possibly different new drugs. In addition, there's no guarantee that this information won't follow you around when you go to get a health insurance or life insurance policy. In short, your most personal health information--what medications you take, and for what conditions--is available to anyone who has the price of admission.
