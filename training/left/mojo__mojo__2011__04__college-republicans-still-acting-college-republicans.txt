Here's the Dallas Morning News : 

An SMU junior and chairman of Texas College Republicans resigned his post this week after a video was posted of him describing getting hammered, hooking up with a young woman and calling political opponents a homosexual slur. 

He also calls his political opponents nerds, which, I'm told, is a word people used to use to make fun of other people in the late 1980s. The context is that this was part of an endorsement speech for Alex Schriver, a leading candidate for chairman of the College Republicans, who was so enthused by the speech he posted it on his website. Now, one of Schriver's opponents presumably a nerd has turned it into an attack ad , complete with scary background music and the requisite white-text-on-black-background. Witness: 



This is more or less business-as-usual for the College Republicans, who for decades have operated as basically a training camp for future GOP operatives (Karl Rove, Ralph Reed, Jack Abramoff, and Roger Stone are all alums). Here's what Benjamin Wallace-Wells wrote six years ago: 

[W]hen I talked to College Republicans in North Carolina, I heard constant, ridiculous allegations thrown at rivals within the organizations. This rival had an illegitimate son in Tennessee, that one paid for an abortion for some poor girl from Missouri. When I asked an innocent question about a network of political consultants in Raleigh, one College Republican stopped me imediately: Surely you must have heard, he said ominously, his drawl thick, about them bisexual orgies. 

For what it's worth, Charles McCaslin, the former Texas College Republicans chairman, has since apologized to any gays, women and, yes, nerds he may have offended.
