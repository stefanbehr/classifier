Forget job retraining or back-to-school money or even another stimulus package. An employment center in central Florida has the answer to ending the Sunshine State's chronic unemployment problem: Red super-hero capes. 

Yes, that's right. As WFTV Orlando reports , a new marketing initiative unveiled by an outfit called Workforce Central Florida (self-described as the region's workforce expert ) called the Cape-ability Challenge gives red capes to jobless Floridians as a way to boost their job-seeking prospects. The state-funded workforce organization reportedly spent $14,000 on 6,000 capes as part of the campaign, which a state workforce group called insensitive and wasteful. The capes fit in with Workforce Central Florida's comic book-inspired campaign that features a villain named Dr. Evil Unemployment. 

Now, the state is investigating Workforce Central Florida over the cape campaign. Hmm, wonder why. Here's more from WFTV: 

The newest allegation of misspending involves a marketing campaign, in which the chairman of the board for the job agency marches around in a super-hero cape. 

[...] 

Job-seekers such as Gregory Bryant said the capes are a waste of money and they're offended by the cartoon-like portrayal of being unemployed. 

Would you wear this around? WFTV reporter Bianca Castro asked Bryant. 

No, I mean, would you? Bryant answered. It's a mockery to Americans. 

The bizarre campaign, however, didn't last long. In a Wednesday press release, the group announced it was canning the cape idea, which it described as an admittedly out-of-the-box creative campaign.
