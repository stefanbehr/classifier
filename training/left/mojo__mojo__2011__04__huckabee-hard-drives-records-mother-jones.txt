Mike Huckabee read the Mother Jones story on the destruction of the records from his time as governor of Arkansas . He didn't like it. 

Speaking to US News and World Report yesterday , Huckabee slammed Mother Jones . [ Mother Jones ] doesn't pretend to be a real news outlet, but a highly polarized opinion-driven vehicle for all things to the far left, he said. You expect that wolves will eat meat. 

Here's part of Huckabee's defense: 

The absurd insinuation that my office destroyed state records or that records are missing is the same old political canard that was attempted years ago and failed then for the same reason it will fail now it's factually challenged. 

As we reported, these are not absurd insinuations. What happened to Huckabee's gubernatorial records was documented in this memo from the Arkansas Department of Information Systems. It confirms that the hard drives containing the records were erased and then destroyed. Copies of the records were placed in the hands of former Huckabee staffer Brenda Turner, who is now the communications director for a Christian greeting card company. Turner has not said what she did with the backups. She refused to talk to us. Here's the relevant part of the 2007 document, which was addressed to the outgoing Gov. Huckabee: 



We also reported that the state of Arkansas had to shell out $335,000 to replace the hardware that the Huckabee administration had destroyed. Huckabee doesn t deny this. But he blames the decision to spend that money on his Democratic successor, Gov. Mike Beebe, who wanted all new equipment, even though the existing hardware was operable and modern. Again, Huckabee is contradicted by the above mentioned memo, which notes that the drives have been subsequently crushed under the supervison of a designee of your office. That is, the drives were destroyed on Huckabee's watch, not Beebe's. 

The US News and World Report reports says the Mother Jones story suggests a sinister motive, which may well be true, to the extent that signing off on the destruction of political history seems a little shady. Can Huckabee a potential presidential contender who extols the cleansing virtue of transparency explain why these records were destroyed, and what happened to the backups handed to his aide? He's scheduled to appear on The Daily Show tonight. Perhaps he ought to show up with Brenda Turner and tell all whether it's funny or not.
