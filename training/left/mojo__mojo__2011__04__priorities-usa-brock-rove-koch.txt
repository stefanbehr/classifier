A pair of Democratic strategists have launched what they're billing as the left's answer to the flood of outside spending by Karl Rove's Crossroads groups and the Koch brothers' money machine. The group Priorities USA , run by former White House aides Bill Burton and Sean Sweeney , opened for business today, as did as the outfit's affiliated political action committee, Priorities USA Action. Sweeney described the groups in a statement as an effort to level the playing field. Americans deserve an honest debate about job creation, the economy, national security, and education. That debate will never happen if only right wing extremists are engaged on the battlefield. 

Burton and Sweeney join Media Matters founder David Brock and his team at American Bridge 21st Century , another liberal independent expenditure group, in building up the left's firepower in the outside spending wars . The need for such a response was painfully clear after the GOP's trouncing in the 2010 midterms , a landslide made possible by the efforts of American Crossroads, Crossroads GPS, and others like them, including American Action Network and Americans for Job Security. Democrats quickly realized they'd have to fight fire with fire to be competitive in 2012, given that right-leaning groups including the Chamber of Commerce are expected to raise more than $500 million to help elect Republican candidates in 2012. The Koch brothers alone are planning to raise $88 million. 

Brock's group, whose donors include a bevy of deep-pocketed supporters, is in the process of hiring staffers and raising cash to build up the group's war chest. Susan McCue, a former chief of staff to Senate Majority Leader Harry Reid, will serve on the group's board of directors, as will former Maryland Lt. Gov. Kathleen Kennedy Townsend. Former Reid staffer Rodell Mollineau will also work for the group. 

Diving headlong into the shadow spending wars, as I reported back in November, puts the left in something of a bind. It's Democrats, after all, who loudly decried the Supreme Court's Citizens United decision, which opened the floodgates to huge amounts of outside spending, and who demanded new safeguards to staunch the flow of shadow cash pouring into American elections. Now they find themselves with a foot in both camps: Still demanding new campaign finance regulations and mimicking the GOP's tactics from 2010. As one prominent Democratic donor told me in November, The Chamber and Crossroads and all them are going to be coming in full bore. So I think you will see donors engaged, and I'm not going to sit here and say we won't need to create some new groups...It's not that I don't want to see a meaningful legislative response to Citizens United, but I'm also not going to unilaterally disarm. 

Jonathan Collegio, a spokesman for American Crossroads, responded to Priorities USA's unveiling today with a sharply worded statement bashing Obama for his brazen hypocrisy which shows how cynical this President can be when it comes to perpetuating his own power. He added, This move may cause the biggest ordeal for the so-called 'good government' groups who publicly called for IRS and FEC investigations of conservative groups last year. To maintain their own 'nonpartisan' tax exempt status, will these groups call for investigations of the new non-disclosing liberal efforts? 

One watchdog group, however, said today there was nothing hypocritical about Priorities USA's plans. There will be those who call the establishment of Priorities USA hypocritical, said David Donnelly, national campaigns director for the Public Campaign Action Fund. They are either trying to score political points against President Obama or are unfortunately out of touch with what it takes to make political change in this country. In order to change the rules of the game, we need to engage in the rules as they are, not as we wish they were. To act otherwise after Citizens United is to take a knife to a gunfight. 

Print Email Tweet Donald Trump s F-Bomb Attack Yet Another Classic Breitbarting Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Will Secret Spending Divide Democrats? 

They denounced shadowy outside groups and dark money during the midterms. Now, as they plan for a nasty 2012, all options are on the table. Outside Spending Wars Reignite in Great Plains 

In North Dakota, 2012 attack ads are already hitting the airwaves. Outside Spending: The Final Tally 

The Left s American Crossroads? 

Democrats are gearing up for the 2012 shadow spending wars. Crashing the Covert Campaign Spending Spree 

What do secretive independent expenditure groups hate more than disclosure rules? When we show up at their doors with a camera rolling. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
