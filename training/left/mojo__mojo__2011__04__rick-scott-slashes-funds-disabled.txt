Continuing his assault on Florida's most vulnerable, Gov. Rick Scott issued an executive order on Thursday that immediately slashes money for the developmentally disabled. The cuts will reduce payments to group homes and social workers by 15 percent. The Orlando Sentinel reports : 

Florida Gov. Rick Scott ordered deep cuts Thursday to programs that serve tens of thousands of residents with Down syndrome,cerebral palsy, autism and other developmental disabilities [which] providers say could put them out of business and threaten their clients' safety.
 

lt's not like, 'Gee, does this mean I have to skip a vacation this year?' said Amy Van Bergen, executive director of the Down Syndrome Association of Central Florida. Potentially, these cuts have life and death implications for these people. 

 An estimated 30,000 Floridians with severe developmental disabilities receive services that help them live outside of nursinghomes typically with family or in small group homes. Aides help them eat, bathe, take medication and otherwise care for themselves.
 

But Scott's executive order is only the first of many cuts that could hurt the disabled. With the governor's full support, the Florida statehouse is currently considering a bill that would privatize Medicaid a proposal that would also turn health care for disabled beneficiaries in the program over to private managed care companies. 

The problem is that HMOs do not have the expertise in dealing with developmentally disabled patients, Debra Downs, executive director of the Florida Developmental Disabilities Council, warns in an interview. What's more, when you put [these services] into the private sector, there's going to be some money off the top for administrative costs, she adds, warning that HMOs could end up spending money on bureaucracy rather than services. 

So Florida's most vulnerable residents have gotten the short end of the stick as Florida trys to rein in its budget. And Republicans in Washington are following the exact same playbook.
