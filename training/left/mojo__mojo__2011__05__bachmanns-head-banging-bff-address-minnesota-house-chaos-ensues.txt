On Wednesday, we told you about the unlikely alliance between Minnesota Rep. Michele Bachmann, a possible GOP presidential candidate, and Bradlee Dean, a head-banging, tatooed, death * heavy metal drummer. (The relationship becomes a lot less confusing when you consider that Dean belives that gays are, by defintion, criminals, and should be prohibited from holding government jobs; that the average gay man will molest 117 people before they're found out ; and that he runs a ministry called You Can Run But You Cannot Hide, which travels to public schools to encourage students to find Christ). 

Today Dean outdid himself. With the Minnesota legislature in the middle of a heated debate over a proposed constitutional amendment to ban gay marriage in the state the opponents of which, Dean has pilloried on his radio show the House Republican Caucus invited the controversial hair-metal evangelist to deliver the opening prayer for Friday's session. 

How did it go? Well, the grand finale consisted of Dean alleging that the President of the United States is not a Christian. Via the St. Cloud Times : 

I end with this. I know this is a non-denominational prayer in this Chamber and it's not about the Baptists and it's not about the Catholics alone or the Lutherans or the Wesleyans. Or the Presbyterians the evangelicals or any other denomination but rather the head of the denomination and his name is Jesus. As every President up until 2008 has acknowledged. And we pray it. In Jesus' name. 

That shouldn't come as too much of a surprise: Dean recently explained on his radio show that there's no real difference between Obama and Osama bin Laden, and that the President's policies like those of his predecessors are part of a plot to bring about the New World Order. 

Dean's remarks didn't go over well: legislators had to summon the House chaplain to deliver a new prayer, and Democratic Rep. Terry Morrow immediately took to the floor to denounce the whole affair. From the Minnesota Independent : 

Today hope was crushed by the words of a single speaker, he said. Mr. Speaker, I do trust and hope that we understand the gravity and the severity of the prayer that has been given to the people within this chamber and out. 

I'm shaking right now because I'm mad, he concluded. This cannot happen again. 

Wow. According to WCCO's Patrick Kessler, Speaker Kurt Zellers, a Republican, has since publicly apologized, saying I denounce him, his actions and his words. (The caucus did not vet Dean's remarks). 

Update : The Independent has the video and a follow-up interview with Zellers. Yes, Dean really wore a track suit: 



*Thanks to the readers who pointed out that Dean's music is better classified as heavy metal, or even rap-core. We regret the error.
