Earlier today, the impossible seemed to happen when Rush Limbaugh suggested that President Barack Obama single-handedly devised the plan to successfully assassinate Osama Bin Laden. Though many in the media took Limbaugh seriously, he was actually being sarcastic . Here are 10 other ways that right-wing pundits and bloggers have tried to spin the Al Qaeda leader's killing into a joke, a trifle, or even a triumph of conservatism: 

1. American Spectator Senior Fellow Chris Horner: The death of Bin Laden is a blow to supporters of cap and trade. (In January, OBL expressed concern about climate change ) . 

















2. Human Events editor Jason Mattera: Remember how Michele Obama isn't supposed to be patriotic? 

















3. RedState blogger Streiff : Flaky liberals hate that bin Laden is dead. 

Presumably Code Pink will hold a Take Back the Night march some place to mourn his passing. 

Bonus: Obama's decision to use military force to take out Bin Laden exposes his ambivalence about military force. 

The death of bin Laden is more likely to give impetus to Obama s ambivalence about the concept of victory and his deep-seated hostility to the success of American military power and thereby give him the political cover he feels he needs to speed up troop withdrawals from those countries. 



4. WorldNetDaily contributor Mychal Massie: Maybe Obama deserves to be assassinated, too. 

















5. Washington Times editorial page editor Brett Decker: For self-centered Obama, everything is about him . 

He used the words I, me and my so many times that it was hard to count for such a quick message. 

6. Eric Bolling, host of Fox's Follow the Money : Bush, not Obama, really deserves the credit. No wait I forgot someone 

































7. Bryan Fischer , host of the American Family Association's Focal Poin t radio show: 

The death of Bin Laden is a major PR problem for President Obama. (Why? You have just invited the entire Islamic world to focus all of their vitriol, all of their hatred, all of their animosity directly on you! ) 





8. Atlas Shrugged contributor Pamela Geller : Her sources say that Al Qaeda is actually small peanuts compared to the terrorists who have already infiltrated the US government : 

Many of us already realize that the Muslim Brotherhood is the much more insidious threat than AQ. . .they are advising our senior leaders and have access to all levels of our government and national security apparatus. 

9 . Gateway Pundit contributor Jim Hoft: The discovery and killing of Bin Laden justify the use of torture. 





10. J. Michael Waller, on Andrew Breitbart's website, Big Peace : Show us the death certificate! 



The free world, particularly the United States, has a right to make sure that Osama Bin Laden is really dead.
