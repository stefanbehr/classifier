This post first appeared on the ProPublica website. 

More details, debates, and even doubts have continued to emerge about the US raid that killed Osama bin Laden this week. 

We've been tracking the coverage with our reading guide . We're also got a weekend wrap-up for the major threads of this evolving story. 



For the doubters (aka the Deathers) 

In an online message that surfaced today, Al Qaeda confirmed that its founder was killed and warned that his death would not be in vain: We will remain, God willing, a curse chasing the Americans and their agents, following them outside and inside their countries. US analysts have yet to verify the authenticity of the message. 

The White House has announced it won't be releasing photos of bin Laden's body, citing the images' potential to incite violence or to be used as propaganda. At least one watchdog group has put in filed Freedom of Information Act requests with the federal government and has said it's prepared to sue for the photos. 

Reuters, meanwhile, has bought several grisly photos of three of the other men who died in the raid at the compound. Beware: they're bloody. 

For those tracking the United States' changing story of the raid and questioning its legality 

The White House didn't offer any more updates on its narrative of the raid, though a piece in today's Washington Post hashes out some details on how bin Laden was shot: 

US officials provided new details on bin Laden's final moments, saying the al-Qaeda leader was first spotted by US forces in the doorway of his room on the compound's third floor. Bin Laden then turned and retreated into the room before being shot twice in the head and in the chest. US commandos later found an AK-47 and a pistol in the room. 

He was retreating, a move that was regarded as resistance, a US official briefed on the operation said. You don't know why he's retreating, what he's doing when he goes back in there. Is he getting a weapon? Does he have a [suicide] vest? 

The details are important for those still scrutinizing the mission's legality . US officials have maintained that the raid was legal, arguing that lethal force was authorized regardless of whether bin Laden was armed. 

The Post story also highlighted the role of the CIA, which had for months been monitoring the Abbottabad compound from a safe house in the city : 

The effort was so extensive and costly that the CIA went to Congress in December to secure authority to reallocate tens of millions of dollars within assorted agency budgets to fund it, US officials said. 

Most of that surveillance capability remained in place until the execution of the raid by US Navy SEALs shortly after 1 a.m. in Pakistan. The agency's safe house did not play a role in the raid and has since been shut down, in part because of concerns about the safety of CIA assets in the aftermath, but also because the agency's work was considered finished. 

The CIA is still going through the material taken from the compound. Early reports indicate that Al Qaeda was considering plans to attack the US by tampering with trains. 



For those still wondering about Pakistan 

As we noted yesterday, tensions between the US and Pakistan have grown since news of the raid first broke on Sunday night, with the initial congratulations giving way to sharp criticisms from both Pakistan's foreign ministry and army. Here's what we wrote yesterday: 

Comments from Pakistani agencies and officials have ranged from congratulations on a great victory to denunciation of a cold-blooded killing by the United States. 

The Pakistani Foreign Ministry criticized the US operation as an unauthorized unilateral action and warned the US and other countries against taking it as precedent. 

Pakistan's top army official also responded yesterday, calling the raid a violation of the sovereignty of Pakistan and warning that in the future such actions would warrant a review on the level of military/intelligence cooperation with the United States. 

Pakistan has continually denied knowledge of bin Laden's whereabouts but maintained that it helped provide intelligence that led to bin Laden. 

In an interview with NPR, former Pakistani President Pervez Musharraf acknowledged that bin Laden's presence could only lead to one of two conclusions Pakistan's intelligence agencies either were complicit or incompetent . I strongly believe in the later, Mushrraf said. I cannot imagine there was complicity. 



For the torture debaters 

For those who believe that torture is simply wrong, there's no debate here. 

But for others, there's a lot to still puzzle over. Many proponents of so-called enhanced interrogation techniques are still trying to make a case for why these techniques banned by the Obama administration but approved under Bush were useful and shouldn't be ruled out in the future. 

Several US military interrogators released a statement on Wednesday pushing back against claims that torture was necessary to obtain the intelligence leading to bin laden. From the Christian Science Monitor: 

We are concerned about the suggestion by some that the use of waterboarding and other enhanced interrogation techniques led US forces to Osama bin Laden's compound, reads the statement, signed by four former military and FBI interrogators. 

That hasn't stopped torture defenders from continuing to make the argument. In an opinion column published today, former US Attorney General Michael Mukasey argued that Khalid Sheikh Mohammed broke like a dam under the pressure of harsh interrogation techniques. Others have pointed out that KSM in fact misled interrogators when asked about bin Laden's courier, according to official accounts of the intelligence trail.
