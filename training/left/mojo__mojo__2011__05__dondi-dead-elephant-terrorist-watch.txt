The ongoing surveillance of environmental groups by state and federal governments under the rubric of rooting out terrorists (which I wrote about last week ) can have its comical side. This was the case with one dispatch uncovered by the Public Intelligence [PDF] site. 

Florida anti-terrorist watchdogs, operating in the interests of domestic security, issued Intelligence Report 6, August 2010, labeled SENSITIVE BUT UNCLASSIFIED. It begins as follows: 

This report is being created as an intelligence product for Region 5 of the Domestic Security Task Force. If you or your agency has any information or notice any trends that you would like included in this weekly report please contact the Central Florida Intelligence Exchange... 

The report begins with a short account of two mosques expanding. There don't appear to be any imminent dangers here, but unpredictable situations can always occur. 

Of more general interest, Florida intelligence professionals are keeping an eye out for terrorist disruptions following the death of a local zoo animal: 

[A]n Asian elephant named Dondi died unexpectedly at the age of 36 at Southwick Zoo outside of Boston, Massachusetts. Although Dondi lived in Boston over the summers, she preformed [sic] at Flea World in Sanford, Florida in the winters. The group In Defense of Animals (IDA) has filed a complaint with the USDA to urge an investigation into the death. 

An anti-terrorist analyst notes that this is of some interest because: ARFF has held numerous demonstrations at the Sanford Flea Market to protest on behalf of Dondi, whom they wanted to be retired and moved to a sanctuary. Currently there are no known protests surrounding the death of Dondi. 

The report then proceeds to the more serious matters at hand: 

During the week of 9 August 2010, the Greenpeace ship Arctic Sunrise will be heading to Gulf of Mexico for a three month expedition to document the true impacts of the BP Deepwater Disaster. 

Greenpeace feels that BP has devoted inadequate resources to the oil spill response, withheld information from the American public, and denied access to spill sites . The Arctic Sunrise will leave from Tampa, Florida and visit the Florida Keys and the Dry Tortugas prior to going to the oil spill site. During the expedition, they will be examining the effects of the spill as well as looking for oiled marine life. 

Analyst Notes: Although there are no known threats associated with this expedition at the time this report was created, aggressive tactics utilized by the Arctic Sunrise in the past may increase the likelihood of unforeseen incidents occurring. Since the beginning of the Gulf spill, Greenpeace have taken numerous actions against BP, including shutting down 40 BP stations throughout London, England in late July 2010. Members of the organization dropped off letters to each station and on their way out pulled a safety switch which cut off power to the station. They also covered BP signs with posters reading Closed: Moving Beyond Petroleum. 

The report ends with the following warnings and provisos: 

If you have or receive any information regarding a possible threat or have questions or comments please contact the Central Florida Intelligence Exchange (CFIX). 

SENSITIVE BUT UNCLASSIFIED 

PUBLIC AND MEDIA RELEASE IS NOT AUTHORIZED 

DISSEMINATION TO AUTHORIZED PERSONNEL ONLY 

NOTE: The accuracy of this information is based solely on the sources from which it was derived.
