Could Rep. Ron Paul of Texas ever be a true contender for the White House? 

To be sure, the conservative political landscape has shifted dramatically since Paul's quixotic bid for the 2008 GOP nomination was met by jeers from the party establishment, and the Ron Paul Revolution has minted a new generation of libertarian activists who've helped lay some of the organizational and ideological groundwork for the tea party movement. Time has come around to where people are agreeing with much of what I've been saying for 30 years, the Texas congressman said on Friday, as he launched his third White House attempt. The time is right. 

Yet despite Paul's growing cult following, many of his views are just a tad extreme for voters from either major party. To name just a few of these politically dicey positions, President Ron Paul would like to... 

1. Eviscerate Entitlements: Believes that Social Security, Medicare, and Medicaid are unconstitutional, and has compared the failure of federal courts to strike them down to the courts' failure to abolish slavery in the 19th century. 

2. Lay Off Half His Cabinet: Wants to abolish half of all federal agencies, including the departments of Energy, Education, Agriculture, Commerce, Health and Human Services, Homeland Security, and Labor. 

3. Enable State Extremism: Would let states set their own policies on abortion, gay marriage, prayer in school, and most other issues. 

4. Protect Sexual Predators' Privacy: Voted against requiring operators of wi-fi networks who discover the transmission of child porn and other forms online sex predation to report it to the government. 

5. Rescind the Bin Laden Raid: Instead of authorizing the Navy Seals to take him out, President Paul would have sought Pakistan's cooperation to arrest him. 

6. Simplify the Census: The questions posed by the Census Bureau's annual American Community Survey , which collects demographics data such as age, race, and income, are both ludicrous and insulting , Paul says. 

7. Let the Oldest Profession Be: Paul wants to legalize prostitution at the federal level. 

8. Legalize All Drugs: Including cocaine and heroin . 

9. Keep Monopolies Intact: Opposes federal antitrust legislation , calling it much more harmful than helpful. Thinks that monopolies can be controlled by protecting the concept of the voluntary contract. 

10. Lay Off Ben Bernanke: Would abolish the Federal Reserve and revert to use of currencies that are backed by hard assets such as gold . 

11. Stop Policing the Environment: Believes that climate change is no big deal and the Environmental Protection Agency is unnecessary . Most environmental problems can be addressed by enforcing private-property rights. Paul also thinks that interstate issues such as air pollution are best dealt with through compacts between states. 

12. Not Do Anything, but Still...: Would not have voted for the Civil Rights Act of 1964 because it was a massive violation of private property and contract, which are the bedrocks of a free society. 

13. Let Markets Care for the Disabled: The ADA should have never been passed, Paul says . The treatment of the handicapped should be determined by the free market. 

14. First, Do Harm: Wants to end birthright citizenship. Believes that emergency rooms should have the right to turn away illegal immigrants . 

15. Diss Mother Teresa: Voted against giving her the Congressional Gold Medal. Has argued that the medal, which costs $30,000, is too expensive .
