At a hearing today of the House Committee on Oversight Government Reform, chairman Darrell Issa (R-Calif.) is expected to attack a Presidential plan to require government contractors to disclose their contributions to political groups. The hearing is a bold move for Issa, who only months ago founded the House Transparency Caucus with the declaration that sunlight is indeed the best disinfectant. 

The disclosure rule at issue is really just a small-bore response to last year's sweeping Citizens United Supreme Court ruling, which opened the floodgates to corporate cash in elections . It focuses exclusively on federal contractors because they presumably have more incentive than other private companies to bribe and influence politicians. So why is Issa throwing a fit? 

The answer, as with most things in politics, probably involves money. The union-backed group Chamber Watch has tallied up how much dark money went last year to support Republicans on the Oversight Committee and the Small Business Committee, which is co-hosting the hearing. The results are striking: 

Source: US Chamber Watch Evidence suggests that a large part of this dark money comes from companies that feed at the public trough. Board members of just one of those dark money groups, the US Chamber of Commerce, earned a collective $44 billion from federal contracts last year, according to Chamber Watch. Only 18 of the Chamber's 53 board members didn't land contracts with the federal government. 

Print Email Tweet Dem Campaign Ad: I Have Issues! I m Korean. 10 CEOs Who Got Rich By Squeezing Workers Josh Harkinson 

Reporter 

Josh Harkinson is a staff reporter at Mother Jones . For more of his stories, click here . Email him with tips at jharkinson (at) motherjones (dot) com. To follow him on Twitter, click here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Issa Stacks the Deck Against Obama s Dark Money Directive 

Congress' top GOP watchdog shuts out a leading campaign finance reformer from a crucial hearing. The Man Behind Citizens United Is Just Getting Started 

Meet the lawyer who could turn our elections upside down. ChamberLeaks: What Did The Chamber Know? 

Leaked emails suggest the powerful business lobby knew more about a devious plan to entrap its critics then it's saying. Darrell Issa s Agenda 

Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
