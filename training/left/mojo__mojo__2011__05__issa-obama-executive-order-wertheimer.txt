On Capitol Hill today, a pair of House committees, including the one led by the pugnacious Rep. Darrell Issa (R-Calif.) , will debate President Obama's plan to issue an executive order that would force government contractors to disclose political contributions. The hearing's title telegraphs the GOP's stance on the order: Politicizing Procurement: Would President Obama's Proposal Curb Free Speech and Hurt Small Business? From the hearing's witness list , it's clear that Issa, who chairs the House oversight and government reform committee, has stacked the deck against the Obama administration. He allowed Democrats, who mostly support Obama's order, to invite only one witness to testify; Issa and House Republicans, meanwhile, invited five witnesses who all oppose the executive order. (A member of the Obama administration will also testify.) 

Fred Wertheimer, a long-time campaign finance advocate who runs Democracy 21, says he was asked by House Democrats to testify, but his testimony was blocked by Issa's office. Wertheimer has been testifying on campaign finance issues since 1973, and he can't remember a hearing as one-sided as this one. As for opponents' claims that Obama's order curtails the free speech of government contractors, he calls that argument absurd. In his written testimony, which will be submitted into the Congressional record, Wertheimer writes: 

The Supreme Court clearly and unequivocally found in Citizens United that campaign finance disclosure laws were constitutional and necessary for the new campaign finance activities permitted by the Court s decision. The draft Executive Order would provide such information to citizens and taxpayers whose funds are being spent on government contracts and who have a basic right to know this information. 

Obama's executive order comes after Congress' repeated failure to pass legislation bolstering disclosure rules and transparency in campaign spending, and in the wake of the Supreme Court's Citizens United decision, which opened the floodgates to corporation donations in American elections. The order has the support of good government groups and campaign finance reformers, but is opposed by GOPers and a few Democrats, including House Minority Whip Steny Hoyer (D-Md.) . Hoyer's suburban district , southeast of Washington, DC, is home to multiple government contractors. 

The Obama administration had initially declined Issa's invitation to testify the hearing, saying the drafting process on the order wasn't complete. But after Issa threatened to subpoena the White House, the adminstration offered up Daniel Gordon, the administrator for federal procurement policy at the Office of Management and Budget. 

Read Fred Wertheimer's complete testimony here: 

Wertheimer Testimony on Obama Administration Executive Order 5 10 2011 

Print Email Tweet What Mitt Romney Doesn t Need to Say About Health Care Dem Campaign Ad: I Have Issues! I m Korean. Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Darrell Issa: Enter Stage Right 

GOP pit bull Darrell Issa is modeling himself after the Dems' fiercest watchdog. Issa Cools to Global Warming Investigation 

Issa s Regulatory Rehash 

Issa s Glaring Omission: War Spending 

Will the House's new watchdog sniff out waste in the defense budget? Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
