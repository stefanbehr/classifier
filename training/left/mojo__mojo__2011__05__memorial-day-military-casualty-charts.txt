How long should you spend commemorating Memorial Day? It can be accomplished in just 60 seconds if you follow a 2000 presidential memo from Bill Clinton that encouraged Americans to pause for one minute at 3:00 p.m. (local time) on Memorial Day, to remember and reflect on the sacrifices made by so many to provide freedom for all. That comes out to 0.0000446 seconds of reflection for each of the approximately 1.3 million Americans who have died in uniform since the earliest days of the republic ( according to Wikipedia ). 

If you have some more time, check out these charts about those who made the ultimate sacrifice. Let's start with a quick review of the biggest conflicts in American history: 



Of course, not all Americans who gave all were participants in such memorable campaigns. This list of historic Marine and Navy casualties reminds us that hundreds perished in all but forgotten engagements with Chinese bandits, Japanese feudal warlords, and even illegal booze makers in Brooklyn. And pirates: 



Being a soldier has always been a dangerous job, but fighting on the frontlines has gotten statistically safer. In the current wars in Iraq and Afghanistan, fewer than 10 percent of all casualties are deaths on the battlefield. 



A major reason why more soldiers are surviving modern combat is the vast improvement in battlefield medicine (germ theory, antibiotics, medevacs, etc.). If you were wounded in the Civil War, your chances of survival were worse than a coin flip. Compare that with Iraq and Afghanistan, where a wounded soldier's chance of survival are about 85 percent. 



Though still relatively low by historical standards, casualty rates are on the rise in Afghanistan as more troops have surged into the country. Meanwhile, the casualty rates have dropped significantly in Iraq as more troops have left (often for Afghanistan). 



Not all wartime deaths occur in combat. A look at the top causes of death for soldiers in Iraq and Afghanistan shows that while IEDs and other weapons have taken the heaviest toll, more mundane incidents such as car crashes are also a risk. 



And with that in mind, stay safe out there on this Memorial Day. 

Sources Major wars: Dept . of Defense ( PDF ) Pirates: US Navy Naval History Heritage Command Combat deaths: Dept. of Defense ( PDF , PDF , PDF , PDF ) Survival rates: Congressional Resarch Service ( PDF ), Dept. of Defense ( PDF , PDF , PDF ) Iraq/Afghanistan: Congressional Resarch Service ( PDF ), Dept. of Defense ( PDF , PDF , PDF ) Causes of death: Dept. of Defense ( PDF )
