Today, a court in Chicago will hear the opening arguments in the trial of Tahawwu Rana, a Pakistani-Canadian doctor charged with providing material support to the terrorists who planned and executed the 2008 Mumbai terrorist attacks . The attacks resulted in the deaths of 166 people, including six Americans. The prosecution's star witness: Pakistani-American businessman and former DEA informant David Headley, Rana's childhood friend and alleged accomplice in the attacks. The prosecution has accused Rana of allowing Headley to use his immigration consulting firm as a cover overseas. 

Both men were arrested in October of 2009 in connection with the Mumbai attack. Last year, Headley pleaded guilty to charges of conspiring to bomb targets in Mumbai, providing material support to the terrorist group Lashkar-e-Taiba, and aiding and abetting the murder of US citizens in the Mumbai attacks. In his confession, he painted a blistering picture of the role of Pakistani's intelligence service, the Inter-Services Intelligence Directorate (ISI), in helping Lashkar-e-Taiba carry out the attacks. Rana's defense team is expected to argue that the conniving, charismatic Headley misled their client about the true, murderous intent of the Mumbai operation. 

ProPublica's Sebastian Rotella reports that both sides will undoubtedly explore the numerous alleged ties between the ISI and Lashkar which should make Pakistani security officials very nervous. Last month, federal prosecutors indicted an ISI officer known only as Major Iqbal for the murders of the six Americans in Mumbai (whose deaths form the basis for the US trial). But there's another security bureaucracy that could get some unwanted attention during the Rana trial, one that's much closer to home: the DEA. From Rotella: 

After a 1997 arrest for heroin smuggling, Headley became a prized DEA informant who targeted Pakistani traffickers. Immediately after the Sept. 11 attacks, the DEA directed him to collect intelligence on terrorists as well as drugs. In December 2001, the U.S. government ended his probation three years early and rushed him to Pakistan, where he began training in Lashkar terror camps weeks later, according to court documents, officials and his associates. 

Some federal officials say he remained an informant at least three more years, but the DEA disagrees. 

David Headley was sent to Pakistan for approximately three weeks to further a drug investigation in 1998, said a DEA official familiar with his work as an informant. The DEA official declined to comment on Headley's mission in late 2001 but said: He was deactivated in early 2002. 

That assertion only deepens the contradictions and mysteries about Headley's missions overseas. Between 2001 and 2008, federal authorities were warned six times by his wives and associates that he was involved in terrorism. None of the resulting inquiries yielded anything. The FBI and CIA say he never worked for them. 



Headley's long, duplicitous history of playing one side against the other should have raised serious flags for his handlers at the DEA. Ugly revelations about the DEA's involvement with Headley could raise some damaging questions about the feds' use of informants.
