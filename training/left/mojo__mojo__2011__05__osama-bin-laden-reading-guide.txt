Elsewhere in Mother Jones, David Corn analyzes the political gamble Obama took in authorizing the attack, Dave Gilson breaks down the numbers behind the most expensive manhunt in history, and Josh Harkinson rounds up ten ways the right is spinning bin Laden's death. Adam Weinstein reports on the reactions of bin Laden's supporters as well as active-duty soldiers, Stephanie Mencimer checks in on the tea party, and Mike Mechanic pulls together a slideshow of major daily front pages . 

Pete Marovich/Zuma This post first appeared on the ProPublica website . 



The death of Osama bin Laden has sent news organizations scrambling for details on how it happened, where it happened, and what it all means. We've rounded up some of the best coverage, being careful to note what's been said, what's already being disputed, and what still remains to be seen. 



How they found the most wanted man in the world: 

The New York Times has a vivid account of the hunt for bin Laden in the weeks leading up to the strike, with dialogue straight from the situation room as the operation unfolded. As for the specific trail of intelligence, the Associated Press traces how detainees in both the CIA's secret network of prisons and in Guantanamo provided clues about the trusted courier who ultimately led the United States to bin Laden's hideaway. The AP cites former officials asserting that Khalid Sheikh Mohammed, the 9/11 mastermind, was not being waterboarded while discussing the courier, though that still leaves unanswered what interrogation methods were used on the other, tip-giving detainees many of whom haven't been identified. 

But here's the interrogation file of one detainee who may have proved useful: The file of Abu al-Libi contained an early clue as to the whereabouts of a bin Laden courier in Abbottabad. 



What we actually know about the operation and what's still fuzzy: 

Most accounts of the bin Laden operation at this point cite background briefings from the White House. Those transcripts are interesting for both the details they provide and the details that officials skirt around. Here's yesterday's and today's . 

Many of the blow-by-blows of the bin Laden operation are still fairly sketchy, and Obama administration officials already appear to be backing away from a few of the earlier descriptions of the circumstances surrounding bin Laden's death. For instance, early claims that bin Laden was armed at the time of his death and had used his wife as a human shield have since been contradicted by officials, Politico reported. 

Given this, Slate's Jack Shafer has a must-read , pointing out several instances of vague sourcing and inconsistencies in some of the coverage of the bin Laden story. 

Of course, some have taken that skepticism a step further and veered into conspiracy theories, seizing on the sea burial and the timing of the President's announcement as suspicious. (Slate has more on why the sea burial is unusual .) The administration has said it's considering releasing the photo of bin Laden's body or videos of the raid and the burial to put these suspicions to rest. 

The Joint Special Operations Command, whose elite team of Navy Seals executed the operation , costs the country more than $1 billion annually, according to National Journal. Despite some of its personnel having been involved in abuse of prisoners and rendition, JSOC has operated without much scrutiny since 9/11 read the piece for more helpful context . 



About the town where he was found, Abbottabad: 

Abbottabad is north of Islamabad, less than mile away from the Pakistani military academy the Pakistani equivalent of West Point , as some have noted. ProPublica's Scott Klein mapped it out . 

Some useful people to follow on the ground in Abbottabad are CNN's Nic Robertson (@ NicRobertson ), TIME journalist Omar Waraich (@ Omar Waraich , and IT consultant Sohaib Athar (@ ReallyVirtual ), the man who unwittingly live-Tweeted the US operation as it unfolded and has since been tweeting photos and observations from the city. 

The New Yorker's Steve Coll notes that the location raises serious questions about what Pakistan knew about the whereabouts of the United States' most wanted man: 

It stretches credulity to think that a mansion of that scale could have been built and occupied by bin Laden for six years without its coming to the attention of anyone in the Pakistani Army. 

The initial circumstantial evidence suggests that the opposite is more likely that bin Laden was effectively being housed under Pakistani state control. Pakistan will deny this, it seems safe to predict, and perhaps no convincing evidence will ever surface to prove the case. 



What Pakistan knew: 

The Obama administration has paid lip service to its robust counterterrorism partnership with Pakistan, but it also said that it planned and executed the operation without Pakistan's prior knowledge. Pakistan's ministry of foreign affairs has said that US helicopters entered Pakistani airspace making use of blind spots in the radar coverage . Pakistan has also maintained that it had no idea that bin Laden was in Abbottabad. 

Foreign Policy has a roundup of Pakistani officials' past denials that bin Laden could be hiding in their country. An unnamed Pakistani intelligence official told the BBC that they're embarrassed that they weren't able to find bin Laden, and that, though they raided the Abbottabad compound while it was in construction in 2003, it was not on our radar since then. 

In a White House press briefing yesterday afternoon, Obama's chief counterterrorism adviser, John Brennan, said that bin Laden must have had some Pakistani support , though he refused to speculate who might have given it and how high it went. 

Relations between the United States and Pakistan have been complicated since 9/11 the recently leaked GITMO files showed that US officials have been suspicious of Pakistan for years. Last week, the chairman of the Joint Chiefs of Staff publicly accused the Pakistani military of supporting the Haqqani network, a wing of the Taliban with close ties to al-Qaida. We've covered how Pakistan's intelligence service has been a frenemy in past cases. Last year, we laid out the evidence that officers in Pakistan's powerful intelligence service collaborated on the Mumbai terrorist attacks . 

This mission also wasn't the first time the United States has carried out a covert raid in Pakistan without alerting local officials. We have a look at past US military operations in Pakistan in recent years and the tensions that have grown out of them. 

As details continue to emerge, some good reporters and experts to follow on Twitter include our own national security reporter Dafna Linzer (@ dafnalinzer ), TIME's national security correspondent Mark Thompson (@ mthompsontime ) and National Journal's Marc Ambinder (@ marcambinder ), who authored one of the most detailed early stories on the team that killed bin Laden. 



Background on bin Laden and his followers: 

The New York Times obituary for bin Laden is six pages a detailed retrospective on who he was and how he founded al-Qaida. 

Check out Frontline's bin Laden files seems there's a lot to dig through , including a chronology of his political life and a brief biography . Frontline's also airing a show tonight about a band of bin Laden loyalists in Afghanistan, CIA kill raids in Pakistan, and new evidence of secret Pakistani support for elements of the Taliban. 

The New Yorker's 2005 piece on bin Laden has several interviews with former schoolmates about his childhood and his radicalization . Over the years, several pieces have been written about the US trail on Osama going cold, including Jane Mayer's 2003 piece in the New Yorker and the Washington Post's 2006 piece . Peter Bergen, writing for the New Republic in 2009, gives an account of a failed attempt by the US military to capture bin Laden at Tora Bora, a mountainous region in Afghanistan near the Pakistani border crucial for the years of war it could have spared the United States had it succeeded. 

The Daily Beast has a few more suggested long reads on the subject. 



So, what happens next? 

While national security officials are saying bin Laden's death and the raid itself have been important blows to al-Qaida, the consensus seems to be that the threat isn't over. The New Yorker's Steve Coll writes that while this is the first time al-Qaida will have to face a change in leadership, it's also a decentralized network essentially an ideology that can't be dismantled by just picking off the leaders. 

Foreign Policy has a useful guide to all the major arguments being made about the implications of bin Laden's death; the Atlantic has a good selection of post-bin Laden analysis and predictions ; and the New York Times has a Room for Debate segment on what comes next in the war on terror . 

Al Jazeera has a piece with reactions from Afghan officials and residents , some of whom fear for the long-term stability of the region if NATO and US forces leave. The Guardian has a guide to bin Laden's inner circle , a potential roadmap of who's next in line. 

For following all the developments, Al Jazeera and the New York Times have live blogs going, and Foreign Policy's AfPak Channel is continually rolling out analysis .
