After nearly two years of relentlessly bashing President Obama, the tea party movement has been strangely quiet in the wake of the killing of Osama bin Laden. It seems that Obama's powerful show of military force has done what none of his other policy moves have been able to do, which is shut them up, however briefly. And not only are they not taking to the airwaves to bash him, some are even grudgingly admitting respect for his administration s success. 

Robin Stublen , a tea party organizer in Florida who s no fan of Obama's, says, I think it's wonderful. He did exactly what a president s supposed to do. 

Stublen says that many of his fellow activists feel the same way, and that most of the chatter he's hearing from grassroots conservatives is pretty positive. We realize a bad guy s been killed, he says. The nearly overnight change in the tea party's focus was apparent Sunday night at the White House, where spontaneous celebrations broke out after the news of Bin Laden's death spread. Among the many Obama campaign signs were enough Gadsden flags to give the celebration the look of a tea party rally. 

Kellen Giuda is the founder of the NYC Tea Party and is already working to help defeat Obama in 2012 through a new PAC . Yet he was among the tea partiers at the White House , cheering the death of Bin Laden. He later posted online photos and video of the scene, which included the Don't Tread on Me flags so ubiquitous at tea party rallies. He wrote: 

Last night I, my girlfriend and a friend went down to the White House to celebrate the death of Osama Bin Laden. Being a Tea Party organizer I was happy to see some Gadsden flags and didn't care at all when I saw some Obama campaign posters. 98% of the celebration was non-partisan and it was wonderful. 

It was crazy with people climbing light poles, songs (someone brought a drum set), singing our national anthem, people climbing in all the trees right outside the White House, chants of USA, USA, USA, and just a great celebration with Americans for justice and freedom. 

Even the cantankerous Judson Phillips, head of Tea Party Nation, was briefly forced to acknowledge that the Obama administration had sent Bin Laden to Hell. Even so, like other tea partiers, he was reluctant to give Obama much credit for the kill, writing: 

Obama is taking credit for this. He did give the order. Did he really have a choice? If word leaked out that he had solid intelligence on where Bin Laden was and did not act, it would have killed any chance he had at reelection. 

For much of Monday morning, there was serious radio silence from one of the most outspoken tea party groups even as the Internet was ablaze with the news about Bin Laden. The website for Tea Party Patriots, one of the largest tea party umbrella groups in the country, was still focused on the debt ceiling and $4 gasoline. Eventually, national coordinator Mark Meckler commented on the big news, telling National Journal that Obama didn t deserve any recognition for the military operation in Pakistan. Taking such credit would be an insult to the courageous men and women in our armed forces who voluntarily put themselves in harm's way, he said. Any credit given is due to them. 

But more the more common sentiment was expressed by a commenter on the Tea Party Patriots website who wrote, Obamma [sic] killed Osama bin Laden - pretty good for a Kenyan Muslim Communist!!! 

Still, as the euphoria over the initial news wears off, the tea partiers will no doubt find more reasons to be critical of the administration. Within hours of the late-night news, some of them were already starting the cries of show me the body, after learning that bin Laden s body had been buried at sea a sentiment fueled by Andrew Breitbart . 

Stublen thinks this bit of conspiracy theorism about Bin Laden is on the margins of the movement. You ll have to really look to find some loons to find someone who really disagrees with what Obama did or doesn't believe it really happened, he says. Stublen recognizes, though, that pitching his ass out there in the ocean is going to create some lingering suspicions about whether Bin Laden is really dead that it could be a problem going forward. I hope they got a lot of pictures. That s the only way we re going to convince people, Stublen says. They re going to have to release the pictures.
