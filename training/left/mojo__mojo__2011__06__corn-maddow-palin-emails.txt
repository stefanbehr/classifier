David Corn appeared on MSNBC's The Rachel Maddow Show to discuss the complicated history of the Palin emails and some of the items of interest the Mother Jones team has uncovered so far. Click here to search through the highlights of the Palin email database. 



David Corn is Mother Jones ' Washington bureau chief. For more of his stories, click here . He's also on Twitter and Facebook . Get David Corn's RSS feed .
