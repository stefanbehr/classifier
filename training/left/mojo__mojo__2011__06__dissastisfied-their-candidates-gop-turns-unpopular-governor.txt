It's a matter of public record that, at least at this point in the campaign, Republican primary voters really don't like their 2012 options. Hence the constant pining for Indiana Gov. Mitch Daniels (not running), former Florida Gov. Jeb Bush (not running) even former Supreme Allied Commander and two-term President Dwight D. Eisenhower (deceased). Now, the new hope for discontented GOPers is New Jersey Gov. Chris Christie, a brash former US Attorney who's become a minor deity on the right on account of his contentious exchanges with (usually) public school teachers. Christie has said he's not running, but continues to hold the kind of meetings you'd hold if you were actually thinking of running. On Tuesday, he met with a delegation of influential Iowa Republicans in Princeton. Per the Des Moines Register : 

It's too early to say the Iowa GOP mission to draft in-your-face New Jersey Gov. Chris Christie to run for president was unsuccessful, two team members said Wednesday. 

Although Christie didn't promise to enter the race during the dinner with the seven Iowa Republicans on Tuesday night, he never flatly declared he wouldn't, said Gary Kirke, a business entrepreneur and an organizer of the recruitment trip. 

Consider this: Christie had 13 of his people at the table, all trusted advisers, said Michael Richards, a West Des Moines Republican who also went on the 9 -hour trip. 

Of course, as my colleague Andy Kroll has noted , Iowa Republicans are pretty much the only folks who actually seem to like Chris Christie, whose approval ratings in New Jersey have plummeted in the last 12 months. Hey, there's always Rick Perry .
