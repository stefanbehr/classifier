Rudy Giuliani , the former New York City mayor, isn't letting his disastrous presidential bid in 2008 prevent him from giving the GOP's 2012 presidential frontrunner Mitt Romney a serious tongue-lashing. 

In a recent interview with the New Hampshire Union Leader , Giuliani singled out Mitt Romney , who officially unveiled his presidential campaign last week, and bashed the former Massachusetts governor for his 2006 health care reform plan that brought universal health care coverage to the Bay State. Giuliani described RomneyCare and ObamaCare loathed by the GOP as exactly the same, and accused Romney of telling us something that just isn't correct: that 'RomneyCare' and 'ObamaCare' are significantly different. 

Giuliani went on to tell the Union Leader that Romney has done a dismal job of distancing himself from RomneyCare, and that the best way for Mitt Romney to deal with it is to admit it's true and to say that it's a terrible mistake. (In a May speech in Michigan , Romney defended his health care reform plan, calling it a state solution to a state problem. ) 

As for Giuliani's own presidential aspirations in 2012, the Union Leader reports, 

While Giuliani has been making frequent visits to New Hampshire and speaking to small groups, he said he does not plan to make a decision on whether to make a second bid for President until late summer. He finished fourth in the 2008 New Hampshire primary; Romney finished second behind John McCain. 

Giuliani said he will first determine whether he has the ability to put together grassroots organizations in New Hampshire and elsewhere and decide whether you have a good chance of winning the nomination and whether you have the best chance of beating Barack Obama, which is the biggest question. 

Giuliani declined an invitation to the June 13 presidential debate co-sponsored by the New Hampshire Union Leader , WMUR, and CNN, saying, I won't debate until and unless I become a candidate.
