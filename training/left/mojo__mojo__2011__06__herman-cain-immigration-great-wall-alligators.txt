The longer Herman Cain is on the campaign trail, the more bizarre and error-filled his pronouncements become. On the day he officially announced his candidacy in May, for instance, Cain made a huge blunder as he pounded home the importance of reading the US Constitution: 

We don't need to rewrite the Constitution of the United States of America; we need to reread the Constitution, and enforce the Constitution... I know that there are some people that are not going to do that, so for the benefit of those who are not going to read it because they don't want us to go by the Constitution, there s a little section in there that talks about life, liberty and the pursuit of happiness.' [emphasis mine] 

That, of course, is the opening to the Declaration of Independence, not the Constitution. Not an ideal roll-out speech. 

This week, Cain, the former head of Godfather's Pizza, pledged that, if elected president, he would only sign small bills . Think really small. Like three pages or shorter. Don't try to pass a 2,700 page bill even they didn't read it! he said, referring to the Democrats' health care bill. You and I didn't have time to read it. We're too busy trying to live send our kids to school. That's why I am only going to allow small bills three pages. You'll have time to read that one over the dinner table. 

But that's not as bizarre as Cain's latest zinger. In Iowa, as ThinkProgress reports , Cain suggested he would, as president, build a huge fence along the US-Mexico border as part of his plan to clamp down on illegal immigration. And not just any big fence; a fence on par with the Great Wall of China, the mother of all barriers. And in case that newly erected Great Wall of America somehow didn't keep out border-crossers, Cain's alligator-filled moat would do the trick. Yes, you read that correctly. Here's the video: 



Transcript: I just got back from China. Ever heard of the Great Wall of China? It looks pretty sturdy. And that sucker is real high. I think we can build one if we want to! We have put a man on the moon, we can build a fence! Now, my fence might be part Great Wall and part electrical technology...It will be a twenty foot wall, barbed wire, electrified on the top, and on this side of the fence, I ll have that moat that President Obama talked about. And I would put those alligators in that moat! 

Herman Cain may be climbing in the polls, but he's not doing himself any favors on the campaign trail with ideas like these. If anything, he seems to be actively undermining his gains in public approval. Alligator-filled moats? And keep in mind, the gaffes mentioned above happened in only the past month. The Iowa caucus is nearly seven months away. Who knows what rhetorical gems and perplexing proclamations he'll think of until then?
