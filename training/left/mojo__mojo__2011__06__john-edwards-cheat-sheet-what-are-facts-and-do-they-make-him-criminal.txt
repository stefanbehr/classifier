This post first appeared on the ProPublica website. 

First, a sex scandal, followed by a messy cover-up and an even messier fessing-up. The sequence has become all but routine in Washington. 

But the criminal case against two-time Democratic presidential candidate John Edwards is anything but run-of-the-mill. While campaign finance experts may not agree on whether the charges are merited, they seem to uniformly acknowledge that the charges against Edwards are unprecedented. Here's a quick look at why, drawing from the prosecution's indictment, the defense's expert witnesses and what's been reported. 

What are the charges against Edwards, exactly? The government lays out its case in a federal grand jury indictment, which you can read in its entirety . Essentially, Edwards is charged with violating campaign-finance law for accepting large sums of money from two wealthy supporters. The money went to concealing Edwards' mistress, Rielle Hunter, from his wife and from the American electorate, the government alleges. 

At issue here is whether the nearly $1 million in funds from longtime Edwards supporters Rachel Bunny Mellon and now-deceased Fred Baron were in fact campaign contributions. If they were, as the government alleges, the donations would violate both disclosure laws and contribution limits. The government also has to prove that Edwards knew about the payments. 

Edwards pleaded not guilty. He said he has personal regrets but never broke the law. 



So what constitutes a campaign contribution? By law, a campaign contribution is any gift, subscription, loan, advance, or deposit of money or anything of value made by any person for the purpose of influencing any election for Federal office. 

The indictment explains it this way : Anything of value provided for the purpose of influencing the presidential election, including (a) contributions to a candidate and his/her campaign; (b) expenditures made in cooperation, consultation, or concert, with, or at the request of suggestion of, a candidate or his/her campaign; and (c) payments for personal expenses of a candidate unless they would have been made irrespective of the candidacy. 

Ah, so the question is whether the money was meant to influence the campaign, right? Yes, and we'll see what the court decides. But it almost goes without saying that a sex scandal and lovechild splashed in the headlines would have hurt if not derailed Edwards' campaign. 

Edwards' legal team has reportedly countered that the money was merely intended to hide the affair from Edwards' cancer-stricken wife, Elizabeth , and it had nothing to do with his protecting his campaign. 

Prosecutors evidently aren't buying that the payments were just to protect Edwards' marriage. In the first paragraph of the 19-page indictment, prosecutors argued that Edwards' public image as a devoted family man was a centerpiece of his candidacy, and that cultivating and publicizing that image was part of the campaign's communication strategy for electoral success. 

Also worth noting: Edwards once told ABC News that he had spoken with Elizabeth about his infidelity in 2006 , and yet the hush-money was paid out in 2007 and 2008. 

The government's case also leans on a 2000 advisory opinion from the Federal Election Commission in which a donor tried to give $10,000 in funds to candidates on the condition that the money was to be used solely for personal expenses , and not for campaign purposes, according to the News Observer. The donor's reasoning was that he wanted to express deep appreciation to the candidate for giving up private sector opportunities to pursue public service. The FEC said no, ruling that personal donations are only permissible if they would have been made irrespective of the candidacy. 



Is there specific evidence tying the donations to Edwards' candidacy? In the indictment, prosecutors point to a note between donor Rachel Mellon and Edwards' aide Andrew Young. Around that time, Edwards and Young had been discussing individuals who could support Edwards' mistress, who was pregnant with his child, according to the indictment. Young apparently gave Mellon a call. Here's what she told Young, emphasis ours: The timing of your telephone call on Friday was witchy. I was sitting alone in a grim mood furious that the press attacked Senator Edwards on the price of a haircut. But it inspired me from now on, all haircuts, etc., that are necessary and important for his campaign please send the bills to me. ... It is a way to help our friend without government restrictions . 

Mellon had by that time already contributed the most she could legally contribute to his campaign, but proceeded to write $725,000 in personal checks that ultimately went to Hunter's living and medical expenses, the indictment alleged. The memo lines of the checks indicated they were for furniture items chairs, antique Charleston table, book case. (Mellon's legal team told the New York Times that the money was a personal gift , and she didn't know how Edwards used it.) 

Edwards also accepted more than $200,000 from Baron, his former campaign finance chairman, which went to Hunter's travel and accommodations. (Some of the facts here are contradictory: After news of the Edwards-Hunter affair broke, Baron said he sent money to keep Hunter out of the media but he did so without Edwards' knowledge . Along with denying paternity of Hunter's child, Edwards said publicly in 2008 that had no knowledge of any money being paid. However, AP reported yesterday that prosecutors have notes between Edwards and a campaign writer that show Edwards acknowledging he knew about Baron's payments though he said he didn't ask for them.) 

Neither Mellon nor Baron are named in the indictment, but are listed as Person C and Person D, respectively. 



So, why are legal experts skeptical of the government's case? First, nobody can recall a similar case. And the lack of a clearly applicable legal precedent makes it risky. 

Plus, the standard of proof for a criminal case is higher than for a civil case. U.C. Irvine law professor and campaign finance expert Richard Hasen, who wrote a good piece for Slate [13] on the Edwards ordeal, wrote on his election law blog on Monday: The law is murky and the facts are murky, and it is going to be very hard to prove in a criminal prosecution beyond a reasonable doubt that Edwards willfully violated clear law. 

Both sides have sought experts to bolster their interpretations of the law. Politico reported that the prosecution has been seeking out former FEC commissioners to testify and has had at least one decline because the case seems somewhat of a stretch. 

The Edwards legal team has retained two expert witnesses. One, former FEC chairman Scott Thomas, said it was his view that the payments would not be considered to be either campaign contributions or campaign expenditures according to campaign finance law, and that the government's understanding of the law was incorrect. A criminal prosecution of a candidate on these facts, Thomas wrote , would be outside anything I would expect after decades of experience with the campaign finance laws. 



What's at stake for Edwards and for the Justice Department? If found guilty of the government's charges, the already-disgraced Edwards would lose his law license and could face a maximum of five years in prison and a maximum $250,000 fine on each of the indictment's six counts . 

For the government, the case could mean a step toward redemption or yet another high-profile failure for the same Justice Department unit that in 2008 botched the politically charged prosecution of the late Alaska Sen. Ted Stevens. 

Print Email Tweet Scott Walker Ditches Symbolic Painting of Homeless, Low-Income Kids Immigration Courts: Still Backlogged Despite New Judges If You Liked This, You Might Also Like... 

Top 11 Political Sex Scandal Apologies Other Than Weinergate [VIDEO] 

Your guide to recent mea culpas. John Edwards Hush Money Headache 

The latest chapter in the downfall of John Edwards. Corn on Hardball: Did Edwards Cover-Up Help Obama? 

Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
