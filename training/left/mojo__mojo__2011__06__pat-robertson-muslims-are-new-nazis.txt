Televangelist Pat Robertson isn't the powerful political force he once was, but as the founder of the Christian Broadcasting Network, he's still an influential voice on the Christian Right. Yesterday, on his television show, The 700 Club , Robertson delivered a warning to a weary nation: Muslims are the new Nazis : 

Robertson: I was thinking, you know, if you oppose Muslims, what is said? Well, you're a bigot, right? Terrible bigotry. I wonder what were people who opposed the Nazis. Were they bigots? 

Co-host: Well, in that day I think they were looked down upon and frowned upon. 

Robertson: Why can't we speak out against an institution that is intent on dominating us and imposing Sharia law and making us all part of a universal caliphate? That's the goal of some of these people. Why is that bigoted? Why is it bigoted to resist Adolf Hitler and the Nazis and to say we don't want to live under Nazi Germany? 

Not to nitpick here, but people who opposed the Nazis were not looked down upon and frowned upon as bigots. This was a few decades ago, so it's understandably a little obscure, but the United States actually went to war with Nazi Germany. There was a movie about it and everything. 

As you'd probably guess, this is hardly the first time Robertson has compared a large and diverse group of people to Nazis: 



Here's Robertson in 1995 : 

Since the advent of the head of the Justice Department, Janet Reno, things have gotten out of control. Something is going on that is just very unwholesome in this nation....this is a shocking abuse of federal power. It is reminiscent of the Nazis. Something has got to be done. 

And in 1993: 

Just like what Nazi Germany did to the Jews, so liberal America is now doing to the evangelical Christians. It's no different. It is the same thing. It is happening all over again. It is the Democratic Congress, the liberal-based media and the homosexuals who want to destroy the Christians. 

Also in 1993 : 

When lawlessness is abroad in the land, the same thing will happen here that happened in Nazi Germany. Many of those people involved in Adolph Hitler were Satanists. Many of them were homosexuals. The two things seem to go together. 

And in 1990 : 

[T]he liberal media and the screenwriters...are the spiritual descendants of Goebbels and the Nazis. They're not just talking about kikes and Jews; they're not talking about, quote, 'n-----s.' They're talking about Christians now. 

And in 1984 : 

The state is steadily attempting to do something that a few states other than the Soviets and the Nazis have attempted to do, namely, to take the children away from the parents and educate them in a philosophy that is amoral, anti-Christian and humanistic and to show them a collectivist philosophy that will ultimately lead to Marxism, socialism, and a communistic type of ideology. 



So as you can see, Muslims are the new gays are the new Clinton Justice Department are the new gays (again) are the new screenwriters are the new public schools are the new Nazis; it's worse than we thought.
