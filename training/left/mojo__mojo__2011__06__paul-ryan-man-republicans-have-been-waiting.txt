Is Rep. Paul Ryan (R Wis.) running for President? Like Matt Yglesias , I think that's the clear takeaway from his address to the conservative Alexander Hamilton Society last night. Via the Weekly Standard : 

Ryan squarely rejected the position of increased isolationism. Today, some in this country relish the idea of America's retreat from our role in the world, Ryan said. They say that it's about time for other nations to take over, that we should turn inward, that we should reduce ourselves to membership on a long list of mediocre has-beens. 

He continued, Instead of heeding these calls to surrender, we must renew our commitment to the idea that America is the greatest force for human freedom the world has ever seen. 

There's nothing new there substance-wise; what's notable is that it's Ryan who's saying it. He's the chairman of the House budget committee, and that's more or less all he talks about. His views on foreign policy are about as relevant as his views on the planking craze . 

That is, unless he's got something bigger on his mind. Although he's previously denied any interest in entering the race, those denials are beginning to take a less definitive tone. Asked last night by Fox News' Neil Cavuto whether he'd consider running, Ryan offered a non-answer : I want to see how this field develops. This morning, meanwhile, he addressed Ralph Reed's Faith and Freedom Conference , where he shared the bill with GOP presidential contenders Michele Bachmann and Herman Cain not the kind of place you'd expect to find a congressman with a (carefully crafted) reputation as an affable budget wonk. 

It's no secret that Republicans are unhappy with their current field of candidates. Hence the constant pining for Chris Christie, or Mitch Daniels, or Jeb Bush, or Rick Perry ( that Rick Perry). And in that sense, the Wisconsin congressman seems like a natural choice. Ryan's budget, which would phase out Medicare, has quickly become the centerpiece of the GOP's domestic agenda. Who better to lead the party into the 2012 election than Ryan himself?
