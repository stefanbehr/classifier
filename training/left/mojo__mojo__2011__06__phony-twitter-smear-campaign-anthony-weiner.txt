Anthony Weiner has no one to blame but himself for tweeting out lewd photos and engaging in racy conversation with women via Twitter, text message, telephone, and Facebook. But Weiner, who resigned from Congress last week, had plenty of enemies, and new evidence suggests they might have used sleazy tactics in an attempt to tar his reputation. 

As the New York Times reported , a pair of Twitter users ginned up fake identities and, months before he tweeted revealing photos of himself, actively sought out the New York Congressman, an active Twitter user. One user pretended to be a 16-year-old high school student named Nikki Reid, and another posed as her classmate Marianela Alicea. Via Twitter, Reid repeatedly tried to get Weiner to be her prom date. Alicea, meanwhile, contacted a group of conservative activists who used the moniker #bornfreecrew and claimed to have embarrassing information about Weiner. (That information was never produced.) 

But, as the Times notes, records show there are no students named Nikki Reid or Marianela Alicea at the high school they supposedly attended, Hollywood High School. More curiously, when a woman who said she was Reid's mother provided evidence to the media site Mediaite to back up that her daughter was who she claimed to be, the information she offered turned out to be bogus. No one seems to know who created the Twitter accounts, and it's unclear if different users created the two accounts or if one person was behind them both. However, it's hard to imagine a possibility other than political trickery behind the fake accounts. 

The plot thickens. Nikki Reid also sought out women Weiner had talked to online, the Times reported: 

The women included Gennette Cordova, 21, the college student; Ginger Lee, 24, a former pornographic film actress from Tennessee who exchanged over 100 e-mails with the congressman; and a Delaware high school student, 17, whose family said she exchanged five private messages with Mr. Weiner that did not include indecent or explicit material. 

In an interview, Ms. Cordova said she was contacted on Twitter by Nikki Reid, who said she admired one of her posts and then began exchanging private messages with her almost every day for three or four weeks starting May 5. There was something weird about it, Ms. Cordova said. At the beginning of their exchanges, Ms. Cordova said, there was no mention of Mr. Weiner. Then the user began to ask her for advice saying, I'm a fan girl too, and How did you get him to follow you? 

[ ] 

Ms. Cordova said that as she looked back on their exchanges, she saw other signs of a fraud. For example, Nikki Reid did not have a Facebook account, like most girls her age. And she made references to The O.C., the television show (featuring the young Hollywood actress Nikki Reed) that was popular among teenagers but ended in 2007. 

There is no way this girl is in high school, Ms. Cordova said. No way. 

To be clear, this isn't to say Weiner was set up. He screwed up when he publicly tweeted the now-infamous underwear pic , and the fault's all his for carrying on multiple online relationships with women who were not his wife. But as more information comes to light, parts of this scandal smack of the tactics used by conservative provocateurs such as James O'Keefe of Acorn and NPR fame. And if new revelations suggest Weiner was in fact the target of an organized smear campaign, it'll no doubt give other outspoken politicians pause in the world of social media.
