All the big names were on display last night in CNN's much-anticipated Republican presidential debate in New Hampshire Mitt Romney, Tim Pawlenty, Rep. Michele Bachmann (R-Minn.), and more. And it was Bachmann who stole the show early on, officially announcing that she'd filed paperwork to run for president and showing off her reinvented, fact-checked persona . Forced to pick a winner in the debate, however, the pundits bestowed the honor upon Romney, the GOP frontrunner. 

Nearly 100 political strategists surveyed by National Journal after the debate overwhelmingly picked Romney as the winner. Fifty-one percent of Republican strategists picked Romney, with Bachmann finishing second at 21 percent. On the Democratic side, 35 percent of those surveyed opted for Romney, while 26 percent chose Bachmann. 

Here's National Journal 's takeaway from the debate: 

Republican Insiders thought that Romney was a winner tonight in large part because none of his rivals were able to land any blows that damaged the party s nominal front-runner. When you are in the lead every day your opponents don t knock you back is a good day, said one GOP Insider. Made no mistakes, seemed comfortable, and confident, said another. 

Romney also won points when he was able to deflect criticism that the health care reform plan that he helped enact in Massachusetts inspired the national health care reform passed by President Obama and congressional Democrats. Handled tough questions effectively, asserted his frontrunner status, said one GOP Insider. Mitt didn t take on any water, remains frontrunner, said another. 

[ ] 

Democratic Insiders were not as bullish on Romney tonight but they concurred with Republican Insiders that Romney emerged from the debate without any permanent scars. Romney acted like the front runner and resisted all attempts to knock off message; he's still the one to beat, said one Democratic Insider. The level of debate was higher than I expected, acknowledged another Democratic Insider. Romney did not win every question, but he did present himself as the front-runner in the nature of Walter Mondale. Steady and gray. Some may sprint ahead, but slow and steady can win this primary race. 

Politico 's Roger Simon also named Romney the clear winner in last night's debate: 

Nobody laid a glove on him, not even for his flip-flop on abortion. Why not? Because they are a-skeered of him, that s why not! He might not make them his vice president! Plus, he can buy and sell them. They are but dust beneath his $600 shoes. He could afford to be gracious, saying things like the ideas Tim [Pawlenty] described are in the right wheelhouse. But nobody stuck one in Mitt s wheelhouse, not even for chickening out of the Ames Straw Poll. 

And they could have rattled him. Romney has a tendency to choke at big moments: His 2008 announcement speech was the worst of his campaign, his speech on religion was second rate and his PowerPoint presentation in May on health care was a joke.
