GOP presidential candidate Tim Pawlenty is set to deliver a major economic address at the University of Chicago this afternoon. There's a lot of the usual stuff in the prepared remarks , and one big idea. Pawlenty, the former Minnesota governor and everyone's second choice for the nomination, unveiled something called the Google test as a means to downsize government: 

We can start by applying what I call 'The Google Test.' If you can find a good or service on the Internet, then the federal government probably doesn't need to be doing it. The post office, the government printing office, Amtrak, Fannie and Freddie, were all built for a time in our country when the private sector did not adequately provide those products. That's no longer the case. 

The US Postal Service's problems are well documented , although it provides a public service that its competitors simply don't aspire to do and a quick Google search for Amtrak competitors doesn't yield much of anything. But beyond that, Pawlenty's Google Test seems to have one very serious failing: you can find a lot of things on Google. 

Here, for instance, is a very short list of goods and services that would also fail Pawlenty's Google Test: the military police fire departments hospitals (if we've replaced the army with Xe, we probably don't need the V.A.) schools prisons diplomats food inspectors 

Some of these are serious points of contention Republicans governors are making a huge push in the direction of private prisons, for instance, and the debate over private school vouchers isn't going away any time soon. Some of them aren't. The point is that can you find it on Google? is really a pretty useless question to ask when you're evaluating the value of a government service. 

In other words, the only Google-related story worth talking about in the 2012 race still involves Rick Santorum .
