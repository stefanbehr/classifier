On Monday, following the release of shirtless photos by Andrew Breitbart's Big Government , Rep. Anthony Weiner (D N.Y.) announced that he had sent lewd photos and text messages to six different women in the last three years all of whom he met online. It was a bizarre scene, made all the more so by the presence of Breitbart, who seized the podium before Weiner's press conference to take questions about his site's coverage of the scandal. But where does it rank on the spectrum of recent political apologies? Here's a quick look: 1. Sen. John Ensign (R Nev.) 



Busted: Had extra-marital affair with the wife of a close friend (and aide). Had his parents pay off the couple to keep them quiet. Used his influence to land the husband of his mistress a job. 

Strategy: Take full responsibility, but don't actually take full responsibility. Ensign says there's nothing to the reports of possible ethics violations. 

Did he resign? Yes, last month, when he faced expulsion from the Senate (the first since the Civil War) on ethics charges stemming from his cover-up. 

Silver lining: Introduces the phrase put your pants on and come home into the lexicon. 



2. Rep. Chris Lee (R N.Y.) 



Busted: Sent shirtless photos to a Maryland woman he met on Craigslist. 

Strategy: Put out a terse statement, resign immediately. 

Did he resign? See above. 



3. Sen. John Edwards (D N.C.) 





Busted: Cheated on cancer-stricken wife with campaign videographer Rielle Hunter. The National Enquirer publishes spy photos of Edwards holding Hunter's baby. 

Strategy: Deny, deny, deny and then eventually confess to the affair on Nightline : I became, at least on the outside, something different than that young boy who grew up in a small town in North Carolina. Continue to deny paternity of the child, and then cave on that too. 

Did he resign? Out of office. But he might go to prison now. 



4. Sen. Larry Craig (R-Idaho) 



Busted: Arrested in a sting at the Minneapolis International Airport, Craig pled guilty to soliciting sex from an undercover cop in a men's restroom. 

Strategy: Blame the local newspaper, the Idaho Statesman , for pressuring him into confessing even though he did nothing wrong. Blame the confusion over whether or not he was propositioning a cop by tapping his foot on a wide stance. Oh, and just to be clear: I am not gay, and I never have been gay. 

Did he resign? Nope. 

Is there a dramatic reenactment of the arrest that uses the police report as a script? Glad you asked . 



5. Gov. Mark Sanford (R-S.C.) 



Busted: GOP rising star goes missing for seven days in early 2009, causing the state police to start looking for him. Told staff that he was hiking the Appalachian Trail, then that he was in Argentina. Conducted long-distance with affair with Argentinian woman. 

Strategy: Apologize to more or less everyone he's ever met: I hurt her. I hurt you all. I hurt my wife. I hurt my boys. I hurt friends like Tom Davis. I hurt a lot of different folks. And all I can say is that I apologize. I I I would ask for your I guess I'm not deserving of indulgence, but indulgence not for me, but for Jenny and the boys. 

Did he resign? Resigned chairmanship of the Republican Governors Association, but finished out his term in Columbia. 



6. Sen. David Vitter (R-La.) 



Busted: Allegedly consorted with prostitutes in DC and New Orleans. 

Strategy: Tell media to drop dead. Hold a press conference to apologize for past failings, and then change the subject to local issues like a water resources bill and I-49 construction projects. Appear with wife at press conference, who says I am proud to be Wendy Vitter. It's a good thing, too, because she had previously told the Times-Picayune , I'm a lot more like Lorena Bobbitt than Hillary and that If he does something like that, I'm walking away with one thing, and it's not alimony, trust me. 

Did he resign? Nope. 

Any way things could get worse? Yes. After apologizing once more at a later event, he ran over a stop sign in the parking lot. 



7. Gov. Jim McGreevey (D-N.J.) 



Busted: Appointed Israeli Defense Forces vet to position as homeland security adviser; had affair with said homeland security adviser. 

Strategy: Come clean, come out. At a point in every person's life one has to look deeply into the mirror of one's soul and decide one's unique truth in the world. Not as we may want to see it, or hope to see it, but as it is. And so my truth is that I am a gay American. 

Did he resign? Spectacularly: 



8. Rep. Mark Souder (R In.) 



Busted: Affair with a part-time staffer. 

Strategy: Blame the poisonous environment of Washington, apologize to his family, acknowledge sins, improbably attempt to regain the moral high ground: I'm sick of politicians who drag their spouses in front of the cameras rather than confront the problems that they caused. 

Did he resign? Yes. 

Can't make it up: Souder and his aide also filmed a PSA advocating abstinence . 



9. Gov. Eliot Spitzer (D N.Y.) 



Busted: Spent $80,000 on call girls as attorney general and governor. 

Strategy: Confess to wrong-doing, keep it short, stand alongside wife. 

Did he resign? Yes. 

Scandal officially jumped the shark when... Call girl Ashley Dupre launched her own music career. 



10. President Bill Clinton (D) 



Busted: Had sexual relations with White House intern Monica Lewinsky. 

Strategy: Tell American people he did not have sexual relations with Monica Lewinsky, but later come clean while calling on the nation to move on: Even presidents have private lives. It is time to stop the pusrsuit of personal destruction and the prying into privates lives, and get on with our national life. 

Did he resign? Nope and he survives the impeachment proceeding too. 

And? If you have nothing better to do, you can read the Starr Report in its entirety here . The 90s were so weird. 



11. Rep. Eric Massa (D N.Y.) 



Busted: Groped male staffer and (in his own words) tickled him until he couldn't breathe . 

Strategy: No discernible strategy. Intitially fesses up to using salty language but denies any wrongdoing. Massa later resigns and goes on a media blitz and accuses a naked White House Chief of Staff Rahm Emmanuel of intimidating him in the Congressional gym. Massa calls Emmanuel the Devil's spawn, and says the administration forced him out because he didn't vote for the Affordable Care Act. 

Did he resign? Yes.
