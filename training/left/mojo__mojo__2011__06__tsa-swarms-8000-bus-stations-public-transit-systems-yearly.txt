Scott Ableman/ Flickr Think you could avoid the TSA's body scanners and pat-downs by taking Amtrak? Think again. Even your daily commute isn't safe from TSA screenings. And because the TSA is working with Immigration and Customs Enforcement (ICE) and Border Patrol, you may have your immigration status examined along with your junk . 

As part of the TSA's request for FY 2012 funding, TSA Administrator John Pistole told Congress last week that the TSA conducts 8,000 unannounced security screenings every year. These screenings, conducted with local law enforcement agencies as well as immigration, can be as simple as checking out cargo at a busy seaport. But more and more, they seem to involve giving airport-style pat-downs and screenings of unsuspecting passengers at bus terminals , ferries , and even subways . 

These surprise visits are part of the TSA's VIPR program : Visible Intermodal Prevention and Response. The VIPR program first started doing searches in 2007, and has grown since then. Currently, the TSA only has 25 VIPR teams doing these impromptu searches: in 2012, it wants to get 12 more. 

The searches are in the name of passenger security, and the TSA says it wants to prevent incidents like the 2004 Madrid train bombings. But if the airports' TSA searches miss security risks like large knives , loaded guns , and explosives , there's certainly the chance that screenings at train stations would be similarly flawed. 

Not to worry: security isn't the only goal of VIPR. A recent VIPR operation /screening at a Tampa Greyhound bus station was conducted with US Border Patrol and ICE. What we're looking for is threats to national security as well as immigration law violators, said Steve McDonald from US Border Patrol. An ICE representative said that they were also looking for smuggling, and Gary Milano from Homeland Security said that although that was the first time the Tampa bus depot had been screened, VIPR would be back again sometime in the future and was using the element of surprise as a deterrent to the bad guys. 

Although one man at the Tampa screening said he felt safer, VIPR operations are not without their naysayers. A VIPR screening at a Des Moines Greyhound station last week is alleged to have targeted Latinos. Another TSA/Border Patrol VIPR screening on a trolley in San Diego resulted in three teens being handcuffed and deported while on their way to school . Around 20 others were also deported , according to local news outlets. 

The trolley is part of the San Diego Metropolitan Transit System. We believe this is a flagrant violation of human rights, when we have a situation in which children are being separated from their families without the proper due process rights being afforded to them, said a spokesman for the girl's family. The three teens nabbed in the San Diego VIPR operation were deported to Tijuana, but later allowed to re-enter the United States on humanitarian visas. 

More children, this time train passengers disembarking at Savannah, Georgia , were treated to questionable TSA treatment in February along with their families. While the passengers (who again, had just gotten OFF a train) were lifting their shirts and having bras handled during pat-downs, their luggage was sitting unattended on the train platform. 

The TSA later admitted that the VIPR operation should have ended before the train entered the station, but told the public that the Savannah passengers didn't have to enter the screening area... even though an eye-witness says a TSA agent instructed them to go into the screening area to collect their luggage... the luggage that was actually waiting somewhere else. 

VIPR operations are now even targeting freight trucks on highways. In addition to the random checks on public transit systems , it makes you wonder: can private vehicles be far behind? Will there be any mode of transportation beyond the reach of the TSA? 

UPDATE: According to at least one news report out of Brownsville, Texas, TSA/VIPR has already conducted unannounced inspections of private passenger cars and trucks. Thanks for the tip, reader @jwindz. 

UPDATE 2: Welcome, Drudge Report readers! If you liked this story, check out our story on how the TSA is scanning your face in an attempt to read your mind , our explainer on the safety of the new porno-scanners , our report on the TSA missing a man's loaded handgun , our investigation of the people who are profiting from the new scanners , and Kevin Drum's anti-anti-TSA rant .
