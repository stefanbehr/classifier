Two years ago, Mother Jones reported that amateur historian Phil Mellinger had made an intriguing discovery about one of the great political mysteries of the 20th Century: the 18 minute gap in the Watergate tape of the meeting in which President Richard Nixon first discussed the break-in with his chief of staff, H.R. Bob Haldeman. 

After examining Haldmen's original handwritten notes of the meeting, Mellinger saw that these two pages contained little information corresponding to the erased portion of the Watergate tape. Which was odd, because Haldeman took copious notes of his sessions with Nixon. There seemed to be a gap in the notes that matched the gap in the tape. This suggested that pages of notes might have been removed. And Mellinger had an idea: a forensic procedure known as impressions analysis under which a page is examined to determine what had been written on the preceding page might determine if indeed pages had been removed and perhaps even reveal what had been written on them. 

Mellinger raised this idea with the National Archives, which holds the Watergate papers and tapes, and officials there thought he was on to something. They initiated the process he had proposed, hoping that they could finally solve this mystery. 

Two years later, on the 39th anniversary of the Watergate caper, the National Archives has revealed its findings in a nifty video. And...it did not answer this enduring question. 

The CSIers retained by the Archives determined there is indented writing on the second page of the notes. It looks like a signature, but it is illegible. They also found that a date written on the top of the first page and the page number written on the top of the second page were written in different ink than the rest of the notes. The Archives declined to draw conclusions from this, but this could mean that the notes were tampered with and that a page number was written on the second page to cover up the removal of notes. 

So the famous gap remains empty. And the Archives, which had previously explored using high-tech methods to recover audio from the tape, notes that in the future additional work may be able to be done on the tape itself. But for now, this central part of the Watergate cover-up is intact. 

Here's the National Archives video. It shows all the procedures used to analyze the Haldeman notes. It was an impressive exercise.
