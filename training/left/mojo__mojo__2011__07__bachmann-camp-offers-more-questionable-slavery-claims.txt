The conservative group The Family Leader may have dropped the controversial line about slavery from its Marriage Vow, but not before the vow's first signer, Rep. Michele Bachmann (R-Minn.), dug in a little deeper. 

Her spokeswoman first stated that Bachmann was only signing the vows portion of the pledge, not the preamble, which is the part that included the controversial line claiming that black families might have been better off during slavery. But then she threw in a line comparing actual slavery with economic slavery ( via Politico ): 

She signed the 'candidate vow,' campaign spokeswoman Alice Stewart said, and distanced Bachmann from the preamble language, saying, In no uncertain terms, Congresswoman Bachmann believes that slavery was horrible and economic enslavement is also horrible. 



Osha Gray Davidson summed up the problem with this pretty well over at Forbes : Bachmann s camp didn't explain what the phrase economic enslavement means, but apparently, the candidate has once again reduced the horrors of historical slavery to a talking point on her presidential quest. 

It's not the first time Bachmann's made slavery analogies, however. Think Progress highlights three other quotes from the candidate likening things that are not at all like the actual enslavement of people to slavery: 

Health care reform : This is slavery It's nothing more than slavery. 

The national debt : It is a slavery, it is a slavery that is a bondage to debt and a bondage to decline It is a subservience of a sovereign people to a failed, self-selected elite. 

Homosexuality : If you're involved in the gay and lesbian lifestyle, it's bondage. It is personal bondage, personal despair and personal enslavement.
