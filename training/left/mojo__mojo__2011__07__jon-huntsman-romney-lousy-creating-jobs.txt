While touting his economic record as Utah Governor during a South Carolina campaign stop Monday night, Jon Huntsman took a passive-aggressive swipe at Mitt Romney's job creation credentials: 

When you look at the absolute increases in job creation, Utah led the United States in job creation. That compared and contrasted with other states say, Massachusetts, I'll just pull that out randomly not first, but 47th. 

Huntsman spokesman Tim Miller upped the ante Tuesday morning by stating Massachusetts's job growth under Romney was abysmal by every standard, and that it edged past only Louisiana, Michigan, and Ohio: 

You know your job creation record is bad when you brag about leapfrogging a state ravaged by Hurricane Katrina We assume Mitt Romney will continue to run away from his record. 

Some of the media reaction to these statements was overblown. For instance, Slate 's David Weigel took this to mean that Huntsman had completely abandoned his campaign's civility pledge (apparently confusing civility with not saying anything at all), and ABC News characterized it as go[ing] nuclear on Romney. 

Nevertheless, in strongly criticizing the Republican frontrunner certainly more sharply than Tim Pawlenty did with the short-lived Obamneycare critique the Huntsman campaign really doesn't have anything to lose. Less than a month into his presidential run, Huntsman is still saddled by unimpressive poll numbers , lack of name recognition, and a low second-quarter cash haul (Romney leads him nearly 5 to 1 in fundraising). So his challenge to Romney sort of feels like that time the director of BloodRayne challenged Michael Bay to a boxing match : some media buzz, but with minimal immediate impact. 

That doesn't mean the jobs issue won't matter down the road. Huntsman's critique is largely accurate: as Mother Jones ' Andy Kroll reported , Romney's claims of being a great job-creator are dubious at best. Huntsman has highlighted something that could become a big liability for the GOP frontrunner in the coming months. But will Huntsman still be in the race to see it?
