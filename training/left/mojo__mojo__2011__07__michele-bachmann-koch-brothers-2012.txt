The billionaire Koch brothers, Charles and David, spread their money far and wide. They fund free-market think tanks , right-leaning academic organizations , and conservative political advocacy groups, such as the Americans for Prosperity Foundation , which helped cultivate the tea party. The political action committee for the Kochs' massive conglomerate, Koch Industries , has also given generously to big-name conservative politicians throughout the country at both the federal and state level. But when it comes to the 2012 presidential race, the Kochs have been more selective with their giving, with only one presidential candidate so far benefiting from their largesse: Rep. Michele Bachmann (R-Minn.) . 

In late May, KochPAC, as Koch Industries' PAC is known, donated $10,000 in one day to Bachmann-linked committees $5,000 to her 2012 congressional re-election committee and $5,000 to her political action committee, MICHELE PAC, according to the most recent records available . (That's short for Many Individual Conservatives Helping Elect Leaders Everywhere PAC.) The two checks weren't KochPAC's first contributions to Bachmann. According to federal campaign records , the committee has given $25,000 to the Minnesota congresswoman since 2006, excluding the May donation. 

It's unclear whether KochPAC has also donated to Bachmann's presidential committee, which was created in June after she officially unveiled her candidacy. Her campaign has yet to report contributions and expenditures to the Federal Election Commission. But it is worth noting that Bachmann can, under FEC rules, transfer her congressional campaign funds into her presidential war chest, since she suspended her congressional fundraising operation last month. When Bachmann does reveal her latest fundraising haul, a large chunk of it is expected to come from her congressional campaign. 

According to campaign records, Bachmann is the only Republican in the presidential race who has received any money from KochPAC in 2010 and 2011. In 2010, the committee did give $50,000 to Texans for Rick Perry, a PAC that supports Texas Republican Gov. Rick Perry, who is flirting with the idea of a presidential run but has yet to decide. Indeed, the only other Republican candidate with a history of KochPAC support is hard-line former Sen. Rick Santorum of Pennsylvania. Between 1999 and 2005, federal records show, Santorum election committees received $16,000 from the committee. 

Election Day, of course, is more than 15 months off, which is plenty of time for the Koch brothers to shower other GOP presidential candidates with cash. The candidate who previously appeared to closest to the billionaire brothers was former Massachusetts Gov. Mitt Romney. It's been previously reported that Romney counted David as a supporter; according to iWatch News , David and his wife even hosted an event at their Hamptons home last August in support of Romney. In April, Romney spoke at a GOP presidential cattle call hosted by Americans for Prosperity, the political advocacy group David founded . And in the last presidential election, David and his wife each gave the maximum $2,300 donation to Romney's campaign. (FEC records don't turn up any donations by Charles Koch to a 2008 presidential candidate.) 

So far, Romney's been the most prolific fundraiser within the GOP field , netting $18.25 million in the second quarter of this year. Even so, he may be wondering if his own KochPAC check is in the mail. 

Print Email Tweet Vintage Bachmann: Pawlenty s a Communist Corn on Hardball: Steele Agrees GOP s Discrediting GOP Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Bachmann Summit With Anti-Gay Heavy-Metal Minister Cancelled 

VIDEO: MoJo Presses Bachmann on Gay Therapy Stories 

And her response is...silence. What Exactly Did Michele Bachmann Learn in Law School? 

Bachmann Vows to End Nonexistent Federal Program 

Michele Bachmann: Flake or Not? 

Your guide to the Minnesota firebrand's most outrageous, outlandish, and out-there remarks. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
