[UPDATE: The DSCC's call to Koch Industries turned out to be a staff error. Read the committee's full response at the end of this post. ] 

Can a political money story get any weirder than this? 

Sen. Patty Murray (D-Wash.), who leads the Democratic Senatorial Campaign Committee, recently called an official at Koch Industries , the industrial conglomerate owned by the infamous Koch brothers , to hit up the company for campaign donations. The DSCC exists to help elect Democratic candidates to the Senate, and it faces a pitched fight in 2012 to retain Democrats' slim majority there. 

You can listen Murray's message, which sounds to me like a pre-recorded robo-call, here . Even if it is, the message suggests that Koch Industries' political action committee has in fact donated to the DSCC in the past, despite the strongly libertarian and Republican leanings of the Koch brothers themselves. 

Here's what Murray said: 

Hi, this is US Senator Patty Murray calling. I'm sorry I missed you today. I wanted to catch up with you. 

As you know I chair the DSCC. You have been a past supporter of ours through your PAC and I wanted to catch up with you to see if you'd be willing to renew your membership. We've got a great retreat coming up this fall in Kiawah Island in South Carolina. We'd love to have you join us. 

So I was hoping you could call me back, 202-[REDACTED]. Thank you. 

In a blog post on the company's KochFacts site, Philip Ellender, who runs Koch Industries' lobbying and PR operations, responded with a spicy open letter to Murray's message: 

Senator Patty Murray, Chair 

Democratic Senatorial Campaign Committee 

Dear Senator Murray: 

For many months now, your colleagues in the Democratic Senatorial Campaign Committee leadership have engaged in a series of disparagements and ad hominem attacks about us, apparently as part of a concerted political and fundraising strategy. Just recently, Senator Reid wrote in a DSCC fundraising letter that Republicans are trying to force through their extreme agenda faster than you can say 'Koch Brothers.' 

So you can imagine my chagrin when I got a letter from you on June 17 asking us to make five-figure contributions to the DSCC. You followed that up with a voicemail indicating that, if we contributed heavily enough, we would garner an invitation to join you and other Democratic leaders at a retreat in Kiawah Island this September. 

I m hoping you can help me understand the intent of your request because it s hard not to conclude that DSCC politics have become so cynical that you actually expect people whom you routinely denounce to give DSCC money. 

It is troubling that private citizens taking part in the discourse have become the targets of White House and DSCC fundraising missives, and we would certainly encourage you to rethink that approach. Ultimately, I expect voters will see through that and will weigh the issues on the merits alone. But in the meantime, if you could provide me some insight on what exactly you are asking of us and why, I would be most grateful. 

Sincerely, 

Philip Ellender 



President, Government Public Affairs 

Koch Companies Public Sector, LLC 

Here's what the DSCC's executive director Guy Cecil wrote in reply to Ellender: 

Dear Phil: 

Thank you for your genuine, heartfelt concern about our recent solicitation and your request for clarification. Indeed, the form letter and follow-up solicitation you received was a staff error. 

However, the bigger and more troubling mistake is the long political history of your employer, the Koch Brothers. As a (former?) Democrat, perhaps your time would be better spent looking into their efforts to privatize Social Security or their opposition to expanding the children's health insurance program. Or maybe, you can post a list of all of the anonymous contributions they have made to right-wing smear campaigns across the country. If you'd like to share voicemails from all the shady groups asking you for millions of dollars, we'd happily listen to those as well. 

So, I write to make it clear that your invitation was an error and has been rescinded. 

Sincerely, 

Guy Cecil 

Executive Director 

Democratic Senatorial Campaign Committee 

P.S.: I was impressed how quickly you responded, given how often you must be on the phone with Governor Walker of Wisconsin. 

Print Email Tweet Rick Perry Pal: Oprah is Triggering the Antichrist State Budgets: Murder by Loophole Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Waxman Targets the Koch Brothers 

Wisconsin Gov. Scott Walker: Funded by the Koch Bros. 

The politician trying to eviscerate public-sector unions is in sync with one of his largest financial backers--the right's infamous billionaire brothers. The Koch Brothers Vast Right-Wing Media Conspiracy 

Watched any conservative programs lately? Chances are, you've seen an oil-funded pundit. Charles Koch s Assault on Academic Freedom 



Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
