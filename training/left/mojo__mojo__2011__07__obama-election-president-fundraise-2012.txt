President Obama's 2012 campaign and the Democratic National Committee together raised a staggering $86 million in April, May, and June of this year to re-elect the president. Broken down, more than $47 million of that was raised by the Obama for America committee, which surpasses the fundraising totals of the entire Republican field combined. (Note, though, that GOP presidential money-leader Mitt Romney's $18.6 million take in the second quarter didn't include Republican National Committee money, so it's not accurate to compare Obama and the DNC to GOPers, only because we don't know what the RNC has raised.) 

So, Obama can rake in the big bucks no surprise there. The real story is the 552,000 donors who gave to the Obama 2012 effort, more grassroots support at this point in the process than any campaign in political history, said campaign manager Jim Messina in a video message to supporters . Messina said that 98 percent of donations last quarter were under $250, and that the average donation was $69. 

That haul throws a huge bucket of cold water on claims that Obama is losing his liberal /Democratic base. Pollster James Zogby wrote in September 2009 that Democrats were souring on Obama after the health-insurance-reform fight and for his policies on the war in Afghanistan. Liberal TV host Ed Schultz told former White House press secretary Robert Gibbs last year that you're losing your base. If Obama's donor rolls are any indication, the left appears to be just as motivated in the 2012 race as they were in 2008. 

Of course, reams of polling data have been reinforcing this for months. According to Gallup polling , Obama's approval rating among Democrats has held steady at around 80 percent, give or take a few percentage points, since September of last year. Among liberals, Obama's doing almost as well, with approval ratings hovering around 70 percent; it's currently 76 percent. 

So all that talk of Democrats and liberals sloughing off Obama? Nothing to it. Today's fundraising numbers prove Obama's still hugely popular, and that whomever the GOP picks to run against him will face the major undertaking of matching the powerful Obama fundraising machine. 

[UPDATE: Adam Green, who co-founded the Progressive Change Campaign Committee, e-mails to remind me that all the donations cited above came in before Obama suggested cuts to Social Security and Medicare, both hugely popular programs among Democrats, as part of a deficit reduction deal. If President Obama continues down the path to a disastrous deal, he will lose millions in donations and millions of volunteer hours from people who once passionately supported him in 2008, Green writes. ] 

Print Email Tweet Phony Democrats Vanquished in WI Recall Primary Pawlenty Rejects Family Leader Pledge Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Inside Mitt Romney s Sprawling Political Money Machine 

Why the former governor of Massachusetts is still the savviest fundraiser in the GOP field. Tim Pawlenty s Weak Fundraising Haul 

It s Fundraising Day! 

Obama and the Debt Ceiling: An Outrage Deficit? 

In the current face-off, the president's playing the adult. Is it time he gives reckless GOPers a public spanking? Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
