On Wednesday, 247 national progressive and good government organizations, including the AFL-CIO, Democracy 21, National Coalition for LGBT Health, and OMB Watch, co-signed a letter opposing House Judiciary Committee Resolution 1 better known as the balanced budget amendment. Both the House and Senate are expected to vote on the measure next week. 

The bill would cap government spending at 18 percent of gross domestic product (GDP). That would slash spending much deeper than Rep. Paul Ryan's (R-Wisc.) Medicare-privatizing, Medicaid-gutting, rich people-coddling budget plan , which holds spending between 20 and 21 percent of GDP over most of the next two decades (currently, spending is around 24 percent of GDP ). 

The proposal also makes it exceedingly difficult for Congress to undo the budget damage inflicted by the cap: it requires a supermajority in both houses to break the cap, increase the debt limit, raise taxes, or close loopholes, strangling the government's ability to increase spending on social programs, health care, or unemployment benefits. Effectively, the bill would shackle Congress to a foolish piece of legislation that could torpedo the economy and send it spiraling into another recession. 

From the letter: 

This irresponsible requirement would create an extremely steep hurdle to raising any revenues effectively blocking revenue measures even as part of packages to restore long-term solvency to Social Security and Medicare while protecting the more than $1 trillion a year in tax expenditures and forcing severe program cuts. 

In short, this amendment is a recipe for making recessions more frequent, longer, and deeper, while requiring severe cuts that would harshly affect seniors, children, veterans, people with disabilities, homeland security activities, public safety, environmental protection, education and medical research. It would almost certainly necessitate massive cuts to vital programs including Social Security, Medicare, Medicaid, veterans benefits and other programs, and, as noted, lead to even deeper cuts than the House passed budget. 

But even if the balanced budget amendment makes it out of the House, it's virtually dead-on-arrival when it lands in the Democrat-controlled Senate. 

So why are John Boehner and Eric Cantor wasting time lobbying for a measure that's almost certain to fail? Because skeptical tea partiers are just waiting to pounce on them for signing off on a deal that raises the debt ceiling and cuts the deficit but also increases taxes. If higher taxes or closed tax loopholes end up in whicever debt deal emerges, at least the Republican leadership can look to the right wing elements of their party, point to the amendment, and say Hey, look: we tried. In other words: it's mostly intra-party politics.
