As the civil war in Libya approaches its sixth month, Muammar Qaddafi is reportedly sitting on a contingency plan that could make Dr. Strangelove blush. 

According to Russian envoy Mikhail Margelov, the embattled despot has said that if rebels were to capture Libya's capital, Tripoli, he would respond by completely leveling the city. The Daily Telegraph has the story: 

Mikhail Margelov, in an interview with the Russian Izvestia daily, said: The Libyan premier told me: if the rebels seize the city, we will cover it with missiles and blow it up. 

Mr Margelov met Libyan Prime Minister Baghdadi al-Mahmudi last month. 

I imagine that the Gaddafi regime does have such a suicidal plan, he added 

The dictator's alleged threat to raze his own capital city fits in with his tendency to make wild, bloodlusty pronouncements, including his promise to target any traitor who is cooperating with the [Americans'] Christian Crusade, or his early-July vow to spread the war to Europe in retaliation to NATO intervention. 

The Qaddafi regime's diminishing supplies and funds suggest that such a large-scale counterattack would be tricky to implement. MSNBC reports: 

[New US intelligence reports] point to three key indicators: dwindling fuel supplies, a cash crisis and reports of low morale among regime troops...A Libyan official also said the country was facing a food shortage as the lack of fuel meant only 20 percent of crops have been harvested. 

But shrinking stockpiles are hardly the main reason why Qaddafi's newest plan probably wouldn't get past the brainstorming stage. I don't think he'd be able to pull it off, Lawrence Korb , a senior fellow and defense expert at the Center for American Progress, told Mother Jones . His people wouldn't put up with it. There have already been many defections. No doubt he might try, but there's a big difference between blowing up, say, a commercial airliner and orchestrating a mass suicide in a city.
