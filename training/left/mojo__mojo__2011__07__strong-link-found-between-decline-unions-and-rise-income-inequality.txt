It's well known that the death of America's labor unions coincides with a staggering rise in income inequality , though the link between the two has never been as obvious as it seems. Many academics argue that unions play a relatively minor role in the equation, instead blaming educational disparities and the shifting makeup of the economy. But now comes a major new study from Harvard sociology professor Bruce Western that suggests that the decline of unions is as important as any other factor, explaining a full third of the growth in of income inequality for male workers. The loss of labor unions explains a full third of the growth of inequality for male workers 

Western and co-author Jake Rosenfeld, a sociology professor at the University of Washington, looked at the period between 1973 and 2007, when inequality in hourly wages spiked by 40 percent. During that time, union membership for private-sector male workers fell from 34 percent to 8 percent (female workers were never as unionized as their male counterparts). Their paper in the August issue of the America Sociological Review concludes that deunionization's biggest effects on inequality were indirect: 

1) The threat of unionization caused non-unionized employers to raise wages; that threat disappered along with unions. 

2) Unions occupied a bully pulpit; knocking them off left the moral case for equality vulnerable to attack. ( What do you mean Viacom's CEO isn't worth $85 million ?) 

3) Workers lost their Washington lobbyists, and with them, any hope of winning political battles for better wages and benefits. 

These ideas are nothing new. Kevin Drum ably explores them in his March/April Mother Jones essay, Plutocracy Now . Yet the Harvard study bolsters them with a rigorous regression analysis of census data, showing empirically what many pundits have long suspected. Our study underscores the role of unions as an equalizing force in the labor market, Western says. If only proving their importance was as easy as figuring out how to replace them.
