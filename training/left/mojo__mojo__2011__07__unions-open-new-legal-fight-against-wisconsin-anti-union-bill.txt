A group of labor unions including the AFL-CIO filed a second lawsuit in federal court on Wednesday challenging Wisconsin Republican Governor Scott Walker's anti-union budget bill that curbed collective bargaining rights for most public-sector unions. 

The unions behind this latest suit, which represent 2,700 employees in state capitol Madison and surrounding Dane County, say Walker's law violates the US Constitution because it exempts certain public employees among them cops and firefighters from the bargaining ban. There is not a constitutionally reasonable basis to justify such unequal treatment under the law, the suit reads, and [the provisions in Walker's bill] are in derogation of the rights secured at the XIVth Amendment to the United States Constitution. 

The lawsuit mirrors one filed by Wisconsin's main teachers union in June that also challenges Walker's bill on same constitutional grounds. 

It also comes after a state-level effort to overturn Walker's bill fell short. In March, the Dane County district attorney sought to throw out Walker's bill, alleging that Republicans in the Wisconsin legislature violated the state's open meeting law by failing to give proper disclosure of a meeting where Senate Republicans voted to approve the bill. A county judge agreed with the DA, and in May struck down the bill. However, a bitterly divided Wisconsin Supreme Court last month reversed the lower judge's ruling and upheld Walker's bill. In mid-June, the legislation went into full effect . 

Read the AFL-CIO's new suit here: 

July 6 Suit Against Walker Bill
