As Americans lose ever more jobs and economic clout to China, the pressure's mounting for us to become more Chinese . Enter Texas Governor Rick Perry, whose 2012 presidential campaign slogan might as well be, If you can't beat 'em, join 'em. In many ways, Texas is the China in our own backyard, a big, brash upstart that's created thousands of jobs by playing economic hardball. Admirers of the Lone Star State have dubbed its economy the Texas Miracle, but maybe a better name would be the Texas Tiger. 



Jobs for the taking: 

China: Since joining the WTO, it has taken or caused the loss of more than 2.4 million US jobs . 

Texas: Home to half of all US jobs created since 2009 . Perry travels to other states to poach major employers . 

Red states: 

China: Deflates the value of its currency by 40 percent to subsidize exports and job creation. 

Texas: Since 2003, has doled out $732 million in tax credits and subsidies to companies that relocated to the Lone Star State. 

Labor on the cheap: 

China: About 10 percent of the population still earns less than a dollar a day (pdf). 

Texas: Tied with Mississippi for the highest percentage of workers that earn the minimum wage or less . 

Eco-impunity: 

China: World's top carbon emitter would rather burn cheap coal than sign a climate treaty. 

Texas: Nation's top carbon emitter was only state to refuse to comply with new federal regulations on greenhouse gas emissions. 

Tea party: 

China: Effective corporate tax rate of 16.6 percent is less than half the US rate. 

Texas: Top corporate tax rate of 1% is fourth lowest among US states. Bonus: No personal income tax. 

Toxic torts: 

China: Tainted milk, poisonous toys, glow-in-the-dark pork: Product scandals are common. Court convictions, not so much . 

Texas: Hurt? Injured? Need a lawyer? Too bad! writes Texas Monthly , pointing out that the state's tort reforms force everyone from the hospitalized to homebuyers to fend for themselves. 

Of rice and men: 

China: Suffers from a lack of adequate (even basic) social protection for a large portion of its 1.3 billion population, according to the International Social Security Association. 

Texas: Ranks 46th out of 50 states in per-capita spending; new budget slashes another $15 billion from social services such as Medicaid, mental health centers, and legal aid for the poor. 

Free-market cronyism: 

China: Princelings such as vice-president Xi Jinping and Chongqing party secretary Bo Xilai have gotten rich by trading on their connections. 

Texas: Good ol' boys such as corporate raider Harold Simmons and real estate mogul Harlan Crow have gotten rich by trading on their political donations. 

Pray for rain: 

China: Encroaching desert consumes a million acres of land a year. 

Texas: The worst drought in history has turned large parts of the state into a moonscape. 

40-gallon hats: 

China: Texas: 



Print Email Tweet We re Still at War: Photo of the Day for July 29, 2011 Which GOP Candidate is the Worst on Reproductive Rights? Josh Harkinson 

Reporter 

Josh Harkinson is a staff reporter at Mother Jones . For more of his stories, click here . Email him with tips at jharkinson (at) motherjones (dot) com. To follow him on Twitter, click here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Rick Perry s Cancelled MoJo Subscription 

The next subscription's on us, governor. What Rick Perry Learned From Ron Paul 

Rick Perry s Top Controversies 

The Texas governor's moving closer to a White House bid. Your guide to his troubled track record. Who Does the National Association of Manufacturers Really Work For? 

Far from representing US manufacturing, the group has allied itself with companies that make money selling cheap Chinese goods back to Americans. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
