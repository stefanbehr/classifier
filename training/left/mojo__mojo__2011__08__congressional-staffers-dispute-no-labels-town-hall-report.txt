Congressional staffers are pushing back against a report by the group No Labels that claims nearly 60 percent of members of Congress did not schedule free, public town halls during the August recess. 

Here's what Ellis Brachman, a spokesman for the House Democratic Caucus, wrote in an email on Wednesday: 

I m sure No Labels had the best intentions in trying to put this report together but it's so riddled with errors, many of which even a simple internet check would have caught, that the result is at best incredibly misleading. I can't speak for House Republicans, many of whom it's been widely reported are hiding behind pay walls to keep a friendly crowd, but House Democrats are out in their districts listening to their constituents at Town Halls and all sorts of other events. Anyone claiming differently is very misinformed. 

A No Labels spokesman didn't immediately respond to a request for comment. You can the group's original report here .
