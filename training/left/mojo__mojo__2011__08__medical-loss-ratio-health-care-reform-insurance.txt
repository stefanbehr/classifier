It seemed like a good idea at the time: a key piece of the Patient Protection and Affordable Care Act (ACA), known as the medical loss ratio (MLR), would require insurance companies to spend 80-85 percent of their customers' premium payments on medical costs. 

If insurance companies spent more on quality care and less on administrative and overhead costs, the thinking went, patients would stand to benefit. Insurers that didn't meet the MLR would have to pay out rebates to customers. 

But insurance agents argued that the MLR will put a crimp on business. And a recent analysis by the Government Accountability Office (GAO) seems to bear that out. The report finds that some insurers are lowering premiums or leaving rates unchanged to help them comply with the MLR rules. Most directly affected are insurance agents and brokers, The Hill explains : 

Commissions to agents and brokers fall into the 15 or 20 percent of revenues that insurance companies can use for administrative expenses and profit. Brokers are worried that insurance companies will cut commissions and redirect that money toward their own bottom lines. 

The GAO said almost all of the insurers it interviewed are cutting commissions. Those cuts enabled the plans to change their premiums. 

While the MLR may push down premiums for customers, it could also limit their coverage choices, according to the GAO report: 

One insurer said that they have considered exiting the individual market in some states in which they did not expect to meet the PPACA MLR requirements, while several other insurers said that the PPACA MLR requirements will not affect where they do business. 

So even as health care becomes more affordable, consumers could potentially find a less diverse marketplace. 

Some health policy analysts anticipate more conflicts like this as implementation of the ACA moves along (or doesn't). Micah Weinberg, a senior policy advisor at the Bay Are Council and a proponent of the ACA, points out that the law was crafted to help change the financial incentives of the health care industry to, in essence, make it more profitable to cover sicker people. In theory, insurers that comply with the ACA's provisions and keep their customers healthy stand to reap major profits. 

But if those same insurers exceed the MLR's 15% limit on administrative costs a not-altogether-implausible result of expanding coverage the ratio penalizes them. By fixing insurers to the MLR, in other words, the ACA penalizes insurers for following the law, according to Weinberg. 

Health care reform represented 1,000 of the best ideas that people have had about health care policy over the past thirty years, all rolled into one big burrito, he says. Though any of them would be fine in isolation, some of those are pretty directly contradictory...the medical loss ratio negates a lot of what we're trying to do through the bill.
