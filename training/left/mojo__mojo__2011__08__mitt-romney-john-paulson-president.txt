Hedge fund guru John Paulson shot to fame (and fortune) with his superbly timed bet against the housing market just before its implosion in 2007, a move christened the greatest trade ever. Now, Paulson is spending some his fortune on politics, and in particular on GOP presidential candidate Mitt Romney. 

As iWatch News' Peter Stone reports today , Paulson is doing double duty for Romney, donating to a pro-Romney Super PAC and hosting a fundraiser for the candidate. Stone says Paulson's dual roles illustrate a new reality in our post- Citizens United campaign finance playing field, where the roles of outside PACs and candidates often overlap: 

A big bundler for Romney s campaign, Paulson who made billions betting on the decline of the housing bubble is emblematic of how wealthy individuals often wear two fundraising hats in the aftermath of the Supreme Court s Citizens United ruling last year. That decision gave the green light for donors (including corporations and unions) to give unlimited sums to independent groups that advocate expressly for candidates. 

Paulson's fundraising is one of a few instances where the fundraising operations of the campaign and the Super PAC appear to overlap and may benefit each other. 

* Romney has shown his appreciation for the Super PAC by attending several of its fundraising events including a dinner in New York on July 19 that drew about two dozen potential and current donors, including Paulson, according to fundraisers familiar with the PAC. 

* Lisa Spies, who runs PAC fundraising and Jewish outreach for the Romney campaign, is married to Charles Spies, a founder of the PAC who serves as its treasurer and was general counsel to Romney s 2008 campaign. 

[ ] 

Campaign finance lawyers and watchdog groups say that Federal Election Commission rules define coordination to allow some fundraising overlap between campaigns and independent groups, thereby creating sizable loopholes. FEC regulations allow an enormous amount of actual coordination that's not considered under the law to be illegal coordination, Trevor Potter, a former chairman of the FEC and president of the independent Campaign Legal Center told iWatch News. The regulations allow a significant amount of overlap in fundraising. 

But Potter added, These loopholes are clearly not what the Supreme Court had in mind when it spoke about independent spending campaigns in its Citizens United decision. 

Stone's full story is well worth a read. 

Print Email Tweet Tracking Prison Deaths Is Tougher Than You d Think Six Stats You Need to Know on Election Day in Wisconsin Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Campaign Watchdogs Fire Back at Romney Super PAC s Mystery Donor 

Mysterious, Defunct Company Donates $1 Million to Romney SuperPAC 

Romney State PACs in Hot Water 

Inside Mitt Romney s Sprawling Political Money Machine 

Why the former governor of Massachusetts is still the savviest fundraiser in the GOP field. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
