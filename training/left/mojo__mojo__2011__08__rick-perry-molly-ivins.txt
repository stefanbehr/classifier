As Texas Gov. Rick Perry embarks on his Obama-bashing , evangelical-courting , tea party-outdoing campaign for the presidency, we miss Molly Ivins more than ever. 

The late and celebrated liberal columnist was known as a writer of sparkling political commentary infused with a trenchant wit and copious shots of zero-bullshit humor. She was the consummate Texan, a ferocious populist critic of the American right wing, and a sorely missed Mother Jones contributor . Here's the C-SPAN footage of Ivins speaking at a Mother Jones fundraising event in 1992: 



Ivins also had a special place in her heart for delivering blistering, bold critiques of Perry's track record as a governor, a poor thinker, and an even worse obfuscator. She was, after all, the writer who bestowed upon Perry the nicknames The Coiffure and Governor Goodhair. 

Here are a few highlights of Ivins tearing apart Perry's stances on the death penalty, creationism, and taking dirty money from Enron. Shrub Flubs His Dub The Nation , June 18, 2001 

In this Nation article from the advent of the Bush years, Molly Ivins makes one of her first observations of just how damn good Rick Perry's hairdo is: 

Bush was replaced by his exceedingly Lite Guv Rick Perry, who has really good hair. Governor Goodhair, or the Ken Doll (see, all Texans use nicknames it's not that odd), is not the sharpest knife in the drawer. But the chair of a major House committee says, Goodhair is much more engaged as governor than Bush was. As the refrain of the country song goes, O Please, Dear God, Not Another One. Molly Ivins August 6 Creators Syndicate , 2002 

During the grand old slugfest between incumbent Perry and the Democratic challenger, Tony Sanchez, Ivins highlighted Perry's misleading smear campaign against his opponent, as well as his eyebrow-raising deployment of the word coincidental : 

This, in turn, brings up the interesting role of coincidence in the life of Gov. Goodhair. Last summer, the Guv appointed an Enron executive to the state's Public Utilities Commission and, the next day, Perry got a check for $25,000 from Ken Lay. He explained this, to everyone's satisfaction, as being totally coincidental. The Not-So-Great Texas Gubernatorial Debate Creators Syndicate , 2006 

When the next gubernatorial election cycle came around in Texas, Ivins had even more to say and write about Perry's inadequacies. In her crosshairs this time: the faux-swagger that characterized his debating style: 

The Coiffure was in his usual form. As one opponent after another attacked his record, Gov. Rick Perry stood there proudly behind that 35 percent voter support he has so richly earned and simply disagreed. The Coiffure seemed to consider blanket denials a fully sufficient and adequate response. Molly Ivins: Molly can't say that about Rick Perry, can she? Sacramento Bee , August 13, 2011 

Over the weekend, the Sacramento Bee published its own collection of Ivins's editorial digs at Perry first printed in the Fort Worth Star-Telegram . Here is one excerpt, from an article originally published on January 12, 2006, in which Ivins rages against the governor's attempts at sleazy political sleight of hand: 

The governor of Texas is despicable. Of all the crass pandering, of all the gross political kowtowing to ignorance, we haven't seen anything this rank from Gov. Goodhair since gee, last fall. 

Then he was trying to draw attention away from his spectacular failure on public schools by convincing Texans that gay marriage was a horrible threat to us all. Now he's trying to disguise the fact that the schools are in free-fall by proposing that we teach creationism in biology classes. 

Now here's a droll, scathing one from June 24, 2001, on Perry's record on executing mentally handicapped inmates: 

First, we Texans would like to salute the only governor we've got, Rick Goodhair Perry, the Ken Doll, for vetoing the bill to outlaw executing the mentally retarded. 

We are Texas Proud. 

Such a brilliant decision not only is Texas now globally recognized for barbaric cruelty, but a strong majority of Texans themselves (73 percent) would prefer not to off the retarded. 

Gov. Goodhair's decision in the face of popular opinion, the Supreme Court and George W. Bush's recent conversion on this subject is a testament to his strength of character. 

Or something. 

His Perryness announced, anent the veto, that Texas does not execute the retarded. I beg your pardon, Governor. Johnny Paul Penry, now on Death Row for a heart-breaking murder and the subject of two Supreme Court decisions, has an IQ between 51 and 60, believes in Santa Claus and likes coloring books. 

And that's not counting the other six we know about for sure since 1990.
