Former Minnesota Gov. Tim Pawlenty finished a very distant third in the Ames Straw Poll on Saturday. He canceled an appearance on Hannity shortly after that, and now we know why: He told ABC's Jake Tapper on Sunday morning that he's dropping out of the Republican presidential race. 

The New Yorker 's Ryan Lizza tweets that the lesson here is that presidential candidates should just skip the straw poll entirely if they don't think they can win (obviously, that was a successful strategy for Mitt Romney, who did not drop out this morning). But at some point, whether it's in Ames, or later on at the caucuses, candidates do have to hit the stump and court voters and Pawlenty was a flop on that front. 

Here's Pawlenty's announcement: 



I saw the Pawlenty's problems up close on Wednesday when I watched him address a room of (mostly) undecided (mostly) senior citizens in Denison who were still smitten by Herman Cain's appearance two days earlier. The ex-governor sounded better on Friday at the Iowa State Fair when he was joined by his wife, Mary, but even then he drew maybe half as many folks as Michele Bachmann. Iowa voters followed the same logic John McCain did when he passed over T-Paw for the vice presidential slot in 2008: He's a safe bet and could get it done (to borrow a line from his stump speech), but you only get one vote, so why waste it? Bachmann captures today's conservative id in a way that Pawlenty never could, no matter how hard he tried. 

Tim Pawlenty will be fine, though he's finally free to grow another mullet . The real tragedy here has more to do with what Pawlenty did to position himself as a presidential candidate. Once he set his sights on the next level, he became a different kind of governor doing a 180 on climate change and leaving a famed Arctic explorer out in the cold ; denying gay couples hospital visitation rights; promising his support for an anti-bullying bill and then vetoing it. It's always tough to identify what politicians do for principle and what they do for their future prospects, but to the extent that ambition changed Pawlenty's politics, it was for the worse.
