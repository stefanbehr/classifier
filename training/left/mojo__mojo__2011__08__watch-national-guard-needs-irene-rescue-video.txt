You'd like to think that the National Guard is there to help in a natural disaster. But not everyone who wears the uniform is cut out for such work. Via Mark Thompson at Time's Battleland blog , here's an incredible amateur video of what appear to be two New Jersey National Guard heavy cargo trucks crossing the flooded JFK Boulevard in Manville, N.J., getting swamped, bailing, and calling the state troopers for a bailout: 



They look like they just might make it at first, plodding on with gusto in this suburban burg a half-hour south of New York. How is that possible? one bystander asks. A friend replies, It's the Army, bro. 

Except that it isn't possible. Note to Guard motor-pool trainers: Tell future recruits that, when in doubt, you don't plow your government-issue cargo truck through 10-foot floodwater. Especially if you can't swim. 

The absolute highlight: After the four soldiers flee their flooded cabs and reach safety atop their vehicles the soft laughter of civilian bystanders is audible in the foreground one soaked service member carefully shouts out a number for the others to call. It's the dispatch line for the New Jersey State Police headquarters on Route 22 in Bridgewater. Hopefully, the troopers had access to a boat.
