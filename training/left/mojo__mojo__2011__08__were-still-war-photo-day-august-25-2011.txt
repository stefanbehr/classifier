It's Raining Flares 

A C-130 Hercules from the Niagara Falls Air Reserve Station launches flares over Lake Ontario during a training exercise Aug. 10, 2011, in Niagara Falls, N.Y. Flares can be launched from an aircraft as a defensive measure against hostile forces. US Air Force photo by Staff Sgt. Joseph McKee.
