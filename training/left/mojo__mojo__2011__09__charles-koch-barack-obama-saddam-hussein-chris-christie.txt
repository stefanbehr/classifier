Unless you've been living in a hole, you've probably heard at least something about a secret confab near Vail, Colorado, where the billionaire industrialist Charles Koch referred to the 2012 elections as the mother of all wars. (He may or may not have been referring to President Obama when he evoked Saddam Hussein more on that below but he certainly used Saddam's battle slogan to characterize efforts by him and his brother to evict Obama from the White House.) 

In the week since we ran Brad Friedman's two-part series, which publicized audio from inside the big event and broke the news that New Jersey Gov. Chris Christie had delivered the keynote speech (a fact Christie had kept hidden from New Jersey voters), dozens of news outlets have picked up the story, and even taken it further. Here are a few highlights. 

UPDATES (September 20) : 

New Jersey senator calls for heads up whenever Christie's leaves the state: Yesterday, New Jersey Sen. Loretta Weinberg introduced a bill that would require the governor to give legislators one day's notice before leaving the state for any reason. The bill was prompted by Christie's secret trip to Colorado to speak at the Koch brothers' June fundraising seminar. I do not think the residents of New Jersey or any of us have a right to intrude on the governor s private time, vacation time with his family. So if he wants to say, 'I m going to Florida with my family,' that s sufficient, Weinberg told NJ.com . If he wants to say, 'I'm flying to Vail, Colorado, for a political meeting,' I think that that is good notice. 

It's all politics, Christie responded , according to the Associated Press. She'd love to be lieutenant governor and she's not. (In 2009, Weinberg ran against Christie on the ticket of former Gov. Jon Corzine, and she has previously criticized Christie for putting partisan events on his public schedule.) Earlier this month, Christie told reporters that they were not entitled to know everything I do. 

The AP also noted that, according to recently released public records, Lt. Gov. Kim Guadagno has formally assumed power for Christie 26 times since the pair was elected last year. Previous governors, the report added, have also spent considerable lengths of time outside the state. 

Gov. Christie drops anti-teacher rhetoric: More than once during his speech at the Koch seminar, New Jersey Gov. Chris Christie targeted state teachers' unions: We need to take on the teachers' union once and for all, and we need to decide who is determining our children's future, who is running this place. Them or us? I say it's us, he said. 

Last week, New Jersey Education Association spokesman Steve Wollmer told NewJerseyNewsroom.com that Christie had backed off on his tough talk. Everyone has noticed the intensity has dropped, Wollmer said. For one, polling data is driving the governor to behave better. He is seen as failing on education and that's not good. Wollmer also accused Christie of refusing to meet with the NJEA, a teachers' union. He is not interested in meeting with us, Wollmer said. He is interested in destroying the rights of organized labor. 

ORIGINAL POST 

Christie mentally deranged, says New Jersey Democratic Assembly leader: In the audio from his June 26 keynote speech, Christie boasts about backroom dealings with two state Democratic leaders Senate President Steve Sweeney and Assembly Speaker Sheila Oliver to pass a bill forcing public employees to pay more for their pensions. (Christie has called the pension overhaul his biggest governmental victory . ) I want to post the bill, but I think when I go on the floor, my own party's going to take a run at me to remove me as speaker. So I can't post the bill, Christie says Oliver told him. I think the only way I survive is if the 33 Republicans in the chamber will agree to vote for me for speaker. Can you work it out? 

After the audio broke, Oliver told the Newark Star-Ledger that Christie was more mentally deranged than some of us thought. Never happened. True or false, Christie's story led to speculation that Oliver could be ousted from her leadership role. But two state Democrats speaking on the condition of anonymity told the Cherry Hill Courier Post that reports of party infighting are overblown, and Oliver's position is safe. Her standing with Christie, whom she also called a rattlesnake , could prove more icy. 

In early July, Sweeney went ballistic on Christie, claiming the governor had double-crossed him on the pension deal by unilaterally using his line-item veto to slash services to the poor. Back then, the former union leader called the governor a rotten bastard and rotten prick and said he wanted to punch him in his head. He responded more coolly to the Koch seminar revelations, but speaking to the Asbury Park Press through a spokesman, he did manage another jab: 

The Senate President has no comment on remarks Governor Christie made while he was wining and dining with ridiculously wealthy people just days before he cut funding for visually impaired people, our most vulnerable seniors, and programs for sexually abused children, while coming to the aid, yet again, of his rich friends. 

Chris Christie's climate smoking gun : In his introduction of the governor, David Koch revealed how he and Christie had gotten acquainted: Five months ago we met in my New York City office and spoke, just the two of us, for about two hours on his objectives and successes in correcting many of the most serious problems of the New Jersey state government, Koch said. 

New Jersey's Sierra Club director Jeff Tittel told the AP that this was the smoking gun that shows [Christie has] been working with the Koch brothers from the beginning. The AP story suggests that the meeting may have influenced Christie's decision to withdraw from the Regional Greenhouse Gas Initiative (RGGI), a 10-state cap-and-trade program. In late May, after Christie announced his plan to exit RGGI by year's end, Tittel told Mother Jones that the governor was trying to have it both ways by supporting some environmental programs in New Jersey while appealing nationally to groups like the Kochs' Americans for Prosperity, a political advocacy group that stridently opposes efforts to regulate greenhouse gas emissions. A Christie spokesman told the AP that the winter meeting with David Koch was wholly unconnected to Christie's decision on RGGI. 

Saddam Hussein and Barack Obama: Part 1 of Friedman's exclusive opens like this: 

We have Saddam Hussein, declared billionaire industrialist Charles Koch, apparently referring to President Barack Obama as he welcomed hundreds of wealthy guests to the latest of the secret fundraising and strategy seminars he and his brother host twice a year. The 2012 elections, he warned, will be the mother of all wars. 

Reporters nationwide quickly picked up this quote, and broadcast hosts replayed the audio clip (which was included in the piece) on their shows. Friedman appeared on a number of radio, podcast , and TV programs, including CNN's Situation Room and MSNBC's The Ed Show , to discuss it: 





But some listeners, including our own Kevin Drum , suggested that Koch may have simply been quoting Hussein's well-worn slogan from the outset of the first Gulf War. Politico 's Ben Smith wrote: As I hear the (ambiguous) line, Koch is quoting Saddam here, not comparing Obama to him. In that version, the quote reads: 'We have, as Saddam Hussein [said] this is the Mother of All Wars.' A Koch Industry spokesman echoed that sentiment . But there's little doubt that the war in question is the war to retake the White House. The quote was all that Obama campaign manager Jim Messina needed to blast out a fundraising email , suggesting that it absolutely should offend Democrats. But it should also motivate you, because you are the only thing that can stop [t]he Koch brothers and the front groups they fund. 

Listen to Charles Koch's Saddam statement here: 





(The complete audio and transcript are available at The BRAD BLOG .) 

Koch ally Art Pope denies attending the seminar: No one on our list of likely million-dollar Koch donors has contacted us to protest their inclusion. But Raleigh, North Carolina, retail magnate Art Pope whom IndyWeek.com previously called a regular seminar attendee told the News Observer that he neither attended the seminar nor donated any of his own money. He did admit that his family's foundation donated several hundred thousand dollars, presumably in the past year, to Americans for Prosperity, where he serves as a national director . A Facing South investigation discovered that the foundation has given a total of more than $1.9 million to Americans for Prosperity. 

Here's a sampling of additional coverage: 

Newark Star-Ledger , NJ Gov. Chris Christie hurt himself by discrediting Assembly Speaker Sheila Oliver 

Philadelphia Inquirer , Editorial: Christie Should Disclose Secret Political Trips 

Milwaukee Journal-Sentinal , Menard, Hendricks on Koch donor list 

Colorado Independent , Why are the Kochs so afraid of Obama? 

Wonkette , War on Everyone Else Round One Million: Charles Koch Defeats Jimmy Hoffa 

Slate , The Koch Brother-Anna Nicole Smith Connection 

Greenpeace.org , Mother Jones Secret Koch Brothers Tapes 

Print Email Tweet We re Still at War: Photo of the Day for September 13, 2011 New Poverty Numbers Likely To Be Bad Gavin Aronsen 

Writing Fellow 

Gavin Aronsen is a writing fellow at Mother Jones . For more of his stories, click here . Follow him on Twitter or send an email to garonsen [at] motherjones [dot] com. RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Audio: Chris Christie Lets Loose at Secret Koch Brothers Confab 

In our exclusive recordings, David Koch introduces the New Jersey governor as "my kind of guy" and "a true political hero." Exclusive Audio: Inside the Koch Brothers Secret Seminar 

A close-up view of the oil billionaires' dark-money fundraiser and 2012 strategy session. Exclusive: The Koch Brothers Million-Dollar Donor Club 

The anonymous patrons who have given $1 million or more to the Koch dark-money machine. Koch, Obama, and Saddam Hussein 

Koch-Backed Group Buys $150K in TV Time for Wisconsin Ad Blitz 

Americans for Prosperity's latest ad buy brings its recall spending to more than $500,000. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
