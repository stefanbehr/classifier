Just in time for Labor Day, here's a handy illustration of how labor is getting shafted by Corporate America: 

Source: Jared Bernstein, Center for Budget and Policy Priorities 

The red line is the share of the economy taken up by wages, salaries etc. The blue line is the share taken up by corporate profits. So the graph shows that corporations have bounced back from the recession even as workers' share of the economic pie continues to narrow. 

Why is this happening? Jared Bernstein of the Center for Budget and Policy Priorities, who put together the graph, says companies have raked in the dough by selling into emerging markets while cutting costs through outsourcing and automating domestic jobs. I'd say the demise of unions certainly also plays a role . 

As the chart also makes clear, widening income inequality in America is no longer just a matter of stratified wages. Today's investment class the CEOs, the hedge fund managers, the bankers owns a stake in an economic system that no longer needs to share much of its wealth with anyone else. In other words, it takes money to make money. And of course, spending some of it to buy off Washington doesn't hurt either.
