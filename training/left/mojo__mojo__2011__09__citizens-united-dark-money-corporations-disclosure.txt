It's one of the most overlooked pieces of Supreme Court Justice Anthony Kennedy's now-famous opinion in last year's Citizens United decision : While Kennedy and the court's majority backed unlimited political spending by corporations, they also stressed the importance of disclosure ; that, in part thanks to the Internet, companies should disclose how much they spent and who they supported or attacked. Shareholders could then use those disclosures to determine the value of particular ads and gauge whether corporate political spending was in their best interests. 

That that disclosure hasn't happened. Nearly half of the money spent by outside political groups in the 2010 elections was dark money, meaning that the fundraisers themselves remained anonymous. 

But if that corporate spending helped clinch elections around the country, it also took something of a toll on the corporations themselves, according to a new report by Public Citizen and Harvard Law School . Looking at the political activity and market value of big, publicly-traded companies on the S P 500, researchers found that companies that were more involved in politics had weaker price/book ratios than less politically active companies. The study suggests that stock prices for politically active companies are lower than they would be if the same companies were less politically active. 

John Coates, a Harvard law and economics professor, found that, in every election cycle between 1998 and 2004, corporations who had more active political action committees (PACs) and more aggressive lobbying efforts had lower price/book ratios. Coates discovered an even stronger connection in the 2010 elections: in that cycle, politically active firms were 24 percent more undervalued than their peers. Even taking into account a number of variables recent profits, sales growth, size, leverage Coates spotted a correlation between political activity and lower company value. 

The report's authors also looked at the effect of disclosure: How does the market value of big, politically active companies with policies for disclosing political spending compare to active companies without them? Looking at 80 S P 500 corporations, the Public Citizen and Harvard team found that companies pledging to disclose their political spending had, on average, 7.5 percent higher price/book ratios than those keeping their spending in the dark. The takeaway here: Disclosing corporate spending doesn't hurt shareholders, and in fact might boost a company's market capitalization. 

There are, of course, plenty of caveats. For starters, the authors stress that they're not claiming a cause-and-effect here that either less political spending or more disclosure directly causes higher company value. They're just pointing out what they believe is an important connection. They also list off previous academic studies suggesting that corporate lobbying (and, to an extent, PAC giving) reaps benefits in the form of tax breaks, more favorable trade policy, and more. 

That in mind, this new research could allay fears that shining some sunlight on corporate spending will damage business. Many people would agree that disclosing political activities is the right thing for publicly-traded companies to do, Harvard's Coates said in a statement on Wednesday. Our study provides new evidence that it is also the thing that smart companies do. 

This post was edited for clarity after publication. 

Print Email Tweet Fox News Paranoid Alternate Universe Bachmann s Pastor Bashed Mormonism in 2007 Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Rove s Dark Money Group Targets 5 Senate Dems 

The Man Behind Citizens United Is Just Getting Started 

Meet the lawyer who could turn our elections upside down. Citizens United: The Shareholders Strike Back 

Forget Congress. It's investors who are battling against corporate dark money. Consumer Protection s Citizens United 

In a high stakes case over your right to sue for corporate abuses, guess which side the Chamber of Commerce is on? Citizens United After Eight Months 

Liberals should get out of their funk and support progressive causes. Corporations won't be. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
