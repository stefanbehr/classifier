The non sequiturs and talking points dished out by the GOP presidential candidates in Monday night's CNN-Tea Party Express debate were as perplexing as they were frequent. But almost as bizarre were some of the reactions from the tea party-heavy crowd in attendance. Consider two incidents from tonight's debate. Hungry for more debate coverage? Check out Andy Kroll on Rick Perry's Wall Street comments , Asawin Suebsaeng on Jon Huntsman's new strategy , or Tim Murphy on Rick Perry's sale price and Rick Perry's secret weapon . 

The first one came when CNN's Wolf Blitzer asked Rep. Ron Paul (R-Tex.) about a hypothetical situation involving a sick, 30-year-old man without health insurance. Here's the exchange , with video afterward: 

Blitzer : Let me ask you this hypothetical question. A healthy, 30-year-old man has a good job, makes a good living, but decides, You know what, I'm not going to spend $200 or $300 a month for health insurance because I'm healthy, I don't need it. Something terrible happens, all of a sudden he needs it. Who's going to pay if he goes into a coma, for example? 

Paul : In a society that you accept welfare-ism and socialism, he expects the government to take care of it. 

Blitzer : Well, what do you want? 

Paul : He should do whatever he wants to do, and assume responsibility for himself. My advice to him would be have a major medical policy. But not forced 

Blitzer : But he doesn't have that. And he needs intensive care for six months. Who pays? 

Paul : That's what freedom is all about. Taking your own risks. This whole idea that you have to prepare to take care of everybody. 

Blitzer : But congressman, are you saying that society should just let him die? 

Crowd : [Yeah! Yeah! Laughs.] 



Society should just let him die. Cheers. Wow. 

Then there was this exchange, a bit earlier in the night, concerning Rick Perry's comments that Federal Reserve chairman Ben Bernanke had engaged in treason: 

Blitzer : You stand by those remarks, Governor? 

Perry : I said that if you are allowing the Federal Reserve to be used for political purposes that it would be almost treasonous. I think that is a very clear statement of fact. 

Crowd : [Loud cheers, clapping.] 



This is the second GOP debate in a row where the audience has cheered at strange, if not disturbing, moments. Remember that during last week's NBC News- Politico debate, the crowd burst into applause at hearing that the Perry administration had executed 234 death row inmates, more than any other governor. The New Yorker 's Amy Davidson put it best in describing how troubling this is: 

Is justice some sort of slot machine that works best, in terms of wins, when it turns out the most bodies? The applause will likely be cited as an example of our national bloodthirstiness. That's not quite right, though; the truth is a little worse. Even a death-penalty supporter might be expected to remember that each execution is part of a story that involves the death of a victim, maybe more than one. For there to be a lot of executions, there have to have been a lot of murders and that can hardly be cause for happiness. But one suspects that, for this audience, death penalty had ceased to be anything but a political symbol a word disconnected from actual lives and deaths. It wouldn't be the only sign of detachment from reality in the debate. 

Who are these Republicans that are so tone deaf as to cheer executions and an uninsured man's death? Rest in peace, compassionate conservatism .
