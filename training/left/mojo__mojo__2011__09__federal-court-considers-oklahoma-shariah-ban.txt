On Monday, the 10 th Circuit Court of Appeals began hearings on a constitutional challenge to Oklahoma's ban on Shariah law. 

The amendment to the state's constitution was approved by popular vote more than 70 percent of Oklahomans voted for it. But although singling out Muslims for official disapproval might have been a workable strategy for getting out the vote, it may doom the law in court. Reuters reports : 

The panel gave no indication how it would rule, but at least one judge, Scott Matheson, asked why the measure was crafted to apply explicitly to just one religion. 

There's no mention of any other specific law, Matheson said in the hearing. We just have Sharia law singled out. 

Oklahoma Solicitor General Patrick Wyrick replied, The intent here was to exclude Sharia law and international law. 

Matheson asked, Why is there any need to mention Sharia law, to which Wyrick answered: To avoid confusion. 

Often questions from judges during oral arguments are less about getting information than making a point, and in this case Obama-appointee Matheson (the older brother of Utah Rep. Jim Matheson, a Democrat) seemed to be offering Wyrick a chance to explain the ban's obvious problems. Instead, Wyrick just reiterated that the law was targeting a particular religion, lest anyone get confused. That's unusual. Laws explicitly singling out certain religions are rare, precisely because the authors of those laws know that doing poses obvious constitutional problems that could lead to the measures being overturned. In previous cases like this, lawmakers have at least tried to pretend that there was a compelling public interest beyond simply curtailing the religious activities of one particular group. But thanks to the language of the amendment and the public statements of Oklahoma lawmakers, there's little doubt who the amendment was targeting. 

Oklahoma's Shariah law ban was called the Save Our State Amendment, but only in the most fevered imaginations is Tulsa in danger of being annexed by Islamic extremists. Instead, the ban will simply interfere with the ability of Muslim Oklahomans to execute wills and uphold business contracts drafted according to their religious beliefs. There is no danger of Shariah replacing American law. Where religious practices come into contact with civil law, the latter prevails.
