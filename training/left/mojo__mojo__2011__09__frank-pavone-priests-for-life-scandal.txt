The Catholic diocese of Amarillo, Texas, has suspended the leader of Priests for Life, Frank Pavone , from active ministry outside the diocese, citing concerns about his finances related to the anti-abortion group. Pavone, whose biography touts him as one of the most prominent pro-life leaders in the world, has run the group since 1993. 

My decision is the result of deep concerns regarding his stewardship over the finances of the Priests for Life (PFL) organization, wrote Bishop Patrick J. Zurek, in a letter to US bishops. The PFL has become a business that is quite lucrative which provides Father Pavone with financial independence from all legitimate ecclesiastical oversight. 

The Catholic News Service has more : 

Bishop Zurek said persistent questions and concerns from clergy and laity about how the millions of dollars in donations the organization has received are being spent led to the action. 

The bishop also asked Father Pavone, national director of Priests for Life, to return to Amarillo to spend time in prayer and reflection. 

In 2008 the last year that the group's tax forms are available online Priests for Life brought in $10.8 million income. While its 990 notes that Pavone made no income from the group, other top representatives were well compensated. The executive director made $95,394, and the vice chairman of the board made $162,253 that year. The group also spent $736,146 on travel some of which may have funded Pavone's efforts to promote the group. In 2007, he also tried to start a seminary in Amarillo but was unsuccessful . The bishop's letter notes that there have been persistent questions and concerns about how PFL is spending millions of dollars in donations. 

Pavone was perhaps best known nationally for his role as an advocate for keeping Terri Schiavo, the Florida woman in a vegetative state, alive. The group also protests euthanasia, in addition to abortion. The pro-choice Catholic group Catholics for Choice put out a report several years ago looking at Pavone and Priests for Life, noting his outsized role in the group: 

Pavone has always personalized the PFL message and image, selling himself often with large photos of himself on PFL billboards much as a candidate for office might do. 

On Monday, Pavone was in the news announcing the Vote Pro-life Coalition , which was to undertake a voter registration drive for the 2012 election. The election, Pavone told Life News , will be crucial to restoring protection to the unborn, and he pledged that every day between now and November 6, 2012, my colleagues and I will do all in our power to see to it that the elections advance the cause of life. 

Pavone says he will abide by the bishop's order, but intends to appeal, according to the Religion News Service .
