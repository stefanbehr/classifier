Two American hikers imprisoned by the Iranian regime since 2009 were finally released Wednesday. The prisoners, Joshua Fattal and Mother Jones contributor Shane Bauer , were released on $500,000 bail and turned over to a delegation comprised of Omani and Swiss officials. 

After 26 months behind bars, the hikers were freed after an Iranian judge who had already delayed their release twice signed a release order, according to the pair's attorney Masoud Shafii . The natural path has taken its course, Shafii told Reuters . As I had mentioned before, I was waiting for a signature. This has now happened. 

Shortly after the hikers' release was confirmed , the government of Oman announced that the hikers had been handed over to Dr. Salem Al Ismaily, the envoy of Qaboos bin Said, Sultan of Oman. Dr. Al Ismaily with the hikers are now on their way to Muscat where they will spend a couple of days before heading home, Oman's envoy in Iran said in a statement. Fattal and Bauer's families are waiting to reunite with them in Oman, an unnamed official told CNN , where the two men will likely receive medical examination before returning to the United States. 

Here is Reuters ' coverage of the news: 



The hikers' release comes eight days after Iranian President Mahmoud Ahmadinejad announced that the prisoners would be granted a unilateral pardon as a humanitarian gesture. Fattal and Bauer were released before Ahmadinejad embarks on his annual media blitz in advance of his visit to New York and the UN General Assembly. 

Bauer, Fattal, and a third hiker, Sarah Shourd , were arrested in July 2009 while hiking along the border between Iran and Iraqi Kurdistan. Theories on how exactly the arrest took place vary. The three Americans may have crossed the border into Iran by accident, but some believe they were abducted by Iranian forces while in northern Iraq . Shourd, who accepted Bauer's marriage proposal while the two were imprisoned, was released in September 2010 after family and supporters forked over $500,000 in bail money. But Fattal and Bauer remained in detention, and last month, the two men were sentenced to eight years in prison on charges of espionage. Prosecutors did not present any credible evidence that the hikers were American spies or government operatives, and President Obama said in a July 2010 statement that Sarah, Shane, and Josh have never worked for the US Government. 

Follow the developing story on Twitter via the hashtag #ssj .
