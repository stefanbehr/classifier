Lost in the build-up to President Obama's big jobs speech Thursday night was a bomb of an announcement, first reported by Peter Stone of iWatch News , from American Crossroads and Crossroads GPS , the conservative independent expenditure groups that are two of the heaviest hitters in the political money game . Founded in 2010 with help from Bush guru Karl Rove, the Crossroads groups are now trumpeting a new fundraising target to double their planned haul of $120 million for the 2012 elections. Yes, you read that right: the Crossroads groups say they will raise a whopping $240 million to vanquish President Obama, help GOPers win the Senate majority, and strengthen their House majority. 

Here's some context for that $240 million target. In the 2010 election cycle, American Crossroads, a super PAC that discloses its donors, and Crossroads GPS, a 501(c)4 that doesn't disclose, spent an estimated $43 million, according to the Center for Responsive Politics . That same cycle, all outside spending groups, excluding party-affiliated committees, spent just over $300 million. 

To help them reach their eye-popping fundraising goal, the Crossroads groups have tapped one of the most successful GOP fundraisers of them all, Mississippi Gov. Haley Barbour . A former DC lobbyist, Barbour helped rake in a record-setting $115 million as chair of the Republican Governors Association from 2009 to 2010. 

Both Governor Barbour and Karl Rove are prodigious fundraisers and brilliant strategists, and we are honored to have them both engaged with us, Steven Law, president of both American Crossroads and Crossroads GPS, said in a statement. We are reaching high in our fundraising goals because we believe this is going to be a destiny-shaping election for our country. 

Michael Beckel, a spokesman for the Center for Responsive Politics (and a Mother Jones alum), a non-partisan group that tracks money in American politics, described American Crossroads as the top dog in the 2010 midterm elections. He said that Crossroads' new goal would blow past anything seen two years ago. It looks like this arms race is ramping rather quickly. 

Print Email Tweet Mixed Bag in South Dakota Abortion Script Decision Obama Getting Close To One Million Deportations Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Rove s Shadowy Crossroads GPS Plans $20 Million Ad Assault on Obama 

Crossroads GPS s New Target: Health Care Waivers 

Karl Rove's secretly funded political group has joined a spurious attack against federal health reform. The Left s American Crossroads? 

Democrats are gearing up for the 2012 shadow spending wars. The Audacious Transparency Hypocrisy of Rove s Crossroads GPS 

The conservative dark-money group gets into the transparency game. Seriously?! American Crossroads Sugar Daddies 

Meet the folks bankrolling Karl Rove's political powerhouse. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
