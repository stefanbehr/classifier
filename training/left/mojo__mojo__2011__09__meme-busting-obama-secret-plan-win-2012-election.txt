Rep. Mike Coffman (R-Colo.) believes he's uncovered President Obama's secret plan to win next fall's presidential election: Grant citizenship to millions of undocumented residents with the expectation that they'll check his name at the ballot box come 2012. He's not alone; Rep. Louie Gohmert floated a similar conspiracy theory last week, telling Fox News that Obama would attempt to steal the election through some combination of massive voter fraud and blanket amnesty. 

Here's what Coffman told Denver's Caplis Silverman radio show last month: 

There's another piece of this puzzle. What the Administration is doing, is taking a very aggressive move in the people that have illegal status and moving them through citizenship and waving all the fees and waving anything they can to get the process done in time for 2012. That's something I would love to see the media focus on. 

Mercy! Expect to hear a lot more talk like this over the next 12 months, as right-wing media outlets shift into overdrive in the run-up to the election. The same thing transpired with ACORN in 2008 when Sen. John McCain (R-Ariz.) famously declared that we are on the verge of maybe perpetrating one of the greatest frauds in voter history in this country, maybe destroying the fabric of democracy. (We're still here.) 



But is there anything to it? Jason Salzman, a Colorado blogger, took Coffman's suggestion and looked into it . The short answer is no. That's the longer answer, too. 

As Salzman notes, undocumented immigrants can't be moved through the citizenship process, because a prerequisite of applying for citizenship is that you have to be living here legally. The number of fee waivers that have been granted have increased, but again, there's no way to grant a fee waiver to someone who isn't a lawful resident so that's kind of moot; the change is due to the fact that there wasn't previously an easy way to apply. And most crucially, there hasn't actually been an increase in the number of naturalized citizens: 

The increased number of fee waivers does not translate into more people actually becoming U.S. citizens with voting rights. The total number of people who went through the naturalization process has decreased during the Obama Administration (about 676,000 in FY 2010, about 744,000 in FY 2009, about 1,047,000 in FY 2008, 660,000 in FY 2007, and 702,000 in FY 2006), according to [United States Citizenship and Immigration Services] data. 

If President Obama is secretly granting citizenship to undocumented residents, he's doing a really terrible job. The larger issue here for Coffman, though, isn't really about immigrants, it's about voter turnout he's also sponsored legislation to repeal the portion of the Voting Rights Act requiring that ballots be printed in languages other than English. For that, I'd recommend Ari Berman's excellent Rolling Stone piece on the GOP's War on Voting .
