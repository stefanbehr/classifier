Rep. Michele Bachmann (R-Minn.) has a habit of breaking out Old Testament references when she speaks to religious audiences. She has previously compared her (increasingly) small but determined band of followers to the Men of Issachar, one of the 12 Tribes of Israel that helped ward off the Canaanite invasion in the Book of Judges. Like Bachmann's teavangelicals, the Men of Issachar were reclaiming their godly inheritance after the Israelites had lost their way. It's also one of the only instances in the Bible in which men are led into battle by a woman Deborah . 

But now, with her presidential campaign on the ropes, the Minnesota congresswoman seems to have picked a new Biblical alter ego; we're not sure she really thought this one through. Via MinnPost , this is what she told the conservative activists at RightOnline last week in June: 

I want to call to mind in remembrance a hero of mine. And he's from ancient Israel. And from history we know, in the recorded annals of time, that this was someone considered more inconsequential, but to me he had an inspiring, powerful story. 

His name was Jonathan. And it was in ancient Israel. His father was king. He was the first king of ancient Israel and his name was King Saul. 

And there was another battle that Israel faced. And that battle was with a group of people called the Philistines. And the Philistines had a position of power. And they were up on a cliff during the time of this battle. 

Then, as the story goes, Jonathan and a comrade scaled the cliff and defeated the entire Philistine army by themselves. 

If the story ended there, that would be a fantastic metaphor for what Bachmann would like to accomplish as a candidate. The story of Jonathan does not end there, however. Given his lineage and heroics in battle, Jonathan was considered the front-runner to become the next head of state. But another, more charismatic (literally) rural farm boy came along and won over the base by killing a coyote with a Rueger killing a giant with a slingshot. Jonathan and Saul died tragically in battle on top of Mount Gilboa; David became king. 

Goliath's severed head can only watch. Wikimedia Commons But there's another twist: Jonathan's mostly famous because of his very close personal relationship with David, with some scholars going so far as to suggest that they might have been lovers. Jonathan and David have been cited by gay rights activists as proof that gay rights are biblically enshrined, as well as by Oscar Wilde at his trial for homosexuality. The Book of Samuel describes the relationship thusly: The soul of Jonathan was knit with the soul of David, and Jonathan loved him as his own soul. Their friendship led to a falling out between Saul and his son, after Jonathan pleads with the king to stop trying to kill David. 

So were they actually lovers? It's hardly the mainstream view, but it's a theory that's out there: Dartmouth religion professor Susan Ackerman wrote a book about it . Here's how she synthesizes the Jonathan and David were gay (or at least bisexual) argument, seizing on the pair's falling out with Jonathan's father: 

[A]s Schroer and Staubli particularly argue, the language Saul uses in his diatribe is extremely sexually charged, so much so that we may be meant to interpret it also in sexual terms; that is, to understand this charged language is used in Saul's insults because Saul perceives his son's misdeeds to be sexual as well as political. According, then, to Schroer and Staubli, Jonathan has not only engaged in the political scandal of a royal son betraying father and kingdom for the sake of a stranger, but also the effrontery of homosexual love. 

Bachmann, for her part, has described homosexuality as personal bondage and a dysfunction and alleged that gay marriage is an earthquake issue that could shake American society to its core. Given the way her campaign gone since Texas Gov. Rick Perry entered the race, though, perhaps Bachmann should have just gone with Job . 

Front page image: Wikimedia ; Davie Hinshaw/Charlotte Observer/Zuma
