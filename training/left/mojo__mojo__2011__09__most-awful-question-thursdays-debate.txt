One particularly telling moment at Thursday night's Republican presidential debate came from co-host and Fox News anchor Megyn Kelly. Speaking to Rep. Ron Paul (R-Texas), Kelly repeated a common, if medically inaccurate, anti-abortion talking point: 

Congressman Paul, you have said that you believe that life begins at conception and that abortion ends an innocent life. If you believe that, how can you support a rape exception to abortion bans, and how can you support the morning-after pill? Aren't those lives just as innocent? 

Let's leave aside the suggestion that we should force women to carry the children of rapists to term and just deal with the premise that using the morning-after pill, or plan B, constitutes taking an innocent life. Well, no, not based on any medical definition of pregnancy. Pregnancy doesn't begin until the fertilized egg implants in a woman's uterus. This is exactly what the pill is designed to prevent. Thus, no pregnancy to end. 

Let's let the Mayo Clinic explain : 

Depending on where you are in your menstrual cycle, the morning-after pill can prevent or delay ovulation, block fertilization, or keep a fertilized egg from implanting in the uterus. 

If that's the case, then by Kelly's standard, any kind of birth control or even just having your period could constitute abortion, too.
