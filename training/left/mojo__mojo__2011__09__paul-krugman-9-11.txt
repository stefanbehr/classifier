On the tenth anniversary of the brutal 9/11 attacks on the United States, Paul Krugman, in a short but scathing New York Times blog post, elected to use the occasion to remind us of those who have used the event to begin unnecessary wars and further their political careers. 

Under the title, The Years of Shame , Krugman wrote: 

What happened after 9/11 and I think even people on the right know this, whether they admit it or not was deeply shameful. The atrocity should have been a unifying event, but instead it became a wedge issue. Fake heroes like Bernie Kerik, Rudy Giuliani, and, yes, George W. Bush raced to cash in on the horror. And then the attack was used to justify an unrelated war the neocons wanted to fight, for all the wrong reasons. 

I completely agree with the substance of what Krugman had to say in his post. 

I also completely deplore his choice of posting his piece on the ten-year anniversary of the attacks. 

Yesterday's commemoration was about letting those who lost fathers and mothers, sons and daughters, sisters and brothers, know that those of us who were more fortunate have not forgotten them. It was about remembering the extraordinary bravery of first responders who put their lives squarely on the line in the attempt to save their fellow countrymen, many of those lives being lost in the effort. 

It was decidedly not a day that needed to be about politics. 

On a day when Americans of all stripes should have been giving thanks to both President Bush and President Obama for doing whatever it is they do that has protected us from a tragic repeat of the events of September 11, 2001, even if you deeply object to things they have done in the days and years that followed, Krugman used the opportunity to commemorate what has gone wrong since 9-11. 

One might have hoped that Mr. Krugman would have displayed a deeper sensitivity and understanding of the meaning of yesterday's events just as one would have hoped that Krugman might understand that, while there is a time and place for everything, September 11, 2011 was not the right time for his piece. 

Nick Greene summed it up nicely in yesterday's Village Voice : 

The opinions he quickly formulates may very well be valid ones, but everyone will be too baffled by his inappropriate brevity and callous timing to carefully weigh them. He knows this, as signaled by his refusal to allow comments. 

Today, the conservative blogosphere is going crazy condemning Krugman's article and I don't blame them one bit. Indeed, the only thing I do not understand is why more progressives are not joining in that condemnation, as Krugman's piece only serves to set back the principles and causes of liberals and progressives everywhere. By forgetting what is important about yesterday's commemorations, Krugman has played right into the hands of those who would use such a reaction to tar the true intent of liberal commentators and, by extention, every other progressive in the country. 

Paul Krugman very much owes the families of 9/11 victims and the first responders who survived an apology. I hope he'll take the opportunity to offer that apology and acknowledge his error, as failing to do so makes all who oppose the right-wing agenda look bad.
