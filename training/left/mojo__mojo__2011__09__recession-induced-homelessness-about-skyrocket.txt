There's more dismal news on the economic front. Earlier this month, the US Census Bureau released the latest poverty data , revealing that the poverty rate is at a record high and the number of Americans living in deep poverty has been steadily increasing. (The Census Bureau defines deep poverty as living below half the annual federal poverty line, or about $11,000 for a family of four. About 7 percent of the country now falls into this category.) 

Now comes the news that as larger numbers of people fall into deep poverty, they're increasingly landing on the streets. The National Alliance to End Homelessness projects that the number of homeless Americans will increase by five percent over the next three years. That would mean an additional 74,000 people homeless people, pushing the national total towards 1.7 million. Homeless numbers tend to lag behind unemployment and poverty indicators, but the Alliance notes that all the warning signs for increased homelessness are there most notably an 11 percent increase in the number of people who are doubling up and living with relatives or other people. That's often just one step from landing in a shelter. Here's the depressing chart of the day: 



Chart courtesy of the National Alliance to End Homelessness
