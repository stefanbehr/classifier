Each morning, the Twitter account @RecallWalkerBot announces the number of days until Wisconsin Gov. Scott Walker can legally be recalled. As of today, the count stands at 96 days. But before that countdown hits zero, GOP legislators in Wisconsin are doing their best to give Walker more power over the recall process and make it harder for recall organizers to gather the signatures needed to trigger a recall election. 

As the Milwaukee Journal Sentinel reports , the move by Wisconsin GOPers, who control the state Assembly and Senate, would allow Walker to block a policy by the state's non-partisan Government Accountability Board that allows organizers to collect signatures in a recall effort through online forms as opposed to the usual on-the-ground, in-person mode of signature gathering. Democrats see the effort as a brazen power grab aimed at blunting any effort to unseat Walker once he's recall eligible in January 2012. You have given the governor control of the chicken coop, so to say, said state Sen. Lena Taylor (D). 

Here's more from the Journal Sentinel : 

The Government Accountability Board, which oversees state elections, adopted policies this month about recall petitions and what student IDs can be used for voting. 

Legislative leaders raised concerns about those procedures, and on Tuesday the Joint Committee for Review of Administrative Rules took testimony from Kevin Kennedy, director of the accountability board. The co-chairs of the committee, Sen. Leah Vukmir (R-Wauwatosa) and Rep. Jim Ott (R-Mequon), expressed skepticism of the policies and said they would likely ask the accountability board to adopt them as administrative rules. 

Walker would have to sign off on such rules, and if he declined to do so, he could stop them entirely. If Walker approved them, the rules would then go before the committee, which could eventually block them, approve them or ask for modifications. 

It's still unclear if Walker foes plan to mount a recall effort or not. They would need to gather about 540,000 signatures in a 60-day window to prompt a recall election. Although this summer's recall elections targeting nine state senators were a win on paper for unions and Democrats, their failure to reclaim the state Senate majority arguably slowed their momentum and raised questions about the wisdom of a statewide recall effort against Walker. There's also the question of whether Walker's opponents will try to recall him right away and possibly tee up a recall election during the GOP presidential primary season or wait until later in the year so that the recall coincides with the November general election. 

All these questions, not to mention the GOP's meddling with the recall process, hang over Wisconsin progressives. Then again, they've got 96 days and counting to figure it out.
