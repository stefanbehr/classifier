A soldier assigned to US Forces Afghanistan maintains security and returns fire as rain falls during an attack in Kabul, Afghanistan, Sept. 13, 2011. Photo by the US Army.
