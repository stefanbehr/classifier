GOP presidential candidate Herman Cain was having a pretty good week. On Tuesday, three polls from Public Policy showed the businessman/gospel singer in the lead in North Carolina, Nebraska, and West Virginia. Another recent poll had Cain trailing only Mitt Romney in the key Florida primary. Then, on Wednesday, he put his foot squarely in his mouth in an interview with the Wall Street Journal . Here's what Cain said when asked about the #OccupyWallStreet movement: 

I don't have facts to back this up, but I happen to believe that these demonstrations are planned and orchestrated to distract from the failed policies of the Obama administration. Don't blame Wall Street, don't blame the big banks, if you don't have a job and you're not rich, blame yourself! It is not someone s fault if they succeeded. 

Cain added that the banks did have something to do with the crisis in 2008, but we're not in 2008, we're in 2011! Okay? 



To put it bluntly: Cain really doesn't have any facts to back him up. The protests were initially organized by Adbusters , which is hardly an organ of the Obama administration. As my colleague Andy Kroll reported this morning , organized labor has made a push to get behind the movement, but they're piggybacking on a movement that has already taken off. 

This is not the first time Cain has found himself on the wrong side of facts. Citing debunked conspiracy theorists , he alleged that Islamic Sharia law was already being forced on American courts in Oklahoma and Texas (he meant Florida), and despite touting himself as a constitutionalist, argued that the First Amendment does not apply to the Muslims of Murfreesboro, Tennessee. (Cain has since apologized, and then denied that he changed his position.) 

Cain's not the only Republican candidate to weigh in on the #OccupyWallStreet: On Tuesday, Mitt Romney called the protests dangerous and class warfare. 

h/t Think Progress . 

Do you appreciate fair and factual reporting on Occupy Wall Street? Please donate a few bucks to help us expand our coverage .
