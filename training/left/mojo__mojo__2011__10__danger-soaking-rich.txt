This week, top Democrats including President Obama and Senate Majority Leader Harry Reid are locked into a full-court press, calling for a 5.6 percent surtax on millionaires to help pay for the president's $445 billion jobs bill. In the week that is/was Occupy Wall Street , that sounds like a can't-miss political winner. 

But the Tax Policy Center's Howard Gleckman says , Hey, slow down! While the surtax would raise the average tax bill of millionaires by some $110,000, the fact is that there simply aren't enough millionaires to actually solve the country's fiscal problems. If we are going to get serious about the deficit, people making $200,000 (or even $100,000) have got to help out, Gleckman suggests. 

Gleckman's chief concern: That the surtax will torpedo any realistic effort at long-term tax reform. Thanks to tax changes in the offing the expected expiration of the Bush tax cuts, which will spike the top rate to 39.6 and curb a number of exemptions, along with a .9 percent surtax on expensive plans as part of health care reform the top marginal tax rate for millionaires would climb to nearly 50 percent by 2013. And according to the current schedule, the tax rate on capital gains will also almost double by 2013. 

So what's the problem? 

With rates this high, the political pressure to protect tax preferences will be enormous. After all, the rich are going to fight much harder to protect breaks that are worth 50 cents on the dollar than one that is worth only 35 cents. And I stupidly thought the idea was to lower rates and eliminate these subsidies. 

Reid wants to be able to say that Republicans blocked a critical jobs bill just to protect their fat-cat millionaire pals. Give him credit for smart politics: By replacing the potpourri of tax increases Obama would have used to pay for his stimulus bill with a simple, easy to understand millionaire tax, the Senate Democratic leader has done a wonderful job clarifying his party s message. 

Gleckman's complaint here is that the surtax, while good politics, spells trouble for long-term tax policy. 

But Gleckman's operating in a universe where broad-based tax reform lowering tax rates and curbing deductions seems somehow politically feasible in the near or even the long term. That's not really the case: Obama's jobs bill , with the surtax (and quite possibly without), doesn't have a shot. So policy-wise, he's backed into a corner. But if the political play one that Democrats and Republicans around the country are clamoring for is the only one you've got, then it's the one you make. With popular opinion still on his side when it comes to making the rich pay their fair share , why not exploit the advantage?
