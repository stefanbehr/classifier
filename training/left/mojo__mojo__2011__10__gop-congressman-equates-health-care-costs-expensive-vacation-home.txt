Just when you thought it could not get more ridiculous, GOP Congressman and Chairman of the House Appropriations Labor-Health and Human Services subcommittee, Denny Rehberg, has come up with a novel idea. He wants the Congressional super committee to solve $1.2 trillion in deficit reduction by simply killing off the expansion of Medicaid and the subsidies that will open the door to health care for millions of Americans. 

In making his argument, Rehberg noted that expanding the Medicaid safety net program, and providing subsidies to low and middle class workers, is akin to the expensive vacation home that the average American would choose not to buy if that American was facing a deficit as serious as the nation's. 

Before getting to the heart of Rehberg's suggestion, one can't help but wonder what makes the Congressman think that the average American can afford an expensive vacation home (or any vacation home for that matter) on what the average American earns, even if that American is not in debt? 

But should we be surprised by the Congressman's view of the world? This is the same Denny Rehberg who is not only listed as number 23 on the list of the wealthiest members of Congress , but is the same Congressman Rehberg who had no idea what the minimum wage was in his own state (check out this video as it is priceless.) 

Of course, far more important is Rehberg's inability to grasp that getting treatment for cancer or unblocking that clogged artery that is going to make someone a widow or widower is not quite the same as purchasing a vacation home expensive or otherwise. 

And while life might not be worth living for Rep. Rehberg and friends without that idyllic home on the lake, the average American would still prefer to remain alive, thank you very much, which is precisely why Medicaid coverage was extended to more people and subsidies are to be made available to the working poor and middle-class so that medical care will become an option in their lives. 

When asked how low and middle class Americans will manage to purchase health care, should the mandate requiring them to do so be found to be Constitutional by SCOTUS, Rehberg answered that Health and Human Services would be able to grant waivers to those who cannot afford coverage without Medicaid or subsidies. 

Thus, Rehberg's solution is to simply leave millions of Americans without coverage by way of a waiver. Nice. 

Health Care For America Now's Executive Director, Ethan Rome, put it this way: 

Rep. Rehberg's proposal is yet another part of the Republican assault on the middle class. Denny Rehberg says that basic health care is a luxury item, as if a mother in Montana taking her children to the doctor or a cancer patient getting treatment is the same as buying 'an expensive vacation home.' 

Considering that estimates place the uninsured under age 65 in Montana at somewhere between 16 percent and 20 percent of the population , a number well in excess of the national average, I suspect that Rehberg's fellow Montanans might disagree with his approach. 

Let's hope they voice that disagreement at the ballot box next November.
