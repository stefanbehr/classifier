They're wrong. 

That was front-runner Herman Cain's short and sweet defense against critics who said his 9-9-9 tax reform plan would hike taxes on the working and middle classes during Tuesday's GOP presidential debate . Cain's plan would wipe out the current federal tax code and replace it with a 9 percent sales tax, a 9 percent corporate business tax, and a 9 percent income tax. Cain took plenty of heat in the debate after multiple analyses of his 9-9-9 plan this one by the Tax Policy Center is eye-opening found it would dramatically increase taxes for the working and middle classes while dramatically slashing taxes for the wealthy. 

Or, in chart form (via Kevin Drum ): 

Tax Policy Center 

But Cain repeatedly insisted that his critics and the outside analyses were wrong. The thing that I would encourage people to do before they engage in this knee-jerk reaction is read our analysis. It is available at hermancain.com, he said. His plan, he went on, is a jobs plan, it is revenue-neutral, it does not raise taxes on those that are making the least. 

Here's the problem: The analysis ( PDF ) on Cain's website doesn't support what he's saying. After reading it I called the group who conducted the analysis, northern Virginia-based Fiscal Associates, but no one answered; if they call back I'll update accordingly. [ Updated: see below. ] Ezra Klein read the Fiscal Associates analysis, too, and had this to say : 

Somewhat oddly, the analysis (pdf) Cain posted from [Fiscal Associates] has the word 'draft' emblazoned on the bottom of every page. Confidence inspiring stuff. But even the draft analysis doesn't tell us much. What we need to know to decide whether the plan will raise taxes on those making the least is what tax wonks call 'a distributional estimate' an estimate of what different income groups will pay under the new proposal. There's no such estimate in the Fiscal Associates Draft. 

To be clear, it's not that the analysis confirms or debunks Cain's claim that the 9-9-9 plan won't jack up taxes on the poor and middle class. The analysis doesn't even say what the impact will be. Which begs the question: Did Cain even read the analysis before citing it to defend the 9-9-9 plan on national TV? I emailed the Cain campaign this afternoon for clarification but have yet to get a response. 

[ UPDATE : This afternoon, I spoke with Gary Robbins, a former tax expert at the Treasury Department who now runs Fiscal Associates and who the Cain campaign hired to analyze the 9-9-9 plan. Robbins confirmed that his analysis, contrary to what Cain says, does not look at how the 9-9-9 plan would impact tax rates for low- and middle-income earners. I wasn't asked to do a distributional analysis, he says. Rather I was asked to look at whether it was revenue-neutral, which it is. Robbins says he doesn't necessarily agree with the Tax Policy Center's analysis, but adds that he needs to look at their methodology before making a conclusion about the plan's effects.]
