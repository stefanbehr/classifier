On Thursday, I defended Herman Cain from the trumped-up charges that he is actually, secretly, pro-choice. Cain had answered a question, from CNN's Piers Morgan, about whether he supported exceptions for rape and the life of the mother, and said that while he opposed abortion even in those circumstances, it wasn't the government's job to tell women what to do. That's not the kind of thing that would endear Cain to NARAL; it's the same position taken by noted reproductive rights icon George W. Bush, among others. Even the activists behind the Mississippi Personhood amendment say that a pregnant woman should be allowed to get an abortion if her life is at risk even if the actual language of the amendment wouldn't allow for it. 

Besides, Cain has a pretty consistent track record of condemning abortion. In March, he called Planned Parethood Planned Genocide , alleging that the nation's largest abortion provider was deliberately targeting blacks for extermination. In 2006, he ran a radio ad campaign targeting Democrats for their support for abortion rights, and proudly noting that the Republican Party calls for the repeal of Roe v. Wade in its official platform. And in 2004, when he was running for Senate in Georgia, he put out the following statement , on the anniversary of Roe , which should eliminate any remaining confusion on where he stands: 

Today we mourn the murder of millions of innocent lives because of the decision made 31 years ago by the United States Supreme Court to give doctors the right to end the life of an unborn child. Unbelievably, the decision of Roe v. Wade shows that our Nation still chooses to place human convenience over the sanctity of human life. 

No great nation can prosper when life is not valued. Once the Supreme Court decided that the right to life was obsolete, the very basis of our country was lost. A civilized society is marked by its treatment of the defenseless. When the most innocent of human life, the life of an unborn child, can be ended for the sake of convenience, the moral fabric of our society is destroyed... 

Infanticide will continue as long as our society accepts the lie that a mother has the right to terminate a child s life. These women are sadly comforted by the blanket of Roe v. Wade. But there is still hope. If people today will continue to unite together to fight against legalized abortion and make their voices heard, we can make a difference. Life needs to be respected and celebrated, not terminated. 

Infanticide is really not a word that gets thrown around a lot by actual pro-choicers. 

What's really happening here is that conservatives are starting to discover what everyone else has realized for a while now: Herman Cain is really bad at answering very simple questions. Partly, that's because Cain has a poor grasp of public policy and tends to rely on pretty dubious sources for information. But it's also a consequence of his inexperience he lacks the encylcopedic arsenal of canned responses that are required of every successful candidate. And so he ends up stammering through incredibly awkward segments like this one, with Fox News' John Stossel in July, in which he simultaneously conveys his key point Abortion should not be legal! I believe in the sanctity of life! while literally leaving the other panelist speechless, about 55 seconds in: 



In case you missed that, this was Newsday 's Ellis Henican's reaction to Cain's discussion of abortion. His mouth is open, but no words are coming out: 

Herman Cain has the rare ability to render a cable news pundit speechless.: Fox News/YouTube h/t Daily Intel .
