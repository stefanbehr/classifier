In the early morning hours on Tuesday, Boston police officers clashed with Occupy Boston protesters demonstrating in the Rose Fitzgerald Kennedy Greenway. Police had surrounded the park and demanded that the protesters leave, but after they did not, law enforcement arrested nearly 100 people. 

Among those protesting was a contingent from Vets for Peace, an anti-war organization made up of US veterans. The following video shows police dragging protesters to the ground and hand-cuffing them with plastic flexi-cuffs. You can see an American flag knocked to the ground during the scuffle, and Boston police also collected crumpled tents, signs, and other materials and tossed them into nearby garbage trucks. 

Here's the video : 



Boston wasn't the only site of conflict in recent days. In Des Moines, activists defying an 11 p.m. curfew to continue their own Occupy protest were pepper-sprayed and arrested by police. Read more about that confrontation, including video of the arrests, here . 

A key point to bear in mind: As Nate Silver points out , police clashes like those in Boston and Des Moines result in more press coverage, and so more momentum, for the Occupy movement. In the case of Occupy Wall Street specifically, Silver found, news coverage jumped after clashes between protesters and New York cops, including the use of pepper-spray and mass arrests of protesters on the Brooklyn Bridge. Given widespread coverage of the Boston arrests , it's likely the same effect will play out in that city as well. 

Do you appreciate fair and factual reporting on Occupy Wall Street? Please donate a few bucks to help us expand our coverage .
