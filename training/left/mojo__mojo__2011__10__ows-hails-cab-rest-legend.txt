After a night of arrests, endless meetings, and hipster cops, the guys manning the Occupy Wall Street livestream finally decided they'd had enough of hoofing it from Washington Square Park all the way back to Wall Street. They caught a cab, camera still rolling. That's when they met Khan from Pakistan, and the rest is TAXI CAB MAGIC: 

Watch live streaming video from occupywallstnyc at livestream.com 

Do you appreciate fair and factual reporting on Occupy Wall Street? Please donate a few bucks to help us expand our coverage .
