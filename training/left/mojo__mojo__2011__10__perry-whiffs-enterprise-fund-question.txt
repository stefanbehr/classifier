At Tuesday night's debate, Gov. Rick Perry was asked about the Texas Enterprise Fund (TEF), one of his key job creation initiatives. Perry boasted about the 54,600 jobs the program supposedly created, and the $14 billion-plus in capital investment it has brought to Texas. (The fund works by giving companies taxpayer money in exchange for them agreeing to add jobs in Texas, usually by moving their operations there.) 

Perry didn't really explain why some of the recipients of fat grants and huge tax breaks are some of his biggest donors, and it's still not clear how many jobs TEF really created. As I reported back in September : 

In January 2010, Perry's office claimed that TEF had created 54,600 jobs since it began in 2003. But company-reported data shows that, by the end of 2009, fewer than 23,000 jobs could be attributed to TEF. And two-thirds of TEF-backed companies failed to meet their job targets. The program handed out nearly $440 million during that period. . . . 

The program also suffers from a conspicuous lack of transparency, according to Andrew Wheat, the research director for [Texans for Public Justice]. Wheat says that his colleagues intended to use government data from 2010 to assess the program, but they were forced to rely on numbers from 2009. We put in a request to get that data covering 2010, he said. We're still waiting. . . . 

Don Baylor, a senior policy analyst at the Center for Public Policy Priorities, says there is no way to verify Perry's claims more broadly. It's quite simply not possible to know whether these companies would have come to Texas without the cash, Baylor says. 

There's just no way of knowing whether all the money Perry spent on drawing companies to Texas actually created any jobs. So the jury should still be out on that Texas miracle.
