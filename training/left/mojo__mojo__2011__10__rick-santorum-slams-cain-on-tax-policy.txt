Since when did Rick Santorum become the champion of the middle class? At Tuesday's GOP presidential debate in Las Vegas (the eighth in four months, if you're scoring at home), the former Pennsylvania senator led the charge against newly crowned front-runner Herman Cain, alleging that Cain's 9-9-9 tax plan was the last thing middle-class Americans need. Prefacing his attack with the obligatory, Herman, I like you, Santorum stated that the tax plan, which replaces the entire tax code with a 9-percent national sales tax, 9-percent income tax, and 9-percent payroll tax, would significantly raise taxes on all but the highest earners. 

From there, the rest of the field piled on. Rep. Ron Paul (R-Texas) called the plan regressive because of the impact it has on low-income earners. Texas Gov. Rick Perrry, happy to see someone else become a punching bag for a change, chided Cain for adding a national sales tax on top of existing state sales taxes. And Mitt Romney, pretending not to know the answer to the question, asked Cain if the federal tax would replace all state sales taxes. When Cain told him no (that video clip will come in handy), Romney announced he was against it. 

So who's right? Santorum and it's not even close. Just check out this chart, from the non-partisan Tax Policy Institute (via Kevin Drum ). 

Data from Tax Policy Institute 

Santorum went on to call for a renewed focus on a lack of income mobility in the United States, noting that it's easier to pull yourself up by the bootstraps in Europe than in the United States. So there you have it: Rick Santorum is the Zucotti Park candidate.
