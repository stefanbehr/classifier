It's a little surprising this comment last week from Rep. Scott Garrett (R-N.J.) hasn't gotten more attention. Here's Philip Molnar of the Express-Times : 

[Local business leader Richard C. Spanier] said the best people to do business with are those in the American Midwest because of their straight-forward attitude. 

Other ethnicities are not that way, Garrett said. They'll say yes to you constantly and then you'll realize they really didn't mean it. 

Garrett said after the meeting he meant people in other countries. 

The clarification doesn't really make this comment much less bizarre or offensive. (Since when do New Jersey congressmen lionize the honesty of midwesterners, anyway?) 

Garrett is no congressional rookie. He was first elected in 2002 and is the most conservative member of New Jersey's congressional delegation, according to the Almanac of American Politics . Today, he chairs the budget task force for the Republican Study Committee, which is basically a club for the GOP's hard-liners. He's also the chairman of the House subcommittee that's in charge of overseeing Fannie Mae and Freddie Mac, the two government-sponsored mortgage giants. 

Garrett's district is around 80 percent white. No word on what percentage of those folks are straight-forward.
