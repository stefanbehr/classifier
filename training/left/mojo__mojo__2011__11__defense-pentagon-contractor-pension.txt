The federal government forks over billions of dollars a year to keep high-earning defense contractors' pensions afloat, according to a new story by the New York Times ' Gretchen Morgenson : 

The so-called supercommittee in Congress has until Nov. 23 to find more than a trillion dollars of new savings in the federal budget. 

Here s one idea: Stop reimbursing the costs of pensions and other retirement benefits at huge, and hugely profitable, defense contractors. Over 10 years, such a move could save an estimated $30 billion the amount by which these pensions are collectively underfunded... 

Many of these funds have lost money in recent years in declining financial markets or on bad investments, so the bill for taxpayers has been growing. 

Considering how much ordinary Americans have lost in their own retirement accounts losses that the government does not cover reimbursing contractors looks like classic corporate welfare. 

And how . Government contracts with megafirms like Boeing, General Dynamics, Lockheed Martin, and Raytheon require Uncle Sam to reimburse the companies when their workers' pension funds take a hit in the market. Over the past five years, Lockheed has secured $3.1 billion in taxpayer dollars for pension reimbursements; that's a significant chunk of the $21.8 billion in operating profits they reported over that period. 

These payments are all the more troubling since politicians from the left and the right ( including President Obama ) have targeted military veterans' retirement benefits in their cost-cutting zeal. But vets make a whole lot less than contractors, or Beltway bandits, as they're called in Arlington and DC. Morgenson reports that the current value of the top five Lockheed Martin executives' benefits is around $40 million. That's just a tad higher than your average retired gunnery sergeant or lieutenant colonel makes in retirement. 

But federal pension payments to contractors are increasing at a furious clip. The reason: These companies made some really bad recession-era investments with their workers' retirement dollars, and a margin call is coming. The Times reports that Raytheon's pension fund was underfunded by $4 billion at the end of 2010. That's nothing compared with Lockheed, whose pension is a whopping $10.4 billion in the red, and [a]s Lockheed contributes money to make up for this shortfall, the government will reimburse it. 

Instead, maybe Lockheed could fund its own pension out of all the extra money it's made (slowly) building fighter jets . Just a thought.
