Just imagine if he'd shown Two and a Half Men . 

An Illinois school district suspended a high school civics teacher for two weeks after parents complained that he had forced kids to watch The Daily Show in class. According to the Bloomington (Ill.) Pantagraph , Rhett Felix showed Eureka students several recent segments from the Emmy-winning show, which resulted in a slew of complaints from offended parents: 

School Superintendent Randy Crump suspended first-year teacher Rhett Felix on Tuesday morning following a two-hour executive session of the Eureka-based District 140 school board Monday night. During the public portion of the meeting, parents complained about bleeped obscenities and some sexual content of the segments and about a perception that Felix appears to have a liberal political bias. 

So what were the offensive segments students were shown? The paper helpfully identifies the specific clips from the October 31 and November 2 episodes. From there, it's not hard to see where things went wrong: One of the segments, which you can see below, begins with Jon Stewart discussing the then-breaking news of Herman Cain's sexual harassment complaint, and offering his own euphemistic pizza phrases: You want sausage on your pie? ; Want me to stuff your crust? ; I told you, I guarantee, I will come in 30 minutes or less : 

Felix, who could not be immediately reached for comment, also took heat for (as the paper puts it), warning students against an Internet search that yields results deemed to be pornographic. As it turns out, that's a reference to another Daily Show clip, in which Stewart discusses Rick Santorum's Google problem . 

Although the Daily Show is in fact a trusted source of news for many American teens, frothy fecal matter is not exactly the kind of subject you'd expect your kids' civics teacher to be discussing in class especially not in conservative Eureka, Illinois, where Ronald Reagan spent his formative years. 

Still, this quote, from a concerned parent, seems a bit much: I look at what happened out at Penn State. Even though this doesn't rise to that particular level, I would ask that this board look at these allegations and respond with appropriate resolve. 

It's exactly like Penn State. Except nothing at all like Penn State.
