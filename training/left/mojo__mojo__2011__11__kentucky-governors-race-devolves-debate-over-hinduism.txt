You would think that securing $43 million in tax credits for a to-scale replica of Noah's Ark would be enough to protect Kentucky Gov. Steve Beshear (D) from allegations that he's secretly some sort of America-hating pagan who takes his policy proposals straight from the malaria-infested mouth of the great swamp god, Mordu. You would be wrong. Beshear, who is heavily favored to win re-election this November, is taking heat from his Republican opponent for participating in a Hindu ground blessing ceremony last weekend at a groundbreaking for a new Indian-owned Elizabethtown factory. Here's how Republican Senate president and gubernatorial nominee David Williams put it: 

He's there participating with Hindu priests, participating in a religious ceremony. They can say what they want to. He's sitting down there with his legs crossed, participating in Hindu prayers with a dot on his forehead with incense burning around him. I don't know what the man was thinking... 

If I'm a Christian, I don't participate in Jewish prayers. I'm glad they do that. I don't participate in Hindu prayers. I don't participate in Muslim prayers. I don't do that. To get down and get involved and participate in prayers to these polytheistic situations, where you have these Hindu gods that they are praying to, doesn't appear to me to be in line with what a governor of the Commonwealth of Kentucky ought to be doing... 

Yet between his not being pro-life and his support for gambling and now getting down and doing Hindu prayers to these Hindu gods, I think his grandfathers wouldn't be very pleased with Steve Beshear. 

Williams, per the Lexington Herald-Leader , went on to dismiss charges that he was demeaning Hinduism by referring to it as idolatry, telling the paper that if anyone had offended Hindus, it was Beshear. Kentucky politicians have a proud and bipartisan history of making absurd allegations about their opponents' faith. Last fall, Democratic Attorney General Jack Conway accused Sen. Rand Paul of worshipping the false-God* Aqua Buddha (a nod to a prank Paul had played in college). 

(h/t Eric Kleefeld ) 

*His assertion, not mine.
