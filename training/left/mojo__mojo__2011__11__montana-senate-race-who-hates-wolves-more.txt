Next fall's Montana Senate race is shaping up to be the most expensive election in the state's history. Karl Rove's dark money outfit, Crossroads GPS, is already saturating the airwaves in the state ( it's also going after Elizabeth Warren ). The race, pitting incumbent Democrat Jon Tester against longtime Rep. Denny Rehberg (R), could well decide which party controls the Senate. There's a lot at stake so naturally, the race has come down to the important question of which candidate hates the endangered western wolf more. 

Eli Sanders files a dispatch from Montana and highlights this element of the race: 

While in most states you won't find a Democrat trying to out-hustle a Republican over who got an endangered animal like the Western wolf less federal protection, this, again, is Montana where a lot of voters see wolves as livestock predators. So when Rehberg, who has a stuffed Canadian Black wolf in his office, suggested he's the one responsible for the de-listing of American endangered wolves, Tester's campaign pounced, reminding people that Rehberg's bill actually didn't go anywhere in Congress. The record is clear as to who did what, Tester told me. It is absolutely, unequivocally clear. He could not get his bill out of committee. I got my bill signed by the president. 

Boom. Maybe noted coyote-killer Rick Perry is running for the wrong office.
