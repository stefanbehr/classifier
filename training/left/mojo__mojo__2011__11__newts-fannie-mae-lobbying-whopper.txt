At Wednesday night's GOP presidential debate in Michigan, Newt Gingrich was asked by the mostly on-the-ball CNBC panel about his work on behalf of housing giant Freddie Mac. For the former Speaker of the House, it was a bit of a welcome-back moment; for the last few months, he's been so much of an afterthought that moderators haven't even bothered with his own personal history and resume. 

But Gingrich had an answer ready. He denied the lobbying charge, and then, via Benjy Sarlin , offered this spirited defense: 

I offered advice. My advice as an historian when they walked in and said we are now making loans to people that have no credit history and have no record of paying back anything but that s what the government wants us to do. I said at the time, this is a bubble. This is insane. This is impossible. It turned out unfortunately I was right and the people who were doing exactly what Congresswoman Bachmann talked about were wrong. 

It's pretty self-evident, though, that Gingrich wasn't hired as a consultant because he was an untenured history professor at North Georgia College in the late 1970s. He was hired because, as a former Speaker of the House, he had a lot of influence with a lot of imporant people. An AP investigative report from 2008 framed Gingrich's role as that of a political operator, greasing the wheels on Capitol Hill. Key section : 

Efforts to tighten government regulation were gaining support on Capitol Hill, and Freddie Mac was fighting back. 

According to internal Freddie Mac documents obtained by the AP, Reps. Bob Ney (R-Ohio), and Paul Kanjorski (D-Pa.) spent the evening in hard-to-obtain seats near the Nationals dugout with Freddie Mac executive Hollis McLoughlin and four of Freddie Mac's in-house lobbyists. Both were members of the House Financial Services Committee. The Nationals tickets were bargains for Freddie Mac, part of a well-orchestrated, multimillion-dollar campaign to preserve its largely regulatory-free environment, with particular pressure exerted on Republicans who controlled Congress at the time. 

Internal Freddie Mac budget records show $11.7 million was paid to 52 outside lobbyists and consultants in 2006. Power brokers such as former House Speaker Newt Gingrich were recruited with six-figure contracts. Freddie Mac paid the following amounts to the firms of former Republican lawmakers or ex-GOP staffers in 2006... 

Pushing back, Freddie Mac enlisted prominent conservatives, including Gingrich and former Justice Department official Viet Dinh, paying each $300,000 in 2006, according to internal records. 

Gingrich talked and wrote about what he saw as the benefits of the Freddie Mac business model. 

Gingrich made a pretty penny as a consultant in the 2000s. As CPI reported, the former Speaker's consulting firm took in $312,000 from the ethanol lobby in 2009. Presumably, they weren't paying him for his historical insights.
