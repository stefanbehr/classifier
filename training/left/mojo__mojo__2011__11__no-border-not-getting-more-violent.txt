Although Tuesday's GOP presidential debate was billed as foreign policy and national security debate, the candidates spent much of the night discussing domestic issues like the Super-Committee and immigration. And that led to one of the night's biggest whoppers albeit one Republican candidates have a tendency to repeat over and over: The suggestion, from Phil Truluck of the Heritage Foundation, that the southern border has become more and more violent Texas Governor Rick Perry claimed that, under President Obama's watch, the southern border has become more and more violent. 

As it happens, the Austin American-Statesman examined the numbers in-depth last month, and reported that in Texas, border violence has actually gone down: 

[A] closer look at crime numbers in border counties since 2006 the year Mexican violence began to spike in earnest does not reveal evidence of out-of-control chaos. An American-Statesman analysis of all 14 counties that share a border with Mexico and two dozen border cities shows that violent crime along the Texas side of the Rio Grande fell 3.3 percent between 2006 and 2010. 

During the same period, the combined number of murders in the 14 counties fell 33 percent, to 73 in 2010 from 97 in 2006. 

Further, most counties and cities situated directly across from the worst of the Mexican violence also saw their crime rates decrease, even as thousands were slaughtered on the Mexican side. 

Read the whole story . 

Update: As a commenter points out, I rushed to put this up without double-checking the transript: Truluck brought up the figure, not Perry. Mea culpa. Perry didn't really answer the question or address the point, although he has made much the same point with regularity most notably at a debate in September.
