Some people think that the large and growing gap between the richest 1 percent of Americans and the other 99 percent, nicely illustrated in our inequality charts , is reflective of broader problems in our society and should probably be smaller. Clark Durant, a Republican businessman who's running for Senate in Michigan, has an alternative view : 

In regards to the Occupy Wall Street movement, Durant said the protesters should go find a job. In regards to the wealth gap the movement decries, Durant said, I think it should be wider. 

Tell us how you really feel! 

(h/t Sean Sullivan ) 

UPDATE: Durant has issued a statement on this matter. Posted without comment: 

Thank you for challenging my statement about 'widening the gap'. I do not believe in widening the income gap between rich and poor, and my life's work in the inner city of Detroit demonstrates that far more than any sound bite. At Calvin College my 'widening the gap' remark, in its context, sought to challenge the students to think outside the box when they hear stock statements that pit one group of people against another. We need a country that embraces all, and rewards innovators, entrepreneurs, job creators, and hard-working people of all sorts. Innovators like Steve Jobs and Henry Ford, a part of the 1%, make life better for us all. But instead of just one, what if we had 100, 1,000, or 10,000 such innovators? And that was my point at Calvin College. I'm for innovation, and a commitment to a rising tide that lifts all boats for all Americans. I believe in the 100%.
