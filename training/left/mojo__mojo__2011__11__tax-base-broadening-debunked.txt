Business leaders, some congressional Democrats, and almost all congressional Republicans think corporate tax rates should be lower. By closing loopholes, corporate tax-cutters argue, the government could reduce rates from the current 35 percent to 25 percent or even lower all without losing revenue. The Tax Policy Center's Howard Gleckman, though, says that idea is bogus : 

The non-partisan [Joint Committee on Taxation] found that even if Congress scrubbed every single corporate preference from the code (a political fantasy if ever there was one) it could not get the corporate rate below 28 percent without adding to the budget deficit, raising taxes on individuals, or cutting spending. 

The JCT study, which was requested and released by House Ways Means Committee Democrats, comes just days after the panel s chairman, Rep. Dave Camp (R-MI), proposed a 25 percent rate as part of a major corporate reform. Camp did not say how he d pay for his proposed changes. 

My Tax Policy Center colleague Eric Toder has been making a similar point for months: It is painfully difficult to find the money to reduce rates very much. Unlike corporate breaks, there are more than enough individual tax preferences out there to pay for individual rate reduction (and have money left over the cut the deficit). The problem is merely a lack of political will. Abolish the mortgage interest deduction anyone? 

According to the JCT, there aren t enough tax breaks, in dollar terms, to lower the rate below 28 percent. 70 percent of corporate tax breaks come from breaks for expensing research and experimentation costs, and capital investment. 

As Gleckman points out, a number of companies, including Google and GE, already pay an effective tax rate below 10 percent, thanks to various subsidy packages and tax breaks. In fact, a study that Citizens for Tax Justice and the Institute on Taxation and Economic Policy released on Thursday found that 71 of the 280 companies surveyed paid a tax rate of less than 10 percent of their pre-tax profits last year. GE, for example, paid a rate of minus 45 percent, according to the report. (The company calls the study inaccurate and distorted, and said it has paid billions in taxes over the past decade.) 

In any case, one imagines that companies that are already paying negative tax rates i.e., getting money from the government would have some pretty unkind words for any lawmakers daring to unwind their favorite loopholes. And that's what makes corporate tax reform so hard to execute.
