The blog Blue Virginia flags what is possibly the most offensive mailer of this election season. The mailer targets Patrick Forrest, an openly gay Republican candidate for state Senate in the 32nd district of Virginia. It's not exactly subtle: 

The mailers claim to come from the group Our Heritage USA, which as Blue Virginia notes, doesn't exist in any of the state or federal election databases. The address it lists is this parking lot in Lynchburg:
