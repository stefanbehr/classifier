I asked Mother Jones ' Josh Harkinson to narrate his night skipping barricades and risking arrest at Zuccotti Park, and I illustrated his story with my own interviews from throughout the night when Occupy Wall Street was raided by police. Read Josh's liveblog and first-hand account from the raid of Zuccotti, and check out all the rest of our #OWS coverage . 



Do you appreciate fair and factual reporting on Occupy Wall Street? Please donate a few bucks to help us expand our coverage .
