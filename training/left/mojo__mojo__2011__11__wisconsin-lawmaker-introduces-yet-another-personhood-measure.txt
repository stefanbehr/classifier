An Assembly member in Wisconsin is the latest anti-abortion lawmaker to introduce a personhood measure, which would define human life as beginning at the unification of a sperm and egg. 

Assembly Joint Resolution 77 , introduced on Thursday, amends the state constitution to strike the word born from the line of the constitution declaring that All people are born equally free and independent, and have certain inherent rights; among these are life, liberty and the pursuit of happiness. The measure also adds the line, As applied to the right to life, the terms 'people' and 'person' shall apply to every human being at any stage of development. 

Even though Mississippi voters overwhelmingly rejected this kind of measure last week, anti-abortion lawmakers in Georgia and a number of other states want to try passing it as well. 

The bill's lead sponsor is Republican Andre Jacque . Abortion is a top issue for Jacque , and something he was active on as president of the Pro-Life Action League as a student at the University of Wisconsin in the early 2000s. In the Assembly, he's also championed a measure to honor so-called crisis pregnancy centers and defund Planned Parenthood in the state. He lists previous memberships in Pro-Life Wisconsin and Wisconsin Right to Life on his official bio . 

It's actually a flaw in our current constitution where you have to be born in order to access inalienable rights to life, liberty, and the pursuit of happiness, Jacque argues . 

It's worth noting that Wisconsin Right to Life is on record opposing this type of measure, as the group believes it would be counterproductive and lead to inevitable and expensive legal fights. And under Wisconsin law, a constitutional amendment has to pass in both chambers in two consecutive sessions of the legislature, after which it's put to voters on a statewide ballot. 

The Wisconsin legislature is also considering a measure that would repealing a state law requiring comprehensive and medically accurate sex ed in schools. Instead, schools would be able to go back to teaching abstinence-only education, if they preferred. 

It's incredibly disheartening to see our state legislators destroy programs that actually serve and help Wisconsin's children and families, yet they'll fight to the bitter end for a fertilized egg, says Sara Finger, executive director of the Wisconsin Alliance for Women's Health. We see where our legislators' priorities are now the top 1 percent and fertilized eggs.
