It's straight out of a Don DeLillo novel: A few hours after television producers set up a replica of Occupy Wall Street for the filming of a new episode of Law Order: Special Victims Unit , the real Occupy Wall Street announced plans to occupy the fake one. At 11:30 p.m. the call to occupy the set went out on Twitter with the hash tag #Mockupy. Located at nearby Foley Square, the fake camp includes a replica of the OWS kitchen and library as well as numerous tarps, tents, and signs. They've delivered us this perfectly wrapped Christmas present with a bow on top: They rebuilt our camp, OWS organizer Jake DeGroot told me shortly before the announcement went out. How could we not go and take it? 

Here's video of the fake Zuccotti Park being occupied by the real occupiers: 



As of about 1:00 a.m., the police had begun to push protesters out of the park and dismantle the set. NYPD does not respect Law and Order, the crowd chanted cheekily. At one point, an occupier asked an officer, Are these real barricades, or a set piece? 

Within about an hour police had cleared out the protesters, which was less time than it took clear the real Zuccotti, but probably more than they'd need on a TV show. You guys just cleared a fake Zuccotti Park, the tweeter @NewYorkist told a police officer, who countered that they'd done no such thing: We didn't clear a fake Zuccotti, he insisted. They're taking the set down. 

A few minutes later, the occupiers regrouped on a nearby set of steps for an impromptu general assembly. This is beautiful, and this points out to us a more clever way to fight the struggle, someone said, echoed by the people's mic. 

Whose park? another man yelled. 

Everyone knew their lines. Our park! 

Do you appreciate fair and factual reporting on Occupy Wall Street? Please donate a few bucks to help us expand our coverage .
