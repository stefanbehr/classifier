The ambush interview is a common feature on Bill O'Reilly's Fox News show. An O'Reilly underling will spring on an unsuspecting subject blogger Amanda Terkel , a Florida county judge with whom O'Reilly disagreed, Columbia Journalism Review editor Mike Hoyt , among many others press a camera in his or her face, and pepper them with leading questions. 

On Wednesday night, O'Reilly got a taste of his own medicine. A protester spotted O'Reilly near DC's Willard InterContinental hotel, the site of a fundraiser for GOP presidential candidate Newt Gingrich , turned on the camera, and repeatedly asked O'Reilly if he'd attended the Gingrich event. O'Reilly responded by shoving his umbrella in the face of the ambusher and later complaining to a nearby cop, the clip shows. The video was sent to Mother Jones by a staffer with the Service Employees International Union. 

Watch: 



O'Reilly's confrontation wasn't the only scuffle between protesters flying under the we are the 99 percent banner and conservative figures. Earlier in the evening, as Mother Jones got on camera , a dozen or so boisterous protesters crashed the Gingrich fundraiser in downtown DC. After nearly ten minutes of chanting and testimonials, the protesters were ejected from the candlelit event by hotel security and other angry attendees. 

Watch: 



Protesters also visited the home of House Speaker John Boehner (R-Ohio) on Thursday, and said they were planning several other actions throughout the day. The actions are in conjunction with Take Back the Capitol, a 99-percent-themed series of protests around Washington, DC.
