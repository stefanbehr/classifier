Courtesy of Fox Nation The 2011 White House Christmas card features a content looking First Pup Bo Obama sitting by a roaring fireplace, flanked by Christmas presents and festive Christmasy ribbons and pine wreaths and bulbs. If you listen hard, you can almost hear sleigh bells. 

It's all pretty non-controversial. Boring, even. Unless, of course, you're Fox News in which case the bookshelf is filled with Lenin's B-sides, the Constitution is burning in the fireplace, Winston Churchill's bust is conspicuously absent, Bo has become dependent on the federal government for handouts, and the empty seat is a stirring reminder of President Obama's nonexistent leadership. I'm exagerrating, but only slightly: 

Former Alaska Governor Sarah Palin told Fox News Commentary that she found the card to be a bit unusual. 

It's odd, she said, wondering why the president's Christmas card highlights his dog instead of traditions like family, faith and freedom. 

... 

Palin said the majority of Americans can appreciate the more traditional, American foundational values illustrated and displayed on Christmas cards and on a Christmas tree. 

As for the Obama card, she replied, It's just a different way of thinking coming out of the White House. 

Why does Sarah Palin hate puppies? 

Update: But since Fox News has brought up the subject of Christmas cards, perhaps we should take a look at the official Fox Business Network Christmas card this holiday season. Here's one, via New York Times media reporter Brian Stelter : 

Courtesy of Brian Stelter That's a pair of foxes roasting the NBC peacock over an open fire which, for you non-Christians out there, is an oft-overlooked aspect of the of the story of the first Christmas. And via reader Jason Sparks, take a look at Ronald Reagan's White House Christmas cards . They're nearly identical to Obama's, except there's no puppy. Family, faith, and freedom are, presumably, represented by the antique furniture, fireplaces, and tacky lighting.
