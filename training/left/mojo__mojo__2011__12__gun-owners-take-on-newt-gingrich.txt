Now that's he's the front-runner in the race for the GOP presidential nomination, Newt Gingrich's 33-year-record is officially open for scrutiny. There are plenty of reasons why conservatives might reject the former Speaker of the House, but here's another one: guns. Georgia Gun Owners, a grassroots gun rights group from his home state, is now blasting Gingrich for his more than two-decade history of supporting gun control. The group has asked its 6,000 members to call Gingrich's new Iowa headquarters and make their complaints known. 

Per the release the group blasted out this morning: 

While Newt used the institutional gun lobby as a mouthpiece to convince millions of gun owners nationwide that as long as he is Speaker, no gun-control legislation is going to move in committee or on the House floor, he was working behind the scenes to pass gun control. 

In 1996, Newt Gingrich turned his back on gun owners and voted for the anti-gun Brady Campaign's Lautenberg Gun Ban, which strips the Second Amendment rights of citizens involved in misdemeanor domestic violence charges or temporary protection orders -- in some cases for actions as minor as spanking a child. 

Gingrich also stood shoulder to shoulder with Nancy Pelosi to pass the Criminal Safezones Act which prevents armed citizens from defending themselves in certain arbitrary locations. Virtually all Americans know that Criminal Safezones don't protect law-abiding citizens, but actually protect the criminals who ignore them. 

One of the problems with having a 33-year political career is that you accumulate a really long record of positions. It doesn't help Gingrich that he made his biggest impact in the '90s, when gun control and crime were much higher-profile issues than they are today. And as Elizabeth Drew recounts in her book Showdown , Gingrich viewed the gun lobby as a group that must be appeased, but it wasn't exactly his core constituency as Speaker. 

An important caveat, though: If Gingrich is unappealing on guns, former Massachusetts Gov. Mitt Romney is hardly a palatable alternative. A similar group in New Hampshire, the New Hampshire Firearms Coalition, has been handing out anti-Romney literature in the Granite State, touting (among other things) his support for a ban on assault weapons. If you're a hard-core Second Amendment rights activist, you likely cast your lot with Ron Paul a long time ago.
