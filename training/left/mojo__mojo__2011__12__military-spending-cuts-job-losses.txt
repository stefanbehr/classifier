Now that the budget supercommittee has failed in its mission to produce a $1.2 trillion deficit-reduction plan, the Department of Defense must automatically cut $600 billion from its budget within the next decade. As expected, the US defense industry is not happy about this, and its lobbyists have been scrambling for ways to convince Congress that the impending cuts are untenable. 

So far, one major theme has emerged: Cut spending in our industry, and you cut hundreds of thousands of American jobs . Marion Blakey, CEO of the Aerospace Industries Association, announced that the cuts in the aerospace and defense portion alone could result in the loss of over 1 million American jobs. 

But that's misleading, according to the Brave New Media Foundation . While the defense industry would lose some opportunities, military spending in fact amounts to a net loss of jobs when compared with government investment in other industries, according to the group's research. Pumping money into the green energy sector and education field creates far more jobs than defense spending does in the case of education, twice as many. 

Watch:
