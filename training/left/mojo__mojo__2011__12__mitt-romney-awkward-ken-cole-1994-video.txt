This clip of then-Senate candidate Mitt Romney being interviewed by Medfield (Mass.) Cable 8 back in 1994 has been making the rounds. It's classic Romney, right down to his preferences for music: I like music of almost any kind, including this. Boy, aren't bass lines great? I love tempo. 

Take a look: 



If you're wondering, What ever happened to that Ken Cole kid? we can report that he seems to have recovered completely from the awkwardness of this interview, as well as the supreme boredom of growing up in Medfield (incidentally, also my hometown). His first directing credit, Tornado Glory , tracked the antics of two Midwestern twister chasers for PBS, and he just completed his second project, a Bourne Supremac y-style mockumentary about IT workers. I sent him the clip; here's his reaction: 

Medfield Day, of course, is the single most exciting event in a town where nothing much happens. Every year the whole town turns out to eat some burgers, walk in circles, and pick up freebies from local banks. 

In 1994 I was a sophomore in high school and a volunteer at Cable 8. On Medfield Day, Cable 8 would deploy their full staff to cover the crowds, the band, and more crowds. To liven it up a bit, we took a camera and interviewed random people around the pond. We were being goofy and irreverent, asking silly questions and trying to make it fun. 

Eventually we saw a group of five people wandering aimlessly around the Lion's Club burger area. They were holding Romney signs, and when I looked closer I saw Mitt Romney himself, grinning at prospective voters. 

Hey! That's the guy from TV! I exclaimed. Romney's campaign was going full blast, and we all recognized him immediately. Do you think he'd do an interview? I went up, asked him if he'd do it and the rest is in the video! 

It's hilarious, I honestly never expected a full-frontal political pitch. We had just interviewed some kids and Big Bird about their favorite kind of candy, and here's Romney gunning for Ted Kennedy! At that moment, I learned that the hardest thing to do when interviewing a politician is to change the subject away from politics. 

It's a lot of fun watching the interview now that he's running for president. The sincere awkwardness makes it amusing and unique. And the line about his favorite music is so delightfully Romney . I mean, it's probably the greatest, shortest non-interview Romney's ever done. 

I think Romney is the same today as he was at Medfield Day in 1994. The guy hasn't even aged. He's basically the ultimate politician he's a machine! I mean, people don't smile that much naturally. It takes years of intense training. If Mitt Romney were a food, he'd be a Wonder Bread PB J. Familiar, bland, easily digestible. 

Familiar, bland, easily digestible. Fits on a bumper sticker. 

In more serious Romney news, the former Massachusetts governor trails both Newt Gingrich and Rep. Ron Paul (R-Texas) in the latest Des Moines Register poll of likely Iowa caucus voters, with the first meaningful votes still a month away. After staying noncommittal for months and consciously avoiding any appearance that he was actually interested in winning the state, Romney's camp has renewed its push to win the first-in-the-nation contest. He does still lead the field in New Hampshire, although Gingrich is closing fast.
