The plot thickens in the case of GOP presidential candidate Mitt Romney's missing emails from his time as Massachusetts governor. Reuters reports that Romney administration staffers spent almost $100,000 to buy up the hard drives in their state computers before Romney left office, preventing reporters, oppo researchers, and anyone else from obtaining the administration's internal deliberations. Staffers also purged administration emails from state servers. All of this took place months before Romney announced his first presidential run, in February 2007. 

Here's more from Reuters: 

Romney's spokesmen emphasize that he followed the law and precedent in deleting the emails, installing new computers in the governor's office and buying up hard drives. 

However, Theresa Dolan, former director of administration for the governor's office, told Reuters that Romney's efforts to control or wipe out records from his governorship were unprecedented. 

Dolan said that in her 23 years as an aide to successive governors no one had ever inquired about, or expressed the desire to purchase their computer hard drives before Romney's tenure. 

The cleanup of records by Romney's staff before his term ended included spending $205,000 for a three-year lease on new computers for the governor's office, according to official documents and state officials. 

In signing the lease, Romney aides broke an earlier three-year lease that provided the same number of computers for about half the cost $108,000. Lease documents obtained by Reuters under the state's freedom of information law indicate that the broken lease still had 18 months to run. 

As a result of the change in leases, the cost to the state for computers in the governor's office was an additional $97,000. 

The Boston Globe first reported earlier this month that Romney staffers had deleted emails from a government server. 

That said, some paper records from the Romney administration do remain in the state archives in Massachusetts, including emails and internal memos. Some of those documents formed the basis of a November Mother Jones story revealing how Romney and his administration prioritized hoovering up as much federal money as possible as they grappled with a $3 billion budget deficit a message at odds with Romney's current support for slashing federal spending and downsizing the federal government.
