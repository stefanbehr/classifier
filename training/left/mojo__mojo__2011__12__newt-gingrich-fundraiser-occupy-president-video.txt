On Wednesday night, Republican presidential front-runner Newt Gingrich held a fundraiser at the posh Willard InterContinental hotel in downtown Washington, DC. Waiting there for Gingrich were a few dozen protesters. Around 7 p.m., they snuck through an unlocked back door to the candlelit ballroom hosting the Gingrich affair and caused a ruckus. An email from the Service Employees International Union alerted me to the protest, and I joined them as they crashed the fundraiser. 

Inside, the protesters squeezed out a few testimonials on the megaphone and chants of We are the 99 percent! before being confronted and ejected from the room or, in my case, pushed out of the room by hotel security and other suited individuals. 

Watch: 



Earlier, protesters had gathered outside the front doors of the Willard, chanting, The poor get poorer, the rich get rich, that's the platform of Gingrich. They hoisted a We are the 99% banner, and the hotel locked several of its entrances. 

The Gingrich fundraiser protest was part of Take Back the Capitol, a five-day, 99-percent-themed series of protests targeting lawmakers at popular fundraising and deal-making spots in DC, including the Capitol Hill Club, a GOP haunt, and Charlie Palmer Steakhouse, a favorite lunch spot for lobbyists and legislators a stone's throw from Capitol. On Tuesday night, protesters lined the entrance to the swanky Lincoln restaurant to protest a fundraiser thrown by House Majority Leader Eric Cantor (R-Va.). At least a dozen were arrested on Wednesday during a march on K Street, the symbolic heart of DC's lobbying industry. 

The protesters' schedule includes a full day of events on Thursday, including actions at the Capitol Hill Club and elsewhere around DC. But no 1-percenter knows where they might strike next. 

Front page image: Rena Schild / Shutterstock.com 

Print Email Tweet The Daily Show Takes the Piss Out of Rick Scott (Almost) Obama Admin Ignores FDA on Plan B Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

What Newt Gingrich Isn t Telling You About His Literacy Program 

The GOP candidate holds up his old nonprofit, Earning by Learning, as a way to teach kids the value of a buck. Here's what he doesn't mention. WATCH: The Gingrich Who Stole Christmas (Saunders Cartoon) 

Gun Owners Take On Newt Gingrich 

Activists in the former Speaker's home state aren't happy about his record on guns. Newt Gingrich Is Sad That Politics Has Gotten So Nasty 

How Newt Gingrich Saved Porn 

In the 1990s, the speaker of the House fought against censorship of sexually explicit materials on the internet. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
