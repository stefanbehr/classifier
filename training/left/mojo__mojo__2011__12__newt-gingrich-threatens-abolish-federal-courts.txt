Newt Gingrich has a reputation, earned or not, as a man of ideas. And at Thursday's GOP presidential debate in Iowa, he suggested a big one: Borrow a page from Thomas Jefferson and abolish federal courts whose judges have handed down decisions he disagrees with. (He's previously called for the Ninth Circuit Court of Appeals to be purged.) If nothing else, he'd call liberal judges before Congress to testify. 

As Gingrich put it, The courts have become grotesquely dictatorial, far too powerful, and I think frankly arrogant in their misreading of the American people, the former House speaker said. I would, just like Jefferson, Jackson, Lincoln, and FDR, I would be prepared to take on the Judiciary if it did not restrict itself in what it was doing. 

Video, via Think Progress : 



Although Jefferson's clashes with the courts aren't as well known, Jackson and FDR's power-grabs have been largely condemned by historians. Gingrich, however, dismissed concerns that dismissing entire courts would unconstitutionally tip the scales on the balance of power: I would suggest to you, actually, as a historian I may understand this better than lawyers. (Never mind that Gingrich, who specializes in counterfactual historical novels, is not a historian.)
