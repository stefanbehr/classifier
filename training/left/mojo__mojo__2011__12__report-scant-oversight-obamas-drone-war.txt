This much we know about America's ever-expanding drone war in South Asia and the Middle East: it's controversial, rarely acknowledged by the White House, often effective, and subject to only the sketchiest Congressional oversight. Greg Miller's lengthy rundown of President Obama's breathtaking expansion of the drone program in the Washington Post only affirms those impressions. 

Running through the publicly available numbers, Miller reports that the current drone arsenal boasts dozens of secret bases and a fleet of 775 Predators and Reapers, with hundreds more on the way. But the CIA also runs a number of stealth drones that the government doesn t acknowledge exist, like the one that crashed in Iran earlier this month . 

Miller also reports that the CIA and Joint Special Operations Command (JSOC) maintain their own, separate kill lists for drone strikes. And that throws up some pesky roadblocks for Congressional oversight: 

Within 24 hours of every CIA drone strike, a classified fax machine lights up in the secure spaces of the Senate intelligence committee, spitting out a report on the location, target and result. 

The outdated procedure reflects the agency's effort to comply with Title 50 requirements that Congress be provided with timely, written notification of covert action overseas. There is no comparable requirement in Title 10, and the Senate Armed Services Committee can go days before learning the details of JSOC strikes. 

Neither panel is in a position to compare the CIA and JSOC kill lists or even arrive at a comprehensive understanding of the rules by which each is assembled. 

The senior administration official said the gap is inadvertent. It's certainly not something where the goal is to evade oversight, the official said. A senior Senate aide involved in reviewing military drone strikes said that the blind spot reflects a failure by Congress to adapt but that we will eventually catch up. 

Miller also notes that the Senate intelligence committee is only now putting the finishing touches on a long investigation into President Bush's controversial interrogation program. That should give us a pretty good preview of what catching up looks like, no?
