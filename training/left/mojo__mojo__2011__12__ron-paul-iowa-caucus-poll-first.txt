Rep. Ron Paul , the libertarian favorite in the GOP presidential field, is giving establishment Iowa politicos headaches with his steady rise in popularity in their state, leading to predictions by some that the Texas congressman will win the state's caucuses next month. A new Iowa State University/Gazette/KCRG poll won't quell that speculation. 

In the poll, Paul has overtaken former House Speaker Newt Gingrich for the top spot, with 27.5 percent of those polled saying they'll back Paul. Gingrich grabbed the second spot, with 25.3 percent. Mitt Romney (17.5 percent), Texas Gov. Rick Perry (11.2 percent), and Rep. Michele Bachmann (R-Minn.) rounded out the top five. 

The ISU/Gazette/KCRG poll's organizer, however, says caucus-goers' opinions remain fluid, and that Paul's rise hardly guarantees his victory, KCRG reports: 

While Paul's lead is easily within the margin of error, James McCormick, professor and chair of political science at Iowa State and coordinator of the poll, says the polling found that 51 percent of those naming the libertarian-leaning Texan as their first choice are definitely backing him. 

The percentage for the next two candidates is much weaker, at 16.1 percent for Romney and 15.2 for Gingrich, McCormick said. 

Moreover, the percentage of respondents 'leaning to' or 'still undecided' in their support for these latter two candidates remains high, at 58 percent for Gingrich and 38 percent for Romney, he said. In other words, I'm going to make the case that these numbers are still very soft for those two candidates.
