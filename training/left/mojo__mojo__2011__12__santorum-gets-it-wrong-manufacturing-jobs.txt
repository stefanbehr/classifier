Presidential candidate and former Pennsylvania Sen. Rick Santorum took an early hit at President Obama's record on manufacturing jobs at Saturday's Republican debate, claiming the president has decimated the sector. 

Let's look at the numbers, via PolitiFact : During Obama's presidency, the number of manufacturing jobs has declined by nearly 800,000. But since January 2010, the number of jobs has increased by nearly 300,000 a rise of nearly 3 percent over the past two years. 

And when you compare Obama's record to that of past presidents, he looks even more impressive: 

In general, manufacturing employment has declined more or less steadily since the mid-1970s. The last time manufacturing jobs saw such a large and sustained numerical increase was in the early 1990s. So while the increase in years two and three of the Obama presidency is relatively small, it is the best in about two decades. 

One way to illustrate the long-term decline of manufacturing jobs is to look at how the numbers have moved during the past few presidencies. In the list below, we began counting from one year into each president s tenure through to the end of their term. Using that methodology, here are the numbers of manufacturing jobs gained or lost per year: 

Barack Obama: Increase of 157,368 manufacturing jobs per year in office 

George W. Bush: Decrease of 434,143 manufacturing jobs per year in office 

Bill Clinton: Increase of 37,143 manufacturing jobs per year in office 

George H.W. Bush: Decrease of 336,000 manufacturing jobs per year in office 

Ronald Reagan: Increase of 1,429 manufacturing jobs per year in office 

Jimmy Carter: Increase of 15,333 manufacturing jobs per year in office 

So by this measure, manufacturing jobs have actually increased by more under Obama than under any of his recent predecessors. 

Dinging Obama for presiding over a still-stagnant economy is all well and good. But the numbers suggest that he's made commendable progress toward reversing a troubling 40-year trend.
