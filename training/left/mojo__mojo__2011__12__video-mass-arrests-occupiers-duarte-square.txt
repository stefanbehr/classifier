Updated December 18th at 9:00 a.m. 

For weeks, Occupy Wall Street has been talking about occupying a vacant lot next to Duarte Square in SoHo. On Saturday, it walked the talk. At about 3:30 p.m, several hundred marchers left the square along with two large wooden ladders concealed beneath banners. They circled the block and converged at the lot's northwest corner, where they hoisted one of the ladders up to a tall chain-link fence. The first person over was retired Bishop George Packard, who writes at Occupied Bisho p. Here's a video of him entering the lot: 



After Packard tumbled over the fence, he climbed onto a wooden bench and waved for the crowd to follow. Other priests mounted the ladder while the the crowd yanked up the base of the fence to make a large opening. Someone cut the lock on a gate, and dozens of people streamed inside, talking, dancing to rap music from a boom box, and urging the rest of the crowd to join them. But the party couldn't last. The police, taken off guard at first, came pouring through the gate with flex cuffs and arrested everyone who didn't flee, including Packard. The New York Daily News reported that about 30 occupiers were loaded into police vans. Here's my video of the first arrests: 



Here's Packard discussing it all with fellow occupiers while riding to jail in a paddy wagon: 

That morning, things had gotten off to an ominous start when police detained and arrested Zach, one of the organizers, while he was walking across a nearby public park. Witnesses said that Zach has just delivered some t-shirts to the park and wasn't doing anything illegal, or even protesting. Police told a Democracy Now reporter that Zach was arrested on a warrant, suggesting that they're targeting key organizers for their role in planning new occupations. 

Occupy Wall Street had a variety of motivations for occupying the lot, which is owned by Trinity Church but not currently being used for anything. Many occupiers desperately want to establish another physical occupation, believing that it will give them a better platform for outreach and organizing. The Trinity lot is one of the few unused parcels remotely near Wall Street, and the occupiers hoped that letters of support from prominent clergymen such as Archbishop Desmond Tutu might sway the church to their side. They've also leaned on the church by highlighting its ties to Wall Street interests. 

Organizers chose December 17th to move on the lot because it marks the one-year anniversary of the self-immolation of Mohamed Bouazizi--the Tunisian fruit vendor who is credited with sparking the Arab Spring--and the three-month anniversary of OWS. Organizers told me that it's likely to be their last major occupation attempt until the spring, and the whole thing felt nostalgic even before it was over. I left my heart in Zuccotti Park, one sign said. After marching through the streets to Times Square--and getting kettled along the way--some organizers gathered at a popular OWS meeting spot in TriBeCa to watch video clips from the movement's early days. 

It was really incredibly optimistic of us to think that we were going to take that space and hold it, tactical team member George Machado told me afterwards. But for a moment it seemed possible: We got the clergy in first, and we had that space, and I thought for a second that we might be able to do it. 

Do you appreciate fair and factual reporting on Occupy Wall Street? Please donate a few bucks to help us expand our coverage .
