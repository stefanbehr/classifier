As a service to our readers, every day we are delivering a classic moment from the political life of Newt Gingrich until he either clinches the nomination or bows out. 

Newt Gingrich touts himself as a conservative futurist and although most futurists recoil at the suggestion that they're in the business of predicting the future, Gingrich wasn't quite so careful. His 1984 book Window of Opportunity is packed with predictions of what America might look like in the year 2000 (hint: a lot of it would be on the moon). For someone whose political rhetoric is so steeped in sweeping statements about transformative political developments, though, Gingrich was way off on one of the most transformative political developments of his day: 

We must expect the Soviet system to survive in its present brutish form for a very long time. There will be Soviet labor camps and Soviet torture chambers well into our great grandchildren's lives: great centers of political and economic power have enormous staying power; Czarist Russia lasted through 3 1/2 years of the most agonizing kind of war; the Nazi state did not collapse even when battlefield defeats reduced its control to only a tiny sliver of Germany. 

We must therefore assume the Soviet Union will survive as a dangerous totalitarian state. 

The Soviet Union collapsed seven years later.
