As a service to our readers, every day we are delivering a classic moment from the political life of Newt Gingrich until he either clinches the nomination or bows out. 



If you want to understand Newt Gingrich, start with what's on his book shelf. That's his advice, anyway. He assigned a reading list to his Republican caucus in 1995, and he peppers his speeches with references to writers like French existentialist Albert Camus. And as Connie Bruck explained in her epic 1995 New Yorker profile , Gingrich was particularly influenced by a novel about a 17th-century Japanese samurai named Toranaga: 

[Former campaign manager Carlyle] Gregory also said that in 1978 Gingrich was reading the novel Shogun by James Clavell, and that a major character Toranaga, a seventeenth-century samurai warlord had a powerful influence on him. [Gingrich's friend] Daryl Conner, too, told me that Toranaga was a critical model for Gingrich. The book is a narrative of Toranaga's quest for the absolute power of shogun. Throughout the book, Toranaga, who confides in no one, violently repudiates the suggestion of his most loyal followers that he should seek to become shogun, even calling it treason. Yet, through his study of individuals' psychology, his patience in listening, his system of punishment and reward, his establishment of an elaborate information network of spies, and his talent in projecting a wholly false self-image (he is an accomplished Noh actor), Toranaga is able to use, manipulate, and deceive all who come in contact with him; thus, in the end, he achieves his goal. 

It gets better: 

He will become shogun, and, moreover (this the reader learns at the very end), it has also long been Toranaga's long-held secret plan to rid Japan of white people. 

What if Gingrich is so outside our comprehension that only if you understand Japanese, anti-colonial behavior, can you begin to piece together his behavior?
