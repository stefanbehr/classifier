Netroots and tech industry pressure has dramatically slowed the progress of the Stop Online Piracy (SOPA) and Protect IP (PIPA) Acts, the sweeping anti-piracy bills backed by the lobbying might of the entertainment industry. As we wrote today , both SOPA and PIPA are on the ropes, thanks in part to a recent flurry of Internet activism. Tomorrow, a number of sites, including Reddit and Wikipedia, plan to blackout their sites in protest i.e., go inactive. And this isn't just some wily fringe movement: late on Tuesday, Google also got into the mix , announcing that it will voice its opposition against the bills on its homepage tomorrow. 

Former Sen. Chris Dodd (D-Conn.), now the CEO of the Motion Picture Association of America, is decidedly unamused: 

Only days after the White House and chief sponsors of the legislation responded to the major concern expressed by opponents and then called for all parties to work cooperatively together, some technology business interests are resorting to stunts that punish their users or turn them into their corporate pawns, rather than coming to the table to find solutions to a problem that all now seem to agree is very real and damaging. 

It is an irresponsible response and a disservice to people who rely on them for information and use their services. It is also an abuse of power given the freedoms these companies enjoy in the marketplace today. It s a dangerous and troubling development when the platforms that serve as gateways to information intentionally skew the facts to incite their users in order to further their corporate interests. 

A so-called blackout is yet another gimmick, albeit a dangerous one, designed to punish elected and administration officials who are working diligently to protect American jobs from foreign criminals. 

It is our hope that the White House and the Congress will call on those who intend to stage this blackout to stop the hyperbole and PR stunts and engage in meaningful efforts to combat piracy. 

The Cato Institute's Julian Sanchez, a prominent opponent of both bills, summarized Dodd's statement via Twitter, writing, Shorter MPAA: If you screw with us, our vassals in Congress will be holding hearings on your 'abuse of power' (aka 'speech'). 

Dodd's dismissal of the blackout as a dangerous stunt betrays one of Washington's most maddening blind spots. Huge portions of the Internet community have embraced a genuine act of protest one that doesn't rely on deep-pocketed lobbyists on K Street. Whatever your thoughts on SOPA and PIPA, it's clear that millions of people will be affected by the blackout. This is far more than a mere stunt.
