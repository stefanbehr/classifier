For some CEOs, the easiest way to get rich is to quit. 

Increasingly, corporations offer their chief executives fantastically generous severance packages retirement bonuses, extended stock options, and pensions that can add up to $100 million or more. Call 'em platinum parachutes. These deals are supposed to benefit shareholders by encouraging CEOs to take a long-term view of corporate profits, but some compensation experts have their doubts. Too many golden parachutes and too many retirement packages are of a size that clearly seems only in the interest of the departing executive, says a new report by GMI, a corporate governance consultancy. 

By way of example, the report singles out 21 CEOs whose severance packages are worth more than the median US earner would make in 49 lifetimes . In the case of GE's John Welch Jr., the figure would be 203 lifetimes. But you could still argue that the most outrageous example is Viacom's Thomas Freston, who put in just one year of work for his $100-million-plus sendoff. 

GMI, Largest Severance Packages of the Millennium
