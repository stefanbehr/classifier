As White House budget director Jack Lew prepares to take over for departing chief of staff Bill Dale y, it's worth revisiting Shahien Nasiripour's blow-by-blow of Lew's brief, less-than-illustrious stint at a unit of Citigroup that made money by betting against the housing market as it prepared to implode: 

Multi-Adviser Hedge Fund Portfolios LLC was a unit of Alternative Investments' Hedge Fund Management Group, the 36th-largest such fund of hedge funds in the world when Lew came aboard, according to a ranking by Alpha magazine, a publication that covers the hedge fund industry. 

That Multi-Adviser fund in particular had $407 million by the end of 2007, a week before Lew was named as Alternative Investments' chief operating officer At that time, it had $18 million invested in Paulson Advantage Plus LP, worth $26.4 million, comprising about 6.5 percent of the Multi-Adviser fund's total capital. 

The Paulson fund was run by hedge fund king John Paulson, the man who made billions off the deterioration of the housing industry by making bearish bets on securities tied to home mortgages particularly subprime home mortgages. 

Under Lew, the Multi-Adviser fund doubled its investment in Paulson's fund to nearly $42 million by March 2008; by the next quarter, it'd cranked that investment up to just over $60 million, making it the biggest piece of the Multi-Adviser fund, Nasiripour reported. So how'd it go for Lew and Citi?: 

Citi paid Lew $1.1 million for his year at Alternative Investments, according to an ethics disclosure report filed in January 2009. He was also eligible for an undisclosed bonus .His unit, though, lost as much as billions of dollars in 2008 as its bets turned sour. In the first quarter of 2008 alone the unit lost $509 million; the company stopped publicly disclosing the unit's individual numbers soon thereafter, but the part of the company that absorbed Alternative Investments lost $20.1 billion in 2008, according to the bank's filings with the Securities and Exchange Commission. 

Citigroup, as you might recall, also received $45 billion in TARP money. 

For all the Twitterage Lew's ascension has already stirred up, Obama has his CoS-replacing process down to a science. Aside from interim CoS Pete Rouse, a long-time Obama confidante, each of his previous appointments to the top spot spent some quality time in the financial industry: Rahm Emanuel on Freddie Mac's board of directors and Bill Daley at JP Morgan. 

But Lew's time with Citigroup should be pared against his efforts to preserve programs for low-income Americans during last year's budget and debt-ceiling negotiations. Lew also boasts a record of policy achievements geared towards protecting the most vulnerable, like helping create the Low-Income Heating Assistance Program . And his time on the board of the Center on Budget and Policy Priorities , one of Washington's premier think tanks on low-income and poverty issues, suggests that his heart isn't with Wall Street, as Mother Jones DC bureau chief David Corn reminds us . 

The New Republic's Noam Schieber explains why liberals shouldn't get too banged up over Lew's Citi record (h/t Playbook): 

It was Lew who devised and, along with White House congressional liaison Rob Nabors, executed the strategy of accepting large-looking cuts to the 2011 budget that s the one Republicans nearly shut the government down over last March and April while implementing the cuts in ways that avoided real pain . . . . 

For what it s worth, one thing I don t think liberals should get too exercised about, though they probably will, is Lew s tenure at Citigroup, where he worked between 2006 and 2008. Lew was basically the chief administrator at Citi Alternative Investments, which runs the company s portfolio of hedge funds and private-equity funds. That is, he was the guy who kept watch over the books and the paperwork, not a guy going out and placing multimillion-dollar bets or making hundred-million dollar deals. 

Tapping political operatives with ties to Wall Street is about giving confidence to the markets even though some in the president's liberal base might view such picks as sellouts. Add to that Lew's enduring popularity with Republicans , and you've got a no-brainer. 

NOTE: This post was edited to more fairly reflect Lew's policy record.
