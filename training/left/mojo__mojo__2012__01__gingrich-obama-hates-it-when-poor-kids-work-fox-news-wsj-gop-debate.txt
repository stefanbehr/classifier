During the Fox News debate in South Carolina Monday night, Newt Gingrich took on a familiar target: liberal elites who routinely thumb their noses at hard work. When asked by moderator Juan Williams about his (arguably racially charged ) statements on Barack Obama as the food stamp president , former front-runner Gingrich quickly rejected the accusations of race-baiting and pivoted to explaining one of his alternatives to government benefits. 

Gingrich repeated his call to ease child labor laws in order to allow poor kids to work as, for example, school janitors an idea that has its roots in Gingrich's controversy-laden Earning by Learning program from the early '90s. Only elites despise earning money, Gingrich said, as he accused the president of hating when poor but enterprising children tried to make their own money. 

One thing Newt forgot to mention: President Obama's American Jobs Act explicitly includes sections on summer, as well as year-round, jobs for kids in low-income families. The bill, which Gingrich derisively labeled as the American Government Rebuilding Act would allot a grand total of $1.5 billion for programs that provide employment opportunities for youths. (Specifics can be found here .) 

A billion and a half bucks is a funny way of showing how much you hate seeing schoolchildren earn their own lunch money.
