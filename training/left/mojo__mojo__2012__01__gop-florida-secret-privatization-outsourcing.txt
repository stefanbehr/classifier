Aiming to make government privatization really private. Photoillustration by Adam Weinstein 

In their longstanding fight to privatize the state's prison system and a lot of other public services Republican lawmakers in Florida are trying a new angle: doing it in secret. 

Proposed Committee Bill 7170 , introduced Tuesday in the GOP-dominated state legislature, aims to prevent information relating to the outsourcing or privatization of an agency function from being reported to the voting public until after the contract for such functions is executed. In other words, taxpayers wouldn't get to know about government work turned over to a contractor until after the contract has been signed. The bill is expected to come to a floor vote later in the recently convened spring session; with Republicans holding supermajorities in both chambers of the legislature and Rick Scott sitting in the governor's office, it could become law by this summer. 

The proposal is very disturbing, Democratic Sen. Gwen Margolis told the committee that passed the bill on Wednesday. The language is pretty broad, she said . Another local activist speaking at the hearing called the measure an attack on the transparency, accountability and due diligence of this body and citizens of this state. 

Florida has long been known for having some of the country's most stringent sunshine laws . Currently they require that any government agency planning to contract out work must release a host of supporting information to the public, including cost-benefit analyses, business plans, and an assessment of the impact such a move would have on services. The new bill gets rid of those requirements for any privatization and outsourcing plans that come from the legislature. 

If successful, the secrecy bill could draw interest from pro-privatization conservatives in other statehouses. But who's behind the audacious plan? The bill was introduced by the Senate's rules committee, which means the identity of the bill's author is...still a secret. 

I don't know why we are undoing these [laws] now, Matt Puckett, leader of the state's old corrections officers' union, testified . I think these are put in place so we don't make mistakes when we privatize. 

Florida lawmakers have had plenty of such privatization plans; chief among them was a controversial agenda, passed last year, to hand most of the state's prisons over to contractors a measure deemed unconstitutional by a state court after Puckett's union successfully sued to stop it. The new bill specifically exempts contractual arrangements with private entities for operation and maintenance of correctional facilities and supervision of inmates. But opponents say it could be used to outsource a lot of state government business to the private sector before Floridians even realize it's happening. 

Proponents say it will save millions of taxpayer dollars, especially if it can be used to push through the divisive jails plan. JD Alexander, the Senate's powerful budget chairman , said privatizing prisons could save the state $40 million. I think this has the promise, contrary to what the critics say, of offering not only savings but the potential for better outcomes, he said at the hearing. 

If that's true if privatizing any public agency or its work is the best financial and ethical policy for Florida's citizens then why do it in secret? That question wasn't directly answered in Wednesday's hearing. When the GOP-led rules committee approved the secrecy bill, however, it did publish a state-mandated analysis [PDF] of its provisions. Under Public Records/Open Meetings Issues, the analysis listed: None.
