On Sunday, Citizens United turned two. In case you're not familiar with the birthday kid, it's the 2010 Supreme Court decision that ruled that corporations can pour unlimited money into groups supporting or opposing candidates. The result has been the rise of a parallel world of super-PACs and shadowy nonprofits dumping millions into the 2012 election, kind of like a giddy toddler dumping Cheerios all over the floor. But far less adorable. 

As Citizens United enters its Terrible Twos, we're throwing a birthday party with some selections from MoJo 's ongoing dark-money coverage . Won't you join us? 

Directions to the bash: Want to party like a politically connected millionaire? Let this handy flowchart show you how to make Citizens United work for you. 

Who's invited? Check out our list of the 2012 race's 20 top donors (so far) , as well as our exclusive list of the superrich donors who have pledged to contribute $1 millon to the Koch Brothers' efforts to defeat Barack Obama. And meet the mystery man behind Mitt Romney's super-PAC . 

RSVP not required: There may be some unexpected guests finding out who's pouring money into elections is now harder than ever . 

Meet the proud papa: Read Stephanie Mencimer's profile of James Bopp , the mastermind behind the Citizens United case (who's got more tricks up his sleeve ). 

No cake for you: Wonder how Citizens United boosts the 1 percent and weakens the majority of Americans' political influence? MoJo editors Monika Bauerlein and Clara Jeffery explain . 

There will be fun and games : Trying to find out who's behind 501(c)4s, supershadowy fundraising groups that don't have to say where they get their money, is kind of like playing pin-the-tail-on-the-donor. 

and a clown: Join comedian Stephen Colbert for a surreal civics lesson about how super-PACs totally don't collude with candidates (wink, wink). Plus: Some of his most bizarro political ads . 

And many more! : Not if anti- Citizens United reformers get their way. Read up on their plans to roll back the ruling . 

Print Email Tweet We re Still at War: Photo of the Day for January 23, 2012 Newt s Florida Suicide Strategy Dave Gilson 

Senior Editor 

Dave Gilson is a senior editor at Mother Jones . Read more of his stories , follow him on Twitter , or contact him at dgilson (at) motherjones (dot) com. RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Candidates and the Totally Unrelated Super-PACs That Love Them 

Nothing to see here, folks! Presidential super-PACs that just happen to be run by former Romney, Obama, and Gingrich staffers. Can Citizens United be Rolled Back? 

Behind the fight to overturn the Supreme Court ruling that unleashed a torrent of corporate election spending. Rick Santorum: Anti-Citizens United Amendment Is Horrible 

Santorum is just fine with corporations pouring their cash in America's elections. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
