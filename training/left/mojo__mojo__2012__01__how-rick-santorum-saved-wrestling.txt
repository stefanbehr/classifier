Rick Santorum's effort in Iowa received a late boost from Jim Gibbons, the much-revered former wrestling coach at Iowa State University, who endorsed the GOP presidential candidate at a Pizza Ranch in Boone on Monday. In a caucus, where voters can be pressured by their peers right up to the minute they cast their votes, these kinds of endorsements tend to carry a lot of weight. But there's another sub-plot to it all: Rick Santorum has sort of a weird fixation with wrestling. 

As Mike Newall reported in his excellent 2005 Philadelphia City Paper profile , prior to getting involved in politics, Santorum worked at a law firm, where he once argued in court successfully that pro wrestling should be exempt from steroid regulations because it's staged (and therefore not a sport). Jake Tapper flags a 2010 quote from the Philadelphia Inquirer in which Santorum spins his wrestling work in small-government terms: Pennsylvania was the most pernicious of states when it came to regulation. They made you pay all this money to the boxing [athletic] commission. They used to just rape these guys. You d have to pay a certain percentage of the gate receipts to have these officials just stand around and watch the match. It was ridiculous. (Emphasis mine.) 

And because three makes a trend here's a Rick Santorum campaign ad from 2006, which has been making the rounds today. It stars Rick Santorum (obviously), using the spectacle of mostly-naked men wrestling as a metaphor for what's wrong with Washington. (If nothing else, he seems to have anticipated the Chris Lee/Anthony Weiner scandals): 



What would Rick Santorum's wrestling name be? We're going with The Vest.
