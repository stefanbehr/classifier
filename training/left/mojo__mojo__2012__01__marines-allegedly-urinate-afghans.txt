[NOTE: This post is being regularly updated with new developments, including reactions from US authorities, the Taliban, and the media. Scroll to the bottom for the latest.] 

The Marine Corps is investigating a YouTube video posted early Wednesday that appears to show four Marines urinating on the heads of Afghans they'd just killed in a firefight. Have a great day, buddy, one of the Marines can be heard saying on the footage. 

The video was posted to YouTube by a user calling himself semperfilonevoice, a play on the Corps' Semper Fidelis motto that suggests the poster might be a Marine with regrets about the warfighters' conduct. (The video, posted by London's Daily Telegraph and TMZ earlier today, is also reposted below. Warning: It contains graphic content. ) 

The poster of the video alleges that the urinators are members of Scout Sniper Team 4, an elite advance combat unit within the Camp Lejeune, N.C.-based Third Battalion, Second Marines. Their identities remain unknown at this point, but the video does contain at least one clue suggesting that's plausible. One service member in the video can be seen holding an M40 rifle , which is typically issued to sniper teams, but not to regular line units. Elements of the 3/2 Marines have seen some fierce fighting in Afghanistan, including a deployment last year to the province of Now Zad called Apocalypse Now Zad by some in which seven American fighters lost their lives. 

Attempts by Mother Jones to contact the poster of the video were unsuccessful; calls to the 2nd Marine Division the 3/2's parent unit and to the Pentagon seeking further information about the video were not immediately returned. But a Marine spokesperson told TMZ that the video would be fully investigated. While we have not yet verified the origin or authenticity of this video, the actions portrayed are not consistent with our core values and are not indicative of the character of the Marines in our Corps, she said. It's also important to note that it remains unclear as to where and when the video footage was taken. 

According to the Geneva Conventions , which the US military observes, combatants must at all times, and particularly after an engagement...search for the dead and prevent their being despoiled. They are also required to ensure that the dead are honourably interred, if possible according to the rites of the religion to which they belonged, that their graves are respected, grouped if possible according to the nationality of the deceased, properly maintained and marked so that they may always be found. (The UK's rules for its military members are even more explicit, threatening court-martial for any soldier for maltreatment of a dead enemy.) 

The recently posted video hasn't yet caused a serious outcry at home or abroad, but it certainly has that potential. Perceived and real American offenses against Muslims have touched off angry riots and anti-US anger in the past from plans to burn Korans , to the rumored use of pork-coated bullets and rifles bearing Bible verses against Muslim targets, to stepping and urinating on detainees' Islamic holy books at Guantanamo. 

Sentiments were divided among some YouTube commenters regarding the video. You must be living under a rock, have you EVER seen the videos of the Taliban with dead body pieces, one defender of the alleged Marines wrote. This video is nothing, ABSOLUTELY nothing compared to what they did to us Americans. 

But one commenter, who identified himself as a veteran, was less willing to dismiss the behavior in the video: Thanks fellas, you just pissed away everything me and my boys fought for. 



UPDATE 1, Thursday, Jan. 12, 7:00 a.m. EST: The original video has been removed from YouTube by the user; thanks to MoJo reader Craig Boehman for providing the copy below. 



UPDATE 2, Thursday, 10:00 a.m. EST: In a statement to the BBC , Pentagon spokesman and Navy Capt. John Kirby said: We are deeply troubled by the video. Whoever it is, and whatever the circumstances which we know is under investigation it is egregious behaviour and unacceptable for a member of the military. The Marine Corps headquarters gave a similar message: The actions portrayed are not consistent with our core values and are not indicative of the character of the Marines in our Corps. This matter will be fully investigated.'' 

UPDATE 3, Thursday, 1:30 p.m. EST: A Taliban spokesman tells the Christian Science Monitor that the video makes no difference in the group's ongoing peace talks with the Western powers. It's not a new thing that has happened. It's normal with the American forces and their allies. The foreign forces have always discriminated and abused human rights in Afghanistan, Qari Yousef Ahmadi told the news site. He added, however, It's an act that makes a person feel ashamed to watch it or talk about it. 

As media reactions to the video go, few so far are as powerful as this one from Hamilton Nolan of Gawker , titled Piss on War: Death, Desecration, and Afghanistan. It must be read in its entirety, but here's a graphic excerpt: 

Excerpt from Piss on War : Courtesy Gawker 

UPDATE 4, Thursday, 1:40 p.m. EST: A Marine Corps source speaking anonymously tells Reuters that the service has confirmed the video is authentic, and it's identified the unit shown in the tape. It's believed to be a contingent of the 3/2 Marines, which is consistent with the statements of the anonymous user who originally published the video to YouTube. 

At a press conference, Secretary of State Hillary Clinton addressed what she called the deplorable behavior in the video, adding that the United States remains strongly committed to helping build a secure, peaceful, prosperous, democratic future for the people of Afghanistan : 



UPDATE 5: Thursday, 4:00 p.m. EST : My colleague Adam Serwer flags a blog post from Islamophobe extraordinaire Pam Geller extolling the video. She writes: I love these Marines. Perhaps this is the infidel interpretation of the Islamic ritual of washing and preparing the body for burial. Surprisingly, there's more fascinating backstory here, and Adam's got it . 

UPDATE 6: Thursday, 6:45 p.m. EST : Reuters reports that two of the four Marines seen in a video have been identified. Apparently the footage could be from last year: According to an unnamed Marine Corps official, the two men identified are still part of the 3rd Battalion, 2nd Marines, based out of Camp Lejeune, and their unit served in Afghanistan's Helmand province from March until September of 2011. 

UPDATE 7: Thursday, 8:30 p.m. EST : Geller isn't the only right-winger to offer a repugnant response to the video. Radio host Dana Loesch who is also employed by CNN, where she appears regularly as a political commentator cheered the Marines' behavior and said, I'd drop trou and do it too. 

UPDATE 8: Friday, Jan. 13, 1 p.m. EST: Where do the GOP presidential hopefuls stand on the Marine video? There's no way of knowing, because not one has released a statement on it, and no reporter has yet pressed them for an opinion. Even though 2008 presidential nominee John McCain said the video does great damage and makes him sad, and the commandant of the Marine Corps condemned it, and a Medal of Honor recipient said there was no excuse for it , and a bevy of Marines and other combat veterans past and present have said they're universally disgusted by it and ashamed, there's been nary a word of condemnation or even acknowledgement from the GOP 2012 field. The candidates have long attacked President Obama's anti-terrorism and war strategies and made the case that they can keep America safer, but on this issue they're curiously silent. There are three more Republican debates between now and Tuesday, when voters in South Carolina select their preferred candidate; it remains to be seen whether the aspiring commanders-in-chief will address the Marines' behavior, and its implications for US foreign policy and a culture of respect, dignity, and the rule of law. 



UPDATE 9: Friday, 2:10 p.m. EST: CNN pundit Dana Loesch has doubled down on her comments from yesterday. I was using absurdity to highlight absurdity, she says, claiming that progressives have distorted her message and are attacking her unfairly. Decide for yourself what she meant here's a more complete transcript of what she said on her radio show on Thursday: 

Now we have a bunch of progressives that are talking smack about our military because there were marines caught urinating on corpses, Taliban corpses....Can someone explain to me if there's supposed to be a scandal that someone pees on the corpse of a Taliban fighter? Someone who, as part of an organization, murdered over 3,000 Americans? I'd drop trou and do it too. That's me though. I want a million cool points for these guys. Is that harsh to say? Come on people, this is a war. What do you think this is? 

Loesch also said this morning that the Left is attacking me so they can avoid calling this Obama's Abu Ghraib. It can't be Obama's fault like it was Bush's. It's long been known that the atrocities at Abu Ghraib were the result of policy designed and directed from inside the Bush White House. On what basis does Loesch equate the behavior of these four Marines? That's a question she's not answering. 

Meanwhile, CNN which has paid Loesch since last February to be part of the Best Political Team on Television is distancing itself from the situation. Spokesperson Edie Emery said in a statement to Politico, CNN contributors are commentators who express a wide range of viewpoints on and off of CNN that often provoke strong agreement or disagreement. Their viewpoints are their own. 

UPDATE 10: Friday, 3:15 p.m. EST: The Marines have appointed a general to handle the investigation into the urination video and decide what disciplinary action should be taken against the service members who appeared in or distributed the video, Stars and Stripes reports. Lt. Gen. Thomas Waldhauser's investigation will be in addition to a criminal probe already set up by the Naval Criminal Investigative Service. Additionally, the commander of NATO forces in Afghanistan announced the military will run mandatory training for all troops on how to handle casualties. I require all [NATO] personnel to treat all coalition, Afghan National Security Forces, civilians and insurgent dead with the appropriate dignity and respect, the commander, Lt. Gen. Curtis Scaparrotti, said Friday. 

Rep. Allen West (R-Fla.) wrote an email, published by the Weekly Standard, castigating critics of the Marines shown in the video. All these over-emotional pundits and armchair quarterbacks need to chill. Does anyone remember the two Soldiers from the 101st Airborne Division who were beheaded and gutted in Iraq? He conceded that the Marines should be punished administratively, the lowest level of discipline permitted under military law, and added, As for everyone else, unless you have been shot at by the Taliban, shut your mouth, war is hell. West, an Iraq vet who resigned from the Army after menacing and allegedly mock executing an Iraq police officer during an interrogation, is a tea party GOP freshman who's gained notoriety for uncivil and Islamphobic statements in the past. 

By contrast, TIME magazine's Nate Rawlings, who also served two tours in Iraq as an Army combat officer, writes that while killing is a part of the job, it is not a sport or a game. You can't teach someone how to be human, he writes, but you can lead and inspire and teach and cajole and most importantly supervise young troops. That s the way to prevent these things from happening again. 

UPDATE 11: Sunday, 1:30 p.m. EST: A Republican presidential candidate has finally weighed in on the video controversy expressing outrage at an alleged anti-military bias in the US government, rather than at the Marine kids shown urinating on corpses in the video. These kids made a mistake, there's no doubt about it, Rick Perry told CNN's Candy Crowley Sunday morning (video below). But to call it a criminal act, I think, is over the top. Perry also suggested that Patton and Churchill had engaged in similar behavior in their times. Obviously, 18, 19-year-olds make stupid mistakes all too often, Perry said. What's really disturbing to me is just, kind of, the over-the-top-rhetoric from this administration and their disdain for the military. As previous updates show, the Marines, the Naval Criminal Investigative Service, and the three-star Army general in charge of Western forces in Afghanistan all took clear stances against the video and initiated the criminal investigations. It's unclear whether Perry meant to argue that these military institutions, too, were showing disdain for the military. 



UPDATE 12: Sunday, 3:30 p.m. EST: Animal activists are using the Marine video furor to draw attention to another video , uploaded anonymously to the internet last November, that appears to show US Army soldiers beating a sheep to unconsciousness (and possibly death) by repeated blows to the head with an aluminum baseball bat. (Warning: The video is graphic and disturbing.) Several of the soldiers can be heard laughing as the sheep attempts to stand up and is hit, again and again. 

PETA President Ingrid Newkirk discussed the video on Huffington Post Friday. PETA did what it always does when someone blows the whistle on these incidents of gratuitous cruelty: We wrote to Secretary of the Army John McHugh [ PDF ] and then, when no answer was forthcoming, to other high-ranking officers, she wrote. No one not PETA and not the thousands of people who have seen this video and are rightly disturbed by it has received any acknowledgment, not even a single comforting word, that an investigation has been started.
