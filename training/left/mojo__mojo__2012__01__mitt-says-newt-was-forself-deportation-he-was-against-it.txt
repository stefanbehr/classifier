Fielding questions from Univision's Jorge Ramos Wednesday afternoon, former Massachussetts Governor Mitt Romney said Newt Gingrich was for self-deportation before he was against it. 

Romney's right: Gingrich once endorsed the strategy of making everyday life for unauthorized immigrants so miserable that they would self-deport. Speaking to Ramos on Univision earlier on Wednesday, Gingrich said that self-deportation was an Obama-level fantasy. He didn't mention that he'd endorsed the idea fairly recently. Patricia Mazzei of the Miami Herald hunted down the quote, which comes from an appearance Gingrich made on the Laura Ingraham show in 2010. We have to enforce this law, Gingrich tells Ingraham . We have to do that first. No work, self-deportation. Come back. We can figure out our immigration system after we enforce this border. But I just think you're not going to get the support of the people unless we really see that border enforced. 

Gingrich has been running an ad in Florida calling Romney an anti-immigrant candidate, but the campaign withdrew the ads after Florida's Cuban American Republican Sen. Marco Rubio said the ad was inaccurate, inflammatory, and doesn't belong in this campaign. Romney told Ramos Wednesday that it was very sad for a candidate to resort to that kind of epithet. 

Romney also suggested that Gingrich's criticism of self-deportation was just pandering. It's very tempting to come to an audience like this and tell people what they want to hear, said Romney, who's been on both sides of immigration issues a few times himself . 

There's a limit to how far Romney's willing to go, however: He's not willing to tell Latino voters he's Mexican American. When Ramos asked whether Romney, whose father was born to US citizens in Mexico, would count as the First Mexican President of the US, the former governor said no. 

I'd love to be able to convince people of that in a Florida primary Romney joked. I don't think people would think I was being honest with them if I said I were Mexican American, but I'd appreciate it if you'd get that word out.
