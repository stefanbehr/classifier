Despite banking another $5 million from billionaire casino tycoon Sheldon Adelson OK, technically Adelson's wife Winning Our Future, the pro-Newt Gingrich super-PAC, is getting trounced by Mitt Romney forces in the ad wars leading up to Florida's primary on Tuesday. 

Quoting a source monitoring the Sunshine State ad war, Politico reports that the Romney campaign and pro-Romney super-PAC Restore Our Future spent $15.3 million on television ads in Florida. That's 450 percent more than Gingrich's campaign and super-PAC Winning Our Future spent on TV ads. And that doesn't include money spent on direct mail sent to voters, get-out-the-vote efforts, and other non-TV campaigning. 

In other words, just as Gingrich clinched a double-digit victory in South Carolina after blitzing the airwaves there with ads attacking Romney, Romney and his allies are doing the same in Florida. And boy is it paying off: Romney now leads Gingrich by 12 percentage points, according to a Reuters/Ipsos poll released Sunday. A separate Miami Herald poll showed Romney trouncing Gingrich among Florida Hispanic voters, 52 percent to 28 percent. 

Restore Our Future is already laying the groundwork for Romney wins in other key primary states. According to ProPublica , the deep-pocketed super-PAC has already spent $52,000 attacking Gingrich in Nevada (Feb. 4 caucus), $120,000 attacking him in Arizona (Feb. 28 primary), and $168,000 attacking him in Michigan (Feb. 28 primary). You can bet those sums will increase dramatically in the coming weeks unless, that is, Gingrich bows out, in which case Restore Our Future and its political guru, Carl Forti , will have done their job. 

Print Email Tweet The Air Force s Latest Budget Bungle Randall Terry s Gory Super Bowl Abortion Ad Gets Intercepted Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Newt Gingrich Now Promising a Blood Feud for the Ages 

Candidates Hate Super-PACs (But Love Them, Too) 

Mitt Romney and Newt Gingrich say they're a "disaster" and "totally irresponsible"--when they're the ones being attacked. Video: 6 Bizarro Ads From the Colbert Super-PAC 

Hilariously weird political ads from mostly fake presidential candidate Stephen Colbert. Candidates and the Totally Unrelated Super-PACs That Love Them 

Nothing to see here, folks! Presidential super-PACs that just happen to be run by former Romney, Obama, and Gingrich staffers. Pro-Romney Super-PAC s New Ad: Newt = Obama 

The outside spending heavyweight Restore Our Future continues its barrage against a struggling Gingrich. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
