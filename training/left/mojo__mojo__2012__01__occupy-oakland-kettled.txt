Another weekend, another Occupy protest. Living in downtown Oakland, it has started to feel routine. But the January 28th protest was promising to be an escalation with protesters planning to take over a vacant building, not just a park or plaza and when I started hearing reports of tear gas, rubber bullets, and flash grenades, I gathered my camera gear and headed down to Frank Ogawa/Oscar Grant Plaza. 

Occupy protesters stand behind shields as they begin to march up Telegraph Avenue. 

Right after a march started, just past 5 p.m., protesters at the front made an abrupt left at 16th street and started sprinting. A building they intended to occupy, the Traveler's Aid building, was just down the street. I hauled ass to get to the front, to get shots of them entering and taking over the building. A metal gate drawn across the front of the building thwarted the protesters. They pleaded with workers repairing windows, which had been broken earlier, to open the gate and let them take over the building. The workers wanted nothing to do with it. Don't put us in the middle of this, one said. We're just here doing our job. 

An Occupy marcher uses an iPad to capture the scene of protesters marching through Oakland. 

The protesters continued down to the end of San Pablo Avenue, at 16th. It was here that I first noticed the easy likelihood of police blocking both ends of the street, and kettling everyone in between. Protesters filled the canyon of 16th between San Pablo and Telegraph. 

Occupy protesters carry a tent as they march through downtown Oakland. 

The protest moved up San Pablo, a wide, open street, then turned down 20th towards Henry Kaiser park, which Occupy Oakland had briefly taken back in November after being ousted from Frank Ogawa Plaza. It was another prime situation in which to be kettled narrow streets, with large condos on all sides. And this time it happened: A line of police moved in from Telegraph, not letting anyone in the crowd out. Another line moved in from the opposite direction. I got cut off from the main protest, along with a few Occupy medics. We made our way around to Telegraph, on the other side of the kettle. A block away, in the kettle, a flash grenade went off. Two girls on bikes pleaded with police to be let out. Then, a large group of protesters broke down a recently re-erected chainlink fence enclosing a vacant lot next to the park. Protesters flooded the lot, breaking free of the kettle. The march resumed up Telegraph Avenue. 

After being blocked on all sides by the Oakland Police, protester break through a fence to escape and resume their march. 

Police respond to protesters breaking out of the blockade. 

At this point, it didn't seem like the march had any real direction, moving up Telegraph to 27th Street. Some protesters started to turn left, others kept going straight. Some called for people at the front to slow down, to keep everyone together. Others wanted to move ahead, to keep the police from blocking intersections. Meanwhile, the police formed a strong line right behind the protesters. 

Occupy marchers walk up Telegraph. 

Next to the Kentucky Fried Chicken at 28th and Telegraph, it seemed like the police were going to surround the group. Protesters were pushed into the KFC parking lot, and police lines cut them off from both sides. Some people started down 28th. I casually walked down the sidewalk, directly into the police line. 

Occupy Oakland approaches 28th and Telegraph. 

Two police approached me and my brother, who also didn't feel like getting arrested. I told them I was a member of the media and showed my press passes. The cop in front of me was boiling with adrenaline. He grabbed the passes and barked, Are these current? He scanned them for a date. After a second, he let go and let us pass the police line. This turned out to be lucky for us, since just a few minutes later, the police would effectively kettle everyone at the downtown Oakland YMCA, arresting everyone who didn't manage to get away by scaling a fence into a nearby parking lot. The mass arrest included at least six journalists including MoJo's Gavin Aronsen, who wrote about his interaction with the Oakland Police and trip to county jail here. 

Occupy protesters, before finally getting kettled by the Oakland Police 



Do you appreciate fair and factual reporting on Occupy Wall Street? Please donate a few bucks to help us expand our coverage .
