Following a third-place finish in the Iowa caucuses, Ron Paul held a boisterous rally, featuring a speech from Army Corporal Jesse Thorsen. Thorsen, who was in uniform, voiced impassioned support for Paul's non-interventionist views. We don't need to be picking fights overseas, he said, and pledged to help make sure this man is the next president of the United States. 

It was an understandable sentiment from a soldier who said he had served in the military for 10 years, which included tours in Iraq and Afghanistan. But the appearance likely violated the protocols for service members included in Defense Department Directive 1344.10 , which states explicitly that they are not to participate in political rallies as anything more than spectators. And if they do attend a political function, they're not supposed to do so in uniform. 

Active-duty service members can register, vote, and express a personal opinion on political candidates and issues, but not as a representative of the Armed Forces, the directive states. It also stipulates: 4.1.2. A member of the Armed Forces on active duty shall not: 4.1.2.1. Participate in partisan political fundraising activities (except as permitted in subparagraph 4.1.1.7.), rallies, conventions (including making speeches in the course thereof), management of campaigns, or debates, either on one's own behalf or on that of another, without respect to uniform or inference or appearance of official sponsorship, approval, or endorsement. Participation includes more than mere attendance as a spectator. 

And it says that service-members shall not: 4.1.2.5. Speak before a partisan political gathering, including any gathering that promotes a partisan political party, candidate, or cause. 4.1.2.6. Participate in any radio, television, or other program or group discussion as an advocate for or against a partisan political party, candidate, or cause. 

My immediate reaction, upon watching Congressman Paul's event, was that the soldier in question was in flagrant violation of department of defense regulations, said Eugene Fidell , who teaches military justice at Yale Law School. Lord knows there are people in the military, as in the rest of American society, who have very strong feelings about who is elected president. But the tradition is the military stays out of partisan politics. 

The issue wasn't necessarily showing up in uniform; it was speaking out at a partisan political gathering. If he was on active duty, it wouldn t matter if he was wearing a Santa Claus costume or his birthday suit, said Fidell. Wearing the uniform only makes it worse. 

Thorsen could be formally reprimanded for doing so, with a court-martial or an administrative discharge, though Fidell didn't seem to think that was very likely. More likely, he could get chewed out by a superior. Even if Thorsen volunteered, the Paul camp should have taken this into account before inviting him on stage last night. (The Paul campaign did not respond to a request for comment. We'll update the post if they do.) 

UPDATE: The Washington Post talked to a spokesperson for the Army Reserve command who told the paper that as of October, Thorsen was not on active duty. They are determining whether he violated any rules by speaking at the rally. 

Adam Serwer contributed reporting for this post.
