Senate Majority Leader Harry Reid (D-Nev.) has postponed Tuesday's scheduled procedural vote on the controversial Protect IP Act, or PIPA. Here's his statement : 

In light of recent events, I have decided to postpone Tuesday's vote on the PROTECT I.P. Act. 

There is no reason that the legitimate issues raised by many about this bill cannot be resolved. Counterfeiting and piracy cost the American economy billions of dollars and thousands of jobs each year, with the movie industry alone supporting over 2.2 million jobs. We must take action to stop these illegal practices. We live in a country where people rightfully expect to be fairly compensated for a day's work, whether that person is a miner in the high desert of Nevada, an independent band in New York City, or a union worker on the back lots of a California movie studio. 

I admire the work that Chairman Leahy has put into this bill. I encourage him to continue engaging with all stakeholders to forge a balance between protecting Americans' intellectual property, and maintaining openness and innovation on the internet. We made good progress through the discussions we've held in recent days, and I am optimistic that we can reach a compromise in the coming weeks. 

Minority Leader Mitch McConnell's (R-Ky.) request to delay the vote and Rep. Lamar Smith's (R-Tex.) decision to postpone work on SOPA , PIPA's counterpart in the House, surely contributed to Reid's decision. The calculated retreats of a number of both bills' original co-sponsors and the blackout on Wednesday mattered, too. 

So what happens next? Reid will probably continue his doomed quest for a non-existent compromise between content providers (Hollywood) and the tech community (Silicon Valley). Remember, Sen. Dianne Feinstein (D-Calif.) an original co-sponsor of PIPA whose state hosts both communities tried and failed to broker a compromise between the two groups in December . Nothing that's happened since would improve the odds of a deal. 

As last night's presidential debate showed, Republicans seem to understand that anti-SOPA/PIPA sentiment aligns rather handsomely with conservative values like free speech, fewer regulations, and less government. And the GOP knows full well that Hollywood traditionally a core, generous constituency for Democrats will be none too pleased with Reid's decision to bow before the might of web geekdom. 

For Republicans, opposing SOPA is a win/win. The only question is why they didn't understand that earlier.
