In the past 10 days, the pro-Mitt Romney super-PAC Restore Our Future and the pro-Newt Gingrich super-PAC Winning Our future dumped $10.4 million trying to sway Republican voters in the Sunshine State. That's about as much as the total all outside groups had spent at this point in the 2008 election cycle. Where did the money go? MoJo tallied up their spending based on what they'd reported to the Federal Election Commission. 



Most of Restore Our Future's spending went to attacking Gingrich. For every dollar spent promoting Romney, the super-PAC spent $25 dragging Gingrich through the mud. 



Of the $12 million Winning Our Future has spent so far in this cycle, more than a third left the PAC's coffers in the past week. It spent most of the money talking up Gingrich; only a sliver was dedicated to attacking Romney. 



How did they reach their audience? Restore our Future spent the bulk of their money, $5 million, buying up television and radio spots slamming Gingrich for having tons of baggage. Since the beginning of January, in Florida alone, Restore our Future has run 3,443 television spots, 1,018 cable ads and 234 radio ads attacking Gingrich. 





Winning our Future spent $4 million on TV and radio advertising (with at least $1.5 solely on radio). 





Print Email Tweet We re Still at War: Photo of the Day for February 1, 2012 Occupy Oakland s Black Panther Roots Jialu Chen 

Editorial Intern 

Jialu Chen is an editorial intern at Mother Jones . For more of her stories, click here . 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Super-PAC Mania 

Romney's haul, Obama's Hollywood bundle, Colbert's lolz: highlights from campaign spending central. Candidates Hate Super-PACs (But Love Them, Too) 

Mitt Romney and Newt Gingrich say they're a "disaster" and "totally irresponsible"--when they're the ones being attacked. Reining in Super PACs Won t Be Easy 

Super PACs Now Dominate GOP Primary Campaign Spending 

Candidates and the Totally Unrelated Super-PACs That Love Them 

Nothing to see here, folks! Presidential super-PACs that just happen to be run by former Romney, Obama, and Gingrich staffers. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
