The DREAM Act, a bill that was once supported by a number of Republican moderates , has become toxic for ambitious GOPers ever since it was embraced by President Obama. The latest iteration of the proposal would provide a path to citizenship for unauthorized immigrants brought to the US as children if they're planning to graduate from college or join the military, provided they meet strict behavior requirements over a decade. It failed to clear the Senate during the 2010 lame-duck session. 

During Monday night's debate, however, the Republican consensus shifted just a smidgen to the left, as both Newt Gingrich and Mitt Romney endorsed the idea of a military-only DREAM Act, an idea once embraced by their fallen rival Rick Perry . 

If you live in a foreign country, and you are prepared to join the American military, you can, in fact, earn the right to citizenship by serving the United States and taking real risk on behalf of the United States, Gingrich said. That part of the DREAM Act I would support. 

I would not sign the DREAM Act as it currently exists, Romney agreed, but I would sign the DREAM Act if it were focused on military service. 

Gingrich has signaled support for the military-only idea before, while Romney has previously rejected the DREAM Act wholesale . Still, Romney also once supported George W. Bush's comprehensive immigration reform plan, so you never really know what he's going to say. And Romney's made it clear he's still on board with forcing the parents and spouses of these prospective veterans to self-deport if they happen to be undocumented. 

The DREAM Act is premised on two basic arguments: Children should not be punished for the sins of their parents, and it's absurd for the United States to jettison potential high skilled workers who are not to blame for their own undocumented status. The military-only DREAM Act treats military service as a kind of punishment and levies a price for citizenship that neither Republican candidate was himself willing to pay.
