Faced with empty coffers, desperate governors and state lawmakers will try just about anything to improve their cash flow. 

Puppy power: California Gov. Jerry Brown is selling t-shirts featuring his corgi, Sutter , and promises to donate $3 from each purchase to the Golden State's general fund. 

Pole tax: In 2007, Texas Gov. Rick Perry instituted a $5 tax on strip club patrons to fund sexual-assault prevention and state health insurance. It has since brought in $15 million. 

Frack party! After he proposed slashing the state education budget by $2 billion, Pennsylvania Gov. Tom Corbett suggested the state university system open up six of its campuses to natural-gas extraction. 

Pass the hat: Faced with a costly court challenge to its draconian abortion consent law, South Dakota is accepting donations to cover $750,000 in legal fees. Less than $65,000 has come in. 

Plane dealing: In 2006, then-Alaska Gov. Sarah Palin pledged to sell off the state's private jet on eBay . That didn't pan out; the jet, first bought for $2.7 million, was eventually sold for $2.1 million. 

School's out...forever: Utah state Sen. Chris Buttars estimated that eliminating the 12th grade would knock $60 million out of the state's $700 million deficit. His fellow legislators flunked the idea. 

The honesty tax: Arizona state Rep. Judy Burges proposed adding an I Didn't Pay Enough option to state income tax filings. Burges estimated it could net an extra $12 million a year; in its first year, it brought in just $13,204. 

Venture capitol: In 2010, Arizona Gov. Jan Brewer approved the sale of three capitol buildings for $81 million. In January, Brewer said she'd buy them back from the investors the state had been leasing them from at a cost of $106 million. 

Image: Cafe Press; Terraxplorer/iStockPhoto; State of Alaska; State of Arizona; Graffizone/iStockphoto.
