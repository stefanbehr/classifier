A pilot takes the F-35 Lightning II joint strike fighter up for its first night flight near Edwards Air Force Base, California, on January 18, 2012. (Courtesy photo Tom Reynolds/Lockheed Martin)
