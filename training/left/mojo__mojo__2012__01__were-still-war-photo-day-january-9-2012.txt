US Army Spc. Kimberly Nicholls, 82nd Combat Aviation Brigade mans her M240 Bravo machine gun during sunset while flying over Logar province, Afghanistan on December 8, 2011. Photo by the US Army.
