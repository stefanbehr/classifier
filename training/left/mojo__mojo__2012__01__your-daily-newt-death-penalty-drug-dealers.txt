A s a service to our readers, every day we are delivering a classic moment from the political life of Newt Gingrich until he either clinches the nomination or bows out. (Daily Newt is back from a brief sabbatical following Newt around South Carolina.) 



Ross Douthat's criticism notwithstanding, Newt Gingrich is very much a man of ideas so many ideas, in fact, that he often ends up floating vastly contradictory proposals within a manner of just a few years. As Daily Newt explained previously , Newt Gingrich wrote a letter to the Journal of the American Medical Association in 1981 calling for marijuana to be legalized for medical purposes. [L]icensed physicians are competent to employ marijuana, he wrote at the time. Pragmatic! 

Flash-forward to 1996, and Gingrich's views had shifted to the right, and then kept going for a little while past that. Gingrich was the lead sponsor of the Drug Importer Death Penalty Act, which, as its name suggests, would have made importation of even a small amount of marijuana punishable by life imprisonment (first offense) and death (second offense): 

How much is 100 usual dosage amounts of pot? About two ounces more than the usual Friday afternoon with Snoop Dogg, but well beneath the load carried by the serious drug traffickers Gingrich's law was purportedly targeting. Our friends at Weedguru inform us that an ounce can last a month for some smokers, but if you smoke multiple times a day it will vary from 1 week to 4 weeks. The law would be just as likely to target college kids coming back from a long night in Tijuana as it would members of an international drug cartel.
