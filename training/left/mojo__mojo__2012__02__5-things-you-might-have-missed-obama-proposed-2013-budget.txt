Just in time for Valentine's Day , the White House gifted to Congress the president's proposed budget for the 2013 fiscal year. President Obama has been pretty clear that the budget is also meant to draw a stark contrast between him and the Republican opposition. Conservative pundits and politicos have been equally clear that they intend to use the proposed budget as more proof that Obama is a die-hard economy wrecker. 

There are plenty of hot-button items sure to provide much grist for the attack-ad-mill as November approaches: Taxing the rich , trimming military spending , cutting spending on some federal health programs , and so forth. 

Here are a few things you might have missed about the 2013 budget released on Monday: 

1. Arab Spring nations get modest earmarks: The Obama administration wants to allot $800 million in economic aid to certain countries affected by Arab Spring uprisings. State Department officials told Reuters that the bulk of the sum would go to initiatives [supporting] long-term economic, political, and trade reforms for countries in transition such as Egypt , Tunisia and Yemen . 

Of that amount, $770 million would be set aside for creating the Middle East and North Africa Incentive Fund, which would provide aid to governments prepared to make reforms proactively, the budget document notes. 

Republican lawmakers like Sen. Jeff Sessions (R-Ala.), the leading GOPer on the Senate budget committee, is skeptical for all the expected reasons : 

First, I'm not sure who'll we'll be negotiating with, and who you could give the money to. And there seems to be some awfully extreme views within the Arab Spring movement. I think we have to be very careful that any money we provide would be well spent. 

2. NASA gets screwed (sort of): Over the years, President Obama has attracted bipartisan criticism for his plans to cut back on pricey space programs. (In 2010, former astronaut Neil Armstrong called Obama's original plan to nix the Constellation program devastating . ) 

The 2013 budget does include a $200-million bump to manned exploration , along with funding for other space-science goodies and pet projects. But the budget calls for taking the axe to NASA funding in general. Adam Mann at Wired has a solid rundown : 

The request takes a deep bite out of Mars and outer-planet science exploration in particular. NASA's funding would fall to the lowest level in four years, with a total budget of $17.71 billion. The president's request projects a flat budget through 2017, with no growth to even account for inflation. We are having to make tough decisions because these are tough economic times, said NASA administrator Charles Bolden during a press conference Feb. 13. 

As expected, planetary science in particular Mars exploration and outer-planets missions is the biggest loser, getting a $309 million decrease compared to last year. This means NASA will not be able to maintain previous commitments to the European Space Agency for dual Mars missions in 2016 and 2018 , Bolden confirmed at the conference. 

Reduced spending on outer-planet exploration also effectively halts other new high-priority missions, including one to study the moons of Jupiter. 

In April 2010, the president had announced his desire for a transformative agenda for NASA that emphasized missions to Mars by the 2030s. 

3. A small boost for arts and humanities: The president came out in favor of a 5 percent increase in funding for a group of DC-based arts institutions and federal grantmaking agencies, the Los Angeles Times reports . The proposal outlines an increase from $1.501 billion to $1.576 billion in spending on the National Endowments for the Arts and Humanities , the Institute of Museum and Library Services, the Smithsonian Institution, the National Gallery of Art, and the Kennedy Center. (The spending bill passed in the House in mid-December included a 5.6 percent budget slash to the arts and humanities endowments for fiscal year 2012.) 

On the campaign trail, Mitt Romney has been advocating for severe but unspecified cuts to the National Endowment for the Arts and also for killing government subsidies to Big Bird . 

4. You soon might have to live without mail on Saturdays: The 2013 budget reiterated the White House's position that the Postal Service should be allowed to put Saturday mail delivery on the chopping block, a move that could potentially save over $3 billion a year. This remains a controversial proposition for many in Washington more than half of all House representatives have signed a nonbinding resolution supporting Saturday delivery. 

[Barack Obama] is wrong to suggest the ending of Saturday mail delivery service, Sen. Bernie Sanders (I-Vt.) told Reuters . In the long run, if the Postal Service is to grow and become financially stronger, speed and maintaining the current mail delivery standards are terribly important. 

The budget would also green-light plans to hike postage rates. Earlier this month, the Postal Service announced upwards of $3 billion in losses in the quarter ending December 31. 

5. The dubious claims of billions saved by ending the wars: The president insists that roughly $850 billion would be saved by wrapping up the wars in Afghanistan and Iraq. The proposed budget calls for shifting over $200 billion of that to building and maintaining highways. The Associated Press's fact-checking contingent has a bone to pick with this : 

There is no direct peace dividend from ending the wars because the [US] government borrowed to pay for them Counting the end of wars as a dividend is like a student coming out of college loaded with debt and aching to buy things, says Maya MacGuineas , president of the Committee for a Responsible Federal Budget. When you finish college, you don't suddenly have thousands of dollars a year to spend elsewhere in fact, you have to find a way to pay back your loans. 

MacGuineas says, Drawing down spending on wars that were already set to wind down and that were deficit-financed in the first place should not be considered savings. 

President George W. Bush kept the cost of the wars out of his budgets , a contentious accounting maneuver that may have papered over the impact on spending projections but deepened the national debt as surely as if the price tag had been shown transparently. 

For more budget items you might have skipped over, check out Suzy Khimm's post over at Wonkblog . 

Here's the 2013 proposal in full: 

The President's Budget for Fiscal Year 2013
