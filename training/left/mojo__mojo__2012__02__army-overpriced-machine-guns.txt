Your tax dollars at work: US Army photo Mark Thompson of Time 's Battleland blog flagged a funny-looking item Monday from the Pentagon's daily contracting announcements click to embiggen: 

As Thompson points out, a $77.4 million contract for 900 machine guns would come out to $86,000 a pop. That's a deal even Blackwater and KBR would envy! 

But not so fast: After calling up the Army's public affairs folks, Thompson learned that the multi-million-dollar pricetag is the cost ceiling for several years' worth of orders on the guns; once future shipments are factored in, the max cost of each weapon should be closer to $8,600 which still is quite a bit more than that Colt .45 your dad takes to his tea party rallies. (Of course, you get a lot more firepower at the higher price point , which is why you should talk your dad out of joining the Oathkeepers and going Rambo on the government anytime soon.) 

Three lessons from this affair: 

1) As Thompson got the Army spokesman to concede, this is a pretty bad contract announcement. It makes you wonder about the accuracy of the military's other 20 or so daily public contract announcements. Pentagon math FTW. 

2) It's nice that the Army spins this as a decent deal for the taxpayer. Keep in mind, though, that $77.4 million is still 26 to 39 times what NPR generally gets got from the federal government each year. 

3) Oh, by the way, this is on top of a $126 million contract that Colt got three years ago for the same danged weapon. That makes $203.4 million in all. Apparently, we're going through crew-served machine guns like a knife through hot butter. Except we don't have any butter, because we bought all these frickin' guns. 

Here's what your tax money really buys: A new M240B machine-gun barrel to replace the one this guy just melted.
