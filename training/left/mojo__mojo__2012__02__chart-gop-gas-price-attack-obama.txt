Was George W. Bush to blame for this summer 2008 peak? Source: EIA 

Driving back from the mountains this past weekend, I commented to my wife how unusual it seemed that we weren't hearing too much public bitching about $4-a-gallon gasoline, because that's what it costs right now in California. But I spoke too soon. When we returned home, there was the Sunday New York Times with an A1 story on exactly that, describing how the GOP leadership planned to attack Obama on the issue, blaming him for high gasoline prices. 

This is the sort of well-worn populist trick that gets people riled up even when there's no substance to it. I created the chart above using an Energy Information Administration data set on weekly retail gasoline prices (excluding taxes) in selected countries. I used premium unleaded because the numbers were more complete. (Click on the chart to find the raw data.*) 

Okay, so what do we see here? Does it look like domestic gas prices (black line) are responding to the policies of one American president or another? Note the peak toward the right: That's the summer of 2008, when George W. Bush was in office. The price crash that follows bottoms out in late-December 2008/early-January 2009, shortly before Obama took over. 

Now certainly America's oil consumption, and to some degree our policies in the Middle East, affect global oil prices. Refining capacity is also a factor, and it happens that an unusual number of US refineries have been shut down recently by their operators. (The West's confrontation with Iran is also having an effect, according to this news report .) But we are also clearly in lockstep with global price trends. So if you want to blame Bush for that peak (as Nancy Pelosi did ; neither party is immune to political opportunism) or credit him for that crash, or you want to blame Obama for the subsequent rise in prices, then you should also blame and credit German Chancellor Angela Merkel, French President Nicholas Sarkozy, and the rest of 'em. 

Other than dip into America's strategic fuel reserves, an American president can do at least one thing to directly influence domestic prices: He can push for a higher or lower federal excise tax on gasoline. Let's look at the same chart with taxes (state and federal) included. The first thing you'll notice is that we pay far less for gasoline than European citizens because they tax it so much more heavily. See how we split away from the pack? And since these numbers are not inflation-adjusted, the increase in price since the 1990s is not as big as it seems. 





Why Europeans drive small cars: gas taxes Source: EIA 

Of course, the guys who want Obama's job claim that he wants to make America just like Europe. A question to test that theory: H ow much more federal tax do you pay per gallon under President Obama than you did under George W. Bush? 

Answer: less than zero. 

The federal excise tax on gasoline has remained largely unchanged at 18.4 cents per gallon since 1993. This flat figure that isn't pegged to inflation, which means that the value of the revenue from the tax has steadily deteriorated, which, in turn, has made it harder to fund highway repair and all that stuff. I t's the states, in any case, that levy most of the gas taxes. (See the chart on page two of this document to see where yours measures up.) If Congress were to raise the federal gas tax by a few pennies, which it really should (but won't), you'd never notice the change in your bank account. 

*I excluded weeks for which the data was incomplete, but not enough to make any noticeable difference in the outcome.
