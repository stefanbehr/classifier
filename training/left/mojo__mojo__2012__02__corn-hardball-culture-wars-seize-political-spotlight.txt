David Corn and Jennifer Donahue joined host Chris Matthews on MSNBC's Hardball to discuss the recent political spates over women's health rights and same-sex marriage . Despite predictions that the 2012 political season would be all about the economy, the discourse has returned to the familiar tropes of the culture wars. 

David Corn is Mother Jones' Washington bureau chief. For more of his stories, click here . He's also on Twitter .
