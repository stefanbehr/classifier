renedrivers /Flickr 

On Tuesday morning, a three-judge panel from the Ninth Circuit Court of Appeals found California's ban on same-sex marriage unconstitutional. 

Proposition 8 serves no purpose, and has no effect, other than to lessen the status and human dignity of gays and lesbians in California, wrote Judge Steven Roy Reinhardt in an opinion that quotes William Shakespeare, Frank Sinatra, and Groucho Marx. Although the Constitution permits communities to enact most laws they believe to be desirable, it requires that there be at least a legitimate reason for the passage of a law that treats different classes of people differently. 

The sweeping language of the appeals court decision echoes that of Judge Vaughn Walker's ruling at the district court level, which found that there was no legitimate government interest in banning same-sex marriage and that proponents' reasons were premised either on faulty reasoning (i.e., that same-sex marriage harms heterosexual marriages) or anti-gay prejudice, such as the idea that gays and lesbians pose a danger to children. The panel leaned liberal: Reinhardt and Judge Michael Daly Hawkins were both Democratic appointees. Judge N. Randy Smith, a George W. Bush appointee and the former chairman of the Idaho GOP, dissented, writing that I am not convinced that Proposition 8 lacks a rational relationship to state interests. 

Contrary to a hysterical fundraising email sent out by the National Organization for Marriage shortly after the ruling, the decision did not find an independent right to same-sex marriage in the Constitution. Whether under the Constitution same-sex couples may ever be denied the right to marry, Reinhardt wrote, was a broader question that we need not and do not answer. Rather the opinion finds that Prop 8 was unconstitutional because it stripped gays and lesbians in California of a right that had already been granted to them. 

Professor Adam Winkler of the UCLA School of Law says that the strength of Walker's original opinion will likely be important to the ultimate outcome. Key were Walker's findings of fact, which foreclosed on most of Prop 8 supporters' arguments. That's the strength of Judge Walker's ruling, Winkler says. He made it difficult for backers of the initiative to successfully defend this law. 

After Walker retired in February, Prop 8 supporters sought to have his ruling vacated on the basis that the judge was in a long term same-sex relationship. (By that self-same reasoning, of course, a heterosexual judge would be similarly compromised because of the impact marriage equality opponents say same-sex marriage would have on heterosexual marriages.) Though they differed on the constitutionality of Prop 8, the three judges were unanimous in rejecting the argument that Walker was obligated to recuse himself. 

The judges were also unanimous in affirming that Prop 8 supporters could stand in for the California government in defending the law. That's not exactly bad news for marriage equality supporters, since the ideal outcome in this case is that the Supreme Court adopts the reasoning that same-sex marriage bans violate gays and lesbians' right to equal protection under the law. That won't happen anytime soon, though. 

It's going to take a while, says Winkler, who estimates that the Supreme Court probably will see the case by 2014 at the earliest. 

In the meantime, Reinhardt's opinion offers a subtle poetic admonition to those like President Barack Obama who are still on the fence about supporting full same-sex marriage rights, retreating instead into support for legally-recognized civil unions or domestic partnerships. 

A rose by any other name may smell as sweet, Reinhardt writes, but to the couple desiring to enter into a committed lifelong relationship, a marriage by the name of 'registered domestic partnership' does not. 

UPDATE: The Washington Post 's Greg Sargent talks to gay rights advocate Richard Socarides, who expresses disappointment the decision didn't go further, saying We were hoping for a very broad ruling that held that there was a right under the Constitution to same sex marriage, one that would have pressured Obama to change his mind on the issue. 

Read the decision here (if the DocumentCloud embed doesn't load after a minute or so, try refreshing your browser):
