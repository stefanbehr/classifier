Last week, the billionaire industrialist Koch brothers held their latest get-together with wealthy conservative political donors. At these meetings, held twice a year under a veil of secrecy, Republican all-stars discuss election strategy and vet potential presidential candidates like New Jersey Gov. Chris Christie . Last September, Mother Jones obtained exclusive audio recordings from a Koch seminar held outside Vail, Colorado, where Charles Koch had declared that the 2012 election would be the mother of all wars and thanked dozens of million-dollar donors who'd pledged to the cause. 

According to a Huffington Post source , 250 to 300 guests attended the most recent event, which was held in Palm Springs, California. They included Citadel CEO Ken Griffin and casino billionaire Sheldon Adelson , who along with his wife has given a staggering $10 million to a pro-Newt Gingrich super-PAC. Guests reportedly pledged a total of $40 million to the effort to oust Obama, with Charles and David Koch promising an additional $60 million. But it wasn't all fun and games, the source said, as guests complained that recent meetings had focused more on alpha male anti-Obama chest-pounding than the strategy sessions for which they'd been known. 

Former ThinkProgress.org blogger Lee Fang also got a peek at the Palm Springs event, which was dubbed Defending Free Enterprise. Fang, who first reported on the Koch seminars before the 2010 midterms , caught wind that someone had booked all 560 rooms at the Rennaisance Esmeralda Resort Spa for three nights in late January and decided to investigate. I arrived at the hotel the night before the event, Fang wrote , but was followed closely by security and asked to leave the next morning before the Koch meeting guests arrived. During the seminar, helicopters, private security, and police officers from neighboring cities patrolled the area constantly. 

A Greenpeace blimp protests a 2011 Koch seminar in California. Gus Ruelas /Greenpeace 

Fang wasn't able to get inside, but he did manage to identify several additional guests by scoping out their private jets at the Palm Springs International Airport. They included billionaire investor Phil Anschutz and Kenny Troutt, a Dallas investor who's given $700,000 to conservative super-PACs. Fang also noticed jets belonging to Harold Hamm, an Oklahoma oil tycoon, and Foster Friess, a Wyoming investor who's helped keep Rick Santorum afloat by pumping $381,000 into two super-PACs supporting the candidate. At last year's Vail seminar, Charles Koch thanked both Friess and Hamm (and Griffin) for their million-dollar contributions. 

At the airport, Fang also spotted Phil Kerpen, vice president of the Koch-affiliated tea party group Americans for Prosperity, which recently spent $5 million on anti-Obama attack ads . Kerpen admitted that he hopes the 2012 election will result in aggressive cuts to government spending and to regulation to allow robust economic growth, but not before complaining to Fang that I thought they had stopped all leaks concerning the whereabouts of the Koch seminars. 

On the contrary, the meetings have become increasingly visible since they began quietly in 2003 . Last January, Greenpeace flew an anti-Koch blimp (above) over the brothers' Palm Springs seminar. This That year, hundreds of anti-Koch protesters showed up outside the hotel amd were met by 60 police officers in riot gear who made 25 arrests. 

Print Email Tweet Your Daily Newt: Ridin the Rails Time for Some Troll Whacking Gavin Aronsen 

Writing Fellow 

Gavin Aronsen is a writing fellow at Mother Jones . For more of his stories, click here . Follow him on Twitter or send an email to garonsen [at] motherjones [dot] com. RSS | Twitter If You Liked This, You Might Also Like... 

Exclusive: The Koch Brothers Million-Dollar Donor Club 

The anonymous patrons who have given $1 million or more to the Koch dark-money machine. Exclusive Audio: Inside the Koch Brothers Secret Seminar 

A close-up view of the oil billionaires' dark-money fundraiser and 2012 strategy session. Audio: Chris Christie Lets Loose at Secret Koch Brothers Confab 

In our exclusive recordings, David Koch introduces the New Jersey governor as "my kind of guy" and "a true political hero." Fallout From Our Koch Brothers Expose 

Did Charles Koch compare Obama to Saddam? Is Chris Christie "mentally deranged"? Was Christie's secret powwow with David Koch a "smoking gun"? The Top 20 Donors of 2012 (So Far) 

The casino mogul betting $5 million on Newt Gingrich, Wall Streeters for Mitt Romney, Obama's Hollywood pal, and the other 1 percenters trying to sway the race. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
