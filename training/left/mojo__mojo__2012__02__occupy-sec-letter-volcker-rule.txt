Yesterday, a group affiliated with Occupy Wall Street submitted an astounding comment letter to the Securities and Exchange Commission. Point by point, it methodically challenges the arguments of finance industry lobbyists who want to water down last year's historic Dodd-Frank Wall Street reforms. The lobbyists have been using the law's official public comment period to try to kneecap the reforms, and given how arcane financial regulation can be, they might get away with it. But Occupy the SEC is fighting fire with fire, and in so doing, defying stereotypes of the Occupy movement. Its letter explains: 

Occupy the SEC is a group of concerned citizens, activists, and professionals with decades of collective experience working at many of the largest financial firms in the industry. Together we make up a vast array of specialists, including traders, quantitative analysts, compliance officers, and technology and risk analysts. 

The letter, which has been in the works for months, passionately defends the Volcker Rule, a provision of the Dodd-Frank Wall Street reforms meant to prohibit consumer banks from engaging in risky and speculative proprietary trading. That barrier had collapsed in the 1990s with the gradual watering down, and eventual repeal, of the Glass-Steagall Act. Occupy the SEC explains why this became a problem: 

Proprietary trading by large-scale banks was a principal cause of the recent financial crisis, and, if left unchecked, it has the potential to cause even worse crises in the future. In the words of a banking insider, Michael Madden, a former Lehman Brothers executive: Proprietary trading played a big role in manufacturing the CDOs (collateralized debt obligations) and other instruments that were at the heart of the financial crisis. . . if firms weren't able to buy up the parts of these deals that wouldn't sell. . .the game would have stopped a lot sooner. 

What makes Occupy the SEC so unique and inspiring is the way that it straddles the two worlds. On the one hand, it's authentically grassroots, forged in Zuccotti Park's crucible of discontent. As such, it is transparent, open to anyone, and accountable to everyone. On the other hand, it includes financial insiders with the education and regulatory vocabulary to challenge high-powered lobbyists at their own game. That's a powerful combination that the SEC can't easily ignore. From the letter: 

The United States aspires to democracy, but no true democracy is attainable when the process is determined by economic power. Accordingly, Occupy the SEC is delighted to participate in the public comment process. . . 

For more on how Occupy the SEC came to be, read my story on its umbrella organization, the Alternative Banking Group . 

Occupy the SEC Comment Letter on the Volcker Rule 

Do you appreciate fair and factual reporting on Occupy Wall Street? Please donate a few bucks to help us expand our coverage .
