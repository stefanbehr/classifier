Not long ago, John Paul Thornton, a 32-year-old mental health worker in Decatur, Alabama, was clicking around Facebook when he noticed someone had posted a video of satirist Stephen Colbert talking about his super-PAC , a long-running gag on the show. Thornton, an active member of the Occupy movement in his home state, thought to himself, Wow, it would be really cool if Occupy had one of those. 

So, last week, Thornton went ahead and filed papers with the Federal Election Commission to create...the Occupy Wall Street Political Action Committee. Unlike Colbert's Americans for a Better Tomorrow, Tomorrow, though, Thornton says in his first interview on the subject that OWS PAC is no joke. 

Newly published FEC documents show Thornton requesting to establish his group as a super-PAC , the type of political outfit that can spend and raise unlimited money so long as they don't coordinate with candidates. The documents list Occupy Wall Street as a connected organization, with a street address of NONE AND EVERYWHERE in the city of ALL OF THEM. Thornton wasn't trying to be cheeky here, he says. Thornton says he plans to launch a website for the super-PAC soon. All he's waiting for is the FEC's blessing. 

Thornton says he's no Occupy novice. He joined Occupy Huntsville, a 20-minute drive from his home in Decatur, three weeks after the occupation of Zuccotti Park in lower Manhattan began on September 17, and has been involved ever since. He's also been active opposing Alabama's draconian immigration measure, HB 56, which passed in June 2011. My parents called me a serial dissenter, he says. I was probably a discontent fetus. 

Thornton admits that some members of the Occupy movement, which contends that the political system is broken and seeks to work outside of it, might not take kindly to OWS PAC. I will admit it's not exactly keeping with strict Occupy ideals, he says. But Thornton doesn't subscribe to the movement's stay-out-of-politics philosophy. The thinking is, if Occupy is going to evolve and to become an actual political player, it needs to participate in major political games. 

Karanja Gacuca, a spokesman for Occupy Wall Street, says it's not surprising that, as the Occupy movement moves forward, someone like Thornton would jump into the political arena. But that's not where OWS is headed. Occupy Wall Street as a movement rejects the political system as a broken system that needs to be overhauled from the bottom up, Gacuca says. OWS PAC, he adds, is an alternative action which if it were to be voted on at the general assembly would never pass. But individuals are individuals and we understand that people are going to use the Occupy name to do alternative actions. 

Here's the full filing: 

Do you appreciate fair and factual reporting on Occupy Wall Street? Please donate a few bucks to help us expand our coverage .
