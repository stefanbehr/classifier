A s a service to our readers, every day we are delivering a classic moment from the political life of Newt Gingrich until he either clinches the nomination or bows out. 

Newt Gingrich was speaking to supporters in Georgia on Tuesday when he decided to relay to the audience an exchange he'd had just a few hours earlier: I was describing the other week some ideas and Romney said, you know, boy if someone came in to see him with ideas like that, he'd have fired him! he said. And somebody in Chattanooga said to me this morning, they said, you know, 'Romney was the kind of guy who would have fired Christopher Columbus.' 

It's a provocative charge. But is it true? 

The Facts: In his remarks in Georgia, Gingrich explained that Columbus' value rested in the power of his ideas. As he put it: Lincoln had a vision of a transcontinental railroad. The Wright Brothers had a vision that they could fly. Edison had a vision that we could have electric lights Henry Ford had this idea you could build a low-cost car with mass production. 

Christopher Columbus had a vision that if he sailed southwest from Spain for 3,000 nautical miles, he would reach Asia. Instead, he got lost, landed on a small island approximately one-sixth of the way to Asia, enslaved all of the inhabitants, set about searching for gold, did not find gold, falsely informed his supervisors then in the act of purging their kingdom of Jews and Muslims that he had reached Asia, and promptly ruled over his newfound land so poorly he was sent back to Spain in chains. 

As a candidate, Romney has expressed zero tolerance for insubordination, firing debate coach Brett O'Donnell after arguably his best debate because Romney felt O'Donnell had taken too much credit. At Bain Capital, Romney likewise demonstrated a knack for stripping new companies of their nonessential parts a euphemism, really, for firing people. 

Our Ruling: Romney's actions at Bain Capital have drawn from immense scrutiny from the press and his fellow candidates. But there's no hard evidence that he's ever illegally seized someone else's company and enslaved its employees. Given Romney's quick hook, it's hard to think he would have tolerated such behavior for very long. 

Besides, Mitt Romney likes being able to fire people who provide services to him . 

We rate this claim True.
