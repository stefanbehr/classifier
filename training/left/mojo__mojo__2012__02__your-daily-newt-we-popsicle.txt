A s a service to our readers, every day we are delivering a classic moment from the political life of Newt Gingrich until he either clinches the nomination or bows out. Daily Newt is back from a two-day sabbatical staring at the tree sloths . 

Newt Gingrich's 1995 college course, Renewing American Civilization, was a blank canvas on which the speaker of the house painted grand portraits of mountain people, forest people , and an idyllic age of family-friendly prime-time entertainment . It also gave him a chance to spin his students on the works of his favorite management consultants among them, Daryl Connor and his theory of freezing-unfreezing-refreezing. It only sound s like a dance move: 

He talks about being frozen, thawing, and refreezing. Now, this is at the heart of how you make the transition, and we'll come back later to his book Managing at the Speed of Change , which I recommend. It's a very, very useful framework for looking at this and having some sense of how you how resilient managers succeed and prosper where others fail, and he talks about this. 

Now, here's his concept. Normally you're frozen. You get up in the morning, you have a habit. The habit's fixed. Then things begin to change, and it's almost like watching you can think about this with a popsicle. It's almost like watching or with an ice cube. It begins to thaw, and you're changing and pieces fall apart, and it doesn't feel right. It's what Drucker means by a discontinuity. See, as long as you're frozen, it's predictable. Now it starts to change. Then you begin to figure out the future and you begin to refreeze, because people normally have to have stable conditions of effectiveness... 

Okay? Everybody understand this concept of frozen, beginning to thaw out, and then refreezing? This is at the heart of thinking about how you manage change. And it allows you to now see the thawing without going, Oh, my god, we're all going to collapse. No, we're going to find a new, more powerful, more appropriate way to refreeze. 

If any of that was confusing to you, we think the film below highlights this concept quite well:
