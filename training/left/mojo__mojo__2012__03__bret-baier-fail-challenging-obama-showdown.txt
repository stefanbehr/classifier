Bret Baier is wrong. 

My new book, Showdown: The Inside Story of How Obama Fought Back Against Boehner, Cantor, and the Tea Party , has stirred up a spat. Many media outfits focused on a nugget in the book reporting that, during a December 2010 meeting with labor officials, President Barack Obama complained that he was losing white males, partly due to cultural issues (gun rights, gay rights, and race), and noted, Fed by Fox News, they hear Obama is a Muslim 24/7, and it begins to seep in. 

Baier, a Fox News anchor, took exception to this. On air he proclaimed , For the record, we found no examples of a host saying President Obama is a Muslim. 

Note the sly use of the word host. Whether or not he or any other host has uttered the direct statement Obama is a Muslim, his network has advanced that notion repeatedly. 

Look at this August 19, 2010 video of Sean Hannity interviewing Brigitte Gabriel, a regular guest on Fox. She clearly defends the view that Obama is a secret Muslim. And Hannity does not challenge her. 







That same month after a poll showed that 18 percent of Americans believed Obama was a Muslim Fox regular Charles Krauthammer, appearing on Baier's show, Special Report , claimed that the emphasis Obama placed on Muslim outreach might incline people to conclude he's not a Christian. This is known as fueling flames. Baier, too, blamed Obama for creating the Muslim misperception, noting that the president has talked openly about his the Muslim heritage in his family. Rather that dismiss the Obama-is-a-Muslim nonsense, they sought to validate reasons for this misguided belief. 

The previous year, Special Report had questioned Obama's faith, asking Islam or Isn't He? Just posing such a query reinforces the notion that the president might be a Muslim. And the network has regularly featured anti-Islam activist Pamela Geller, who has referred to Obama as the Muslim president. 

Let's not forget Glenn Beck who was a Fox News host. In an August 23 show , Beck said Obama was yikes! sympathetic to Muslims. He criticized Obama for having stated at the inauguration that we are a nation of Christians and Muslims, Jews and Hindus, and non-believers. Beck then referred to this piece of evidence : Michelle Obama had visited the Alhambra Palace mosque in Spain, a well-known tourist attraction. Beck discerned something dark and sinister in this outing. Are they sending messages? he asked. I don't know. I don't know. I've never had to look for messages before. Messages? What sort of messages? Beck seemed to be suggesting the Obamas were part of a secret Muslim conspiracy. 

During his show the next day , Beck tried to have it both ways. He said, Obama is not a Muslim. I'm taking his word that he is a Christian. But then he implied that Obama was not truly a Christian: 

[W]here your father is a Muslim, an atheist, your mother at least is not practicing any religion, your stepfather is non-practicing Muslim, your grandparents in frequent something called the Little Red Church, I don't even I mean, is there any wonder why so many Americans are confused by him? They don't recognize him as a Christian. No. 

There's more (as Media Matters has documented ). Donald Trump, appearing on Bill O'Reilly's show in March 2011, speculated that Obama was hiding his birth certificate because it declared he was born a Muslim. On April 26, 2011, controversial pastor Robert Jeffress on Fox Friends said, why do 20 percent of Americans think the president is a Muslim? We'll, as my kids would say, duh. Last year, conservative radio talk show host, Lars Larson, echoing a familiar right-wing trope, said on Fox that the president shows a whole lot of deference to Muslims and seems to forget Christians. Fox folks, of course, have gone wild over Obama's bowing before the Saudi king, and the network in 2008 pushed the false story that Obama attended an Islamic school in Indonesia. 

Obama was correct in what he said to those labor officials. He wasn't blaming Fox News for all his political troubles, but he was pointing out that it was shaping a political culture in which the most foul anti-Obama assertions could breed. Fox News has indeed provided a platform for those who question Obama's faith, for those who defend the view he is a Muslim, for those who explain that belief in a manner that lends this false notion credence, and for those who hint that Obama is not really a Christian and that there may be something a little Muslim-y about the guy. Baier is often a fine journalist remember his interview with Mitt Romney but now he is defending what should be indefensible.
