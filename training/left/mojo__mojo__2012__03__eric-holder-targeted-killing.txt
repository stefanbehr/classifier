On Monday, the Obama administration explained when it's allowed to kill you. 

Speaking to students and faculty at Northwestern University law school, Attorney General Eric Holder laid out in greater detail than ever before the legal theory behind the administration's belief that it can kill American citizens suspected of terrorism without charge or trial. In the 5,000-word speech, the nation's top law enforcement official directly confronted critics who allege that the targeted killing of American citizens violates the Constitution. 

'Due process' and 'judicial process' are not one and the same, particularly when it comes to national security. Holder said. The Constitution guarantees due process, not judicial process. 

Who decides when an American citizen has had enough due process and the Hellfire missile fairy pays them a visit ? Presumably the group of top national security officials that, according to Defense Secretary Leon Panetta , decides who is targetable and forwards its findings to the president, who gives final approval. 

There won't be any drone strikes in Denver anytime soon. But you might want to be careful when traveling abroad, because Holder made it clear that there are no geographical limits in the fight against Al Qaeda. Neither Congress nor our federal courts has limited the geographic scope of our ability to use force to the current conflict in Afghanistan, Holder said. We are at war with a stateless enemy, prone to shifting operations from country to country. 

Holder's speech did outline some concrete limits to when the US government is allowed to target its own citizens. The target has to pose an imminent threat of violent attack to the US and be beyond the ability of American authorities to capture, and the strike can't violate international standards governing the use of force by killing too many civilians or noncombatants. 

But don't assume that when Holder says imminent threat of violent attack, he means that you're actually part of a specific plot threatening American lives. The Constitution does not require the president to delay action until some theoretical end stage of planning when the precise time, place, and manner of an attack become clear, Holder said. That would introduce an unacceptably high risk of failure. When he refers to failure, Holder presumably means failing to kill the target before the attack or plan for an attack materializes, not the possibility that the government might accidentally kill an innocent person. 

If the standards for when the government can send a deadly flying robot to vaporize you sound a bit subjective, that's because they are. Holder made clear that decisions about which citizens the government can kill are the exclusive province of the executive branch, because only the executive branch possess the expertise and immediate access to information to make these life-and-death judgments. 

Holder argues that robust oversight is provided by Congress, but that oversight actually amounts to members of the relevant congressional committees being briefed. Press reports suggest this can simply amount to a curt fax to intelligence committees notifying them after the fact that an American has been added to a kill list. It also seems like it would be difficult for Congress to provide robust oversight of the targeted killing program when intelligence committee members like Sen. Ron Wyden (D-Ore.) are still demanding to see the actual legal memo justifying the policy . 

Both supporters and opponents of the administration's targeted killing policy offered praise for the decision to give the speech. They diverged, however, when it came to the legal substance. It's essential that if we re going to be doing these things, our top national security and legal officials explain why it's legal under international and constitutional law, said Benjamin Wittes, a legal scholar with the Brookings Institution, who said he thought the speech fulfilled that obligation. I think [the administration] is right as a matter of law. 

In a statement, Hina Shamsi, director of the ACLU's national security project, called the authority described in the speech chilling. She urged the administration to release the Justice Department legal memo justifying the targeted killing program a document that the ACLU and the New York Times are currently suing the US government to acquire. A nyone willing to trust President Obama with the power to secretly declare an American citizen an enemy of the state and order his extrajudicial killing should ask whether they would be willing to trust the next president with that dangerous power. 

The question is no longer an abstract one. In September, radical American cleric Anwar al-Awlaki was killed in a drone strike in Yemen alongside fellow American Samir Khan . Awlaki and Khan produced the English-language extremist publication Inspire , but until the sentencing of underwear bomber Umar Abdulmutallab , the US government provided little evidence they were much more than propagandists. Awlaki's son, Abdul Rahman al-Awlaki, also an American citizen, was killed about a month later . 

These deaths and those to come, Holder insisted Monday, do not represent a violation of America's founding principles. This is an indicator of our times, Holder said, not a departure from our laws and our values. 

Maybe it's both.
