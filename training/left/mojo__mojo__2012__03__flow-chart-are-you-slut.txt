Are you a slut? It's a question that, to be perfectly honest, we would have felt more than a little uncomfortable asking as recently as a few weeks ago. For one, there's the word itself as misogynistic an insult as you could conjure. And there wasn't much of a peg, what with the rest of the world focused on more pressing issues, like Israel's threats of conflict with Iran, and jokes about Mitt Romney's dog ( this is a particularly good one). 

But then conservative icon Rush Limbaugh who was once caught trying to bring 29 100 mg Viagra pills with him to the Dominican Republic spent three days ripping into Georgetown law student Sandra Fluke (rhymes with look ), calling her a slut and a prostitute for testifying before Congress about birth control, and suggesting that he'd like her to send him a sex tape. The #iamnotaslut Twitter campaign went viral; Limbaugh began losing sponsors (20, at last count). And now we can't seem to talk about anything else but sluts. Seriously, just take a look at this chart from BuzzFeed. Does Limbaugh think birth control pills work like Viagra? Plus: our handy calculator shows how much you'll spend on birth control over a lifetime. 

The national conversation about sluts of 2012 hasn't really given us much clarity but it has given a variety of commentators a platform from which to disseminate their definition of slut. Which, it turns out, is really, really broad. Fluke who noted in her testimony about contraception access that she has a friend who uses the pill out of medical necessity has been maligned for oversharing about her sex life, which she didn't even discuss on the Hill. One Georgetown law school classmate of Fluke's quoted in the National Review put it worst: When did Georgetown Law start admitting Kardashians? 

So back to that question: Are you a slut? It's a head-scratcher, so we've put together this handy flowchart to help you out: 



Here it is in chart form, for the clicking-impaired: 



Photos: Sean Wandzilak/ Shutterstock.com ; Courtesy of Pepsi/ YouTube ; Louis Lopez/Cal Sports/ZumaPress.com; Jiri Hera/ Shutterstock.com ; Lasse Kristensen/ Shutterstock.com ; Kirill Vorobyev / Shutterstock.com ; Subbotina Anna/ Shutterstock.com ; Jeff Thrower/ Shutterstock.com ; Foonia/ Shutterstock.com ; .shock/ Shutterstock.com ; Orhan Cam/ Shutterstock.com ; Monkey Business Images/ Shutterstock.com
