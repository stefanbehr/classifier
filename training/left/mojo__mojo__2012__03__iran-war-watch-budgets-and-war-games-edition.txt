Are the United States and Iran on a collision course over the Middle Eastern country's controversial nuclear program? We'll be posting the latest news on Iran-war fever the intel, the media frenzy, the rhetoric. 

The chairman of the House Armed Services Committee is pushing for the upcoming 2013 budget to include items that would further beef-up the Pentagon's ability to fight a war with Iran. Carlo Munoz at The Hill 's defense blog reports : 

Committee Chairman Buck McKeon (R-Calif.) is spearheading an effort to pump defense dollars into a specific slate of weapons and programs that could be used in a potential conflict with Tehran. 

On Tuesday, the California Republican refused to back down from that plan, even if it could further inflame tensions between the two countries. We are doing what we can to make sure [the United States] is protected and that is what we are going to do, McKeon told reporters during a briefing on Capitol Hill...[Rep. Michael Turner (R-Ohio)], chairman of the committee's Strategic Forces sub-panel , added that the United States cannot afford to bet against Iran turning bellicose rhetoric into action. 

Right now, it is unclear what programs or weapons systems would be included in the proposed budget items. (McKeon, Turner, and other committee members declined to comment on specifics.) In recent weeks, the United States military has augmented its sea and land defenses in the Persian Gulf to combat any potential efforts by Tehran to shut down the Strait of Hormuz (a loudly publicized threat from the Iranian regime that is, likely , another bluff ). This has included expanded surveillance, more mine-detection technology, and requests for extra shore-launched cruise missiles. 

Earlier this week, the New York Times ran a story on a classified war game conducted by Central Command titled Internal Look that mapped out possible consequences of a preemptive airstrike on Iran by the Israeli military. The two-week exercise held earlier this month yielded some grim hypothetical scenarios, including the retaliatory sinking of a US Navy ship, an initial body count of at least 200 American sailors, and the escalation to a larger regional war.
