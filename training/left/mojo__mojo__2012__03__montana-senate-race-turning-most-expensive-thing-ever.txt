There were two big stories out of Montana this weekend. First things first: They finally caught Bigfoot . 

The second story, of perhaps more national significance, comes from Mike Dennison of the Helena Independent-Record , who reports that, thanks in part to a handful of recent Supreme Court rulings, the state has been buried by a deluge of attack ads ahead of Novemeber's US Senate race. As of early March, outside groups like Karl Rove's Crossroads GPS and the 60 Plus Association have spent more than $3 million on advertisements targeting either incumbent Democrat Tester, or his GOP challenger, Rep. Denny Rehberg (rhymes with Freebird ). There's a lot at stake: The race could determine which party controls the upper chamber come 2013. 

That $3 million is for television advertisements alone to say nothing of radio, newspaper, and smoke signals. And in Montana, your $3 million goes a long, long way: A Montana gubernatorial candidate recently explained to the Missoulian that It costs $150,000 to $175,000...to get one message across (the state), so 80 to 90 percent of (TV viewers) will see it six to eight times. So with that as our blueprint, it's not all that unreasonble to suggest that your average Montanan has been exposed to at least 137 iterations of ads like this: 



And it's still only March. This is your campaign finance system on drugs.
