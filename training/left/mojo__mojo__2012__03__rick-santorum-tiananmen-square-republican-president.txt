Kristin Brenenman /Flickr. Original photograph by Jeff Widener, Associated Press. 

On the presidential campaign trail, Rick Santorum has never shied away from calling out the bluster of his fellow Republicans, chief among them Newt Moon Base Gingrich. Grandiosity has never been a problem with Newt Gingrich, Santorum deadpanned at a January presidential debate. Casting Newt as a loose-lipped gaffe machine, Santorum said, I don't want a nominee that I have to worry about going out and looking at the paper the next day and worrying about what he's going to say next. Santorum in '93: More Government Needed in Health Care 

Santorum: Single Moms Are Breeding More Criminals 

Rick Santorum's Housing Hypocrisy Rick Santorum in '94: PACs Have Undue Influence Flashback: How Rick Santorum Ruined Pro Wresting Flashback: Santorum Compares Voting For Him To Blocking a Tank Near Tiananmen Square 

Yet Santorum himself is no stranger to overblown campaign talk. In a previously unreported radio interview from April 1994, then-Rep. Rick Santorum (R-Penn.) argued that supporting his underdog US Senate campaign and voting for him in the election was every bit as important as the bold Chinese protester who, in June 1989, blocked a column of military tanks near Beijing's Tiananmen Square, the site of student protests against China's Communist ruling elite. 

Santorum's remarks were included in tracking documents compiled by Sen. Harris Wofford, the Democrat who Santorum narrowly defeated in his 1994 Senate race. Here is Santorum's full statement: 

...What you have to do is recognize that when you get up in the morning, you look in the mirror, you're looking at the person who really bears the moral responsibility for the future of that country, so your children can be safe and prosperous and free, and unless you take that responsibility seriously, you have no one to blame for the Roberta Achtenbergs being in the White House than yourselves. 

You've gotta take that responsibility seriously, and the work you do, if it just means going out to vote, if it means passing the word onto friends and neighbors about what you've heard on this program, the work that you do is every bit as important as the guy who stood in front of the tank at Tiananmen Square when it comes to the future of our civilization in this country , and I don't know if I can say it any more strongly than that. 

Here's the transcript itself: 

Suffice it to say that comparing voting for Rick Santorum with the act of defiance by Tiananmen's Tank Man, as he's known, far surpasses any of Newt Gingrich's bombast. The Tank Man shuffled left and right to block the tanks' forward progress, then clambered on top of the first tank, stuck his head inside, and reportedly said , Why are you here? My city is in chaos because of you. Tank Man could've easily been shot or run over; one theory about his fate holds he was killed by a government firing squad weeks later. He etched his name, if anonymously, into the history books with one of the most iconic protests in the 20th century. 

The Tiananmen line was one of many hyperbolic one-liners uttered by Santorum during his 1994 campaign. As Mother Jones reported , Santorum made welfare reform a pillar of his Senate bid. His welfare stump speech often targeted single mothers, and he claimed they were breeding more criminals and that lawmakers were needed who weren't afraid of kicking them in the butt. In another grandiose touch, Santorum argued that the single mother problem posed an existential threat to the United States itself. At a February 1994 Clairtown, Pennsylvania, town hall, he said, We are seeing the fabric of this country fall apart, and it's falling apart because of single moms.
