This week, the oral arguments conducted before the Supreme Court concerning the constitutionality of President Barack Obama's health care overhaul seemed to spell bad news for supporters of Obamacare. But there was a moment that could hearten those progressives yearning for a single-payer type of national health care system. 

Conservatives have spent the last few years falsely characterizing the Affordable Care Act, which preserves the private insurance system, as a government takeover of health care. Yet during oral arguments, a lead lawyer opposing Obamacare as unconstitutional suggested an actual government takeover of health care might be constitutional. 

In an exchange between Justice Sonia Sotomayor and Michael Carvin a lawyer representing the National Federation of Independent Businesses, which opposes Obamacare Sotomayor got Carvin to concede that a single-payer system would be constitutional: 

JUSTICE SOTOMAYOR: So the I I want to understand the choices you're saying Congress has. Congress can tax everybody and set up a public health care system. 

MR. CARVIN: Yes. 

JUSTICE SOTOMAYOR: That would be okay? 

MR. CARVIN: Yes. Tax power is 

JUSTICE SOTOMAYOR: Okay. 

Justice Ruth Bader Ginsburg had previously attempted to trap former Solicitor General Paul Clement into saying the same thing. It seems to me you're saying the only way that could be done is if the government does it itself; it can't involve the private market, it can't involve the private insurers, Ginsburg said. There has to be government takeover. We can't have the insurance industry in it. Is that your position? 

Clement, perhaps flashing forward to a future where he is arguing against the constitutionality of the health care system liberals would prefer to the private sector alternative they actually got passed, refused to be cornered. No. I don't think it is, Justice Ginsburg. I think there are other options that are available. 

Justice Anthony Kennedy wondered aloud whether the ability of the government to set up a single-payer system meant that the Affordable Care Act was constitutional. Let's assume that it could use the tax power to raise revenue and to just have a national health service, single payer, Kennedy said. In one sense, it can be argued that this is what the government is doing; it ought to be honest about the power that it's using and use the correct power. 

On the other hand, he mused, it means that since Congress can do it anyway, we give a certain amount of latitude. I'm not sure which the way the argument goes. If the Affordable Care Act gets struck down in its entirety, however, single payer may be the only alternative left for liberals, as monumentally difficult as it would be to get it passed. And though some conservatives as they assail Obama's health care reform now concede such a plan would be constitutional, no doubt the right can be expected to argue the opposite should single-payer ever become a viable alternative to Obamacare.
