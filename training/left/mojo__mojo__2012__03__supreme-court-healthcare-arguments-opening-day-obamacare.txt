The fate of the Affordable Care Act will likely be decided before the 2012 election, as the first day of much-anticipated oral arguments at the Supreme Court concerning Obamacare showed the justices wary of the case for delaying a ruling. 

With the chambers crowded with journalists, lawyers, advocates, politicians, and legal tourists as protesters milled about outside the court the question before the court on Monday was whether the Tax Anti-Injuction Act, which tries to limit lawsuits by forcing Americans to wait until they have paid a given tax before suing to overturn a tax-related law, ought to apply to the individual mandate provision of the ACA, a part of the health care reform measure that doesn't take effect until 2014. Under the individual mandate, if a covered person does not obtain health insurance, he or she must pay a penalty. If this fee is considered a tax under the Tax Anti-Injunction Act, then it can't be challenged until it is levied upon someone. 

See our full coverage of the Supreme Court's Obamacare hearings. What the Supreme Court Could Do About Obamacare, Explained 

Obamacare's Supreme Court Disaster 

Why Obamacare Will Survive Supreme Court Probably Won't Punt on Health Care Medicaid is the Big Sleeper at the Supreme Court 

The Obama administration chose not to make this case for putting off a ruling. Instead, the task fell to attorney Robert Long, who was invited by the justices themselves to argue the position (because this issue had been raised in a lower court decision now being reviewed as part of the justice's consideration of the constitutionality of the ACA). Long took a pounding from the black-robed jurists. Even the Democratic appointees on the court appeared skeptical that the individual mandate to buy health insurance was a tax covered by the Tax Anti-Injunction Act. 

Aren't you trying to rewrite the statute in a way? Justice Elena Kagan asked Long, suggesting he was stretching the definition of a tax. 

I would not argue that this statute is a model of clarity, Long grumbled. The audience in chambers erupted in laughter. Justice Ruth Bader Ginsburg, another Democratic appointee, sharply rebutted Long's argument, referring to the mandate as a must-buy requirement, not a tax. 

The Tax Anti-Injunction Act does not apply to penalties that are designed to induce compliance with the law rather than to raise revenue, Ginsburg said. And this is not a revenue-raising measure, because, if it's successful, they won't nobody will pay the penalty and there will be no revenue to raise. 

This reasoning seemed to represent a major blow to anyone hoping Obamacare could escape Supreme Court review this year on the basis of the it's-a-tax argument. 

Solicitor General Donald B. Verilli Jr. had a complicated argument. He maintained that the individual mandate is not a tax covered by the Tax Anti-Injunction Act, but that the mandate is authorized under the federal government's taxation power in the Constitution. This position reflected the Obama administration's desire to avoid having Obamacare be depicted as an outright tax. It drew needling from conservative and moderate justices. 

Today you are arguing it is not a tax, tomorrow you will be here arguing that it is a tax, Justice Samuel Alito, a Republican appointee, said to Verilli. When Verilli argued that Americans could not be punished for not purchasing health insurance beyond being hit with the mandate-imposed tax, Justice Stephen Breyer dryly observed, why do you keep saying 'tax'? The audience chuckled both times. 

The justices seemed clear that they would not duck the historical moment by avoiding a ruling on Obamacare under what might be called a tax dodge. Judging by their remarks, the Obama administration is likely to see a verdict on its signature domestic program prior to the November election. But there's still no telling what that verdict might be.
