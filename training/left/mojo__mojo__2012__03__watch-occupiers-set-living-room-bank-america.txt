Occupiers have tried to squat in bank lobbies before, but never with as much style as this crew from Occupy Wall Street: 



Notice how the occupiers look like they are having fun, instead of foaming with anger? That was completely conscious, says Nelini Stamp, an organizer with Occupy Wall Street whose mother lost her home to Bank of America in 2006. This is not a shutdown. We want to make light of the situation but also carry the message that this is a serious thing. 

The creators of the video are members of Occupy Wall Street's Fight BAC group (BAC stands for Bank of America), which formed two months ago to highlight the bank's foreclosure practices and too-big-to-fail business model. They're coordinating March 15th protests against the BofA in Manhattan; Phoenix; Danbury, CT; and Sarasota, FL. Many of them come from the anti-foreclosure group Occupy Homes, which is holding a national week of action starting today. The #M15 events are online at FTheBanks.org ( F stands for Foreclose or a different thing if you use your imagination, Stamp says). 

And yeah, in case you were wondering, $230 billion is how much taxpayers spent bailing out Bank of America. 

Do you appreciate fair and factual reporting on Occupy Wall Street? Please donate a few bucks to help us expand our coverage .
