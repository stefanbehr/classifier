On Friday, in an attempt to demonstrate once more that he's a totally normal humanoid with wide-ranging cultural interests, Mitt Romney published a playlist of his favorite music from the campaign trail. The mix, which you can find on his Facebook page and the music app Spotify, includes a mix of country, oldies, top-40, and whatever you'd call Kid Rock. 

It also includes The M.T.A., a song by the Kingston Trio that has likely never appeared within a 40-track radius of Kid Rock. It goes a little something like this: 



This was one of my favorite songs growing up, with the unintended consequence being that I developed an acute and highly irrational fear of subway turnstiles (something I'm sure Romney and I have in common). The thought of Romney blasting the Kingston Trio's rendition of M.T.A. on his campaign bus, feet tapping, head bopping, over and over and over again, actually makes him seem kind of what's the word here human . 

I'd just add that M.T.A. (otherwise known as Charlie on the M.T.A. ) is a song about a Boston man who embarks on what is supposed to be a smooth and uneventful ride, gets in over his head, becomes trapped, and is forced to have his wife try to bail him out. She fails and he's then doomed to spend the rest of his life trapped in an endless loop, eating sandwiches. So there's that. 

Update: Here's the full mix. 

I am a Man of Constant Sorrow The Soggy Bottom Boys 

Read My Mind The Killers 

December, 1963 (Oh What a Night) Frankie Valli The Four Seasons 

Ring of Fire Johnny Cash 

Somebody Told Me The Killers [ Ed note: Mitt is apparently friends with singer Brandon Flowers . Right? ] 

The MTA (The Boston Subway Song) The Kingston Trio 

Good Vibrations The Beach Boys 

Desperado (Live) Clint Black 

Crying Roy Orbison 

Only You (Long Version) Commodores 

Runaway Del Shannon 

It's Your Love Tim McGraw 

As Good as I Once Was Toby Keith 

Born Free Kid Rock 

Over The Rainbow Willie Nelson 

Stardust Nat King Cole 

In Dreams Roy Orbison 

Somebody Like You Keith Urban 

All-American Girl Kerry Underwood
