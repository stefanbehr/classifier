A video in which aging right-wing rocker Ted Nugent told the National Rifle Association convention last week that he'll be dead or in jail if President Barack Obama is reelected has been removed from YouTube following reports that Nugent had been contacted by the Secret Service. 

Right Wing Watch's Josh Glasstetter first noticed that the video had been removed and replaced by a message that states it has been removed by the user. 

Nugent, who is also a columnist at the Washington Times and whose endorsement was welcomed by presidential candidate Mitt Romney , told the NRA in his speech that If Barack Obama becomes the president in November, again, I will either be dead or in jail by this time next year. 

Nugent has carved out an over-the-top right wing media persona over the past few years. In 2008 he invited candidate Barack Obama to suck on his machine gun. Of course, Obama wasn't the president then.
