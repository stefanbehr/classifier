With a flick of his pen, Florida's tea party Republican governor, Rick Scott, used a line-item veto to cut funding to the state's rape crisis centers last week in the middle of Sexual Assault Awareness Month. 

The centers in all of Florida's 67 counties, coordinated by the Florida Council Against Sexual Violence , are supported by a trust fund made up of fines levied on sexual offenders. But the fund, established in 2003, isn't yet large enough. Fewer than 10% of sexual violence programs are able with current resources to provide the standard services identified as those most needed by rape victims, the group's site states. As a result, many programs have waiting lists. 

The state legislature had approved $1.5 million to help close the gap so the centers could keep serving the approximately 700,000 women in Florida who've been victims of rape. But in reviewing the state's $70 billion budget, Scott decided last Tuesday that the .002 percent slated for the crisis centers was just too much. He used his line-item power to veto the funds, alongside $141 million in other cuts targeting a wide range of projects, including an indigent psychiatric medicine program, Girls Incorporated of Sarasota County, the Alzheimer's Family Care Center of Broward County, and a state settlement for child welfare case managers who were owed overtime. The entire list of vetoed programs is available here (PDF). 

But the cuts to rape crisis centers first reported by the nonprofit investigative Florida Independent especially hurt, coming as they did in the middle of Sexual Assault Awareness Month . Jennifer Dritt, director of the Florida Council Against Sexual Violence, told the Independent she was disappointed by the veto, since the council's money had been included in the budget passed by the mostly Republican legislature. There must be some kind of disconnect, she said. We are really surprised and frankly stunned [and] are trying to figure out what the heck happened. 

Scott's explanation, offered by spokesman Lane Wright, was that this new funding of $1.5 million would have been duplicative, since, as a state, we already fund sexual violence programs The state already provides about $6.5 million for rape prevention and sexual assault services. 

Dritt said the governor needs to check his numbers. He's probably including rape prevention and education money, she said. You think they would have asked us about that, and we could explain to them very clearly what money is available for our programs. It looks like $1.5 million is a lot of money to ask for, but frankly, when you spread it across 67 counties, it's not. 

It's not a lot of money, but it could have a lot of impact and not just on rape victims, but on politics in the state. In his defense of the governor, spokesman Wright quickly added : [A]nyone who's trying to say this veto is evidence of a war on women is deliberately trying to mislead the public for political ends. 

Who would say that?
