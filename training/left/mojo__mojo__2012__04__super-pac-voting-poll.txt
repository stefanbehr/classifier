The post has been updated. 

The ultra-rich may be psyched that super-PACs give them more power to influence elections, but average voters aren't wild about the new election spending groups, according to a new poll commissioned by the Brennan Center for Justice at NYU School of Law. 

One in four respondents in the poll said they're less likely to vote in elections this year because of the growing influence of super-PACs. That percentage climbs among those earning less than $35,000 a year (34 percent) and those with only a high school education (34 percent). The perception that super-PACs are corrupting government is making Americans disillusioned, and an alarming number say they are less likely to vote this year, Adam Skaggs, senior counsel for the Brennan Center's democracy program, said in a statement. (The poll's margin of error is 3.1 percentage points.) 

The Brennan poll also found that 73 percent of respondents agreed with that statement that there would be less corruption if there were limits on how much could be given to super-PACs. Nearly 70 percent concurred that new rules that let corporations, unions, and people give unlimited money to super PACs will lead to corruption. And the majority of those polled disagreed that regular voters enjoy equal access to candidates as big super-PAC donors. (One in five said they had access was the same.) 

Brad Smith, chairman of the Center for Competitive Politics, which supports deregulating the campaign system, wrote in an email that the Brennan poll's result were utterly predictable but said there was no hard evidence proving that a spike in political spending depresses voter turnout. He also noted that Gallup's tracking of public trust in government had ticked upward since the advent of super-PACs. Given the hysteria over super-PACs and the well-documented errors in media coverage of them, it is not surprising that people feel negatively about them, he added. But the facts don't square with conventional wisdom. 

Read the full results of the poll here .
