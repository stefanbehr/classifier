This story has been updated. 

McDonald's, it turns out, isn't the only fast-food giant to have cut ties with the American Legislative Exchange Council , the corporate-funded organization that writes up model bills for thousands of state lawmakers nationwide. Wendy's said on its official Twitter feed on Tuesday night that it, too, had left ALEC. We decided late 2011 and never renewed this year. It didn't fit our business needs, the company tweeted . Wendy's is currently not a member of ALEC, it stressed. 

Bob Bertini, a spokesman for Wendy's, confirmed the move. The tweet is correct. Wendy's is not a member of ALEC. Last year, we made the decision not to renew for 2012, he wrote in an email to Mother Jones . 

Wendy's departure is arguably more significant than McDonald's given Wendy's past support for conservative and staunchly pro-industry causes. For instance, Wendy's International has funded the Center for Consumer Freedom , a phony grassroots group that fights regulation of the food and beverage industries . And Wendy's political action committee has given significantly more of its money in recent election cycles to Republican lawmakers than Democrats, according to the Center for Responsive Politics. 

Wendy's joins a handful of other major food and beverage companies McDonald's, Pepsi , Coca-Cola , Kraft in ditching ALEC. Intuit , which makes the Quicken personal finance software, has also ended its ALEC membership. 

The departures come as a coalition of liberal groups presses high-profile ALEC to members to leave group over what the coalition calls the discriminatory voter identification legislation peddled by ALEC. The coalition, which includes Common Cause and ColorofChange.org, has also bashed ALEC for supporting Florida's Stand Your Ground law at the center of the Trayvon Martin shooting in central Florida. 

On Tuesday, ALEC released a statement in response to what it called a well-funded, expertly coordinated intimidation campaign pressuring member companies to cut ties with ALEC. At a time when job creation, real solutions and improved dialogue among political leaders is needed most, ALEC s mission has never been more important, said Ron Scheberle, ALEC's executive director. This is why we are redoubling our commitment to these essential priorities. We are not and will not be defined by ideological special interests who would like to eliminate discourse that leads to economic vitality, jobs and fiscal stability for the states. 

UPDATE: Common Cause, the good-government group that's part of the coalition pressuring member companies to leave ALEC, says in a statement responding to ALEC that It's no surprise that ALEC would call the free exercise of democracy an 'intimidation campaign.' Common Cause goes on to say: 

The truth is that many Americans find ALEC's secretive efforts to push public policy for the economic and partisan interests of its corporate sponsors both undemocratic and unsavory. That's why companies like Coca Cola, Pepsi, Intuit, Kraft, and McDonalds are exercising their free market right to pull out. Good corporate managers know that attaching their brand to radical and divisive legislation is not in the best interest of their shareholders, and Common Cause applauds their leadership. 

Sunlight is the best disinfectant, and we will continue to educate the public about ALEC's agenda, and highlight the influence of corporate money in our state laws and public policies, said Bob Edgar, president of Common Cause. 

Print Email Tweet Super Rich Still Have Little To Fear From IRS We re Still at War: Photo of the Day for April 11, 2012 Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter If You Liked This, You Might Also Like... 

Exclusive: McDonald s Says It Has Dumped ALEC 

The fast food king joins Pepsi, Kraft, and Coca-Cola in leaving the corporate-backed group. The Gates Foundation Is Done Funding ALEC 

The $34 billion charity joins Kraft, Coca-Cola, and Pepsi in cutting ties with the corporate-backed ALEC. GOP, ALEC Could Make It Harder For 5 Million To Cast Ballots 

Ghostwriting the Law 

A little-known corporate lobby is drafting business-friendly bills for state legislators across the country. Behind Michigan s Financial Martial Law : Corporations and Right-Wing Billionaires 

The think tank that inspired Gov. Rick Snyder's budget bill is bankrolled by Koch money and some of the same donors that backed Wisconsin's attack on unions. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
