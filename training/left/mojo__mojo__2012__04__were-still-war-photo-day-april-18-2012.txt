US Army Staff Sgt. Charles Stokes pauses while on patrol in a local village near Combat Outpost Terezayi in Afghanistan's Khowst province , on April 10, 2012. Photo by the US Army.
