On Thursday, Charles Taylor, the former president of Liberia, became the first head of state convicted of war crimes by an international court since German naval commander Karl D nitz ( Hitler's successor ) faced judgment at the Nuremberg trials. Taylor, who cut his teeth in the '80s as an embezzler and a warlord, was convicted by a UN-backed court in The Hague of aiding and abetting war crimes and crimes against humanity. The Liberian government handed him over to UN security officials in March 2006. 

The court at The Hague found Taylor guilty of providing weapons and technical support to Revolutionary United Front rebel forces fighting in the brutal civil war in neighboring Sierra Leone between 1991 and 2002. (The rebels paid Taylor in blood diamonds in exchange for his support.) The RUF army gained international notoreity for its child soldiers , sadistic attacks on civilians, and widespread use of torture. Announcing the verdict, presiding judge Richard Lussick called Taylor's support for the RUF fighters sustained and significant. Taylor will serve out his sentence in a maximum security prison in the United Kingdom. 

Taylor's six-year presidency was by marked by a record of repression and the Second Liberian Civil War . Here are some bizarre facts about the busted war criminal. 

1. Taylor went to school in the United States. Like other mass murderers and foreign terrorists, Taylor was educated in America. In 1977, he graduated with a degree in economics from Bentley University in Waltham, Massachusetts. While at studying at Bentley, Taylor was also busy fathering a second child and showing off his sports car on campus grounds. Other notable Bentley alumni include Dallas Cowboy Mackenzy Bernadeau and Mike Mangini, the drummer for the prog-metal band Dream Theater. Comedian Jay Leno also attended, but dropped out after his first semester. 

2. There's a chance Taylor was busted out of jail by CIA spooks. During war crimes court testimony in 2009, Taylor claimed that the US government conducted a covert operation to break him out of the Plymouth County Correctional Facility in Massachusetts while he was detained there in 1985. (He had been imprisoned on an extradition warrant related to the embezzlement of nearly a million dollars in Liberian government funds.) Four years later, Taylor participated in the coup in Liberia that would pave the way to his path to power overthrowing the government of military dictator Samuel Doe, whom the United States had come to find useless and corrupt . In January 2012, the Boston Globe ran an investigation alleging ties between Taylor and the CIA, further fueling speculation that the former Liberian strongman doubled as a pawn in some convoluted geopolitical spy game. 

3. His presidential campaign slogan was unbelievably bad . For the 1997 Liberian general election, his supporters regularly chanted this slogan: 

He killed my Ma, he killed my Pa, but I will vote for him . 

Yes We Can, this was not. But it was on this slogan (along with his platform of intimidation and bribery ) that Taylor won 75 percent of the vote and made the career move from feared warlord to head of state. 

4. He costarred with Nicolas Cage . Well, not really, but Taylor's charisma and flamboyance , and his brutal legacy, served as the basis for the character Andre Andy Baptiste Sr. (played by the great Eamonn Walker ) in the 2005 drama-satire Lord of War . Baptiste is an American-educated, RUF-supporting, blood-diamond loving Liberian dictator, so it's not like the association is subtle. Here's a short clip from the film: 



5. Taylor and Qaddafi were BFFs. In the 1980s, Taylor received training at one of the desert revolutionary camps run by the late Libyan despot Moammar Qaddafi. After his stint at terrorism summer camp, he launched the National Patriotic Front for Liberia (NPFL), a rebel faction that fought in the first Liberian civil war between 1989 and 1996. Following Taylor's 2006 arrest, Qaddafi spoke out in support of his West African prot g : This means that every head of state could meet a similar fate. It sets a serious precedent, Qaddafi warned. 

In a just a few years time, Qaddafi would indeed meet a similar fate . 

6. He used his bling to disarm not only supermodels , but televangelists, too. Pat Robertson thought it was a good idea to say this back in 2006 : 

So, we're undermining a Christian, Baptist president to bring in Muslim rebels? How dare the [President Bush] say to the duly elected [Charles Taylor], 'You've got to step down.' It's one thing to say, we will give you money if you step down and we will give you troops if you step down, but just to order him to step down? He doesn't work for us. 

Robertson's open support for Taylor might have had something to do with that multimillion dollar gold mining deal he cut with the Liberian ruler. 

7. He was ready for a second go. Like Saddam Hussein and other captured or deposed tyrants, Taylor was convinced that he would get another shot at leading his country. Joshua Hammer wrote the following for a 2006 issue of Mother Jones : 

Unless and until Taylor is convicted, safely imprisoned, and forgotten by the rebels of West Africa, nobody is going to rest easy. Having fought his way to power, Taylor could do so again, says Rep. Edward Royce (R-Calif.), who led the bipartisan congressional effort to pressure Nigeria to surrender Taylor. Royce recalls the words Taylor uttered upon fleeing Liberia, in his own aircraft, back in August 2003: God willing, I will be back. 

After today's verdict, it looks like Taylor was envisioning a curtain call that will luckily never come.
