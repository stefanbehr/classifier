Thought abstinence only education was a thing of the past, at least at the national level? Looks like it's still part of the officially sanctioned curriculum from the US Department of Health and Human Services for teen pregnancy prevention. 

The HHS Office of Adolescent Health lists the Heritage Keepers Abstinence Education as one of the 31 evidence-based programs that met the effectiveness criteria for preventing teenage pregnancy. RH Reality Check flagged its inclusion on Tuesday, noting that the program was quietly added to the list sometime in April. The program is based in South Carolina and focuses on schools in the state. 

The Heritage program is aimed at middle and high schoolers and advocates abstaining from sex until marriage. Heritage Community Services, which created the curriculum, describes its programs as a logic model that addresses the risky behavior of adolescents from the perspective of changing the behavior that is causing the problem rather than dealing with the consequences of the risky actions. 

The HHS fact sheet on the program lists five different sections, on topics like sexual abstinence and family formation. The STD Facts section discusses how to refuse sex in different settings. Students are also taught a four-step plan for resisting sexual activity, and provided with role-playing exercises to help students practice it. In other words, don't count on learning anything along the lines of what young people should do if they are having sex. There's no talk about condoms or birth control. And if you're gay, forget about it. 

The RH Reality Check piece notes that an August 2007 report on the Heritage program prepared for HHS found that it had little or no impact on sexual abstinence or activity. So it's not exactly clear why it's on a list of evidence-based programs that met the effectiveness criteria for HHS. 

Yet the Heritge website now proudly boasts that it is the only authentic abstinence education program in the United States identified by the U.S. Department of Health and Human Services as having demonstrated it's [ sic ] effectiveness.
