On the vast list of right-wing conspiracy theories about President Obama he's Malcolm X's illegitimate son ; he was photoshopped into the iconic Situation Room photo; his memoir was ghost-written by Bill Ayers; he's people! this new bit of muckraking from conspiracy cauldron WorldNetDaily deserves a place of honor on the mantle. 

According to Jack Cashill, an Emmy-award winning independent writer and producer with a Ph.D. in American Studies from Purdue, President Obama didn't simply have Bill Ayers ghost-write his memoir he had someone ghost-write love letters to his college girlfriend too: 



In a recent Vanity Fair excerpt, [David] Maraniss reprints two extended excerpts from one of those letters. I believe that these letters were volunteered to Maraniss to impress the kind of people who read the New York Times. 

If so, the Times took the bait. Reporter Adam Hirsch gushes over the young Obama's literary sensibility and his ironic, literary mind. Although regretting that Obama s authenticity has not made him a transformative figure, Hirsch remains certain that Obama has it in him to produce the best post-presidential memoir ever if he is willing to let that unguarded early voice speak again. 

What Hirsch refuses to question is whether that unguarded early voice is Obama's own. 

Among other things, Cashill wonders why Maraniss hasn't produced original copies of the love letters: In 1982-1983, when the Vanity Fair letters were written, college students did not use word processors. If they typed, they did so on a typewriter. The odds are that this letter, if an original, was not typed. And why, he asks, were Obama's letters so erudite and coherent when his other writing reveal him to be a bumbling nincompoop? 

Nowhere in Dreams is there any mention of T.S. Eliot, M nzer or Yeats, or any of the themes in this letter that so excited Adam Hirsch. As Obama tells it, he and his pals discussed neocolonialism, Franz Fanon, Eurocentrism, and patriarchy. This I can believe. 

Totally missing from Dreams, too, are the more exotic words in the letter to McNear: ecstatic, mechanistic, asexual, stoical, moribund, reactionary, fertility, dichotomy, irreconcilable, ambivalence, plus hazard and counter used as verbs, as in I will hazard these statements and Counter him with Yeats and Pound. 

Cashill's argument is, of course, foolproof. One small quibble, though: If Dreams from my Father was as WorldNetDaily has already established ghost-written, we can hardly use that as an example of Obama's sub-par writing standards. The plot thickens.
