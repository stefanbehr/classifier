David Corn and Salon's Joan Walsh joined host Chris Matthews on MSNBC's Hardball to discuss the newly minted Romney-Trump alliance and what it says about Mitt Romney's strategy and values. 

David Corn is Mother Jones' Washington bureau chief. For more of his stories, click here . He's also on Twitter .
