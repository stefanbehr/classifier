Mother Jones Washington bureau chief David Corn joined host Bob Schieffer on CBS's Face the Nation on Sunday morning to discuss Obama, Romney, and rough 2012 battle ahead. We live in a time where I think fisticuffs happen by the nanosecond, and no one waits, Corn says. 

Peggy Noonan , Michael Gerson , and John Dickerson also appeared on the panel. 



David Corn is Mother Jones' Washington bureau chief. For more of his stories, click here . Follow him on Twitter .
