When I began investigating right-wing celebrity and evangelical activist Kamal Saleem's past earlier this year, large parts of his story seemed to fall apart. Although in his book, Saleem, a self-proclaimed ex-terrorist with ties to Moammar Qaddafi and Saddam Hussein, claimed to have called the cops after nearly being murdered by a band of swarthy-looking South Asian hitmen in Southern California, there was no record of a report being filed with any of the local police departments. The FBI couldn't corroborate his claim to have advised them on counterterrorism strategies. And a former roommate in Oklahoma disputed the notion that he could have been waging a stealth jihad as a broke immigrant who needed a ride just to leave the house. 

The fact that Saleem's credentials as an ex-terrorist with inside knowledge of the Islamist plot to take over the United States were dubious at best has done nothing, however, to diminish his star power on the right. He's still a fixture at churches and at conservative confabs. Right Wing Watch reports on his latest appearance, at The Awakening conference in Orlando late last month. Per Brian Tashman , Saleem not only detailed a treacherous scheme by President Obama to use immigration reform to legalize terrorism, but also uncovered a liberal plot to use the Supreme Court's 1973 decision in Roe v. Wade to 'bring Sharia law liberally in our face.' 

Essentially, his argument is that Roe v. Wade broke down the basic tenets of the American legal system, paving the way for an Islamist takeover. You can watch it here: 



It's a trap!
