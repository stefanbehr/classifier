Since the US Supreme Court stayed a Citizens-United -defying ruling by Montana's Supreme Court in February, politicians and advocacy groups have lined up to take sides, filing amicus briefs urging the high court to let stand, reverse, or review the decision. The majority of the briefs, like the one submitted by Sens. John McCain (R-Ariz.) and Sheldon Whitehouse (D-R.I.) , come from petitioners upset by the unlimited outside spending triggered by Citizens United . 

Yet those calling on the Supreme Court to let the Montana ruling stand ( including one group with a rather unorthodox argument ) or use it as an opportunity to roll back Citizens United face an uphill battle . The court's conservative majority is likely to be more sympathetic to those friends of the court calling upon it to summarily reverse ( i.e. , overturn without hearing) the Montana ruling. Their main arguments: Want to ditch Citizens United ? Try this choose-your-own-adventure-style guide to making America super-PAC-free. 

Why mess with a good thing? 

In his amicus brief, Senate Minority Leader Sen. Mitch McConnell (R-Ky.) argues (PDF) that the Montana Supreme Court's decision to uphold the state's strict campaign finance laws should be summarily reversed since it contradicts Citizens United . Nothing that has occurred since that ruling warrants its reconsideration, his brief reads. It goes further, noting that the majority of super-PAC contributions come from individuals, not corporations, and therefore concerns about the ruling are greatly exaggerated. The brief also approvingly cites a column in the New York Post by Reason 's Jacob Sullum that claims that independent groups, funded mainly by wealthy individuals, have increased competitiveness, which is usually considered good for democracy. 

Corruption ? Sorry, can't hear you. 

The US Chamber of Commerce, which has spent upwards of $3 million against Democrats in this election cycle and has vowed not to disclose its donors , argues (PDF) that in light of Citizens United it is settled law that independent expenditures do not create the appearance of corruption because they aren't donated directly to candidates. As Lee Fang notes , it's pretty clear that evidence of actual corruption does exist, pointing to a multitude of evidence filed by a judge in McConnell v. FEC , the 2003 case that upheld the McCain-Feingold campaign-finance limits (which Citizens United partly reversed). Nevertheless, the Chamber says that even if such evidence of corruption should come to light, the Supreme Court should not use empirical data to reconsider its previous ruling. 

Don't know much about history 

Citizens United, the group behind the case of the same name, has also weighed in. Montana Attorney General Steve Bullock argued that his state's unique history of political corruption, dating back to the political stranglehold held by the Anaconda Copper Mining Company in the late 1800s, was reason enough to disprove the Supreme Court's contention that independent expenditures do not give rise to corruption or the appearance of corruption. However, Citizens United says that the Montana ruling violates a 2009 Supreme Court decision, Northwest Austin Municipal Utility District Number One v. Holder , in which this Court confirmed that history alone is an insufficient ground for sustaining a constitutionally suspect statute. 

CU's brief also argues that Citizens United is not a factbound ruling and that any claims otherwise are disingenuous efforts to create an unconstitutional state-level exemption to free-speech rights. Meanwhile, Montana AG Bullock may have inadvertently strengthened his argument against Citizens United : After leaving his seat open to run for governor, an unprecedented amount of money has been poured into the Republican primary for attorney general, much of it from out-of-state PACs. 

Print Email Tweet Texas Democratic Primary Just Got Real Dem Poll Shows Walker and Barrett Tied in Recall Gavin Aronsen 

Writing Fellow 

Gavin Aronsen is a writing fellow at Mother Jones . For more of his stories, click here . Follow him on Twitter or send an email to garonsen [at] motherjones [dot] com. RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Want to Ditch Citizens United? A DIY Guide 

Pick your own path to an America without super-PACs. Could Citizens United Be Toast in Just Two Months? 

A reform group claims it's possible, thanks to its secret legal weapon: the 11th Amendment! Will One of These Cases Be the Next Citizens United? 

Four campaign finance lawsuits that could speed--or stem--the flow of unlimited election cash. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
