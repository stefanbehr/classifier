Wisconsin's June 5 gubernatorial recall , pitting Gov. Scott Walker against Milwaukee Mayor Tom Barrett , is no mere statewide race. It's a national fight. The Tea Party Express group calls Wisconsin ground zero for the battle against Obama's liberal agenda. 

It's not surprising, then, to learn that out-of-state money is pouring into the Walker recall at a record pace and it's powering the efforts of Democrats, Republicans, interest groups, and unions alike. 

In Wisconsin's 2006 gubernatorial election, as the Milwaukee Journal Sentinel reports , out-of-state campaign donations made up 15 percent of all donations. In 2010 it was 9 percent. But in the Walker recall? It's a staggering 57 percent. 

According to an analysis by the political-money-watching Wisconsin Democracy Campaign, $3 out of every $5 raised by Walker came from outside Wisconsin. Walker's largest donors include Texas homebuilding king and Swift Boat for Veterans backer Bob Perry, Las Vegas casino tycoon Sheldon Adelson, and Richard DeVos, heir to Amway fortune. A little more than $1 of every $10 given to Barrett was out-of-state campaign cash. 

Walker raised $13 million in the first three months of 2012, bringing his total fundraising haul since January 1, 2011, to $25 million. Walker benefitted from a quirk in state election law allowing him raising unlimited campaign cash for months to fend off the recall challenge. Barrett raised $750,000 in the first 25 days after entering the race in late March. 

Interest groups bankrolled by out-of-state cash are also playing a pivotal role in the recall. The Republican and Democratic Governors Associations, both based in Washington, DC , have together ponied up nearly $7 million for the Walker recall. The RGA, as Mother Jones has reported , is the GOP's corporate-funded dark money machine, shuffling tens of millions in campaign cash to boost Republicans and bash Democrats nationwide. Labor unions have pumped millions more into the groups We Are Wisconsin , which supported Democrats in last summer's state Senate recall races and supports Barrett now, and Wisconsin for Falk , which supported Kathleen Falk in the recall Democratic primary and opened field offices around the state. 

Print Email Tweet Corn on Hardball : Trump Revives the Birther Meme Americans for Prosperity: Our Pro-Walker Bus Tour Has Nothing to Do With Recall Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Dem Poll Shows Walker and Barrett Tied in Recall 

A labor official dismisses the "pre-mortems" predicting Walker's win on June 5. Scott Walker Pulls Ahead in New Recall Poll 

Republicans hold the edge in voter enthusiasm in the Badger State. Tea Partiers Backing Scott Walker May Run Afoul of IRS 

Will the Tea Party Patriots violate their tax-exempt status by going all in for the Wisconsin governor in the recall fight? It s Recall Time for Wisconsin Gov. Scott Walker 

On November 15, a grassroots effort to recall the controversial governor kicks off. Can it succeed? Tom Barrett Will Face Scott Walker In Wisconsin s Recall Election 

The Milwaukee mayor trounced liberal favorite Kathleen Falk. Now he gets a shot at revenge for his 2010 loss to Walker. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
