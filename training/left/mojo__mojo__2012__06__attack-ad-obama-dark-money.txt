It takes more than secret donors to run a dark-money group it takes chutzpah. For a perfect example of that, consider the letter that the American Future Fund sent to the Federal Election Commission in April, alerting the body that AFF wishes to speak out on issues of national policy significance with minimal government intrusion into its affairs. 

In its missive , the conservative 501(c)(4) social welfare group asked the FEC to issue formal advice on eight ads that it was planning to make. Specifically, AFF wanted to know if the proposed ads would trigger FEC disclosure requirements. If groups like AFF make ads that mention candidates by name but don't expressly tell you to vote for or against them, they have to file spending reports with the FEC. (They do not have to reveal their donors though a federal court case could change that. More on that below.) 

AFF, though, didn't want to subject itself to the burden of reporting its ad spending to the FEC. If it could convince the FEC that its ads were true issue ads and had nothing to do with any specific candidate, it would be in the clear. 

Here's how the group, with a wink and a nudge, proposed dancing around the rules by targeting President Obama in an ad without explicitly mentioning who it was talking about. Think of it as a non-attack attack ad. 

Scene one: AFF's script for its first proposed ad opens with an image of gas pumps and price displays as a narrator explains, Since 2008 began, gas prices are up 104 percent. And the US still spends over $400 billion a year on foreign oil. So far, so good. Just another issue ad. 



Scene two : The ad switches to an image of the Washington Monument as the narrator introduces audio of President Obama saying, We must end our dependence on foreign oil. 

Explaining its use of the president's voice, AFF told the FEC that the ad will not identify the speaker in any way. Only those familiar with President Obama's voice will know that it is President Obama speaking. Does this scene, AFF inquired, contain any reference to a clearly identifiable candidate for federal office? 

Scenes three through five: As the ad switches between images of science labs, the Washington Monument again, and middle east oil, the narrator says, But the government stopped American energy exploration and banned most American oil and gas production the government wants foreign countries to drill so we can buy from them , keeping us dependent on foreign oil and crippling our economy. 



Scene six: The narrator urges viewers to Tell the government it's time for an American energy plan...that actually works for America . If the spot is in keeping with AFF's other ad proposals with the same narration, this is where, Via on-screen text, the viewer would be instructed to 'Call the White House at (202) 456-1414.' 

Surely, AFF wondered, none of this could be found to contain any reference to a clearly identifiable candidate for federal office? 

Another proposed ad submitted to the FEC, which criticizes the president's health care reform legislation, pushes the limits even further on how much it can say without referring to a clearly identified candidate. Though the ad makes repeated mention of Obamacare, AFF argued that the health-care law is known to the public and discussed in the media almost exclusively by that name, and that a 2004 FEC advisory opinion said mentioning a candidate's name in a reference to his eponymous car dealership did not constitute electioneering communication. 

In a 4-to-2 ruling on June 13, the FEC commissioners shot down AFF's line of argument (PDF), concluding that all but one of its proposed ads clearly referred to Obama. And they didn't buy the notion that the president's voice wasn't a clear reference to him: As even one of our colleagues conceded during the Commission's discussion of American Future Fund's request, 'everybody and their dog' would recognize President Obama's voice. 

AFF sent its letter to the FEC after a district court took the first step toward closing a major loophole in April, ruling in Van Hollen v. FEC that 501(c) groups would have to start disclosing their donors if they ran electioneering communications, which clearly refer to a federal candidate and run in the 60 days before a general election. AFF also wrote that it was concerned that if its ads weren't considered authentic issue ads, it might hypothetically risk being compelled to violate its donors' privacy expectations. If the disclosure loophole does get closed it may not happen until after the 2012 election is over. 

In the meantime, AFF has gone back to making ads that make no bones about targeting Obama, like this online video slamming his support for rolling back Citizens United : 



Print Email Tweet Corn on Hardball: Why Obama Appeals to Latino Voters, But Romney Can t GOP Candidate Says Tammy Baldwin s Philosophy Has Its Roots in Communism Gavin Aronsen 

Writing Fellow 

Gavin Aronsen is a writing fellow at Mother Jones . For more of his stories, click here . Follow him on Twitter or send an email to garonsen [at] motherjones [dot] com. RSS | Twitter If You Liked This, You Might Also Like... 

Your Weekend Longreads List on Dark Money 

Some of MoJo's favorite in-depth stories on money and American politics from the New Yorker, Texas Monthly, and our own reporters. How Dark-Money Groups Sneak By the Taxman 

Nonprofits like Karl Rove's Crossroads GPS are all about "social welfare," not partisan politics. Well, at least that's what they tell the IRS. Will One of These Cases Be the Next Citizens United? 

Four campaign finance lawsuits that could speed--or stem--the flow of unlimited election cash. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
