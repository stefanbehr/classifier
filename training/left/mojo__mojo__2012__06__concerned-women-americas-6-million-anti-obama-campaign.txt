Concerned Women for America, a conservative anti-feminist operation dedicated to bring[ing] Biblical principals into all levels of public policy, announced late last week that it is spending $6 million to run ads that highlight the consequences of President Obama's health care plan. But $6 million is an unusually large ad buy for the group, which hasn't explained (and doesn't have to disclose) where it got the money. 

The ads claim that the bill is forcing doctors to drop many patients and that it will add billions of dollars to the deficit. Starting on June 20, the ads have been running in six key swing states: Iowa, Wisconsin, Minnesota, Virginia, New Mexico and New Hampshire, and CWA claims that the ad is the first presidential ad to run in the general election in Minnesota. 



During a presidential election, it's not unusual for outside groups to run ads attacking either candidate, especially using money from donors whose names don't have to be disclosed, as is the case with the CWA ads. The media buy was sponsored by CWA's lobbying arm, the Concerned Women for America Legislative Action Committee, which is a nonprofit 501(c)4. Unlike CWA, the CWA Legislative Action Committee is allowed to get involved in politics. 

The $6 million advertising blitz vastly exceeds the action committee's entire budget from the past several years. According to its most recent tax filings, filed in October last year, CWA's advocacy arm only brought in $2 million in 2010, and ended the year about $500,000 in the hole. The previous year, the group brought in less than a million dollars. 

That makes a $6 million ad buy a pretty significant investment, and suggests that CWA has gotten a big donor to foot the bill during this year's campaign season. But CWA doesn't have to say who gave it all that money, and a spokeswoman from the group did not respond to emails requesting comment. 

What's really curious about the CWA's ads, though, is the timing. They're airing just as the Supreme Court is expected to issue a ruling that could overturn the law at the heart of the commercials. CWA's move suggests that regardless of what the court does, health care is going to continue to be a major issue for the duration of the presidential campaign. 

Print Email Tweet Conspiracy Watch: Did Obama Give American Islands to Russia? We re Still at War: Photo of the Day for June 27, 2012 Stephanie Mencimer 

Reporter 

Stephanie Mencimer is a staff reporter in Mother Jones ' Washington bureau. For more of her stories, click here . You can also follow her on Twitter . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Monika Bauerlein and Clara Jeffery Talk Dark Money With Bill Moyers 

On the war for Wisconsin, the work of "journo-detectives," and the law that underlies all other law. This Week in Dark Money 

The Obama campaign taunts Karl Rove, attack ads haunt "Game of Thrones," and more news from the campaign money trail. The One-Sided War Against Obamacare 

Conservatives are fighting hard while liberals mostly cower. Obamacare Lives. What s Next? 

The Supreme Court has upheld President Obama's signature legislative accomplishment. Here's what it all means. If Obamacare Is Struck Down, These Americans Are in Trouble 

Millions could be hurt if the Supreme Court scraps health care reform. Here are their stories. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
