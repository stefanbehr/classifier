This post has been updated. 

One of the strongest pieces of political money disclosure legislation in the nation went down in flames on Friday, after Connecticut Gov. Dannel Malloy, a Democrat, vetoed the measure. Lawmakers are already talking about reviving the legislation in a special session as early as late June. 

Here's what House Bill 5556 supporters called it disclosure on steroids would do if passed on the second attempt. The bill would require groups to immediately disclose the cost, funding source, and target of any TV or radio ads or other communications that cost more than $1,000 and mentioned a candidate within 90 days of an election. Political advertisers who ran ads outside that 90-day window would have to disclose key information about their ads within 24 hours faster than current law requires. Ambiguously named groups like Connecticut Citizens for a Better Future or Americans for a Stronger Economy would have to disclose all donors who gave $1,000 or more; they would also have to include the names of their top five donors in any political ads. Any corporate political spending greater than $4,000 would have to be approved by a vote of that corporation's governing body. 

Reformers had hailed HB 5556 as a dream package of disclosure measures . Gov. Malloy's veto squashed that dream at least for now. 

Malloy wrote in his veto message that he believed parts of the bill to be unconstitutional, potentially infringing on individuals' free speech protections under the First Amendment. Other parts of 5556, he argued, represent poor public policy choices. He went on, While I have advocated for transparency in the elections and campaign finance process for a long time, and could certainly support sensible reform in this area again, I cannot support the bill before me given its many legal and practical problems. 

Malloy wasn't the only critic of HB 5556. Connecticut's largest business association, the state chapter of the American Civil Liberties Union, and the Connecticut Daily Newspapers Association all urged Malloy to veto the bill. The newspapers association intepreted the bill to mean that sponsoring and airing a political debate would force through the onerous process of calculating and reporting the expenses of the debate as an independent expenditure. 

Reformers blasted Malloy's constitutional complaints as misguided, and called his veto a pledge of support for dark money and unaccountable elections. Miles Rapoport, president of the left-leaning think tank Demos, said on Friday: The governor's veto statement argues that HB 5556 is unconstitutional under the First Amendment, but this argument is simply incorrect. The Supreme Court has made clear on numerous occasions, including in Citizens United itself, that disclosure laws are on firm constitutional footing. 

Nick Nyhart, president of the pro-reform group Public Campaign, jabbed Malloy even harder : Malloy had a chance to be a national leader on reform, but opted to side with wealthy special interests and secret money. 

Print Email Tweet Chart: Why Liberals Lose Primaries More Often Than Conservatives Republicans Bad-Faith Objections to Letting DREAMers Stay Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

How to Sweep Dark Money Out of Politics 

Undoing Citizens United, the DIY guide. How Dark-Money Groups Sneak By the Taxman 

Nonprofits like Karl Rove's Crossroads GPS are all about "social welfare," not partisan politics. Well, at least that's what they tell the IRS. It Takes Dark Money to Make Dark Money 

Karl Rove's dark-money group doles out $2.75 million to an outfit fighting to keep dark money in the dark. Could This Pro-Walker Dark-Money Group Torpedo Recall Turnout? 

The Coalition for American Values doesn't want you to know who's behind it, but its last-minute Wisconsin ad blitz could affect the outcome of Tuesday's election. The Dark Money Behind the Wisconsin Recall 

Some startling stats about the $63.5 million election to decide the future of Gov. Scott Walker. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
