David Corn and the Huffington Post's Howard Fineman joined Chris Matthews on MSNBC's Hardball to discuss the tough work that lies ahead for the Obama campaign, especially in light of Friday's dismal unemployment numbers . 



David Corn is Mother Jones' Washington bureau chief. For more of his stories, click here . He's also on Twitter .
