As they dominate the airwaves with political ads, dark-money nonprofits are coming under increased scrutiny. At issue is the charge that they're running afoul of their tax-exempt status by focusing on partisan political activity. Earlier this week, New York Attorney General Eric Schneiderman sent subpoenas to a nonprofit affiliated with the Chamber of Commerce to see if millions of dollars had been improperly funneled to the Chamber (a 501(c)(6) nonprofit) for political purposes. 

Now, the Wall Street Journal reports , the Internal Revenue Service has begun a process of determining whether Karl Rove's Crossroads GPS should have to give up its nonprofit 501(c)(4) status and disclose its donors. Democrats and campaign-finance watchdogs have been urging regulators to crack down on groups like Crossroads GPS; last week the Obama campaign's top lawyer sent a snarky letter to Rove saying he was filing a complaint with the FEC laying out the case obvious to all that Crossroads is a political committee subject to federal reporting requirements. 

If the IRS revokes Crossroads GPS' or other 501s' nonprofit status, the Journal explains, the organizations could be held liable for large tax bills for the millions of dollars they have received tax-free. However, reaching such a determination will likely take months. Which means that dark-money groups can keep spending freely on ads and promising anonymity to their donors through the November election. 

For more, check out our interactive dark-money universe map , which tracks Crossroads GPS and 17 other 501 groups. 

Print Email Tweet Congressional GOPers React to Health Care Ruling: Obamacare=Cancer Was the Obamacare Dissent Originally the Majority Opinion? Gavin Aronsen 

Writing Fellow 

Gavin Aronsen is a writing fellow at Mother Jones . For more of his stories, click here . Follow him on Twitter or send an email to garonsen [at] motherjones [dot] com. RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

New York Attorney General to US Chamber Affiliate: I m Coming for You 

AG Eric Schneiderman is probing whether a foundation broke the law by sending $18 million to the US Chamber. Anatomy of a Non-Attack Attack Ad 

An inside glimpse at the thinking--and chutzpah--behind a dark-money group's election ads. An Interactive Map of the Dark-Money Universe 

Red giants, blue dwarfs, and cash-sucking black holes: Welcome to the final frontier of money in politics. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
