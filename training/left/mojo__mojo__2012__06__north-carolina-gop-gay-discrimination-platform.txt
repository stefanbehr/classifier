During the first weekend of June, Republicans, including guest speakers Donald Trump, Rick Perry, and former Minnesota governor Tim T-Paw Pawlenty, gathered at the North Carolina GOP State Convention in Greensboro. Energized by last month's passage of Amendment One , which banned recognition of any domestic legal union except heterosexual marriage, the state's GOP presented a platform with a plank that effectively condones discrimination based on sexual orientation. 

Section 3 of Article III (titled Individual Liberty ) reads: Government should treat all citizens impartially, without regard to wealth, race, ethnicity, disability, religion, sex, political affiliation or national origin. We oppose all forms of invidious discrimination. Sexual orientation is not an appropriate category. 

This [plank] is something that has been ongoing, Buck Golding, a member of the 2012 Platform Committee, explained in a phone interview. Referring to Amendment One (which passed with 61 percent of the vote) he added, We had a poll here. Marriage in my state is thought of as between a man and a woman and I concur with that. 

North Carolina LGBT rights groups called the platform an inaccurate representation of Republican interests, pointing out the broad bi-partisan opposition to anti-gay legislation. Equality NC, an active opponent of Amendment One, released a statement shortly after the convention denouncing the state's GOP members for seek[ing] to put our state on the wrong side of history. 

The state Republican Party platform targeting LGBT North Carolinians stands in stark contrast to the recent tidal wave of Republican opposition to the anti-LGBT Amendment One, said Dan Gurley, Equality NC's Board Chairman, who was formerly a RNC Field Director and the Executive Director of the North Carolina Republican Party. North Carolina Republicans deserve better than an agenda that would ceaselessly expand government encroachment in the lives of LGBT North Carolinians as well as singling out gay and lesbian citizens for exclusion from basic provisions. 

And while the gay marriage ban dominates the current conversation, alarm bells are going off for those who noticed that the language of the plank goes further than denying same-sex couples the right to get hitched. As the New Civil Rights Movement , an online journal of LGBT issues, pointedly blogged : 

Sexual orientation is not an appropriate category of school kids to protect from bullying? 

Sexual orientation is not an appropriate category .of workers to protect from being fired? 

Sexual orientation is not an appropriate category of travelers to guarantee a night's lodging? 

Sexual orientation is not an appropriate category of college kids to keep from being hung on a fence post? 

Most states are behind on granting LGBT citizens the civil liberties protections that most Americans take for granted; as the Human Rights Campaign notes, it is still legal to fire people for their sexual orientation in 29 states. But given bipartisan efforts around the country to advance civil rights for all citizens, a pro-discrimination manifesto is not just beyond the pale, it's totally behind the times. And though platforms aren't policies, they are our best indicator of the values that party officials will try to turn into law and in states where one party controls both houses of the legislature, as the GOP does in North Carolina, they just might succeed.
