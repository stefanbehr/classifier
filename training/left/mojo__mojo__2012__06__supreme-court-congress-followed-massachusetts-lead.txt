The Supreme Court upheld the Affordable Care Act by a 5 to 4 vote on Thursday. Maybe you heard. (My colleague Adam Serwer has a great analysis of what this actually means .) GOP presidential nominee Mitt Romney, responding to the ruling in front of the US Capitol, reiterated his pledge to spend his hypothetical first day in office working to repeal Obamacare. (House Majority Leader Eric Cantor is already promising another repeal vote in early July.) 

But it's safe to say Romney, the only other elected official in American history to mandate that citizens buy health insurance, probably won't like this passage from Justice Ruth Bader Ginsburg's opinion, which partially concurred with Roberts and partially dissented: 

See our full coverage of the Supreme Court's Obamacare decision. Conservatives Despair Over Health Care Decision 

Full Text of the Supreme Court's Decision 

Obamacare Lives. What's Next? 

10 Things You Get Now That Obamacare Survived The Obama Administration's Disastrous Oral Arguments 

Ginsburg: Congress Followed Massachusetts' Lead Your Supreme Court Obamacare Playlist Was the Obamacare Dissent Originally the Majority Opinion? How Big a Deal Is the Court's Medicaid Decision? The Obamacare Ruling Kevin Drum Would've Liked To See 

By requiring most residents to obtain insurance, see Mass. Gen. Laws, ch. 111M, 2 (West 2011), the Commonwealth ensured that insurers would not be left with only the sick as customers. As a result, federal lawmakers observed, Massachusetts succeeded where other States had failed. See Brief for Commonwealth of Massachusetts as Amicus Curiae in No. 11 398, p. 3 (not ing that the Commonwealth s reforms reduced the number of uninsured residents to less than 2%, the lowest rate in the Nation, and cut the amount of uncompensated care by a third); 42 U. S. C. 18091(2)(D) (2006 ed., Supp. IV) (noting the success of Massachusetts reforms). In cou pling the minimum coverage provision with guaranteed issue and community-rating prescriptions, Congress followed Massachusetts' lead. 

Ginsburg, of course, is completely correct. And in a sane political climate, Mitt Romney would happily take credit for this. As it stands, he's in the uncomfortable position of once more distancing himself from his biggest political accomplishment. 

(h/t Politico 's Alex Burns .)
