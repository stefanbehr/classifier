A quick look at the week that was in the world of political dark money ... the money shot 





quote of the week 



We need our side to wake up. 

Democratic Senate Campaign Committee executive director Guy Cecil, in an plea to wealthy liberal donors to start giving to super-PACs to narrow the party's outside-spending gap. 

chart of the week 



Casino tycoon and former Newt Gingrich super-PAC megadonor Sheldon Adelson gave $10 million to the pro-Mitt Romney super-PAC Restore Our Future this week the largest disclosed donation in support of Romney to date. Adelson and his wife have contributed $35 million to super-PACs so far, and Sheldon has said that he plans to give at least $100 million to conservative groups . For the billionaire, that's just a drop in the bucket : 



STAT of the week 



$367/hour: That's how much the average House member has to raise to keep her seat. Senators must come up with $819 an hour. Check out our list of Congress' most and least expensive seats , on an hourly basis. 

race of the week 



Outside spending dominated the year's first general election contest, held Tuesday to replace Rep. Gabrielle Giffords (D-Ariz.) ( who stepped down after last year's assassination attempt ). Parties and super-PACs poured more than $2.3 million into the race between tea partier Jesse Kelly and former Giffords staffer Ron Barber (the victor). At least $1.1 million of it was in support of Kelly, including $100,000 from the Citizens United PAC, and nearly $200,000 from Karl Rove's American Crossroads. This recent ad from the House Majority PAC, which spent $458,000 supporting Barber and was the only Democratic super-PAC in the mix, featured a clip of Kelly calling Giffords a hero of nothing in 2010, before she was shot: 



more mojo dark money coverage 



How Dark-Money Groups Sneak By the Taxman : Nonprofits like Karl Rove's Crossroads GPS are all about social welfare, not partisan politics. Well, at least that's what they tell the IRS. 

Sheldon Adelson's $10 Million Donation to Romney Super-PAC : Is this just the tip of the Iceberg? 

Tune in: This weekend , MoJo editors-in-chief Monika Bauerlein and Clara Jeffery will appear on Moyers Company to talk dark money. Check your local listings for times. 

more must-reads 



Obama senior campaign adviser David Axelrod calls for a constitutional amendment to undo Citizens United after meeting with super-PAC donors. New York Times 

Campaign-finance reform advocates hail a decision to let people to donate to campaigns via text message. Center for Responsive Politics 

Corporations are people: A conservative dark-money group lists a corporation as a board member. Republic Report 

73 million cans of Natty Light and other stuff Sheldon Adelson could buy with $10 million. Huffington Post 

Print Email Tweet Corn on MSNBC: Romney s Better Economy and Obama s Choice We re Still at War: Photo of the Day for June 15, 2012 Gavin Aronsen 

Writing Fellow 

Gavin Aronsen is a writing fellow at Mother Jones . For more of his stories, click here . Follow him on Twitter or send an email to garonsen [at] motherjones [dot] com. RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
