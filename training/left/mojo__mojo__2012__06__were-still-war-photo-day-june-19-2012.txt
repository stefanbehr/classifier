Maj. Pete Reddan writes a song while leaning on the front tires of a C-17 Globemaster III at Joint Base Charleston, S.C. on June 13. Reddan's military inspired tune, Off to War , was recently recorded by Nashville recording artist Brad Anderson. The song was inspired by his experiences during deployments as well as the experiences of the men and women Reddan serves with. Reddan is a 437th Airlift Wing pilot. US Air Force photo by Airman 1st Class Ashlee Galloway. 

UPDATE: You can learn more about Reddan's song and this photo here .
