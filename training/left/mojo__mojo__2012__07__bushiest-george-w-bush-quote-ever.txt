From an interview late last week, allow us to present the Bushiest Bush quote that has ever been Bushed ( see the 4-minute mark below ): 

Eight years was awesome and I was famous and I was powerful. 

One more time: 

Eight years was awesome and I was famous and I was powerful. 

Just to recap, George W. Bush is saying that his 96 months as chief executive of America endorsing torture , launching war on convoluted make-believe , jumping the gun , decimating a record budget surplus, politicizing NASA and the DOJ , wiretapping , ditching Kyoto, bungling Katrina, restricting stem-cell research, fighting AIDS in Africa, pushing reasonable immigration reform, and getting pissed at the South Park creators were straight awesome. 

Will Ferrell is officially allowed to retire; George Walker Bush now ad-libs self-caricature at a level of sharpened, terse, crystallized wit that even the most practiced comic or impersonator could only hope to channel. (The only thing that was perhaps missing from this impressively formulated one-liner was Bush labeling his presidency as extreeeeemme . ) 

The quote was practically made for reverse-caption contests: 

1) 

Eight years was awesome and I was famous and I was powerful. Joyce Boghosian /The White House 

2) 

Eight years was awesome and I was famous and I was powerful. Wikia 

3) 

Eight years was awesome and I was famous and I was powerful. Crawford-Texas.org 

4) 

Eight years was awesome and I was famous and I was powerful. Joyce Boghosian /The White House 

5) 

Eight years was awesome and I was famous and I was powerful. Paul Morse /The White House 

As Andrea Higbie explained in Salon in 2008, awesome is one of Bush's go-to adjectives, used to describe everything from dead soldiers to the pope. This latest awesome quote comes from an interview last week with Peter Robinson, the man who wrote tear down this wall . During their hour-long conversation, the ex-POTUS talked baseball, family, and politics and the economic landscape today. (Bush was presumably there to push his just-released book of economic prescriptions, The 4% Solution .) As for other stuff that the former president is up to: The George W. Bush Presidential Center will open in a little over a year, and will become the second largest presidential library in the country. 

It's gonna be powerfully awesome. 



Here's the hour-long interview with Robinson:
