Iraq War veteran Jon Soltz and Mother Jones' David Corn take a closer look at Mitt Romney's foreign policy, revealing that both in terms of ideals and advisers it's not a far cry from George W. Bush's approach. 

David Corn is Mother Jones' Washington bureau chief. For more of his stories, click here . He's also on Twitter .
