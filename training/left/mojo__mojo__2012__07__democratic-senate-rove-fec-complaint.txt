Democrats are continuing their offensive against conservative dark-money groups, arguing that they are not social welfare nonprofits as they claim but rather political committees flaunting donor disclosure rules. In a new complaint to the Federal Election Commission first reported by the New York Times , Democratic Senatorial Campaign Committee executive director Guy Cecil calls out Karl Rove's Crossroads GPS , the senior citizen-focused 60 Plus Association, and the Koch brothers-funded Americans for Prosperity. The three groups, Cecil writes, are in the vanguard of using secret money to subvert the democratic process. 

The complaint singles out a factually challenged ad from 60 Plus targeting Sen. Sherrod Brown (D-Ohio), a similarly fuzzy Crossroads ad targeting Sen. Jon Tester (D-Mont.), and an Americans for Prosperity ad attacking Senate candidate Tim Kaine (D-Va.). Cecil writes ( PDF ): 

Each group shields its donors from disclosure by disavowing political committee status under FECA, and claiming exemption from tax under 501(c)(4) of the Internal Revenue Code. None has a legitimate claim...Outrageously, 60 Plus and AFP each told the Internal Revenue Service on its 2010 Form 990 that it engaged in no direct or indirect activities on behalf of or in opposition to candidates at all during the bulk of the 2010 cycle. These claims are risible on their face, given what is known publicly about these groups' activities. 

Crossroads spokesman Jonathan Collegio told the Times that complaints that fail to mention similar groups supporting Democrats doing exactly as their center-right counterparts are publicity stunts to promote partisan causes and are not taken seriously by serious people. Likewise, 60 Plus chairman James Martin said the complaint is naked politics, pure and simple. They need to stop their whining and stop trying to achieve with lawyers what they can t in the arena of public opinion. Last week, Senate Minority Leader Mitch McConnell penned an op-ed for USA Today arguing that requiring 501(c)(4)s to reveal their donors could inspire a Nixonian enemies list used to harass big political donors. 

While a federal appeals court ruled last month that the government must start determining the major purpose of 501(c)(4) groups like Crossroads GPS, the decision will likely be subject to more legal wrangling . And as the Times reminds us, the FEC is usually slow to respond to such complaints, and any action is unlikely to affect the 2012 election. 

Print Email Tweet The Key to Mitt Romney s $106 Million Haul? Barack Obama Corn on Hardball: Obama, Romney, and the Future of the Bush Tax Cuts Gavin Aronsen 

Writing Fellow 

Gavin Aronsen is a writing fellow at Mother Jones . For more of his stories, click here . Follow him on Twitter or send an email to garonsen [at] motherjones [dot] com. RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Obama Campaign to Rove: Reveal Your Secret Donors 

The president's chief election lawyer has some snarky words for Karl Rove and his dark-money group Crossroads GPS. IRS Takes a Closer Look at Rove s Dark-Money Group 

Is Crossroads GPS bending tax rules? We may not find out until after the election is long over. Karl Rove s Crossroads Groups Double 2012 War Plan to $240 Million 

Yes, you read that right. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
