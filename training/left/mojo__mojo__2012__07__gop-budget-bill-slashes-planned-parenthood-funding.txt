The Planned Parenthood wars are back on in Congress. On Tuesday, Rep. Denny Rehberg (R-Mont.) released a Labor, Health Human Services and Education appropriations bill that would slash funding for Planned Parenthood and any other abortion providers. 

The bill is a grab-bag of nearly everything Republicans want on reproductive issues. In addition to cutting off funds needed to implement President Barack Obama's Affordable Care Act, it eliminates Title X family planning funds, allows employers to opt-out of covering contraception or pretty much any other medical care on the basis of religious beliefs or moral convictions, and creates $20 million in abstinence education grants. 

The bill states that Planned Parenthood could continue to get federal funds if it stops offering abortions. 

Markups on the bill were held Wednesday morning, and it's likely going to be used as a starting point for budget negotiations with the Democrat-controlled Senate, the Huffington Post reports .
