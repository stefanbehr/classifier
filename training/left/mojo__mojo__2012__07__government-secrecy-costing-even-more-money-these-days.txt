$12 billion is a lot of money. $12 billion can buy you one NFL lockout , the most expensive house in the world ( twelve times over ), or a month's worth of occupying Iraq. 

It's also the amount the Obama administration spent to keep government information classified in 2011. 

Via the Federation of American Scientists, citing figures reported last week by the Information Security Oversight Office : 

The estimated cost of securing classified information in government increased last year by at least 12% to a record high level of $11.36 billion. An additional $1.2 billion was spent to protect classified information held by industry contractors...The ISOO report breaks down the expenditures into six categories (personnel security, physical security, etc.). But it does not provide any explanation for the rapidly escalating cost of secrecy...While some essential security costs are fixed and independent of classification activity, the failure to rein in classification and especially overclassification is a likely contributor to marginal cost growth. 

For 2010, the ISOO put the total secrecy price tag at around $10.17 billion , a 15 percent increase from 2009. The 2010 and 2011 estimates are lowball numbers, though, because the ISOO reviews the classification of 41 agencies, but not the CIA and NSA , among others. (For certain intelligence agencies, the act of classifying is itself classified , so wrap your head around that.) 

The ballooning financial cost of classification is more or less in lockstep with how the most transparent administration ever conducts business with regards to national security matters. When taken together with the Obama administration's Xeroxing of Bush-era State Secrets policy and its unprecedented clampdown on leaks and whistleblowing it's surreal to look back on what the president said on, for instance, his second day in office: 

The old rules said that if there was a defensible argument for not disclosing something to the American people, that it should not be disclosed. That era is now over. 



It's safe to say that it is long past due to officially declare the Obama era a transparency # fail .
