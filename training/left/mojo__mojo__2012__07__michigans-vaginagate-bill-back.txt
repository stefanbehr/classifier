Remember Michigan's omnibus anti-abortion bill ? It's baaaack. 

On Thursday morning, Michigan's Senate Judiciary Committee held a hearing and passed the bill out of committee by a vote of 3 to 1 a crucial step towards a vote in the full Senate. Abortion rights groups complained that the hearing was only announced on Wednesday, giving both legislators and the public little time to prepare. When the bill passed in the House last month, it erupted into an argument about the use of the word vagina. 

The bill requires abortion providers to meet the same standards as ambulatory surgical centers a provision often referred to as targeted regulation of abortion providers (or TRAP) laws by abortion rights advocates. That provision alone would likely shut down all abortion providers in Michigan. Other provisions of the bill require a physician to be physically present in order to dispense abortion drugs (which would outlaw the use of telemedicine abortions ), and implement new procedures for disposing of fetal remains that require them to be treated like the body of a dead person. 

The Center for Reproductive Rights put out a statement shortly after the committee vote calling on the Senate to end this relentless attack on Michigan women's constitutional rights. The full Senate is not back in session until August 15, so the bill isn't expected to move any further until after that.
