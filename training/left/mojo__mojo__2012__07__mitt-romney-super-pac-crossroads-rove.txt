June was the best month ever for super-PACs since their creation in 2010. In all, super-PACs large and small reeled in $55 million last month, according to the Sunlight Foundation . That brings their overall haul for the 2012 election cycle to $313 million. 

Leading the charge in the outside money wars was Restore Our Future, the pro-Romney super-PAC run by dark-money guru Carl Forti . Restore Our Future raised $21 million in June. Seven families ponied up $15 million of that haul: casino tycoon Sheldon Adelson and his wife, Miriam; Boston financier John Childs; Dallas investor Harlan Crow; the company owned by Bill Koch, brother of the billionaires Charles and David Koch; Houston homebuilder Bob Perry; former Rick Santorum bankroller Foster Friess; and TD Ameritade executive Joe Ricketts. 

And in an even more encouraging sign for the Romney super-PAC, USA Today points out that one in 10 donors to Mitt Romney's campaign have also given to Restore Our Future. Fundraisers says that kind of donor crossover is key to a presidential super-PAC's success and that crossover has so far eluded the super-PAC supporting President Obama. 

Restore Our Future wasn't the only GOP super-PAC to notch a record month. American Crossroads, the super-PAC cofounded by Karl Rove, raised $5.7 million in June, including $2 million from Texan Bob Perry. John Childs also chipped in $500,000 to Crossroads last month in addition to the million each he gave to Restore Our Future and the Club for Growth. 

Priorities USA Action, the pro-Obama super-PAC, also beat its monthly fundraising record, pulling in $6.2 million. Donors included Qualcomm founder Irwin Jacobs ($2 million), actor Morgan Freeman ($1 million), and Chicago media executive Fred Eychaner ($1 million). 

Print Email Tweet After Bachmann Allegations, Clinton Deputy Reportedly Under Police Protection Study: Super-Rich Hiding $21 Trillion In Offshore Tax Havens Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

The Money Man Who Helped Mitt Bank Florida 

Meet Carl Forti, the dark-money whiz who helped Romney bury Gingrich using tons of campaign cash. Dems: Dark Money Groups Use Secret Money to Subvert the Democratic Process 

A new complaint targets political nonprofits that attack Democrats and hide their donors. Follow the Dark Money 

The down and dirty history of secret spending, PACs gone wild, and the epic four-decade fight over the only kind of political capital that matters. Democratic Super-PACs Bank $25 Million--But Lag Karl Rove and Co. 

A quartet of Democratic super-PACs hauled in more than $25 million in April, May, and June of 2012. Sheldon Adelson s $10 Million Donation to Romney Super-PAC: Tip of The Iceberg? 

The casino mogul and his wife have given $25 million so far. They could give as much as $100 million to defeat Barack Obama and other Democratic candidates. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
