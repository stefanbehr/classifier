Before Mitt Romney was dodging questions about his tax returns and his role at Bain Capital , he was was trying to squirm out of stating a coherent immigration policy . In a new Spanish-language ad, Romney carefully avoids taking an actual position on immigration policy, substituting gauzy platitudes about bipartisan solutions and America being a nation of immigrants instead of saying what he'd actually do. That's probably because most Latino voters wouldn't like it if Romney said what he'd actually do. 

The ad, which is narrated by Romney's son Craig, also touts the fact that Mitt Romney's father was born in Mexico: 



The spot isn't only vague it's also misleading. As Huffington Post 's Elise Foley notes , Craig Romney says, As president, my father will work on a permanent solution to the immigration system, working with leaders of both parties. Typically that has meant comprehensive immigration reform, which Romney has opposed or supported over the years based on which part of the Republican base he's trying to appeal to. 

During the GOP primary, Romney campaigned as an immigration restrictionist, hammering his colleagues for supporting amnesty and talking up self deportation . He promised to veto the DREAM Act, though he won't say where he stands on Obama's decision not to deport the undocumented immigrants who might benefit from it. Now he's attempting to soften his record on immigration not by shifting from his hardline positions, but by moderating his rhetoric . It's easy to understand why the latest Latino Decisions poll shows President Barack Obama with 70 percent of the Latino vote to Romney's twenty-two percent. Romney doesn't have to win the Latino vote to make it to the White House, but those kind of numbers certainly hurt his chances.
