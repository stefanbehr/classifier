Justice Antonin Scalia told CNN's Piers Morgan in an interview Wednesday night that there's nothing to fear from unlimited political spending in elections as long as the American people know where its coming from. 

Defending his role in the Citizens United decision that struck down limits on political spending by corporations and labor unions, Scalia told Morgan that Thomas Jefferson would have said the more speech, the better. That's what the First Amendment is all about. So long as the people know where the speech is coming from. 

Scalia has expressed similar sentiments before, most notably in a 2010 case where anti-gay rights advocates in Washington State were attempting to block disclosure of signatories to a petition on the grounds that compelling them to do so violated their First Amendment rights. The Supreme Court disagreed, and in a concurring opinion Scalia wrote that There are laws against threats and intimidation; and harsh criticism, short of unlawful action, is a price our people have traditionally been willing to pay for self-governance. 

Requiring people to stand up in public for their political acts fosters civic courage, without which democracy is doomed. For my part, I do not look forward to a society which, thanks to the Supreme Court, campaigns anonymously and even exercises the direct democracy of initiative and referendum hidden from public scrutiny and protected from the accountability of criticism. This does not resemble the Home of the Brave. 

Nevertheless, Republicans are looking forward to that society. Once in favor of disclosure in political spending , post- Citizens United GOP elected officials have fought tooth and nail to protect the identity of secret donors trying to influence American elections, most recently by blocking the DISCLOSE Act. They have embraced the Sarah Palin interpretation of the First Amendment : that the Constitution envisions not just freedom of speech but freedom from criticism. 

Still, Scalia has experienced convenient changes of heart before that have brought him in line with mainstream GOP positions. But it doesn't seem like he's had one here yet.
