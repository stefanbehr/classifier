David Corn and Eugene Robinson joined host Chris Matthews on MSNBC's Hardball to discuss John McCain's comically hawkish speech, Condoleezza Rice's terrific speech, and the rest of the Bush era neocons in attendance at the Republican national convention. 



David Corn is Mother Jones' Washington bureau chief. For more of his stories, click here . He's also on Twitter .
