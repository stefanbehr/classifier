The official Republican party platform has some pretty extreme stuff . It condemns President Obama for standing up to the persecution of gay people in certain parts of Africa. It takes aim at the the creeping threat of Islamic Shariah law. It calls a nationwide ban on abortion with no exceptions for rape. So it shouldn't come as too much of a surprise that Texas was represented on the platform-drafting committee by none other than David Barton, the the right-wing historian whose work has influenced everyone from Newt Gingrich to the Texas State Board of Education. 

Barton's brand of history is creative. His driving idea is that the Founding Fathers were divinely inspired to found a nation based on Christian principals. As a consequence, he believes that the theory of evolution is antithetical to the Constitution, the Seventh Amendment bans abortion , Jesus would've opposed the minimum wage, and the federal government is controlled by demons . On Wednesday, he told Glenn Beck's television station that of the 71 amendments he'd introduced to the platform, 70 had passed. But Barton's prominence at the RNC comes even as his own work is facing increasing scrutiny from his longtime allies in the Evangelical community. They say many of his claims are unsbustantiated. Earlier this month, Barton's publishing house, Thomas Nelson, pulled his new book, The Jefferson Lies . 

When I ran into him outside the front security checkpoint in downtown Tampa, though, (he was easy to pick out, in his trademark cowboy hat and Texas-flag t-shirt), Barton wasn't backing down. 

David Barton waits outside a Rick Santorum rally in Tampa. See we've got all the documentation, he said. They've never asked for the documentation. So we're doing a response that comes back out that produces tons more than they've got and it makes them look shoddy. The response is it's the old thing of the rite of confrontation: One side sounds good until you've heard the other. 

Barton promises to shame his critics with new troves of information and testimony from experts. We have cartons of documentation, he said. We've taken groups of PhDs through it since the attack came out and they've all agreed, it's documented. The other guys may not like it. 

But could Barton provide the names of the professionally trained historians he's said are on his side? 

There are several. 

And who are they? 

They'll come out with their own thing. There's a group that will come out with it and stand on it. 

In other words, it's a secret. Still, even as his reputation continues to erode among Evangelical scholars, Barton said the kerfuffle over his record hasn't done much to hurt his business. Since he's found a new publisher, the sales have been through the roof.
