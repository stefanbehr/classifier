When Mitt Romney had his birther moment this morning, some defenders tried an age-old tactic to shift attention off the candidate's remarks: react to the reaction to the remarks. In this case, the conservatives in question worked at Michelle Malkin's website, the Twitchy, and their outrage was directed at a hashtag meme that had taken off on Twitter: 

When you've been dealt a bad hand, you can still play the race card. At least that s the strategy liberals subscribe to. After Mitt Romney cracked a birth certificate joke earlier today, the Left experienced nothing short of a major meltdown. Bereft of any rational thought, they decided to birth a ludicrous hashtag game, #FutureMittJokes. 

Actually, Twitchers, there's no need to blame liberals for spotlighting the presidential candidate's racial blindspot with some pointed tweets: You can just blame us. #FutureMittJokes was the brainchild of MoJo 's Adam Serwer, who spontaneously tweeted: 

Maybe Mitt could weave some more jokes about being a white dude who doesn't face racist assumptions into his stump speech. AdamSerwer (@AdamSerwer) August 24, 2012 

He got it warmed up with: 

I never get stopped and frisked in New York City either. # futuremittjokes AdamSerwer (@AdamSerwer) August 24, 2012 

From there, it just sort of took off. With writers from: 

The American Prospect: 

No one has ever left a noose on my door! # futuremittjokes Jamelle Bouie (@jbouie) August 24, 2012 

Wired: 

Everyone assumes I earned my college diploma. # futuremittjokes attackerman (@attackerman) August 24, 2012 

Gawker: 

im white im white im white im white hahahaha *points at skin* the other guy is black # futuremittjokes max read (@max_read) August 24, 2012 

Here's my personal favorite, because it sounds like the kind of joke I could really hear Romney saying: 

# futuremittjokes Birther? I don't even know her! Katie Halper (@kthalps) August 24, 2012 

So, yeah, we built that. (We can take no credit, however, for American Bridge, a liberal-connected super PAC , taking the ball and sticking one of their campaign plugs on the hashtag's search page as a sponsored tweet. Way to piggyback on a good thing, dudes.) 

Apparently, this is all outrageous! and shocking! to conservatives who, as quick as they were to condemn Rep. Todd Akin's luddite notions of female assault and reproduction earlier this week, quietly dismissed Romney's birther shoutout as a cute, banal, not-at-all-racially-coded joke. Apparently the only thing that's more offensive than racial pandering is being accused of racial pandering. Those are fighting tweets, sir! 

But, whoops, a couple folks didn't get the memo and tried to highjack the hashtag and use it to dump some anti-Obama barbs: 

@ justplainbill # FutureMittJokes I hear now that Obama's out of office he's going to be on a sitcom The Fresh Prince of Bill Ayers . Mike Barr (@maxnrgmike) August 24, 2012 



I like dogs, but I don't think I could eat a whole one. # futuremittjokes # obamajokes Corr Comm (@corrcomm) August 24, 2012 

@ aggedor_hobbit # FutureMittJokes Guy came up to me said he hadn't had a bite in 3 days, so I got him a dog costume and Obama bit him. JLThorpe (@JLThorpe) August 24, 2012 

Pretty sure that's a win, right there, the Twitchy's anonymous blogger wrote of the attempted highjacking. Hmm... Depends on what your definition of win is.
