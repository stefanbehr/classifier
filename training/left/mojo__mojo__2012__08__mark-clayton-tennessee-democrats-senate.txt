Update, 2:55 p.m.: Via the Tennessean , the Tennessee Democratic party has condemned Clayton, saying in a statement that he is associated with a known hate group (a reference to Public Advocate of the United States), and blaming his victory on the fact that his name appeared first on the ballot. 

Mark Clayton believes the federal government is building a massive, four-football-field wide superhighway from Mexico City to Toronto as part of a secret plot to establish a new North American Union that will bring an end to America as we know it. On Thursday, he became the Tennessee Democrats' nominee for US Senate. 

Clayton, an anti-gay-marriage activist and flooring installer with a penchant for fringe conspiracy theories, finished on top of a crowded primary field in the race to take on GOP Sen. Bob Corker this fall. He earned 26 percent of the vote despite raising no money and listing the wrong opponent on his campaign website . The site still reads, DEDICATED TO THE DEFEAT OF NEO-CONSERVATIVE LAMAR ALEXANDER, whom Clayton tried to challenge in 2008. (That year, he didn't earn the Democratic nomination.) 

On his issues page, Clayton sounds more like a member of the John Birch Society than a rank-and-file Democrat. He says he's against national ID cards, the North American Union, and the NAFTA superhighway, a nonexistent proposal that's become a rallying cry in the far-right fever swamps. Elsewhere, he warns of an encroaching godless new world order and suggests that Americans who speak out against government policies could some day be placed in a bone-crushing prison camp similar to the one Alexander Solzhenitsyn was sent or to one of FEMA's prison camps. (There are no FEMA prison camps.) 

In April 2008, Clayton issued a press release accusing Google of censoring his campaign website on behalf the Chinese government: 

After spending the opening three weeks of the campaign ranked between first and third place on the first page for users who type mark clayton senate into the widely used internet search engine, Google, the Clayton campaign website has utterly vanished from rankings, and is nowhere to be found in the first ten pages. 

Google is suspected to be acting in concert with the Communist Chinese government, which in past months has been extremely sensitive to global outrage at its treatment toward Tibet. As the Olympics, which are to be held in China, draw nearer, pro-humanitarian voices have increasingly exasperated and embarrassed the authoritarian Beijing regime. 

Clayton has another intriguing theory . This one involves a former governor of California: Schwarzenegger, born in Austria, wants to amend the Constitution so that he can become president and fulfill Hitler's superman scenario. 

The closest thing Clayton has to political experience is his work as vice president of a Virginia-based organization called Public Advocate of the United States . The group's mission, per its website, is to restore the country to its conservative Christian roots. Public Advocate supports a constitutional amendment banning gay marriage, opposes abortion rights, and believes the Boy Scouts are under assault from the gay agenda. The group refers to mayors Rahm Emanuel of Chicago and Tom Menino of Boston as pro-homosexual socialist dictators for stating their opposition to the fast-food chain Chick-fil-A. (It also issued a statement condemning the gay muppets . ) 

Clayton's primary victory is only the latest blow for the Tennessee Democrats in a state that's becoming redder every year. Democrats lost three congressional seats during the 2010 midterms, plus control of the governor's mansion. Corker, a rising GOP star who edged Democratic Rep. Harold Ford Jr. by just 2.7 points in 2006, is now virtually assured of another six years in Washington. The political shift is more pronounced at the local level, where Republicans have taken advantage of their new-found dominance in Nashville to advance far-right proposals like a bill to criminalize Shariah law and to ban the discussion of homosexuality from public schools. 

With a conspiracy theorist now a leader of the state Democratic party, the local GOP a bastion of evolution and climate change denialism has an opening: It can be the party of rationality. 

Tennessee Democratic Senate nominee Mark Clayton (third from left) during his 2008 campaign Clayton for Senate
