A quick look at the week that was in the world of political dark money ... the money shot 







quote of the week 



The point of having a super PAC is like having a Rolex. It's a pain to tell time with a Rolex, but it's great for looking rich and picking up girls. Similarly, our super PAC isn't here for raising money, but it gives us a very unique bragging right. 

Anthony Kao, president of the Joe Six PAC super-PAC, talking with the American Prospect . He got the idea to create his super-PAC, which has raised $0 to date, after finding an article on Reddit about a Florida man who launched 60 of them . 

attack ad of the week 



A new ad (which has yet to air on TV ) from the pro-Obama super-PAC Priorities USA Action features a familiar face. Joe Soptic, who was laid off from a Missouri steel plant after it was sold to Bain Capital, appeared in an Obama campaign ad in May that blamed Mitt Romney for closing the plant. Soptic appeared in the Priorities ad wearing the same shirt (but no glasses) and insinuates that Romney is partly responsible for his wife's death from cancer. Campaigns and super-PACs are prohibited from coordinating with each other, and both camps said they hadn't. But after news that Soptic told his story to reporters during an Obama campaign conference call in May, an Obama spokeswoman walked back a claim that the campaign was unaware of Soptic's story. 

Here are the two ads: 





stat of the week 



193 percent: How much spending by groups that don't have to disclose their donors has increased in 2012 compared with the same time in 2010, according to the Center for Responsive Politics. 



chart of the week 



A new study by the liberal think tank Demos and the US Public Interest Research Group sheds some light on the wealthy donors behind this year's tsunami of slime . Among the findings: 58 percent of all outside spending has come from just five groups, and 1,082 donors account for 94 percent of all super-PAC donations from individuals. We chartified some of the data : 



more mojo dark-money coverage 



How Secret Foreign Money Could Infiltrate US Elections : Think the United States is immune from foreigners' campaign cash? Think again. 

250 Years of Campaigns, Cash, and Corruption : From George Washington to Citizens United , a timeline of America's history of political money games. 

Dark Money's Top Target: Claire McCaskill : Will Dems save the centrist Missouri senator from Karl Rove's attack ad onslaught? Don't hold your breath. 

more must-reads 



New York Attorney General Eric Schneiderman ramps up his investigation into shadowy 501(c) groups. New York Times 

Republican senators urge the IRS to ignore political pressure for sudden changes to well-established law when it considers revising dark-money rules. Senate Finance Committee 

Senate candidate Heidi Heitcamp (D-N.D.) gets Karl Rove's Crossroads GPS to pull a false attack ad against her. Talking Points Memo 

Kansas Senate President Steve Morris, a Republican who lost his primary Tuesday, blames Koch brother-affiliated outside groups from moderates' defeat in state races. Huffington Post 

Print Email Tweet Andrew McCarthy s Defense of McCarthyism The Top Six Classes at Newt University Gavin Aronsen 

Writing Fellow 

Gavin Aronsen is a writing fellow at Mother Jones . For more of his stories, click here . Follow him on Twitter or send an email to garonsen [at] motherjones [dot] com. RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Dark Money s Top Target: Claire McCaskill 

Will Dems save the centrist Missouri senator from Karl Rove's attack ad onslaught? Don't hold your breath. Dems: Dark Money Groups Use Secret Money to Subvert the Democratic Process 

A new complaint targets political nonprofits that attack Democrats and hide their donors. Report: David Koch Will Be a Romney Delegate 

Koch has also hosted multiple fundraisers for Romney in the Hamptons. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
