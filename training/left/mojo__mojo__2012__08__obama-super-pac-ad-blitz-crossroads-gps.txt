Priorities USA Action , the super-PAC backstopping President Obama's presidential campaign, isn't about to let the GOP money machine steamroll the president this fall. To bolster Obama, Priorities is ramping its anti-Romney campaign by buying up $30 million in broadcast and cable TV time in six crucial battleground states Colorado, Iowa, Florida, Ohio, Pennsylvania, and Virginia, the Washington Post reports . 

This ad buy is arguably Priorities' most crucial of the 2012 cycle. Obama's road to victory in November runs through these six battleground states. And the GOP's biggest outside groups, including the pro-Romney super-PAC Restore Our Future and dark-money groups Americans for Prosperity and Crossroads GPS, are pumping tens of millions in anti-Obama ads in these states. Obama will need all the reinforcements he can get that's where Priorities comes into play. 

Here's more on the buy from the Post : 

[Ad buy] sources would not indicate whether this was the totality of the ad spending Priorities USA Action would make on the election or whether this was the first flight of a broader buy. The group, in coordination with the Service Employees International Union, is currently funding Spanish-language ads in Colorado, Florida and Nevada an effort they say will continue. 

Priorities USA Action, which is run by former White House aides Bill Burton and Sean Sweeney, has raised $16 million this year as of the end of June and $21 million since the start of 2011. It recently received a $1 million donation from actor Morgan Freeman. The group says it has another $20 million in commitments. 

Those fundraising figures are dwarfed by the activity of Republican super-PACs and other conservative-aligned outside groups led by American Crossroads and Crossroads GPS, which has pledged to spend $300 million on the 2012 election. 

Priorities' fight against GOP groups is an uphill battle. In the super-PAC cash race, GOP-aligned super-PACs have raised nearly $219 million compared to $77 million for Democratic-aligned super-PACs, according to the Sunlight Foundation . For some perspective, the largest GOP super-PAC, the pro-Romney group Restore Our Future, has raised more money than every Democratic super-PAC combined .
