You may have seen this photo of Republican presidential candidate Mitt Romney from earlier this month. Shot by Wheeling Intelligencer photographer Scott McCloskey, it shows Romney appearing at a Beallsville, Ohio rally flanked by a large group of coal miners: 



Associated Press /Wheeling Intelligencer 

The rally was meant to show that Coal Country Stands With Mitt, as the campaign signs touted. But it turns out that the owners of the mine told workers they were required to attend the rally, reports The Plain Dealer (via Grist ): The Pepper Pike company that owns the Century Mine told workers that attending the Aug. 14 Romney event would be both mandatory and unpaid, a top company official said Monday morning in a West Virginia radio interview. A group of employees who feared they'd be fired if they didn't attend the campaign rally in Beallsville, Ohio, complained about it to WWVA radio station talk show host David Blomquist. Blomquist discussed their beefs on the air Monday with Murray Energy Chief Financial Officer Rob Moore. Moore told Blomquist that managers communicated to our workforce that the attendance at the Romney event was mandatory, but no one was forced to attend. He said the company did not penalize no-shows. 

The radio interview is here . Moore also confirms that they bused workers to the rally, and that the mine was shut down for the day (probably because so many workers would be at this mandatory rally). So even if workers wanted to, you know, work, they'd be forced to take a day off without pay anyway. 

Murray Energy CEO Robert Murray is a major Romney supporter , and the company's PAC has given $10,000 to the Republican presidential candidate. This, according to Murray, is because he worries about his workers whose lives have been destroyed by the policies of Barack Obama.
