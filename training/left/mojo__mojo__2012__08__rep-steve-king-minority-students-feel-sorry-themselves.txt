Rep. Steve King (R-Iowa) came this close to following in his Missouri colleague Todd Akin's footsteps on Tuesday, when he told a local news reporter that he didn't know any women who had become pregnant from statutory rape or incest. (King's spokesman clarified that the congressman meant that he didn't personally know women who had gotten pregnant from rape, but understood, contra Akin, that it is biologically possible.) 

But that doesn't mean he's off the hook. Facing his toughest-ever re-election fight against Democrat Christie Vilsack, King has only doubled down on the nativist rhetoric that's been his bread-and-butter in Washington. On Tuesday, trackers from the super-PAC run by the progressive phone service provider CREDO grabbed footage of King at a town hall meeting in Le Mars lamenting that minority students are falling for a communist victimization narrative promoted by student organizations. 

King, who recently sponsored a bill to make English the national language, launched into an extended rant on the perils of multiculturalism which was only reinforced by a visit to Iowa State University, where he says he encountered 59 different student groups rooted in the idea. Merlin's pants! As he put it, It started with Asians and it ended with Zeitgeist. So from A to Z. And most of them were victims groups, victimology, people that feel sorry for themselves. And they're out there recruiting our young people to be part of the group that feels sorry for themselves. 

Watch: 



Obscure Italian communist*: check. Karl Marx: check. Saul Alinsky: check check check. King has for years faced only token opposition in his rural western Iowa district, but in 2012 he faces a Democrat with serious upset hopes in Vilsack, the wife of the former governor-turned-Agriculture-secretary. King, who has previously flirted with birtherism and lamented America's emphasis on diversity, is banking that come November, his nativist ravings will be an asset, not an albatross. 

* Update: By obscure I just mean that Antonio Gramsci is not as well known among conservatives as Marx and Alinsky, who need no introduction.
