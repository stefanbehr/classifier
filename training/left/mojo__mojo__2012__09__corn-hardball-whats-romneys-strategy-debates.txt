David Corn and Bob Shrum joined Chris Matthews on MSNBC's Hardball to discuss the upcoming presidential debates and the strategies we'll see from the Romney and Obama campaigns. 



David Corn is Mother Jones' Washington bureau chief. For more of his stories, click here . He's also on Twitter .
