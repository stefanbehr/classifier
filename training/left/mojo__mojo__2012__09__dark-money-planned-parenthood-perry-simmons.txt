A quick look at the week that was in the world of political dark money ... the money shot 





quote of the week 



If [Republicans] win in November, we won't recognize the America they'll create. 

A fundraising plea from the Democratic Senatorial Campaign Committee, which previously warned big donors to wake up and start giving to super-PACs, decrying the hundreds of millions in Citizens United corporate dollars flooding into the 2012 election. 

attack ad of the week 



Planned Parenthood's super-PAC has spent $1.8 million on a new ad hitting back at Mitt Romney over his hostility toward the group. The spot, titled Mitt Romney Would Turn Back the Clock on Women's Health, will run in Ohio and Virginia, according to the group. (Romney has said that as president he would slash Planned Parenthood's federal funding , but the ad takes a clip of him out of context to suggest he would get rid of the organization altogether.) 



Chart of the Week 



It's too big to cram onto this page, but head over to the Texas Tribune for a great visualization of the Lone Star State's deep-pocketed donors funding some of the country's biggest super-PACs. Topping the chart: billionare businessman (and Dallas' most evil genius ) Harold Simmons, whose favorite super-PAC (to the tune of $11 million) is Karl Rove's American Crossroads, and Houston homebuilder Bob Perry, who's given $8.75 million to the pro-Romney Restore Our Future, among other groups. 

stat of the weeK 



$570,000: The minimum amount raised by the Coalition of Americans for Political Equality, a super-PAC run by a former Arizona GOP county chair that put up a series of websites disguised as candidate homepages in an apparent effort to trick prospective donors. After the National Journal reported on the websites last Sunday the super-PAC's front sites briefly disappeared , but the group chalked it up to a GoDaddy outage . NJ snapped some screenshots just in case (note the disclaimer in the top-right corner): 



more must-reads 



The dark-money group attacking Sen. Sherrod Brown (D-Ohio), revealed. ProPublica 

Changing corporations, not the Constitution, is the key to a fairer post-Citizens United world. Democracy Journal 

Milllions of dollars from outside spending groups have flooded into the presidential race, but House candidates may have more reason to fear the groups. Center for Public Integrity 

Mitt Romney still hasn't disclosed all of his, but the New York Times has a list of President Obama's biggest bundlers. 

Print Email Tweet Corn on MSNBC: Why Won t Mitt Apologize for Politicizing the Libya Attack? Sarah Palin and Free Stuff Vs. Freedom Gavin Aronsen 

Writing Fellow 

Gavin Aronsen is a writing fellow at Mother Jones . For more of his stories, click here . Follow him on Twitter or send an email to garonsen [at] motherjones [dot] com. RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Karl Rove s Role in Komen s Planned Parenthood Flap 

Abortion foes worry Rove isn't pro-life enough. Black Gold for the GOP 

Trevor Rees-Jones made his name as a Dallas fracking pioneer. So what's he doing bankrolling political attack ads halfway across the country? Allen West s Challenger: Sometimes I Have No Idea What He s Talking About 

Inside the hectic life of a top congressional recruit in Charlotte. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
