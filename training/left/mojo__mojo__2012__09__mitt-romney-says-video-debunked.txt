This is getting ridiculous. 

After Mother Jones posted video of Mitt Romney sharing remarks with millionaire donors that he would never express to voters noting that nearly half of the American electorate are moochers and that Romney doesn't believe a two-state solution to the Israeli-Palestinian conflict is feasible Romney did not deny he said what he said. As the clich goes, he doubled down , saying his remarks were inelegant but a reflection of his views about the rapid growth of entitlement programs in the United States. (Actually, this was a bait-and-switch operation. Romney was not talking policy when he disdainfully described half of the citizenry as parasites and victims.) 

On Wednesday afternoon, he went further, with his campaign claiming that the video had been debunked. In lashing out at the Obama campaign, Romney's crew issued this email: 

Today, The Obama Campaign Leveled False Attacks Against Mitt Romney Based On A Debunked And Selectively Edited Video: 

Today, Obama Campaign Spokesperson Ben LaBolt Attacked Mitt Romney Based On A Debunked Mother Jones Tape. OBAMA CAMPAIGN SPOKESMAN BEN LABOLT: You heard on the tapes released this week that it's Mitt Romney who would walk away from the peace process. (MSNBC, 9/19/12) 

But This Morning, Politico Reported That The Mother Jones Video Was Selectively Edited To Give A False Impression About Mitt Romney s Views On The Middle East Peace Process. But the clip initially provided by Mother Jones does not include that part of his remarks, and therefore was not reported by the aforementioned news outlets. Romney's complete remarks about the Mideast peace process were included in the complete video Mother Jones published Tuesday afternoon, less than 24 hours after it released clips from the fundraiser. But the clip posted to the Mother Jones website, which was cited by the national media, cuts out the excerpt in which Romney says that 'American strength, American resolve' will cause the Palestinians to 'some day reach the point where they want peace more than we're trying to force peace on them.' (Dylan Byers, Technically, Romney Said Peace Was Possible, Politico , 9/19/12) 

The Romney campaign was clearly implying the whole video was rubbish. But there's a slight problem. Politico 's Dylan Byers, the source for the debunking charge, quickly noted that he had done no such thing. He wrote : 

there is nothing in my report that debunks the video. 

In his article, posted earlier in the day, Byers had noted how some folks were complaining that we had edited a long clip of Romney talking about the Middle East selectively. In that clip watch it here Romney trashed the two-state solution to the Israeli-Palestinian crisis, said the Palestinians (whom he lumped into one mindset) did not want peace and only sought the destruction of Israel, that he would not actively pursue the peace process and would instead seek to kick the ball down the field, and that he had paid no real attention when a former secretary of state had told him that peace might be possible in the Middle East. 

That is a total diss of the peace process and would represent a radical break with US policy, which has supported a two-state solution since the Clinton years. 

Yet Romney went on to say and this clip did not include this that if the United States showed resolve .the Palestinians will some day reach the point where they want peace more than we're trying to force peace on them. Thus, peace might be theoretically possible at some point in the distance. 

This was not a case of selective editing. The point was to show what was newsworthy: Romney breaking with current policy and stating views that he has not stated publicly. (In an interview this summer, he said he supported a two-state solution.) Nevertheless, some Romney backers have cried foul and managed to turn this into a dispute they can use to raise questions about the secret Romney tape. 

But don't take my word. Here's more from Byers: 

More mysterious still, is why the Romney campaign wants to debunk a video containing remarks that the candidate doubled-down on in a follow-up press conference. 

Slate 's Dave Weigel has weighed in as well : 

By calling the whole tape debunked and selectively edited, the campaign's hewing closer to the Breitbart.com argument -- the real story is liberal media-Obama collusion. And the result is a sort of paradox, in which Romney stands by what he said in a video that you can't trust. 

It was bizarre. After Byers and Weigel had debunked the Romney camp's debunking, Byers heard from a Romney aide who said that the campaign only takes issue with the clip regarding Romney's view on the Mideast, not the entire video. 

In other words, the Romney campaign walked back the push-back. It's not challenging the 47 percent material or anything else; only the Mideast remarks. But, as I've said a few thousand times on television these past few days, the wonderful thing about this story is that people can view for themselves. Watch Romney talking about the Mideast, and it's clear he has contempt for the peace process as it has been conceived for years; does not believe it can work; and would chart a radically different course. The few sentences not included in that clip but which were included in the full transcript and complete tape we released do not a debunking make. This maneuver smacks of desperation from a campaign hurt by the undeniable words of its candidate.
