A quick look at the week that was in the world of political dark money ... the money shot 





quote of the week 



I mean, if somebody here has a $10 million check I can't solicit it from you, but feel free to use it wisely. 

Barack Obama, speaking to guests about donating to outside spending groups at a fundraiser hosted by Beyonce and Jay-Z. As the Huffington Post reported , the remark was seemingly in jest but also the latest example of how closely campaigns have flirted with the ban on coordinating their activities with outside groups. That rule is hardly ever enforced, though, and as Campaign Legal Center senior counsel Paul S. Ryan told HuffPo , Obama's comment was vague enough to not qualify as a direct request for contributions. 

attack ad of the week 



This week, Mother Jones made waves with the release of a secretly recorded video of Mitt Romney making a pitch to wealthy donors in May at the Boca Raton home of private equity manager Marc Leder. Pro-Obama super-PAC Priorities USA Action has already pounced on one of Romney's most controversial statements in the video, in which he dismissed 47 percent of the electorate as entitled, government-dependent victims who will vote for Obama no matter what. After showing an image of a wealthy home and Romney's comments, the Priorities ad cuts away to a modest house as a narrator replies, Behind these doors, middle-class families struggle, and Romney will make things even tougher. 



stat of the week 



More than 70: The number of dark-money 501(c)(4) groups, ostensibly operating as tax-exempt social welfare organizations, that the Internal Revenue Service is investigating . The groups can't legally make political activities the majority of what they do, although many make little effort to conceal their political spending, and the IRS has had its eye on groups like Karl Rove's Crossroads GPS for several months now. Even so, the agency hasn't stripped a single group of its 501(c)(4) status in the past six months. 

chart of the week 



The Daily Beast and Center for Responsive Politics dug through tax filings to do their best job to piece together the interconnected world of dark-money 501(c)(4)s. The chart doesn't paint a complete picture, since numbers from the 2012 election won't all be released until at least mid-next year, but it does list known grants made by the groups and to whom they were given. (Obscuring sunlight further, a federal appeals court overturned a lower court's decision , in Van Hollen v. FEC , to require tax-exempt groups to reveal their donors.) 



more mojo dark-money coverage 



Romney Funder's Israeli Newspaper Buries Video Controversy : Sheldon Adelson spends millions on ads for Mitt, but tries to downplay the GOP candidate's gaffes. 

Crossroads, US Chamber, and Other Dark Money Groups Notch Big Court Win : A federal appeals court has overturned an earlier ruling demanding that nonprofits unmask their donors for certain ads. 

Liberal Super-PAC Targets Koch Brothers With Attack Ads in Wisconsin and Iowa : Patriot Majority takes the fight to the Kochs in the state that, for liberals, made them infamous. 

Who Was at Romney's 47 Percent Fundraiser? : Some possible guests at the $50,000-a-plate Florida event where the candidate cast Obama voters as moochers. 

SECRET VIDEO: Romney Tells Millionaire Donors What He REALLY Thinks of Obama Voters : When he doesn't know a camera's rolling, the GOP candidate shows his disdain for half of America. 

more must-reads 



Sen. Bryan Dorgan (D-N.D.) joins the campaign finance reform cause. Center for Public Integrity 

Outside spending groups have accounted for close to half of all political ad spending, according to one analysis. MSNBC 

More charts from the Daily Beast and Center for Responsive Politics . 

Print Email Tweet Corn on MSNBC: Romney s Kitchen Sink Election At First Debate, Scott Brown Gets Nasty Gavin Aronsen 

Writing Fellow 

Gavin Aronsen is a writing fellow at Mother Jones . For more of his stories, click here . Follow him on Twitter or send an email to garonsen [at] motherjones [dot] com. RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

Comedy Central Reveals Obama s True Feelings About Romney s 47 Percent Video 

Key & Peele give us Obama's inner reaction to the Romney videos. Corn on DemocracyNow!: The Backstory to Our Romney Fundraiser Video 

David Corn reveals how he and Jimmy Carter's grandson got in touch with the source. 7 Highlights You Missed From the Romney Video 

The GOP candidate mocked immigration, made a false claim about the Fed, predicted an economic rebound "without actually doing anything," and more. How Nonprofits Spend Millions on Elections and Call It Public Welfare 

It's spending by nonprofits, not super-PACs, that may sway this election. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
