Twelve months after they slept, ate, and occasionally got arrested with the demonstrators, our team of journalists has returned to Lower Manhattan to follow #s17 protesters observing the birthday of Occupy Wall Street. Below is our Storify of MJ street reporting, plus updates from our friends and colleagues across the internets (please be patient: The Storify may take a few seconds to load): 

[ View the story "Tracking the #OWS Anniversary Protests" on Storify ] 

Do you appreciate fair and factual reporting on Occupy Wall Street? Please donate a few bucks to help us expand our coverage .
