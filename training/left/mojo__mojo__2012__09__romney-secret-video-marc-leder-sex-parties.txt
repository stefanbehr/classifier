When Mitt Romney at a private fundraiser dismissed all Barack Obama voters as moochers and victims showing disdain for nearly half of the American electorate he was speaking at the home of controversial private equity manager Marc Leder in Boca Raton on May 17, 2012. (It was Romney's second fundraising event in Boca that day .) This is evident from references made by Romney within the full video recording of the event that has been reviewed by Mother Jones . 

When Mother Jones first disclosed secret video of Romney's remarks, we were obliged to not reveal details regarding the time and place of the event. That restriction has been lifted, as the story has garnered attention throughout the media. 

More MoJo coverage of Mitt Romney : Full Transcript of the Mitt Romney Secret Video Part One: Romney Tells Millionaire Donors What He REALLY Thinks of Obama Voters 

Part Two: On Israel, Romney Trashes Two-State Solution 

WATCH: Full Secret Video of Private Romney Fundraiser 

Who Was at Romney's 47 Percent Fundraiser? Romney Reacts to 47 Percent Video, Stands By Remarks 

6 Things Mitt Romney Is Hiding 

At the fundraiser, Romney was asked how he could win in November, and he replied: There are 47 percent of the people who will vote for the president no matter what. All right, there are 47 percent who are with him, who are dependent upon government, who believe that they are victims, who believe the government has a responsibility to care for them, who believe that they are entitled to health care, to food, to housing, to you-name-it. That that's an entitlement. And the government should give it to them. And they will vote for this president no matter what These are people who pay no income tax [M]y job is is not to worry about those people. I'll never convince them they should take personal responsibility and care for their lives. 



Romney made those remarks before donors who had paid $50,000 a plate to attend the dinner at Leder's swanky house . 

Leder has long been a fan of Romney. In January, the New York Times reported : Years ago, a visit to Mr. Romney's investment firm inspired Mr. Leder to get into private equity in the first place. Mr. Romney was an early investor in some of the deals done by Mr. Leder's investment company, Sun Capital, which today oversees about $8 billion in equity. 

The paper noted that Leder is something of a poster boy for private equity and not in a good way: Mr. Leder personifies the debates now swirling around this lucrative corner of finance. To his critics, he represents everything that's wrong with this setup. In recent years, a large number of the companies that Sun Capital has acquired have run into serious trouble, eliminated jobs or both. Since 2008, some 25 of its companies roughly one of every five it owns have filed for bankruptcy. Among the losers was Friendly's, the restaurant chain known for its Jim Dandy sundaes and Fribble shakes. (Sun Capital was accused by a federal agency of pushing Friendly's into bankruptcy last year to avoid paying pensions to the chain's employees; Sun disputes that contention.) Another company that sank into bankruptcy was Real Mex, owner of the Chevy's restaurant chain. In that case, Mr. Leder lost money for his investors not once, but twice. 

But Leder does differ from Romney in one significant fashion: how he likes to have a certain sort of fun. In August 2011, the New York Post reported , It was as if the Playboy Mansion met the East EBond at a wild party at private-equity titan Marc Leder's Bridgehampton estate, where guests cavorted nude in the pool and performed sex acts, scantily dressed Russians danced on platforms and men twirled lit torches to a booming techno beat. The divorced Sun Capital Partners honcho rented a sprawling beachfront mansion on Surf Side Road for $500,000 for the month of July. Leder's weekly Friday and Saturday night parties have become the talk of the Hamptons and he ended them in style last weekend with his wildest bash yet. Russell Simmons and ex-wife Kimora Lee attended a more subdued party thrown by Leder who's an event chair for Simmons' Art For Life charity on July 29 together. But the revelry hit a frenzied point the next day before midnight when a male guest described as a chubby white meathead and a tanned female guest stripped and hopped into the pool naked. 

At Romney's fundraiser at Leder's Boca Raton home, not a single sex act was recorded. 

Print Email Tweet Romney Reacts to 47 Percent Video, Stands By Remarks Corn on MSNBC: Romney Caught on Tape David Corn 

Washington Bureau Chief 

David Corn is Mother Jones ' Washington bureau chief. For more of his stories, click here . He's also on Twitter and Facebook . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

SECRET VIDEO: Romney Tells Millionaire Donors What He REALLY Thinks of Obama Voters 

When he doesn't know a camera's rolling, the GOP candidate shows his disdain for half of America. Romney Reacts to 47 Percent Video, Stands By Remarks 

Citing our exclusive video, campaign manager Jim Messina accuses the GOP presidential nominee of throwing Americans under the bus. Exclusive: The Koch Brothers Million-Dollar Donor Club 

The anonymous patrons who have given $1 million or more to the Koch dark-money machine. Romney Invested in Medical-Waste Firm That Disposed of Aborted Fetuses, Government Documents Show 

And these documents challenge Romney's claim that he left Bain Capital in early 1999. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
