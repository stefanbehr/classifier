Crossroads GPS , the dark-money nonprofit group co-founded by Karl Rove , is running robocalls in Massachusetts attacking Democratic Senate candidate and progressive favorite Elizabeth Warren . The robocalls, which Massachusetts residents reported hearing on Wednesday, hit Warren for her support of President Obama's health-care reform law and for her work as the head of a bailout watchdog in Washington. 

The robocalls appear to be Crossroads GPS' first serious foray into the Massachusetts Senate race, pitting Warren against Republican Sen. Scott Brown , since the two candidates agreed in January to discourage outside political spending in their race. Because it operates as a 501(c)(4) nonprofit, Crossroads GPS does not disclose its donors. Crossroads is one of the biggest spenders of all the independent groups pouring money into the 2012 elections. 

Here's the full audio of one Crossroads robocall, hitting Warren for her support of Obamacare, provided by the Massachusetts Democratic Party: 



TRANSCRIPT: Today, you can change your future by voting against Elizabeth Warren. A vote for Warren is a vote for the same type of government failures that got us into the situation we are currently in. Warren supports President Obama's health-care takeover that will cut over $700 billion from Medicare spending. The health-care law backed by Warren could limit the availability of care seniors depend on from the Medicare program they paid for. Vote no on Elizabeth Warren for Senate this November. Paid for by Crossroads GPS. 

To be clear, President Obama's health-care law is not a takeover four different news organizations have debunked that claim, and PolitiFact named this its 2010 Lie of the Year. As for Obamacare cutting $700 billion in Medicare spending, PolitiFact rated that Mostly False. 

Another Crossroads robocall criticizes Warren for mismanaging the funds provided to the bailout watchdog panel she ran from December 2008 to September 2010, according to an independent Massachusetts voter who received one such call. The call also highlights Warren's pay as the head of that watchdog, known as the Congressional Oversight Panel, seeming to portray it as exorbitant for the work she did, the voter says. Warren received nearly $193,000 for work on the panel during that nearly two-year period. 

Crossroads' robocalls technically do not violate Brown and Warren's pact to dissuade spending by outside groups super-PACs, nonprofits, and so on spend money to influence their race. That agreement applied only to TV, radio, and online advertisements. (Crossroads spokesman Jonathan Collegio did not respond to requests for comment.) 

Althea Harney, a spokeswoman for Warren's campaign, said in a statement that Crossroads' robocalls were evidence that the Massachusetts Senate race is about whose side you stand on and we all can see who stands with Scott Brown. Karl Rove, Grover Norquist, and national Republicans are wading into this race because they want Republican control of the Senate. 

Cracks are showing in the Brown-Warren truce. As Politico recently reported , Grover Norquist's anti-tax organization Americans for Tax Reform is dropping $215,000 on mailers in the Massachusetts Senate race. The AFL-CIO, meanwhile, has sent out fliers hitting Brown for putting party before people, and the League of Conservation Voters has also spent six figures bashing Brown. 

Crossroads GPS' latest foray into the race with ads that explicitly call for voting against Warren, no less puts another dent in the Brown-Warren agreement. That truce, to the surprise of many politicos, has held firm for much of 2012, exempting Massachusetts voters from the negative political ads flooding states such as Ohio, Virginia, and Missouri. 

That Rove's group would come to Brown's defense is not at all surprising. Rove told big-time donors in August that Crossroads GPS and its sister super-PAC, American Crossroads, intended to spend $70 million on 2012 Senate races. At that same donor meeting, Rove also screened a Crossroads ad targeting the Massachusetts Senate race. (The ad was one of two, the Boston Globe reported , that ran on TV in Massachusetts last winter, but has not run since the Brown and Warren agreed to their truce.) Brown was also spotted chatting with Rove at a Tampa restaurant during the Republican National Convention. 

Print Email Tweet Romney Advisers: Bring Torture Back Obama Campaign Responds to Romney s Harvest Comments Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

WATCH: Scott Brown Staffers Tomahawk Chop at Warren Volunteers 

The Massachusetts Senate race gets real nasty, real fast. Elizabeth Warren Is Part Native American 

But that's not really the story. Elizabeth Warren Sets Her Sights on Scott Brown 

The Harvard professor became a populist hero by taking on Wall Street. Now she's aiming for the GOP incumbent's Senate seat. Scott Brown Latest GOPer to Turn on Mitt s 47 Percent Line 

The kicker: They share a top adviser. Crossroads, US Chamber, and Other Dark Money Groups Notch Big Court Win 

A federal appeals court has overturned an earlier ruling demanding that nonprofits unmask their donors for certain ads. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
