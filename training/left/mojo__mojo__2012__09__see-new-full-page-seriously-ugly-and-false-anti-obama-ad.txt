Tampa Bay Times political writer Adam Smith has flagged this full-page ad from Sunday's Sarasota Herald-Tribune a reliably balanced reporter, he calls it a seriously ugly and false anti-Obama ad : 

According to Smith, the ad has also run this month in tiny dailies all over Florida, Iowa, Ohio, and Pennsylvania. It was created by the Government Is Not God Political Action Committee , the 18-year-old soft-money vehicle of religious conservative crusader William Murray (who is the son of famed atheist activist Madalyn Murray O'Hair, as well as a big supporter of Todd Akin ). It's hard to say exactly what electoral impact the GINGPAC ad will have, especially since Murray himself came out in the primaries as a Santorum-loving Romney-resister: 



By the way, Murray who lists his favorite books as the King James Bible and the Annapolis Book of Seamanship is not a Texan; he hails from Maryland and the District of Columbia . This is what he sounds like when not wearing a cowboy hat and pandering to Lone Star staters:
