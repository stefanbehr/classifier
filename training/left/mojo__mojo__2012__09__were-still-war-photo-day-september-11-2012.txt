1st Lt. Michael Moore, platoon commander for 1st Platoon, Bravo Company, Battalion Landing Team, 1st Battalion, 2nd Marine Regiment, 24th Marine Expeditionary Unit , crosses paths with Djiboutian wildlife as he walks back to camp after taking part in assault climber training with his Marines in Djibouti, Aug. 29, 2012. 

U.S. Marine Corps photo by Cpl. Michael Petersheim.
