About halfway through Wednesday's debate, Romney took a hit at host Jim Lehrer's network PBS, and one of its most well-known stars: Big Bird. Talking about federal subsidies he would eliminate in order to reign in the deficit, he said this : 

I'm sorry, Jim, I'm going to stop the subsidy to PBS. I'm going to stop other things. I like PBS, I love Big Bird. Actually I like you, too. But I'm not going to -- I'm not going to keep on spending money on things to borrow money from China to pay for [it]. 

The Twittersphere exploded with a Big Bird hashtag (#savebigbird) and Twitter handles, and the character's name earned 17,000 tweets per minute, according to The Hill. Incidentally, the Center for Public Broadcasting, which funds PBS, made up .00014 percent of the federal budget last year. But whatever. 

Here are a few of the Big Birds that were born. 

I guess I should have seen it coming; I am part of the 47% after all. #savebigbird Big Bird (@BigBirdLives) October 4, 2012 

Obama killed Osama Bin Laden.Romney wants to kill Big Bird. I think that says enough. #FactCheck . Big Bird (@BigBirdRomney) October 4, 2012 

Herman Cain / Big Bird 2016 Big Bird (@FiredBigBird) October 4, 2012 

If Romney had grown up with me, he wouldn't have his numbers all screwed up. :( #denverdebate #debates #PBS #SupportBigBird Big Bird (@BlGBlRD) October 4, 2012 

Aw. Sad face. 

It even inspired avatar solidarity: 

Now being RT'd by people who have changed their avatars to Big Bird. This is the new go green for Iran. Tim Murphy (@timothypmurphy) October 4, 2012
