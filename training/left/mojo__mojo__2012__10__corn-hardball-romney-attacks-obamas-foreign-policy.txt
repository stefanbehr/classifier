Mother Jones Washington bureau chief David Corn and The Atlantic 's Steve Clemons joined Chris Matthews on MSNBC's Hardball to discuss Mitt Romney's Monday foreign policy address, where he aggressively accused the president of leading from behind in foreign affairs. 

David Corn is Mother Jones' Washington bureau chief. For more of his stories, click here . He's also on Twitter .
