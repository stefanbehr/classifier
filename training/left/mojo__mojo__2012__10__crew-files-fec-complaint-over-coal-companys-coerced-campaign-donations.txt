An ethics watchdog group wants to know whether an Ohio coal company violated federal election rules by forcing employees to donate to Mitt Romney's presidential campaign. Citizens for Responsibility and Ethics in Washington sent a complaint to the Federal Election Commission on Tuesday alleging that Murray Energy and its CEO Robert Murray coerced company employees to make contributions to its political action committee (PAC). 

The company's PAC regularly donates hundreds of thousands of dollars to Republican politicians. The CREW complaint, based on reporting in The New Republic , alleges that the company threatened employees with financial reprisals, including the loss of their jobs if they did not contribute to the PAC. 

The TNR piece published last week cites interviews with Murray employees as well as internal memos that suggest employees were pressured to donate. An internal memo from Robert Murray also complains about salaried employees not attending political functions for whichever coal friend in Congress happens to be visiting, and includes a list of specific staffers who had not attended the CEO's fundraisers. Another letter from September 2010 is very direct in its threat for staffers who don't give to the PAC: We have only a little over a month left to go in this election fight. If we do not win it, the coal industry will be eliminated and so will your job, if you want to remain in this industry. Salaried employees were expected to give 1 percent of their salary to the PAC, TNR 's Alec MacGillis reported, and would receive letters telling them which candidates to support. 



In August, Murray Energy forced miners in Ohio to take a day off work without pay to attend a Mitt Romney rally. Romney then used the miners as props in television ads complaining about President Obama's war on coal. 



CREW Executive Director Melanie Sloan says that Murray's tactics are outrageous and claims they are a violation of campaign finance rules . Whether coercing company executives to make campaign contributions or insisting coal miners take time off without pay so that a candidate can stage pretty pictures it is all illegal, said Sloan. 

In the TNR piece, Murray's lawyers insist they aren't breaking any rules: Murray Energy general counsel Mike McKown says the firm s approach to political giving complies with federal laws. Employees are not required to give to the PAC, he says, nor are they reimbursed. We follow carefully the FEC [Federal Election Commission] rules about what employees can be solicited and how they can be solicited, he says, adding that Bob Murray s encouragement for employees to contribute to individual candidates is the CEO s personal endeavor. The PAC and Mr. Murray s fundraising are kept separate, he says.
