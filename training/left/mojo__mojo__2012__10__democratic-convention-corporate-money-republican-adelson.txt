In the lead-up to their party's convention in Charlotte last month , top Democrats vowed to fund their every-four-years gala solely with individual donations. We will make this the first convention in history that does not accept any funds from lobbyists, corporations, or political action committees, Rep. Debbie Wasserman Schultz (D-Fla.), the DNC chairwoman, said last year. That pledge, it turns out, was a whole lot of bluster. 

New election filings show that the Democratic National Convention benefited from millions in corporate cash. The convention took in at least $5 million in corporate money to rent the Time Warner Cable basketball arena that served as the convention hall. Convention officials also tapped almost $8 million from a line of credit from Duke Energy, a Charlotte-based electricity company with close ties to the Obama administration . The DNC banked a million more in in-kind contributions from corporations such as AT T, Bank of America, Coca-Cola, Microsoft, and Costco. Labor unions gave nearly $6 million to the convention as well, according to the Center for Responsive Politics . 

A separate fund created to support the convention, New American City, also brought in corporate money. Bank of America gave $5 million, Duke Energy contributed $4.1 million, and AT T chipped in $1 million. 

DNC spokeswoman Melanie Roussell told the AP that convention officials broke no laws in raising corporate money. Any fundraising restrictions, Roussell said, were self-imposed by conventional officials. In all, Democrats' official convention fund raised $24 million to supplement the $18 million provided by taxpayers. New American City raised $19 million. 

The GOP had no qualms raising corporate cash , and it ultimately hauled in nearly $56 million for its convention held in Tampa in late August. The RNC's corporate donors included Archer Daniels Midland, Bacardi, Blue Cross Blue Shield, Chevron, Citigroup, Comcast, CSX Corporation, Duke Energy, Ernst Young, FedEx, Ford, General Electric, Google, JPMorgan Chase, Lockheed Martin, Walmart, Wells Fargo, and Xerox Corporation. GOP mega-donor Sheldon Adelson chipped in $5 million, while industrialist David Koch and investor John Paulson both gave $1 million. 

Print Email Tweet Romney Military Adviser Wanted Separate Housing for Gay Troops Romney Enlists General Behind Iraq Debacle as Key Military Adviser Andy Kroll 

Reporter 

Andy Kroll is a reporter at Mother Jones . For more of his stories, click here . Email him with tips and insights at akroll (at) motherjones (dot) com. Follow him on Twitter here . RSS | Twitter 

Advertise on MotherJones.com If You Liked This, You Might Also Like... 

A Final Look at the Convention Bounce: It s All Obama 

A Tale of Two Conventions 

The nation is deeply divided, but the gatherings in Charlotte and Tampa show how starkly dissimilar the Democratic and Republican visions of the American experience are. At Democratic Convention, Unions Battle Thug Image 

Labor tries to make the best of a dismal situation in "right-to-work" Charlotte this week. WATCH: Clint Eastwood s Bizarre Empty Chair Obama Speech at the GOP Convention 

Things got very weird. Grab some popcorn and see for yourself. (Also, read the full transcript.) Will Obama Take Advantage of Romney s Big Convention Mistake? 

The president's advisers say Mitt Romney's substance-free speech in Tampa gave Obama an opportunity to exploit in Charlotte. Get Mother Jones by Email - Free. Like what you're reading? Get the best of MoJo three times a week.
