Anti-abortion state lawmakers have tried all manner of new laws to limit access to abortion in the past few years: mandatory ultrasounds , 72 hour waiting periods , scare-mongering mandatory scripts , and strict building codes , just to name a few. 

But Robin Marty at RH Reality Check flags another potential new tactic : mandatory spousal notification. The National Pro-Life Alliance (NPLA) sent a questionnaire to candidates for the Kansas legislature, which Huffington Post obtained . Included in the 11 questions, most of which have become fairly typical anti-abortion fare, is this: Will you support legislation giving spouses the right to be notified and intervene before any abortion is performed on the couple's baby? 

As RH Reality Check 's Marty notes, NPLA's 2008 presidential questionnaire also included that question. Republican candidates Tom Tancredo, Mike Huckabee and now-governor of Kansas Sam Brownback said yes to all the questions, including that one. Between that and this latest survey, it wouldn't be too surprising if something like this popped up in the Kansas legislature sometime soon. 

Of course, if a woman hasn't already discussed her desire to end a pregnancy with her husband, it's probably for a good reason. And last time I checked, her spouse isn't the one who has to be pregnant and give birth.
