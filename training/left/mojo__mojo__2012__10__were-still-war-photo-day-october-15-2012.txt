Lance Cpl. Gabriel L. Parks, ammo man, and Lance Cpl. Kevin D. Cassara, gunner, both with Machine Gun Section, Weapons Platoon, Lima Company, Battalion Landing Team 3/5, 15th Marine Expeditionary Unit , practice magazine reload drills in the well deck of the USS Green Bay, Oct. 9. 

US Marine Corps photo by Cpl. John Robbart III.
