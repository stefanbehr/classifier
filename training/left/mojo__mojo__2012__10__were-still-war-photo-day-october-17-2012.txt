CH-53E Super Stallion helicopters from Marine Medium Helicopter Squadron-262 (REIN), 31st Marine Expeditionary Unit , prepare to land and offload Marines during a helicopter raid exercise in Crow Valley, Philippines, Oct. 14. 

U.S. Marine Corps photo by Pfc. Caleb Hoover.
