Kaydence Hoversten, 22-month-old daughter of Gunnery Sgt. Jared Hoversten, sits in front of buses shortly before her dad and other Regimental Combat Team 7, 7th Marines leave for their deployment to Afghanistan Oct. 9 at 7th Marine Regimental Headquarters. 

US Marine Corps photo by Cpl. William J. Jackson.
