In an interview published in a monthly legal magazine , Supreme Court Justice Antonin Scalia says women have no specific constitutionally protected right against discrimination, defends the court's decision to hear Bush v. Gore and rules that pizza in Washington D.C. and Chicago doesn't measure up to pies from New York City. 

The remarks about gender discrimination came in response to a question about the 14th Amendment. Scalia says there's nothing stopping Congress or any other legislative body from passing a law preventing sex discrimination but emphatically says the 14th Amendment, ratified in the wake of the Civil War giving rights to freed slaves, is silent on the matter. "Certainly the Constitution does not require discrimination on the basis of sex," Scalia said. "The only issue is whether it prohibits it. It doesn't. Nobody ever thought that that's what it meant. Nobody ever voted for that." 

Scalia's comments came during an interview with University of California Hastings College of Law professor Calvin Massey and were published in this month's issue of California Lawyer under an article titled "The Originalist." 

On the controversial 2000 decision in favor of George W. Bush that allowed the Texas governor to win the presidency, Scalia who once dismissed critics opposed to the ruling by saying "get over it," struck a more conciliatory tone. He said the justices didn't like having to address the matter but were essentially compelled to do so. "I think that the public ultimately realized that we had to take the case... I was very, very proud of the way the Court's reputation survived that, even though there are a lot of people who are probably still mad about it." 

As for his choice in pizza, Scalia says nothing beats a slice made in New York City. "I think it is infinitely better than Washington pizza, and infinitely better than Chicago pizza. You know these deep-dish pizzas-it's not pizza. It's very good, but ... call it tomato pie or something. ... I'm a traditionalist, what can I tell you? 

Speaking of food, in another article in the same issue , a lawyer details the dinner party she hosted for Justice Ruth Bader Ginsburg .
