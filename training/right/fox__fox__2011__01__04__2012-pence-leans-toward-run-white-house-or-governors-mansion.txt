Former House Republican leader Mike Pence is a favorite among grassroots conservatives and Tea Party activists, many of whom have been pushing him for months to make a run for the White House in 2012. 

When asked about his political aspirations Tuesday night, Pence replied, "My little family has made no decision about my future." 

However, if the congressman from Indiana is planning a presidential run, his schedule sure has a funny way of showing it. 

The next few months will be the time for presidential hopefuls to be dropping into the all-important early presidential states of New Hampshire and Iowa. 

However, Fox News has learned that Pence is in the process of scheduling "his share" of Lincoln Day Dinners in Indiana through January, February and March. 



This would be a move much more in line with a man seeking residence at the Hoosier state governor's mansion instead of 1600 Pennsylvania Ave. 

"He is well positioned to run for Governor," said one Indiana GOP insider. "However, I don't think he's made up his mind yet." 

Outgoing Democratic Senator Evan Bayh and Republican Lieutenant Governor Becky Skillman have announced they will not seek the gubernatorial nomination, which potentially clears the way for Pence. 

"I think he's keeping his options open," said one national Republican. "He certainly looks like he's getting ready to run for something other than his current office." 

Many Republicans warn that writing off a presidential run from Pence is premature, "It's an easy flight from Indianapolis to Iowa... Just because he's doing some Lincoln Day Dinners in Indiana doesn't mean he's out," said one Washington GOP insider. 

Fox News' Chad Pergram contributed to this report.
