It's been rough going for middle-of-the-road politicians. 

The 2010 elections saw both parties push out moderates in favor of ideologically pure alternatives. Failed Senate candidates Arlen Specter and Mike Castle could get together and have a good cry about the political dangers of centrism. 

The poster boy for this trend, though, is Florida's Charlie Crist, who went from being a popular Republican governor to a flailing independent Senate candidate. Crist's unraveling should cool, for a little while at least, the undying media narrative that what voters really want are nonpartisan centrists. 

Election after election shows that voters are less concerned about labels than they are with character and competency. Americans elect lots of conservatives and many liberals, but have deep misgivings about candidates whose views vary with the situation. 



Some suggested that New York Mayor Mike Bloomberg's presidential chances have been damaged by his city's ham-handed response to last week's blizzard. But every poll shows that his chances weren't very good to begin with. 

Or consider that louche Republican Arnold Schwarzenegger once spurred discussions of the need to rewrite the Constitution so foreigners could become president. But instead of heading to Washington, Der Governator left Sacramento with a 22-percent approval rating. 

Voters just aren't in search of a post-ideological president, no matter the pining of Beltway journalists. 

It may sound good to focus group participants when a questioner offers the abstract idea of politicians putting aside their differences to work together. But when you get down to whose differences will be put aside, things get unhappy very quickly. The American ideal of bipartisanship usually involves the other guy giving up. 

While Democrats overall took a pasting in November, it was moderate Democrats who suffered the most. 

Conservative Democrats who had held on for many cycles in Republican-leaning and swing districts by drawing distinctions between themselves and their national party got wiped out. 

Democrats in the most liberal districts, meanwhile, were untouched by the crimson tide. 

Outsiders were puzzled when liberal doyenne Nancy Pelosi retained her spot atop the House Democratic caucus despite the worst drubbing of her party in more than 60 years. But the Republican wave of 2010 has made the moderate Democrat an endangered species. 

And times are about to get even tougher. 

Some of the surviving moderate Democrats in the House plan to mark Wednesday's votes for speaker with protests ballots for neither John Boehner nor Nancy Pelosi , but Blue Dog Rep. Heath Shuler of North Carolina. That contortion shows just how tricky their situation has become, and the vote for speaker will be just the first in a series of painful twists. 

A few Democrats will feel obliged to vote for repealing President Obama's national health-care law, certainly not enough to get it through the Senate. But there will be many more in the party who will feel pressure to vote for changes to the law that could render it nearly meaningless. 

Remember that there are 23 Senate Democrats up for reelection next year, and for those in precarious positions, like Virginia's Jim Webb and West Virginia's Joe Manchin, votes against things like ending the health care law's mandatory insurance provision could be politically poisonous. 

The Republican House will continue to serve up these unappetizing options for moderate Democrats, first on health care and then on spending. Soon after will come test votes on blocking the EPA from imposing new costs on carbon emissions and preventing the FCC from regulating the Internet. 

The conventional wisdom crowd in Washington pooh-poohs House Republicans for passing bills that everyone knows can't pass the Senate. They may have thought that Pelosi's Democrats were principled for passing measures bound to die in the Senate, but they seem to think that Boehner's Republicans are either fools or cynics, and possibly both. 

But the sages of the Potomac - usually the same ones roaring for centrist government in the Bloomberg/Crist/Schwarzenegger model - forget that Boehner's bills may not always produce laws, but will produce lots of squirming among the remaining moderate Democrats in Congress. 

And having seen the bloodbath of 2010, some of those moderates will be looking for even more chances to get away from their own party on controversial issues. If Boehner keeps throwing bills at the Senate, something might just stick. 

Chris Stirewalt is FOX News' digital politics editor. His political note, Power Play, is available every weekday morning at FOXNEWS.COM.
