President Obama had a very full inbox when he returned to Washington Tuesday after his Hawaiian vacation. He signed 35 laws including the FDA Food Safety Modernization Act. 

The Food Safety law is considered the largest reform of national food safety in more than 80 years. It received bipartisan support as well as an endorsement from the Chamber of Commerce. The White House says the safety bill directs the FDA "to build a new system of food safety oversight - one focused on applying, more comprehensively than ever, the best available science and good common sense to prevent the problems that can make people sick." But the new Republican majority in the House of Representatives may hold up funding of the $1.5 billion over the next five years. 

President Obama also signed the Diesel Emissions Reduction Act of 2010 which modifies and reauthorizes the Environmental Protection Agency's Diesel Emissions Reduction Program. Another important law to the administration now law, the "Anti-Border Corruption Act" which requires polygraph exams for all applicants for customs border agents. 

The full list of all 35 laws the President signed today: 

H.R. 81, the "Shark Conservation Act of 2010 and International Fisheries Agreement Clarification Act," which generally prohibits the removal of shark fins at sea and amends certain laws related to international fisheries; 

H.R. 628, which establishes a pilot program regarding the adjudication cases where patent or plant variety protection issues are to be decided; 

H.R. 1107, which restates and reorganizes the public contract laws of the United States in Title 41, United Sates Code; 

H.R. 1746, the "Predisaster Hazard Mitigation Act of 2010," which authorizes appropriations for the Federal Emergency Management Agency's Pre-Disaster Mitigation (PDM) program for FYs 2011-2013; 

H.R. 2142, the "GPRA Modernization Act of 2010," which amends the Government Performance and Results Act to establish a Federal government performance plan; 

H.R. 2751, the "FDA Food Safety Modernization Act," which modernizes the food safety system to better prevent food-borne illness and better respond to outbreaks; 

H.R. 4445, the "Indian Pueblo Cultural Center Clarification Act," which repeals a restriction on the treatment of certain lands held in trust for Indian Pueblos in New Mexico; 

H.R. 4602, which designates a facility of the United States Postal Service as the Emil Bolas Post Office; 

H.R. 4748, the "Northern Border Counternarcotics Strategy Act of 2010," which requires the Office of National Drug Control Policy to develop a Northern Boarder Counternarcotics Strategy; 

H.R. 4973, the "National Wildlife Refuge Volunteer Improvement Act of 2010," which reauthorizes and amends authorities relating to volunteer programs and community partnerships for national wildlife refuges; 

H.R. 5116, the "America Creating Opportunities to Meaningfully Promote Excellence in Technology, Education, and Science (America COMPETES) Reauthorization Act of 2010," which reauthorizes various programs intended to strengthen research and education in the United States related to science, technology, engineering, and mathematics; 

H.R. 5133, which designates a facility of the United States Postal Service as the Staff Sergeant Frank T. Carvill and Lance Corporal Michael A. Schwarz Post Office Building; 

H.R. 5470, which excludes specified external power supplies from certain energy efficiency standards required by the Energy Policy and Conservation Act; 

H.R. 5605, which designates a facility of the United States Postal Service as the George C. Marshall Post Office; 

H.R. 5606, which designates a facility of the United States Postal Service as the James M. "Jimmy" Stewart Post Office Building; 

H.R. 5655, which designates the Little River Branch facility of the United States Postal Service as the Jesse J. McCrary, Jr. Post Office; 

H.R. 5809, the "Diesel Emissions Reduction Act of 2010," which modifies and reauthorizes through FY 2016 the Environmental Protection Agency's Diesel Emissions Reduction Program; 

H.R. 5877, which designates a facility of the United States Postal Service as the Lance Corporal Alexander Scott Arredondo, United States Marine Corps Post Office Building; 

H.R. 5901, which authorizes the U.S. Tax Court to appoint employees under a personnel management system that includes the merit system principles and prohibitions on personnel practices; 

H.R. 6392, which designates a facility of the United States Postal Service as the Colonel George Juskalian Post Office Building; 

H.R. 6400, which designates a facility of the United States Postal Service as the Earl Wilson, Jr. Post Office; 

H.R. 6412, the "Access to Criminal History Records for State Sentencing Commissions Act of 2010," which requires the Department of Justice to exchange records and information with State sentencing commissions; 

H.R. 6510, which directs the General Services Administration to convey to the Military Museum of Texas the parcel of real property in Houston, Texas, on which the museum is located; 

H.R. 6533, the "Local Community Radio Act of 2010," which modifies current restrictions on low-power FM radio stations; 

S. 118, the "Section 202 Supportive Housing for the Elderly Act of 2010," which amends financing and project operation requirements for the Department of Housing and Urban Development's program to allow for increased housing opportunities for low-income seniors; 

S. 841, the "Pedestrian Safety Enhancement Act of 2010," regarding pedestrian safety related to motor vehicles; 

S. 1481, the "Frank Melville Supportive Housing Investment Act of 2010," which amends financing and project operation requirements for the Department of Housing and Urban Development's program for low income individuals with disabilities; 

S. 3036, the "National Alzheimer's Project Act," which establishes a National Alzheimer's Project within the Department of Health and Human Services and an advisory council on Alzheimer's research, care, and services; 

S. 3243, the "Anti-Border Corruption Act of 2010," which requires the Department of Homeland Security to ensure that all applicants for law enforcement positions with U.S. Customs and Border Protection (CBP) receive polygraph examinations; 

S. 3447, the "Post-9/11 Veterans Education Assistance Improvements Act of 2010," which amends the Post-9/11 GI Bill, and other educational assistance programs for veterans; 

S. 3481, which clarifies the Federal Government's responsibility to pay reasonable service charges to a State or local government to address stormwater pollution from Federal properties; 

S. 3592, which designates a facility of the United States Postal Service as the First Lieutenant Robert Wilson Collins Post Office Building; 

S. 3874, the "Reduction of Lead in Drinking Water Act," which modifies the Safe Drinking Water Act definition of "lead free" with regard to pipes, pipe fittings, plumbing fittings, and fixtures; 

S. 3903, which authorizes 99-year leases on land held in trust for the Ohkay Owingeh Pueblo in the State of New Mexico; and 

S. 4036, which amends authorities of the National Credit Union Administration.
