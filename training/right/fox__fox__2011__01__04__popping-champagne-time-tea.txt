Is hosting a $2,500 a plate fundraiser with A-list celebrity entertainment consistent with Tea Party principles? Rep-elect Jeff Denham, R-Calif., thinks so. 

And in an unprecedented twist, rather than having a low-key big money affair, as is usually the case, Denham chose to start his event with a press conference, though an aide asked the media to be brief with the questions, "because we have a party to get to." 

Why have a media availability before a high dollar fundraiser you ask? "You guys all wanted an interview, so we decided at the last minute we would accommodate you," Denham shot back. 

Denham defended the glitzy soiree from allegations that the event was not in keeping with the Tea Party principle of austerity. "We're going to be self reliant," Denham said, "We're going to make sure that we are going to have the funds for our reelection and hold all of our own seats and bring our members back." 

The former state senator has already learned one of the vital lessons of politics: elections cost money, so you better raise a lot of it. 

"Campaigns are very expensive," Denham noted, "We know the Democrats are out there right now not only recruiting candidates but also raising funds." 

Denham brushed aside the concerns of some social conservatives, who felt the night's entertainment, country singer LeAnn Rimes , was inappropriate. She confessed to having an affair with a married man in 2009 prior to divorcing her husband. " LeAnn Rimes is a Grammy Award winning entertainer. We've asked her to come play some country music for us and we're excited that she's come to do that." 

The event was expected to draw not just freshmen representatives, but members of the Republican leadership as well. National Republican Campaign Committee Chairman Pete Sessions, R-Texas, and fellow Californian Majority Whip Kevin McCarthy were anticipated guests. 

Denham didn't think that this event was a way to prove his potential rainmaker status with his colleagues. "I think this is an opportunity for me to show some leadership, and to do something good for our entire class," he said.
