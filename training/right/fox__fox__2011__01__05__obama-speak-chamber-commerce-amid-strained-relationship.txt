The Obama Administration and the Chamber have had a contentious relationship through the first two years of the Obama presidency, but there are signs that both sides may have made a New Year's resolution to try to get along. A spokesperson for the U.S. Chamber of Commerce tells Fox News that President Obama will speak at the U.S Chamber of Commerce on February 7th. 

"We look forward to hosting the President next month to discuss jobs and the economy," Tom Collamore, Senior Vice President of Communication for the Chamber told Fox News. "This remains the top priority of the Chamber and the business community, and we're committed to working together to put Americans back to work." 

The last two years were marked by aggressive attacks by Tom Donohue, President of the Chamber, on President Obama's signature legislative initiatives. Donohue called the health care bill a "burdensome mandate on employers" and said Obama's Wall Street reform would "choke off" access between business and capital. 

But lately, it seems Obama and the Chamber may be working to patch things up. In addition to President Obama's upcoming visit, the President is considering William Daley, a former Clinton Secretary of Commerce with close ties to the Chamber, to replace Rahm Emanuel as White House Chief of Staff. Daley is an executive at J.P. Morgan and worked on the Chamber's commission on financial deregulation. 

The Chamber has also been supportive of some Obama legislation, including the Food Safety Modernization Act the President signed into law Tuesday. 

Fox Business Network's Peter Barnes contributed to this story.
