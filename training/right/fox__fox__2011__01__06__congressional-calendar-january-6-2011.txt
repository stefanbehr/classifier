Do you hear that? It's the Constitution. 

The House will have a bipartisan reading of the document that serves as the supreme law of the land at approximately 11:00 a.m. ET. It should take one and a half to two hours to complete. 

Afterwards, the House will vote on a resolution offered by Rep. Greg Walden, R-Ore., under suspension rules, which require a two-thirds majority, to cut Congress' budget by five percent. Republicans estimate that this cut will save $35 million in the first year. 

House Republicans inch closer to fulfilling one of their campaign promises inside a tiny room on the third floor of the Capitol at 10:00 a.m. ET. The House Rules Committee meets to hammer out the terms and conditions for debating and voting on a bill to repeal the so-called ObamaCare health care legislation passed during the 111th Congress. The chamber plans to hold a vote on the matter January 12. 

Senate Democrats are not pleased with the budgeting rules passed by the House Wednesday. Sen. Chuck Schumer , D-N.Y., and other party members claim that they are full of loopholes that will exacerbate the nation's deficit, which is now just over $14 trillion. Schumer and others talk to the press on the matter at 10:00 a.m. ET. 

Chairman of the House Budget Committee Paul Ryan , R-Wisc., also has some strong feelings on the deficit and the nation's long-term financial stability. He'll be addressing those topics and more at a luncheon off the Hill at the National Press Club at 12:00 p.m. ET. 

Later in the afternoon, Senate Majority Leader Harry Reid , D-Nev., plans to hold a media availability to discuss Democratic priorities for the session. Senate Republicans will have a similar event at 3:30 p.m. ET following their annual issues conference.
