Republicans already face a procecural dilemma days after regaining control of the House, as two of their members cast votes before being sworn in. 

Rep. Pete Sessions, R-Texas, and Rep. Mike Fitzpatrick, R-Pa., weren't on the floor during the official swearing-in ceremony on Wednesday. 

The House Rules Committee adjourned abruptly Thursday because Rep. Sessions, who was present at the rules hearing Wednesday, hadn't been sworn in. Sessions was sworn-in on the House floor Thursday afternoon. 

That triggered an immediate response from the left, House Rules Committee Chairman David Dreier, D-Ca., said the committee will "vitiate votes" taken Wednesday. And added, "We have a unique situation here...we are in unchartered waters." 

Ranking Democrat Louise Slaughter, D-NY, who called for the panel to start over tomorrow, said, "This is the day we read the Constitution. We don't want to be in violation of that." 

All of this raises questions as to whether a resolution that Sessions introduced last night in the Rules Committee (which was adopted) is invalid. The Rules Committee is still trying to figure this out. 

Meanwhile Mike Fitzpatrick, R-Pa., apparently took part in a mock swearing-in, but not the real swearing-in. 

He was in the Capitol Visitor's Center Wednesday when the House swore in the representatives. Even though Fitzpatrick dodged the official ceremony that was to make him a member of Congress, he showed up hours later for a photo op of the "mock" swearing-in ceremony with House Speaker John Boehner (R-OH). 

The mock swearing-in is where a member, his family and the Speaker stage a picture of them acting like they're taking the oath. 

Dreier pointed out that Jefferson's Manual (written by Thomas Jefferson [1], and is the guidebook to many House operations to this day) states that members-elect must be sworn-in by the Speaker in "proximity" of the Speaker. Dreier insisted that being in the Capitol Vistor's Center and not the House floor does not constitute being in the "proximity" of the Speaker for the oath. 

When asked how exactly Fitzpatrick was sworn in, Spokesman Darren Smith said, "He stood in front of the TV that all the constituents were watching it on and took it there." 

Fox News producers John Brandt and Wes Barrett contributed to this report.
