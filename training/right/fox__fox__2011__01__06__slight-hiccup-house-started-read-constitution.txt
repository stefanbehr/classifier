UPDATE: 12:20 PM: Another hiccup took place in the House Chamber as members read the Constitution. US Capitol Police confirm to Fox News that they have arrested one person for disrupting Congress. The alleged offender yelled something from the public viewing gallery while they were reading the part of the Constitution about qualifications for president. That suspect is now being processed at US Capitol Police headquarters and the cops promise more details later. 



Even before Rep. Bob Goodlatte, R-Va., could begin reading the Constitution in the House of Representatives Thursday, Rep. Jay Inslee, D-Wash., raised a parliamentary inquiry, trying to ascertain exactly what would be read. Would it be the original Constitution, or the Constitution as it now stands, with certain portions superseded by amendments. 

Showing the rust of Republicans not having presided over the House Chamber for the last four years, Rep. Mike Simpson, R-Idaho, ruled that Inslee was not making a proper parliamentary inquiry, even though he was. Inslee then asked Goodlatte to yield for a unanimous consent request to find out the answer to his question. 

Simpson initially ruled that out of order again, even though such a request is, in fact, in order. Since Goodlatte allowed it, ultimately, Inslee got to ask his question. 

Somewhat in jest, Inslee pointed out that since the Republicans did not alert Democrats until now exactly what version of the Constitution would be read, Democrats had not had the appropriate 72 hours ahead of time to consider the language now before the House.
