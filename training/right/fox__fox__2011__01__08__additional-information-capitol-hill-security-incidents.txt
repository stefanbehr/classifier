Working backwards, just before Christmas, the US Capitol was placed on lockdown, meaning no one could come or go, after a man identified himself to US Capitol Police as having a gun. U.S. Capitol Police took cover behind barricades and eventually apprehended the man between the Russell and Dirksen Senate Office Buildings. The suspect was found to have no gun. 

In July, 2009, U.S. Capitol Police pursued a suspect in a high speed chase down the wrong way of Louisiana Avenue in the shadow of the Capitol. The suspect eventually crashed his car in front of a Senate garage and drew a gun on US Capitol Police officers and began to shoot. He was shot and killed by US Capitol Police. 

In January, 2008, a man parked his car on the Senate side of the Capitol (stocked to the brim with explosives) and was spotted carrying a shotgun across the Russell Senate Park. He was apprehended by Capitol Police after he told officers he had a meeting with US Chief Justice John Roberts . 

In July 1998, Russell Weston Jr. approached the Memorial Door of the Capitol on the House side and shot and killed Officer Jacob Chestnut. Weston then entered the building and got into the office suite of then-House Minority Whip Tom DeLay (R-TX). Detective John Gibson was a plainclothes officer detailed to DeLay. Gibson was hit by Weston's gunfire. But before he died, got off a shot that wounded Weston. Weston would have died had had then-Sen. Bill Frist (R-TN), a heart surgeon, not saved his life. 

Weston was declared incompetent to stand trial. Doctors say he suffers from paranoid schizophrenia and has never been tried. 

Chestnut and Gibson remain the only people ever killed defending the US Capitol.
