A law enforcement memo based on information provided by DHS and obtained by Fox News suggested strong suspicion linking Jared Loughner, the man accused in the Tucson shooting on Saturday, to what it called an "anti-ZOG (Zionist Occupational Government) and anti-semitic" group known as American Renaissance. 

In an effort to counter those charges, the head of the organization responded directly to Fox News' James Rosen on Sunday. 

Jared Taylor called DHS' views "scurrilous" and took especial issue with the reference to his group being "anti-ZOG." 

"That is complete nonsense," he said. "I have absolutely no idea what DHS is talking about. We have never used the term 'ZOG.' We have never thought in those terms. If this is the level of research we are getting from DHS, then Heaven help us." 

Taylor, who earned a BA in philosophy from Yale in 1973 and a master's degree in international economics from the prestigious Institut d' tudes Politiques de Paris ("Paris Institute of Political Studies," in English) in 1978, says he had never heard of Loughner until yesterday. Taylor says he checked his organization's records going back twenty years and Loughner never subscribed to AmRen's publications. 

Taylor says he also has no indication that Loughner ever attended any of AmRen's events, all of which have been held on the East Coast.
