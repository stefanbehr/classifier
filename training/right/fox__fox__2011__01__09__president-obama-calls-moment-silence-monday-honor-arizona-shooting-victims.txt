President Obama is calling for a moment of silence Monday morning in the wake of the Arizona shooting that left six dead and 14 wounded including Rep. Giffords,D-Ariz., who is in critical condition. 

"Tomorrow at 11:00 a.m. eastern standard time, I call on Americans to observe a moment of silence to honor the innocent victims of the senseless tragedy in Tucson, Arizona, including those still fighting for their lives. It will be a time for us to come together as a nation in prayer or reflection, keeping the victims and their families closely at heart," the president said in a statement. 

Obama plans on observing the moment of silence with other staff members on the South Lawn at the White House . 

Sunday he also signed a proclamation calling for flags to be flown at half-staff.
