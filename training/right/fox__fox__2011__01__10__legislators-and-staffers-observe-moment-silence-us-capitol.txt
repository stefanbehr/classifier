Swarms of staff congressional staffers flooded across Independence Avenue from the House office buildings to the U.S. Capitol steps to observe Monday's moment of silence in response to Saturday's shooting in Tuscon. 

They filled the steps, all the way back to the columns and U.S.Capitol Police checked to see that each person had a staff ID. The group was flanked by Bill Livingood, House Sergeant at Arms and Terrence Gainer, Senate Sergeant at Arms. 

Rep. Emanuel Cleaver, D-Mo., an ordained minister, led the group in prayer.
