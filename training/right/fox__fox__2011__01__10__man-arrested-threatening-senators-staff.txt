Just two days after the shooting of a U.S. congresswoman, a man was arrested by the FBI, in coordination with the Capitol Police, in Denver, Colo, for threatening to shoot the staff of Sen. Michael Bennet, D-Colo. 

The two incidents are not related, but the alleged shooter, John Troy Davis, according to the criminal complaint filed in U.S. district court, told one of the aides that he "killed that woman," possibly referring to Cong. Gabrielle Giffords, D-Ariz., who was not killed but critically wounded by an assailant in her district on Saturday. It was first reported by many that she was dead after being shot in the head. 

"I'm just going to come down there and shoot you all," Davis reportedly told one Bennet staffer. 

Davis, the complaint reads, was "well-known" to Bennet's staff, as he had called numerous times to request a hearing about his Social Security benefits, saying, "I'm a schizophrenic and I need help." 

Bennet's aides arranged a hearing for the man, but he never showed, the court documents state. 

Davis called so often that aides knew his voice, and according to the complaint, Davis would refer to himself as "Sid" or "Troy." Three staffers were victims of Davis' threats, the document says. 

The accused has been charged with a federal crime, making threats to a U.S. official, and if found guilty could face up to 10 years in prison and/or a $250,000 fine.
