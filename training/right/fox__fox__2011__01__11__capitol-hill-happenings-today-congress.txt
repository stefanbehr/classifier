The Capitol continues to reflect on the weekend's tragic events in Tucson, Arizona. 

Visitors and staff will continue to have the chance to sign a book of well wishes and condolences in the Rotunda of the Cannon House Office Building. 

Senate Judiciary Committee Chairman Patrick Leahy , D-Vt., plans to address the Giffords incident at an 11:00 a.m. ET address on his panel's priorities in the 112th Congress. 

Sen. John Kerry , D-Mass., delivers an address at the Center for American Progress, a Washington, DC think tank, on "the urgent need to restore a sense of purpose to the U.S. political process" at 10:00 a.m. ET. 

We'll be following all these stories and more, so stay with Fox News for all the latest.
