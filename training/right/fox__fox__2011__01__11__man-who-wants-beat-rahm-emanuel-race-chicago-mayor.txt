The countdown to elect Chicago's next mayor is only three weeks away. Early voting begins at the end of January, which for many Chicago residents, will be the first time they will vote in a highly competitive race for mayor in 20 years. That's how long it's been since current mayor, Mayor Richard M. Daley took office. 

At this stage in the race, the field has narrowed significantly, with several candidates dropping out. The top three contenders include former White House Chief of Staff, Rahm Emanuel , former U.S. Senator Carol Moseley Braun and a man named Gery Chico. 

Chico's name is well known in Chicago political circles. He served as current Mayor Richard M. Daley's chief of staff for several years in the 90's, then went on to head the Chicago public schools and the Chicago park district. 

A recent boost to Chico's profile, came in the form of an endorsement from U.S. Rep. Luis Guitierrez (D-IL). Chico is finally making his debut on the Chicago air-waves, launching two TV ads, one which lays out his credentials and tells viewers he wants "to take Chicago in a whole new direction." Neither ad mentions any of his opponents. 

Tuesday Gery Chico made possibly the largest announcement yet of his campaign, by declaring his support to build a world-class health facility in Chicago devoted to curing the most prominent diseases. Those spearheading the project say the American Center for Cures (ACC) would focus on curing cancer, diabetes, Parkinsons and Alzheimers. 

The ACC, which has been in the planning phase for 10 years would require serious federal support and cash, to the tune of $36 billion a year for six years.One of the project leaders, Lou Weisbach, says he's pitched the ACC idea to Barack Obama , prior to him becoming president and has also received support from President Clinton and Secretary of State Hillary Clinton . Chico says a health facility of this size could create, "tens of thousands of new jobs in Chicago for years to come." 

The big question is where will the money come from? Chico and others continue to push for Congressional support and say they have verbal commitments from Sens. Joe Lieberman (I-CT), Kay Bailey Hutchison (R-TX) and Kirsten Gillibrand (D-NY). 

Chico says if he's elected mayor of Chicago, he would make the ACC project a top priority for the city. But at this point, Chico's biggest challenge remains beating out his opponents on election day, February 22. There are nine people on the ballot vying to be Chicago's next mayor. If no candidate receives the majority of the vote on February 22, the top two vote-getters compete in a runoff election on April 5.
