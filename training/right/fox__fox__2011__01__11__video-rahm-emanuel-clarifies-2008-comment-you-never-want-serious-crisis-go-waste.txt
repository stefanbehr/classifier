"You never want a serious crisis to go to waste." 

-- Rahm Emanuel during a November, 2008 Wall Street Journal Forum 

It is a comment former Congressman and White House Chief of Staff Rahm Emanuel might wish he'd never made, in light of the fact that its being repeated in newspaper articles and editorials about Saturday's tragic shooting in Tucson, Arizona. The quote is being used to explain how some lawmakers and political pundits on both sides of the aisle may be trying to exploit the incident and place blame on their opponents. 

Emanuel said the infamous comment was never intended to be about an incident such as the shooting on Saturday which killed six people and wounded 14 others, including Rep. Gabrielle Giffords, D-Ariz., who was hosting a "Congress on Your Corner" event at a Tucson grocery store. 

At a press conference in Chicago Tuesday, a very solemn looking Emanuel called Giffords a friend that he helped get elected, and said she was doing what she loved most; meeting with the public. 

"First of all, what I said was never let a good crisis go to waste when it's an opportunity to do things you had never considered or you didn't think were possible," he told FOX News when asked about the quote. 

Despite the tragedy in Arizona, Emanuel, who said he started the 'Congress on Your Corner' program as a way for lawmakers to connect with constituents, doesn't want to see the public meetings end. 

"I think it is important to continue to do that so people have direct access" Emanuel said. "Do I worry about security? Yes. I will not change the way I do stuff." 

When asked whether he thought liberals and conservatives were trying to use the incident for political gain, Emanuel didn't address the question directly, but simply said "I think the president has been reflective and set the exact tone that we as a country should take, not only a moment of silence, but think deep about where we are...the president has done exactly right and set the right tone." 

Watch the latest video at FoxNews.com
