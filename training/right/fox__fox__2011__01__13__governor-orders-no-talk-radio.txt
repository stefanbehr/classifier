No talk radio. 

That is the order from Rhode Island Governor Lincoln Chafee , who has banned state officials from conducting interviews on talk radio, except in emergencies. 

A spokesman for the Governor's office says the decision is not related to the Tucson shootings and was considered before the tragedy. 

Some have blamed talk radio for the deadly shooting in Arizona, even though talk radio defenders say there is no evidence that it influenced the suspect at all, or even if Jared Loughner listened. 

Chafee, a former Republican Senator who is now an Independent, barred state employees, including himself, from appearing on what he calls "for-profit entertainment" radio programs, saying they are more entertainment than journalism. He will go on NPR, public radio though. 

The Governor's spokesman told us "This is a brand new administration facing a mammoth deficit. We are looking in every way to be efficient. We do not think it's appropriate to invest taxpayer resources... To support what we see as an entertainment format not a news format." 

But during the snow storm yesterday, officials did go on talk radio to discuss their handling of it. 

Some are slamming Chafee's policy, but his office says it is getting some praise. 

One critic is former Providence Mayor turned radio talk show host Buddy Cianci, a Rhode Island legend. 



He served time in federal prison for racketeering, and now hosts his own talk radio program on Providence's WPRO 630AM and says the Governor is way off base. 

"He's the loser in this situation," Cianci told Fox News. "The Governor did the wrong thing...the fact is when you say, 'I will not be available and my staff and my cabinet won't be available,' I think that just breeds a tremendous amount of discontent in the community. People cannot ask questions of those who are serving them, and those who have been put in office to straighten out some of the problems that we have." 

Cicanci says talk radio is a place for public dialogue and that he tries to make his show entertaining and witty, as well as informative. 

"It's a public conversation," says Cianci. "We're a small state and talk radio has really been a staple. In Texas they have football as a sport, in Rhode Island we have politics as kind of a blood sport and the fact is people want to be part of that conversation." 

An estimated 30 million Americans listen to talk radio across the country every day.
