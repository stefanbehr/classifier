Republican Senator Kay Bailey Hutchison of Texas will not seek re-election in 2012. 

Hutchison wrote a letter to supporters, telling them of her intent to step-down at the end of the term. 

"I am announcing today that I will not be a candidate for re-election in 2012," the letter said. "That should give the people of Texas ample time to consider who my successor will be. In the next two years, you can be assured that I will pursue my duties, and my responsibilities to our state and people, with the same vigor that I have employed during my Senate service." 

In the letter to her supporters, Hutchison went on to say that serving in the Senate had become more difficult of late. 

"When my current term is up, I will have served Texas for 19 years in the United States Senate," she wrote. "I intended to leave this office long before now, but I was persuaded to continue in order to avoid disadvantage to our state. The last two years have been particularly difficult, especially for my family, but I felt it would be wrong to leave the Senate during such a critical period." 

Democrats reacted quickly to the news Hutchison is stepping aside in 2012 saying they see a chance for gains. 

"The 2010 cycle was full of surprises and it turns out 2012 will have some twists and turns as well: the first Senate retirement is a Republican," Democratic Senatorial Campaign Committee Communications Director Eric Schultz said in a statement. " We look forward to running a competitive race in Texas as the Lone Star state is now one of several Democratic pick-up opportunities next November." 

But one political analyst doubts Democrats can compete for the vacant seat. University of Virginia political scientist Larry Sabato said through his Twitter account, "Real question in Texas SEN is whether Dems, landslide losers of late, can even compete. Sharing ballot with Obama in '12, highly doubtful." 

Hutchison won a special election in 1993 to replace Democratic Sen. Lloyd Bentsen who resigned to become Secretary of the Treasury under President Bill Clinton . She was elected to full terms in 1994, 2000 and 2006. 

Sources tell FOX News that Hutchison conducted a conference call with the Texas Congressional delegation Thursday and told them she would not run for another term. 

Fellow Texas Republican Senator John Cornyn offered praise for his retiring colleague saying in a statement, "Kay came to the Senate to make a difference, to work to find solutions to the complex problems of modern society, and to attain real and lasting change. She has succeeded in brilliant fashion." 

Former President George W. Bush -- also a fellow Texan -- says Hutchison "has been an effective leader who has served her fellow citizens with honor and dignity." 



Hutchison tried to make a move back to Texas in 2010, mounting an unsuccessful challenge to Governor Rick Perry in the Republican gubernatorial primary. Now that she is leaving the Senate, Hutchison says she looks forward to living in Texas with her family full-time. 

FOX News Senior House producer Chad Pergram contributed to this report.
