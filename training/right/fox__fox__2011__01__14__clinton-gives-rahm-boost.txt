Former White House Chief of Staff Rahm Emanuel is getting a visit from a heavy hitter next week, in his bid to become mayor of Chicago. 

On Tuesday former President Bill Clinton is coming to the windy city to appear at a campaign rally with Emanuel. 

The event at the Chicago Cultural Center is expected to draw a great deal of attention, cash and support for the candidate. But Clinton's visit is also ratcheting up a lot of controversy among Democrats. 

One of Emanuel's opponents in the mayoral race, former U.S. Senator Carol Moseley Braun, D-Ill., said Clinton should keep his influence out of the race, while former candidate, Congressman Danny Davis, D-Ill., begged Clinton to keep his distance. "Mr. President, don't come. Don't help Rahm." he said. 

Davis loudly complained that by getting in the middle of the mayoral race, Clinton, who was once famously nicknamed "The First Black President" because of his connection with the African American community, is working against Chicago's African American community by funneling some of its votes to Emanuel instead of black candidates. 

Davis issued a statement in December before he dropped out of the race warning the former President what could happen if he inserted himself into the race. 

"The African American community has enjoyed a long and fruitful relationship with the Clintons, however it appears as though some of that relationship maybe fractured and perhaps even broken should former President Clinton come to town and participate overtly in efforts to thwart the legitimate political aspirations of Chicago's Black community." 

Emanuel's campaign refuses to comment on the controversy and representatives for former President Clinton didn't immediately respond to a request for comment. 

The Illinois Democratic Party is staying out of it. Spokesman Steve Brown says they're "staying neutral." 

Chicago's top job is becoming available because longtime Mayor Richard Daley isn't running for re-election. 

The well connected Emanuel was a senior aide in President Clinton's administration and is connected among top Democratic leaders. He's a former Congressman and former White House Chief of Staff to President Barack Obama . 

Chicago voters go to the polls to elect a new mayor on February 22.
