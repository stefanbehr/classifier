A close advisor to Florida Senate President Mike Haridopolos confirms to FOX News that he has officially decided to run against incumbent Democratic U.S. Senator Bill Nelson , who is up for re-election in 2012. 

Haridopolos is the first challenger to Nelson, and will be congregating with potential campaign donors in Orlando to discuss campaign strategy in February, according to the Haridopolos insider. First elected to the state Senate in 2003, Haridopolos is consdered a rising star in the Republican party of Florida, and has been an effective fund raiser for the GOP in the Sunshine State. 

A recent Public Policy Polling poll showed that Jeb Bush, the former governor, would be the clear choice among Florida Republicans to challenge Nelson, with 72 percent of those surveyed indicating that they would back him. However, since the release of the PPP poll, Bush has repeatedly rejected any interest in that seat. 

" I love the Republican people of Florida but I had a chance to serve.I said what I wanted to do, and I worked hard to do what I was going to do, " Bush told FNC's Geraldo Rivera in an interview Thursday. " I don't view politics as a career and I have a duty I think to my family to achieve financial security." 

With Bush out of the picture, 31 percent of Florida Republicans remain unsure about whom to back. The PPP survey showed that the GOP field is wide-open, and has the potential for a battle royale of a Senate primary race. Other expected GOP contenders: U.S. Rep. Connie Mack, former Senator George LeMieux, former House Majority Leader Adam Hasner, and U.S. Rep. Vern Buchanan, among others.
