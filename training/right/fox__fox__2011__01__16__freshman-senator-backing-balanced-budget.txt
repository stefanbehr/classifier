Buoyed by strong Tea Party support during his campaign, freshman Senator Mike Lee (R-Utah) has arrived in Washington and says he's determined to take significant steps to rectify the staggering U.S. deficit . Lee talked to Fox News on Sunday about his proposed constitutional amendment to balance the budget. 

In order to propose such an amendment, several steps would need to be taken. The first option, which Lee says is the path he will choose, is for both houses of Congress to pass a two thirds majority vote. The second option would be a Constitutional Convention, which would also require a two thirds vote from state legislatures. The proposal would then need to be ratified. 

When asked about cutting government programs, including those that could be lifelines for Americans, Senator Lee took a strong stance, saying "no program will be held immune from review" and that Congress needs to consider "categorical across the board cuts" regardless of a program's perceived importance. 

If the U.S. government's budget functioned more like a typical American household, Lee says "the credit cards would have been "cut to pieces" a long time ago. While acknowledging that a serious reduction in spending would not necessarily help ease existing debt, Lee said its still time to start "paying off the bill. It may take time but we need to stop accumulating new debt." 

Speaking during his weekly internet and radio address over the weekend, President Obama called for bipartisanship among both parties in Congress, citing reduction of the budget deficit as a top priority.
