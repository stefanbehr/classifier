UPDATE: A Lieberman aide confirms first to FOX News that the senator will announce he is not running for re-election on Wednesday. 

"At this stage in his life - it's time for a new season and new chapter," the aide said. 

Lieberman intends to remain in public life and fight for causes in which he's been involved such as national security and the environment. 





Four term Sen. Joe Lieberman , I-Conn., is set to announce Wednesday whether or not he will become the third senator to retire in the 2012 election cycle, or if he will stay on for six more years. 

Marshall Wittman, a senior spokesman for the senator, would only say, "After many thoughtful conversations with family and friends over the past few months, Senator Lieberman made a decision about his future over the holidays which he plans to announce on Wednesday." 

The long-awaited announcement will come at 12:30pm EDT in Stamford. Fox plans to cover the announcement. 

Recently, Lieberman opened a small window into this thinking process, telling a local TV station, "I've got to decide at this stage of my life, as much as I like this job and feel privileged to be a senator, do I want to do it again, or do I want to try one more career chapter that's different," adding that he felt a combination of "a sense of excitement and also the natural anxiety, because you don't know what the next chapter will be." 

The senator has also openly toyed, in the past few months, with a change in parties, possibly even to the Republican Party , though many politcos consider that unlikely, as it would mean the loss of his chairmanship of the Homeland Security and Governmental Affairs Committee, a critical post in the middle of national security issues, a key Lieberman interest. 

There will likely be no shortage of Democrats seeking Lieberman's seat, whether he is in it not. Already, Connecticut's Secretary of State Susan Bysiewicz has announced a primary challenge. Rep. Chris Murphy, D-Conn., and millionaire cable executive Ned Lamont, who beat Lieberman in a 2006 Democratic primary, could also get into the race. 

Lieberman, once his party's vice presidential nominee, has had a troubled history with his former party though he still caucuses with Dems in the Senate. His strong support for the war in Iraq enraged the liberal wing of the party and brought the primary challenge from Lamont, which forced Lieberman to change his party I.D. in order to win re-election. And though many liberals were happy with the senator's successful effort to quash the Pentagon's "Don't Ask, Don't Tell" policy against gays serving openly in the military, many cannot forgive Lieberman's vociferous and steadfast support for the 2008 presidential candidacy of his close friend, Sen. John McCain , R-Ariz., against then-Sen. Barack Obama , D-Ill. As well, Lieberman's opposition to the so-called public option in health care reform was a bitter pill for many on the left who have long fought for this kind of government-run system. 

As for Republicans who might take a stab at the seat, there are reports that wrestling executive Linda McMahon could opt to return to the political ring, after losing her 2010 Senate challenge for the seat vacated by Sen. Chris Dodd , D-Conn. Also, one senior GOP aide suggested that millionaire Greenwich businessman Tom Foley, who very narrowly lost the governor's race in 2010, could also jump in. He originally ran for Senate in 2010, but the GOP field was crowded, and when the governor's seat opened up, he chose to make the change. 

Should Lieberman opt to leave Washington after 24 years of service, his retirement is not expected to tip the balance in the chamber, unlike the departure announced Tuesday by his colleague, Sen. Kent Conrad, D-ND. That race, according to the Cook Political Report's Jennifer Duffy, is a "tossup," but, she tweeted, "Democrats will need a star recruit to keep it there." North Dakota has increasingly gone to Republicans of late, though it has had a history of electing conservative Democrats to national office. 

UVA Center for Politics Director Larry Sabato tweeted of the Connecticut race, "If Sen. Joe Lieberman (ID-CT) retires'12, as reported, Senate tally won't change. D will be nominally favored in prez year to win open seat." 

In a sign that the road is wide open for a change of careers, Lieberman recently said his wife, Hadassah, whom he jokingly called "the Commander in Chief" of their household, has given him the green light. "She basically said, 'You do what you feel you want to do, and I'll be supportive.' ," Lieberman told WFSB, a Connecticut news station.
