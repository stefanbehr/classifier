After the politics and policy earlier at the White House on Wednesday, President Obama welcomed President Hu Jintao of China for what the White House is calling a "quintessentially American" evening and the third state dinner of the Obama administration. Jackie Chan and Vera Wang , as well as many other Chinese Americans of cultural influence will join the President in this evening's festivities. 

The Blue and Red rooms as well as the State Dining Room have been set up with large round tables covered in rich patterned jewel toned table clothes adorned with pheasants, the national bird of China. The White House says the first lady chose gold flatware and gilded utensils for each room and there are floral arrangements in pink, red, purple, yellow and orange. 

The meal, keeping in the American theme, was created by White House Executive Chef Cristeta Comerford and White House Pastry Chef William Yosses and is as follows: 



D' Anjou Pear Salad with Farmstead Goat Cheese Fennel, Black Walnuts, and White Balsamic 

Poach Maine Lobster, Orange glaze Carrots and black trumpet mushrooms 

Lemon Sorbet 

Dry Aged Rib Eye with Buttermilk Crisp Onions, Double Stuffed Potatoes and Creamed Spinach 

Old Fashioned Apple Pie with Vanilla Ice Cream. 

The East room will be the epicenter of the nights entertainment, with performance by Chris Botti, Dee Dee Bridgewater, Herbie Hancock , Dianne Reeves and world-renowned pianist Lang Lang. Lang Lang, the only Chinese performer of the evening, told Fox News' Peter Doocy the program will focus on American music, but he will perform a Chinese song "Pagoda" from Ravel's Mother Goose Suites. 

Additional performances and musical accompaniment will be provided by Theloniuous Monk Institute of Jazz.
