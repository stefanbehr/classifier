Sources inside and outside Iowa have helped Fox News identify five candidates who've agreed to appear at The Family Leader's series of "presidential lectures," scheduled to begin next month. 

Former Minnesota Governor Tim Pawlenty has accepted an invitation by the socially conservative organization to speak in the lecture series. Pawlenty has put in a lot of early work in Iowa, perhaps more than any other yet-to-officially-announce candidate. 

It's believed Pawlenty will lead off the lecture series next month. We are learning the other four to be announced Thursday will be former US House Speaker Newt Gingrich , former US Senator from Pennsylvania Rick Santorum , former Godfather's Pizza CEO Herman Cain , and former Arkansas Governor Mike Huckabee . The host of FNC's 'Huckabee' has tentatively accepted an invitation to speak, but this will be contingent on whether he actually enters the race. 



The Family Leader is led by Chief Executive Officer Bob Vander Plaats. His organization's efforts behind the removal of three Iowa Supreme Court justices through retention votes last November have made Vander Plaats a much more visible and influential figure in Iowa Republican politics. 



An official at The Family Leader says the lecture series will begin in February.
