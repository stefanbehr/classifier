Gruesome allegations surrounding what some are calling an abortion mill in Philadelphia are reverberating across Capitol Hill and could shape a new national debate on abortion laws. 

Doctor Kermit Gosnell is accused of using his Philadelphia clinic to murder one woman and abort at least seven late term babies. 

"In case after case, Dr. Gosnell and his assistants induced labor, forced the live birth of viable babies in the 6th, 7th and 8th month of pregnancy and then killed those babies by cutting into the back of their necks with scissors, severing their spinal cords," said Philadelphia District Attorney Seth Williams. 

A 261-page page grand jury indictment released this week against Gosnell and other clinic staffers lays out a myriad of failures and refusals to uphold basic public health guidelines. It also details a litany of regulatory failures by the Department of Health and other agencies. 

The damning document also includes an observation about the unity of mind among members of the grand jury. "We ourselves cover a spectrum of personal beliefs about the morality of abortion... We find common ground in exposing what happened here, and in recommending measures to prevent anything like this from ever happening again." 

However, in Washington the reaction is heated and not as of yet anywhere near common ground. "[Gosnell's] abortion mill was like many others," said New Jersey Representative Chris Smith (R), a staunch pro-lifer. 

Smith has introduced legislation called the No Taxpayer Funding For Abortion Act,' which would permanently ban all funding for elective abortion government-wide. Smith tells Fox News he has approximately 200 members of congress co-sponsoring the act. 

"Our hope is not that not only will we completely get federal funding out of this grisly abortion business... [But also] that Americans will take a second look at this violence against children and this exploitation against women called abortion." 

Most House Democrats were at a retreat in Maryland and not responsive to questions about Gosnell and his clinic. 

Meanwhile, Pro-choice advocates commented only on the GOP's legislative effort. 

"As these politicians take control of the House, they want to be able to interfere with our personal, private decisions, especially a woman's right to choose," said Naral Pro-Choice for America President Nancy Keenan. "They are out of touch with our country's values and priorities. What happened to the jobs agenda?" 

There is one political entity that clearly bears some blame for what happened at Gosnell's Philadelphia clinic. The prosecutor found that key employees in the State Department of Health could have shut its doors, but they stopped their inspections, allegedly to suit what they believed to be the preference of their pro-choice governors. 

It is worth noting that those governors were from both sides of the aisle over the years in question.
