President Obama continues his outreach to the business community with his latest appointment, selecting General Electric CEO Jeffrey Immelt to lead the new Council on Jobs and Competitiveness. 

U.S. Chamber of Commerce President Thomas Donohue praised Immelt's new roll in the administration in a statement, calling the announcement, "a promising step toward a renewed focus on creating jobs, boosting economic growth, and enhancing America's global competitiveness." 

Donohue also lauded Immelt's business resume, saying, "Jeff is an excellent choice to lead this effort with his business experience and acumen, and understanding of global markets." 

The new Council on Jobs and Competitiveness replaces the President's Economic Recovery Advisory Board (PERAB), which dissolves next month. But by signing the executive order on the new council, Donohue says President Obama still has to create results. 

While Donohue looks forward to working with Immelt in his new role, he doesn't want government intervention to stall an economic turnaround. 

"A more robust economy and stronger job growth will require regulatory restraint and reform, trade expansion, reducing deficits, reining in spending, strengthening our education system, and rebuilding our nation's infrastructure."
