During a ceremony in Washington celebrating 50 years since her father became attorney general, the eldest daughter of Robert F. Kennedy insisted the lack of a federal assault weapons ban played a role in the recent shootings in Arizona, which left six dead and 13 others, including Congresswoman Gabrielle Giffords, wounded. 

A ban on the production of certain semi-automatic firearms became law in 1994, but it expired seven years ago and efforts to renew it have since been unsuccessful. 

"If that statute were still in place, the Tucson shooter would not have had such easy access to the guns or easy ability to keep running his deadly errands," Kathleen Kennedy Townsend told those gathered Friday afternoon inside the Robert F. Kennedy Department of Justice Building in Washington. 

However, 22-year-old Jared Loughner allegedly used a 9mm handgun when he opened fire outside a Tucson supermarket on Jan. 8. He had purchased the gun, with an extended magazine, in November 2010. The morning of the shooting, Loughner allegedly had trouble finding a store in Tucson that would sell him ammunition for the gun, but he ultimately obtained what he wanted from a Super Walmart, according to local police. 

Still, Townsend said she believes the Justice Department and "this country have got to do a better job on gun regulation and on gun control and making our citizens safe." 

"As my father said, we glorify killing on movies and on television screens and call it entertainment," she said. "We make it easy for men of all shades and sanity to acquire weapons, and violence breeds violence. Repression brings retaliation, and only a cleaning of our whole society can remove this sickness from our soul." 

Some in the crowd clapped during the first part of the remarks, but the room sat still and silent once Kathleen Kennedy Townsend finished them. 

Her father and her uncle were both gunned down in the peak of their political careers. Robert F. Kennedy was the nation's 64th attorney general, sworn-in on Jan. 21, 1961. But seven years later, after becoming a U.S. senator, he was killed while running for the White House . His brother, President John F. Kennedy, had been killed five years earlier. 

Kathleen Kennedy Townsend's remarks came on the same day that Giffords, a Democrat from Arizona, was transferred from a hospital in Tucson to a rehabilitation center in Houston, where she is expect to undergo weeks of therapy. 

On Wednesday, Loughner was indicted on three initial counts for attempting to kill Giffords and two of her aides, Ron Barber and Pamela Simon. While authorities have yet to offer a motive in the case, a history of possible mental illness, including sometimes incomprehensible rants and videos posted online, has emerged in the days since the shooting. Before Townsend spoke on Friday, Attorney General Eric Holder praised Robert F. Kennedy's efforts as the 64th attorney general, particularly his work on civil rights. But, Holder said, "Despite all that's been accomplished in recent decades, we still do not live in tranquil times." 

In a pre-recorded video message, President Barack Obama echoed those sentiments, urging people to draw inspiration from Kennedy's legacy. 

"In a violent time that revealed man's capacity to do harm, he never lost faith in our capacity to love," the president said. "He never lost his sense of possibility, his belief that we -- every single one of us -- can narrow the gap between the world as it is, and the world as it might be. ... Let's [now] refuse to accept things as they are." 

Four days earlier, while honoring Martin Luther King Jr. at a church in Atlanta, Holder said the "senseless rampage in Tucson, Arizona, reminded us that, more than 40 years after Dr. King s own tragic death, our long struggle to end suffering and to eradicate violence goes on." 

But, Holder said, "once more, we can see the stars." 

"We see them in the courage of a husband who died shielding his wife from a spray of bullets, in the kindness of strangers who raced to the aid of those in need, and in the strength of a young, energetic Congresswoman whose fight for her own life has inspired each of ours," Holder said. "As we continue to mourn those recently lost, and to pray for those now in need of healing and comfort, let us also recommit ourselves to carrying on Dr. King's work and to honoring the values that were at the center of his life: tolerance, non-violence, compassion, love, and -- above all -- justice." 

Kathleen Kennedy Townsend worked in the Justice Department during the Clinton administration and then served as Maryland's lieutenant governor from 1995-2003.
