President Obama is doing a YouTube interview Thursday afternoon -- opening up the door for America to ask about any of the pressing issues America is facing -- the economic crisis, rising gas prices, the escalating deficit, pay freezes, the State of the Union speech, working with Republicans...or pot. 

The White House set up the interview session to be about his State of the Union address, and of the thousands of words the president uttered, marijuana was not among them. 

Apparently, according to UPI who reviewed the nearly 140,000 questions submitted, the questions most posted for the commander-in-chief dealt with legalizing marijuana and drugs. 

"The top 10 questions all involved ending or changing the government's war on drugs, legalizing or decriminalizing marijuana and embracing industrial hemp as a "green" initiative to help farmers...," UPI reported. 

There were 193,069 people who submitted 139,619 questions (the time for submitting has now ended), according to the latest on the website . YouTube designed the interview so that the online community votes for which questions get submitted. As of Thursday lunchtime in the East Coast 1,379,126 votes had been cast. 

Drug policy reform groups have already started weighing in. In a press release an alliance of several groups said, " We are encouraged by the grassroots response bubbling up around this issue and urge President Obama to address this issue seriously and thoroughly." 

Of course there are a broad range of questions people are asking - including some about energy, gardening, pollution and everything and anything in between. 

The official Web page for the interview says that Obama "will answer a selection of the most popular" ones, meaning he likely won't get peppered with a bunch of questions about drugs. 

YouTube news and politics chief Steve Grove will moderate the interview. 

Fox News' Mike Emanuel contributed to this report.
