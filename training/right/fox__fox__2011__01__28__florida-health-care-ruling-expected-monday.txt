MIAMI - The fourth federal judge to rule on President Obama's health care law is expected to issue his decision from Florida Monday. 

U.S. District Judge Roger Vinson's ruling will decide the biggest of the many court challenges to Obama's hallmark accomplishment, which now has 26 state attorneys general backing the suit. 

Opponents of the Affordable Care Act specifically target two key aspects of the law. First, that the "individual mandate," which requires every American have health insurance (or suffer a tax penalty) exceeds the constitutional powers of Congress and violates the Commerce Clause. Secondly, that forcing the states to expand their Medicaid programs unconstitutionally infringes on states' rights. 

So far, it's 2-1 in favor of the president and the last Congress. A U.S. District Judge in Michigan and another in Lynchburg, Virginia heard essentially the same arguments and ruled that the law does pass constitutional muster. A U.S. District Judge in Richmond, Virginia ruled it does not, and is unconstitutional. 

November's midterm elections grew the Florida suit. More Republican AG's were elected, and six of them joined the 20 others who had already jumped on board the initial challenge, filed by then-Florida Attorney General Bill McCollum. He filed it almost immediately after President Obama signed the bill last year. The National Federation of Independent Business also joined the suit. 

Whatever Monday's ruling is, the loser is expected to file an appeal in the U.S. Court of Appeals in Atlanta. But ultimately, all of these rulings are just turns in the road as the Affordable Care Act will likely head to the U.S. Supreme Court .
