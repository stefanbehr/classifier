Though there are no events formally scheduled to discuss the situation in Egypt, the deteriorating social order in the Arab nation will be a subject of debate throughout the week on the Senate floor and elsewhere on Capitol Hill . 

Crude oil futures are on the rise amid concerns about possible disruptions to tanker shipments through Egypt's Suez Canal . Could increased oil prices spur the development of a new energy policy in Congress? Senate Energy and Natural Resources Committee Chairman Jeff Bingaman , D-N.M., discusses that possibility at a 12:00 p.m. ET news conference. 

Upon returning Monday afternoon, the Senate will debate a bill to fund the Federal Aviation Administration. Among other things, the measure would implement a new air traffic control system using GPS satellites rather than the current radar-based system. 

Senate Democrats also have tentative plans to introduce a small business bill later in the week. 

There's plenty more news ahead, so stay with Fox for all the latest.
