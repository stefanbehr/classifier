The controversial health care repeal vote is scheduled to occur at approximately 5 or 6p.m. EST, Senate Majority Leader Harry Reid (D-N.V.) said on the Senate floor Wednesday morning. 

The Republican-controlled House of Representatives voted to repeal the Affordable Care Act in January, but its prospects of passing in the Senate are less likely - and Senate Minority Leader Mitch McConnell on Wednesday morning indicated that the vote will not succeed. 

"I mean, if we had the votes to do it, we would obviously repeal it and replace it with more modest, targeted reforms that would actually bring the cost of health care down," McConnell told Bill Hemmer in an appearance on Fox News when asked what his next move would be. He added that Republicans would advocate for replacement provisions like medical malpractice reform and interstate insurance competition. 

"It's not often that you get a second chance to correct a mistake, and we're hoping that a number of our Democratic friends will realize...that this was the wrong thing to do," he said. "And so they've got a second chance today, and we're proud to give it to them, and hope some of them will support it." 

As for whether bringing repeal to a vote when it is unlikely to pass is merely symbolic, McConnell says, "It's not a waste of time. We promised the American people we would try to repeal this job-crushing Washington takeover of our health care, and we're keeping that commitment. The House has voted to repeal it, and we'll have that vote in the Senate today." 

Senior Capitol Hill Producer Trish Turner contributed to this report.
