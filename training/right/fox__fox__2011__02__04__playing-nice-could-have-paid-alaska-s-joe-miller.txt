UPDATE 8:35pm - The Joe Miller campaign offered the following response to Fox News late Friday, from spokesman Randy DeSoto (hyperlinks as original): 

"The Senator's and Mr. Miller's memories of the conversation are not reconcilable. She did not congratulate him. Moreover, there is evidence that even before that conversation, the Senator had already retained consultants during the week between primary election day and when she conceded and made decisions suggesting that she would be a candidate in the general election. Finally, Mr. Miller issued a press release (on the very day that Murkowski claims the conversation took place), praising Murkowski for her then-honorable actions. Mr. Miller also made several similar public statements in interviews during this same time frame. 

"Remember, the Senator originally told the press that she decided to go back on her word (on August 20, four days before primary election day) to support the nominee, because the Miller campaign had made unfair attacks on her after this commitment was made. The Miller campaign pointed out that we made no changes in the substance of public statements and no significant advertising changes after that date." 



Did former Alaska Senate hopeful Joe Miller get schooled on being a good sport? That's the gist from Senator Lisa Murkowski , who suggests she may not have launched her historic - and ultimately successful -- write-in campaign if Miller had simply been more polite after the primary. 

In a recent interview that ran on the Huffington Post , Dan Rather recounts, "As for Joe Miller , Murkowski told us she might not have challenged him at all had he been more gracious during her concession call." 

Miller won the Alaska Republican primary. But whatever the private motive, Murkowski jumped back into the ring as a third-party candidate for the November general election, saying publically it was the right thing for her state. After a bitterly divisive recount that saw Congressional Republicans in Washington side with the Tea Party favorite as the rightful winner, Murkowski was vindicated. Miller conceded the contentious race December 31st. after unsuccessfully taking his election challenges to the courts. 

"Another lesson for the campaign trail," Rather notes, "that anyone's mother could have passed along for free."
