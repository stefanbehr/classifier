For years, Americans have sent mixed messages to lawmakers in Washington. 

Please lower taxes. Please slash spending. Please maintain Social Security, Medicare and Medicaid. And for goodness sakes, get us out of debt. 

It's hard to do all of that at once. 

Reducing spending and fixing the deficit emerged as the mantra of the 2010 midterm elections, amplified by the tea party. And Republicans know they had better have something to show for it in the fall of 2012. For nearly two years, House Speaker John Boehner (R-OH) declared that the GOP had "lost its way" when it came to spending. And as a result, voters showed them the door. 

Republicans now have an opportunity to slash that spending. 

They passed a resolution in the House last week to return federal spending to 2008 levels. And next week, they'll approve another one establishing a ceiling for cuts. 

House Appropriations Committee Chairman Hal Rogers (R-KY) says that no agency or district will be immune. So let's look at the table...and the cutlery...to inspect those potential cuts and what they mean to voters back home. To say nothing of the fortunes of lawmakers dispatched to Capitol Hill , practically armed with torches and pitchforks. 

Lawmakers anxious to trim spending often threaten to wean the NASA budget. 

Reducing NASA's $18.7 billion budget to 2008 levels would eliminate about $1.4 billion from the space agency. 

$1.4 billion is a fractional cut worthy of a decimal point and a lot of zeroes when calculated as a percentage of the government's $3.8 trillion budget. 

Few states witnessed such a massive overhaul in Congressional personnel as Florida. The Sunshine State is the home to the Kennedy Space Center and the adjacent space industry. Voters elected Sen. Marco Rubio (R-FL) and installed eight other freshmen House members. Freshman Rep. Sandy Adams (R-FL) and sophomore Congressman Bill Posey (R-FL) represent the districts that encompass much of what's known as Florida's "Space Coast." 



It will be interesting to see how these lawmakers vote when it comes to NASA funding, because, as they say, all politics is local. 

NASA's situation could be even more problematic in northern Alabama. 

Rep. Mo Brooks (R-AL) is a Washington newcomer. Brooks represents Huntsville's Marshall Space Flight Center and scored a seat on the House Science Committee that oversees NASA. Brooks is on record as pursuing that very committee assignment so he could protect jobs related to NASA in northern Alabama. But NASA is an agency whose budget is often on the chopping block. 

Brooks has already declared that NASA is an agency that adds value to America. It could create a tough vote for Brooks who was sent to Washington with the "cut spending" edict. Does Brooks vote to support his district? Or does he vote to cut NASA as part of a broader spending reduction effort? Or can he do both? 

It's easy to demand spending reductions for NASA when your ox isn't being gored. Especially if you don't represent Florida or Alabama and come instead from Kansas or Iowa or Nebraska where NASA is of little direct importance. 

But something else is important in those regions: farm programs. 

In 1996, the upstart, GOP-led Congress led an effort to get farmers off crop subsidies by passing the Freedom to Farm Act. The law aimed to eliminate the federal government's role in farming and instead send farmers direct payments that weren't contingent on commodity production. The plan was to transition farmers off the federal dole after five years. But 15 years later, farmers are still reaping the benefits sown on the federal pasture. 

Federal farm programs have been around since the New Deal. Back then, farmers earned half of what most other Americans made. But today, farmers make about a third more than those who don't farm. 

And of course, many of those federal efforts were designed to salvage the romantic vision of the family farm. That may not have helped much because there are exponentially fewer farmers today and agribusiness dominates the industry. 

Want to chop agriculture subsidies? That $13 billion slice will save taxpayers very little in that nearly $4 trillion budget. And taking a knife to farm programs is a risky proposition for any politician. 

In February, 1979, some 6,000 farmers drove their tractors to Washington to protest President Carter's farm policies. The images of thousands of John Deere, Massey Ferguson and Farmall Cub tractors wheeling up and down Pennsylvania Avenue created a great tele-narrative for the news and the farmers won the day. 

But the bulk of federal agriculture spending is devoted to food stamps and child nutrition programs. That visual isn't as ripe for the TV cameras. And of course, most of that assistance goes to districts represented by liberal, urban Democrats who are no longer in charge. If lawmakers do start cutting agriculture spending, do they slash those food assistance programs that often benefit the constituents of liberal lawmakers or do they take an ax to farmers in rural America, who are now represented by conservative Republicans. 

Of course, farm program advocates invoke old chestnuts that warn against cutting food production. They argue that producing inexpensive food is critical to national security and the U.S. needs to help feed the world when the global population tops 9 billion in less than 30 years. They assert that superior food production helped the U.S. military win World War II. 

But again, it's hard to have things both ways. True cuts would mean taking a chainsaw to farm programs. And that won't go over very well back home in the ag heavy districts represented by freshman Reps. Rick Berg (R-ND) and Kristi Noem (R-SD). 

Two years ago, Rep. Barney Frank (D-MA) called for substantial reductions in the defense budget. Then Defense Secretary Robert Gates announced plans to shave $78 billion from the Pentagon budget. And now, even some tea party loyalists are seeking to trim defense dollars. 

Again, when it comes to the military, freshman Republicans could find rough sailing ahead. Rep. Vicky Hartzler (R-MO) unseated House Armed Services Committee Chairman Ike Skelton (D-MO) last fall. A fiscal conservative, Hartzler wants to pare the federal budget. But the Congresswoman also inherits a defense-centric district from Skelton which includes Whiteman Air Force Base and Fort Leonard Wood. 

It's no surprise that Hartzler is leery of defense cuts, arguing they aren't appropriate in wartime. 

Rep. Scott Rigell (R-VA) is another freshman who also wants to reduce the size of government. But Rigell represents the military-infused Hampton Roads and Norfolk areas in southern Virginia, home to the Oceana Naval Air Station. 

It's said that all politics is local. Yet there is a national demand to prune government spending. Hartzler and Rigell are two freshmen who could face pressure back home if voters don't believe they supported their districts. (read, jobs). The trick will be to walk a tightrope and vote for spending cuts while preserving defense interests vital to the economic engines of their communities. 

And then there are tax cuts. 

President Obama blinked late last year and accepted the GOP demand to renew the Bush-era tax cuts. Freshman House Republicans are adamant they want serious debt reduction and are loathe to approve an increase in the amount of debt the government is allowed to incur. Proponents argued that renewing the tax cuts would help reinvigorate a stagnant American economy. But it's hard to have things both ways. The Congressional Budget Office (CBO) said reauthorizing the tax cuts through 2020 would heap an additional $3.3 trillion onto the debt. That's to say nothing of nearly three-quarters of a trillion dollars that the U.S. has to cough up in debt service alone. 

Lower taxes usually result in increased federal revenues. But this formula shows that it's much harder for the U.S. to simply cut spending to balance the budget. Diminished taxes have consequences too. And no freshman Republican (and barely any Democrat who survived November's election debacle) is advocating tax increases to help yank the U.S. out of the fiscal ditch. 

In the coming months, look for lots of cuts that target the perennial triumvirate of "waste, fraud and abuse." No one from either party will defend government inefficiency. But streamlining government can only go so far when the mountain of debt the U.S. faces tops $14 trillion. 

So what's next? Some political insiders argue that the House GOP leadership will move to "protect" freshman lawmakers who could be forced to take challenging votes to match their campaign rhetoric but out of line with their district. There is chatter that the Republican brass could "engineer" the votes in such a way so that veteran lawmakers from safe districts would carry the party's water and give freshmen from swing districts a pass so they can vote in favor of local interests. There are nearly 90 GOP freshmen in this class. So they make up a sizeable chunk of the Republican membership. Which means Republicans have limited wherewithal to inoculate them from damaging votes. 

There will be some serious bloodletting if Republicans prove to be serious about fiscal discipline. That means cuts that could sting back home. 

And if voters balk at these cuts, they will have again given lawmakers mixed messages.
