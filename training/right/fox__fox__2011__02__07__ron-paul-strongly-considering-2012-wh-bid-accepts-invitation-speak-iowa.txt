Texas Congressman Ron Paul "is strongly considering a run [for president] in 2012 and is currently assessing his prospects," according to Jesse Benton, Senior Advisor to Rep. Paul's Campaign for Liberty organization. This news comes on a day when we've also found out Paul has accepted an invitation to speak in Iowa next month. 

The Family Leader, a new conservative activist organization, sent out a news release this morning announcing Paul would participate in the organization's "presidential lecture series." 

The organization has been reaching out to potential 2012 candidates and inviting them to Iowa to give voters in the Hawkeye State "an educational opportunity to become better informed on the pro-family vision of each lecturer." 

The series kicks off today with former Minnesota Governor Tim Pawlenty , who is widely considered to be preparing for a run at the 2012 GOP presidential nomination. Other possible candidates who've committed to speak at events with the group in the coming months are Rep. Michele Bachman, former US Senator Rick Santorum, former Speaker of the US House Newt Gingrich and radio talk-show host Herman Cain. 

Paul has run unsuccessfully for president twice. Once as the Liberterian Party nominee in 1988 and as a Republican in 2008. The 11-term Congressman's son, Rand, was elected by Kentucky last fall to represent them in the US Senate.
