House Majority Leader Eric Cantor (R-VA) says he's looking forward to lunching Wednesday at the White House with President Obama and that he's "appreciative of the invitation." 

House Speaker John Boehner (R-OH) and Majority Whip Kevin McCarthy (R-CA) will join Cantor at the White House for the luncheon. Cantor suggested that Mr. Obama may willing to work with Republicans on some issues. 

"Let's not forget what happened November 2nd. It was an absolute repudiation of what has gone on in Washington the past few years," Cantor said. "I think the president got that when in the lame duck session he went along with the tax deal. I'm hopeful it is in that spirit that the president has this lunch tomorrow." 

Cantor said he hoped to discuss government spending and efforts to create jobs with President Obama. But the Virginia Republican said he would defer to Mr. Obama when it came to his administration's handling of the crisis in Egypt . 

"I don't think it's helpful for the president to have 535 members opine on foreign policy," said Cantor. "I think the primary goal should be to stop the spread of radical Islam."
