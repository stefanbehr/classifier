As three different pro-life, GOP-sponsored measures wind through the House, pro-choice Democrats are firing back, calling the bills "extremely dangerous." 

The No Taxpayer Funding for Abortion Act, sponsored by Rep. Chris Smith (R-N.J.), would take away tax benefits for employers who provide health care if the plans offer abortion coverage. The Protect Life Act, introduced by Rep. Joe Pitts (R-Pa.), is aimed at those who would get insurance through state exchanges. It would block those consumers from purchasing abortion coverage. 

Wednesday, Rep. Louise Slaughter (D-NY) called the measures "draconian" and "despicable," and warned that they would allow hospitals to deny life-saving abortions to dying women. Slaughter was joined by a number of pro-choice Democrats who voiced similar concerns. 

"The Republican leadership has prioritized divisive and extreme legislation, and we will just not stand for it," said Rep. Diana DeGette (D-Colo.), adding, "We don't think the American people will either." 

A third GOP bill, backed by Rep. Mike Pence (R-Ind.), aims to block health clinics, specifically Planned Parenthood, from receiving federal funding if they perform abortions. Pence says the American public is "learning that their taxpayer dollars are subsidizing these organizations that are promoting abortion across this country." 

DeGette was among the Democrats Wednesday asking why Republicans are more focused on defunding Planned Parenthood than on creating jobs. 

"It's getting in the way of how we need to move our country forward with job creation," said DeGette. 

Rep. Jerry Nadler (D-N.Y.) argued Wednesday that all three bills run counter to a core GOP principle. Nadler says Republicans have lectured Democrats for years about "the overreach of government into people's private lives." He believes the measures they're now offering are "the height of hypocrisy." 

Nadler argues that Republicans "would have the federal government intervene in the private and personal medical decisions of women." 

Republicans say their intention is not to block access to abortions, but to make sure that taxpayers aren't forced to fund them. 

The battle on the Hill is playing out as Ohio considers what could become the most restrictive law in the nation: no abortions permitted once a heartbeat is detected, as early as 18 days after conception. 

State Representative Lynn Wachtmann (R) formally introduced the bill Wednesday. He has also been criticized for prioritizing an abortion measure over economic bills. Wachtmann responded today that he will "always put life first."
