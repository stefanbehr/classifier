House Appropriations Committee Chairman Hal Rogers (R-KY) announced the first batch of proposed spending cuts that lawmakers will try to approve next week. 

Rogers is taking a knife to a temporary spending resolution that funds the federal government through March 4. Under Rogers plan, the bill would slash money for some 70 programs. 

The cuts come in various shapes and sizes. Low end reductions include the elimination of $2 million for the Minority Business Development Agency and $6 million for the National Endowment for the Arts. Higher end cuts focus on chopping $1.6 billion for the Environmental Protection Agency and $1.7 billion for the General Services Administration Federal Buildings Fund. 

Money for the Corporation for Public Broadcasting and AmeriCorps is eliminated in Rogers blueprint, which he bills as the first round of proposed cuts. 

"Never before has Congress undertaken a task of this magnitude," said Rogers in a statement. "The cuts in this CR will represent the largest reduction in discretionary spending in the history of our nation." 

Speaking before Rogers released the list, House Minority Whip Steny Hoyer (D-MD) was skeptical of GOP efforts to truly reduce deficit spending. 

"I think there's a lot of message and not a lot of substance," said Hoyer. "Substance is tough." 

For his part, Rogers argued that the cuts were not what he called "low-hanging fruit," noting that the cuts "are real." 

There are alternative ways to calculate as to the total dollar figure in the cuts. One price tag is $58 billion, compared to what President Obama proposed to spend on domestic programs. The figure rises to around $74 billion once defense reductions are considered.
