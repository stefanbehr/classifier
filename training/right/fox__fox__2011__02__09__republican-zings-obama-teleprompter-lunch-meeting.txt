Some barbs are already flying before the meeting between House Republican leaders and President Obama at the White House this afternoon. 

House GOP leadership has said their lunch meeting with the president will focus on cutting spending and eliminating regulatory burdens for business. 

However, Republican House Whip Kevin McCarthy of California had these choice words, "the teleprompter won't be there and we will have a very frank conversation about spending." 

That should give them something to chew on.
