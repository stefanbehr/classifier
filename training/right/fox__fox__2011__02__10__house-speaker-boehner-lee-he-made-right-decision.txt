House Speaker John Boehner (R-Oh) addressed Rep. Chris Lee's (R-NY) sudden resignation, telling reporters Thursday, "I think he made the right decision for himself and his family." 

Boehner would not amplify his comment last summer to Roll Call that he told some members to "cool it" when it came to their behavior with lobbyists. Lee was believed to be one of the members who Boehner had concern with, but Thursday Boehner would only offer, "My conversations with members are private and will remain that way." 

The 46-year-old Lee resigned Wednesday after the gossip website Gawker disclosed that the married lawmaker had e-mailed a picture of himself not wearing a shirt to a woman he met through Craigslist. 

"I regret the harm that my actions have caused my family, my staff and my constituents," he said in a statement, saying he "made profound mistakes." 

Boehner insisted to Fox News that he did not speak with Lee Wednesday nor persuade him to step down. 

The Speaker did however say, "I believe that members of Congress should be held to the highest possible standards."
