The race for the Republican presidential nomination in 2012 is wide open. 

So open, in fact, that billionaire real estate developer Donald Trump is once again flirting with a run. 

Trump will step further into the ring than ever before Thursday by speaking at one of the largest conservative conferences in the country. The Donald is a last minute addition at CPAC and will appear in a lineup with other potential 2012 candidates before a crowd of nearly 10,000. 

The reality television host has been talking to friends and associates about the possibility of mounting a run for months and has said as much during multiple cable news appearances. 

Trump has floated the idea of running for president at least three times since the 1980's.
