White House Press Secretary Robert Gibbs held his last briefing at the White House on Friday, his 250th since taking the job in January 2009. Even though the news out of Egypt dominated the day, President Obama did not let the occasion go unnoticed, stopping by the White House briefing room to talk about one of his oldest and fondest memories of Gibbs - the story of the 2004 Democratic convention and a tie. 



Watch the latest video at FoxNews.com
