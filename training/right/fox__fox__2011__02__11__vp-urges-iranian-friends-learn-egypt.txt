Friday Vice President Joe Biden was the first person from the administration to comment on the resignation of Hosni Mubarak in Egypt , calling it "a pivotal moment in history." 

While delivering remarks at the University of Louisville in Kentucky alongside Senate Minority Leader Mitch McConnell , the VP issued a blatant warning to Iran - stressing the importance of learning from this moment in Egypt . 

"The Iranians trying to take advantage of the situation in Europe have only exposed the bankruptcy of their system. I say to our Iranian friends: let your people march, let your people speak, release your people from jail, let them have a voice!" 

Biden's comments were met by loud applause from the crowd. 

Friday happens to mark the 32nd anniversary of the Islamic Revolution. Tens of thousands gathered in the capitol's main square to celebrate.
