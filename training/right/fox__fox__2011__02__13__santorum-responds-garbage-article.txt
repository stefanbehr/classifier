Former Senator Rick Santorum insists there's no bad blood between him and former Alaska Governor Sarah Palin . 

During a Sunday interview with Fox News, Santorum claimed the controversy was just "another attempt to try to pit Republicans against each other by the mainstream media." 

The "attempt" that Santorum is referring to is a Politico article titled, " Rick Santorum : Sarah skips CPAC for C-Notes." In it, the former Pennsylvania senator is quoted as saying "I have a feeling that she has some demands on her time, and a lot of them have financial benefit attached to them." 

Santorum blasted the item on Twitter , saying, "This article is garbage. All I said was- she is VERY busy, PERIOD. Reporter trying to create something out of nothing." 

Santorum tells Fox News he has touched base with Palin and "She's fine, I'm fine." 

When asked if he's "kissed and made up" with the fellow Fox News Contributor, Santorum was quick to say, "I'm not going to go to kissed and made up'. But we're okay." 

The former senator is in New Hampshire this weekend, fuelling speculation that he is considering a 2012 presidential run. 

Santorum says the decision will come in the next couple of months, but added that he realizes the clock is ticking. 

"Things will start to get serious and the Iowa straw poll is in August and that's sort of really the first milestone, I think, in the campaign season," Santorum told Fox News. "So, we'll want to be at least, you know, in the game for that and be able to prepare somewhat."
