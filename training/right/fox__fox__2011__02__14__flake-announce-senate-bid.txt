Rep. Jeff Flake, R-Ariz., will make a bid for the Senate seat soon to be vacated by retiring Sen. Minority Whip John Kyl (R), the Arizona Republican announced Monday. 

"As soon as possible, as the congressional schedule allows, Sheryl and I will get the kids...and do a statewide campaign," Flake said at the same Phoenix hotel where three-term Sen. Kyl announced his retirement days ago. 

In a statement released shortly after the announcement, National Republican Congressional Committee Chairman Rep. Pete Sessions called Flake a "stalwart fiscal conservative." The fiscally conservative Club for Growth also followed the press conference with a glowing endorsement. 

And the six-term congressman, known for being a budget hawk, was quick to lay down his priorities. "Of immediate concern to Arizona, of course, is runaway federal government spending. This is a crisis. It presents a clear and present danger to Arizona's families and to the country as a whole," he said. 

With the president's budget released Monday, Flake said he was heading to Washington directly after his press conference to reengage in the deficit debate. 

"We're going to have to get a lot more serious than the president's budget reflects," he said, stressing that the federal government needs to tackle dreaded entitlement reform before it heads over a "fiscal cliff." 

"We're going to have to put everything on the table. Whether it's the retirement age, recalculating benefits, means testing, you name it," he said. "That's the price we have to pay. If it means losing an election, you do that." 

Former Arizona governor and Department of Homeland Security Secretary Janet Napolitano and Rep. Gabrielle Giffords, D-Ariz., who is still recovering from injuries sustained in the Tucson shooting are also thought to be possible contenders for the seat.
