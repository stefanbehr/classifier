The budget President Obama submitted Monday to Congress was conspicuously absent many of the recommendations from his own debt commission, but the president sees no irony. He told reporters in a news conference Tuesday that he agrees with much of the commission's framework, laid out in December, and that he disagrees with some of it. He said, however, that just because something doesn't happen immediately doesn't mean it's never going to happen. 

The president put the National Commission on Fiscal Responsibility and Reform together to advise him on the best ways to decrease the nation's $1.4 trillion deficit. 

Mr. Obama told reporters that carrying out the commission's recommendations will be a process of negotiation that will involve both parties in Congress so that the result can be legislation that has a fighting chance to pass. His goal, he said, is to solve the problem, not get a "good headline" on the first day.
