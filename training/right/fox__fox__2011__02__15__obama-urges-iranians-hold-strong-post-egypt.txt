President Obama's approach to Iran differs from how he has dealt with the tumult in Egypt , but Mr. Obama says the fundamentals remain the same. "[P]eople should be able to express their grievances," the president said. The Middle East has been undergoing a wave of protests and unrest over recent months, resulting in a change of leadership in some cases and, protesters hope, more democracy and stability. 

The turmoil in Egypt has gotten the most attention by the American media and tested President Obama's leadership. Turning his attention to Iran in a news conference Tuesday, the president asserted what has been different in the situation with Iran is the Iranian government's response: "to shoot and arrest people," he said. 

Still, the president encouraged the Iranian people to hold strong and to continue to express their "yearning for a more representative government."
