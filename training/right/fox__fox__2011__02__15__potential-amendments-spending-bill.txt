This is just a taste of some of the amendments that could hit the House floor as a part of the spending bill that will be up for debate over the next several days. 

A total of 403 amendments were filed last night. Republicans wrote 262. Democrats crafted 141.A majority of the amendments (285) come at the end of the bill because of the way the legislation is structured. It's hard to get an amendment in unless it's a broad amendment to trim the bill of an overall program. Which is why many of those come at the end of the bill where it's a little easier to make them germane from a parliamentary perspective. 

One amendment even tries to eliminate funding for a teleprompter for the president. 

Here are a few interesting ones: 

Rep. Randy Neugebauer (R-TX): No funds to repair, alter or improve the Executive Residence of the White House 

Naugebauer: Cuts the funding for alterations to the White House and adds it to deficit reduction 

Steve Womack: No funds for a teleprompter for the President 

Rep. Huelskamp: eliminating money for a "tsar" to oversee the closure of Gitmo.
