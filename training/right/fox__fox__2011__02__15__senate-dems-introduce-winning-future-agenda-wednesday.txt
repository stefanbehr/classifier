Senate Democrats are expected to unveil their jobs and competitiveness agenda entitled "Winning the Future" on Wednesday, Fox has learned, a title and set of legislative proposals modeled, according to one senior Senate Democratic leadership aide, on the president's January State of the Union address. 

President Obama, in that speech, called on Americans to turn their competitive spirit outward toward a common goal of beating other nations like China and India in a rapidly changing world and global, interconnected economy, calling for an American "Sputnik moment." 

At a morning news conference, Democratic leaders will introduce a series of measures to help Americans "out-innovate, out-educate, and out-build the rest of the world," the aide said, quoting Obama. 

In 2010, Democrats introduced an agenda focused solely on job creation, but this time around, the leadership aide said, Democrats want to focus on "enhancing America's competitiveness," with proposals like the expansion of broadband, something the president put in his current budget introduced Monday, with a goal of wiring every U.S. household for the Internet. 

Republicans are likely to have a similar reaction to this agenda as they did to the president's annual address to Congress, decrying the spending inherent in the proposals at a time when the GOP is more focused on drastic cuts in current spending levels and a steep reduction in the debt and deficit.
