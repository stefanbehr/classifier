This is the exact quote I got a bit ago from a senior House Republican aide: 

"We're gonna be here for a looooooooooooooooooooooooooooooooooooooooooooooooooooooooooong time." 

That's 59 "o's" in "long." 

Which, in Congress-speak, could apparently mean Saturday. Which is actively being talked about here now as a legitimate possibility. 

I was just told by a senior GOP House leadership aide that it would take them 24 hours to whittle away all of the Republican amendments alone and at least 10 hours to to through the Democratic amendments. Add a few hours of fudge time onto that. 



The Appropriations folks are imploring House GOP leaders to try to talk to rank-and-file members to dial back amendments. But I am told by one senior aide "that's not going so well." 

There is an attempt to take a page out of the Senate's playbook here and peel back the number of amendments and get a time agreement through "unanimous consent" (known here as obtaining UC). That would then save time and allow everyone to know the size of the universe they're dealing with. But under the current operating structure, any member can speak for up to five minutes on ANY amendment being offered. And when you still have about 100 amendments on the table, that chews up a lot of time. 

I was told Wednesday night they were going to get a UC agreement. But it never came. And here we are at 2:25 pm in the afternoon and there is no deal. 



The original plan was to finish this legislation by 3 pm Thursday. 

So, at a minimum, expect a late night Thursday, going well after midnight (they got out close to 4 am Thursday morning) and burning well through Friday and potentially into Saturday. 



There are several big ticket items that are still in the mix: restoring funding to public broadcasting, cutting money for the health care law (which will be a battle royale unto itself) and a plan to build a firewall between the federal government and Planned Parenthood.
