People come to Washington to visit their congressman for many reasons. To say hello, to thank them for supporting legislation that addresses their needs, and to request funds for projects back home. 

One new member wants folks to know that steering dollars back home isn't in his job description, and to let folks down easy, installed a large sign above the reception area of his office. 

It reads, "If you are here to ask for more money, you're in the wrong office!"Benishek doesn't think his constituents mind either. 

"Back in Michigan everyone I talk to when going back to the district says we gotta cut spending," Benishek says, "Everyone here says don't cut spending for me. We need to cut spending." 

Benishek isn't completely opposed to government spending though, "If we have the money, all these programs are great," he said, "But there's no money." 

The Michigan freshman feels getting spending under control will improve the economy, which will allow for more government investment in the future.
