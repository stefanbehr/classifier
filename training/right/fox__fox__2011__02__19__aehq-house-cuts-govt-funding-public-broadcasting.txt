Oregon Congressman Earl Blumenauer (D) says he knows there's a need to cut back, but doesn't understand why the Corporation for Public Broadcasting (CPB) got the ax. 

During a Saturday interview with Fox News, Blumenauer lamented, "It is unfortunate we are zeroing in on this little piece." 

The House of Representatives voted to eliminate the CPB's funding as part of a government spending bill that was passed in the early hours of Saturday morning. 

The Corporation for Public Broadcasting provides support for ventures like National Public Radio (NPR) and PBS, home of " Sesame Street ." 

Colorado Congressman Doug Lamborn (R) says the vote reflects the will of the people, and that its time "to get our fiscal house in order." 

This wasn't Lamborn's first push to stop taxpayer money from paying for public programming. His proposal to eliminate federal funding for NPR won a weekly "YouCut" vote last year, but failed in the 111th Congress. 

While Lamborn calls this weekend's vote a "historic step," Blumenauer says the funding cuts have less to do with fiscal needs and more to do with politics. 

"Up until now support for public broadcasting has always been bipartisan," said the Oregon Democrat. "This is the very first time it was a complete partisan move. Even in the height of the Gingrich revolution we've always had dozens of Republicans who joined with us." 

But Lamborn tells Fox News that in Gingrich's day, there was more of a need for public broadcasting than there is today. 

"We live in a different day and age than we did back in the 90s," argued Lamborn, "There are so many media outlets available to people we don't need a government-sponsored media anymore." 

Blumenauer disagrees, and says public broadcasting is a necessity to America's youth. 

"Yes, there are 500 channels but no programs for educating our children," says Blumenauer. "That's the problem, that many of the things we're talking about, there isn't a viable commercial option. Getting to rural and small town America is much, much more expensive." 

Lamborn says the vote doesn't necessarily mean the end of popular public broadcasting programs. 

"There are a lot of quality programs on public broadcasting that I like. It can survive on its own. Americans realize, we have to take steps now. We have to make a difference. We have to start with the first step and this is it."
