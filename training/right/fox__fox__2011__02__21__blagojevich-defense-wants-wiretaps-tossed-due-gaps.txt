"I've got this thing and its 'f-ing' golden!" That's what former Illinois Governor Rod Blagojevich infamously said about Barack Obama's former U.S. Senate seat, according to the government's wiretapped recording. 

But you might not hear that and other similar statements again during his retrial in April. Blago's lawyers are trying to throw out all wiretap recordings, according to court documents they filed Monday. 

Prosecutors want to use the recordings as evidence for the 23 charges against Blagojevich, including claims that he tried to sell the appointment he would make to the senate seat. 

But according to the Monday filing, the defense says it wants to "suppress any and all fruits of the wiretaps, because the government failed to establish a prima facie showing of reasonable minimization. In other words They're saying they don't want any of the wiretaps played because they're not accurate." 

"The recordings were turned on and off and critical dialogue was missed, so they either want the tapes completely suppressed or played in full, hoping the jurors notice the conversations don't flow and parts of them are missing," explained Attorney Steve Greenberg, who is not working on the Blagojevich case. 

The defense filing goes on to say that on "one call that has been deemed critical to the prosecution, in which Blagojevich describes the Senate appointment as "f-ing golden" contains three minimizations, or 'gaps' totaling four minutes. In fact, there is a 'gap' less than a minute before Blagojevich used that now famous phrase--a phrase the government made sure was 'heard around the world'--that has unfairly prejudiced so many." 

Blago's attorney Sheldon Sorosky did not return calls asking for comment. 

There are hundreds of hours of tapes, recorded from Blagojevich's office, home and cell phone. Many times during the first trial, the recordings provided levity and humor in the courtroom, as jurors were able to hear expletive laced conversations between the former governor and his wife. 

At that trial selective parts of the tapes were played. The government said it would take far too long to play all them all. 

Play the tapes, Don't play the tapes... the Blago drama continues.
