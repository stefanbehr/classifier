Email Invitations the White House sent to Cleveland area business owners inviting them to a roundtable looked like spam to many of the recipients, leading some to delete the invites from their inboxes. 

A vague subject line and generic language that asked for the recipient's social security number set off red flags among some who got the email. Others may not have ever gotten the email as it didn't make it through their spam filters. 

PartsSource Chief Executive Ray Dalton tells FOX News his company received an invitation from the White House to meet with the president but they discarded the email as spam, suspecting it of being a phishing solicitation. 

But Dalton says he now plans to attend after getting a call from the White asking why he hadn't responded to the emailed invite. 

FOX News Radio's Mike Majchrowitz contributed to this story.
