President Obama is receiving regular briefings on the violence in Libya. 

According to a senior administration official, the president was briefed Sunday night by National Security Adviser Tom Donilon , and is being "kept up to speed" on events Monday. 

The White House says it's analyzing a speech by Muammar al-Qaddafi's son, Saif al-Islam Qaddafi, to see if his remarks contain any hints for "meaningful reform" in Libya. Saif al-Islam Qaddafi spoke on state TV Sunday night, vowing his father - Libya's leader of 42 years - and security forces would fight "until the last bullet." 

According to reports in the region, more than 200 have been killed in protests around the country. Demonstrators were celebrating Monday in Benghazi claiming control of Libya's second largest city as protests also spread in the capital of Tripoli. 

Since last week the Obama administration has urged the governments of Libya, Bahrain and Yemen to show restraint in dealing with the evolving protests. In a written statement issued Friday, Mr. Obama offered his condolences to the families of those killed during the demonstrations, and Monday the senior administration official once again called for "the need to avoid violence against peaceful protesters," reiterating the need to respect universal human rights, including the right to peaceful assembly. 

The senior official said the administration is considering "all appropriate actions."
