By this time in the last presidential cycle, a whopping 18 contenders had already either announced plans to run or form an exploratory committee. 



The last presidential campaign was not only up and running, it was months into the running. 

Four years ago 10 candidates had already officially jumped in the race. 

Then Senator Barack Obama had already stood on the Illinois State Capitol and announced his intention to run, and on this very day in 2007, Iowa Governor Tom Vilsack jumped out of the race after three months of exploring. Then Sen. Hillary Clinton had already been a month into her candidacy and Sen. John McCain had filed with the Federal Elections Commission a couple weeks before Thanksgiving. 

The statistics are pretty remarkable considering that so far only one candidate has publicly stated any official plans for 2012. 

Republican and Godfather's Pizza businessman Herman Cain is the only one who has publicly stated that he's formed an exploratory committee for a 2012 run. We also know that Sen. John Thune , R-S.D., will not be running after he made an official announcement Tuesday. 

In recent history, presidents have typically announced their intention for a second term about a year and half before the election, around spring-time. President Obama is expected to follow suit with a similar timeline, but his staff and organization already getting in place now. 

Of course there have been plenty of GOP candidates flirting with the idea and even more speculation about it, with many potential hopefuls capitalizing on questions and attention about their future plans. We are still waiting to hear what Sarah Palin , Mitt Romney , Tim Pawlenty are doing and if any Democrats will buck the liberal establishment and take on Obama. 

The last election was certainly an unusual year, with no incumbent or vice president gunning for the nomination, which left the field very wide open. 

Here's a look at the data and dates of who jumped in the 2008 race and when: 

Nov. 9, 2006 Former Gov. Tom Vilsack (D-IA) files with the FEC to establish a presidential campaign committee 

Nov. 10, 2006 Former Mayor Rudy Giuliani (R-NY) files with New York State to establish a presidential exploratory committee 

Nov. 16, 2006 Sen. John McCain (R-AZ) files with the FEC to establish a presidential exploratory committee 

Nov. 20, 2006 Former Mayor Rudy Giuliani (R-NY) files with the FEC 

Nov. 30 2006 Former Gov. Tom Vilsack (D-IA) formally announces candidacy 

Dec. 4, 2006 Sen. Sam Brownback (R-KS) files with the FEC to establish a presidential exploratory committee 

Dec. 12, 2006 Rep. Dennis Kucinich (OH) announces candidacy on Dec. 12, 2006 

Dec. 13, 2006 Former Gov. Tommy Thompson (R-WI) files with the FEC to establish a presidential exploratory committee on Dec. 13, 2006; announced candidacy on 

Dec. 28, 2006 Former Sen. John Edwards (NC) announces candidacy 

Jan. 3, 2007 Former Gov. Mitt Romney (R-MA) files with the FEC to establish a presidential exploratory commitee on Jan. 3, 2007 

Jan. 7, 2007 Sen. Joe Biden (D-DE) declares on "Meet the Press" -- "I am running for president" 

Jan. 9, 2007 Former Gov. Jim Gilmore (R-VA) files with the FEC to establish a presidential exploratory committee 

Jan. 11, 2007 Sen. Christopher Dodd (CT) announces candidacy on Jan. 11, 2007 

Jan. 11, 2007 Rep. Ron Paul (R-TX) files papers in Texa 

Jan. 12 Rep. Duncan Hunter (R-CA) files with the FEC to establish a presidential exploratory committee 

Jan. 16. 2007 Rep. Tom Tancredo (R-CO) announces formation of a presidential exploratory committee 

Jan. 16, 2007 Sen. Barack Obama (D-IL) files with the FEC to establish a presidential exploratory committee 

Jan. 20, 2007 Sen. Sam Brownback (KS) announces candidacy 

Jan. 20, 2007 Sen. Hillary Rodham Clinton (NY) announces formation of a presidential exploratory committee, posts on website "I'm in" 

Jan. 21, 2007 Gov. Bill Richardson (NM) announces formation of a presidential exploratory committee 

Jan. 25, 2007 Rep. Duncan Hunter (CA) announces candidacy 

Jan. 29, 2007 Former Gov. Mike Huckabee (R-AR) files with the FEC to establish a presidential exploratory committee 

Jan. 31, 2007 Sen. Joe Biden (D-DE) files with the FEC to establish a presidential campaign committee 

Feb. 10, 2007 Sen. Barack Obama (IL) formally announces candidacy in Springfield, IL at the Old State Capitol 

Feb. 13, 2007 Former Gov. Mitt Romney (R-MA) announces candidacy on Feb. 13, 2007 

Feb. 23, 2007 Vilsack announces withdrawal 

March 12, 2007 Rep. Ron Paul (R-TX) announces candidacy 

April 2, 2007 Rep. Tom Tancredo (R-CO) announces candidacy 

April 4, 2007 Fmr. Gov Tommy Thompson (R-WI) announces candidacy 

April 25, 2007 Sen. John McCain (R-AZ) announces candidacy 

April 26, 2007 Former Gov. Jim Gilmore (R-VA) announces candidacy 

May 21, 2007 Gov. Bill Richardson (D-NM) formally announces candidacy 

June 2007 Former Sen. Fred Thompson (R-TN); forms testing the waters committee at the beginning of June 2007 

Aug. 12, 2007 Tommy Thompson (R-WI) announces withdrawal 

July 14, 2007 Former Gov. Jim Gilmore (R-VA) announces withdrawal 

Sept. 1, 2007 Hillary Clinton changes exploratory committee to campaign committee 

Sept. 6, 2007 Former Sen. Fred Thompson (R-TN) announces candidacy 

Sept. 14, 2007 Dr. Alan Keyes (R) announces candidacy 

Source: Democracy in Action, 2008
