Most Democrats want the government to pay for public broadcasting. 

Most Republicans want the government to sponsor NASCAR . 

There. That was simple enough. 

And now you know a couple of the basic differences between Democrats and Republicans on Capitol Hill . 

These outcomes were as clear as day in last week's House spending bill to fund the government through September and shave $61 billion from the budget. 

Republicans wrote the bill in a way to eliminate the Corporation for Public Broadcasting and its $460 million budget. Rep. Earl Blumenauer, D-Ore., crafted an amendment to restore the funding. But the House ruled that Blumenauer's amendment wasn't in order. So stood the original intent of the legislation to kill government money for public broadcasting. 

Then Rep. Betty McCollum, D-Minn., offered an amendment to strike a program where the Pentagon spends $7 million to sponsor Sprint Cup driver Ryan Newman. 

The Army says the NASCAR sponsorship helps with recruiting. But McCollum called the program an "absurdity." The House rejected McCollum's amendment, 281-148, on a mostly-party line vote. 

So, the legislative process worked its will. And the result shows that the United States has a pro-NASCAR, anti-public broadcasting House of Representatives. 

A yawning chasm separates the policy priorities of Congressional Democrats and Republicans. And you can distill those differences into the support or opposition of government-involvement in public broadcasting or NASCAR. 



Certainly there are some Republicans who back government support for public broadcasting. And 30 Democrats voted against taking away the Pentagon's NASCAR money. But this is the essence of the debate now raging in Washington as people begin whispering about a potential "government shutdown." 

Democrats have priorities they think the government should spend its money on. Republicans do, too. And the schism over those priorities is why the two parties are now staring into a gigantic crevasse that divides their positions. 

"We have had a very elevated week of debate about the entire government," said House Appropriations Committee Chairman Hal Rogers, R-Ky., on the floor last week. 

It was "elevated" all right, as lawmakers duked it out about defunding the implementation of the health care reform law or eliminating federal support for Planned Parenthood. The "entire government" that Rogers refers to snakes into tributaries and streams that most Americans aren't even aware existed. But if you want to understand the policy priorities, follow the money. That tells you a lot about the agenda of one side or the other. 

Which is why there is chatter of a potential government shutdown unless the sides can bridge the gorge that separates them. 

It's moments like these, usually centered around talk of a "government shutdown" that helps define political movements for years to come. 

Nothing undid the "Republican Revolution" of 16 years ago as quickly as the partial government shutdowns of late 1995 and early 1996. 

For starters, it was not a "full" government shutdown. Each year, Congress must approve 12 or 13 annual spending bills which fund various aspects of the federal government. The House and Senate must eventually approve the same versions of the bills and the president must sign each piece of legislation into law. 

In 1995, President Clinton signed a number of spending bills into law, thus giving those sections of the federal government the money and authority to operate. Mr. Clinton reached agreements with the GOP Congress on legislation to run for the Department of Agriculture, the Pentagon, energy and water programs, the legislative branch (which funds Congress), the Treasury Department, Transportation and military construction efforts. 

But the president and Republicans reached a standoff when it came to how much money (and what policy priorities should be paid for) in bills to fund the Departments of Commerce, Justice, State, Interior Labor, Health and Human Services, Education and Veterans Affairs. The sides waged an intractable battle over abortion in a foreign operations spending bill. 

House Speaker Newt Gingrich , R-Ga., rose to power in early 1995 on the crest of his upstart, Republican freshmen who were determined to change Washington. But the partial government shutdowns soon clipped Gingrich's wings. 

The government closures of '95 and '96 may have been the beginning of the end for Gingrich. But ironically, it was a short-lived government shutdown in 1990 that helped propel Gingrich to the speakership five years later. That government closure may have marked the eventual undoing of President George HW Bush. 

In October, 1990, Mr. Bush and Congressional Democrats hit a wall when he vetoed a spending bill, shuttering the federal government for the Columbus Day weekend. In many respects, that was a token shutdown, impacting mostly tourists who were turned away from federal parks and Smithsonian museums. 

In 1988, President Bush defeated Democratic nominee Michael Dukakis with the mantra "Read my lips: no new taxes." But right after the 1990 shutdown stalemate, the president signaled he would accept some tax increases for the wealthy in exchange for a cut in the capital gains tax rate. 

At the time, Gingrich was the House Republican Whip and disagreed with the pact the president ultimately engineered with Congressional Democrats on spending and taxes. Gingrich told ABC that a GOP revolt was a afoot and many Congressional Republicans would balk at whatever plan Mr. Bush concocted. 

Years later in a "Frontline" documentary (aired perhaps ironically on PBS), Gingrich that he "believed" President Bush's no new taxes pledge from the '88 campaign and felt Mr. Bush sold out Republicans. 

"I thought that violated every aspect of the conservative movement," Gingrich said. "I thought it was what distinguished Republicans from the old-fashioned, traditional Republicans. And I thought I had no choice. I would have betrayed everything I stood for in my career." 

Two years later, voters showed Bush 41 the door. And four years after that, Gingrich engineered the first GOP takeover of the House in decades. Which pitted Gingrich against Clinton in the 1995 impasse. 

"The differences that exist between the two sides are very deep, very fundamental," said President Clinton's spokesman Mike McCurry when the government was locked in a shutdown on December 28, 1995. 

The "differences" McCurry refers to are the same, if not even more intense today. The sides are again warring over the size and role of government. White-hot issues such as health care, abortion, foreign aid and public broadcasting return as key ingredients in this fusion. 

Both Democratic and Republican leaders insist they want nothing to do with a government shutdown when the current spending bill expires in early March. 

But we've seen this movie before. And if the dispute does trigger a government shutdown, expect the political echoes to resonate for years to come. 

Just like what happened in 1995 and 1996.
