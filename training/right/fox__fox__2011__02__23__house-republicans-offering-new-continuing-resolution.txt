House Republicans now plan to offer a two week extension of federal spending in a new Continuing Resolution or CR. 

Senate Democratic leaders said Tuesday they wanted a "clean" Continuing Resolution, meaning no additional spending cuts, just an extension of current spending. 

But Republicans have divided up the $61 billion they were seeking in cuts over the next 30 weeks, which comes out to about $2 billion in cuts a week. 

So they will offer a two-week extension of the CR with $4 billion in cuts. 

Those cuts will be specified but that has not been worked out yet.
