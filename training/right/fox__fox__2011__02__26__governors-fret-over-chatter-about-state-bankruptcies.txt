Quick quiz... 

What's the most-vexing challenge facing the nation's governors? Scraps with their state employee unions? Chasms in their budget deficits? Federal mandates from Washington? Skyrocketing unemployment figures? 

Try chatter. Specifically, chatter about allowing fiscally-strapped states go bankrupt. 

"Not only do we not want it, we want to stop the discussion," said Washington Gov. Christine Gregoire (D), the chair of the National Governors Association, about the possibility of state bankruptcy measures. 

The nation's governors are caucusing the next few days in Washington. And suggestions about potential federal legislation that would permit states to go belly up is roiling governors from coast to coast. 

"This is the most dangerous discussion that we've had in political terms in a long time," said Connecticut Gov. Dan Malloy (D). "Please don't destroy the municipal bond market." 

States and cities often utilize the municipal bond market to obtain loans. And proposals to permit states to declare bankruptcy rattled the $3 trillion market earlier this year when some key Republican leaders either floated the idea or suggested they would be open to such legislation. 

"We're exploring that as a reasonable option," said influential Sen. John Cornyn (R-TX) in January. 

Former House Speaker Newt Gingrich (R-GA) and former Florida Gov. Jeb Bush (R) briefly promoted a plan to permit insolvent states to escape their debts. 

House Majority Leader Eric Cantor (R-VA) quickly put the kibosh on moving any legislation to grant states bankruptcy protection. 

"The states can deal with this and have been able to do so on their own," Cantor said at the time. 

Gregoire suggested that the talk alone of legislating state bankruptcy provisions drove up municipal bond rates. That in turn makes it tougher for local government to secure funds for infrastructure projects. 

"You're talking about drying up the capital for public works projects," said Malloy. "It would be the height of insanity."
