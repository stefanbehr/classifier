House Majority Leader Eric Cantor (R-VA) may not have paid a courtesy call to embattled freshman Rep. David Rivera (R-FL) during a visit to south Florida late last week. But that doesn't mean Cantor's not paying attention to Rivera and possible criminal probes involving the rookie lawmaker. 

"I'm obviously very concerned about the reports surrounding these investigations, but we'll wait to see the results of them," said Cantor "There are ongoing investigations in Florida involving Congressman Rivera, and we are respectful of those investigations." 

The remarks by Cantor on Monday marked the first time he formally commented on Rivera's possible legal woes. 

Authorities are reportedly investigating Rivera's payment of more than $800,000 to a political consultant. Florida investigators are also studying Rivera's failure to disclosure more than $100,000 in loans he made to himself as unexplained campaign reimbursements while he served in the state legislature. Rivera did not initially disclose those loans. Rivera denies any wrongdoing. 

Cantor's comments stand in contrast to remarks made a few weeks ago by House Speaker John Boehner (R-OH) when asked about Rivera's alleged transgressions. 

"We're waiting to see how this plays out," Boehner said at the time, noting that none of the allegations involving Rivera unfolded during his short time on Capitol Hill . 

GOP leaders are reportedly scouring south Florida for potential successors to Rivera should he have to step aside or decide against running again.
