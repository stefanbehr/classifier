Frank Buckles, the last living veteran of World War I , died at home Sunday in Charles Town, West Virginia. He was 110 years-old and many in Washington are reacting to his death. 

"Frank Buckles lived the American Century," President Obama said through a White House statement. "...just as Frank continued to serve America until his passing, as the Honorary Chairman of the World War I Memorial Foundation, our nation has a sacred obligation to always serve our veterans and their families as well as they've served us." 

Senator Jay Rockefeller , D-W.Va., also joined those praising Buckles and noted the lack of a national memorial to those who served in World War I . 

"Frank Buckles was a unique American, a wonderfully plain-spoken man and an icon for the World War I generation," Rockefeller said in a statement. "Mr. Buckles fought for our nation's freedom, and continued to fight to make sure that he and the more than 4.3 million Americans who served in the first Great War are honored with a fitting tribute through a national memorial - and that is a fight that I will continue." 

Rockefeller and Sens. Jim Webb , D-Va., John Thune (R-SD), Claire McCaskill (D-MO), and Roy Blunt (R-MO), have introduced a bill that would make the District of Columbia World War I Memorial a national memorial and would also rededicate a memorial and museum in Kansas City as a memorial to the war. 

"A national memorial to the more than 4 million who served in World War I is long overdue," Webb said in a statement. "I look forward to their eventual recognition amongst the other tributes on the National Mall." 



The bill, named for Frank Buckles, would also create a centennial observance of World War I. 

Buckles lied about his age so he could enlist and was only 16 years old when he headed out to fight in the first World War. He turned 110 at the beginning of February and had become the last living World War I veteran when Florida's Harry Richard Landis died in 2008. 

Sen. Joe Manchin , D-W.Va., called Buckles a true American patriot. 

"It was an honor and a privilege to get to know Frank Buckles, and I count him among the most inspirational Americans I've ever met," Manchin's statement said. "Frank touched my life, and the lives of so many, with his bravery and commitment to serving this great nation." 

Congresswoman Shelly Moore Capito, R-W.Va., echoed that praise. 

"Mr. Buckles represented the very best of this great country-service, determination and patriotism," she said. 

Capito plans to introduce legislation in the House Monday authorizing Buckles to lay in honor in the U.S. Capitol rotunda.
