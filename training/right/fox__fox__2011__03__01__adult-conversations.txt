If there's anyone in politics who knows something about having an "adult conversation," it's probably House Speaker John Boehner (R-OH). 

Boehner likes to tell people he's one of 12 children and that his dad owned a bar. Which probably makes him uniquely qualified to conduct an "adult conversation." 

Lots of politicians embark on "listening tours" when they're considering a run for higher office. 

Boehner is now setting sail on an "adult conversation" tour about American debt, the federal budget deficit, entitlement spending and efforts to cut money and avoid a potential government shutdown. 

Having an "adult conversation" about these nettlesome issues has emerged as the speaker's favorite phrase the past few months. 

"I do believe that the American people expect us to have an adult conversation with each other about the serious challenges that face our country," said Boehner at a news conference shortly after taking the speaker's gavel. 

"I think it's time for Washington to have an adult conversation with the American people about the big challenges that face us," said Boehner on Fox News Sunday in late January. 

"This is not the way you begin an adult conversation' in America about solutions to the fiscal challenges that are destroying jobs," wrote Boehner about President Obama's budget in his February 19th weekly column. 

Lucky for Boehner, President Obama and his administration are also interested in 'adult conversations.' Not long ago the president called for more age appropriate discussions, then his press secretary echoed the specific call for 'adult conversations' five times the next day. So far, though, it seems like they're speaking a different language. 

So what does having an "adult conversation" mean? 

For Boehner, it's acquainting the public and lawmakers (many of whom are House Republican freshmen) about what the U.S. is facing economically if it doesn't impose some serious fiscal restraints. To Boehner, it's about curbing spending. It's about crafting a spending package that the GOP-led House, the Democratically-controlled Senate and President Obama can all agree to and avert a government shutdown. And it's about figuring out how to handle a looming deadline to increase the federal debt ceiling (the total amount of debt the U.S. is allowed to carry at any one time) without defaulting on its obligations and rattling global financial markets. 



All of these challenges are materializing right now. And Boehner knows the outcome will have staggering economic implications for years to come. 

"These are going to be the most important two, three, four months that we've seen in decades," Boehner said recently. 

For starters, Boehner and the House GOP leadership may have bought itself a little bit of time to have adult conversations by crafting a two-week stopgap spending bill that probably averts a government shutdown later this week and slices $4 billion in spending. The ad hoc legislation allows the Senate to grapple with the spending issue for a bit. And while the Senate is expected to go along with it, not everyone is pleased. 

"I think this two-week business is not the way to go," said Senate Budget Committee Chairman Kent Conrad (D-ND) on CNN. "The big problem is we are focusing on just a small part of the budget." 

Conrad's right. Remember that spending bill the House debated nearly round-the-clock two weeks ago that axed $61 billion? Yes, it was ambitious and Republicans (along with some Democrats) applauded the depth of the cuts. But that legislation probably isn't going anywhere, despite the time and energy lawmakers exhausted on it. And it only dealt with chopping $61 billion out of a $4.6 trillion total budget pie. And the "small part" that Conrad talked about? That bill never touched most of the total spending that the government is on the line for. The bulk of that money is devoted to Social Security , Medicare, Medicaid and net interest on debt. 

Here's another intriguing tidbit about the House measure: Republican leaders initially drafted a bill that proposed $32 billion in cuts. While deep, it was widely believed that the House, Senate and president could agree to this package and fund the government through September and be done with it. And then start work on spending for fiscal year 2012 (which starts in October). 

"We have taken a wire brush to the discretionary budget and scoured every program to find real savings that are responsible and justifiable to the American people," said House Appropriations Committee Chairman Hal Rogers (R-KY) in a statement at the time. 

Only the cuts weren't deep enough for House Republican freshmen. They came to Washington with a mandate: cut government. And the GOP plan, while tenable, wasn't enough to satiate the demand from firebrand conservatives, stoked by tea party loyalists. 

"Whatever the White House decides to do, the speaker isn't going to agree to anything his caucus won't support," said Boehner spokesman Brendan Buck. "There is no difference, ultimately, between the speaker and his members." 

So the House GOP went back to the drawing board. It produced deeper cuts and burned the midnight oil debating a bill that faces an uncertain future in the Senate and probably won't score President Obama's signature. 

Which brings us back to the possibility of a government shutdown not this Friday, but perhaps March 19. 

So are the adult conversations just starting? Or are they not working? Or will they truly be had in the coming weeks? 

When Republicans seized control of the House last November and talk turned to the looming budget battles between a GOP-House and a Democratic president, many harkened back to the legendary partial government shutdowns of 1995 and 1996. But political insiders promised that things would be different this time. John Boehner was the fourth-ranking Republican in the House in the mid-1990s and saw the missteps of his GOP colleagues up close. 

Most political observers noted that Boehner learned from the mistakes of then-House Speaker Newt Gingrich (R-GA). In fact, many Capitol Hill sources described Boehner as the "adult" in the GOP leadership today due to his experience in the mid-1990s. Thus, Boehner was the perfect person to conduct "adult conversations." 

But there are differences between now and then. Gingrich initially had control of his burgeoning freshman class that escorted the GOP to the majority. Not only does Boehner not "have" control of the freshmen, he doesn't "want" control of the freshmen. Boehner asserts that's not his place as House Speaker. And while freshmen were home last week, they heard a lot from constituents, urging them to make even deeper cuts, to steel their resolve against the Senate and even shutter the government. 

Even if the GOP brass wanted to "control" the freshmen, they probably couldn't because many of them came to Washington on a mission. Or aren't enamored with Capitol Hill and don't mind if they never come back, so long as they vote their conscience. 

This is the "Perriello Effect." 

Many regarded the upset victory by former Rep. Tom Perriello (D-VA) as the surprise of the 2008 election cycle. Perriello came out of virtually nowhere to topple former Rep. Virgil Goode (R-VA) by less than 1,000 votes in a contest no one had on their radar until hours before election day. 

Perriello promptly voted for the touchstones of the Democrats' agenda: the stimulus package, the cap and trade climate bill and health care reform. Perriello then went on the stump and defended his votes. 

"I don't think it makes sense to stand for something and run away from it," Perriello said just days before Rep. Robert Hurt (R-VA) ousted him last fall. 

In short, Perriello felt that his votes on those three issues were the right things to do. And if he lost his seat, so be it. 

The irony is, Perriello came close to beating Hurt, losing by less than four percentage points. 

In Perriello's heart of hearts, he wanted voters to re-elect him. But at the end of the day, Perriello believed he voted his conscience and did what was right. After all, few gave him a shot to even win in 2008. 

The House Republican Conference is brimming with dozens of Tom Perriellos. They're not wedded to Washington. Many won in tough races by just a thousand votes. And if they're able to vote to change the culture in Washington, fine. If the voters show them the door, all you'll see from them is exhaust fumes as they escape the confines of the Beltway. 

So do these freshman have much to lose? Yes. 

One chapter in Boehner's adult conversation repertoire came over the weekend when he spoke to religious broadcasters in Tennessee. 

"We have a moral responsibility to address the problems we face. That means working together to cut spending and rein in government. Not shutting it down," Boehner said. 

That's certainly a message for Democrats. But it's also directed at some Republicans who think a standoff with President Obama and Democrats will help them achieve their budget goals and score political points with the base back home. 

In the coming weeks, Boehner must pilot the House through the next two rounds of government operation bills, a vote on raising the debt ceiling and a clash over reforming entitlements. This is all essential if the U.S. is to get a grip on its spending addiction. 

That means a lot of adult conversations ahead. 

And plenty of temper tantrums.
