At a House Appropriations subcommittee hearing on Capitol Hill Tuesday, Attorney General Eric Holder admitted he has no idea when the administration will be able to close the Guantanamo Bay Detention Center, the first time a senior administration official has indicated it could remain open through 2012. 

"I don't know," Holder said when he was asked by Rep. Frank Wolf, R-Va., if Gitmo would close by the end of the president's first term. 

Holder also said that on a regular basis, his daily intelligence briefings show that Guantanamo is still being used as a recruitment tool by al Qaeda . Wolf strongly disagreed and countered that al Qaeda attacked the World Trade Center in 1993, the U.S. embassies in East Africa, and the USS Cole long before Gitmo was a point of contention. 

Asked to address the high rate of return to the battlefield which is now at least one in four for detainees, Holder insisted the administration was still determined to close the detention facility and squarely blamed Congress for putting road blocks in place. 

Faced with high rates of recidivism and turmoil in countries like Yemen (where the majority of detainees are from), Holder insisted the administration would still try to close it. 

Holder also walked back CIA Director Panetta's statement that Bin Laden would go to GTMO if he was caught. Holder said Panetta comments were "clarified" later and that a committee would decide how Bin Laden would be handled.
