One day after Democrats warned that Republican-backed budget cuts could endanger national security, a top Republican on Capitol Hill has broken ranks and praised the Department of Homeland Security for its proposed spending. 

"In a time of budget restraint [when] cuts have to be made, I actually commend the [DHS] secretary for putting forth a budget which I believe ... is very much on target and is trying to accommodate the needs for cuts and also to protect our nation," said House Homeland Security Committee chairman Peter King, R-N.Y. "We saw just last week the importance of this, when we saw the arrest of [Khalid] Aldawsari, a Saudi Arabia national in Texas. This was another reminder of how serious the threat to our nation is." 

Aldawsari, 20, of Lubbock, Tex., was arrested by FBI agents after allegedly trying to build a bomb and researching several potential U.S. targets, including President George W. Bush's home in Dallas. Aldawsari was admitted into the United States in 2008 on a student visa. 

During a committee hearing on Thursday, King said any cuts to security programs and grants "would be offset immediately if we should see a successful attack launched in the United States," adding that such an attack would have a "devastating impact on our economy." He is particularly worried about cuts to mass transit and port security grants. 

Lawmakers on both sides of the aisle have been sparring over how to tighten federal spending, and on Wednesday they averted -- at least temporarily -- a government shutdown by passing a "continuing resolution" to fund operations for two more weeks. In passing the resolution, Democrats accepted $4 billion in cuts to some spending. Administration officials now worry the resolution could form the basis for a long-term deal. 

"Nobody will escape this unscathed if that budget remains the budget," said DHS Secretary Janet Napolitano , the hearing's sole witness. 

She echoed statements made during a series of budget-related hearings the day before. 

"Today's threat picture features adversaries who evolve quickly and are determined to strike us here at home, from the aviation system and the global supply chain to surface transportation, to critical infrastructure, to our cyber networks," she said. "President Obama's [2012] budget for the department allows us to continue to meet ... evolving threats and challenges by prioritizing our essential operational requirements while reflecting an unprecedented commitment to fiscal discipline that maximizes the effectiveness of every dollar we receive." 

The committee's ranking member, Rep. Bennie Thompson, D-Miss., said there is "a very real threat that the funding for DHS operations" will "plunge to 2006 levels." 

"Our efforts to address one of the nation's greatest threats, cyber attacks from rogue nations, terrorists and lone wolf activists, would be severely hampered," he said. 

But King, Thompson and others expressed particular concern over cuts by both Republicans and DHS itself to efforts along the Southwest border. 

"The picture it presents is potentially devastating to the department," Thompson said, adding that Customs and Border Protection alone would lose $3 billion, forcing more than 8,200 border patrol agents or 2,800 CBP officers to be let go. 

Similarly, Rep. Dan Lungren, R-Calif., said Napolitano's investment in the "See Something, Say Something" campaign to fight terrorism "makes a good deal of sense" and he "applauded" her for budget requests to expand cyber-security efforts, but he said the "greatest threat" is "fiscal irresponsibility." 

"On the one hand there appears to be an appropriate emphasis and priority given to cyber-security, on the other hand it does not appear ... [to have] a similar stress on the area of border control," Lungren said of the administration's budget requests. 

Napolitano told Lungren that his remarks presume "that the president's budget is not the most aggressive in history with respect to the border." 

"If the president's budget is adopted, we will have more border patrol agents at the border then at any time in our nation's history," she told the committee. "I must say, however, that I'm very troubled by the [continuing resolution] particularly if it becomes the basis for the [2012] budget because it does not fully protect those expansions in CBP and ICE and all of the operations that we see under the president's budget. So I would ask the House as it gets us hopefully out of 'Continuing Resolution Land' and into a real budget for [2011] and [2012] that they'll examine those priorities." 

She said the Republican-backed budget "basically stops our progress in its tracks." 

"We have a pathway forward on that border, and that includes manpower, it includes technology, it includes infrastructure," she said. "That's why the President has put more border patrol agents in his budget than any other time in our nation's history. That's why he's put more funding into technology. ... That's why he has supported the largest deployment of technology to the Southwest border in our nation's history. That is the pathway forward, that is the plan. Unfortunately, the [continuing resolution] that passed here contradicts that plan and goes backwards. It will take us back to where we were several years ago in terms of the actual resources that are available at the Southwest border." 

Rep. Ben Quayle, R-Ariz., disagreed with Napolitano's assessment, offering his "lay of the land" regarding the continuing resolution. 

"From my looking at it, it's going to be adding more border agents, not decreasing border agents," he said, adding that funds for Customs and Border Protection are bring increased by nearly $148 million over the previous year and another $550 million is being put into fencing, infrastructure and technology. 

Napolitano said she "really can't agree with the lay-down you gave of the facts in terms of how they really affect funding" for the Southwest border. 

"It's not a good border budget," she said. "It's not a good immigration budget, and we believe very strongly to keep moving in the direction we're moving [the administration's budget] is the right thing. The numbers that need to change are all going in the right direction, and dramatically so, particularly in Arizona." 

During a Senate hearing one day earlier, a top Democrat questioned whether Republicans are "simply cutting resources or cutting the throats of people in our society," while Republicans accused the Obama administration of employing "cost-cutting gimmicks," "fudging numbers" and failing to grasp the fiscal reality. 

Sen. Frank Lautenberg , D-N.J., said the "urgent threats" facing the U.S. homeland come "from our friends in the House who want to cut funding for programs [at] the Department of Homeland Security to keep us safe." 

He said "unreasonable" and "irresponsible" cuts backed by Republicans would "slash funding for valuable homeland security grant programs." The newly-passed continuing resolution cuts two major grant programs for the New Jersey and New York areas by two-thirds, from $300 million each to $100 million each. The Obama administration, however, has proposed maintaining the grants at $300 million each. 

"This is not a time to cut back on homeland security," Lautenberg said. "We've seen an increased risk of homegrown terrorism -- the Ft. Hood massacre, the Times Square bombing attempt, and the New York City subway plot ... It's incidents like these [that] remind us that the threat of terrorism is as real as ever, and we're doing more than skimping on public safety. Are we simply cutting resources or cutting the throats of people in our society?" 

On Tuesday, in an interview with the Wall Street Journal, King also criticized the proposed cuts. 

"From a security perspective and a dollars and cents perspective, it's very shortsighted, it's dangerous, and it's wrong,'' he said. "We're talking about security for ports all around the country. The fact is this is a national issue." 

But during Wednesdays Senate hearing, Republicans repeatedly cited "reality." 

"The reality is that we do have this significant deficit problem," Sen. Dan Coats, R-Ind., said. "Along with national defense, homeland security and a couple others are essential functions of the U.S. government. [But] there just are fiscal realities that we have to deal with. ... Doing as much or more with less is something that all of America has had to do in these last two years, and it's easier for some than others. But everyone has been forced to make those hard decisions." 

Napolitano said Wednesday -- and repeated Thursday -- that she and her department have already made hard decisions, with all DHS components identifying cuts totaling more than $800 million. 

"We went through that analysis in ... finding places or things that could be put off," she said Wednesday. "For example, postponing the move to a real department headquarters [in Washington] ... We made a tough choice there. It means we can't have all our components co-located. It means from a managerial standpoint we still are spread out." 

The move to a single headquarters in Washington would cost hundreds of millions of dollars.
