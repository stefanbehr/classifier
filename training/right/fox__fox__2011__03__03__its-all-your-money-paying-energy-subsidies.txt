Cheap gas. Energy independence. More power from wind and solar. Many Americans want these things, but do they want to pay for them with tax dollars? 

Call them handouts, giveaways or corporate welfare, the federal government subsidizes every form of energy in this country, some more than others. 

Here's a look at some subsidies in President Obama's 2012 Fiscal Year budget: 

-$126 million for wind 

-$340 million for bio fuels 

-$457 million for solar 

-$452 million clean coal 

-$800 million for nuclear 

Oil and gas get even more of your money through tax incentives, deductions, depreciation and investment credits. Those total about $3.6 billion a year, according to the Administration. 

When you add them all up, for all energy sources including ethanol, Taxpayers for Common Sense President Ryan Alexander says taxpayers lose billions each year. 

"We need a level playing field and the easiest way, the cleanest way would be to eliminate all energy subsidies," says Alexander. 

But that is unlikely. Industrial giants, oil and coal corporations have been sucking off the federal feeding bottle for more than a century, and their lobby in Washington is no less influential today. 

Defenders of the status quo say energy subsidies guarantee a secure and reliable energy source, vital to our economy, vital to our national security.And on a per megawatt basis, fossil fuel subsides deliver energy for pennies on the dollar, compared to expensive green energy, that produces relatively little, for the billions in tax breaks and handouts from Congress. 

According to figures from the Energy Information Administration, oil and gas subsidies compute to about 25 cents per megawatt hour, while wind and solar is about $24. 

Congressman John Sullivan (R-OK) says taxpayers shouldn't have to subsidize some of these technologies and they should be developed on their own. 

"I think it's better to have the free market develop these technologies and these energy sources and compete in a fair market based economy," says Sullivan, who is Vice chair of the House Energy and Power Subcommittee. 

Renewable energy supporters argue they need the money to compete with China and save the planet from global warming. Besides, they say, it's their turn, and it would be unfair for them to compete on a level playing field that for decades favored fossil fuel. 

What do you think? Is it time to let the energy sector stand on its own or should the federal government direct tax money build a future supply of renewables to 'win the future?' 

To find out how much you pay for the $12 billion in direct Energy Department handouts, click here to check out the Taxpayer Calculator. 



Watch the latest video at FoxNews.com
