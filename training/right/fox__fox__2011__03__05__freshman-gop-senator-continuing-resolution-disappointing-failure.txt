To pass a continuing resolution that cuts $4 billion dollars and to call that a cut is a little bit like, as my friend [Senator] Jim Risch (R) from Idaho says, it s like we re outside trimming the bushes in front of a burning house and saying, aren't these bushes great? 

That s what freshman Senator Mike Lee (R-Utah) told Fox News he thinks about the short-term spending bill approved by the House and Senate this week. Lee was one of five Republican Senators to vote no on the bill that will keep the government running until March 18. 

In a statement released Wednesday, Senator Lee called the proposal a disappointing failure on the part of both parties to seriously address the economic meltdown we face from our massive deficit and growing national debt. 

Lee said he doesn t think anyone in his party wants to see a government shutdown. But he added more aggressive budget cuts are needed. 

Senator Lee said he is working on a balanced budget amendment . Lee said his plan would prohibit Congress from engaging in this practice of perpetual deficit spending. 

If they certainly want to get my vote they ll have to put in place some kind of a mechanism to make sure we don't continue borrowing at this rate, added Senator Lee. 

Leaders of the House and Senate met Thursday to discuss plans for another continuing resolution to keep the government funded through September 30. 

Lee is somewhat hopeful that a longer term solution is possible, saying I have some optimism that they ll see how disappointed their voters are and the next time we have to address this a week or two down the road they ll realize we need to be more aggressive. 

For more on where the budget debate stands now, check out Fox News Senior producer Chad Pergram s blog .
