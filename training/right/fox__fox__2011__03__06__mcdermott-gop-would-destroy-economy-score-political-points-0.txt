Congressman Jim McDermott, D-Wash., claims Republicans are purposely hurting the economy to gain political leverage. 

During a Sunday interview with Fox News, McDermott said, "They simply want the chaos to go on and use this meat-ax approach to cut $60 billion across the board with no thought of what the impacts are." 

When asked if he thought the GOP would let the economy fail in order to gain votes in 2012, McDermott responded, "Absolutely. No question." 

McDermott, a senior member of the Ways and Means Committee, says the GOP's $61 billion cuts included in H.R. 1 are "just whacking for the sake of whacking." 

"The cuts they made the other day in the House bill were the most irresponsible piece of legislation I have seen in 40 years of my experience in government," argued McDermott. 

Kansas Senator Jerry Moran (R) says McDermott's claim of Republican sabotage "makes no sense." 

Moran was on Fox News less than one hour after McDermott, and responded, "I don't think anyone is looking for a fight. We're looking for a resolution."
