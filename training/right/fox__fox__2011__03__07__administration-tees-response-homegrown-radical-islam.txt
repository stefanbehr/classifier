The Obama administration is set to announce a comprehensive approach to combating domestic radicalization, according to Deputy National Security Adviser Denis McDonough . 

In a speech to interfaith community groups at a Muslim center in Sterling, VA, McDonough outlined steps the administration has taken to combat what McDonough called "violent extremists of all kinds" at home and abroad. 

"For a long time, many in the U.S. thought that our unique melting pot meant we were immune from this threat ... That was false hope, and false comfort. This threat is real, and it is serious," McDonough said. 

McDonough's speech comes just days before a congressional hearing slated to investigate radicalization among the American Muslim community and how that community has responded to homegrown terrorism among its ranks. New York Rep. Peter King, chairman of the House Homeland Security Committee, argued in an appearance on "Fox Friends" Sunday that a hearing into the matter is a necessary measure to combat extremism, not a "witch hunt." 

"Who should I be investigating if not within the Muslim community? I have said, time and again, the overwhelming majority of Muslims are good Americans," King said. "But the threat is coming from their community. We have to find out why--how it's being done, how we can stop it." 

"I would think that the Muslim community should be embracing what I'm doing, should be willing to cooperate, because they should be trying to root these people out of the community and cooperate with the police and FBI," he added. 

McDonough echoed this notion in principle, but argued casting blame upon an entire ethnic or religious group is dangerous. 

"We have a choice," McDonough said. "We can choose to send a message to certain Americans that they are somehow less American' because of their faith or how they look; that we see their entire community as a potential threat ... And if we make that choice, we risk feeding the very feelings of disenchantment that may push some members of that community to violent extremism. Or, we can make another choice. We can send the message that we're all Americans." 

"The bottom line is this-when it comes to preventing violent extremism and terrorism in the United States, Muslim Americans are not part of the problem, you're part of the solution," he added. 

Meanwhile, Muslim Americans and supporters rallied in New York Sunday in opposition to King's hearings this week. 

Asked about the timing of these remarks, just before King's hearings and after two years of near silence from the Obama administration on al Qaida's efforts to recruit and deploy young American Muslims to attack their fellow citizens, McDonough told reporters, "We welcome congressional involvement in the issue; it's a very important issue."
