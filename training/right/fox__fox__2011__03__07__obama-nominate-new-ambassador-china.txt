Fox News has learned that President Obama will nominate Commerce Secretary Gary Locke , to be the next U.S. Ambassador to China as early as Tuesday. 

The president's current ambassador, Jon Huntsman, is set to leave on April 30th. It's widely speculated Huntsman will set his sights on his boss' job and run for president. 

Locke, whose grandfather emigrated from China to Washington state, is the first Chinese-American Commerce Secretary. He took his post in 2009, but before that was the Governor of Washington state, where he worked to open Chinese markets to his state's companies. He did the same while in the private sector. Perhaps that is why the president picked Locke. Mr. Obama has leaned on big business of late to boost hiring and has suggested increased trade as a way to accomplish that. 

Locke of course would have to be confirmed by the Senate. His prospects are unclear at this point, but his confirmation for Commerce Secretary was relatively smooth. Then again, Locke was President Obama's third pick. His first two choices-- New Mexico Governor Bill Richardson and New Hampshire Republican Senator Judd Gregg -- each withdrew themselves. 

Fox News Channel's Mike Emanuel contributed to this report.
