Mississippi Gov. Haley Barbour has said he's waiting until the state's legislature adjourns in early April to determine whether he's running for president, but it appears he's close to a decision already. 

Fox News has confirmed that Jim Dyke, former RNC communications director and a formidable political operative in South Carolina, has signed on to be a communications adviser to Haley's PAC. In the race to court the strongest campaign staff for a primary bid, the announcement is the strongest indication yet that Barbour is serious about throwing his hat in the ring. 

Henry Barbour, who serves as Treasurer for Haley's PAC, said in a statement, "Haley is delighted to have someone as talented as Jim Dyke joining his team to help us manage the increased national media interest and demand in the Governor."He continued, "Jim's knowledge and experience will prove invaluable to Haley in the coming few months." 

Henry, who is also the governor's nephew, tweeted yesterday with a trace of ambition: "Haley hates to miss Steve Scheffler's Faith and Freedom event in Iowa tonight," in reference to the first major gathering of potential presidential candidates in Iowa. 

Dyke has worked on four presidential campaigns and served as communications adviser to President George W. Bush and Senate Majority Leader Bill Frist. 

His firm has deep ties both inside the beltway and outside of it; DC-based Kevin Madden, who served as spokesperson for Mitt Romney in the 2008 primary, is a partner at JDA Frontilne, which also has an office in Charleston, SC. 

Madden tells Fox News he is still a Romney supporter, but not in any official capacity, and this move does not mean he won't be with the former Massachusetts governor if he makes a run in 2012. 

"Both Jim and I will be doing all our political work outside the firm," said Madden. 

Fox News' Jake Gibson contributed to the report.
