A Democratic Congresswoman insisted the witnesses chosen by the House Committee on Homeland Security to discuss radicalization in the American Muslim community were not "experts" and thus Thursday's hearing was "grossly incomplete." 

Congresswoman Jackie Speier, D-Calif., said her practice of the Roman Catholic faith and attending church every Sunday didn't make her an expert on pedophiles in the Catholic church any more than the personal experiences of two Muslim men whose family members fell prey to Islamic radicalization makes them experts on the issue. 

Melvin Bledsoe, a Memphis businessman, told the story of how his 19 year old son, Carlos, was recruited by a radical mosque in Nashville and now stands accused of opening fire on a military recruitment center in Arkansas. 

Minnesotan Abdirizak Bihi explained how his 17 year old nephew, and approximately twenty other Somali-American youths, were recruited by an al-Qaeda affiliated group who sent his nephew to Somalia where he was killed. 

While Speier referred to the experiences they described as "interesting," both men insisted there was nothing interesting about it. Bledsoe described his son's death as a "tragedy." Bihi explained, "there are no words to describe what I went through." 

Republican Congressman Chip Cravaack of Minnesota defended the witnesses, saying that because the two men and their families had lived through such an ordeal, they qualified as experts. 

Bledsoe likened Congresswoman Speier's opinion to someone who is "afraid of political fear," saying she might be afraid of "not getting re-elected." 

But he assured members of the committee that what his son and family went through was, "a real thing happening in America."
