Nearly 10 years after the terrorists attacks of September 11, 2001, an influential group of Republican senators, led by Sen. John McCain , R-Ariz., and joined by Independent Sen. Joe Lieberman of Connecticut, introduced legislation Thursday that radically changes the way the nation treats military detainees captured in the war on terrorism. 

The legislation prohibits civilian trials for those designated as enemy combatants, strictly prescribes interrogation procedures, restricts the transfer of detainees to countries where combatants have returned to the battlefield, and dries up funding for any alternative prison facilities that might be built on U.S. soil, effectively cementing the long-term existence of the prison at Guantanamo Bay, Cuba (Gitmo). 

After two years of failed negotiations with the White House , Sen. Lindsey Graham, R-SC, an Air Force Reserves lawyer, said, "The time for talking is over. It is now time to act." 

Captured members of al-Qaeda, the Taliban or other affiliated terrorist groups could only be held in military custody, unless the Secretary of Defense certified that civilian custody was in the national security interest, a strong rebuke to Attorney General Eric Holder who had previously sought to try the mastermind of the 9/11 attacks, Khalid Sheikh Mohammed, in federal court in New York City. 



"We're saying, when it comes to Khalid Sheikh Mohammed (and his co-conspirators), we're not going to fund a civilian trial," Graham declared. "It's the only alternative left to me," the senator said, adding, "Putting them in a civilian court after holding them under the law of war for years makes no sense....And it says somehow our military commission system is not good enough." 

The Obama Administration this week issued an executive order that creates a formal system of indefinite detention for current prisoners and authorizes some to be tried in military commissions, while leaving the door open to federal criminal trials for others. But the senators gathered Thursday, all members of the Armed Services Committee, rebuked the president for an "incoherent" and "confusing" policy. Sen. Lieberman said the order "did not go far enough" and said the nation's policy should also include all future prisoners captured in the ongoing war on terror. 

"They ought to be tried in military tribunals," Lieberman said. 

The new bill would also require the Secretary of Defense, the Director of National Intelligence, the FBI Dir, and the Attorney General to submit procedures for interrogating a terror suspect. "It calls for a process designating a person like (Christmas Day bomber Umar Farouk) Abdulmutallab an 'unprivileged enemy belligerent' and transferring that individual to DOD for custody and interrogation," outlined Georgia's Saxby Chambliss, top Republican on the Intelligence Committee. 

"Collecting timely and actionable intelligence must take precedence over the interests of criminal prosecution," Chambliss went on. "When lives are at stake, we can't afford to risk missing that valuable intelligence on terrorists plots or associates simply because the Department of Justice is trying to preserve a statement for use at trial." 

Republicans were widely critical of the Administration for allowing the Christmas Day bomber, who attempted to detonate a bomb in New York's Time Square in December 2009, to obtain legal counsel, though the prisoner did agree to cooperate later. Supporters of the Administration's decision have long argued that providing appropriate legal counsel and access to federal court would be a positive symbol to the rest of the world. 

The senators reserved perhaps their strongest rhetoric for the long-running dispute over whether or not Gitmo should be shuttered for good, once a desired outcome for both Sens. McCain and Graham, along with President Obama. As the Republican nominee for president in 2008, McCain drew fire from many conservatives for saying the prison was "a liability in the fight against radical extremism and as president of the United States I will close it." 

But Thursday, both senators seemed to embrace the idea that the prison would remain open indefinitely and blamed the Administration for that outcome. 

"In a perfect world I would like to see it closed, because it was a symbol, particularly in the Middle East , of improper treatment of prisoners," McCain said, but added that the Administration's "incoherent, incomprehensible, and impossible-to-translate" policy left no alternative. 

"Basically, the administration has thrown up its hands by the announcement that they just made, so we are going to live and operate within an environment that in the short term it's not going to be closed. It is what it is," the Arizona senator said. "Over time, I would like to see it closed, but the fact is, the environment right now, it would be impossible to do so." 

Graham was even more emphatic. "It's not being used," the senator said of the detention facility, adding, "We need to use the jail. We don't have another viable alternative...We're not going to close Guantanamo Bay. We don't have near the votes, because there's never been a plan to give people confidence to vote to close it." 

That sentiment is bipartisan in Congress, which has, on numerous occasions, used the appropriations process to bind the hands of the administration, refusing to allow any alternative facility to be built in the U.S. and its territories until a satisfactory plan for alternative incarceration was produced. 

Sen. McCain said he had talked with his counterpart in the House, Rep. Buck McKeon, R-Calif., who introduced similar legislation this week, and that the two would work out any discrepancies, which the senator called "not significant." 

And though the legislation is likely to take some time to wend its way through a number of committees of jurisdiction, Sen. Lieberman said he thought the prospects for passage ultimately were "very good."
