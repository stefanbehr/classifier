While speaking to a group of around 20 people at MIT in Cambridge, Massachusetts, State Department Spokesman PJ Crowley described the Defense Department's treatment of Pfc. Bradley Manning, the accused WikiLeaks source, as "ridiculous and counterproductive and stupid," according to a blogger who attended the event. 

The scathing comments from a typically well composed spokesman took officials in the Pentagon and the State Department by surprise. 

Manning's lawyer has filed complaints recently arguing his client should not be on maximum detainee and prevention of injury status while being held at the brig on Marine Base Quantico in Virginia. At least twice Manning has been made to stand at attention in the nude at the front of his cell in the morning. Officials at Quantico declined to explain those measures, other than to say it was for his own protection. 

Meanwhile the Pentagon offered a terse response to Crowley. "We are aware of Crowley's comments and we have since sent him the facts about Manning's confinement. We also understand from the State Department that he has said the comments were his personal opinion and not reflective of the government," Pentagon Spokesman Col. Dave Lapan told FOX News. 

After the event at MIT and attendee named Philippa Thomas took to her blog, Philippa Thomas Online, to recap Crowley's remarks. She says Crowley was there to speak on his views on social media, Twitter, and the Arab revolution when one person finally asked about Manning. Her blog reads: 

"And then, inevitably, one young man said he wanted to address "the elephant in the room". What did Crowley think, he asked, about Wikileaks? About the United States, in his words, "torturing a prisoner in a military brig"? Crowley didn't stop to think. What's being done to Bradley Manning by my colleagues at the Department of Defense "is ridiculous and counterproductive and stupid." He paused. "None the less Bradley Manning is in the right place." 

Later the blogger claims she asked Crowley if his remarks were on the record and he replied, "sure." 

One State official told CBS News that Crowley was speaking for himself, not the State Department. A DoD official told FOX News he would be surprised if Crowley had intended those remarks to be "on the record." 

President Obama was asked about Crowley's remarks during his Friday news conference. 

"I have actually asked the Pentagon whether or not the procedures that have been taken in terms of his confinement are appropriate and are meeting our basic standards. They assured me that they are," the president said. "I can't go into details about some of their concerns, but some of this has to do with Private Manning's safety as well."
