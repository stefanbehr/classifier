Pakistan's intelligence agency is buying time by legally prolonging the case against a jailed CIA-operative so the two agencies can wrestle out an agreement on covert operations by the U.S. inside Pakistan . 

The ISI is adamant that the CIA stop performing covert ops behind its back and wants to know exactly who is operating inside Pakistan . In return, the CIA wants continued access to the country. And if an agreement is reached, Raymond Davis - the American arrested for killing two men at a traffic stop, which he maintains was self-defense - will be set free. 

Fox News met with a U.S. Embassy source who confirms the CIA and ISI are making progress in accommodating each other's demands, helping to diffuse the bitter stand-off that is one of the biggest obstacles to U.S.-Pakistan relations. 

That means that while Davis could still be committed to criminal trial Wednesday, it would be a case of "kicking the can along" to buy time while the two agencies wrestle out a deal, says the source. 

"[The ISI] is going to torture us until it gets what it wants" the source said. "It is not in the United States nor Pakistan's interest to let this go on. If Davis is found guilty the U.S. would be outraged, and that would lead to a lot of problems for Pakistan." 

The plaintiff's lawyer says compensation will be made to the victims' families to close the chapter down the road. 



Sib Kaifee contributed to this report
