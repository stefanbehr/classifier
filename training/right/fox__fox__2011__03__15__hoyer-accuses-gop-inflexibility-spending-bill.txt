House Minority Whip Steny Hoyer (D-MD) accused House Republicans on Tuesday of being inflexible when it comes to breaking an impasse over spending. 

Hoyer says "there is not a consensus in their conference as what they should do." The Maryland Democrat believes that's why Republicans continue to stick by their guns and support a bill the House approved in February that trims $61 billion in federal spending over the rest of the year. 

"Take it their way or no way," Hoyer said."Why don't we just eliminate the rest of us and let them do what they want to do?" 

The House has now approved a temporary spending bill to keep the government open through April 8. In fact so many Republicans voted against the legislation that the package needed help from Democrats just to pass. But deep divides remain between the sides on how much Washington should spend on federal operations for the rest of the year.
