Fox News has obtained a copy of a "Dear Colleague" letter from Rep. Charlie Rangel, D-N.Y. in which he reintroduces a bill to require "national service" or a draft. 

The letter comes on a day the House is debating a bill from Rep. Dennis Kucinich , D-Ohio, that would require the withdrawal of U.S. forces from Afghanistan . 

Rangel introduced a similar draft bill in 2004 but it failed in the House by a vote of 402-2. 

The text of the Rangel letter is below. 

SUPPORT THE UNIVERSAL NATIONAL SERVICE ACT 

Dear Colleague: 

The test for Congress, particularly for those members who support the war, is to require all who enjoy the benefits of our democracy to contribute to the defense of the country. All of America's children should share the risk of being placed in harm's way. The reason is that so few families have a stake in the war which is being fought by other people's children. 

If there were a Universal National Service Act, there would be no shortage of troops to fill the ranks without repeatedly deploying the same exhausted troops over and over. 

I urge you to support my legislation for the Universal National Service Act as a co-sponsor. 

Requires all persons in the United States between the ages of 18 and 25 if called upon by the President during a declaration of war, a national emergency or a military contingency operation to perform national service for a minimum of two years with few exceptions. 

Cuts down the number of deployments for active duty and reserve military units who now see multiple deployments during the course of their enlistment due to troop strength shortages. 

Provides a National service to work education, health care, ports, security and other services as deemed necessary by the President. 

Benefits us ALL as Americans by helping ensure the United States is ready to protect and respond to our nation's needs at home and abroad at times of peace, national emergency or war if necessary. 

The question of whether we need a Universal National Service Act will be important as long as this country is placing thousands of its young men and women in harm's way. We make decisions about war without worry over who fights them. Those who do the fighting have no choice; when the flag goes up, they salute and follow orders. 

I urge each of you once again to cosponsor this bill as I bring it to the floor for legislation.
