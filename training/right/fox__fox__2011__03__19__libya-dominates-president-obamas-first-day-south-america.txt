On the first day of President Obama's first trip to South America, it was not relations with Brazil or its president that was front and center, but instead, attention was directly focused on Libya and the start of military action. 

Obama, who arrived early Saturday morning, was set to talk about economic partnerships with Brazil's President Dilma Rousseff and planned to speak to a U.S.-Brazil CEO forum. But as events unfolded in the U.S. and in Libya, the president was eventually forced to change direction and address the situation in Libya. 

The White House press corps had originally expected the president to take questions and perhaps talk about Libya earlier in the day at a press availability with President Dilma Rousseff. But, Rouseff nixed the questions and the president was only able to address Libya in a few brief lines. 

By mid-afternoon, as more and more information leaked out about on-going U.S. operations in Libya, the president - who had concluded most of his events for the day in Brasilia - did address the situation. 

"I am deeply aware of the risks of any military action, no matter what limits we place on them. I want the American people to know that the use of force is not our first choice, it is not a choice I make lightly," the president told a small group of reporters. "But we cannot stand idly by when a tyrant tells his people that there will be no mercy and his forces step up their assaults on cities like Benghazi." 

The president is in South America until Wednesday. He will be in Brazil until Monday and then travels to Chile and El Salvador. Obama returns to Washington, D.C. on Wednesday evening.
