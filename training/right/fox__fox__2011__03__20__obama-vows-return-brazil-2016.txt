For the second day in a row, President Obama promised a return visit to Brazil on Sunday while speaking in Rio de Janerio. The avid sports fan indicated he will come back to Brazil for the Summer Olympic Games in 2016. 

"You might have heard that this city wasn't exactly my first choice for the Summer Olympics," Obama said. "But if the Games couldn't be in my hometown of Chicago, there's no place I'd rather see them than right here in Rio. And I intend to come back in 2016 to watch it happen."
