Former Minnesota Governor Tim Pawlenty announced to FaceBook supporters Monday that he will form an exploratory committee, the first formal step in the process of launching a presidential bid. The video announcement pointed supporters to his new website . 

The video points to economic woes Pawlenty says he saw recently in stops across the nation. "America needs to grow jobs, limit government spending and tackle entitlements," Pawlenty said in the video. "That's why today, I'm announcing the establishment of an exploratory committee to run for president of the United States." 

The Minnesota Democratic-Farmer-Labor Party immediately panned the announcement, claiming Pawlenty had ignored issues at home while he was governor. 

"There's nothing in Tim Pawlenty's record that suggests he should get a promotion. Tim Pawlenty has failed at running the state he was supposed to lead at every turn - and no amount of exploring will change that simple fact," state chairman Ken Martin said in a release. 

For months political insiders have seen Pawlenty as a likely candidate for the 2012 GOP presidential nomination and with the formation of an exploratory committee, he can now begin raising funds, hiring staff and renting office space, all directly for a potential presidential campaign. 

Pawlenty had long said he would make a decision in March and today's news shows his budding campaign is right on schedule. Insiders tell us to expect a formal announcement in May. 

Pawlenty has spent considerable time in the early states' in presidential politics over the past year and will be in the First in the Nation Presidential Caucus State' of Iowa on April 1. Aides also tell us he will spend nearly the entire month of April in Iowa and New Hampshire. 

Fox News' Greta Van Susteren will sit down with Pawlenty for an exclusive interview on Monday evening. On the Record with Greta Van Susteren ' airs at 10pm EDT. 

Fox News' Carl Cameron contributed to this report.
