On his second country stop in South America, President Obama did not shy away from the drug trafficking epidemic affecting the United States and Latin America. Speaking in Santiago, Chile Monday, Obama expressed the concerns that gangs and traffickers threaten not only security of citizens but democracy for the region. 

He emphasized the importance of training and shared technologies so governments can work cross boarders to maintain safety. "We're improving coordination and sharing more information so that those who traffic in drugs and human beings have fewer places to hide," he said. "And we're putting unprecedented pressures on cartel finances, including in the United States." 

The address in Chile was the major speech of President Obama's five-day trip to the entire Latin American world. More focus on security is expected in his next country stop of El Salvador, the only Central American country Obama will visit this week. 

"The northern half of Central America is by far the most violent region in the world, and what lies behind that to a large extent is the drug trafficking situation," Kevin Casas-Zamora, the Former Vice President of Costa Rica told Fox News. 

The problem of organized crime and drugs is an issue for all the America's and the President thanked his counterpart in Chile during a joint press conference with President Sebastian Pinera. 

"Mr. President, I want to thank you for offering to share Chile's security expertise with Central American nations as they fight back against criminal gangs and narco-traffickers." 

But President Obama's commitment to security in the war on drugs may not be as front and center as some might hope. Just last week Guatamala hosted a security summit with all Central American Presidents meeting with UN Secretary General Ban Ki Moon. 

"If there was any intention of the US to make a significant commitment when it comes to security, I'm sure they would have found a way to coordinate agendas and have a regional summit about the issue," Casas-Zamora says, "They chose not to do it and I think that sends a message that should not be lost." 

With US budget uncertainty, the ability for the United States to deliver on the issue of security is for now, very limited.
