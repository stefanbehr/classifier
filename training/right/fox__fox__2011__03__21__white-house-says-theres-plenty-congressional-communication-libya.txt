Members of Congress have been complaining that the actions taken by the president on the Libyan No-Fly Zone were unilateral, and conducted without any Congressional input. 

But the White House insists a consultation process was in place and all members were briefed before any action was taken. 

White House Press Secretary Jay Carney and Senior Adviser Ben Rhodes both assert that not only were members of Congress consulted, but they were also called about the imminent action at the United Nations regarding the No-Fly Zone and there was an all members' briefing by Undersecretary of State Bill Burns on March 17 explaining what actions were being pursued at the U.N. 

"[W]e take very seriously the need to consult with Congress and we have been doing that. " Carney said. 

Rhodes pushed back even further telling reporters the actions taken were within the rights of the president and that Congressional support of the no-fly zone doesn't mean the two sides should stop speaking. 

"[W]e're going to continue to brief and consult going forward. But again, with regard to the specific question, an action that is limited in scope and duration is very much within the president's constitutional authority and has plenty of precedent, as well...That doesn't mean that we don't believe that it's absolutely incumbent upon us to consult very regularly and in a very robust way with Congress." Rhodes said.
