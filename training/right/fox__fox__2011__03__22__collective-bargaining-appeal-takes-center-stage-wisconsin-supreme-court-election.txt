Races for State Supreme Court positions generally happen without a great deal of headlines or bitter, partisan politicking. It is different this year in Wisconsin where all the politicking is bitter and even supposed non-partisan races for the bench have gotten caught up in the recent union flap. 

"The best is yet to come," says former State Democratic Chair Joe Wineke. "This is only heating up." 

The incumbent, David Prosser Jr., is being challenged by JoAnne Kloppenburg in the April 5th election for a seat on the state's highest bench. 

Prosser has earned a reputation for letting the law influence his decisions. "[Prosser] is an unpredictable, independent Justice," Michael Brozek, a lobbyist with Midwest Strategy Group of Wisconsin says. 

Still, Prosser previously spent time as a Republican and the state's assembly speaker. He ran a campaign ad before Governor Scott Walker jumped into the deep end of the controversy pool, suggesting he would complement Walker if re-elected to the court. 

In the great partisan battle that is now Wisconsin, Prosser has all the guns from the left aimed at him. "It's more of an anti-Walker thing, that ties Walker to Prosser," says Wineke. 

And the very divisive Budget Repair Bill that Walker championed, which strips state employee unions of most collective bargaining powers, will be the centerpiece of the State Supreme Court race. 

But more immediately, a temporary restraining order issued by Dane County Circuit Judge Maryann Sumi that has delayed the law from going into effect will take center stage. The ruling was immediately appealed by the Wisconsin Attorney General and is heading in the direction of the State Supreme Court. 

That restraining order goes back to the court challenge from the state assembly's Minority Leader Peter Barca. It says Republicans violated Joint Rule #3 of the Wisconsin legislature by abruptly assembling the committee that paved the way for the bill to be muscled through the Senate without 24 hours notice. 

"The public notice for the meeting was misleading and insufficient to apprise the news media that the majority of the Senate would take a vote to pass the bill," reads Barca's complaint. 

Republicans argued that Senate rule #93 keeps them in the clear. It reads as follows: 

Senate Rule 93. Special or extraordinary sessions. 

(2) A notice of a committee meeting is not required other than posting on the legislative bulletin board, and a bulletin of committee hearings may not be published. 

The GOP posted the meeting roughly two hours before the committee was assembled. Senator Scott Fitzgerald says that was a courtesy, also arguing in the court of public opinion that there was enough notification for the public and press to pack the Senate parlor. 

However, the big issues seem to be the case laws that set legal precedent. 

The opinion in LTSB v State says the legislature's compliance with rules of proceeding is exclusively within the province of the legislature, because "a legislative failure to follow [its own] procedural rules is an ad hoc repeal of such rules, which the legislature is free to do at any time." In plain speak, the committee rules are legal because the majority says they are. 

Another Wisconsin case, Lafollette vs. Smith, furthers the Republican argument. "Accordingly, courts will not intermeddle in purely internal legislative proceedings," the ruling states. 

Despite those precedents, Judge Sumi, still issued the restraining order. 

"That's incredibly weak," says Rick Esenberg, Law Professor at Marquette University. "That Judge's decision was awful." 

So, if the case makes it to the Wisconsin Supreme Court before the election, Prosser will issue an opinion as to whether Judge Sumi crossed over the separation of powers and "intermeddled" in the legislature. If the case doesn't make it to the top court before the election, Prosser will be perceived as a conservative vote and targeted by the left preemptively. 

But don't think that only Democrats are playing politics in the high court of the Dairy State. 

Daro Crandall, chair of the Douglas County Republican party says, "We can't just support Governor Walker. We need to stand up and back Prosser to keep these measures in place." 

However, Crandall concedes that the union supporters are doing a better job of mobilizing activists to impact the Supreme Court election. "It's a little more passionate on the left than on the right."
