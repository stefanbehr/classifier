Donald Rumsfeld thinks Muammar Qaddafi is part of an elite group. 

"If you wanted to pick the people you wouldn't want in office, Qaddafi would rank in the top five, six, or seven," declared the former Defense Secretary on Tuesday night. Rumsfeld told Fox News' Greta Van Susteren that he thinks Qaddafi belongs in the same group as the "obviously repressive" and "violently anti-U.S." leaders of Syria, Iran , North Korea , and Venezuela. 

Now that action has started against the Libyan leader, Rumsfeld says the international alliance against Qaddafi has approached Operation Odyssey Dawn backwards. 

"I've always believed that the mission determines the coalition, and the coalition not ought to determine the mission," he explained. Rumsfeld also questions President Obama's "reluctant leadership" and advance planning, while critiquing the overall U.S. response. "I suspect that one of the reasons that the administration didn't go to Congress is they didn't know what to ask for." 

He says the reason George Herbert Walker Bush didn't push for regime change in Iraq during the first Gulf War, is because that wasn't the goal the coalition had agreed on. But at least they agreed on something ahead of time. 

The mention of "regime change" offers a stark contrast to current calls for Qaddafi's removal, and arguments about whether or not that would be an appropriate goal given the restrictions of the UN Resolution. 

Rumsfeld also believes countries in the anti-Qaddafi coalition should have been selected after the mission was clearly defined. But, since that didn't happen, he offers a bleak prediction for the current group: "You are doomed." 

Long-term in Libya , Rumsfeld thinks Qaddafi could be a candidate to remain in control, unless the coalition establishes a strong leader and a clear resolve to end his four decade long reign, adding that it wouldn't even be very hard for Qaddafi to maintain power. 

Rumsfeld says all Qaddafi would have to do to is tell his people that whether they love him or hate him, they have no other options. "The way he would do that would be to inject fear into anybody who decided to oppose him because the mission of the coalition was not to eliminate his regime," explains Rumsfeld. "That would be public. That would be known. Once that's known, people would be quite reluctant to turn against what may very well end up staying in power." 

One little known story Rumsfeld shared might brighten the outlook, though. 

He says Qaddafi used to have a nuclear program, but he doesn't now. And he isn't even thinking about using a nuclear radiological weapon against the coalition because he doesn't want to end up like Saddam Hussein . 

"After the major combat operations and after Saddam was captured, when he was pulled out of the spider hole, apparently, Qaddafi went to some Western leaders and said, 'I do not want to be the next Saddam Hussein ," Rumsfeld recalls. 

One of the "Westerners" was rumored to be Italian Prime Minister Silvio Berlusconi, although that claim was never verified. 

Rumsfeld also made clear that despite Qaddafi's interest in chemical weapons, there was never any indication he was pursuing biological weapons.
