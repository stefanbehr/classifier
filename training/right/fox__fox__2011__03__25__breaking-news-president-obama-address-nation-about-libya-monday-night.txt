President Obama is scheduled to address the nation on the ongoing situation in Libya Monday evening from the National Defense University in Washington. He's slated to give a speech at 7:30pm, and according to the White House will talk about what's happening now and what's expected next. 

In a release, the White House said he will, "update the American people on the situation in Libya, including the actions we've taken with allies and partners to protect the Libyan people from the brutality of Moammar Qaddafi, the transition to NATO command and control, and our policy going forward." 

Making an address about U.S. armed forces at a military institution has been something the president has done before. He announced a surge in U.S. troops in Afghanistan from West Point in December 2009. 

Prime time is typically considered to start at 8pm ET, so this technically is not considered a prime time address, which the White House especially liked doing in the first year of the administration. 

Lawmakers - both Democrats and Republicans alike - have called for more briefings and discussion about what is going on, both for lawmakers and the American people. 

Some who consider our military action "war" have said he should make a formal address from the Oval office, similar to what other presidents have done in the past. The White House has refrained from calling the Libya involvement "war." 

Obama held a conference call with Congressional leadership Friday afternoon. 

The president first address the nation about Libya a week ago, and then spoke about the military action to enforce the no-fly zone last Saturday while he was traveling in South America. White House Press Secretary Jay Carney has been saying for days that the president will address the situation soon and several times.
