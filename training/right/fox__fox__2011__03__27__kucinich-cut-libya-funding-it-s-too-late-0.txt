Ohio Congressman Dennis Kucinich (D) says he's not the only one who wants to ban federal funding of the U.S. military action in Libya. 

During a Sunday interview with Fox News, Kucinich said he has bipartisan support for his amendment to stop taxpayer funding of the operation. 

Kucinich says Texas Congressman Ron Paul, North Carolina Congressman Walter Jones and Illinois Congressman Tim Johnson are just a few of the Republicans who are "on board," and added Reps. Pete Stark and Lynn Woolsey, both California Democrats, have also voiced support. 

"We need to move now to cut off funds while most of the action is being done in the air and at sea," said Kucinich. "Congress has the authority ultimately under the Constitution to say no to this war by cutting off funds." 

Kucinich plans to offer the amendment to the next Continuing Resolution or Omnibus Appropriations bill. 

"There are more members who I will be in touch with as we return this week," added Kucinich. "I think Congress needs to take a strong stand on this." 

Shortly after President Obama's decision was announced, Kucinich released a statement, saying, "This is a grave matter because such an attack is specifically without the required constitutional authority." 

Kucinich wasn't the only one to voice displeasure with the move. The president was welcomed back to Washington with a flood of criticism following his Latin America trip. 

Then Kucinich dropped the i-word: impeachment. 

This week, Kucinich told the Web site RawStory.com that the president's actions in Libya "would appear on its face to be an impeachable offense." 

When asked about his comments, Kucinich told Fox News, "There is no question the president exceeded his constitutionally authorized authority." 

This isn't the first time Kucinich has called an act "impeachable." Kucinich launched efforts to impeach President George W. Bush and Vice President Dick Cheney for various issues, including post-9/11 surveillance, the war in Iraq and the response to Hurricane Katrina. 

But does Kucinich think President Obama will actually be impeached? 

"No, there's not going to be an impeachment," said the Ohio Democrat. "But there has to be brought forward to the American people a discussion about what a president is permitted to do under any circumstances."
