Mitt Romney's Free and Strong America PAC announced Monday that his chief domestic adviser in his 2008 presidential bid has returned to join his PAC as policy director, another apparent building block for Romney's likely entry in the 2012 race for the White House . 

"It's good to be back with Team Romney, " Lanhee Chen posted on his twitter account soon after the news. 

Chen was a health policy adviser to Bush-Cheney in their 2004 re-election campaign,and served in the Bush White House as a senior policy and political aide at the U.S. Department of Health and Human Services, according to a news release. 

Romney's PAC also announced today that it has given $25,000 donations to the New Jersey GOP to "help state legislative races this year." 

"It is important to help candidates who are fighting for conservative principles whether in Washington or in the states. I am happy to support the New Jersey GOP as its leaders fight to cut spending and stand up for the taxpayer," Romney said in a statement. 

Romney has contributed $300,000 to congressional races and state Republican parties so far this year.
