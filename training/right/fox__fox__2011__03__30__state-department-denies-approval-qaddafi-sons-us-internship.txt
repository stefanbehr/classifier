The State Department is pushing back against the suggestion it "approved" an internship for Muammar Qaddifi's youngest son, Khamis, that had him working in several U.S. locations just prior to the uprising in Libya . During the internship with AECOM, an international engineering, design and construction company, Qaddafi reportedly spent time in San Francisco, Colorado, Houston, Washington and New York. 

But the company claims the State Department knew about -- and approved of the internship. 

"Early this year, we participated in a short-term educational internship for a 27-year-old MBA student, Khamis Gaddafi, from an accredited university in Spain and with the knowledge and approval of the U.S. Dept. of State," AECOM said in a statement. 

A State Department spokesman disputes that claim saying it "did not approve" the internship because it was private. Thus, the spokesman says, there is "nothing for the State Department to sign off on." 

He added that the department was aware of the internship because Qaddafi was the son of a foreign leader, but that it also tried to keep a low profile to avoid the appearance of government interference in the private internship. 

Khamis Qaddafi is reported to have returned to Libya to fight rebels in the uprising against his father's regime, but AECOM says it was unaware of any military connection. 

"We were aware of the student's family relationship, but we were not informed of any military connection whatsoever," a company statement said. "When we read reports citing the student's role in the crisis in Libya , we were shocked and outraged." 

The Air Force Academy confirms that Khamis visited the school in Colorado Springs February, just over a week before the Libyan uprising started. 

But a spokesman says Qaddafi wouldn't have learned anything of benefit to the Libyan military. 

The Associated Press contributed to this report.
