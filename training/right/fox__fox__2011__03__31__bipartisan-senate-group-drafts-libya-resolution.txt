A bipartisan group of senators, working with the Obama Administration, is drafting an authorization resolution for the mission in Libya. 

Sen. John McCain said Thursday he is working with Foreign Relations Committee Chairman John Kerry , D-Mass, GOP Leader Mitch McConnell, R-Ky., and Joe Lieberman , I-Conn., to make sure any resolution they develop contains "language that can receive an overwhelming vote in the Senate. It would not be a good signal, otherwise." 

The group has "not yet decided" whether the resolution will be a symbolic measure, known as a "sense of the Senate" resolution, or something binding. 

The group is also working with the Administration. "We want to try pass something that they can be supportive of," said McCain, top Republican on the Armed Services Committee. 

"I think it's very clear that a majority of my colleagues want some statement from the Senate on this issue," the senator added. 

An aide to McCain said the group wants to introduce something "sooner rather than later," as there is a growing concern that some senators could introduce a resolution to force an end to the current mission in the north African nation. 

Stopping short of that for now, Sen. Rand Paul , R-Ky., condemned the current military action on Wednesday as he introduced a nonbinding resolution that quotes directly from President Obama who, in 2007 as a presidential candidate said, "The President does not have power under the Constitution to unilaterally authorize a military attack in a situation that does not involve stopping an actual or imminent threat to the nation."
