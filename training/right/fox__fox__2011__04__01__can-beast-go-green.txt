On the day when President Obama unveiled his "green fleet initiative" to ensure all new government vehicles run on electric, hybrid or alternative fuels by 2015, White House Press Secretary Jay Carney was asked if the presidential limo, nicknamed 'The Beast' was among those vehicles. 

"You know the limousines are part of I believe, run and operated and commissioned by the United States Secret Service," Carney said. 

Secret Service Special Agent Robert Novy told Fox News, "certain specialized vehicles with law enforcement and security specifications are not subject to this directive." The Beast is among them. 

President Obama rides in a custom-built black Cadillac which made its debut on his inauguration in 2009. Most details of "The Beast" also known by the White House as "Cadillac One" are classified for security purposes. The vehicle is fitted with military grade armor at least five inches thick with doors as heavy as those of a Boeing 757 airplane. 

Last year Mr. Obama was asked if his limousine would receive any hydro-electrical updates. The president himself explained the cars he rides in are heavily reinforced. "So they weigh twice or three times what an ordinary car weighs. So they just couldn't get the performance, in terms of acceleration, using a hybrid engine," Obama said at an energy event in 2010. 

A new report shows last year federal governmental vehicles used more gasoline than in the last five years, but Obama promises to cut those emissions 30 percent by 2020. A large part of that commitment is through the efficiency of new purchased government vehicles. The Beast, will not "go green".
