False statements and "misperceptions" about the Obama administration's efforts along the Southwest border are contributing to a faltering economy there, according to Homeland Security Secretary Janet Napolitano . 

"Damaging misinformation about border communities has been repeated so often it's almost become a given in American life," Napolitano said Friday during a forum on immigration issues in Washington. "Everybody's saying the border's out of control, it doesn t work, it's not safe, it's not secure. And that means for [border communities] they can't recruit business there. It means that colleges can't recruit students there. You name it, they have after-effect after after-effect after after-effect." 

She said such "misperceptions" are "hurting" cities like San Diego, Calif., El Paso, Tex., and Nogales, Ariz., whose residents "live on, work in, and need jobs in communities along this side of the border." 

"You've got to get below the folks that are volunteering to be on the cameras," said Napolitano, who met with a series of business leaders and business groups in El Paso last week. "You've got to go down below and actually talk to the business owners, the people that are involved in economic development efforts -- those individuals -- and they will tell you almost uniformly that they're having a hard time because they're constantly fighting this image that somehow this is an area of the United States where the rule of law is not being enforced." 

The mayor of Yuma, Ariz., agreed, blaming misperceptions on a "national message that is inaccurate." 

"I'm not in denial nor are any of the other border communities about the challenges that we face with the drug cartels," Mayor Alan Krieger said during a panel after Napolitano's remarks. "It's real. It's dangerous. ... But I can't afford to let the overriding message of border wars simply rob us of an economic opportunity to create jobs. So I want to tell business, I want to tell the chamber-of-commerce industry that Yuma, Ariz., is safe, secure and ready for business. And that rings true for a lot of other communities." 

Despite his city's unemployment rate of 23 percent, Krieger expressed hope for the future, especially with help through grants from the federal government. 

"The reality is we're suffering economically, but we're tough," he said. "We're going to find a way through this thing." 

Napolitano took particular issue with suggestions -- "sometimes made to score some political points," she said -- that the U.S.-Mexican border is "overrun or out of control." 

Administration critics along the border, such as Pinal County Sheriff Paul Babeu in Arizona, have long questioned Napolitano's assessment of her own department. 

"The threat from an unsecured border is real, where 241,000 illegals were apprehended last year by the border patrol and an additional 400,000 got away just in Arizona alone!" Babeu wrote in February, responding to a group of fellow Arizona mayors who were asking him to tone down his rhetoric. "These are failing grades by anyone's score card. ... [The administration's] agenda is clear: They want 'immigration reform' and must convince the public that everything is just fine." 

On Friday, though, Napolitano said such statements are "just plain wrong." 

"Continuing to make these assertions in the face of everything that's happening and everything that's been done not only has negative consequences for our own border communities but it also disrespects the significant efforts of the law enforcement men and women we have put down on that border who work every day -- day in and day out -- to make sure that we do have a safe and secure border region," she said. 

In their letter to Babeu in February, the Arizona mayors similarly expressed concern that the immigration debate's tone could impact their cities economies. 

"While your misstatements about efforts to keep communities along the U.S.-Mexico border may keep national media coming to Arizona, at the same time your consistent inaccuracies hurt cities and towns like ours by causing those who live and travel to the border to fear for their safety when in our communities," said the letter, signed by Nogales Mayor Arturo Garino, San Luis Mayor Juan Escamilla and Douglas Mayor Michael Gomez. 

On Friday, Krieger described the back-and-forth as "turf battles" between elected officials. 

Napolitano, meanwhile, said, "It's time for the American people to understand that the investments we have been making along the border are producing significant and tangible results." 

While she said she's "deeply concerned" about Mexican drug cartels and the threats they pose, Napolitano said the Obama administration has employed "the biggest surge of manpower and technology to the Southwest border ever, and it represents the most serious and sustained action to secure that border in our nation's history." 

"Every key metric shows that that approach is working," Napolitano said. She has often cited her department's "record" number of deportations for illegal immigrants, especially convicted criminals. On Friday she also noted that Border Patrol apprehensions, which she described as "an indicator of how many people are trying to cross illegally," are down at least 36 percent in the past two years. 

But some Republicans have taken issue with that metric. 

"The administration has touted the decrease in the apprehensions along the Southwest border as an indication of fewer illegal crossings and therefore successful deterrence," Rep. Robert Aderholt, R-Ala, said during a House hearing on border enforcement March 16. "How do we really know that is the case? For the entire history the so-called denominator has been evasive." 

On Wednesday, Sen. Joe Lieberman , I-Conn., and John McCain , R-Ariz., said in a statement the administration "has no good indicators to measure whether the decreased apprehensions are, in fact, a result of the increased funding or a weaker economy which attracts fewer job seekers from Mexico." 

Nevertheless, violent crime in border cities has continued to plummet, Customs and Border Protection Deputy Commissioner David Aguilar said during Friday s panel, which was hosted by Washington-based think tank NDN. 

In Tucson, Ariz., he said, violent crime has dropped 17 percent since 1992. In San Diego, Calif, it dropped 20 percent, and in El Paso, Tex., it dropped 34 percent. McAllen, Tex., saw a drop of seven percent.
