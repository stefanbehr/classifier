Donald Trump says the U.S. is being ripped off by the rest of the world, and wouldn't be if he was President. 

"If I decide to run, we are not going to have the kinds of problems we have now because I won't be taken advantage of by the rest of the world," Trump told Fox's Bill O'Reilly on The O'Reilly Factor Thursday. 

"We are being ripped off by the rest of the world to the tune of trillions of dollars," Trump said. "I am not talking about the old word, billions. I am talking about the new word, trillions." 

Trump says he knows just how to fix things, starting with China. 

"Twenty-five percent tax on China, unless they behave," he told O'Reilly. 

"You're threatening China with a trade bill. Twenty-five percent tariff. That's big," O'Reilly retorted. 

"No, they're threatening us. They're going to make a three hundred billion dollar, let's call it profit, this year on the United States." 

Trump says the mere mention of a twenty-five percent tax will scare China into coming to the negotiating table. 

"You're never going to have to do it because they are going to come to you. They're going to call you and say, 'What do we have to do?'"
