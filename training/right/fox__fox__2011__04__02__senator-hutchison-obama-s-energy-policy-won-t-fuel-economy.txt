"We must expand oil and gas production in our country." That's the response from Senator Kay Bailey Hutchison , R-Texas to President Obama's newly announced energy policy . 

In this week's address, the President said that he wants to cut the country's oil imports by one-third over the next ten years. But, Senator Hutchison says that in order to do that, we need to use the resources in our own backyard. 

"We are the only country in the world that has vast resources that we are not tapping and we will not be able to cut one-third of our imports without developing our own resources," she said during an interview on Fox News on Saturday. Hutchison sees promise in the President's words, but now is looking for action. "Right now, they are not permitting in the deep water in the Gulf of Mexico which is our second largest opportunity. Alaska is our first largest opportunity and we are prohibited from most of the drilling in Alaska." 

But, unlike the Texas Republican, the President said doesn't see drilling for oil domestically as the solution. ""Even though America uses 25 percent of the world's oil, we currently have only about 2 percent of the world's oil reserves. Even if we used every last drop of all the oil we have, it wouldn't be enough to meet our long-term energy needs."
