As we wait with bated breath for the federal document filing to officially launch Obama For America 2012, let's take a look at a few contenders you may not have heard of (and some you have). 

President Emperor Caesar and Rutherford B. Hayes. Ordinarily this would be a great thing for the American public: two proven leaders, the 19th president of the United States and the former Emperor of the Known World have risen from the grave to run for the nation's highest office. 

Not quite. 

A candidate for the "Government All Party's Democratic Organization," Caesar is actually running to be "District of Colombia President," according to FEC papers he or she filed March 24. The former Roman dictator, and current Florida resident, is no stranger to Presidential campaigns, having filed in 2008 too. 

Hayes seems to have taken a more traditional approach, first filing a political action committee and then his presidential papers in 2010. But there's a catch: this Hayes's B stands for Bert, not Birchard, the former president's middle moniker. And Bert Hayes is reportedly absolutely unrelated to the former president. 

Jonathan "The Impaler" Sharkey. As a democracy, the United States might not be ready for a candidate who draws his name from the brutal Romanian prince whose tyrannical rule may have inspired Bram Stoker's "Dracula." But fear not--Sharkey is a man who is apparently sensitive to the needs of the people. 

According to letters filed with the FEC, Sharkey withdrew his 2008 candidacy to appease his girlfriend. "I filed this letter, because I felt my candidacy was causing my girlfriend grief," Sharkey wrote in July 2007. 

"However," the letter continues, "Sheila is a lot stronger than most candidates' girlfriends or wives. Hence, I am OFFICIALLY [emphasis as original] withdrawing my letter from 5 Jun 07, and stating I am still a candidate for President in 2008." 

With that record, opponents may be quick to paint "The Impaler" as a flip-flopper. 

Jeff Howard Taft Davis. Someone's parents must have been big history buffs. Davis shares names with two American notables--Jefferson Davis, president of the Confederacy, and William Howard Taft, 27th president of the U.S. 

Taft may enjoy the distinction of being the only president to get stuck in a bathtub, but candidate Davis enjoys the distinction of being perhaps the only candidate on this list to have a real organizing structure, with an extensive platform grounded on "world peace."
