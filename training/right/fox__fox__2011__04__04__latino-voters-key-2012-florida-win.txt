From the Cuban-Americans in Miami to the Mexican Americans of California, Latin Americans in the United States now number more than 50-million strong. 

"One in six in the United States is Latino. One in four children is Latino," says Gloria Montano-Greene of the National Association of Latino Elected Officials. "This is a great opportunity for the country to be more inclusive of the Latino community in their agenda."According to N.A.L.E.O, "their agenda" is education, economy and immigration, and for Political candidates in 2012, those are the 3 issues--and a growingly powerful demographic--that will have to be addressed. 

Both parties have created "Latino outreach" positions in the Sunshine State, to work heavy, Hispanic areas like Miami-Dade County, get more voters registered and, they hope, ensure Latino men and women vote for their candidates. 

In 1980, America's Latino population was just under 15 million. Since then, it has more than tripled. 

The states with the largest Latino populations are California, with 13 million (37 percent of the state,) Texas, with 9 million (36 percent,) Florida with 4 million (21 percent) and New York, with 3 million (17percent). 

Yet of those four states, only Florida is a true swing state, the fourth largest of the nation, with 29 electoral college votes. This makes capturing the majority of the Hispanic vote in the Sunshine State more relevant and more critical to winning in 2012 and beyond. 

"The past election here in Florida indicates that if you win Hispanics, you win the general election," says Christopher Mann, a political science professor at the University of Miami. 

In November, Republican Marco Rubio became Florida's junior U.S. Senator, pulling 55 percent of the state's Latino vote, but two years before, in 2008, President Obama took Florida, winning 57 percent of the states Latino vote. 

Historically, though, Hispanics vote 60 percent of the time for Democrats and 38 percent of the time for Republicans. 

That is not to be assumed, though, in 2012 and beyond. Political analysts say the Latino vote is up for whomever best addresses the key concerns of education, economy and immigration. 

"Whichever candidate is more successful in avoiding mistakes in appealing to the Hispanic vote, probably wins this state in the 2012 election," says Mann. 

So, once again, it may all come down to Florida, and specifically, Florida's Latinos.
