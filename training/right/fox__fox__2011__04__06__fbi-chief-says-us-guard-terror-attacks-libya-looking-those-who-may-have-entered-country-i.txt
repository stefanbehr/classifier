The U.S. homeland is "on guard" for terrorist attacks "emanating" out of war-torn Libya , as Libyan operatives "may well" be in the United States after possibly crossing into the United States illegally from Mexico or Canada, according to the head of the FBI. 

That assessment came one day after the FBI confirmed it was contacting Libyan nationals living in and visiting the United States in an effort to "determine whether there is a threat to Americans." Testifying before a House Appropriations subcommittee Wednesday, FBI Director Robert Mueller offered more details about his agency's efforts. 

He said the FBI wants to "make certain" the U.S. government has sufficient information "that may bear on what is happening in Libya ," including the identities and aims of rebel forces fighting to topple Muammar Qaddafi's brutal regime. 

"We also want to make certain that we are on guard [for] the possibility of terrorist attacks emanating somewhere out of Libya , whether it be Qaddafi's forces or, in eastern Libya, the opposition forces who may have amongst them persons who in the past have had associations with terrorist groups," Mueller said. He noted that coalition forces are still trying to figure out exactly who is part of Libya's rebel forces. 

Nevertheless, Mueller said intelligence officers from Libya "may well" be part of "establishments" in countries outside Libya, or "there may be intelligence officers that are operating with different types of cover in the United States." 

"We want to make certain that we've identified these individuals to ensure no harm comes from them, knowing they may well have been associated with the Qaddafi regime," he added. 

Ensuring no harm comes from them involves talking to intelligence officers themselves. In addition, other individuals inside the United States who represented the Libyan government or Libyan international organizations but have since "renounced" or "denounced" Qaddafi may be especially willing to talk with the FBI, according to Mueller. 

He said visitors from Libya such as students may also have useful information, including intelligence "that may alert us to any attempts at retaliation within the United States or elsewhere by pro-Qaddafi individuals." 

Asked whether Libyans might enter the United States illegally through the nation's southern or northern borders, Mueller said "that has been a concern." But, he noted, the FBI works "very closely" with U.S. Customs and Border Protection and many illegal immigrants from war-torn countries may "have a legitimate claim to asylum and [are] not in any way associated with terrorism." 

U.S. counterterrorism officials have privately discussed the need to prepare for a possible "exodus" to the United States of former security officials and other officials who were loyal to governments recently toppled or on the verge of collapse, an official said Tuesday. Some could feel a "sense of betrayal" or be "embittered" that the U.S. government stopped backing their governments, the official said. 

In fact, counterterrorism officials from some of the countries recently besieged by unrest have been trained by the U.S. military and the FBI. As for Libya, the FBI currently does not have any agents on the ground there, according to Mueller.
