The Obama administration has reached a free trade deal with Colombia, the White House announced Wednesday. 



The agreement came after Colombia addressed human rights concerns and ensured they'd improve protections for workers. 

The White House said the deal was vital for the president's plans for the economy and jobs, saying it would pump U.S. exports up to more than $1 billion a year. 

Republicans have long supported the deal and used it as a negotiating tactic to hold-up other nominations and bills on Capitol Hill . 

Senator Orrin Hatch, R-Utah, who is on a key committee overseeing trade, praised the news. "While long overdue and despite unreasonable delay, today's announcement by the administration is welcome news. Colombia has emerged as one of our strongest allies in Latin America and our workers, farmers and job creators can no longer afford to be put at a disadvantage in this growing economy," said Hatch in a statement. 

Some Democrats, on the other hand, are concerned that there won't be enough time for Colombia to fulfill the promises it made as part of the deal prior to Congressional approval. In a statement released Wednesday afternoon, a handful of House members, including Democrats Linda Sanchez, D-CA, and George Miller, D-CA, said, "When so many lives are at stake, we do not believe this is a matter to be rushed to Congress until we are able to determine, including through consultation with the targets of violence and human rights violations in Colombia, whether genuine and lasting change has taken place on the ground - not just on paper, but in the reality that so adversely affects so many Colombians every single day." 



President Obama is scheduled to meet with Colombian President Juan Manuel Santos on Thursday at the White House where they will "discuss important issues related to our enduring bilateral partnership" and discuss the next steps of the agreement, the White House said in a statement. 

Under President George W. Bush, agreements with Colombia, Panama and South Korea were reached - however Congress never took them up for a vote. 

This gave the Obama administration and U.S. Trade Representative, Ambassador Ron Kirk, time to clear out provisions they didn't like. 

The South Korean agreement was re-worked late last year and is pending a final vote, and the Panama one is being held up over tax laws. 

The next step for the Colombia one is Congressional approval. 

The business community also hailed Wednesday's development, with the U.S. Chamber of Commerce President Tom Donohue saying, "The U.S.-Colombia trade agreement will level the playing field for U.S. workers, farmers, and companies by immediately eliminating Colombian duties on more than 80 percent of U.S. exports." 

Fox News' Mike Emanuel contributed to this report.
