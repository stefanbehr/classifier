The potential for a government shutdown has President Obama postponing his scheduled visit to Indianapolis, Indiana on Friday, April 18. The White House Press Office released a statement late Thursday evening announcing the President would "no longer travel to Indianapolis..." where Obama planned to tour the Allison Transmission manufacturing plant and speak to workers there. 

Allison Transmission is the world's largest manufacturer of transmissions for heavy duty vehicles and is known as a leader in hybrid technology. President Obama was expected to highlight his plan to reduce America's dependence on foreign oil by decreasing oil imports and promoting alternative energy. 



Earlier Thursday the leader of Indiana's Republican party , Eric Holcomb took aim at President's Obama's scheduled visit to Indiana. In a conference call Holcomb told reporters, he appreciated President Obama "wandering the state from time to time. He (Obama) realizes what we know. Getting reelected (in 2012) will be tougher this time around." 

President Obama announced his intention to run for reelection in 2012 earlier this week.
