The irony is that a shutdown might not be averted, even if the sides forge a pact Friday. 

Government funding is slated to expire at 11:59:59 Friday night. But there is a problem with that. On Wednesday night, House Appropriations Committee Chairman Hal Rogers, R-Ky., told me that he needs "drafting time" and "a few days" to get together such a bill. 

So even if there is an agreement, there is NOTHING to tide the government over. Unless both houses of Congress can approve what Rogers calls "a plain, old CR." 

The Senate could still take up the bill that the House approved today, with $12 billion in cuts and government funding for the week. But as of today, senior Senate sources say that would be unlikely. 

Moreover, House Speaker John Boehner , R-Ohio, doesn't seem to have the votes from conservatives and rambunctious freshmen who say they won't vote for a regular CR just to keep the government open. 

So, this is reminiscent of the scenario in October, 1990...not the legendary, lengthy shutdowns of 1995-96...when the government closed for three days over the Columbus Day Weekend. It didn't impact much...just national parks and the Smithsonian. 

Boehner could perhaps persuade his side to vote for an interim CR...if they get some healthy items in return in the broader deal. It would require some arm-twisting...and Democratic help for sure. But it could be done. 

Remember, both Senate Majority Leader Harry Reid and Boehner are CONSUMMATE dealmakers...part of the reason they made it to their respective posts. 

Also, these sorts of crisis "government shutdown" moments tend to have lengthy political echoes in American politics. In 1990, President George H.W. Bush went back on the touchstone of his 1988 presidential campaign. Bush broke his "no new taxes" pledge. Then-House Minority Whip Newt Gingrich (R-GA) revolted and believed the president sold out conservatives. 

Bush lost in 1992 and Gingrich ushered in the Republican Revolution as the GOP claimed the House in 1994 and annointed Gingrich as speaker. 

In 1995-96, Gingrich was riding high. But President Clinton's victory after the shutdown permanantly undercut Gingrich and he was never the same again as speaker.
