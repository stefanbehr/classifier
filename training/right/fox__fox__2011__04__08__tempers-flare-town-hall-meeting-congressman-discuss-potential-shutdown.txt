Tensions were high at an emergency town hall meeting held by Rep. Jim Moran, D-Va., to address concerns regarding a potential government shutdown. Frustrated constituents shouted from the audience, asking the congressman to work longer hours and through the upcoming two week holiday break. A large number of federal workers live in Moran's Northern Virginia district. 

He mentioned that this was the sixth continuing resolution and attempt to finalize funding to keep the government from shutting down, but said he "saw no reason why [lawmakers] would not keep the government running at the same level of spending for just a few days." 

According to Moran, each federal agency will use its discretion to determine priorities that would still be funded, and he reminded those present that most federal workers were reimbursed for lost wages the last time the government shutdown. But he added, "you should not take that for granted." 

Then tensions escalated during the question and answer session. 

Moran stated that it was House Speaker John Boehner , R-Ohio, and the vast majority of new members, many in support of the Tea Party , that want to stick to cutting $100 billion from the 2011 budget and that they weren't specific about cuts. 

Then a retired service member of 27 years stood up and asked Moran why Congress was not in session at the moment. As he spoke he became more passionate about what he believed was a lack of effort on the Congress's part to prevent a shutdown. 

He continued, bringing up a potential lack of pay that could occur for soldiers serving overseas and became more outraged at the notion of Congress continuing with their Easter break. 

Moran explained that it was the Republican leadership's choice as to when Congress recesses and debates. 

"What are you going to do to fix it?" the man yelled at Moran, instigating a quick shouting match. 

Moran stated that he wants to keep Congress open until the issue is resolved. 

After things calmed down, the congressman took questions on Social Security checks, last year's omnibus bill, the effect on small businesses, and guidance for younger federal workers to deal with withheld paychecks.
