House Minority Whip Steny Hoyer, D-Md., the Democrats' top vote counter in the House, says he doesn't know yet if he will support the new continuing resolution posted overnight. 

Hoyer points out that the new bill was "filed late last night" and that his staff is sorting through the it now. 

"There are a number of things I don't like," Hoyer said. 

In addition, Republicans lost 28 of their own on the interim bill approved early Saturday morning. The GOP can only lose 27 members before needing help from Democrats to pass legislation in the House. 

Hoyer said he did not know if the GOP would ask him for help "whipping" votes on the Democratic side to see if they can approve the new bill. 

"My presumption is we don't know where our people are and I don't think they know where their people are," Hoyer said.
