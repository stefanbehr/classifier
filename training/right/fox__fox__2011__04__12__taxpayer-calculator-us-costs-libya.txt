As bombs started to drop in Libya last month , the cost to U.S. taxpayers began to soar. 

Almost 200 Tomahawk cruise missiles were fired from U.S. ships and submarines, costing $1.5 million each. Operating the F-15 and F-16 fighter jets rang up at $10,000 an hour. Almost 500 one ton warheads were dropped, each one: $40,000. A downed F-15, cost an estimated $31 million. 

CLICK HERE TO SEE YOUR SHARE WITH OUR TAXPAYER CALCULATOR 

According to General Carter Ham, who oversaw the American military campaign, the price tag to the U.S. is more than $600 million and growing. 



"We ought to think about all these things before we start blowing things up because that seems to be what we've been doing lately," said Congressman Buck McKeon, R-Calif., Chair of the House Armed Services Committee. "We blow things something up and then we go back and rebuild it." 

Experts say it's that rebuilding and stabilizing Libya that will continue to cost the United States and its allies. 

"If this continues on for months, instead of weeks, and our NATO allies aren't able to take over a large share of the cost, then the cost to the U.S. would start to mount quickly and we could see the administration wanting to put in a supplemental appropriation bill sometime later this summer ," said Todd Harrison, a senior fellow at the Center for Strategic and Budgetary Assessments. 

Harrison estimated that a limited no-fly zone over Libya , which would cover major cities and locations of air strikes against rebels, would be in the range of $30-100 million a week. 

Arming the rebels could cost billions more. Harrison said weapons provided to Afghan rebels against the Soviet Union in the 1980s cost approximately $1 billion. 

"If the rebels do succeed and they do oust Qaddafi from power, then we immediately have to start thinking about, how do we stabilize the country, establish a secure environment and in parallel you want to begin the process of economic reconstruction and try to get some economic growth because at the end of the day, economic growth and stability go hand in hand," said Harrison. 

So how much does the $608 million spent so far on U.S. military action in Libya cost you? 

Click here to check out the taxpayer calculator.
