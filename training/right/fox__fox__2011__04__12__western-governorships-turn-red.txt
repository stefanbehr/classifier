After the 2008 election there were six Democratic governors in the West (excluding the west coast states and Texas). Now there are only two, John Hickenlooper in Colorado and Brian Schweitzer in Montana. 

Schweitzer, the popular Democratic Governor of Montana will step down at the end of his second term in 2012 because of term limits. With no Democrats in the state who can match his popularity and electability, this seat is considered a toss-up at best, meaning that after 2012, Colorado's Hickenlooper may be the lone Democratic governor in the inner West. 

But before the GOP becomes overconfident, it should remember that historically, independent minded westerners swing back and forth. In Colorado for example, about a third of registered voters are registered as unaffiliated. And significant numbers of those registered Republican or Democrat have shown a willingness to cross over. 

Hickenlooper, a former Denver mayor, avoided the red tide sweeping the West and the nation in 2010, garnering 51 percent of the vote. Though popular in heavily Democratic Denver, his victory was far from assured until Colorado Republicans nominated little known businessman and Tea Party favorite Dan Maes, who had never before run for office. 

Maes' controversial public comments calling Denver's bike sharing program a covert attempt at converting Denver into "a United Nations community," and financial shenanigans (he paid $27,000 in penalties and fines for violating state campaign finance laws before the election was even held) prompted former Republican Congressman Tom Tancredo to jump into the race on the American Constitution Party ticket. 

Even if combined, Tancredo's 37 percent of the vote and Maes' 11 percent could not have stopped Hickenlooper after the race became a national joke. 

Soon after, Colorado GOP Chairman Dick Wadhams stepped down, saying in a statement, "I have loved being chairman, but I'm tired of the nuts who have no grasp of what the state party's role is." 

By "nuts" Wadhams meant Tea Party enthusiasts unwilling to compromise principles in order to win races. Wadhams went on to predict that, "The ability of Colorado Republicans to win and retain the votes of hundreds of thousands of unaffiliated swing voters in 2012 will be severely undermined." 

Veteran Colorado political analyst Floyd Ciruli agreed, noting that, "Democrats are smiling. Not only was Dick Wadhams a master campaign tactician, but he realized that in a high turnout Presidential race, Colorado Republicans must reach out and attract sufficient weak partisans and unaffiliated voters to get to a majority. It can't just be done with Republican loyalists and conservatives." 

A similar analysis could be made in Montana, where Governor Schwietzer has said bills introduced by Tea Party freshmen in the state legislature, "make Republicans look bat-crap crazy." 

A voter backlash there, if it does happen, could keep the governorship of the Big Sky state in Democratic hands.
