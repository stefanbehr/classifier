Former Senator Rick Santorum is formally exploring a run for the presidency, the Pennsylvania Republican announced Wednesday night."I've gotten a lot of great feedback from people, a lot of encouragement," Santorum told FOX News' Greta Van Susteren . "We've got a good strong presence on the ground. And now really the test for me is to see if we can raise the money that's necessary. So we're going to set up a committee." 

The outspoken social conservative and suspended FOX contributor has formed the " Rick Santorum Exploratory Committee," a fundraising account he called "a testing-the-waters effort." Former Massachusetts Gov. Mitt Romney , former Minnesota Gov. Tim Pawlenty , former Louisiana Gov. Buddy Roemer, and former Godfather's Pizza CEO Herman Cain , have announced exploratory committees. The Federal Election Commission website lists Romney, Pawlenty, and Roemer as having filed the necessary paperwork. 

For Santorum, the move comes in the midst of a months-long tour through early primary states, including over a dozen trips each to New Hampshire, Iowa, and South Carolina. 

Santorum's candidacy was called into question in March after he pulled out of a scheduled appearance at Iowa Rep. Steve King's Conservative Principles Conference when his youngest daughter, born with genetic disorder trisomy 18, fell ill. The father of seven has said his daughter's health would be a determining factor in his decision to run for the presidency. 

But asked whether he had resolved whether a presidential run would be the right move for his family, Santorum indicated he is now focusing on fundraising. "Well, resources [are] a huge part of it. You're out there to find out whether it's real or not," he said. "We're going to determine over the next few weeks as to whether the resources are going to be there to do it." 

Political handicappers contend the former Senator, who lost his Senate seat in 2006 by a third of the votes in his home state of Pennsylvania, would not be competitive in a field that is likely to include big names like former Governor Mitt Romney and former Speaker of the House Newt Gingrich . 

"You know, 2006 is not like 2010 or 2012, it's a very different election cycle," Santorum countered. "I think people have learned, from the 2006, and particularly from the 2008 election cycle." 

"The American people, you know, wanted a president that they could believe in, and I think after two years of that, what they have realized is that America needs a president that believes in them, and I really think that's the fundamental issue here." 

Santorum finished ninth in a home state straw poll at the conservative Pennsylvania Leadership Conference earlier this week. On Saturday, however, Santorum won a straw poll in Greenville, South Carolina, followed shortly thereafter by a smaller straw poll by a California home school association - a sign that the avid abortion foe and vocal critic of President Obama holds some appeal for conservative voters. 

Santorum will make his fourteenth trip to New Hampshire Thursday.
