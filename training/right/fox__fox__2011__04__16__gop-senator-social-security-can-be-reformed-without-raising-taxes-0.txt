Three Republican senators say they have a plan to "Make social security solvent for 75 years... without raising taxes or touching benefits for current retirees or for Americans older than age 56," so says one of those senators - republican Mike Lee , of Utah. 

The senators also say their Security Solvency and Sustainability Act "reduces debt held by the public by $6.2 trillion by 2085." 

So how would that happen? The plan would decrease benefits for wealthier Americans and increase both the retirement and early retirement ages. 

"A couple decades from now, we would end up with a system that everyone retired at the age of 70, and the early retirement age, would see a less drastic increase from 62 up to 64," explained Lee. 

Lee said that part of the problem is that Congress needs to "think about not only what their benefits might look like, but what their children and grandchildren might be able to expect. The more we can get people to look to the long term more I think they'll understand the need to do this," Senator Lee added. 

This legislation is expected to come up as part of the entitlement reform debate. 

Lee hopes that other members of Congress will offer their own proposals to fix Social Security. "If somebody else has the way of getting to the same end that is making this program solvent without increasing taxes I d love to see it and I welcome more proposals."
