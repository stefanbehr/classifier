Congressman Ted Poe is demanding answers from NASA . The Texas Republican says he was shocked to hear that Houston was denied one of the four retired space shuttles. 

On Tuesday, NASA Administrator Charles Bolden announced Atlantis will go to Florida, Endeavour will be sent to California and Discovery was awarded to the Smithsonian's Steven F. Udvar-Hazy Center near Washington Dulles International Airport. 

The prototype orbiter Enterprise, which was used for testing but never flew in space, will be sent to the Intrepid Sea, Air Space Museum in New York City. 

"NASA made a mistake," Poe said during a Sunday interview with Fox News. "People understand the center of space exploration is at the Johnson Space Center in Houston, Texas. It has been for almost 50 years. It is a historical snub, if you will, to take the shuttle and put it somewhere else." 

Poe added that sending Enterprise to New York was "like putting the Statue of Liberty in Omaha." 

After the decision was announced, Poe and other members of the Texas delegation sent a letter to Bolden. In it, the Texas lawmakers pose questions such as "What factors did you use in making your decision?" and "Are there any historical connections between NASA and New York City in general?" 

In a statement to Fox News, NASA responded, "We understand that some are disappointed with the selections for placements for retired orbiters. However, NASA undertook an exhaustive process, led by civil servants, to ensure these national treasures would be displayed at institutions where the greatest number of Americans could see them and learn more about their storied histories." 

The NASA statement closed with, "We stand by our process and the selections made." 

Poe says that answer "is not good enough." 

"Astronauts live here, their families live here. Several generations of people who came from all over the United States to start the NASA program live in Houston," Poe argued. "So we want the shuttle to be where it should be." 

Johnson Mission Control ran every shuttle mission, including Apollo 13, when the infamous line, "Houston, we have a problem" was spoken. 

When asked if he would consider a lawsuit to get a shuttle to Houston, Poe replied, "We are open to everything."
