Founder of the Faith Freedom Coalition, Ralph Reed, says yes, he did have a telephone conversation with Donald Trump about three weeks ago. But contrary to reports, Reed tells FOX News it was not an interview to become campaign manager for a possible Trump presidential campaign. 

In fact, Reed says he's spoken with a number of potential candidates, but not about jobs. 

"Because of my involvement with the Faith Freedom Coalition, I do not plan to take an active role in a presidential campaign in 2012," he told FOX News. 

That's not to say Reed's organization will avoid shaping the Republican presidential nomination race. The Faith Freedom Coalition has state affiliates around the country. 

For example, the Iowa Faith Freedom Coalition back on February 7th hosted the very first event attended by a block of potential presidential candidates. The Iowa organization is headed by RNC committee member Steve Schiffler, an influential figure in state Republican politics. 

Reed notes his national organization made 58-million voter contacts (largely Evangelical Christian and Roman Catholics) in 2010, in an election year which saw Republicans gains seats in the US Senate and take control of the House. 

And to the question many political pundits are asking aloud these days, "will Donald Trump actually run for president?" 

Based on that singular phone call, Reed says he thinks Trump is likely to run.
