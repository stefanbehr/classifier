President Obama regularly held basketball clinics throughout the winter for his daughter Sasha's basketball team, first lady Michelle Obama revealed on " The Gayle King Show " Tuesday. 

He also had a very official role for the team, and was an assistant coach. 

The team only had one loss this year, and it was actually the one game where the president was fill-in coaching, and Sasha was not present. 

The first daughter was on vacation in Vail, Colorado with the rest of the family. 

According to a pool report from that day, the president was stepping in for the regular parent coach. 



The Washington Press Corps knew the president played with his daughters and attended games on the weekends, often following him to the various events, but the first lady gave some new details. 

She described playing and coaching with the girls as one of her husband's "favorite" things to do. 

Dr. Jill Biden also noted their granddaughter plays on the same team. The first and second ladies described it when they're all there as the "safest rec gym in the universe." 

They added that their husbands at times may have gotten a little too into the game and they had to tell them to tone it down. 

"You see some of the kids say, is that the president yelling to play tighter defense?," the first lady said. 

The president regularly held clinics on Sundays for the girls (games were on Saturday) and even brought in professional players. Washington Mystics star Alana Beard attended one of them.
