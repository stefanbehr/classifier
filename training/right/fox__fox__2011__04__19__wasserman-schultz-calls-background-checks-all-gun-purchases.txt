The incoming head of the Democratic National Committee says she supports background checks for all gun purchases, and Rep. Debbie Wasserman Schultz , D-Fla., says she'll co-sponsor a bill in Congress to enact a new law requiring them. 

Wasserman Schultz announced her support for the measure at a Miami rally held by Mayors Against Illegal Guns. The Hill reports the Florida congresswoman, who is also set to take the helm at the DNC , told rally goers she was "outraged" by loopholes that allow buyers to purchase guns without a background check. 

"While we likely cannot end all gun violence, we certainly can do much, much better," Wasserman Schultz said. "We have laws on the books designed to keep guns out of the hands of those that should not have them. We just need to close the loopholes and improve the information available to law enforcement." 

So she plans to support the Fix Gun Checks Act Rep. Carolyn McCarthy, D-N.Y., plans to introduce later this year. That legislation would require unlicensed gun dealers to perform background checks whenever someone buys a gun, even at gun shows. Previously, only licensed gun sellers have had to perform the checks. 

Sen. Chuck Schumer , D-N.Y., has introduced a companion bill in the Senate that the National Rifle Association panned as a government overreach that ignores due process."... although Sen. Schumer touts his bill primarily as one to change the background check process, it would also greatly increase the number of people who are prohibited from possessing any firearm, with little regard for every American's right to due process before being stripped of a fundamental civil right," the NRA said in a statement on its website. 

Wasserman Schultz is a close friend of Rep. Gabrielle Giffords , D-Ariz., who was critically injured in a shooting spree that targeted her Congress on Your corner event in Tucson on January 8th. That mass shooting led to calls for new gun legislation. 

The gun allegedly used in the rampage was purchased legally after a background check was performed. Officials say Jared Lee Loughner, who was charged in the shootings, bought the gun legally despite a questionable mental background.
