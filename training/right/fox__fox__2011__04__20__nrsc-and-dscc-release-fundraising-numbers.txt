The two Senate re-election committees released similar fundraising numbers for the first quarter of 2011. 

The National Republican Senatorial Committee praises its first quarter fundraising, especially the $5 million it raised in March, saying it has exceeded its goals. 

"The NRSC met, and exceeded, those goals last cycle, and while we're still up against a Senate Democrat majority and the Fundraiser-In-Chief in the White House , we are committed to building on that success and winning back the majority next year," said Rob Jesmer, the NRSC Executive Director. 

The group has raised $11.2 million in 2011 but still has almost twice as much debt as cash on hand. Despite the debt, NRSC fundraising is trending upward when compared to previous years. 

Their counterparts, the Democratic Senatorial Campaign Committee, raised $5.6 million in March and has packed in $11.7 million in 2011. The DSCC has $5.5 million in cash on hand with a debt load of $3.75 million. 

"The strong support we're seeing so early in the cycle shows that we're going to be in a position to not only protect our majority next year, but also play offense in 2012," said DSCC Executive Director Guy Cecil.
