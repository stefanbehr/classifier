President Obama announced a new team to investigate potential fraud in the oil markets Thursday. The task force will be lead by Attorney General Eric Holder "to look into any cases of price gouging, so we can make sure no one's being taken advantage of at the pump," Obama said at a town hall even in Reno, Nevada. 

Acknowledging the rise in gas prices and the strain it has placed on American families, the president hopes the Financial Fraud Enforcement Working Group will "root out any case of fraud or manipulation in the oil markets that might affect gas prices". 

The task force, expected to include multiple Cabinet members, federal regulators and the National Association of Attorneys General, will focus on traders and speculators, said Mr. Obama. 

Democrats on Capitol Hill reacted favorably to the president's announcement. 

Senators Jack Reed , D-RI., and Carl Levin, D-Mich., released a statement expressing "hope [that] the Administration continues to seek out other avenues to fight speculation and abuse." 

As of the announcement Thursday, the national average for a gallon of gasoline was $3.84. The president called the price inching toward four dollars a gallon, "another hardship, another burden, at a time when things were already pretty tough."
