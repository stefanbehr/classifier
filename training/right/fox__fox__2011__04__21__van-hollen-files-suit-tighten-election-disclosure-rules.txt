It's going back to court. 

Rep. Chris Van Hollen (D-MD) filed another lawsuit Thursday to challenge Federal Election Commission (FEC) rules that curb some campaign finance declarations which many Democrats believe hurt them in the last election cycle. 

Last year, the Supreme Court ruled in favor of looser standards in the Citizens United v. FEC case, paving the way for many nonprofit groups to buy political ads. Many right-leaning groups then flooded the airwaves with ads targeting Democratic candidates. 

Democrats in Congress tried to combat the ruling through legislation the DISCLOSE ACT (short for "Democracy is Strengthened by Casting Light on Spending in Elections"). The House approved the measure but failed to break a filibuster for full consideration there. 

"The disclosure of campaign-donor information is essential to our democracy," said Van Hollen in a statement."The absence of transparency will enable special interest groups to bankroll campaign initiatives while operating under a veil of anonymity." 

Van Hollen is now the top Democrat on the House Budget Committee. But in 2008 and 2010, the Maryland Democrat chaired the Democratic Congressional Campaign Committee (DCCC), the national organization charged with electing Democrats to the House. 

"The lawsuit I am filing today seeks to restore the statutory requirement that provides greater disclosure of the donors who provide funding for electioneering communications. If this standard had been adhered to, much of the more than $135 million in secret contributions that funded expenditures in the 2010 congressional races would have been disclosed to the public," Van Hollen said.
