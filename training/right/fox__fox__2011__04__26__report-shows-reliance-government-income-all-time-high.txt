Americans are relying more on federal government aid than ever before according to an analysis of federal data conducted by USA Today . 

Last year, 18.3 percent of American income came from government programs such as Social Security , Medicare , unemployment benefits and food stamps, while earned income accounted for only 50.1 percent, the lowest number recorded. 

The percentage of government income Americans received hovered around 12 percent during the 80s and 90s but rose during the last decade, the report says, due to an aging population, the economic downturn and an expansion of health care benefits. The biggest increase took place in the back half of the decade as the recession took hold. 

When looking at those percentages as actual income, the amount of money the average American received from the government doubled from 1990 to 2010, increasing from $3,686 to $7,427, adjusted for inflation. 

The report notes that Medicare spending is set to skyrocket once Baby Boomers start to retire in the coming years. Most were still working in 2010. 

Click Here for the full USA Today report.
