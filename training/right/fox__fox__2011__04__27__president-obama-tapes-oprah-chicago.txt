President Obama traveled to his political home-town of Chicago Wednesday to tape an interview for one of Oprah Winfrey's final shows. The President and first lady sat down for an hour with the Queen of Daytime at her Harpo Studios in the Windy City. 

The Oprah Winfrey Show episode with the Obamas is scheduled to air on May 2, 2011. 

White House Press Secretary Jay Carney told the press Monday, "her show is coming to a conclusion of a terrific run, and I think, as you know, [the President] considers Oprah a friend, and he looks forward to being on the show with the First Lady." 

Ms. Winfrey actively campaigned for then-Senator Barack Obama in 2007 and 2008, holding rallies in the early voting states of Iowa, New Hampshire and South Carolina. The TV star also held a high profile, big-dollar fundraising gala for him in California. 

The first lady appeared on Oprah's program earlier this year where she announced her campaign for military families. President Obama appeared on Oprah in December 2009. This is the first time the first couple will appear on the program together.
