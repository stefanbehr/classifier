The U.S. ambassador to Libya says Muammar Qaddafi will only continue attacking rebels and protesters in his country and that it is time for him to go. 

"It has become clear that Qaddafi and his henchmen have no intention of stopping the violence," Gene Cretz said in an interview with Fox News. 

Cretz added that plenty of members of Qaddafi's government want to defect, just as former foreign minister Moussa Koussa did soon after the uprising began in March. But he says terror and fear in the Libyan capital of Tripoli have kept that from happening. 

"The time is fast approaching where they have to make a decision," Cretz said. "And they have to decide whether to go down with the ship." 

Italy, France and Qatar have recognized the Libyan Transitional National Council as the nation's government and the ambassador says he finds the Libyan Rebels to be "a credible body." 

"They continue to say the right things," he said. "They are working through the usual bugs in a transitional country where you haven't had politics for 40 years." 

Cretz adds that Qaddafi has to step down for Libya to move forward. 

"I don't believe anyone sees any credible solution to Libya without the removal of Muammar Qaddafi," Cretz said. 

Fox News senior House producer Chad Pergram contributed to this report.
