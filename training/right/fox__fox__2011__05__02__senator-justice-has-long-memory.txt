With reserved enthusiasm, senior Senate Democratic Leader Harry Reid, D-Nev., and Armed Services Committee Chairman Carl Levin, D-Mich., praised the president and the military and intelligence operations that killed Al Qaeda leader Usama bin Laden. 

The chairman added, quite pointedly, that Pakistani Army and Intelligence officials "have a lot of questions to answer given the location, the length of time, and the apparent fact that this facility was actually built" for bin Laden. And though he praised comments made Monday by Pakistani President Asif Ali Zardari. who called the operation "a great victory," Levin said he hoped the president would "follow through" and "ask some very tough questions of his own" of both groups. 

Reid told reporters that he had been briefed for months by CIA Director Leon Panetta on "the general aspects of what (intelligence officials) were looking at," and he added, "Today, Americans across the country are welcoming the news that this awful man, this man who epitomized evil, has been brought to justice by American forces. His death is the most significant victory in our fight against Al Qaeda and sends a strong message to terrorists around the world." 

"Justice has a long memory. It has a long arm," Chairman Levin said, adding that the "mythology of bin Laden" had been "punctured" following the successful strike. 

Reid said he received a call from President Obama at 9:30 pm EDT Sunday night. Levin was told soon after in a phone call with Defense Secretary Robert Gates while the chairman was at a Detroit airport en route to Washington. 

Both lawmakers praised the Navy SEAL's who conducted the harrowing operation in Abbottabad, Pakistan. Reid called the men "courageous" and recounted a story of one young man, Shane Patton, who was killed in action in Afghanistan in 2005. The Senate leader said he played ball with the young man's father. 

Levin said, "I was stunned by the capability of our special forces, the amazing performance of our men and women in uniform that they were able to pull this off. They never cease to amaze me in their skill, their confidence -- competence and their bravery." 

Both men praised the president, though Reid, with an unusual tinge of politics, added, "This...is a direct result of President Obama's efforts to refocus on Afghanistan and Pakistan as a central battleground in our fight against terror." 

The Nevada Democrat occasionally smiled as he recounted Sunday's events, but he offered one note of caution, "We should be very cautiously optimistic...I think that we still have a lot of work to do. That this man is out of circulation is a shot in the arm for justice all over the world. And I think that's going to be good for everybody."
