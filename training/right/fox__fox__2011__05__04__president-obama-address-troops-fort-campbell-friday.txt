President Obama will travel to Fort Campbell, Kentucky Friday to address servicemembers who have just returned from deployment, the White House announced Wednesday. The visit comes after a key victory for the U.S. military with the successful mission against Usama bin Laden. 

The president already plans private meetings on Thursday in New York City with family members of 9/11 victims and first responders. He'll also lay a wreath at the National September 11th Memorial at Ground Zero. 

The president connected with servicemembers at the White House Wednesday when he kicked off the Wounded Warrior Project's Soldier Ride on the South Lawn. 

"You've earned your place among the greatest generation of Americans," he told the group just before sounding the horn to start the event. 

"And we saw that again this past weekend when -- thanks to the courage and precision of our forces -- the terrorist who started this war and who took so many innocent lives learned that America does not forget; America will ensure that justice is done." 

Praising the military's commitment, Mr. Obama said, "Tour after tour, year after year, you've done your duty. You've met every challenge, from the deserts of Iraq to the mountains of Afghanistan . You've risked everything. And you've carried in your hearts the memory of fallen heroes who gave everything." 

The president will finish up his week with Fort Campbell's 101st Airborne Division, some of whom are just back from Afghanistan . Much of the division, however, remains deployed.
