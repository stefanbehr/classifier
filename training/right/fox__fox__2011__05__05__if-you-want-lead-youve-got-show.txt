Thursday marks a significant milestone in the 2012 presidential election. 

FOX News and the South Carolina Republican party are hosting the first presidential debate of the cycle in Greenville, S.C. and the participants say it's not too early to kick off the discussion. 

"One of the things about leadership is you've got to show up," said former Minnesota Governor Tim Pawlenty , taking a shot at some of his potential rivals who will not be participating in the debate. 

"If you want to be president of the United States, you've got to make the case to the American people that Barack Obama needs to be dismissed from his position," added Pawlenty. 

Pawlenty spoke to Fox News as he toured the debate stage. 

Fox News also caught up with Former Pennsylvania Senator Rick Santorum, who has spent more time in the early voting states than any other candidate this year and casts himself as the most unwavering conservative in the field. 

"These people who say, you know, put the moral issues aside or don't talk about the family, they don't understand what makes America work," said Santorum. "They don't understand the dynamic that is at play that has made America the greatest country in the history of the world." 

At the other end of the conservative spectrum is former New Mexico Governor Gary Johnson, a libertarian leaning Republican who supports gay rights and legalizing marijuana. 

"If Republicans don't have my name to check off then perhaps the name they check off isn't necessarily representative of what their views are," said Johnson. 

Former federal banker and Godfathers Pizza CEO Herman Cain toured the debate hall first and promises to bring an outsiders perspective as the only one on the stage who has not held high office. 

Texas Congressman Ron Paul's 2008 presidential run inspired the modern Tea Party movement and he has renewed his demand for US troops to leave Afghanistan and Iraq. It's a posture that may prove problematic in the Palmetto State, which boasts the highest population of active and retired members of the military in the entire country.
