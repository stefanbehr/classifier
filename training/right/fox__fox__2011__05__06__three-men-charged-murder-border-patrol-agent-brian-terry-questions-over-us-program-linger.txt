Just days after lawmakers repeated calls for justice in the killing of Border Patrol Agent Brian Terry, at least three men have now been charged in connection with the murder. The move, however, will not end questions over whether investigative tactics employed by parts of the Justice Department played a role in Terry's death. 

On Friday, authorities unsealed an indictment against Manuel Osorio-Arellanes and two other Mexican nationals, charging them with murder and weapons and conspiracy charges. Osorio-Arellanes was already in U.S. custody, but the other two, whose names remain under seal, are still at large. 

"Today's indictment is an important step in this case, but it is only a first step to serving justice on behalf of Agent Brian Terry, his family and the other agents who were with Terry and their families," U.S. Attorney Dennis K. Burke said in a statement. "This is an active ongoing investigation that is making more and more progress every day." 

In the days before the encounter that killed Terry, Osorio-Arellanes and others planned to confront U.S. personnel along the Southwest border, according to the indictment. On Dec. 14, 2010, the group, two of them armed with two assault rifles, began "patrolling" an area near Mesquite Seep, Ariz., and when they came upon Terry and three other Border Patrol agents, the two armed men opened fire even though the agents had identified themselves as police, according to the indictment. 

Osorio-Arellanes was wounded in the subsequent firefight. He was apprehended and has been in federal custody since on felony immigration charges. The others fled. 

Weapons recovered near Terry's body have been linked to an investigative program run by Justice Department officials aimed at Mexican cartels and their gun-traffickers. The investigation in Arizona was dubbed "Operation Fast and Furious," and it was part of a broader effort called "Project Gunrunner," launched in 2009 by the Bureau of Alcohol, Tobacco, Firearms and Explosives. The plan was to follow suspected straw buyers to see if they would illegally traffic guns and, ultimately, lead investigators to the heads of the enterprise. But hundreds of high-powered rifles and other guns ended up in Mexico, and self-described whistleblowers now accuse ATF of letting the guns "walk" to Mexico even after safety concerns were raised. 

Attorney General Eric Holder has ordered his department's internal watchdog to investigate whether guns were knowingly allowed to "walk" to Mexico -- and whether such guns may have had a role in Terry's death. Some Republicans on Capitol Hill , though, say they're already convinced the program played a part. 

"Federal authorities facilitated the purchase of assault weapons for drug cartels and chose not to interdict them before being transported to Mexico," the chairman of the House Oversight Committee, Rep. Darrell Issa, R-Calif., said Wednesday, a day after Holder appeared before his committee. "[T]the reckless decisions made in Operation Fast and Furious ... have created a serious public safety hazard." 

Issa has been one of the Justice Department's most vocal critics on the issue, using his congressional subpoena power to demand information from the department. Isse even sent a team of investigators to Arizona last week. 

On Wednesday, Issa released documents he obtained showing federal prosecutors in Arizona agreed with a plan "to allow the transfer of firearms to continue to take place ... in order to further the investigation and allow for the identification of additional co-conspirators who would continue to operate and illegally traffic firearms" to Mexican cartels. It's unclear, though, if prosecutors expected those guns to be sent to cartel operatives inside the United States or inside Mexico. 

In any case, according to a "briefing paper" from January 2010, investigators and prosecutors in Arizona determined "there was minimal evidence at this time to support any type of prosecution; therefore, additional firearms purchases should be monitored and additional evidence continued to be gathered." 

On Tuesday, under heated questioning from Issa, Holder said he took "great exception" with the congressman's assessment of events. 

"The notion that somehow or other this Justice Department is responsible for [Terry's death], that assertion is offensive," Holder said. "I have had to look into the eyes of widows, of mothers who have lost sons. I have felt their pain. And the notion that somehow -- in some way -- we are less than vigilant, less than strong in our determination to keep [safe] the people who put their lives on the line every day to protect the American people ... is inconsistent with the facts [and] inconsistent with the people who serve in the Department of Justice." 

The trial for Osorio-Arellanes has been set for June 17 in Tucson. 

"Agent Terry - who served his country honorably as both a Marine and a member of the Border Patrol -- made the ultimate sacrifice in service to the people of the United States," Burke, the U.S. attorney, said in his statement. "His family deserves to see justice served, and everybody involved in this investigation is deeply committed to making that happen."
