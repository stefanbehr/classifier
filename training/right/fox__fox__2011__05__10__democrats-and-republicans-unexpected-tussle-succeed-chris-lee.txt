In February, Republicans worried after former Rep. Chris Lee (R-NY) lost his shirt in pictures plastered all over Craigslist. 

Now they're worried about losing his seat in upstate New York. 

Lee abruptly resigned just hours after his sartorial indiscretions hit the web. 

And even though Lee's seat is historically Republican, a real contest is emerging in an upcoming special election for which party wins the first special election of the 2012 cycle. 

Republican Jane Corwin faces Democrat Kathy Hochul in the bid to succeed Lee. 



But then there's tea party candidate Jack Davis, who could be the spoiler in the race. 

Davis lost the Democratic nominee to Hochul. Davis then assumed the tea party mantle for this contest. 

But Republicans argue Davis isn't a typical tea party candidate. 

Less than five years ago, Davis ran for the same Congressional seat as a Democrat. In 2006, Davis nearly upset then-Rep. Tom Reynolds (R-NY). At the time, Reynolds chaired the National Republican Congressional Committee (NRCC), the political organization charged with electing Republicans to the House. And Reynolds presided over a 26 seat GOP loss as Democrats seized control of the chamber for the first time since 1994. 

Republicans are quick to say that if they lose the seat this time, they'll blame Davis. 

"This race is close because Jane Corwin is running against two Democrats who are attempting to steal the election and advance their party's spending spree in Washington," said NRCC spokesman Paul Lindsay. 

Democrats are optimistic they could pull an upset, regardless of what factors influence the contest. House Minority Whip Steny Hoyer (D-MD) said his party has "no expectations" and described the district as "very, very difficult" to win for Democrats. 

But Hoyer adds that Democrats could celebrate a win there as vindication that GOP plans to alter Medicare aren't popular. 

"Should we win, that would send a very clear message that (the Republican Medicare blueprint) is not reflective of what (voters) want done," said Hoyer. 

On Tuesday, the Democratic Congressional Campaign Committee (DCCC) released an appeal to potential donors stating that "just-released public polls show Republicans are in danger of losing a special election in a deep-red Western New York district." The message asserted that that Republicans are "scrambling to save the seat." 

A Democratic political operative tells Fox that the DCCC is placing $250,000 media buy in the district. 

Both House Speaker John Boehner (R-OH) and Majority Leader Eric Cantor (R-VA) campaigned for Corwin. Both of their visits were coordinated before the race tightened up. 

During a meeting with reporters Friday, Cantor described Corwin as "a terrific candidate" and dismissed efforts to portray the race as a referendum on potential changes to Medicare. Cantor said that jobs were the issue, describing Corwin as a job creator. 

"She's taken that message and I believe it will be successful," Cantor said. 

Republicans boast a 30,000 voter registration advantage in the district. The district runs west from the Rochester suburbs to the outer reaches of Buffalo. 

The special election is slated for May 24.
