You don't hear her name as much as you used to, particularly when dozens of Republican candidates converted her into a foil and demonized her during the 2010 midterm elections. 

But NRCC Chairman Pete Sessions , R-Texas, is resurrecting part of the GOP's 2010 playbook by infusing the name " Nancy Pelosi " into the competitive special election in upstate New York between Republican Jane Corwin, Democrat Kathy Hochul and former Democrat Jack Davis. 

In an interview with Fox News, Sessions tied Hochul to the House Minority Leader on multiple occasions. 

"Nancy Pelosi is doing all she can do to help Kathy Hochul," Sessions said. "We're trying to match Kathy Hochul's words to where she says that Nancy Pelosi is doing a great job." 

Sessions also took on Davis, who ran as a Democrat for the seat that stretches between Buffalo and Rochester and nearly defeated Rep. Tom Reynolds, R-N.Y., in 2006. 

"[Davis] is a tax and spend liberal Democrat who is very deceptive about his past," said Sessions. 

The NRCC, the national organization devoted to electing Republicans to Congress, formally jumped into the contest Thursday, infusing more than a quarter million dollars into the race.
