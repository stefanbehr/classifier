Rep. Ron Paul is expected to officially announce his bid for the presidency Friday to a group of a few hundred supporters outside the town hall in Exeter, New Hampshire, a source tells Fox News. 

This comes after weeks of muscular fundraising, trips back and forth between Iowa and New Hampshire, and participation in the first debate of potential candidates for the Republican nomination. A second source, in Paul's Iowa operations, says, "We're gonna have a candidate. It will be official." 

The Texas congressman opened his first campaign office in the nation in Iowa earlier this week. He's also established an exploratory committee and raised more than a million dollars in an online, one-day fundraising drive last week. Supporters in Iowa, Nevada and Texas have said the reception Paul's been getting so far from local Republican leaders is almost an about-face from his last presidential campaign when people gave him the cold shoulder. Now, with the dawn of the Tea Party and continued tough economic times, many are warming up to Paul, if not outright courting him. 

What might derail the campaign before it gets started are Paul's radio show comments on how he, as president, would have handled Usama bin Laden. On WHO radio, Paul said he did not agree with President Obama's decision to give the go ahead to kill bin Laden. "It was absolutely not necessary," Paul said. Paul instead advocated that bin Laden should have been captured, tried and punished. 

Supporters say Paul is just being consistent with his own beliefs about abiding by the rule of law. But how will it sit with voters? Paul's Iowa leadership chairman Drew Ivers said the statement will differentiate Paul from President Obama and the other Republican candidates. Ivers added, "The American people and the world deserve the right to participate in justice," and that by killing bin Laden instead of capturing him, they were denied that opportunity. 

Paul came to New Hampshire Thursday night for a fundraiser for New Hampshire's Speaker of the House William O'Brien. After his announcement in Exeter Friday, Paul will attend a dinner in Lebanon hosted by the Grafton County Republican party .
