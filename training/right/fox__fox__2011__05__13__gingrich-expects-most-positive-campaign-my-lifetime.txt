Republican Presidential candidate Newt Gingrich is calling for a pro-business agenda and aggressive tax cuts to trigger job growth and in the process, pay off the national debt. The former House Speaker made the comments Friday during a Washington speech before a conference led by Reagan administration economist Arthur Laffer. 

It was Gingrich's first public appearance since declaring his 2012 presidential campaign on Wednesday and he played to his audience by offering up a heavy dose of conservative economic proposals, including lowering taxes across the board, curbing government spending and paying off the national debt, "faster than anyone thinks." 

Gingrich said as House Speaker he helped balance the federal budget in the 1990s and knows how to achieve that goal again. "The first big step to balancing the budget is 4 percent unemployment," said Gingrich. "I don't think it's possible to balance the budget at 9 percent unemployment." 

Triggering job growth would be achieved in part by cutting the corporate tax rate from 35 percent to 12.5 percent, and completely eliminating the capital gains tax on stock profits, according to Gingrich. 

Gingrich will headline his home state of Georgia's Republican convention later on Friday and on Saturday, he will give speak at Eureka College in Illinois, the alma mater of Ronald Reagan . Next Monday he will hit the campaign trail in earnest, heading to Iowa, the home of the first-in-the-nation presidential caucuses, where he will criss-cross the state over most of the week. 

Although he has starred in some infamous and withering battles during his political career, Gingrich is says he is starting this run with a sunny outlook. "I think this is going to be the happiest, most positive campaign of my lifetime," Gingrich said. 

Yet at the same time Gingrich also maintains he is bracing for President Obama's billion dollar campaign machine. 

"It tells you about the Obama world view that he doesn't think he can win a fair election... a lot of it is coercing, there are people who are afraid not to give to him."
