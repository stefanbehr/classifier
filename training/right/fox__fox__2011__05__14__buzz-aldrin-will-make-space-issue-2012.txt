As the Republican contenders roll out their platforms and announce their candidacies for President, one of the world's most celebrated astronauts plans to launch his own campaign. Buzz Aldrin - a member of the famous Apollo 11 mission to the moon in 1969 - told Fox News he's got a plan to make space an issue in the 2012 Presidential race. 

"We have a wonderful opportunity coming up in a couple of weeks. On May 25, it is the 50th anniversary of John F. Kennedy's speech before Congress. I believe this nation should commit itself to sending a man to the moon and bringing him back safely," Aldrin, 81, told Fox News. "I feel this can be an opportunity for this President [Obama]to make a proclamation about our space future. Unfortunately, I just don't think that is going to happen." 

On Monday, space shuttle Endeavour is scheduled to take off for the last time. It will dock with the International Space Station and according to NASA , "deliver the Alpha Magnetic Spectrometer (AMS) and spare parts including two S-band communications antennas, a high-pressure gas tank and additional spare parts." The shuttle will be piloted by Captain Mark Kelly, husband of Arizona Congresswoman Gabrielle Giffords , still recovering from that January shooting in Tucson that left six people dead. Giffords will reportedly be at Monday's launch. 

Space shuttle Atlantis will be the last orbiter in space. It's scheduled to launch in early July and when it lands back on Earth the shuttle program will be history. 

Aldrin will not be in Cape Canaveral for Monday's launch but plans to be at Atlantis' takeoff for last shuttle mission. "We have too many other things distracting ourselves from a space program proclamation." Says Aldren who believes the U.S. should lead efforts to create a permanent presence on Mars by 2035. 

In April, 2010, President Obama announced the end of the shuttle program will mark the beginning of a new era in space exploration. Private industry, Mr. Obama told a crowd at the Kennedy Space Center in Florida, must take the lead in the space race. He said new companies will, "compete to design and build and launch new means of carrying people and materials out of our atmosphere." 

Although Aldrin agrees, "the private industry will take over where the government paved the way" he feels the government should take the lead on the "big stuff" like the Mars mission. 

Aldrin is giving a speech at the John F. Kennedy Library in Boston on may 25 and feels after that, many people around the world will realize this can be a campaign issue. 

Lexi Stemple contributed to this web post.
