Not even 48 hours after Mike Huckabee made it official that he's not running for president, one of his key early state operatives has endorsed someone else. Mike Campbell, the South Carolina Chairman for Huckabee in 2008, has announced he will back Jon Huntsman if he gets into the race. 

In a statement released to Fox News, Campbell indicated he spoke with Huntsman on the phone Monday and urged him to seek the Republican nomination. Campbell, who is the son of former South Carolina Gov. Carroll Campbell says, "Like my Dad, Huntsman was a popular Governor who kept his promises and left office with a powerful record of achievement. As more South Carolinians get to know Governor Huntsman, I believe they will come to the same conclusion I have - that he is the right leader for our party and our country in this important election". 

Campbell's backing is interesting, because Huckabee is known for his appeal to evangelical Christians and Huntsman is a Mormon. According to a Huckabee source, Huntsman and Huckabee had dinner in New York a couple weeks ago. The South Carolinian says, "They are very close" and "The governor thinks (Huntsman) is a very good man". The two are both musicians and have even jammed together. Huntsman plays keyboard, and Huckabee, of course, the bass guitar. 

South Carolina, considered part of the so-called Bible Belt, holds the "First in the South" presidential primary. The state has nine electoral votes and 50 GOP delegates. Not long after wrapping up his time as President Obama's ambassador to China, Huntsman spoke to graduates at the University of South Carolina and paid a visit to the state's governor, Nikki Haley . 

This week, Huntsman is making up for lost time in New Hampshire, home of the first primary, where he's scheduled to make 12 stops in five days. Part of the Granite State swing includes another commencement address at Southern New Hampshire University's on May 21. 

Huntsman is also slated to address the Faith and Freedom Conference and Strategy Briefing in Washington, D.C., next month. The conference website bills it as a "destination for conservative activists who want to learn how to beat the Left". 

Fox News Field Producer Serafin Gomez contributed to this report.
