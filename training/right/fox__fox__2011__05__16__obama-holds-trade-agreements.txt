The Obama administration has announced it will not submit three pending trade agreements that are strongly supported by Republicans and the Chamber of Commerce without a "robust" extension of a program that provides assistance to workers who lose their jobs because of outsourcing. 

The Trade Adjustment Assistance program was beefed up in the stimulus bill in 2009 and will cost about $2.4 billion this year. Iowa Republican Sen. Charles Grassley has reportedly suggested it's no longer affordable. But Gene Sperling , head of the National Economic Council, told reporters "we will not submit the Free Trade Agreements without an agreement on an enhanced" TAA program. 

Congress declined to extend the TAA program in December and again in February. Officials declined to suggest how much an "enhanced" program might cost. Sperling only says it would depend on the performance of the economy. But Trade Ambassador Ron Kirk said the president will insist on a "holistic approach" to trade that "keeps faith" with America's workers. 

The TAA was passed in 2002, but prior to 2009 Kirk says, it was "poorly administered and underfunded." As part of the Recovery Act, service workers were added to those eligible for job retraining and other benefits and 430,000 have been certified as eligible since then. 

Negotiations on the three pending free trade agreements, with South Korea , Colombia and Panama, have been finished. Congressional committee heads are reviewing the text in advance of drafting implementing legislation for the bills.
