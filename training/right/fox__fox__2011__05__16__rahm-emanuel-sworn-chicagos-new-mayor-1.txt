For the first time in 22 years, the Windy City has a new mayor. Under crystal clear blue skies, Rahm Emanuel was sworn in as Chicago's mayor ushering in a new era in Chicago politics. 



Emanuel, who resigned as President Obama's Chief of Staff to run for the post, told the crowd at Millennium Park he has big shoes to fill replacing now-former Mayor Richard M. Daley, the longest serving mayor in Chicago history. 

During his speech, Emanuel was quick to give credit to Daley for his lifetime of service, leadership and vision. "No one ever loved Chicago more or served it better than Richard Daley ," Emanuel said. 

Close friend and political ally, David Axelrod said "they (Emanuel Daley) are both larger-than-life figures. Running a big city is a huge task...you have to be a larger-than-life figure to do it well." 

Emanuel also publicly thanked his former boss for his support, "I want to thank President Obama, who turned our nation around and who loves Chicago so much, he understood why I wanted to come home to get our city moving again." 

Today's swearing-in ceremony brought out the "who's who" of Chicago and Illinois politics. Vice President Joe Biden and his wife, Jill, also attended. They were sitting on stage between Emanuel and Daley, a symbol of the important relationship between Chicago and the White House . 

Emanuel has his work cut out for him. Referring to Chicago as the "hardest working city in America," Emanuel made it clear he plans to make education his first priority. "As some have noted, I am not a patient man. And when it comes to improving our schools I will not be a patient mayor." 

Emanuel's relationship with Chicago's new city council will be one to watch. The first city council meeting with newly-elected alderman will be held later this week.
