Donald Trump is meeting with NBC executives today to discuss the future of "Celebrity Apprentice," Fox News confirms. 

Trump's Special Counsel Michael Cohen says NBC is eager to keep the top rated show on the air, but disputes reports that NBC could continue to produce the reality show, even without the big boss. Cohen says the reality is: NBC can't make that decision because the network doesn't own the show. He says Trump and the reality show's creator, Mark Burnett, own it. 

Trump is supposed to make an announcement regarding his run for the presidency during the season finale of "Celebrity Apprentice" which airs on NBC May 22. His office has said at the end of the show, he will announce a time and place for a future press conference where he would announce whether or not he is running for president. Cohen says as of now, he doesn't know if that's still the plan or if Trump will make any public announcement today. 

Stay tuned...
