GOP presidential hopeful Newt Gingrich picked up an endorsement from Georgia Rep. Jack Kingston, R, late Tuesday, positive news for the former House speaker who's been under fire from fellow Republicans this week. 

According to Gingrich's campaign website, newt.org, the fiscal and socially conservative congressman told Politico that Gingrich is the candidate with the right experience. 

"Newt, I think, know policy better than any of the other candidates," Kingston said. "At a time we need entitlement reform more than anything, he's a guy who's already done it." 

"He's always been able to connect with young people as a reformer," he added. 

Gingrich later thanked the congressman on Twitter , writing, "Glad to have you on board."
