Control. Alt. Delete! 

Little more than a week after the U.S. Secret Service launched its official Twitter account, the federal law enforcement agency has had a major system failure. 

Shortly after 3 p.m. Wednesday, the Secret Service's account declared on the social media site: "Had to monitor Fox for a story. Can't. Deal. With. The. Blathering." 

The posting was quickly removed, but within a half-hour dozens of other Twitter users had already begun re-posting -- or "re-Tweeting" -- the message. 

"Love the Secret Service tweets!" one Twitter user said. 

Earlier in the day, Fox News had been covering the story of Vito LaPinta, the 13-year-old from Tacoma, Wash., who was recently visited by a Secret Service agent for posting a message on Facebook suggesting President Obama should watch out for terrorist attacks in the wake of Usama bin Laden's killing. LaPinta's mother was not present when the Secret Service interviewed her son, a move she decried as inappropriate. 

On the Twitter post, a spokesman for the Secret Service explained that "an employee with access to the Secret Service's Twitter account ... mistakenly believed they were on their personal account" and posted "an unapproved and inappropriate tweet." 

"The tweet did not reflect the views of the U.S. Secret Service and it was immediately removed. We apologize for this mistake, and the user no longer has access to our official account," spokesman Ed Donovan said in a statement to Fox News. "Policies and practices which would have prevented this were not followed and will be reinforced for all account users. We will ensure existing policies are strictly adhered to in order to prevent this mistake from being repeated, and we are conducting appropriate internal follow-up." 

The Secret Service launched its Twitter account May 9. 

"By using social media sites, we hope to supplement our recruitment efforts, while providing an informative, helpful tool to businesses and individuals who are interested in information from our agency," Secret Service Assistant Director Mickey Nelson said in a statement at the time. 

He called the internet "a valuable resource for people all over the world."
