The most tech savvy president in history Thursday noted the role technology has played in the recent Middle East and North African uprisings and vowed the United States will utilize it to promote democracy in that part of the world. 

"Across the region, we intend to provide assistance to civil society, including those that may not be officially sanctioned, and who speak uncomfortable truths," President Obama said during a speech laying out policy in the Middle East . "And we will use the technology to connect with - and listen to - the voices of the people." 

The uprisings during the so-called "Arab Spring" have often been organized and fueled through the use of social networking and video-sharing technology. 

Arguing that reform can't come through elections alone, the president suggested the U.S. will tap into the two-way communication of the Web to pass information to and from people in nations that are struggling toward reform. 

"Through our efforts we must support those basic rights to speak your mind and access information," he said. "We will support open access to the Internet, and the right of journalists to be heard - whether it's a big news organization or a blogger." 

Obama said that Web technology not only gives citizens a worldwide voice, but is a measuring stick for new governments that form in the region. 

"In the 21st century, information is power; the truth cannot be hidden; and the legitimacy of governments will ultimately depend on active and informed citizens."
