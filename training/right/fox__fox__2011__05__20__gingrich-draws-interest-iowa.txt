The uproar following Newt Gingrich's most recent Sunday Show appearance doesn't seem to have diminished voter interest in the former House Speaker. Campaign spokesman Rick Tyler tells Fox that they've had two to three times the number of people expected at various campaign stops in Iowa where Gingrich is seeking support. 

"We had to remove a wall to double the size of the room to fit them all in. The reaction is overwhelmingly positive. Each stop has had at least 75 people, most over 100 even at mid-day and some over 200," Tyler told Fox.
