There's no doubt about it, former Alaska Governor Sarah Palin has a burning desire to be a political leader potentially even a presidential candidate, but whether that flame will actually ignite a 2012 run remains to be seen. 

Appearing on Thursday's On the Record program with Greta Van Susteren , Palin was asked whether she had a fire in her belly in a way that Mike Huckabee said he did not when announcing his decision to skip another presidential campaign. 

"My problem is that I do have the fire in my belly," Palin offered. "I am so adamantly supportive of the good traditional things about America and our free enterprise system. And I want to make sure that America is put back on the right track and we only do that by defeating Obama in 2012. I have that fire in my belly." 

Palin, a Fox News contributor, did not suggest that a decision to run is imminent. She cited concerns over her family's privacy as reasons not to make a run. Earlier in the interview she spoke of the "unpleasant" and "unnatural" demands of fundraising that are a necessary component of a national campaign. 

Click here to watch Palin's comments in full and her thoughts about the current field of GOP presidential candidates.
