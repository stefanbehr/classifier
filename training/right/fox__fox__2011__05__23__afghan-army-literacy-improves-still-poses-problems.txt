NATO plans to train the Afghan security force to take over all 34 provinces in Afghanistan by Dec 31, 2014. But 86 percent of the recruits are coming to them so illiterate that they have had to invest $88 million just to get the new recruits to a 3rd grade reading level. 

On any given day the U.S. military and NATO have 34,000 Afghan army and police recruits enrolled in literacy classes, and so far 92,000 recruits have received literacy training. It's translating to a better public perception. The Afghan army now counts as the number one trusted institution in Afghanistan , according to recent polls conducted by the Asia Foundation and others, according to Dr. Jack Kem, the senior adviser to Lt. Gen. William Caldwell who is in charge of the NATO training mission. 

More than 2,200 Afghans are now employed as language instructors. It takes NATO 64 hours to get a new recruit to a first grade reading level. Some recruits are now carrying pens around in addition to their weapons because they are so proud of their new skills, according to Kem, who briefed press at the Pentagon Monday. 

To combat some of the corruption that goes along with having an illiterate security force, NATO has begun putting blue dye in the fuel that security force vehicles use. "If you are driving around with blue fuel, you stole that fuel," Kem said, as an example of ways in which the U.S. and others are trying to reduce corruption in the security forces. They have also instituted a new anti-corruption phone line, and there is now a lottery for police and army assignments to halt the practice of buying assignments which contributes to more corruption and a loss of confidence in the force that is supposed to stabilize Afghanistan after U.S. forces leave. 

Eighteen months ago the Afghan national police faced an attrition rate of 70 percent per month. That is now down to 30 percent a month. They are paid a maximum of $240 per month with danger pay. On average a private in the Army and a new police recruit are now paid about the same. 

The security forces have grown by 98,000 since November 2009.
