In advance of his announcement rally in Des Moines, Iowa Monday, former Minnesota Governor Tim Pawlenty hit a few broadcast morning shows to make his case for why he should be the Republican Party's presidential nominee in 2012. 

In each appearance he touted his record on fiscal matters saying it proves that he's ready to face exploding deficits in Washington. 

"I know how to balance budgets. I know how to tackle spending. I know how to get economies growing," Pawlenty told CBS. "And in my state, we led the way back under difficult circumstances. It's a very liberal state. And if you can do it there, you can do it anywhere." 

In an interview with NBC Pawlenty saying he didn't want to criticize anyone, passed up the opportunity to contrast his virtues against presumptive GOP frontrunner Mitt Romney . But he had no hesitation in explaining why he'll do a better job than the incumbent. "President Obama, unfortunately doesn't have the courage to look the American people in the eye and tell them the tough truth about the things that we're going to need to do to get our spending under control. I'll do that." 

Pawlenty was also asked whether he lacked the charisma to run a successful campaign for the presidency and he bluntly said he's not running for "entertainer-in-chief" and that voters who're looking for the loudest candidate will not be supporting him.
