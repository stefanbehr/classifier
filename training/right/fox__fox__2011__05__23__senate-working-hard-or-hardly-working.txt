Just days before a week-long Memorial Day recess, a whopping 18 senators missed a Monday night vote related to the terrorism law known as the PATRIOT Act. 

That's nearly 20% of the U.S. Senate absent. 

Now, to be fair, a number of absences could certainly be weather-related, especially where tornado-ravaged Missouri is concerned. Others could be due to airport and/or plane problems. 

But it seemed worthy of being noted. That's a very large number of members missing a vote. 

The following were listed as no-show's: 

Sen. Lamar Alexander , R-Tenn., Sen. Mike Bennet, D-Colo., Sen. Roy Blunt, R-Mo., Sen. Scott Brown, R-Mass., Sen. Sherrod Brown, D-Ohio, Sen. Thad Cochran , R-Miss., Sen. Bob Corker, R-Tenn., Sen. Dick Durbin, R-Ill., Sen. Lindsey Graham, R-SC, Sen. Jim Inhofe, R-Okla., Sen. Mike Lee, R-Utah, Sen. Claire McCaskill, D-Mo., Sen. Mark Pryor , D-Ark., Sen. Jim Risch, R-Idaho, Sen. Marco Rubio, R-Fla.,Sen. Richard Shelby, R-Ala., Sen. David Vitter, R-La., Sen. Sheldon Whitehouse , D-RI
