Congressional staffers will learn how to spot and treat symptoms of mental illness at a Capitol Hill workshop Thursday. 

The program will instruct participants in crisis de-escalation techniques, recognizing the signs and symptoms for common mental illness, and connecting people in crisis situations with medical services. 

The National Council for Community Behavior Healthcare, which is providing the training session, plans to modify the course to reflect situations that congressional staffs encounter on a daily basis. 

Mental health has been a topic of increased scrutiny on the Hill after the January shooting of Rep. Gabrielle Giffords , D-Ariz., at a constituent meet-and-greet in January. Six people were killed and Giffords and 12 others were wounded in that attack. Wednesday accused shooter Jared Lee Loughner was found to be incompetent to stand trial. 

Reps. Grace Napolitano, D-Calif., and Tim Murphy, R-Pa., are hosting the training session.
