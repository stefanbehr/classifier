First lady Michelle Obama recalled a controversial White House appearance by the rapper Common while meeting with London school students Wednesday. 

"We had a poetry session and we invited young kids in just last week from all over the country. And they talked to some of the most outstanding poets, and they read their poetry in the state room," the first lady said to a group of high school girls gathered at Oxford University. "And then we had a poetry night and Common was there. He's very cute. But everybody from poet laureates to hip-hop folks, being able to mix up the world in that very interesting way, the White House allows you to do that." 

Common's invitation to the White House poetry event earlier this month raised the eyebrows of some conservatives, and the ire of New Jersey police officers because of his tune "A Song for Assata." It pledges support for Assata Shakur who was convicted of killing a trooper in the Garden State in 1973. Some of Common's other lyrics criticize the 2003 U.S. led invasion of Iraq. Still, others argue the rap artist is a positive force whose music and message have matured over the years. 

As she answered the question about life as first lady, Obama relished the role of being able to use events like the poetry night to reach out to young people. 

"[M]oving you guys and pushing you to see more for yourselves is all that matters," she said. "So, if the White House lets me do that... we can open it up and let kids in."
