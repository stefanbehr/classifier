Funny moment at the GOP presser Thursday. 

Republicans were running down a long list of committee chairmen, speaking about their economic recovery program. 

House Judiciary Committee Chairman Lamar Smith, R-Texas, finished his remarks and immediately said, "I think Kevin Brady is next," referring to Joint Economic Committee Chairman Kevin Brady, R-Texas, who was standing behind him. 

"No, I'm just eye candy," deadpanned Brady, which drew a gigantic laugh from the members and press corps. 

House Speaker John Boehner , R-Ohio, responded, "Thanks for passing that along."
