The House of Representatives has defeated an amendment to the defense bill that would have curbed the president's war powers authority. 

Offered by Rep. Justin Amash, R-Mich., this amendment could crack down on the the president's power to use force against any group or individual that he determines is associated with Al-Qaeda or the Taliban . 

There is a section of the bill that increases the president's power in this area. 

In light of the Libya operation, there are many lawmakers from both sides of the aisle, who want to harness the president more, rather than enhance his authority when it comes to war powers. 

The House defeated the amendment, 244 to 187.
