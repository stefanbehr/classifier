House Budget Committee Chairman Paul Ryan is unfazed by opponents who use CBO numbers to attack his budget proposal. 

Senate Majority Leader Harry Reid (D-Nev.) and other Democrats have argued Ryan's plan would ask seniors to pay more for their benefits -- charging every senior $6,000 more every year -- in exchange for fewer benefits. 

"What that [CBO] analysis forgot to include was the extra $7,800 a year that go to lower income seniors to cover all of their out of pocket costs. They're measuring any Medicare reform plan like ours against a mythical future, a fiscal fantasy, which is a collapsing system," Rep. Ryan (R-Wisc.) emphatically told Bret Baier Thursday evening. 



"They literally forgot to put that in the analysis," he added. 



Posed with Reid's critique that Ryan's plan "would turn over seniors' health to profit-hungry insurance companies," Ryan struck back. 

"Private companies already offer Medicare, have Medicare advantage, offer prescription drug benefits, Medigap insurance is offered, Part A and B is already delivered through private health insurance companies. So if he's concerned about private businesses involved in the Medicare delivery system then he should be concerned about Medicare as we know it because that's exactly how the benefit is delivered today." 

"What we're saying is have a system of choice and competition where Medicare sets up a list of competitive plans, guaranteed coverage options that seniors choose from for 54 year-olds and below and the Medicare subsidizes those plans. It's the same system that I've had as a congressman; it's the same plan that President Bill Clinton's bipartisan commission to save Medicare recommended. It saves Medicare from going bankrupt. Senator Reid seems to be content with the fact that Medicare is going bankrupt because they're offering no plan or solution to solve the problem." 

Ryan also said his plan works the same as the prescription drug benefit by offering the freedom to leave a plan. "It came in 41% below cost projections. Why? Because the seniors are in charge, because they get choice and competition. So, Medicare already has experience doing this." 

As for his interaction with Bill Clinton Wednesday, during which the former president said he hoped Democrats would not use the NY-26 election results "as an excuse to do nothing," Ryan characterized Clinton's remarks as "elder statesmanship," adding that he "knows the math, he knows it's going bankrupt, and I think the president was acknowledging that fact." 

Calls for the fiscal hawk from Wisconsin to run for president have grown louder, and Ryan admitted "I get this quite a bit." 

But, he said, "I really believe I can do more for this cause where I am right now as Chairman of the House Budget Committee. I have no plans to do this -- it takes an enormous undertaking to do this -- and right now where I am at this moment, I need to focus on this budget fight we're in. This summer we're going to be spending a lot of time in budget fights and to me, that's where I can make the biggest contribution to the debate right now." 

Former Minnesota Governor Tim Pawlenty has not offered a ringing endorsement for Ryan's budget plan, though he has not proposed how he would change it either. Instead Pawlenty has said he will introduce his own plan, though if he had to choose between signing Ryan's plan or not, he "would sign it." 

Asked about Pawlenty's lukewarm comments, Ryan said, "What matters to me not that we get an endorsement on every little detail, what matters to me is that leaders step up and offer solutions to our country's problems." 

A strong endorsement has not come from any Republican presidential candidates, but it has come from former Vice President Dick Cheney who said, "I worship the ground Paul Ryan walks on. I hope he doesn't run for president because that would ruin a good man who has a lot of work to do." 

Ryan said it sounded like a fatherly comment, reiterating that remaining in his current position as House Budget Committee Chairman allows him to do what's important: being a family man. 

But Ryan indicated he feels the heat of recruitment upon him as Republicans question who will lead the face-off against President Obama in 2012. 

"I think people are hungry for people to step up and offer solutions. That's what leaders do and those of us who are leading -- I think people want that." 

Ryan gave strong advice to GOP presidential candidates: "If you want to be the president of the United States, you should put up ideas on how to solve this country's massive fiscal and economic problems. The current president isn't doing that. Our nominees should do that."
