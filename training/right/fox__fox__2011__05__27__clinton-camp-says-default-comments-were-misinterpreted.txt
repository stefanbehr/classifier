Former President Bill Clinton earlier in the week suggested the U.S. defaulting on debt for a couple days wouldn't be so bad, but now aides say his comments were misinterpreted. Obama administration staff also apparently contacted Clinton's people after he made the remarks and encouraged him to back track, the Wall Street Journal reported. 

Clinton initially said at a Peter G. Peterson fiscal event in Washington on Wednesday, "If we defaulted on the debt once for a few days, it might not be calamitous." 

The WSJ also reported that White House Chief of Staff Bill Daley and National Economic Council Director Gene Sperling suggested to the Clinton camp that he should maybe clarify. Daley was traveling with the president for his European trip and Sperling attended the fiscal event where Clinton spoke. The top aides were apparently worried that argument sounded too similar to Republicans and they could use that against administration. 

"We regret if there has been a misinterpretation of a comment President Clinton made about raising the debt limit. President Clinton did not in any way mean to suggest that a default would not be highly damaging for the economy even for a very short period of time. He inadvertently misspoke. What he meant to say was that if a vote to extend the debt limit failed in advance of a default, that might not be harmful for a couple of days, but that if people thought that we might actually default, that in his words we were literally not going to pay our bills anymore, then they would stop buying our debt," said Clinton spokesman Matt McKenna. 

The remarks came as Congress is in a heated battle over raising the debt ceiling and approaching an August deadline. Treasury Secretary Tim Geithner has been finding other means and loopholes to keep the U.S. above water since May, but said he could only do so for a limited time. 

"There's been no national presidential address on the consequences of the U.S. defaulting on its debt," Clinton also said, adding that Obama may not need to give one just yet, because it's unclear if default will happen. 



Fox News' Mike Emanuel contributed to this report
