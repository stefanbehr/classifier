Jazz, poetry and rap master Gil Scott-Heron just died. 

Scott-Heron was a master of distilling disparate political movements and social rumblings into distinct signals. In the song "The Revolution Will Not be Televised," Scott-Heron drops references to pop culture like a junkie on acid, citing everything from the Vietnam war to an Exxon advertising campaign to the use of instant replay on television. 

With Scott-Heron's death, perhaps it was appropriate that it was a week of signals on Capitol Hill . 

Signals as to the political climate. Signals on Medicare. Signals about foreign policy. Signals about the freshman class and the tea party. Signals about 2012. Signals about the Patriot Act. And signals urging unsuspecting lawmakers to keep it down to a dull roar. 

Early in the week, the political and scribe classes focused their eyes on a special election in the 26th Congressional district of New York. Democrat Kathy Hochul, Republican Jane Corwin and independent Jack Davis all vied to succeed former Rep. Chris Lee (R-NY). Lee abruptly resigned in February after half-nude pictures of the married Congressman materialized online. 

Hochul emerged as the victor in what has historically been Republican territory. And many in the press weighed whether that contest was a bellwether for the 2012 election cycle. After all, the public has given Republicans a rough go after they announced their plans to alter Medicare. And Democrats made sure they injected the Medicare debate into the New York race. 

The day before the special election, House Majority Leader Eric Cantor (R-VA) cautioned reporters from reading too much into the results of the race. 

"I do not think it can be seen as a signal as to the role of the budget reforms that we have proposed, including Medicare," said Cantor on Monday. "I know this town loves to take signals from individual races. I think the best signal you can take is the 63 seats that we picked up in November. That was a signal that the American people were tired of the direction being taken by the other side." 

Hochul won on Tuesday. And by Wednesday, Rep. Steve Israel (D-NY), the chairman of the Democratic Congressional Campaign Committee (DCCC) argued that Hochul's victory signaled something that contradicted Cantor's argument. 



"They are now having a sense of buyer's remorse," Israel said of voters rejecting Republicans in favor of Hochul. "We saw buyer's remorse in New York 26." 

On Thursday morning, House Minority Leader Nancy Pelosi (D-CA) seized on Hochul's win to blend together two signals. As reporters gathered in the House Radio-TV Gallery for Pelosi's weekly press conference, aides positioned a poster next to the lectern in an effort to send a new signal to the public. 

The poster depicted a photograph of an oil rig, obscured by a Medicare card. It was a didactic icon, screaming multiple political messages all at once. 

"Wow," mused a fellow reporting colleague once he saw the poster. "There's lot of signals going on there." 

And it wasn't long until Pelosi wove those images together into a unified signal. 

"How can you face the public when you vote for a bill, the Republican budget that gives tax breaks to big oil while requiring seniors to pay more for less and empowering the health insurance companies?" asked the California Democrat. 

Signals are sometimes sent, but not always detected. 

Such was the case Thursday afternoon as the House voted on amendments to a defense authorization bill. Rep. Jim McGovern (D-MA) crafted an amendment to require the Obama Administration place a timetable on withdrawing from Afghanistan. The House ultimately defeated McGovern's amendment, 215 to 204. But switch just six votes and the House would have adopted McGovern's amendment. 

The signal? Lawmakers from both parties are fretting about operations in Afghanistan. They want answers as to how long the U.S. is committed to what has become a decade-long conflict. 

Freshman Sen. Rand Paul (R-KY) sent a different signal this week. 

Paul nearly singlehandedly tied the Senate in knots over civil liberty concerns he had with the Patriot Act. Paul was relentless in his efforts to secure votes on various amendments to curb the Patriot Act. And because of his filibuster, the Kentucky Republican nearly caused the controversial surveillance law to expire just after11:59 pm Thursday. 

In the end, Paul relented, scoring two key votes on his amendments. But the signal from Paul was that he's committed to use Senate rules to his advantage when he thinks there's a miscarriage of justice. 

And it's not just signals. To Paul, it's sometimes symbols. 

"I think there are victories and then there are symbolic victories," Paul said to the New York Times. "And I think we had a symbolic victory here in the sense that we did get to talk about some of the constitutional principles of search and seizure and the Fourth Amendment." 

The Senate finally re-upped the Patriot Act late Thursday afternoon, sending the bill to the House for approval there as well. And when the House voted on the Patriot Act one lawmaker may have sent an important signal about the 2012 presidential race. 

There was little doubt that the House would approve the Patriot Act. But there were questions as to whether Rep. Michele Bachmann (R-MN) would even be on hand for Thursday evening's tally. 

Bachmann was scheduled to speak at the Robb Kelley Victory Dinner in Des Moines, IA. And once it appeared there would be an evening vote on the Patriot Act, Bachmann sent her regrets because she needed to stay in Washington. 

During a conference call with reporters, Bachmann indicated that a potential June announcement could come in Waterloo, IA. But she noted it wasn't correct to say that she was running for president. Yet. 

A bit later, Bachmann appeared in the House chamber with the Patriot Act vote well under way. Rather than voting immediately, Bachmann traversed the chamber and watched the gigantic scoreboard that lists how every member votes on a given issue. She scanned the board and then checked a tally sheet that's kept at a desk near the rear. The clock ran down to 00:00. Yet Bachmann still hadn't voted. 

Members continued to file into the chamber to vote. Even if the clock reads 00:00, the vote remains open until the chair gavels it closed. And Bachmann continued to confer with colleagues at the back of the chamber. 

Finally, nanoseconds before Rep. Charlie Bass (R-NH) gaveled the vote closed, Bachmann sprinted to a voting station, jammed in her voting card and pressed the green button. 

A digital, green "Y" appeared instantly next to Bachmann's surname on the giant scoreboard across the room. 

Bachmann had voted for the Patriot Act. 

Bachmann is a doyenne of the tea party movement. But her yea vote on the Patriot Act won't sit well with her base. 

After the vote, Bachmann commandeered the House floor. For five minutes, Bachmann stared directly into the television camera to justify her vote. It's rare in the House for a member to pick up the camera across the chamber and, like a TV news anchor, speak directly into it when appearing on the floor. In fact, House rules dictate that lawmakers address the chair when speaking on the floor. But with radar-like precision, Bachmann locked her eyes onto that camera staring down onto the House floor from the mezzanine level. 

It was a sharp contrast to Bachmann's State of the Union response where she looked into one privately commissioned camera, but appeared off-center at the TV network pool camera. 

That gaffe prompted parodies on Saturday Night Live. 

"We have had calls, we've had requests on our Facebook, Twitter and on our email urging a no' vote tonight on the Patriot Act," Bachmann said. In defense of her vote, Bachman said the U.S. is fighting "a new war, a new enemy, new tactics." 

It was an interesting set of signals from Bachmann. 

First a yes vote. Then Bachmann's laser-like focus on the camera, explaining the vote in a move that could anger some of her most ardent supporters. 

But the entire episode may signal that Bachmann is poised to take a big step in her meteoric political ascendency. 

Finally, there was a signal from a U.S. Capitol Police officer. 

The officer was patrolling a hall in the Rayburn House Office Building across the street from the Capitol. He attempted to send a signal to a group of people carrying on too loudly just outside a meeting of the House Appropriations Committee. 

An exasperated committee staffer tried in vain to get the group to simmer down. Their voices echoed throughout the marble Rayburn corridors which double as an acoustical nightmare. Finally the aide flagged down the officer to ask if he could get the group to pipe down. 

Rather than just politely ask, the uniformed officer gave two brief toots on his whistle, drawing the group's attention. 

What no one realized is that Rep. Mike Honda (D-CA) was in the middle of the scrum, energetically engaging a cadre of folks who had come to see him. Honda had his back to the officer and wasn't wearing his jacket. Once the officer blew his whistle, Honda turned to face the chagrined officer who then apologized to the Congressman. 

In other words, it's okay if a lawmaker is involved in a vigorous conversation. But not okay if it was anyone else. 

And the signal that sends is pretty clear.
