President Obama honored America's fallen heroes Monday in Washington, participating in the annual Memorial Day Service at Arlington National Cemetery and laying a wreath at the Tomb of the Unknowns. 



"To those of you who mourn the loss of a loved one today, my heart goes out to you. I love my daughters more than anything in the world. I cannot imagine losing them," he said, speaking at the cemetery's Memorial Amphitheater. 

Following the service, the president made a stop at Arlington's Section 60 where those killed in Iraq and Afghanistan are buried. He and the first lady met with families of the men and women buried there. 

Defense Secretary Robert Gates and Adm. Mike Mullen, Chairman of the Joint Chiefs of Staff, also delivered remarks in honor of America's fallen service members. Gates, who will leave the Pentagon next month after more than four years of service, stressed the importance of supporting the troops beyond the holidays. "I urge all Americans to remember that just as each and every day, the troops now serving, faithfully peruse their mission to protect us, so each and every day, they deserve our recognition, our respect and our conscious gratitude," he said. 

Earlier at the White House the president spoke in the Rose Garden about the observance of Memorial Day and the importance of remembering those who were lost. "We'll pause to honor all those who've given their last full measure of devotion in defense of our country. Theirs was the ultimate sacrifice, but it is one that every man and woman who wears America's uniform is prepared to make -- so that we can live free."
