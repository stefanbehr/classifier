White House Spokesman Jay Carney channeled former President Ronald Reagan in the White House briefing Tuesday, suggesting President Obama's position on raising the debt ceiling mirrors that of the fiscally conservative icon. As House Republicans pushed forward on a clean but doomed debt limit vote that some say is aimed more at political gamesmanship than legislative action, the administration suggested they all rally around the Gipper. 

"I would point them ... to President Ronald Reagan, who wrote then-Senate Majority Leader Howard Baker in 1983, he said that the full consequences of a default, or even the serious prospect of default by the United States are impossible to predict and awesome to contemplate. Denigration of the full faith and credit of the United States would have substantial effects on the domestic financial markets and on the value of the dollar in exchange markets,'" Carney said, reading from the letter. 

But while there is some agreement on the need to increase the debt ceiling, the administration, Republicans and some Democrats disagree on how to do it. Many on Capitol Hill want spending cuts tied to any increase in the ceiling, but the administration wants a clean increase. Still Carney suggested Tuesday that Reagan's 1983 letter shows Republicans should jump on board with the administration. 

"We can cite a long list of validators of our position on this that go back to at least Ronald Reagan in 1983 and probably predate President Reagan," Carney said. "So that's our position and we believe that, in the end, Congress will do the right thing and vote to raise the ceiling." 

The administration has been waving the warning flag for weeks, saying that failing to increase the debt ceiling would be "calamitous." And in repeating Reagan's words, Carney tried to separate the issues of the debt ceiling and spending cuts. 

"'The risks, the costs, the disruptions and the incalculable damage lead me to but one conclusion: The Senate must pass this legislation before the Congress adjourns...'" Carney said, reading from a letter Reagan wrote seeking a debt limit increase. "We agree with Ronald Reagan and many others that we cannot default on the full faith and credit ... of the United States" 

But despite Carney's suggestion that all sides should come together around Reagan's idea, Republicans are likely to suggest that the former president would have wanted spending cuts too. But Reagan ultimately got the debt limit increase he sought in November of 1983 as the ceiling was raised from $1.389 trillion to $1.49 trillion.
