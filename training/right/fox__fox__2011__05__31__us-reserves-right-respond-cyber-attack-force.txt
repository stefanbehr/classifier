The Pentagon is set to release a new report which makes clear the United States reserves the right to respond to foreign attacks on its cyber networks with military force. 

The Defense Strategy for Operating in Cyber Space is expected to be released publicly by the Pentagon in the next two or three weeks, partly as a follow up to the International Strategy for Cyberspace, issued by the White House on May 16th. Both strategy reports say, that when warranted, "the U.S. will respond to hostile acts in cyber space as we would to any other threat to our country." 

According to Pentagon Spokesman Col. Dave Lapan, that includes the right to use military force. "There is certainly a deterrent effect to letting our adversaries know how we would consider those actions and what steps we might take," Lapan said. 

Lapan said that no one should assume that a cyber attack would limit the U.S. to a cyber response. "All appropriate options would be on the table if we were attacked via cyber," Lapan said. 

Deputy Secretary of Defense William Lynn has made cyber security a leading cause during his tenure. At the White House last month he said the Department of Defense networks are "probed millions of times a day" and that "foreign intelligence agencies have tried to penetrate our networks or those of our industrial partners." 

Just last week one of those industrial partners, defense contracting giant Lockheed Martin, suffered what it called "a significant and tenacious attack on its information systems network." A spokesman for the company said the attack was repelled quickly and that all its systems and client information remained secure. 

It goes to show that defining what type of attack will warrant a response will be a tricky matter in the future. Lapan says this Pentagon strategy will not outline potential scenarios that would constitute an act of war. 

The U.S. also needs to be careful not to write rules that it may be at risk of violating. The now infamous STUXNET worm that crippled part of Iran's nuclear program last year is widely believed to have been launched with at least some U.S. participation.
