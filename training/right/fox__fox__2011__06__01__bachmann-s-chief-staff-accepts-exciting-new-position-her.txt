If it looks like a duck, swims like a duck and quacks like a duck... 

Michele Bachmann's congressional chief of staff Andy Parrish announced via email Thursday that he is moving on to new duties with the same boss; another sign Bachmann is positioning herself for a run at the 2012 Republican presidential nomination. 

"I wanted to let you know that after an enormous amount of prayer I have decided to take an exciting new position with Congresswoman Bachmann," wrote Parrish. 

"I will be able to tell you more about what I am doing in the near future." 

Rep. Bachmann has said repeatedly she'll announce her intensions in birthplace of Waterloo, Iowa. The exact date of the event is yet to be announced. 

There may be more staff shuffling ahead. 

Brooke Bialke, now acting chief of staff for Bachmann, said, "My lips are sealed," when asked if she might be joining Parrish on his new assignment. 

Bialke last year served as Bachman's deputy chief of staff before Parrish was hired for the job. 

Bachmann's had some difficulty keeping people in the top spot in her congressional office. There have been seven different people who've served as Bachmann's chief of staff (in either full-time, acting or deputy capacity). 

The Minnesota Republican began serving in the US House in January of 2007.
