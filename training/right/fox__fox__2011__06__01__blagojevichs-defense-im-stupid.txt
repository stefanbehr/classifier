Former Illinois Gov. Rod Blagojevich isn't exaggerating when he calls it the "phrase heard round the world." That, of course, is his taped conversation about President Barack Obama's vacated Senate seat. 

You can probably repeat it like a verse from an overplayed 80's song. "I've got this thing here and it's f--- ing golden," and "I'm just not giving it up for f---ing nothing." 

Two trials and a zillion sound bites later, Blagojevich has finally given his take to jurors about what he meant. He said appointing someone to that seat was "a unique opportunity" and he did not want to give it up without discussing his options. 

"I'm stupid here," Blagojevich said. "I'm just talking and being stupid." 

The Blagojevich defense team has been attempting to argue that he always thought what he was doing was political "horse trading," and the governor always believed his actions were legal. A history buff, Blagojevich did an eloquent job of citing examples of U.S. presidents who handed out appointments as rewards. The problem for the defense -- he did it without the jury in the room. 

That's because Judge James Zagel said the Blagojevich team's argument was based on the fact that nobody told him his actions were illegal. So, Blagojevich just assumed it was legal. "The fact that he thinks it is legal is not relevant here," said the judge. He ruled that they cannot present that to the jury. 

Still, Blagojevich found ways from the witness stand to slip in a sentence that he always thought he was in the clear. This prompted an angry chastising from Judge Zagel. "This is not fair, this is a repeated example of a defendant who wants to say something by smuggling (it) in," he scolded from the bench. "And when you do it more than once or twice, it is inevitable that I'm going to believe there was some purpose other than the pursuit of the truth." 

At Judge Zagel's urging the defense is wrapping up its questioning of Blagojevich on the witness stand. The government team, lead by Reid Schar, is waiting like a bull in the chute to begin cross examination. They have meticulously documented everything Blagojevich said from his initial press conference after being arrested when he questioned the fight in the prosecutors to appearances on shows like "The View." They have been waiting for two trials now to use Blagojevich's own words to punch holes in his statements - and few expect it to be pretty.
