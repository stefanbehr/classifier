The second GOP presidential debate will not include one of the candidates who participated in the first. Former New Mexico Gov. Gary Johnson will not be allowed to participate in the June 13 debate in New Hampshire hosted by CNN, the New Hampshire Union Leader and WMUR. 

The seven hopefuls on stage that night will include former Pennsylvania Sen. Rick Santorum, businessman Herman Cain , Texas Rep. Ron Paul, and former Minnesota Gov. Tim Pawlenty , all of whom participated in the first debate of the season in Greenville, S.C . which aired on Fox News on May 5. 

There will be three debate newcomers, Minnesota Rep. Michele Bachmann and former House Speaker Newt Gingrich, and former Massachusetts Gov. Mitt Romney . CNN says invitations were also sent to former Utah Gov. Jon Huntsman, Sarah Palin , Rudy Giuliani , Mike Huckabee , Donald Trump and Indiana Gov. Mitch Daniels. Huntsman, Palin and Giuliani declined to attend, and Huckabee, Trump and Daniels have now announced they will not run. 

Johnson participated in the May 5 debate, but was not invited to participate in the New Hampshire debate. A source in the Johnson campaign tells Fox News, "No reason was given, other than being told by CNN before this morning's announcement that they would be applying objective criteria'. Otherwise, we have heard nothing other than the announcement of who would be participating." 

On their website CNN lists the " objective criteria " for participation in the New Hampshire debate as follows: 

"1. A candidate must have received an average of at least 2.00 % in at least three national polls released between April 1 and April 30 that were conducted by the following: ABC, AP, Bloomberg, CBS, CNN, FOX, Gallup, Los Angeles Times, Marist, McClatchy, NBC, Newsweek, Pew, Quinnipiac, Reuters, USA Today and Time. 

2. A candidate must have received an average of at least 2.00 % in at least three national polls released between May 1 and May 31 that were conducted by the following: ABC, AP, Bloomberg, CBS, CNN, FOX, Gallup, Los Angeles Times, Marist, McClatchy, NBC, Newsweek, Pew, Quinnipiac, Reuters, USA Today and Time. 

3. A candidate must have received an average of at least 2.00 % in polls of New Hampshire voters conducted by the University of New Hampshire Survey Center released between May 1 and May 31." 

Johnson, whose strategy focuses heavily on New Hampshire where his libertarian-leaning philosophy stands a better chance than in other early states like Iowa or South Carolina. 

"I respect the right of CNN and the other sponsors of the June 13 New Hampshire Republican presidential primary debate to apply their own criteria and invite who they choose," Johnson's campaign said in a statement released Friday. "What will be missing is the voice of those who hold an undiluted view of individual liberty..." he continued. 

"I sympathize with the millions of Americans whose beliefs will not be on display in Manchester on June 13."
