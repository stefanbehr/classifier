While the main message coming from the White House Friday focused on the resurgent U.S. auto industry, the Obamas sent mixed messages on the food front as the first lady planted vegetables while the president dined on less healthy fare. 

"Mustard, onion, chili sauce sounds just right... a little cheese on it, nothing wrong with that," the president said as he placed his order at Rudy's, one of the locals' favorite hot dog joints in Toledo, Ohio. Obama was in town to deliver an address at the local Chrysler auto assembly line. 

After the hotdog, the president continued by ordering fries, a bowl of chili and a sweet iced tea. But even he admitted his eyes may have been bigger than his stomach. 

"I can't guarantee I'm going to eat all of it!" Obama said to the workers behind the counter. 

Later, reporters travelling with the president said he enjoyed a second hotdog when offered, but there was no word on whether he finished the fries and side of chili. 

But back home in Washington, first lady Michelle Obama was in the White House garden harvesting vegetables and stressing the importance of healthy eating. She described her healthy lifestyle initiative called "Let's Move" to children on hand in the garden. 

"It's a whole initiative to work with people all across the country to think about how we eat and how we move our bodies so that you guys grow up healthy and strong and able to do well in school and be successful in life," the first lady said. "That's what the whole Let's Move' effort is about." 

The event came just a day after she and Agriculture Sec. Tom Vilsak launched the new "MyPlate" icon to replace the traditional food pyramid. The new icon eliminated small portions of unhealthy foods from one's diet, and the first lady said it has changed things around the White House . 



"I've seen the icon, I can't help but look at my own plate a little differently to see whether I have enough fruits and veggies," Obama said. "And trust me, we are implementing this in our household... I find myself doing a quick checklist to make sure that I have a balanced meal. " 

While it's unknown whether she will run a checklist on the president's Friday lunch, it is clear he had Mrs. Obama and the garden on his mind while in Toledo. 

During a stop at a local hardware story, the president bought the first lady a new pair of gardening gloves which he gave her as he stepped off Marine One back in Washington.
