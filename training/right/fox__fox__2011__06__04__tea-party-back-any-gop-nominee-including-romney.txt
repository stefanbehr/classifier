The Tea Party will support whoever wins the GOP presidential nomination - - even if that person is former Massachusetts Gov. Mitt Romney , the chairwoman of the Tea Party Express told Fox News. 

"Whoever the Republican nominee is will have to have the support of the Tea Party movement, the entire Tea Party movement," Chairwoman Amy Kremer said. 

Kremer went on to say the Tea Party is "completely neutral," adding that it "just wants to see the cream rise to the top... If Romney is the nominee I believe that we want to defeat Barack Obama ." 

Her confidence comes while some Tea Partiers, including some at FreedomWorks, a leading voice in the Tea Party movement, set their sights on stopping a Romney nomination. Other groups immediately rejected Kremer's comments as not representative of the entire amorphous movement. 

Kremer said she also wants to put to rest the idea the Tea Party would support a third party candidate, insisting the Tea Party can work from within the Democratic and Republican parties. 

"There is no way that we are going to support a third party candidate. It would split the vote and it would guarantee reelection for Obama, and we need to crush Obama we have to get him out of the White House ," Kremer said. 

Kremer also said she believes the Tea Party movement will have a "massive impact just as we did in 2010." 

In fact, she said the Tea Party and its elected representatives are responsible for the failure of a clean debt ceiling vote in Congress this past week. 

"We are still having an impact here in Washington. I really believe that if it weren't for the Tea Party movement they would already raised the debt ceiling and the spending would continue to be out of control... The Democratic Party was completely split. 

"If it wasn't for this movement, I don't think you would have even seen that." 

The Tea Party Express is one of the three main Tea Party groups. It began as a cross-country bus tour aimed a rallying supporters. 

Following the coverage of Kremer's comments, Tea Party Express Communications Director Levi Russell responded with, "Amy's comments were reflective of our confidence in the strength of the Tea Party movement, in which we don't think a candidate can win the nomination without already having won over a majority of tea party support." 

Watch the latest video at FoxNews.com
