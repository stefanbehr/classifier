Republicans and Democrats still have not come up with a budget deal to fund the 2012 fiscal year, and one of the major sticking points is defense spending. 

Freshman Rep. Allen West, R-Fla., said in an interview on "America's Election HQ" Sunday that during his 22 years in the military and the time he has spent on Capitol Hill , he has "found three wasteful programs in the DOD budgets which will come out to about 800 million dollars in savings for American tax payers." 

But he's also worried some members of Congress may chop too much out of the Pentagon's budget. 

"My concern is that we don't want to strip off all the meat from the bone of the Department of Defense, because when we look at soldiers, sailors, airmen, marines, and even Coast Guard, who are on fifth and sixth tours of duty, we have got to do better for them and their families," West said. 

When it comes to the economy as a whole, the congressman said some of President Obama's policies just aren't working. "There are things that the Obama administration has done that have proven to be, you know, of baring no good fruit," West said. 

"The unemployment rate that is now we see rising again and if you throw in underemployment and the employment rate in the urban areas of the minority communities, it is exorbitantly high." 

"It keeps coming back to our tax policy," West added. "I mean, we want to continue to talk about taxing the American people--why should the American people trust us with their tax resources when we do not first show them that we are going to be fiscally responsible? And that's the most important thing; it's not the revenues that's the issue here, it's our spending which is the issue."
