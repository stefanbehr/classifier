He truly couldn't have found a more appropriate way to start an address. 

So when Rep. Anthony Weiner (D-NY) bounded up from his seat to the lectern as one of the keynote speakers at the Congressional Correspondents Dinner on March 30, the New York Democrat launched his remarks with this gem: 

"I will keep my remarks brief." 

Perhaps it was foreshadowing. 

Little did anyone know at the time that Weiner's "briefs" (or someone else's) would become THE topic of conversation in Washington over the past ten days. Even as the House of Representatives voted against raising the debt ceiling. Even as lawmakers adopted a resolution demanding that President Obama explain the military objectives in Libya. Weiner's "Twouble with Twitter" was front and center. And once the Congressman returned to Capitol Hill last week, the lawmaker became the Weiner the World Awaited as journalists couldn't wait to get their Hanes on him. 

Even the appearance of the Weiner story on the AP wire produced chortles. The slug line for stories about Weiner read "US-Congressman-Twitter." But if your computer didn't have enough space to read the entire line, the slug was reduced to hilariously read "Congressman-Twit" before cutting off. 

Reporters tracked the story with an odd combination of both prurient curiosity and ardent disgust. 

The usually loquacious Weiner was initially mum about the lewd photo Tweeted from his account. But embattled, veteran politicians sometimes use humor to deflect controversy. Which is what Weiner tried during a t te- -t te with reporters in the Speaker's Lobby right off the House floor. 



Weiner then proceeded to simultaneously amuse, offend and stun reporters during a testy defense about the photo, lurching into a series of double entendres that one reporter remarked were "coming too fast." 

About the only thing Weiner didn't do was sing Chuck Berry's rendition of "My Ding-a-Ling." 

"I'm sorry if I was a little stiff yesterday," said Weiner. 

When speculating what the photo of a man's underwear may be covering up, Weiner wondered aloud if it may have been "the tip of al-Qaida's sword." 

Weiner explained that the photo couldn't possibly have originated from his account because he was Tweeting about a hockey game at the time. 

Good thing the Congressman didn't Tweet anything about high-sticking. 

The point is, Weiner has long used his surname for self-deprecating humor. A great example of that came during Weiner's remarks at the Congressional Correspondents Dinner in March. 

He conceded it was "difficult" having a last name like his. 

"It's something that caused me a lot of ridicule and hardship," the New York Democrat said, adding that he heard "the last, original Weiner joke in the fifth grade." 

Weiner said he used his name as a campaign slogan, such as "Vote for Weiner, he'll be frank" and "Vote for Weiner, he's on a roll." 

Even before Twittergate, Weiner possessed the second-most guffawed-after name in Congress. That's because the first-most guffawed after name belongs to House Speaker John Boehner (R-OH). 

"Who is Boehner fooling?" asked Weiner at the dinner. "I'm serious, brother. Just embrace it." 

A few moments later, Weiner unknowingly waded into an area which could fuel the embers of this scandal. 

"Time magazine named me one of the top 140 twitterers in the country," Weiner boasted. "Please follow me." 

Caveat Emptor.... 

This is where Weiner's Tweeting arrives at a unique nexus between the management of political messages and the burgeoning field of social media. 

The emergence of social media is a conundrum for politicians. First, Facebook and Twitter allow the political class to bypass traditional media and deliver its messages directly to mass audiences of followers. Many politicians have been quick to embrace new communications technologies with remarkable success. Look at Sarah Palin . 

A few years ago, blogs were the rage for politicians. At a discussion about technology and politics, the late-Sen. Ted Stevens (R-AK) even asked if he could "buy" a blog. Stevens is also the lawmaker who famously described the internet as a "series of tubes." 

But the problem with politicians blogging is that it cuts against the very essence of contemporary media strategy. Messages from today's politicians are market-researched, tailored and sanitized. A true blog is not. It's just someone's musings about events and issues. It's likely politicians would quickly find themselves in hot stew if they blogged honestly in a free-form fashion. 

Which is why they don't. 

Most politicians don't truly blog. They don't actually update their Facebook page. They don't Tweet. That's left to staff who concoct positive images and messages to portray their bosses in a certain light. Consider this: almost everything you read on a lawmaker's Twitter feed or Facebook account is something cooked up by a 24-year-old aide and scrubbed by the chief of staff or political advisers. 

In many respects, social media may actually have unwittingly constructed another barrier between the public and politicians because everything is posted is so cleansed and devoid of controversy. 

Unless of course, a given politician is the one who handles the Tweeting himself. 

Which is how Anthony Weiner does things. 

In recent days, Weiner noted he maintained an "edgy, very aggressive" Twitter feed. In fact, it's Weiner's very reliance on generating his own Tweets that has the potential to hoist him by his own, digital petard. If a Weiner aide handled the social media postings like most Congressional offices do, the Congressman could then have plausible deniability if not exculpatory evidence to demonstrate he was not responsible for the Tweet in question. Weiner is vulnerable because he Tweets himself and not an intern on his behalf. 

Perhaps it s refreshing that Weiner sends his own Tweets and doesn't follow the traditional Capitol Hill media management rules. Weiner's approach creates a more frank and certainly more entertaining feed. But by the same token, this episode could trigger a chilling effect for other politicians who have taken the go-it-alone approach. That would undoubtedly produce bleached exchanges which would blend into the political ether. 

Politics wasn't always this sanitized. Especially when it comes to talk of a lawmaker and their private parts. 

In the Pulitzer Prize winning tour-de-force "Master of the Senate," Robert Caro goes into exhaustive detail describing the "earthiness" of Lyndon Baines Johnson when he arrived in Congress. Caro writes about how Johnson wouldn't think twice about urinating in front of female secretaries in the parking lot of what is now the Cannon House Office Building. Caro says it would be nothing for Johnson to relieve himself in front of staff in his office, not into a toilet, but into a wash basin, no less. 

Caro says that Johnson would sometimes show himself off to Congressional colleagues and staff. 

"'Have you ever seen anything as big as this?'" Caro says Johnson would sometimes ask. 

LBJ would have struggled in the era of Twitter. 

This is the question now facing Anthony Weiner and other politicians who, for right or wrong, undoubtedly find themselves under fire due to a questionable social media posting. 

For former Rep. Chris Lee (R-NY), a semi-nude picture of the married Congressman sent to another woman was enough for him to resign in less than three hours. Especially as Lee was never accused of doing anything illegal. 

Even if the underwear picture turns out to be of Weiner, there doesn't appear to be anything illegal there, either. 

But a question of discretion lingers. 

Which is what vexes the social media landscape. 

Perhaps there are three classes of people who shouldn't use Twitter and Facebook: college students at a frat party, wide receivers for the Cincinnati Bengals and political figures. 

The Weiner saga will force politicians to reassess the potential risks of using these new communication forms. And that could produce bland postings which will dissolve quietly into the noise of the digital frontier.
