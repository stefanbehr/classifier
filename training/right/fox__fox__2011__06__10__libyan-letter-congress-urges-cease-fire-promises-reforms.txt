Congressional leaders received a letter purportedly from Libyan dictator Muammar Gaddafi asking the U.S. Government to push NATO for a cease fire with the North African nation. The beleaguered Libyan leader also says the peace offer also comes with a pledge to institute democratic reforms. 

The lengthy missive, which could not be authenticated by Capitol Hill offices, arrived Thursday. 

In it, the regime says that it is ready to halt armed hostilities. "Libya has consistently indicated to the international community its willingness for a cease fire and the deployment of international observers to monitor it," the letter said, "And we have also called for the cessation of military operations by NATO to provide the opportunity to address the crisis by political rather than military means." 

The letter also promises that the Libyan government is commited to democratic reforms. 

The memo attempts to drive a wedge between the United States and its NATO allies, specifically France. It accuses the European nation of engaging in armed hostilities "to advance its own commercial interests and to thwart the expansion and the expansion (sic) of America's commercial interests in the Libyan market." America, by contrast, is a nation with which the dictatorial regime would like to establish a "special relationship" based on "mutual respect and mutural benefit." 

House leadership staffers were dismissive of the troubled regime's appeal for peace. "If authentic, this incoherent letter only reinforces that Gaddafi must go. There's no disagreement about that. That's why so many Americans have questions - which the White House refuses to answer - about the administration committing U.S. resources to an operation that doesn't make his removal a goal," a Boehner spokesman said in a statement. 

Before leaving for a Congressional recess, the House passed a non-binding resolution demanding more information from the White House about the scope and duration of the US military operation in Libya. Several House committees will also take up the issue in hearings next week. 

Despite the letter's calls for a ceasefire, a rebel spokesman told reporters Friday that the Gaddafi regime launched an artillery attack on the city of Gadamis, 600 km southwest of Tripoli.
