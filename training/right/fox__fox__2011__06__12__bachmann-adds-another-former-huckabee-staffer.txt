Michele Bachmann's yet-to-be-officially-announced presidential campaign has added another former Mike Huckabee staffer. An announcement is expected shortly that Alice Stewart will serve as national press secretary. 

Stewart held a similar role with former Arkansas Governor Mike Huckabee's 2008 White House bid. 

The addition of Stewart brings a Huckabee flavor to the Bachmann bid. The Minnesota Congresswoman already has commitments from Iowa field-hand Wes Enos and campaign manager Ed Rollins, both filled the same roles with Huckabee's campaign. 

Stewart's job, until now, has been with the Arkansas Secretary of State's office. Stewart will jump in right away. She is expected to assist Rep. Bachmann with press chores in New Hampshire connected to Monday night's debate.
