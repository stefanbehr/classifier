Vice President Joe Biden wrapped up more than two hours of bipartisan deficit reduction talks saying he was "convinced" that an agreement could be reached that raises the nation's debt ceiling and "makes a real, serious down payment on the commitment to four trillion bucks (in deficit reduction) over the next 10 years." 

The veep offered no details on any possible package, joking that if he did, there would be no talks. But he did offer a goal for when an agreement might be reached, a first as these talks have gone on over the past two months, albeit with a few caveats. 

"We pray that, as my grandfather said, by the grace of God, the good will of the neighbors, and the creek not rising, I think we're going to be in a position hopefully by the end of the month, by the 4th of July recess, we have something to take to the leaders and actually begin to get down to the implementation piece of what kind of legislation is needed. That's the goal," Biden said. 

Aides to members in the discussions said Tuesday's topic was all about spending cuts, a highly-controversial topic between Republicans and Democrats. 

Rep. Chris Van Hollen, D-Md., exited the meeting reiterating what many Democrats have said this year and part of last, that a proposal to gradually reduce the nation's debt to 20% of GDP is off the table. 

When asked just how much any agreement would slash the debt, Biden said, "We're going to get well beyond $1 trillion." 

The talks are now occurring "round the clock," according to the former Delaware senator, "three days a week." And in a sign of the weight the White House has given to these negotiations, the vice president showed up on Capitol Hill at about 12:30pm Tuesday, two hours before his scheduled meeting with lawmakers, a time that was spent "prepping," according to his staff.
