The work began on Jon Huntsman's image began before he even returned from his ambassadorship to China . Now the future GOP presidential candidate has a new 26 second video on his personal website to continue that image campaign. 

The video shows a motocross rider zooming through what looks like the scenic backcountry of Utah. The trailer, set to music, teases with captions, "In 6 Days" and "Did not become famous with his band Wizard," hearkening back to when Huntsman briefly dropped out of high school to play keyboards in a rock band. 

The rider is not Huntsman himself, but it's symbolic as the former governor of Utah has been dubbed "The Motocross Mormon" who's gunning to be the "cool uncle" in the Republican field. 

The bit tags with "Paid for by Jon Huntsman" and is embedded in what's believed to be the first mass campaign e-mail from Huntsman's wife, Mary Kaye. In the message to potential supporters, she writes "Six days away -- from an incredible journey with someone completely different". 

Huntsman will announce he's running for president on June 21 in New Jersey's Liberty State Park. Mary Kaye's note encourages people to sign up to show up for the announcement. 

"In less than a week, a new generation of conservative leadership will emerge. No loud voices or drama, instead a vision for America that reverses the course we're on," the email reads. 

Polling points to the work that's cut out for Huntsman's team on building an image and name recognition. A recent Fox News Poll among GOP Primary Voters revealed 2 percent prefer Huntsman. It was conducted June 5-7, 2011 with a margin of error of plus or minus 5.5 percent.
