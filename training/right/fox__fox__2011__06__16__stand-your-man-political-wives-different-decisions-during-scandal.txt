When Rep. Anthony Weiner, D-N.Y., took the podium to resign after a Twitter photo scandal, his wife, Huma Abedin was not alongside him. 

Here is a look at some other political sex scandal press conferences and whether the wives decided to stand by their men. 



Bill Clinton 

January 26, 1998 at a press conference on education, Clinton said the famous line: "I'm going to say this again. I did not have sexual relations with that woman, Miss Lewinsky." 

He then gave a nationally televised speech/apology on August 17, 1998 in which he admits he had a sexual relationship with Lewinsky. 

Hillary Clinton was not by his side for either. 



Sen. and Former Presidential Candidate John Edwards (D-N.C.) 

On August 8, 2008 Edwards admitted a sexual affair in a television interview - but strenuously denied being involved in paying Rielle Hunter hush money or fathering her newborn child. Elizabeth Edwards was not by his side. 



Gov. Eliot Spitzer (D-N.Y.) 

Spitzer held two press conferences (March 10, 2008 and March 12, 2008) with Silda Spitzer at his side for both. He talked about allegations he patronized a prostitute at a Washington hotel. 



Gov. Mark Sanford (R-S.C.) 

Jenny Sanford was not present at a June 24, 2009 press conference in which the South Carolina governor tearfully apologized for lying about his whereabouts as he carried on an affair with an Argentine woman. 



Gov. Jim McGreevey (D-N.J.) 

McGreevy held an August 12, 2004 press conference to address allegations of a homosexual affair. His wife Dina Matos McGreevy was by his side. 



Sen. David Vitter (R-La.) 

Vitter held a press conference on July 16, 2007 to discuss accusations he patronized prostitutes. His wife Wendy Vitter was by his side and spoke to reporters. 



Rep. Chris Lee (R-N.Y.) 

Lee didn't hold a press conference after bare-chested Craigslist photos of him surfaced and he was accused of trying to instigate an affair through the website. He sent a resignation letter to Congress and New York Governor Andrew Cuomo dated February 9, 2011. 



Sen. Larry Craig (R-Idaho) 

Craig held an August 28, 2007 press conference to say he did nothing wrong after his Minneapolis Airport bathroom arrest. His wife, Suzanne Craig, was by his side.
