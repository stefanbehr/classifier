House Democratic Caucus Chairman John Larson of Connecticut: 

It's difficult to watch the self destruction of a friend, and to witness the breaking of hearts over that can only be categorized as reprehensible behavior and bad judgement." 

== 

Republican Texas Congressman and Doctor Ron Paul to Fox News' Bill Hemmer : 

As a physician, I think what he needs is a psychiatrist more than anything else. 

== 

Democratic New York Congresswoman Nita Lowey: 

"There is life after Congress for Anthony Weiner and I hope he devotes himself to repairing the damage he caused to his personal life." 

== 

New Jersey Democratic Congressman Bill Pascrell: 

"If he came out straight forward in very beginning... I think he could have survived this, I think that was straw that broke camel's back." 

== 

Democratic New York Congressman Steve Israel : 

"His behavior is reckless and distracting... Last week, Republicans introduced a billion dollar bill to privatize Social Security . Most people don't know this because we're distracted by Weiner." 

== 

Democratic New York Congressman Ed Towns: 

"I think a lot of people will be happy this will be resolved. This is a personal matter between him and his constituents." 

== 

Republican New York Congressman Peter King , says it's unfortunate that Rep Anthony Weiner has chosen to resign as he was a dedicated member, even though the two lawmakers had their policy differences. 

King added though that it would have been impossible for Weiner to continue on the Hill, "He would have been a dead man walking."
