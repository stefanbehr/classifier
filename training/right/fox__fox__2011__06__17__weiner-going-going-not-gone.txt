"Bye, bye, pervert!" 

Not just yet. 

Benjy Bronk of The Howard Stern Show was in rare form, hectoring Rep. Anthony Weiner (D-NY) as he announced his resignation at a news conference in New York yesterday. 

Not so fast... 

If there's one thing you should know about Anthony Weiner right now... 

Just one thing. After all the Tweeting. The sexting. Weiner's cracks about "the tip of al-Qaida's sword" and being "a little stiff." The denials, the lies, network interviews, allegations of hacking, backtracking, double entendres, grey underwear and yes, even "certitude"....... 

The one thing you need to know is that Anthony Weiner remains a duly-chosen and sworn Member of Congress, representing the Ninth Congressional District of New York. He has not resigned yet. And Weiner remains on an official, two-week leave of absence, as granted by the House Monday night. 

That's the House of Representatives. Not the Senate, as Bronk called Weiner "senator" at one point while asking him about anatomy. 

Bababooey. 

Resigning from the House is easy. But it's done in two steps. And usually in the form of a letter. 



We presume Weiner won't notify officials via Twitter. 

When Weiner is ready, he will send a letter to New York Secretary of State Cesar Perales to notify him of his resignation. Perales will then inform New York Gov. Andrew Cuomo (D). Cuomo will then declare the Congressional seat vacant. It's then up to Cuomo to decide when and if to hold a special election. 

Weiner will also send a near-identical letter to House Speaker John Boehner (R-OH). 

To Congress, nothing is official until the letter is read on the House floor. And with the House out of session Friday, the earliest that can happen is sometime next week. 

But first, Weiner has to write the letter. He doesn't even have to show up to Congress in person to step aside. 

Resignation letters take different forms. They're usually short and declarative. Former Rep. Chris Lee (R-NY) stepped down earlier this year after messaging semi-nude photos of himself to a woman he met on Craigslist. Lee's letter indicated his resignation was effective at 5 pm that day. 

Former Rep. Neil Abercrombie (D-HI) set a date several weeks in the future when he tendered his resignation letter. 

Former Rep. Mark Foley (R-FL) simply announced he was resigning. 

So, Weiner survives in Congress just a little longer. 

The irony is that Weiner may have been able to remain in Congress were it not for a serious error. 

"If he came out straightforward from the very beginning, I think he could have survived this," said Rep. Bill Pascrell (D-NJ). "It's a flaw in his character." 

Pascrell spoke with Weiner about his future and took umbrage with his colleagues who called on the New York Democrat to resign. 

"We owe something to the other person not to be so judgmental," Pascrell said. "People try to be their own moral barometer." 

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 

The Congressional barometric pressure gauge indicated a news storm was brewing early Thursday morning. 

House Democratic leaders planned a closed door meeting to discuss stripping Weiner of his committee assignment. Multiple sources indicated that this was really an effort to apply pressure on Weiner to resign. Weiner holds a seat on the coveted Energy and Commerce Committee. And a lawmaker without a Congressional committee is like person without a country. 

The Capitol buzzed with rumors that Weiner was about to resign. Everyone on Capitol Hill wanted to talk about Anthony Weiner. Except for those reporters hoped would talk about Weiner. 

House Republicans promptly started a press conference about job creation at 9:30. But no one wanted to listen to that. 

"Speaker Boehner, has Congressman Weiner put in his resignation?" asked CNN's Deirdre Walsh as soon as GOP lawmakers concluded their opening remarks. 

"No," came a terse reply from Boehner, anxious to move on to a different topic. 

"Are you aware he might resign from Congress?" continued Walsh. 

"No," answered Boehner. "When I become aware of it, I'll let you know about it." 

A cacophony of questions showered in about Weiner. So Boehner tried to escort the discussion back to job creation. 

"It's been a distraction," Boehner continued. "The American people are asking where are the jobs. They want us to focus on job creation. And this is just a distraction that was unnecessary." 

The conversation then moved to Libya. 

Less than an hour later, House Minority Leader Nancy Pelosi (D-CA) tried a more direct approach during her meeting with reporters. Pelosi brought along Reps. Sander Levin (D-MI) and Mark Critz (D-PA) to discuss the economy and potentially run rhetorical interference. And the leader fired a pre-emptive strike in hopes of quashing the Weiner discussion. 

"We are here to talk about jobs, about Medicare and protecting the middle class," instructed Pelosi. "If you are here to ask a question about Congressman Weiner, I won't be answering any." 

But of course, the first query out of the gate for Pelosi dealt with Weiner. 

"Perhaps I was unclear," Pelosi replied icily. 

++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 

Larry Flynt was not unclear Thursday. 

And even though Weiner had yet to write his resignation letter, Flynt was busy writing the Congressman a letter of his own. 

"I would like to make you an offer of employment at Flynt Management Group, LLC," Flynt wrote, the founder of Hustler magazine. "This offer is not made in jest." 

In the employment missive, Flynt complimented Weiner on his "intensity" and suggested that the Congressman's service on the Energy and Commerce Committee would make him a "valuable asset" to his, um, firm. He also offered to bolster Weiner's Congressional salary and pay to move the lawmaker to Beverly Hills. 

Of course, this is pending Weiner's presumed resignation from the House. 

Even though Weiner was in New York, the scene outside Weiner's office Thursday was a familiar one. A phalanx of television photographers squatted in the hallways in hopes he wayward lawmaker might swing by. 

The photographers were there all day. 

Except around 1 pm. That's when U.S. Capitol Police ordered the corridor closed for security purposes. 

Someone discovered a "suspicious package" outside Weiner's office. 

Which is what launched this sordid tale a few weeks ago.
