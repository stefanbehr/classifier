A White House quest for input from leaders in the business community about what changes they'd like to see in their relationship with the federal government, may have resulted in more feedback than the administration bargained for. 

William Daley , Obama's Chief of Staff, pow-wowed with hundreds of manufacturing executives Thursday at the National Association of Manufacturers meeting in Washington, D.C. and the take-away was that they don't like federal regulations they say are hampering their businesses. 

Daley admitted, "There is an enormous number of rules and regs that are kinda in the pipeline." Citing the administration's efforts to streamline burdensome regulations, he added, "and we're trying to bring some rationality to them, especially at a time of economic crisis." 

Some were dismissive of those efforts. Associated Industries of Florida CEO Barney Bishop said bluntly, "We think there's a thin facade by the administration to say the right things, but they don't come close to doing things." 

At the White House Friday, Spokesman Jay Carney countered, "The regulatory look-back is real. It is -- it has teeth. It will -- has and will produce results in terms of eliminating unnecessary regulations or regulations that have outlived their usefulness or ones that are overly burdensome." 

Back in January, when the president tapped Daley as his Chief of Staff, the Manufacturers applauded the choice, saying, "The NAM had a strong relationship with [Daley] during his days as secretary of Commerce, and we look forward to working with him again on policies that will move our country forward - especially on job creation, economic growth and global competitiveness." 

The administration has been touting the flourishing manufacturing sector as a promising arena for much-needed jobs. Perhaps it's that growing confidence that led executives to speak out. 

When asked about the myriad of complaints from industry leaders that Daley faced, Carney agreed that there's no denying many are legit, "And you have to understand that he went in there with no prior knowledge about the cases that were put before him, but obviously the ones that sounded bad he thought sounded bad. And he said so. "
