Amid a breakdown in debt negotiations, House Majority Leader Eric Cantor , R-Va. announced Thursday that the House will consider a balanced budget amendment next month. 

"House Republicans have made clear that we will not agree to raise the debt limit without real spending cuts and binding budget process reforms to ensure that we don't continue to max out the credit card," Cantor said in a statement. "One option to ensure that we begin to get our fiscal house in order is a balanced budget amendment to the Constitution, and I expect to schedule such a measure during the week of July 25th." 

The Tea Party has been clamoring for a balanced budget amendment. House Republicans passed such an amendment as a part of the Contract with America with precisely 300 votes. But the measure failed in the Senate by one vote in 1995. 

A Constitutional Amendment needs a two-thirds vote of both bodies of Congress and approval by three-quarters of all states.
