The Obama administration is working on a $350,000 program that would enlist government-hired contractors to call doctor's offices and set-up fake appointments. 

The goal would be for callers to see how long it takes to get an appointment, if they're accepted at all, and then see whether kind of insurance - private or a government program like Medicare or Medicaid -- has an influence on getting in to see a doctor. The administration would then use the data to assess why there's a shortage in primary care physicians. 

The details were first reported by the New York Times Monday, with a lot of specificity including the states where the calls would be targeted, and even a script of a sample call. 

The story noted 465 calls would be made to nine states, totaling 4,185 attempted fake appointments to Florida, Hawaii, Massachusetts, Minnesota, New Mexico, North Carolina, Tennessee, Texas and West Virginia. There would be at least two phone calls - one where the fake patient has private insurance and one call where he or she has public insurance. A small percentage would then be called a third time when it would be revealed the call is on behalf of the government. 

Callers would dial from a blocked number, and follow a script, as reported by the New York Times: 

Mystery shopper : "Hi, my name is Alexis Jackson, and I'm calling to schedule the next available appointment with Dr. Michael Krane. I am a new patient with a P.P.O. from Aetna. I just moved to the area and don't yet have a primary doctor, but I need to be seen as soon as possible." Doctor's office: "What type of problem are you experiencing?" Mystery shopper: "I've had a cough for the last two weeks, and now I'm running a fever. I've been coughing up thick greenish mucus that has some blood in it, and I'm a little short of breath." 

When asked in Monday's White House briefing how the "stealth" operation could be so "stealth" with so many details already revealed, White House Press Secretary Jay Carney didn't see that as a concern. 

"I think it's important to point out that this is a proposal, there will be public hearings, hasn't happened yet. We will look at this and decide after comment from all quarters about moving forward." 

Carney also noted that this practice has been used by previous administrations, including George W. Bush's, who was looking into Medicare Advantage. The White House also points to the Government Accountability Office and the Federal Trade Commission using similar undercover-type techniques. 

Former Deputy Health and Human Secretary under George W. Bush, Tevi Troy, says the program is going a bit too far. 

"It's clearly creepy, why is the federal government spending $350,000 dollars on some kind of James Bond spy program. The real problem is that doctors don't like the impositions that the government makes in terms of payment, practice and process when you deal with the government. When you take Medicare and Medicaid patients, and if you want to address this issue, what they should do is try to fix the problem rather than spy on doctors," Troy said. 

He adds that he worked under the Medicare Advantage program and said it was different - these new rounds of calls aren't looking for fraud and theirs was a quality control issue. 

The program is expected to start in a few months and the administration reportedly signed on with National Opinion Research Center at the University of Chicago to help gather the information. 

Dr. Marc Siegel is part of Fox News' medical "A-team" and noted how elderly patients have more medical problems and harder to take care of and that the government knows they will be restricted. 

He adds that finding out that a doctor would prefer to see a private patient doesn't help. 

"There is no way they can show anything accurate by doing this except making us more upset," Siegel said. 

Fox News' Shannon Bream and Mike Emanuel contributed to this report.
