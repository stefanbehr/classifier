President Obama will hold a news conference with reporters Wednesday at the White House at 11:30am ET, the 14th of his presidency. 

His last solo news conference was in March. Mr. Obama did hold a joint press availability with German Chancellor Angela Merkel in June when she was in Washington, but only a few questions were asked. 

In March, the president was mostly asked about energy and gas prices, but also talked about Japanese earthquake and tsunami.
