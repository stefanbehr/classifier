Is President Obama also a Supreme Court justice? Does he wear a black robe in the West Wing? Rep. Michele Bachmann , R-Minn., thinks so. 

An unofficial dispute broke out Wednesday between President Obama and the GOP presidential candidate. 

Stumping in Charleston, South Carolina, Bachmann passionately spoke of defending the constitution saying it's a magnificent political document.' She then quickly pointed out that Obama would not uphold the law of the land as it pertains to the Defense of Marriage Act. The administration stopped defending court challenges to the law when Attorney General Eric Holder advised he believes it is unconstitutional. 

"Well, Mr. President take off that Supreme Court robe," she told the crowd. "You are not the Supreme Court. You don't get to decide if a law is constitutional or not." 

Later Wednesday at a presidential press conference in the East Room of the White House , the second question, asked by NBC's Chuck Todd, was a hodge-podge question,' according to Obama, that sought answers on an array of legal issues. 

"Do you believe the War Powers Act is constitutional? Do you believe that the debt limit is constitutional, the idea that Congress can do this? And do you believe that marriage is a civil right?," Todd asked. 

That's when the former constitutional law professor distanced himself from his legal background in answering the question. 

"I'm not a Supreme Court justice, so I'm not going to put my constitutional law professor hat on here," said President Obama in his initial response to the question. 

Throughout Obama's nearly nine-minute response, he wore the hat of a politician instead of professor to avoid answering the question directly. 

Obama said, "I don't have to get to the constitution question," "I don't have to get to the question" and "I'm just saying I don't have to reach it. That's a good legal answer." 

But Bachmann seems to disagree with the president on how heavy of a constitutional role he plays.
