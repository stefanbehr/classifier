Chris Stirewalt talks to FCC Commissioner Robert McDowell about internet neutrality and the Fairness Doctrine. 

McDowell asks Chris, "most people think the internet works pretty well and that the government doesn't work very well, so why put the government in control?" 



Watch Power Play live at 11:30am Eastern Monday through Friday at http://live.foxnews.com/ 

Watch the latest video at FoxNews.com
