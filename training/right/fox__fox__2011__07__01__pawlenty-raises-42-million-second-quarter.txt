Senior officials in former Minnesota Governor Tim Pawlenty's presidential campaign tell Fox News the campaign raised $4.2 million in the second quarter. 

While the amount is far less than the nearly $20 million in second quarter fundraising Mitt Romney's campaign announced Thursday, it doubles former Utah Governor Jon Huntsman's outside contributions in the quarter and likely solidifies Pawlenty's place in the race for the GOP nomination. 

"Gov. Pawlenty will report that his campaign has raised about $4.2 million, and begins the third quarter with more available cash-on-hand than the Republicans who won the Iowa caucuses and New Hampshire primary had in July 2007," the campaign's communications director Alex Conant told Fox News in a statement. 



Other campaigns are expected to announce their second quarter amounts in the coming days as a July 15 deadline approaches on which the Federal Elections Commission discloses the amounts of money they raised.
