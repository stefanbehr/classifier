A popular social networking site that has infiltrated the nation made its way inside the White House Wednesday as the president took questions during a Twitter Forum in the East Room. Twitter cofounder and executive chairman Jack Dorsey moderated the event where the president answered selected questions from users by way of so-called "curators" who filtered the tweets. 

Questions on jobs, the economy and debt ceiling negotiations started off the town hall with the first questioner asking Obama what mistakes he has made in handling the economic downturn and how he would have done things differently. 

"[O]ne would have been to explain to the American people that it was going to take a while to get out of this. I think even I did not realize the magnitude, because most economists didn't realize the magnitude of the recession, until fairly far into it, maybe two or three months into my presidency where we started realizing we had lost four million jobs before I was even sworn in," Obama said. 

"And so I think people might not have been prepared for how long this was going to take and why we were going to have to make some very difficult decisions. And I take responsibility for that because setting people's expectations is part of how you end up being able to respond well," he said. 

Obama added that the housing market hasn't bottomed out as quickly as his administration expected and that it continues to be a drag on economic growth. 

But the questions quickly turned to the debt and deficit battle in Washington. A question from a New York Times reporter dug deeper, asking Obama whether it was a mistake not to secure a debt ceiling increase at the same time he made a deal to extend the Bush era tax rates. 

"It would have been great if we were able to also settle this issue of the debt ceiling at that time," the president said. "That wasn't the deal that was available." 

The president was able to dance around the question of whether he would use the 14th Constitutional Amendment to raise the nation's debt limit without congressional approval by pointing out the potential financial fallout from if the nation defaults and saying it's up to Congress to keep that from happening. 

"There's some people who say under the constitution, it's unconstitutional for Congress not to allow Treasury to pay its bills and are suggesting that this should be challenged under the constitution," Obama said. "I don't think we should even get to the constitutional issue. Congress has a responsibility to make sure we pay our bills, we've always paid them in the past. The notion that the U.S. is going to default on its debt is just irresponsible." 

Obama's answers weren't held to the famous 140 character limit within which Twitter users must express their thoughts. While his answers weren't as long as those at last week's presidential news conference, the president did take at least a couple of minutes on each answer. 

And it wasn't just average Twitter users who were able to question the president. Twitter's curators selected a question from the Twitter handle of House Speaker John Boehner , R-Ohio. 

"After embarking on a record spending binge that has left us deeper in debt, where are the jobs?" the tweet read. 

The president noted that since the question came from the speaker, it had a partisan slant, but Obama added there are things both sides can agree on and that he'll keep working to find common ground on spending issues. 

"We haven't gotten the kind of cooperation that I'd like to see on some of those ideas and initiatives but I'm just going to keep on trying and eventually I'm sure the speaker will see the light," Obama said. 

While some of the tweeted questions moved on to other subjects -- like NASA and education -- the deficit, debt ceiling and economy were the common thread that ran throughout the town hall. And the president used one of those questions to repeat the idea that the debt ceiling is too important to be used as a negotiating tool. 

"The debt ceiling should not be something that is used as a gun against the heads of the American people to extract tax breaks for corporate jet owners or oil and gas companies that are making billions of dollars because the price of gasoline has gone up so high," he said. "I'm happy to have those debates. I think the American people are on my side on this. " 

But a Boehner spokesman responded to that answer, saying "That kind of rhetoric might make for a colorful sound bite, but it isn't reflective of the debate taking place. Republicans have simply noted that the American people will only tolerate an increase in the debt limit if it comes with even bigger spending cuts as well as spending reforms, and is free from job-destroying tax hikes." 

The debate moves from Twitter to the West Wing Thursday when congressional leaders come to the White House to again meet with the president on the debt and deficit.
