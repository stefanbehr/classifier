In what seemed more like a Sunday church service than a Capitol Hill press conference, Rep. Charlie Rangel, D-N.Y., called out for his fellow lawmakers and all Americans to do "the Lord's work" as a solution to fixing the debt ceiling. 

"These are not political questions," Rangel asserted. "These are moral questions." 

The raspy-voiced congressman urged spiritual leaders to highlight the role federal programs including Medicaid, Medicare and Social Security play in protecting the vulnerable, sick and poor despite Washington's concern about the $14.3 trillion deficit. 

"Why don't you call your pastor, your rabbi, your imam? There has to be a moral answer," Rangel preached to reporters. It was just a year ago that the former chairman of the House Ways and Means Committee was censured by his colleagues for various ethical violations. 

Before Rangel's comments, Friday's debate about cutting Social Security was discussed at a Ways and Means subcommittee hearing where congressmen vacillated on whether reforms would lessen the debt crisis. 

"Social Security has never contributed a dime to the nations $14.3 trillion debt, not a penny to our federal deficit or any year of our nation's history, yet some in this town insist that we should cut Social Security benefits for seniors to pay for these deficits," said Rep. Xavier Beccera, D-Calif. 

"In times of this deficit, Treasury [Department] has to borrow it. Today the U.S. borrows 40 cents for every dollar it spends, much of it from the Chinese and sends the bill to our children and grandchildren, and part of that's to cover Social Security," said Rep. Sam Johnson, R-Texas. 

While lawmakers from both parties are asking what President Obama is going to do about the debt ceiling, Rangel continues to ask, "what would Jesus do?"
