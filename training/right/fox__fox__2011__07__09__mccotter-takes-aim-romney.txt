Most of the Republican presidential hopefuls have spent more time criticizing President Obama than attacking each other. 

That all changed when Rep. Thaddeus McCotter, R-Mich., threw his hat into the ring, and he wasted no time zeroing in on fellow 2012 GOP contender, former Massachusetts Gov. Mitt Romney . 

"Unemployment rising, people struggling in the Obama-Romney economy," McCotter tweeted Friday. 

The congressman expanded on that critique during a Saturday interview with Fox News. "We have Obama as the champion of big government, we see Romney as champion of bail out banks in Wall Street bailout. They are less rivals than they are running mates," he said. 

McCotter has good reason to focus on Romney. The former Massachusetts governor leads the latest Fox News Opinion Dynamics poll with 18 percent. Plus, as the son of former Michigan Gov. George Romney, he has strong ties to McCotter's home state. 

Romney's support of the 2008 Wall Street bailout is at the heart of McCotter's attack. 

"Unfortunately, what we're seeing out of Washington today and out of many in the Wall Street community is they're thinking inside an economic coffin for the rest of America," McCotter said 

But McCotter defended his own support for the 2008 auto bailout, saying he's favor of keeping Americans working instead of losing those jobs to overseas manufacturers, saying that opponents of the plan "don't have a problem with communist nuclear-armed China being the manufacturing giant in the 21st century."
