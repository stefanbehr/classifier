Former New Mexico Governor Gary Johnson was the first to jump into the GOP presidential race, but admits he is "the least known candidate." 

Johnson did not even pull 1 percent in the latest Fox News GOP Presidential Poll, which was taken at the end of June. But, in his home state of New Mexico, Johnson says it's a different story. 

"In the only state who knows who I am [New Mexico], I have a plus 12 favorability.... So what the polls are overwhelmingly saying is that there is no clear front runner, that people don't know who the candidates are," Johnson told Fox News this Saturday. 

Johnson argues that once you get to know him, you'll like him. 

So, in an attempt to introduce Johnson. Here are some highlights: 

He is for gay marriage and believes the government should "get out of the marriage business" 

He "supports a women's right to choose" when it comes to abortion 

He won't be signing pledges on social issues like the Susan B Anthony List Pro-life Pledge or Bob Vander Plaats's Iowa pledge 

He says signing the Cut, Cap and Balance pledge was an "easy decision" 

Johnson's strategy for getting known lies in the First-in-the-Nation Primary state. 

Johnson says, "It's about New Hampshire, and it's about going out and meeting a whole bunch of people in New Hampshire, which by the way is a great political environment. New Hampshirites really take it as a responsibility to go and meet and talk and discuss the issues with all the candidates." 

Watch the latest video at FoxNews.com
