GOP presidential contender Ron Paul blames big government for holding up debt talks and is forecasting an all-out bankruptcy for America. 

"I am very hopeful and positive in the long run, but I think we are going to go through a bankruptcy first," the Texas congressman told Fox News in a Sunday Interview. 

Paul admits a compromise will eventually come, but at the expense of younger generations. 

"Everything we have done so far has just spent more and run up the deficit," Paul said. 

In a statement released Friday, the hopeful said he's doubtful that a deal on the debt will resolve the country's problems. "In Washington, if you hear about a so-called deal, you can be sure the taxes will come, but the cuts never will," he wrote. 

Paul also took time during his Sunday interview to reflect on what he sees as the bright prospects of his campaign. 

Paul highlighted a win Saturday in a straw poll conducted by the Coalition of New Hampshire Taxpayers, a conservative grassroots organization dedicated to smaller government and lower taxes. 

All smiles, Paul promised to keep working hard, "especially in the early states."
