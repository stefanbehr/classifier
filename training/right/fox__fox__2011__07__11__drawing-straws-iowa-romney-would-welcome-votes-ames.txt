Mitt Romney is running an Iowa-lite campaign in his second bid for the GOP presidential nomination. In 2008, Romney was all-in in Iowa competing early and arduously. His campaign also spent heavily in the Iowa Straw Poll, winning the Summertime contest. 

This go-around, Romney's made it clear he that while he respects the first-in-the-nation nomination contest, he will visit and spend less in Iowa. And August's Iowa Straw Poll? Romney is not playing. 

That does not mean Romney won't get votes at the Straw Poll. 

First of all, take a look at every Republican poll in Iowa this year. Romney has been at or near the top in every one. Having a name familiar with likely caucus-goers certainly helps, but the likes of Rick Santorum, Tim Pawlenty, Herman Cain and of late Michele Bachmann have been criss-crossing the Hawkeye State. 

But while news organizations have been chasing after the candidates, Romney has been staying in touch with his Iowa backers from afar. 

David Kochel, who handles press chores for the Romney campaign in Iowa, says in at least one instance supporters have asked about casting Straw Poll votes for the former Massachusetts governor. 

Kochel says last month Romney held a conference call with 40 to 50 Republican activists who backed Romney's 2008 campaign. At the tail end of the call, he says a supporter on the call asked about casting a Straw Poll vote for Romney. 

Romney, says Kochel, reminded those on the call he would not be an active participant but added if they wanted to vote for him, "They would be welcome to." 

Kochel was quick to add that the call and Romney's "welcome to" reply does not constitute an attempt to run an off-the-radar Straw Poll campaign. 

It was an answer to a question, which now begs another query: How many votes might Romney gather on that second Saturday in Ames? 

We will see.
