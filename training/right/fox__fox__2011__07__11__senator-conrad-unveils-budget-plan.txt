Sen Budget Cmte Chairman Kent Conrad, D-ND, unveiled his budget blueprint Monday which he says cuts the deficit by $4 trillion over 10 years, $50 billion more than House Republicans' plan, reducing the nation's deficit from 9.3% of Gross Domestic Product to 1.3% by 2023. The plan calls for a tax hike on individuals making more than $500,000 annually and couples earning in excess of $1 million in part to pay for a 10-year, $1.5 trillion fix for those Americans who would otherwise be hit by the Alternative Minimum Tax . An additional hike in the estate tax to 2009 levels will also help cover lost revenue, as will a sliding scale shutdown of both offshore tax havens and the elimination of tax expenditures (if you close down more havens, you have to end fewer expenditures, etc). 

A member of both the president's fiscal commission and the bipartisan "Gang of Six," Conrad sought to debunk Republicans' assertion that raising taxes on the wealthy could tank the fragile U.S. economy. 

"Facts are stubborn things...The fact is, we had the longest period of uninterrupted growth in the economy during a period in which revenue was where we propose in this budget," Conrad argued, saying that if the nonpartisan Congressional Budget Office were to analyze his plan, it would score it "as being a $765 billion tax cut, because we are replacing revenue lost by extending other tax cuts." 

The chairman, who plans to retire when his current term ends in 2012, said he hopes his plan will inspire the president and the bipartisan negotiators trying to find a deal on a major deficit reduction plan, but in an ominous sign, Conrad hinted that he could oppose a plan that cuts only $2 trillion from the deficit. 

"What's needed for the country is a $4 trillion package," Conrad said, "$2 trillion does not solve the problem. Virtually any economist will tell you that. I'm going to do everything that I can to push for the larger package." 

Conrad said he followed the parameters of the Simpson-Bowles fiscal commission when calling for "$886b out of the security function," in other words defense spending cuts coupled with reductions in homeland security outlays. 

On the entitlement side, the Conrad plan differs starkly from the House GOP plan, which calls for a major overhaul of Medicare. "I think very significant changes are required, but we made very significant changes in Medicare," Conrad said, referring to the $500 billion in savings called for under President Obama's health care reform plan. Details were not available, but Conrad described his budget's Medicare savings as "modest." 

"If Senate Democrats' plan is so great, why won't Democrats make it public - as required by law?" asked Stephen Miller, spokesman for Sen. Jeff Sessions , R-Ala., top Republican on the Budget panel.
