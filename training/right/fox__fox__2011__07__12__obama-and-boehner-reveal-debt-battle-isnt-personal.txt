In the midst of summertime deficit negotiations as heated as the steamy Washington streets, President Obama seems to have altered the Harry Truman adage to: "If you want a friend in Washington, find a Republican." 



For Obama, that Republican is House Speaker John Boehner of Ohio, the GOP's point man in the everlasting debate over the nation's debt limit. 

"My experience with John Boehner has been good," the president said of his personal dealings with the leader during a press conference Monday. "I think he's a good man who wants to do right by the country." 



The speaker returned the compliment at his own press availability a short while later: "I appreciate what the president said today about the need for us to come together and get this done. Our disagreements are not personal. They never have been." 

So how could the two most powerful men in Washington who have such a personal respect for one another still be scrapping it out on such consequential issues as the debt ceiling? 

The answer, according to the president, lies within their caucuses. 

"[The] politics within his caucus are very difficult," Obama opined of Boehner's pitching problem. "And this is part of the problem with a political process where folks are rewarded for saying irresponsible things to win elections or obtain short-term political gain." 

But the president admitted, "[L]ook, it's also going to take some work on our side, in order to get this thing done." 

He was explaining the public face of things. But the back-room negotiations tell a different story. 

Back in April, a similar down-to-the wire scene played out over the nation's budget when differences between the parties threatened to shut down the federal government. 



In briefings with reporters then, administration officials painted a picture of an honest broker in Boehner. This was the first real test of a divided government, they said. 

The president didn't want a shutdown and neither did the speaker. 

As the talks intensified, the two worked out the parameters of an agreement, along with Senate Majority Leader Harry Reid, D-Nev. But when their respective staffs began hashing out the details, something got lost in translation. 

So the president picked up the phone and spoke frankly with the speaker. They each knew what needed to happen. A deal was reached and a shutdown was averted. 

The burgeoning relationship between the president and the speaker gave hope that there was something to build on. 

Days and months went by. All the while, from the public's point of view, things looked a lot like the same old D.C. acrimony. There were barbs from podiums and accusations of obstinance--an image of discontent on one issue or another. 

Then, in mid-June, friendly round of golf between the president and speaker stirred speculations of deals being made on the debt. 

White House Press Secretary Jay Carney indicated it was something far short of that, saying the outing was "meant to be an opportunity for the speaker and the president, as well as the vice president and Ohio Governor [John Kasich] to have a conversation, to socialize in a way that so rarely happens in Washington." 

But those who know Boehner's political prowess best say the speaker thrives in social environments, building relationships over drinks or dinner. Before it was banned by then-House Speaker Nancy Pelosi , D-Calif., Boehner was known to hold caucuses in the back of the House Speaker's Lobby over cigarettes. 

Boehner's style often resonates with Washington insiders. 

"I've known the speaker since the mid-1990s, when I met him while covering Congress," Carney, a former journalist, tells Fox News. "I've always gotten along well with him, both as a reporter and since I left journalism to work for President Obama and Vice President Biden." 

Whether borne of necessity or a genuine fondness for one another, this unlikely kinship between president and speaker has appeared to remain resilient. 

Though fundamental issues remain, Speaker Boehner on Monday stuck to the view that those differences are exclusive to the business at hand. "The gulf between the two parties right now is about policy," he said. 

"It's not about process, and it's not about personalities." 

Fox News' Chad Pergram contributed to this report.
