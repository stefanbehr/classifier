Watch the latest video at FoxNews.com 

Florida Sen. Marco Rubio is putting to rest predictions that he will be the 2012 GOP vice presidential nominee, for now. 

Appearing on "Fox and Friends" Tuesday morning, Rubio squashed Fox News Contributor Dick Morris's suggestion on Monday night's "Hannity" that whomever gets the Republican presidential nod should pick Rubio, "no question." 

"That's a nice compliment, but it's not true," Rubio said. "I'm not the vice presidential nominee; I will focus on my job as the Senator from Florida." 

"I'm not the vice presidential nominee," the senator, a Tea Party darling, continued. "This is the job I wanted. It's the job I ran for." 

In the Senate, he said, "I can make a difference."
