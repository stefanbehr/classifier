Former New York Republican Senator Alfonse D'Amato says Senate Minority Leader Mitch McConnell's, R-Ky., plan to give the president unilateral power to raise the debt ceiling is "dead on arrival." 

"They're not going to give to Obama absolute power, almost dictatorial," he said. "It would really be unprecedented." 

D'Amato made the comments to Juan Williams who was hosting today's edition of Power Play Live. You can catch Power Play Live with Fox News Digital Politics Editor Chris Stirewalt each weekday at 11:30 a.m. EDT at http://live.foxnews.com 







Watch the latest video at FoxNews.com
