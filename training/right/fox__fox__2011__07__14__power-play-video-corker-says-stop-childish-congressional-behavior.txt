Sen. Bob Corker , R-Tenn., says he's fed up with the childish behavior that has developed on Capitol Hill , especially around debt ceiling and deficit talks. 

He told James Rosen on today's Power Play Live that a lack of discipline has led to the last-minute contentious negotiations. 

"The leadership in the Senate on both sides of the aisle has tried to keep us from having to make tough decisions so that in 2012, one side or the other will have an advantage," Corker said. "And so today, we're going through a spending bill on the U.S. Senate floor but we don't have a budget." 

You can catch Power Play Live each weekday at 11:30 a.m. EDT at http://live.foxnews.com 





Watch the latest video at FoxNews.com
