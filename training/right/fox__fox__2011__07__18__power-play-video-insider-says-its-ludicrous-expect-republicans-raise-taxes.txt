The heat that's blanketed Washington this summer with 90 degree temperatures and humidity so high it feels like the triple digits has been no match for the political hot air being blown up and down Pennsylvania Avenue as leaders try to come to a debt ceiling deal. 

Both sides say they want a solution, but stark ideological differences (spending cuts vs. revenue increases) on what constitutes an acceptable outcome has lead to a rather hostile political environment. 

"The catastrophic consequences of the U.S. government reneging on existing obligations would be so dire that no serious person would consider it," Chris Stirewalt pointed out in Monday's edition of Power Play , noting the chances of an actual default are highly unlikely. 

But in the words of President Obama: Until everything is agreed to, nothing is agreed to. And so the threats of default and "Armageddon" continue to loom large. 

Texas Republican Kevin Brady, member of the powerful House Ways Means Committee and ardent conservative, told Power Play Live that House Republicans will continue to offer up ways to cut spending and push a balanced budget amendment because so far, the president is "still stuck on the tax increases." 

House Republicans have proposed something called Cap, Cut and Balance, which Obama has already vowed to veto on the extremely remote chance it would ever make it to his desk. 

Political insider, Charles Hurt of the Washington Times, says most Americans agree with the Republicans line of thinking: cut spending instead of raising taxes. But Hurt also says the marketing of that message has fallen woefully short - and points to a key argument that could help the GOP back President Obama and Democrats into a political corner. 

Watch the latest video at FoxNews.com 



It's impossible to predict if the debt ceiling drama will be resolved and whether or not it will happen before average Americans feel the pain . Make sure to tune into Power Play Live w/Chris Stirewalt weekdays at 11:30 ET at live.foxnews.com . Read the print edition beforehand and jump into the online debate during the show.
