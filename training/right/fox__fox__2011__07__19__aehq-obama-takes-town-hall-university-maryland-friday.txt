With the August 2 deadline to raise the nation's debt ceiling fast approaching and the majority of negotiations to lift it happening on Capitol Hill , President Obama is looking to garner support from college students Friday when he goes to the University of Maryland to host a town hall. 

White House officials haven't announced the topic of the town hall, though the nation's debt crisis is sure to come up in the event that begins at 11 a.m. 

This will be the eighth town hall Mr. Obama has hosted this year. The president has made a strong effort to cater to the younger generation, holding a town hall at Facebook headquarters in Palo Alto, California in April and earlier this month, hosting the Twitter Town Hall in the East Room of the White House . 

Obama last visited Maryland in early April for an event at a UPS facility in Landover to promote the use of electric vehicles. This will be Obama's fourth time speaking on the College Park campus. He last spoke in the Comcast Center in September 2009 on healthcare reform.
