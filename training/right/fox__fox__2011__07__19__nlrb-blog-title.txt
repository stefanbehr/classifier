A huge, rat-shaped balloon sits menacingly in protest outside the National Labor Relations Board (NLRB) headquarters in Washington, DC. as the federal agency's board members hear arguments for and against proposals to change union-organizing election rules. 

In May, the NLRB ruled the inflatable rat-gesture, usually reserved for businesses deemed unfair to workers by labor unions, an appropriate tool in the arsenal of labor protestors. 

But, labor protestors aren't responsible for this particular rodent. Pro-business advocacy group Americans for Job Security (AJS) put it there. AJS said in a statement they did it to "shed light on the agency's recent decision allowing union bosses to employ intimidation tactics in labor disputes." 

Inside the NLRB headquarters, the issue of intimidation was at the center of a hearing over a government proposal to streamline union-organizing elections. Among other things, the NLRB has proposed cutting the number of days between when a union files to hold an organizing election and when ballots are cast. The current median is 38 days, but could be cut to as few as 10 days if the measure passes. 

Business groups argue the proposed measure would unfairly limit management's right to make a case against unionization. 

"Employees will be rushed into making a decision without the benefit of an opportunity to receive and digest information, contemplate the consequences of their ballot, and review and question information, " said Jay Krupin, labor and employment litigator with EpsteinBeckerGreen. "Your proposal transparently precludes sufficient time for employees to receive and consider information which dramatically effects their workplace and lives. It makes the election process for employers to be an away game." 

Employment attorney Harold Weinrich, a partner with Jackson Lewis LLP who has represented employers in a wide range of industries, told the board - dominated by appointees of President Obama - that they were overstepping their bounds legally. 

"The board's rule-making authority is strictly circumscribed," Weinrich said. "The board may only adopt rules to implement the will of Congress, not as a means to further their own agenda." 

Unions and pro-labor groups say shortening the amount of time between filing for an election and voting would give employers adequate time to make their arguments, and less time to intimidate employees and wage, what one union-backer called, "psychological warfare," in the days leading up to an election. 

"Currently, when employees ask for an election on whether to form a union they encounter significant obstacles in the form of needless beauracratic delays and costly taxpayer-funded litigation," said Kimberly Brown of the pro-labor American Rights at Work. "It can take months and even years before they cast a vote. Some never get to vote at all. Meanwhile, the process rewards unscrupulous employers who game the system by pursuing claims that are often irrelevant or found to be without merit in order to stall the election date. These tactics work." 

Elilzabeth Bunn of the AFL-CIO told the board, "The goal is not to inform. The goal is to harass, delay, confuse and intimidate." 

The NLRB's hearing took place amid the ongoing controversy over the agency's effort to block the expansion of Boeing in South Carolina because workers there voted to quit the machinists union. 

A vote by the NLRB on the proposed changes is still weeks away. A 60-day comment period ends on August 22. The board will then take two weeks to respond to comments and then adjust the proposed changes accordingly before voting.
