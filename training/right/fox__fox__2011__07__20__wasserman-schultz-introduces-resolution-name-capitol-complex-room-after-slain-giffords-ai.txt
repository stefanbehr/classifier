Fox News has learned that Democratic National Committee Chairwoman Rep. Debbie Wasserman Schultz, D-Fla., is introducing a resolution to name a room in the Capitol Visitor's Center after Gabe Zimmerman, the aide who was killed in the shooting of Rep. Gabrielle Giffords , D-Ariz., in January. 

Zimmerman was one of six people killed in the rampage. Two other Giffords aides were seriously wounded. 

Zimmerman worked in the district office of the Arizona Democrat, handling community outreach. 

It's rare for any rooms in the Capitol complex to be named after anyone. Most rooms are named after well known leaders. 

Some of the rooms include John F. Kennedy, Lyndon B Johnson, Senate Majority Leader Mike Mansfield, D-Mont., Senate Majority Leader Robert Byrd, D-W.Va., Senate Majority Leader Howard Baker, R-Tenn., Sen. Ted Kennedy , D-Mass., Reps. Henry Hyde, R-Ill., Sonny Montgomery, D-Ala., and Rep. Lindy Boggs, D-La. 

The resolution would have to be approved by the House of representatives. Generally speaking, the new GOP House majority has been loath to approve "honorary" bills since they gained control of the chamber this year. 

The room Wasserman Schultz wants to name for Zimmerman is HVC 215, meaning it is underground and is part of the Capitol Visitor's Center on the east front of the Capitol. 

It's a room Democrats frequently use for meetings and caucuses.
