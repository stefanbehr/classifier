Former Louisiana Gov. Buddy Roemer officially declared his candidacy for the White House Thursday, imploring the American people to stand with him despite the crowded GOP field. 

Before a small crowd in Hanover, New Hampshire, the 67-year old Roemer said he's "old enough to know what to do - but young enough to get it done." 

Roemer might not be a household name, but he's no stranger to Washington, having served in Congress for seven years in the 80's as a Democrat. Roemer switched his party affiliation in 1991, becoming a Republican during his first -- and only -- term as governor. That decision likely cost him his reelection bid. 

After a 16 year absence from politics, Roemer is back and staking his presidential bid on a pledge to limit campaign contributions to $100, saying special interests have a stranglehold on Washington. "I am no one," he said, "but I challenge the system." 

In further indication of the platform on which he intends to run centered on financial fairness, Roemer pointed to what he called unfair trade practices, noting that 'made in America' is a label that's disappeared. 

" China is having the greatest boom in history," he said, "and we're paying for it." 

Roemer went on to say the system needs to be reformed to save U.S manufacturing jobs and believes tax credit loopholes for businesses that ship jobs overseas need to be closed. 

Harvard educated, Roemer intends to spend lots of time in New Hampshire, even taking up temporary residence in the first-in-the-nation-primary state. His campaign is seen as an extreme long shot, even by Roemer himself who readily admits he faces an uphill battle against better known, better funded candidates.
