With just 12 days left for Congressional action to avert a debt default, nerves are clearly frayed. So much so, that Senate Majority Leader Harry Reid, D-Nev., took to the Senate floor Thursday morning to give his House colleagues a tongue-lashing for taking the weekend off. 

Speaking extemporaneously before taking up his prepared remarks, Reid lashed out, "I think this is a very bad picture for our country to have the House of Representatives out for this weekend when we have to likely wait for them to send us something, because I understand that negotiations taking place deal with revenues which constitutionally have to start in the House of Representatives. So I think it is just untoward, and that's the kindest word I can say, for the House of Representatives to be out this weekend. What a bad picture that shows the country." 

The House approved earlier this week legislation it considers to be the answer to the debt crisis, the "Cut, Cap, and Balance" bill that imposes strict caps on spending and calls for a constitutional amendment to balance the budget. 

And though Senate Republicans support the measure, it is not expected to pass the chamber when the members vote on Saturday morning, leaving lawmakers and the White House back at square one in terms of finding a compromise product to allow the president to increase the $14.3 trillion debt ceiling by $2.4 trillion before the August 2 Treasury Department-set deadline for default. 

Brad Dayspring, spokesman for House Majority Leader Eric Cantor , R-Va., wasted no time in responding to his boss' Senate counterpart. 



"Again, what is the Senate doing today?", Dayspring asked, "What I thought I heard you say was sitting around for two days deciding whether to vote on the work that the House has already done." 

And the verbal ping-pong did not stop there. 

Reid spokesman Adam Jentleson volleyed back, "House Republicans would rather take a few days off than make sure that seniors will get their Social Security checks on August 3rd. We are less than two weeks away from a massive economic crisis but House Republicans can't be bothered to stick around and work towards the bipartisan compromise that the American people are looking for. As Senator Reid has made clear, the Senate will be in session every single day until we reach a deal that averts this fast-approaching crisis." 

And so it goes, as the ideological divide seems to grow ever wider, despite a bipartisan plan emerging in the Senate that appeared to have traction in that chamber, and despite the fact that a default is less than two weeks away. 

Judging by past instances where Congress is deeply divided over legislation, it will take nearly every one of those 12 days left to craft a compromise that can be analyzed by the nonpartisan Congressional Budget Office, which determines a bill's overall price tag, and then make it through both chambers.
