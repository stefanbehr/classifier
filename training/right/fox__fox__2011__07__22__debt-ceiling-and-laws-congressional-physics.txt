Laws of science can simply not be broken. 

There's Kepler's Three Laws of Planetary Motion. 

There's Newton's Laws of Motion which govern force, mass and inertia. 

And then there are scientific dictates like Heisenberg's Uncertainty Principle. 

Congress makes laws. But it's governed by laws, too. 

And there are laws of legislative and parliamentary physics which simply can't be broken. 

This is why securing a "deal" to avert a federal default is so vexing. 

The laws of Congressional physics are more elementary than Heisenberg's Principle or Newton's Laws. But they carry as much weight. Certain political applications cannot go into motion without adhering to these statutes. 

It's easy: you need 218 votes to approve something in the House and 51 in the Senate. And in the Senate, there's a crucial "cloture" axiom. It dictates that 60 votes are required to halt a filibuster (invoking cloture, as it's called in Capitol Hill-speak) and thus proceed to a vote that needs the majority of 51. 

The House can pass the so-called "Cut, Cap and Balance" proposal to trim spending and impose a Constitutional Balanced Budget Amendment in exchange for hiking the debt limit. But that bill won't pass in the Senate. The Senate could try to approve the so-called "McConnell" plan, named after Senate Minority Leader Mitch McConnell (R-KY). This would permit incremental debt ceiling increases but allow Congress to "disapprove" of these hikes. No one is sure that would make it through the Senate, let alone the House. 



Then there's the ambiguous "Gang of Six" phenomenon, aimed at slashing $3 trillion over a decade. 

When it comes to the quantum mechanics of the debt limit, the Gang of Six premise is the equivalent of "string theory," a relatively new field that's not quite understood. So few can handicap the fate of the Gang of Six. 

After weeks of negotiations to stave off a debt crisis, only one theorem still rings true. It's known as Cantor's Postulate, named after House Majority Leader and legislative physicist Eric Cantor (R-VA): "Nothing can get through the House right now. Nothing." 

The exception was the Cut, Cap and Balance plan. But even that bill didn't adhere to the laws of Congressional physics. Lacking the votes for Cut, Cap and Balance in the other body, Senate Majority Leader Harry Reid (D-NV) plans to euthanize that legislation with a procedural vote this morning. 

Once the Senate's jettisoned Cut, Cap and Balance, the hope is that Congressional leaders and President Obama can find common ground on another idea which can adhere to the laws of Congressional physics: 218 votes in the House, 51 in the Senate, contingent on the 60 vote predicate to eliminate a filibuster. 

For all of Thursday, rumors of an alleged "deal" between the president and the GOP brass were hotter than the temperatures baking the Capitol Dome in a heat wave not seen in Washington for 15 years. Only, there was no deal. A senior GOP aide signaled that no one would discuss a "deal" until Cut, Cap and Balance was finally off the table. 

On Tuesday night, conservatives basked muscling the Cut, Cap and Balance plan through the House. And they were in no mind to accept anything less. 

"This is the plan. This is the compromise," boasted Rep. Jim Jordan (R-OH), the head of the Republican Study Committee which represents nearly 180 of the most conservative House members. 

At his Thursday briefing with reporters, House Speaker John Boehner (R-OH) conceded that many rank-and-file Republicans sided with Jordan and were unwilling to bargain. Still, Boehner saw daylight. 

"We've got some members who believe that (Cut, Cap and Balance is the compromise)," said Boehner. "But I don't believe that's anywhere close to the majority." 

Which is why Boehner spoke of another path forward. 

"It would be irresponsible on behalf of the Congress and the president not to be looking at back-up strategies for how to solve this problem," said Boehner. "It's not enough to wish or wait for a solution to materialize." 

The House Republican leadership has said repeatedly that the federal government cannot default in less than two weeks. Sure, many tea party loyalists question why the U.S. should up the debt ceiling when the books are already awash in red ink. But key Republicans are delivering a very sobering message to even the staunchest conservative about what would happen if they failed to increase the debt limit. A default could cost the federal government an additional $1 trillion. That's hard to stomach when your goal is to slash the government's obligations. And that says nothing of the country's economic consequences. 

"If the credit rating is downgraded, every interest rate in America will go up," said Boehner. 

Freshman Rep. Nan Hayworth (R-NY) convened a session with lawmakers and economists from Standard Poor's late Thursday afternoon to explain how a default could upend the economy. One of the briefers warned that the economy was teetering like a game of Jenga and could collapse if someone withdrew the wrong block. 

One source inside the room said they declared that the U.S. was so late to grapple with the looming crisis that it could anticipate a credit downgrade anyway. 

It's discussions like these which could help Boehner and others craft a final agreement that aligns with the physics of 218. One lawmaker who asked not to be identified said the speaker needed to remind his troops what they were fighting for. In other words, letting the country default failed to achieve their objective of a strong economy and a svelte government. 

House Republicans executed a unique tactic by canceling a proposed weekend session. This brought a torrent of criticism from Harry Reid who intends to have senators toil through the weekend on the debt ceiling. But several GOP lawmakers believed that no weekend session could help the effort in the House. 

"It's like letting steam escape," said one Republican lawmaker. "It sends home the firebrands and gets them away from each other. It lets them cool off." 

Another GOP Congressman agreed, saying that holding lawmakers in Washington would just make them stew and harden their positions against the debt ceiling. But getting them home means they would listen to their constituents who are growing more vocal about a possible default. 

"The most dangerous place for me to go after church is to the grocery store," said one Republican lawmaker. "It takes me an extra hour to get out of there." 

The thought is that constituents might buttonhole lawmakers in the grocery store, perhaps back by the cans of peas. And even though Democrats and Republicans don't want to increase the debt ceiling, constituents might be able to apply just enough pressure to produce 218 yea votes. 

Which, if the planets align, could coincide with 51 votes in the Senate. 

Until then, Congressional leaders won't speak of any "deal" until they're confident a potential plan conforms with the laws of Congressional physics. 

Which is why the rumor mill will churn. 

Word came late Thursday that negotiators were working on a series of "triggers" which could impose pain on each side if the government fails to meet key targets for reforming taxes and entitlements. 

In other words, Democrats want to increase tax rates on individuals earning more than $250,000 if a certain contingency isn't met. 

But there must be an equilibrium of pain in this potential agreement. 

In turn, the GOP trigger would scale back key portions of the Democrats' coveted health care law, including the mandate that everyone purchase insurance. 

It's hoped these triggers would incentivize the sides to make good on concrete cuts or sacrifice of their sacred cows. 

Scientists might call mutually-assured entropy a "balanced equation." 

And if the president and Congressional leaders can concoct one of those, they might just be able to comply with the laws of Capitol Hill physics and avert a default.
