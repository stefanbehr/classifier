Republicans are attempting to use President Obama's health care law as leverage to reach a deal in the on-going debt talks, says a senior source familiar with negotiations. 

The source says President Obama and House Speaker John Boehner , R-Ohio, have discussed this as part of a potential "trigger" provision that would mandate comprehensive tax and entitlement reforms by a specific date. 

The goal would be to force both parties to complete a deal or have provisions neither side wants to take effect. 

Republicans want to eliminate or scale back portions of the health care bill, such as eliminating the individual mandate, the source says. Democrats have asked for an end to the Bush tax cuts that were extended last December. 

This means if a deal isn't completed by a set date, then parts of the health care law would be repealed and the current tax rates for individuals and small businesses making more than $250,000 would be allowed to expire at the end of 2012. 

A Republican official familiar with the talks says the "trigger" provision needs to be designed very carefully to ensure it does not kick in. 

The Bush tax cuts and Obama health care law repeal would not be included in any actual deal, only in the "trigger" provision. 

Fox News' Chad Pergram and FBN's Rich Edson contributed to this report.
