Good Washington press flacks know the art of timing the release of bad news to generate the least attention possible: a Friday afternoon, the evening before a holiday, etc. 

The practice, known as a " turkey drop" or "taking out the trash," is a Washington tradition as old as any, but the weeks-long battle over President Obama's request to increase the federal government's $14.3 trillion debt ceiling has provided more cover for awkward announcements than even the most calculating spokesperson could hope for. 

On today's "Power Play w/ Chris Stirewalt," panelists offer their nominations - including Rep. David Wu's sex scandal, the damning report on the ATF's "Fast and Furious" gun buying scheme and the admission of a stalemate in the Libyan civil war -for which big pieces of bad news have benefitted the most from a distracted Congress and press corps. Plus, viewers have their say on the top turkey dropper. 



Watch the latest video at FoxNews.com 





Chris and his rotating band of panelists share their perspectives on Washington and all-things political every weekday at 11:30a ET at live.foxnews.com . Make sure to check out his column first at foxnews.com/politics and jump right into the live chat.
