A spokeswoman for the House Administration Committee tells Fox News that they have not posted a "system alert" as they did yesterday when a deluge of phone calls flooded the Capitol switchboard, mostly over the debt ceiling debate. 

Monday night President Obama suggested people call their Congressional representatives to tell them to compromise on a debt and deficit deal. 

But as of 10 a.m. EDT Wednesday, call volumes had returned to normal levels. 

The normal level is about 20,000 calls to the House side of the Capitol per hour but Tuesday it spiked as high as 35,000 calls per hour.
