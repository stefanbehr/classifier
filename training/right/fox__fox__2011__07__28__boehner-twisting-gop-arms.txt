When he was House speaker, legendary Texas Democrat Sam Rayburn had a room on the first floor of the Capitol called the "Board of Education" where he would take reluctant members and persuade them to vote for bills. 

It's unknown if room H-217 just off the House floor serves the same purpose for current Speaker John Boehner . It's Boehner's ceremonial office, the same room Boehner spoke from Monday night when he responded to the president on national TV. 

During a vote sequence Thursday afternoon, Boehner's staff escorted several lawmakers inside for a little chat. 

One was Rep. Tom McClintock, R-Calif., who emerged saying he wouldn't tell us how he would vote. 

Also escorted back to the room by Boehner staff was Tennessee Rep. Chuck Fleischmann. 

Fleischmann emerged looking very stressed. 

Boehner then walked back to the floor, found Rep. Bill Posey, R-Fla., and walked him back to the ceremonial office. 

No word if this room will now be known as the "Get your ass in line" room.
