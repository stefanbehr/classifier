Steve Jobs has more cash than Uncle Sam. 

That's what became apparent Thursday when the Treasury Department announced the U.S. had almost $74 billion in cash reserves, about two billion less than the nearly $76 billion Apple reported it had earlier this month in its most-recent quarterly earnings report. 

Apple said its cash reserves grew $11 billion during the third-quarter, while the government's reserves continued to fall. 

Apple, one of the world's largest technology companies, takes in more money than it spends, something the federal government does not do -- a source of conflict on Capitol Hill throughout the on-going debt crisis. 

The government's number shows how much cash it has available until reaching the debt ceiling ahead of the Aug. 2 deadline determined by the Treasury. 

Apple stock currently sells for more than $390 a share.
