The clock is ticking on a debt deal, House Minority Leader Nancy Pelosi ,D-Calif., said Saturday night as she left the Capitol with her condfidante, Rep. George Miller, D-Calif. 

When asked if she thought she could get a deal tomorrow, she answered, "I would certainly hope so. The clock, she is a-ticking." 

As to why Senate Majority Leader Harry Reid , D-Nev., seemed so pessimistic after the White House meeting, Pelosi said that as we get closer to the time, we have to get closer to a solution. Sometimes when you eliminate possibilities, you make progress. 

Although she didn't say it, this suggests that they lopped off a couple of potential routes that Reid wanted to take at the White House meeting this afternoon. 

"I'm already unhappy with $1.2 trillion in cuts," she said. 

Pelosi then told me I should go home.
