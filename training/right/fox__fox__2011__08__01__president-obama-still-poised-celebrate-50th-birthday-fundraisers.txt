President Obama is still poised to celebrate his 50th birthday this week with fundraisers in Chicago after his attendance was in jeopardy with high stakes debt negotiations going on in Washington. 

Congress still has to vote on the bill and send it to the president's desk, but Sunday's agreement on raising the debt ceiling increased the likelihood that he would be able to attend. 

Officials have not yet updated his status this week, but as of last Friday, the birthday bash was still on his official schedule. He's slated to travel Wednesday afternoon and return that night for the events. His actual birthday is Thursday, August 4. 

One thing is certain, it seems he already got his birthday present. 

"What I really want right now is to, to get a debt-ceiling deal for my birthday," the president said in an NPR interview recently. He also noted he's feeling good about turning 50, but might be have a little more grey hair. 

Previously, White House Press Secretary Jay Carney left his schedule open for adjusting or canceling events as the debt ceiling debate was still going on, and they had already cleared his schedule of other DNC fundraisers not related to his birthday in recent weeks. 

The bash is expected to raise big bucks for the president's re-election and the Democratic National Committee (DNC) and will kick-off with a concert that will reportedly include Jennifer Hudson and Herbie Hancock . 

Then there's a dinner at the Aragon ballroom where attendees will pay as much as the legal allowable limit of $35,800 to attend. 

The concert tickets will begin at $50 and go up to $10,000. Normally, the lower-dollar events start at $44 dollars (President Obama is the 44th president), but in honor of Obama's 50th, they have bumped them up to 50 bucks. 

The Obama campaign is also holding a series of organizing events across the country on August 3 to try to build support. They're encouraging supporters to find 50 new people into the campaign and calling it "50 for 50." 

Obama has said that he looks forward to getting the perks of his AARP card, which includes discounts. AARP tells Fox News that the organization don't plan a formal presentation of the president's card. 

It wasn't immediately clear how he would celebrate, if at all, in Washington on Thursday. In the past, the President has kept his birthday plans low key. Last year President Obama enjoyed a birthday dinner in Chicago with White House advisor and friend Valerie Jarrett , his golf buddy Dr. Eric Whitaker and local Chicago businessman Marty Nesbitt. The year before President Obama spent his first birthday in office in Washington, with a weekend trip to Camp David. 

Fox News' Kelly Chernenkoff contributed to this report.
