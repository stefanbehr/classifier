The home page for the Republican National Committee's website, GOP.com, implores people to "change direction" by contributing money to help regain majorities in Congress and elect a Republican president in 2012. But that it also includes a picture noting President Obama's last day in office as January 20, 2017. 

That date would mark the end of a second Obama term if he was to win re-election in 2012, rather than January 20, 2013, his last day as president if he isn't re-elected. 

An RNC spokeswoman tells Fox News the picture corresponds with a video on the same page showing what could happen if Obama was president for four more years and that the committee isn't conceding the 2012 election. And the photo does appear to be a frame from the video that the GOP says foretells what another Obama term would be like. 

"The RNC is clearly not giving President Obama a free pass in 2012 - quite the opposite actually, we are aggressively showing voters what our country would look like after another four years of President Obama and his tax and spend policies that have done nothing to create jobs and leave us vulnerable to governments like China ," said an RNC spokesperson in a statement. 

Fox News Producer Megan Dumpe Kenworthy contributed to this story.
