The U.S. economy continued to show signs of weakness Thursday morning as the Dow plunged by more than 300 points. Fox Business Network's Charlie Gasparino said on Thursday's Power Play w/ Chris Stirewalt that while there are multiple reasons for the continued economic woes, Washington's inability to make timely economic decisions is weighing the economy down. 

"It used to be that Wall Street and the Fed were the major players in economic policy, now it's clearly Washington," Gasparino said. "They're a player and can't agree on a solution and that's kind of a scary situation." 

You can catch Power Play Live at http://live.foxnews.com each weekday at 11:30 a.m. EDT. 



Watch the latest video at FoxNews.com
