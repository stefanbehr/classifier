As the Obama administration prepares to withdraw the remaining US troops from Iraq by the end of this year, the White House projects over one million service members will be returning to civilian life and entering the workforce between 2011 and 2016. The White House wants to be sure these troops are given the opportunities for advancement and prepared for the challenges ahead. 

President Obama will announce several steps to prepare veterans for their return Friday, including a proposal for career readiness training before they retire from the military--a sort of reverse boot camp. "The present structure of the military really requires intensive training on the front end, weeks of boot camp," one administration official said Thursday. "But one of the things we're finding is that there's not as much effort on the back end as those service members are separating and preparing to enter the work force." 

Obama is calling on the Department of Defense and the Department of Veterans' Affairs to develop a proposal for post-military career readiness. The task force would create an extended transition period, including counseling for outgoing service members and providing veterans with more information on the benefits available to them through the GI Bill. The Labor Department is expected to play a role in this plan to transition service members to the civilian work force. 

More than one million veterans are currently out of work in the United States, and the unemployment rate for veterans in the post-9/11 era was 13.3 percent in June. The plans Obama intends to unveil at the Washington Navy Yard Friday also include incentives to hire these out-of-work veterans. 

"Among the things the president will be announcing is a returning heroes tax credit and wounded worriers tax credit," an administration official said. 

These tax credits serve as incentives for companies to hire unemployed veterans and service men and women who suffered disabling injuries as a result of their service. The "hero program" would provide a $2,400 tax credit to companies hiring unemployed veterans currently on short term unemployment, or $4,800 for soldiers out of work six months or longer. 

The incentives are higher for companies hiring disabled veterans. The Wounded Warriors Tax Credit awards $4,800 for short-term unemployed injured hires and $9,600 for every long-term out of work vet. 

The White House estimates the tax credits will cost $120 million over two years. "We hope it will cost more," one administration official said, suggesting emphatically that the higher the payouts, the better the employment situation for veterans will be. 

The president is challenging the private sector to hire or train 100,000 unemployed veterans by the end of 2013. The more companies that sign on, the more the tax credits will cost. "We look at that as a positive thing if more companies are taking advantage of this by answering the call for those who have already answered the call [of duty]." 

But how will all this be paid for when Congress nearly failed to raise the national debt ceiling and avoid default? Where does the money come from? 

The White House expects to consult with members of Congress when they return to Washington after the August recess. Committees in the House and Senate that focus on veterans issues as well as budget committees will be asked to set a proposal in motion. One official said the administration intends to work with Congress to offset the costs, ensuring the tax credits will be revenue-neutral. 

Regardless of whether the money is in place, President Obama will charge his new Secretary of Defense, Leon Panetta , to report at the end of the 2011 calendar year with the first military transition proposal.
