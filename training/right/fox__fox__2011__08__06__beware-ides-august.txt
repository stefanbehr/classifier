Beware the Ides of March, warned Shakespeare in Julius Caesar. 

In Washington, beware the Ides of August. 

The House and Senate usually abandon the Capitol for most of the month. But that doesn't matter. Year after year, August is the most-volatile month on the Congressional calendar. 

For example: 

Last year, the House returned just days after lawmakers left town so they could pass a big state assistance bill. Then, in an extraordinary move during the same session, Rep. Charlie Rangel (D-NY) seized the floor to defend himself against ethics charges. In his impassioned defense, Rangel dared his colleagues to kick him out. 

On that same August day, Washington learned that legendary former Sen. Ted Stevens (R-AK) perished in an overnight plane crash in Alaska. A federal court had just vacated Stevens' conviction for improperly accepting gifts. Stevens survived another plane crash decades ago that killed his first wife. 

Two years ago, the tea party blossomed as August town hall meetings devolved into utter mayhem over health care reform. 

Three years ago, Republicans commandeered the House floor daily during August, even though the House was out of session, to hold their own rump sessions about skyrocketing gas prices. 

So is it any surprise that August 1 of this year would bring one of the most remarkable scenes to unfold on the House floor in years? 

Everyone on Capitol Hill was dreading this August in particular. The Treasury Department set August 2 as the deadline for Congress to increase the nation's debt ceiling or risk a potential default and credit rating downgrade. Congressional leaders scrambled for months to cobble together a package that could pass both the House and Senate. This process prompted exhaustive negotiating sessions, white-hot rhetoric, walk-outs of meetings and volleys of barbs. Much of this played out over a series of weekends or after business hours. Administration officials, lawmakers, aides and reporters alike endured late-night background briefings, witching hour stakeouts, foot chases through the Capitol and impromptu, prime-time televised addresses by both President Obama and House Speaker John Boehner (R-OH). 



It all culminated on the evening of August 1 with the House of Representatives voting on a crucial package to prevent a government default. 

The vote wasn't as dramatic as it could have been. Few expected it would pass by a razor-thin margin, much like health care reform in 2010. They weren't voting at the 11th hour to avert a government shutdown the next morning. But everyone was focused on the House chamber as lawmakers made the first step toward sidestepping a default. 

It was high-stakes poker. The main event. In the center ring. All the networks went live from the House floor. 

After all, it was August 1. And it surprised no one on Capitol Hill that August, 2011 would start with such commotion. 

But Beware the Ides of August. 

This story was about to get turned on its ear. 

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 

"She's following the debt limit debate, just like everyone else. She watches the news." 

Those were the mid-July words of Pia Carusone, Chief of Staff to Rep. Gabrielle Giffords (D-AZ). The House was about to approve the annual bill that funds Congressional operations. And tucked into that legislation was a provision to name a meeting room in the Capitol Visitor's Center after Gabe Zimmerman, Giffords' aide gunned down during the January bloodbath that wounded the Congresswoman. 

Zimmerman is the only Congressional staffer ever killed in the line of duty. 

Rep. Debbie Wasserman Schultz (D-FL) and others convened a press conference with Zimmerman's family to announce their plans to name the room. And at the end of the meeting, reporters huddled around Carusone to pepper her with questions about Giffords' condition. 

Carusone indicated that Giffords improved every week. Someone asked if Giffords had any plans to return to Washington any time soon. 

"It's a little hot here right now," Carusone said of Washington's infernal July, the hottest on record. "Maybe in the fall." 

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 

The mercury kissed the century mark on Monday, a new record for August 1 in Washington, DC. And as soon as the heat dissipated that evening, the House dove into a vote sequence that summoned all members to the floor. 

Three roll call tallies were scheduled in that vote series. The first vote was to establish whether there was a quorum. The second vote was on the debt ceiling package. The third vote was on an extraneous bill. 

Reporters rubbered into the chamber, scanning the tally board to see who voted yes and who voted no. They scribbled on notebooks, dissecting the vote, discerning which Democrats voted for and against. They checked to see how many tea party lawmakers opposed the bill. 

I floated between the chamber and the daily press gallery directly above the House floor, documenting the vote and scanning the monitors to see how much time remained. 

And then came a tip via email, followed quickly by a Tweet and two urgent phone calls. 

Giffords was back and we should expect her in the chamber shortly. 

I glanced at my reporting colleagues. No one was running around. No one showed signs of panic. 

That's because no one knew. 

Yet. 

I knew pandemonium would erupt the moment Giffords surfaced in the House chamber. Everyone was so focused on the vote, it was obvious no one knew Giffords was coming. 

That would soon change. 

Beware the Ides of August. 

Giffords entered the House chamber through a door on the Democratic side. Word spread throughout the room that she had appeared, prompting a climactic standing ovation from her colleagues and thunderous applause. 

And then the fire drill hit upstairs. 

Reporters went ape. Some dashed to the chamber to catch a glimpse of Giffords triumphant return. Some rushed out of the chamber to start hammering away on their laptops. Others loped downstairs to the second floor in hopes of seeing Giffords when she departed. 

No one quite knew which was the best place to be, resulting in a major collision between several scribes. 

"Giffords! Giffords! Giffords is here," shouted one reporter like a modern day Paul Revere. 

Beware the Ides of August. 

And then Vice President Biden arrived. 

Biden had lurked around Capitol Hill all day. He spent a good chunk of the morning on the Senate side, chatting with Democratic senators. Then Biden was ensconced for close to three hours with House Democrats in the very room that could soon be named for Gabe Zimmerman. Many liberal Democrats felt Mr. Obama betrayed them by agreeing to the debt limit package. So the vice president needed to allay fears about the bill and get an earful from liberals. 

Biden wasn't about to let an appearance by Giffords pass without seeing her on the House floor. 

As President of the Senate, Biden makes routine appearances in the Senate chamber, sometimes presiding over close votes. As a senator, President Obama orchestrated a famous "victory lap" in the House chamber during a vote sequence the day after he all but sewed-up the nomination mathematically in 2008. 

But it's rare to have a sitting president or vice president ever come to the House chamber at all, except for State the Union in January. 

Now, everyone wanted to grab Biden and hear what he said to Giffords. 

"I told her she's now a member of the cracked head club like me with two craniotomies," said Biden as a pack of reporters swelled in a Capitol corridor. 

The debt ceiling vote was billed as the most-critical vote Congress would take in decades. But it was nearly an afterthought after it was trumped by Giffords and trumped by the vice president. 

Rarely has one story so momentous pivoted so quickly, eclipsed by something more dramatic. 

"Did Giffords vote?" someone asked. 

She did. 

For 678 roll call votes, the letters Y, N or P remained blank next to Giffords' name on the tally board above the dais in the House chamber. In fact, many noted that the House only needed 216 yeas to pass the debt bill rather than the traditional 218. There's a mathematical reason behind that. During that vote, there two vacancies in the House. One seat is newly open, held by former Rep. Anthony Weiner (D-NY). The other seat was once held by now-Sen. Dean Heller (R-NV). But everyone anticipated at least two absences. Rep. Maurice Hinchey (D-NY) is out due to health reasons. And no one expected Giffords to return any time soon. So 216 was the key number, not 218. 

Beware the Ides of August. 

At 7:06 pm Monday, the a green "Y" flashed on the board next to Giffords' name. 

"It was above and beyond the call of duty," said House Minority Leader Nancy Pelosi (D-CA) to a throng of reporters massing in a hallway just outside the chamber. "She's a heroine. An inspiration." 

It is still just early August. So beware. A lot more can unfold under the Capitol Dome between now and September. 

The Senate approved the debt ceiling hike early Tuesday afternoon and President Obama quickly signed it into law. A few hours later, Moody's affirmed the AAA credit rating of the federal government, but cautioned it still had Washington on a "negative" watch for default. 

But Congress was so past all of that Tuesday afternoon. Sens. Barbara Boxer (D-CA) and Ben Cardin (D-MD) took to the floor to try to force a short-term extension of money for the Federal Aviation Administration (FAA). Sen. Tom Coburn (R-OK) immediately blocked that. Then Coburn tried to move a bill that the House already approved to keep the FAA operating. In turn, Boxer blocked Coburn. 

By nightfall, it was clear no one could broach the impasse. Press releases and barbs flew about the FAA. Democrats blamed Republicans and the tea party. Republicans blamed Democrats, their allegiance to labor unions and "pork." 

The issue is resolved now. But as soon as the debt ceiling bill was off the Senate floor, the headline flipped again, like someone flicking a switch. 

Beware the Ides of August. 

On Friday night, S P announced that despite the efforts of lawmakers to avoid a default, it was still downgrading the federal government's credit rating to AA+, a notch below AAA. 

"Clearly what we have done so far is not enough," said Sen. Richard Burr (R-NC) in a statement. 

Rep. Jack Kingston (R-GA) called on Congressional leaders to reconvene the House and Senate to solve the crisis. 

And by Saturday morning, the story changed again. 

31 U.S. service members were killed when the Taliban blew a NATO chopper out of the sky with a rocket-propelled grenade. 

In nearly a decade of fighting, it was the deadliest day in the war in Afghanistan. 

Beware the Ides of August. 

- Fox's Trish Turner contributed to this report.
