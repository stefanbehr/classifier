While the top Republican presidential candidates are in Iowa gearing up for the Ames Straw Poll, Gary Johnson (R- N.M.) says you can count him out... He'll be in New Hampshire. 

Johnson's campaign released a statement Friday night saying "We certainly don't begrudge the Iowa GOP for sponsoring what has become a very successful fundraising activity, but we simply cannot and will not buy into an event that has been granted far more status in the nomination process than it should have." 

Johnson's campaign calls the straw poll a "pay-to-play" event, saying "Asking supporters in Iowa to buy tickets and show up for an event six months before the caucuses is just not a part of providing that voice - or our campaign." 

The Former New Mexico governor says he is not completely abandoning Iowa. He participated in some events there last week. 

And while most candidates are in the first in the nation caucus state, Johnson's campaign says he will be in the first in the nation primary state next week. 

Johnson's low poll numbers make him ineligible for next week's Fox News debate in Iowa. He wasn't allowed to participate in CNN's New Hampshire debate either. The latest polls show him with less than 1 percent of voters.
