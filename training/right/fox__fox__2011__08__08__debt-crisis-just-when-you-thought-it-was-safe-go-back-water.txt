In 1975, the movie "Jaws" terrorized an entire generation of beachgoers with the legacy of a gigantic, man-eating shark. 

A few years passed. The horror subsided. And by 1978, no one had the same trepidation about going for a swim. 

Until "Jaws 2" debuted. 

The movie spurred one of the most-renowned catchphrases in cinematic history: 

"Just when you thought it was safe to go back in the water..." 

++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 

It's the summer of '78 again on Washington's silver screen. The Obama Administration and Congress just forged a deal to trim spending and raise the debt ceiling. All eyes are on the formation of a joint committee designed to target even deeper cuts this fall. And everyone is back frolicking on the beaches and splashing in the surf, thinking it's safe enough to go back in the water. 

Until the killer shark returned. 

You could almost hear the chilling Jaws theme by John Williams playing in the background. 

The shark returned to the lagoon off Washington's coastline in the form of a credit downgrade from Standard Poor's. S P knocked down the U.S.'s credit worthiness from the stellar category of AAA to AA+, and in doing so, took the Dow down well over 200 points in early Monday trading . 

In its report, S P indicated that "difficulties in bridging the gulf between political parties" was a key reason it stripped the feds of a AAA rating for the first time since 1917. 

In essence, S P didn't think the deal was enough "to stabilize the government's medium-term debt dynamics." 



S P cast doubts on the ability of a special, joint committee to concoct a viable plan to slash more spending and successfully move it through Congress. 

It's been a tumultuous five years in Washington. The electorate has careened back and forth between the parties for three consecutive election cycles. The unprecedented result of three straight wave elections is a firm rebuke of the way things are going. But S P's decision to downgrade the U.S. has the potential to become the most-powerful condemnation of Washington yet. 

Invective and blame spewed from press releases and the Sunday shows as to who should face blame for the default. Democrats argued it was the tea party for making House Speaker John Boehner , R-Ohio, back away from a "grand bargain" that would have produced deeper cuts and significant entitlement reform. Republicans cast aspersions on Democrats for not stomaching more cuts, failing to support a balanced budget amendment and insisting on tax increases to help decrease the debt. 

Either way, it's not safe to go back in the water right now. The S P downgrade was a body blow to the United States. The stock markets are convulsing. And now lawmakers from both parties are home, jittery that they didn't do enough. They fret that voters will plaster them with haymakers all month long over who will serve on this joint committee and where the cuts will come from. 

Lawmakers have questions, too. Representatives from S P briefed rank-and-file Republicans in mid-July about the consequences of a default, indicating that an increase in the debt ceiling may still not be enough. 

"They were agnostic about how we get there. Clearly we did not hit that $4 trillion mark," said one lawmaker familiar with the conversation. 

So, with an energized tea party and even rattled Democrats, this could prompt a demand to do more right now. 

"So they can take more tough votes on Medicare and Medicaid?" wondered one senior aide about the chances of lawmakers coming back in August. 

This is uncharted territory. And how and whether Congress will be forced to respond is contingent upon two things: the mood of their constituents and the volatility of the markets. 

For starters, there could be a fight lurking over the spending agreed to in this deal compared to the general spending outline authored earlier this year by Budget Committee Chairman Paul Ryan ,R-Wis. House conservatives clamored to adopt Ryan's non-binding blueprint, setting spending at $1.019 trillion for Fiscal Year '12 which starts October 1. Meantime, the agreement in the debt ceiling package sets spending at $1.043 trillion for FY '12, or $24 billion above the Ryan plan. 

In addition, the House has already okayed some of its spending bills for FY '12, working off of Ryan's lower baseline. So this requires conservatives and tea party loyalists to swallow hard, knowing that the cuts aren't as deep as the bills they've already voted for. 

The Senate hasn't approved any of the annual spending bills that run the federal government for the next fiscal year.This is where there's potential for mischief. 

In the spring, some tea party-backed Republicans balked when they discovered that the agreement that averted a government shutdown saved a pittance. One accounting method indicated that the legislation only saved $352 million. 

That's why S P's downgrade could provoke many conservatives to request more cuts before October 1. 

There are already plans for the House and Senate to approve a stopgap bill (known as a Continuing Resolution or "CR" in Congressional vernacular) to avert a government shutdown at the end of September. But if conservatives already feel they didn't get what they wanted with the CR in the spring, are stung by the package approved last week and are now witnessing a credit downgrade, they could push for more extreme cuts immediately. 

"We're just getting started," said one tea party affiliated freshman Republican who asked not to be identified. 

But there's an operational problem in this. Conservatives in the House can push. But they will likely hit a brick wall in the Senate. 

"Tea partiers can think whatever they want," said a senior House GOP aide. "But no deal with the Senate will get down at a lower number than what was agreed to already." 

Aside from that, there are serious questions as to who the big four Congressional leaders will appoint to the joint committee. Boehner, Senate Majority Leader Harry Reid, D-Nev, House Minority Leader Nancy Pelosi, D-Calif., and Senate Minority Leader Mitch McConnell, R-Ky., need to each select three lawmakers each to the panel by August 16. 

Look for leaders to craft a roster similar to the one they used for the ad-hoc debt negotiations led by Vice President Biden. 

But some now view an assignment to this panel as radioactive. 

"The question is who wants to serve on this God-damned thing," said Jim Manley, who served for years as a senior aide to Reid and the late-Sen. Ted Kennedy , D-Mass. 

Many automatically think that Paul Ryan would be a shoo-in. Ryan is second-to-none in understanding the intricate tangle of budget numbers. But once Democrats got a hold of Ryan's budget this spring, they made the Wisconsin Republican into a poster child for how the GOP wanted to "wreck" Medicare. Democrats vow a competitive race against Ryan next year. 

Other obvious choices on the House GOP front would be House Ways and Means Committee Chairman Dave Camp, R-Mich. Boehner also needs some buy-in from the tea party wing of his conference. Some expect him to tap House Republican Conference Chairman Jeb Hensarling, R-Texas, for the gig. Hensarling has rock-ribbed conservative credentials plus an extensive record serving on the Budget and Financial Services Committees. 

Diversity is also important on this panel. 

If Boehner wanted to go another direction, he could select a freshman like Rep. Kristi Noem, R-S.D., or Diane Black R-Tenn. Both sport significant service in state government and boast a solid grasp of fiscal issues. 

Then there's the defense question. If the committee fails to make substantial cuts, defense spending is on the chopping block as one of the "triggers" to inflict pain for not reaching an agreement. It's thought that Boehner could tap someone like House Armed Services Committee Chairman Buck McKeon, R-Calif., for the job. Other names mentioned to shield against possible defense cuts are Reps. Mac Thornberry, R-Texas and Randy Forbes, R-Va. 

Expect the weekend casualties in Afghanistan to galvanize those anxious to reject cuts in the defense sector. 

A wild card or two? Some have whispered the possibility of asking former Army Lt. Col. and freshman Rep. Allen West, R-Fla., to serve. Another possibility is Rep. Charles Boustany, R-La. Boehner asked Boustany to deliver the official GOP response to President Obama when he presented his health care reform pitch to a Joint Session of Congress in September, 2010. 

When it comes to the Pentagon, Democrats could counter with their own defense cadre as well. One possibility is Rep. Norm Dicks, D-Wash., the top Democrat on the House Appropriations panel and leading Democrat on the subcommittee that funds the military. 

Democrats could also look at Rep. Chris Van Hollen, D-Md., who can match Ryan, word-to-word and number-to-number on budget issues. Another possibility is Assistant Democratic Leader Jim Clyburn, D-S.C. who is a longtime appropriator and a veteran of the Biden talks. 

Other possibilities include Democratic Caucus Vice Chairman Xavier Becerra, D-Calif., and Rep. Jan Schakowsky, D-Ill. Both toiled last year on the Simpson-Bowles commission to cleave the debt. Pelosi could also give the nod to Rep. Allyson Schwartz, D-Pa., who serves on the Budget Committee. 

If Pelosi thinks Boehner could try to appoint one of his vaunted freshmen, she could counter with one of her own. A likely contender would be Rep. Karen Bass, D-Calif. Bass sits on the Budget Committee and is no stranger to big assignments, having served as Speaker of the California State Assembly. 

On the Senate side, Reid may face a challenge if he has to choose between Majority Whip Dick Durbin, D-Ill., and Sen. Chuck Schumer, D-N.Y. He is also likely to have pressure from the Gang of Six to include one of its members. Durbin fits the Gang of Six bill. But if he assigns Durbin, can he also appoint Senate Budget Committee Chairman Kent Conrad, D-N.D., or Sen. Mark Warner , D-Va., also a part of the Gang of Six. 

On the Republican side, the biggest challenge for Mitch McConnell could come from whether he feels he can appoint Sen. Tom Coburn, R-Okla., also a member of the Gang of Six. Earlier this year, Coburn introduced a budget that lopped off $9 trillion in spending over a decade. McConnell may also have to tap a tea party senator for the joint panel like freshman Sens. Marco Rubio, R-Fla., or Mike Lee, R-Utah. 

Regardless, people are watching who leaders appointment to this committee. And these appointments will now face even more scrutiny amid the S P downgrade. 

Yes, Congress passed a debt bill last week. But after weeks of negotiations, it still may not be enough to avert economic or political disaster. 

The S P downgrade spilled blood into the water. The sharks are now circling. 

At this rate, they're going to need a bigger boat.
