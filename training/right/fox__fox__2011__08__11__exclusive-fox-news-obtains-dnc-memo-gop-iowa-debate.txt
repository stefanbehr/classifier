Fox News has exclusively obtained a Democratic National Committee memo which paints the Republican field of presidential contenders as Tea Party flaks pushing tired, divisive policies which elevate the rich and predicts much of the same at tonight's Fox News co-hosted debate in Ames, Iowa. 

"As we expect to see tonight, the GOP candidates' extreme aims to appease the far-right wing of the Republican Party shows that Republicans are more concerned with protecting their special interest friends and the wealthy than protecting working families," the memo, penned by DNC National Press Secretary Melanie Roussell, reads. 

"The GOP would rather seniors and the middle class sacrifice and the wealthy pay even less in taxes. Unlike President Obama, their policies would eliminate, not create, opportunities for the middle class." 

Though Republicans are far from choosing their last man standing, the memo zeroes in on several top names. 

"[T]he actions or lack thereof of candidates like Mitt Romney , Tim Pawlenty, Michele Bachmann and others are clear - these Republican candidates for President are not interested in leading. They simply want to please the far-right, extreme Tea Party base of their party because that is what being the standard bearer of the Republican Party today requires," says Roussell. 

The memo is described as a state of play for media and "Democratic friends" which spells out the DNC's predictions for tonight's debate. 



In it, Roussell predicts the GOP responses on a series of issues: 

ON THE DEBT CEILING DEAL 

"Expect the overwhelming majority of the Republicans on the stage tonight to proudly state that they would not have supported the bipartisan debt agreement that prevented our country from defaulting on its debt for the first time in history." 

S P DOWNGRADE 

The Democrats put the S P downgrade at least partially in the laps of the Republican party, saying the party's obstinance to raising revenues was a determining factor. 

"At a moment when America should have been their top concern, they put politics, party and rigid ideology first. And the Republican presidential candidates backed them up - and the GOP field will double down on that flawed approach to leadership tonight." 

ON SENIORS 

"Both Mitt Romney and Tim Pawlenty endorsed a Cut, Cap and Balance proposal that would have cut Medicaid by one-third over 10 years and nearly half by 2050. According to the Kaiser Family Foundation, that alone could cause 36 million people to lose their Medicaid coverage, including individuals with disabilities and seniors in nursing homes." 

ON MITT ROMNEY 

" Mitt Romney has consistently ducked and dodged on the most pressing issues facing America including the debt ceiling debate. Not only did he wait until 12 hours before the final vote to actually say something-leading one reporter to say Romney was in a "Mittness Protection Program"-he endorsed the Cut, Cap and Balance plan that would have harmed our economic recovery in the short-term while simultaneously hurting our long-term economic competitiveness all while putting a greater burden on America's oldest citizens and our most vulnerable. Look for him to reassert his support for Cut, Cap and Balance tonight." 

ON MICHELE BACHMANN 

"To her credit Michele Bachmann did not actually vote for the Cut, Cap, and Balance - because she didn't think it was extreme enough! She told Fox News that America should pay off all our foreign creditors first, even if that means slashing Medicare and Social Security payments to America's seniors. We expect her to repeat similar sentiments tonight." 

ON HERMAN CAIN 

"Additionally, look for Herman Cain to tout more failed economic policies like his support for a 'fair tax' for everyone, which even the Wall Street Journal editorial board called 'the most radical reform imaginable' and one that would 'hit hard the young, middle-income families.'" 

ON JON HUNTSMAN 

Regarding Huntsman, President Obama's former Ambassador to China, the memo states, "Jon Huntsman, who proudly called himself a radical for wanting to end Medicare as we know it, endorsed the principles of Cut, Cap and Balance including a balanced budget amendment," Roussell says before saying why such an approach won't work. 

Watch the Republican presidential debate at 9 p.m. ET on Fox News and FoxNews.com LIVE from Iowa State University in Ames, co-hosted by the Iowa Republican Party and the Washington Examiner. 

READ THE FULL MEMO HERE: 





From: Melanie Roussell, DNC National Press Secretary 

To: Interested Parties 

Date: August 11, 2011 

Re: What to Expect from Tonight's Republican Debate 



________________________________________ 

With 179 days left before the Iowa Caucuses , the American people are increasingly turned off by what they are learning about the Republican presidential candidates and who they fight for. Instead of new ideas that give the middle class more security, we hear the same old policies that carve out special benefits for the special interests. Instead of a new approach that puts party aside and America first, we hear a rigid and extreme ideology that asks working and middle class families to carry the entire load. 

This is why the GOP is seeing its support erode significantly. In two recent polls the GOP is seeing some of the highest negative ratings ever-driven in large part by their willingness to let the extreme Tea Party dictate the Republican Party's agenda and take over the party, with the 2012 Republican candidates following their lead. 

In a CNN/ORC poll released Tuesday: "[F]avorable views of the Republican party dropped eight points over the past month, to 33 percent. Fifty-nine percent say they have an unfavorable view of the Republican party, an all-time high dating back to 1992 when the question was first asked." http://bit.ly/pp9t8F: "More Americans now think that members of Congress who support the Tea Party are having a negative effect than said that in January, at the start of the new Congress. Currently, 29% judge the impact of Tea Party supporters as mostly negative compared with 22% who see their impact as mostly positive. 

At the beginning of the year, the balance of opinion was just the opposite: 27% said that Tea Party members in Congress would have a positive impact, while 18% expected a negative effect." http://t.co/v7hZidL 

Despite these historic negative ratings, we should expect to hear more from this field of Republican presidential contenders who have not only embraced the same failed policies of the past that eroded middle class security and led our country to the brink of depression, but who have also embraced an extreme and rigid ideology that will hurt America's seniors and the middle class. 

In advance of tonight's debate, we wanted to highlight some key points we expect to hear from the Republican candidates as they continue to fight for the title of "Most Extreme," in their efforts to pander to the far-right Tea Party base of the Republican Party to seek the nomination. 

Failure of Leadership - Duck, Dodge and Dismantle 

Make no mistake the Republican presidential contenders have fully embraced what we call the Duck, Dodge and Dismantle approach to leadership because they are ducking their obligation to the Middle Class; dodging their obligation to our nation's seniors; and, they want to dismantle critical education and job creation programs while protecting tax breaks for the wealthy, big oil and corporate jet owners. They support the same unbalanced economic policies as Washington Republicans. Tonight you will hear GOP presidential contenders continue to tout their support for the Republican budget that would end Medicare as we know while giving NEW tax breaks to millionaires and billionaires and the special interests. While they ask average Americans to cut back and sacrifice, they ask nothing of those at the very top. 

Moreover, the actions or lack thereof of candidates like Mitt Romney , Tim Pawlenty, Michele Bachmann and others are clear - these Republican candidates for President are not interested in leading. They simply want to please the far-right, extreme Tea Party base of their party because that is what being the standard bearer of the Republican Party today requires. 

Pushing the Nation to the Brink 

Expect the overwhelming majority of the Republicans on the stage tonight to proudly state that they would not have supported the bipartisan debt agreement that prevented our country from defaulting on its debt for the first time in history. To these candidates, compromise is a dirty word and the prospect of America not paying its bills was a risk they were willing to take. 

The recent decision by S P to downgrade the United States debt, as flawed as it is, highlighted the need for the type of compromise and consensus that the candidates opposed. In fact, part of what the S P based its reasoning on was the fact that Republicans in Congress were unwilling to consider revenues. While Democratic leaders showed their commitment to standing with President Obama to support a grand bargain that asked everyone to compromise, Speaker Boehner and his Tea Party Caucus, during negotiations, walked away no less than three times. At a moment when America should have been their top concern, they put politics, party and rigid ideology first. And the Republican presidential candidates backed them up - and the GOP field will double down on that flawed approach to leadership tonight. 

Extreme Aims toward America's Seniors 

Over the course of their campaigns, each of the Republicans running for president has proven that their priority is not standing up for seniors or America's middle class; their priority is protecting the wealthy and the special interests. Look for them to say more of the same tonight. 

Both Mitt Romney and Tim Pawlenty endorsed a Cut, Cap and Balance proposal that would have cut Medicaid by one-third over 10 years and nearly half by 2050. According to the Kaiser Family Foundation, that alone could cause 36 million people to lose their Medicaid coverage, including individuals with disabilities and seniors in nursing homes. 

Jon Huntsman, who proudly called himself a radical for wanting to end Medicare as we know it, endorsed the principles of Cut, Cap and Balance including a balanced budget amendment. The amendment that the House will consider would put a cap on what the government pays out in Social Security and Medicare while allowing one-third of the members of Congress to block asking the wealthiest to pay their fair share. It would be easier to cut Social Security than it would to close corporate tax loopholes for private jets and Wall Street bankers. The amendment, coupled with the cuts from the House Republican budget, would change the rules for generations of Americans who paid into Social Security and Medicare week after week and paycheck after paycheck. Instead of getting what they paid for and earned, they would see cuts that could result in a $1,500 per year average reduction in benefits for the 70 million seniors on Social Security , and an average reduction of approximately $1,100 per year for those who receive Medicare. 

Extreme Aims toward the Middle Cla 

All of the Republicans have committed to the same extreme economic policies of the Republicans who held Washington hostage over the course of the last month. They all support protecting corporate subsidies and tax loopholes instead of protecting investments in critical programs that would create the jobs of the future, like education, research and development of clean energy. In addition, their cuts would cost middle class families jobs. 

For example, Mitt Romney has consistently ducked and dodged on the most pressing issues facing America including the debt ceiling debate. Not only did he wait until 12 hours before the final vote to actually say something-leading one reporter to say Romney was in a "Mittness Protection Program"-he endorsed the Cut, Cap and Balance plan that would have harmed our economic recovery in the short-term while simultaneously hurting our long-term economic competitiveness all while putting a greater burden on America's oldest citizens and our most vulnerable. Look for him to reassert his support for Cut, Cap and Balance tonight. 

To her credit Michele Bachmann did not actually vote for the Cut, Cap, and Balance - because she didn't think it was extreme enough! She told Fox News that America should pay off all our foreign creditors first, even if that means slashing Medicare and Social Security payments to America's seniors. We expect her to repeat similar sentiments tonight. 

Additionally, look for Herman Cain to tout more failed economic policies like his support for a "fair tax" for everyone, which even the Wall Street Journal editorial board called "the most radical reform imaginable" and one that would "hit hard the young, middle-income families." 

Conclusion 

As we expect to see tonight, the GOP candidates' extreme aims to appease the far-right wing of the Republican Party shows that Republicans are more concerned with protecting their special interest friends and the wealthy than protecting working families. The GOP would rather seniors and the middle class sacrifice and the wealthy pay even less in taxes. Unlike President Obama, their policies would eliminate, not create, opportunities for the middle class. They are not interested in a balanced approach to reducing America's deficit that increases revenues and protects America's promise to our seniors and middle class through Medicare, Social Security and Medicaid. 

From day one President Obama has been willing to do the hard work to address the fiscal mess he inherited and move our country in the right direction. The President has worked tirelessly to bring our economy back from the brink, get it growing again and to create jobs for middle class families. He understands we have a long term deficit problem and that special interests aren't left off the hook when it comes to paying their fair share. And sadly, to date all we've seen from the current GOP field is a Duck, Dodge and Dismantle approach. Tonight in Iowa we should not expect anything different than more of the same failed economic policies that brought our economy to the brink and advocacy of extreme policies that will hurt seniors and the middle class.
