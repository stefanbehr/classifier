A group of Blue Dog Democrats is saying it's time for everyone to return to Washington. 

Citing the credit downgrade and current economy, House Democrats Jim Cooper of Tennessee, Michael Michaud of Maine, Mike McIntyre of North Carolina and Henry Cuellar of Texas are calling for Congress to cut the August vacation short and return to Washington. 

"I know there are disagreements, but how are we going to start addressing the issues if we don't get back?" asked Rep. Cuellar during a Saturday interview with Fox News. "In 30 days we're not going to be having disagreements?" 

The four democrats sent a letter to House Speaker John Boehner and Minority Leader Nancy Pelosi arguing, "S P believes our political system is so dysfunctional that it will be unwilling to find a fiscal solution. The American people wonder why, in the midst of this crisis, we are on an extended break." 

Cuellar says he's hearing that sentiment straight from his constituents back home, telling Fox News, "I am meeting with a group of people here in Laredo, Texas right now and people are frustrated that we cannot sit down and work things out." 

The 4th-term Democrat says while he knows his colleagues look forward to vacation, an exception should be made. 

"There has been a tradition for years that Congress goes home in August and we see that," said Cuellar. "But we are in very difficult times, and I think we do need to sit down and talk."
