When it comes to choosing a president Americans usually look to governors and senators. 

JFK. LBJ. Richard Nixon. Jimmy Carter. Bill Clinton. Ronald Reagan. George W. Bush. Barack Obama. Even the runners-up were folks like Nixon, Hubert Humphrey, George McGovern, Walter Mondale, Bob Dole, John Kerry and John McCain. 

So should anyone be shocked that two U.S. House members are now at the top of the GOP presidential field after the influential Iowa Straw Poll? 

Reps. Michele Bachmann (R-MN) and Ron Paul (R-TX) battered their adversaries like deep-fried Twinkies at the Iowa State Fair. Granted, Rep. Thad McCotter (R-MI) didn't do as well, pocketing only 35 votes. But he wasn't that far behind former Utah Gov. John Huntsman (R) who collected a mere 69. Former Massachusetts Gov. Mitt Romney (R) placed seventh of ten in the poll. And the poor showing by former Minnesota Gov. Tim Pawlenty (R) forced him to withdraw from the sweepstakes altogether. 

No one quite knows how this pageant ends for Bachmann, but analysts certainly give her a shot. Paul's ridden in this rodeo before and has the support to hang around in the race for a while. Political handicappers view McCotter as a lower-tier candidate. But just the fact that there are three U.S. House members in the race and not a single U.S. senator in the field speaks volumes about the tectonic shift of the electorate over the past several years. 

And more importantly, the 1-2 finish by Bachmann and Paul shows how quickly the House of Representatives has morphed into a political incubator for the GOP. 

Fueled by social media and propelled by the tea party, the Bachmann and Paul experiences in Iowa attest to the wave of nouveaux populism that now has a chokehold on American politics. 

It should stun no one that Bachmann and Paul both serve in the House of Representatives, not in the Senate and not in some governor's mansion somewhere. After all, the Founders designed the House to be the most-direct, immediate voice of the people. House members would stand for re-election every two years and face the fickle whims of voters. Meantime, senators were awarded six-year terms. Voters didn't even get a say as to who went to the Senate until the early 20th Century. The Constitution initially dictated that state legislatures tap senators. 



A floe of searing-hot political magma has cascaded over the country during the past three electoral cycles. Control of the House and by how much has mirrored the political tenor of the moment. Dissatisfied swing voters shifted the House to Democratic control in 2006. Democrats then maximized their House majority in 2008 on the coattails of President Obama's win. Buyer's remorse set in quickly, wiping out all of the Democrats' gains in 2010, propelling 87 freshman Republicans to Washington, fueled substantially by the tea party movement. 

Now in August, 20111, the tea party remains at the rudder of the GOP. This phenomenon is particularly acute at an organization-driven event like the Iowa Straw Poll. Considering the contemporary political climate, it's only natural that candidates like Bachmann, Paul and McCotter would emerge. And at the end of Iowa, the House tandem of Bachmann and Paul materialized out of the cornfield as magically as Shoeless Joe Jackson in "Field of Dreams." Meantime, their competitors are still fighting their way through the stalks, tassels and husks clinging to their clothes. 

Bachmann is the marrow of the tea party. She founded the House Tea Party Caucus, vehemently opposed raising the debt ceiling, is hell-bent on repealing the health care reform law and wants to bestow Americans with the divine right to screw in any light bulb they darn well please. 

Paul represents a slightly different wing of the GOP. Like Bachmann, he advocates limited government. But his libertarian brand of conservatism is reflected best in the tea party mantra of "Don't Tread on Me." Paul's leery of the Patriot Act, suspicious of the Federal Reserve and endorses legalizing marijuana. In addition, Paul and his supporters reject the "neo-con" philosophies espoused by the Bush Administration. Paul believes the U.S. continues to engage in three, calcified conflicts (Afghanistan, Iraq and Libya) and should essentially mind its own business. 

One can distill much of the political energy so prevalent on the Republican side of the aisle into the candidacies of Bachmann and Paul. They hail from the House. The House mirrors the political tone of the moment. And that's why these candidates excelled in Iowa. 

But will it last? 

Opposition to a hike in the debt ceiling is good retail politics in Iowa. Bachmann and Paul both opposed the final agreement that raised the debt limit two weeks ago. But does that wash among pliable Democrats and critical swing voters who watch the daily gyrations of the Dow? How does a vote like that that stand up against the record of a state executive like Romney or Texas Gov. Rick Perry (R)? Bachmann's been vocal in her opposition to "Obama-care." That said, the Minnesota Republican might turn up the volume on her anti-health care reform mantra and target Romney for his statewide efforts in Massachusetts. Bachmann also pirouetted hastily into a "jobs and economic growth" message. But how will that fare against Perry, who has one of the most-impressive economic records of any governor? 

And then there's the question of how the vicissitudes of the tea party and conservative-libertarianism championed by Bachmann and Paul would compare in the general election against Mr. Obama. Neither are the architects of any major legislation that's become law or chaired a major Congressional committee. In the case of Paul, he's been in Congress for years. Bachmann just arrived on the scene in 2006. Many Republicans pilloried President Obama for his relative lack of experience when he sought the White House in 2008. They can't use that against him now. He's the president. And in the case of Bachmann, well, she has little more experience on Capitol Hill than Mr. Obama did four years ago. 

Yes, the economy remains in the ditch. Unemployment remains at an alarmingly high rate. The nation could easily teeter on the precipice of a federal default in just a matter of months. The president is vulnerable. But if the likes of Bachmann or Paul were to surge past a Romney or a Perry, what would their message be when challenging the president next fall? It could sound like something very familiar from three years ago: hope and change. 

Which brings us to the bare essence of why two House candidates gained traction at this stage of the presidential cycle: the candidacies of Bachmann and Paul crystallize the political tempest that's stirred in the American electorate for some time. Members of the House reflect the passion of the moment. And when anti-Washington sentiments are inflamed the way they are now, it's only natural that the politicians who succeed are those with pedigrees like Bachmann and Paul. 

For the moment. 

It's entirely possible that Bachmann and Paul are total creations of the here and now. Once the presidential race fully ripens, Republicans could revert to the historic trend of tapping governors and senators to carry the flag in the general election. That's where Romney, Perry, Huntsman, former Sen. Rick Santorum (R-PA) or even former Alaska Gov. Sarah Palin (R) would excel (should Palin decide to run). 

But for now, the most significant political fire in the country smolders in the House of Representatives. The 87 conservative freshmen voters elected last fall stoke its embers. They represent the core of the debt limit fight and why a previously stray, forgotten-after vote evolved into a battle royale between the administration and Congress. The feistiness of the electorate is embodied in the 59 House Republicans who voted nay on the bill to avert a government shutdown in April. And the public's unease was on full display late last month when conservatives forced House Speaker John Boehner (R-OH) to yank a Republican-crafted bill to cut spending and raise the debt ceiling off the floor just moments before a scheduled vote. The reason was simple: even a legislative master like Boehner couldn't muster the votes among House conservatives for that particular effort. 

This restlessness on the right is why House members like Bachmann and Paul are enjoying early success. 

The Founders intended the House's membership to reflect the nation's political atmospherics at two years intervals. But if Bachmann, Paul or McCotter buck recent candidate selection trends and capture the Republican nomination next year, it's clear that the special potency realized in the 2010 House contests is more than just a momentary reading of the political barometer. If the GOP selects its nominee from the House of Representatives, that portends a more formidable, longer-term shift that extends beyond the two-year election cycle. 

In that case, picking a presidential candidate from the House might not represent "hope." But it would signal a seismic "change" to the type of candidates voters choose to run for the White House.
