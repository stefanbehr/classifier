Las Vegas, Nevada - House Minority leader Nancy Pelosi was scheduled to give the keynote address at the United Steelworkers Convention in Las Vegas on Monday, but it turned out to be more of a stump speech for President Obama. 

"We have to consider the successes we had in the first four years when (the Democrats) had the (House) majority, especially when President Barack Obama became President of the United States and what a great President he is," said the former Speaker of the House. 

Pelosi's speech to a crowd of union workers at the MGM Grand Convention Center was similar to Vice President Biden's address at the Teamsters convention in Vegas a little over a month ago, stating that the Democrats stood in sharp contrast to what she terms as the "anti-worker agenda" of the GOP. 

"For months in Ohio, Wisconsin, and states nationwide, Americans have seen Republican governors and legislators attack teachers and public servants," said Pelosi. "We've seen workers, union and non-union alike, inspire the nation to fight back." 

She even jumped into the presidential political fray by taking a shot at GOP presidential frontrunner Mitt Romney , who during a speech at the Iowa State fair last week responded to a heckler's suggestion of raising taxes on corporations as a way to reduce deficit by saying "corporations are people, my friend." 

"Now you heard one of the (GOP) presidential candidates, who shall remain nameless," said Pelosi,"he just kept insisting 'CEOs are people' and shareholders are people. Well apparently I have news for him, workers are people too!" 

But the address ultimately came back to Pelosi praising President Obama's accomplishments during his first two years, citing legislation to end discrimination of women in the workplace, signing a repeal of "Don't Ask, Don't Tell," and the President's signature healthcare law, which she called a "jewel in the crown" that made "healthcare for all Americans as a right, not a privilege." 

After talking about the President's accomplishments, Pelosi made it clear that the Democrats will need all the support they can get from the unions in order to re-elect President Obama. 

"He had a great couple of years as President and we want him to continue to do that in the next year and a half," said Pelosi, "and also in his re-election bid as President of the United States... Barack Obama ."
