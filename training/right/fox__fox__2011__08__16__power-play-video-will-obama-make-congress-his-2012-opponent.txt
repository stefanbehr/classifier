While the 2012 GOP presidential candidates have been attacking the president, he is focusing many of his election-style attacks at an unpopular Congress. 

On Tuesday's Power Play Live, Chris Stirewalt and the panel explored whether Obama's attacks on Congress will continue toward election day. 

"At the moment, Congress is an easy target because they're about the only entity whose approval ratings are even lower than the presidents," said Costas Panagopoulos, a political scientist from Fordham University. "So I think that the American public has very little love for the Congress right now and when a president's approval rating has dropped below 40 percent for the first time in some polls, the economy is not improving as rapidly as we'd like to see and there are not necessarily many other triumphs to tour on the campaign trail, I think Congress is an easy target." 

Watch the video below for more and click http://live.foxnews.com each weekday at 11:30 a.m. Eastern for Power Play Live. 





Watch the latest video at FoxNews.com
