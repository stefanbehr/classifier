The battle between supporters of creationism and evolution made its way onto the 2012 campaign trail Thursday as a young boy, prompted by his mother, asked Texas Gov. Rick Perry questions on the subject, leading Perry to suggest both are taught in Texas public schools. 

"How old do I think the Earth is?" Perry said repeating the boy's question. "I have no idea - it's pretty old. It goes back a long way - I'm not sure anyone knows really completely know how old it is." 

But Perry continued, saying the boy's mother was really trying to get a response about creationism and evolution. While Texas public schools don't officially teach creationism, some claim Perry has pushed a weakened evolution curriculum to open the door for creationism in schools. 

"I know your mom is asking about evolution," he said. "It's a theory that's out there and it's got some gaps in it. In Texas, we teach creationism and evolution because I feel you're smart enough to figure out which one is right." 

A Texas group that monitors the intersection of religion and education pushed back against Perry's claim of creationism in public schools and says the presidential candidate was looking for political gain. 

"Gov. Perry has once again waded into the culture wars for political gain, but without considering the harmful consequences," Texas Freedom Network President Kathy Miller said in a statement. "[I]t is outrageous that Gov. Perry would erode respect for and trust in public education in Texas, simply in order to promote his political aspirations." 

The Texas Education Agency, the state office that oversees curriculum in Texas public schools, says the truth might fall somewhere in between Perry's claim and TFN's position. 

"Our science standards require students to analyze, evaluate, and critique scientific explanations, so it is likely that other theories, such as creationism, would be discussed in class," an email from the agency read. "Our schools can also offer an elective course on Biblical history and it is likely that creationism is discussed as part of that class, too." 

Though the New Hampshire boy's mother prodded him to further question Perry about "why he doesn't believe in science," many in the crowd moved on to criticize Perry's positions on entitlement reforms, chanting "hands off our benefits!" 

Fox News Radio's Jared Halpern contributed to this report.
