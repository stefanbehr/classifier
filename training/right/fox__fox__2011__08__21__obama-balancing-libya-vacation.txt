Vineyard Haven, MASSACHUSETTS -- President Obama declared late Sunday night in a written statement that Libya is "slipping from the grasp of a tyrant" as it appeared that rebels in the streets of Tripoli were edging closer to bouncing dictator Moammar Qaddafi from power. 

"Tonight, the momentum against the Qadhafi regime has reached a tipping point," Obama said. "Tripoli is slipping from the grasp of a tyrant. The Qadhafi regime is showing signs of collapsing. The people of Libya are showing that the universal pursuit of dignity and freedom is far stronger than the iron fist of a dictator." 



The statement came shortly after Obama convened a 9:00 p.m. time secure conference call about the situation in Libya with senior members of his national security team. "The president asked that he continue to be updated as neccesary and is scheduled to be briefed on this topic (Monday morning)," said White House spokesman Josh Earnest. 

Participants in the secure call included defense secretary Leon Panetta, Joint Chiefs Chairman Admiral Mike Mullen, White House Chief of Staff Bill Daley, and several other top officials. 

At the end of a day in which rumors were running rampant about Qaddafi's fate, Obama's statement made clear that the U.S. believes that the dictator is still in power -- at least for now. 



"The surest way for the bloodshed to end is simple: Moammar Qadhafi and his regime need to recognize that their rule has come to an end. Qadhafi needs to acknowledge the reality that he no longer controls Libya," Obama said in a written statement. "He needs to relinquish power once and for all. Meanwhile, the United States has recognized the Transitional National Council as the legitimate governing authority in Libya. At this pivotal and historic time, the TNC should continue to demonstrate the leadership that is necessary to steer the country through a transition by respecting the rights of the people of Libya, avoiding civilian casualties, protecting the institutions of the Libyan state, and pursuing a transition to democracy that is just and inclusive for all of the people of Libya. A season of conflict must lead to one of peace." 

Obama's statement also suggested the U.S. has concerns about what a post-Qaddafi Libya will look like, amid chaos on the streets of Tripoli. 

"The future of Libya is now in the hands of the Libyan people. Going forward, the United States will continue to stay in close coordination with the TNC," Obama said of the rebels. "We will continue to insist that the basic rights of the Libyan people are respected. And we will continue to work with our allies and partners in the international community to protect the people of Libya, and to support a peaceful transition to democracy." 

During his vacation here on this picturesque island, Obama has been getting updates on the dramatic scenes playing out halfway around the world in Tripoli. But before issuing the written statement, Obama told reporters he will not make an on-camera statement on the turmoil until the U.S. government has full confirmation about the fate of Qaddafi. 

"We're gonna wait until we get full confirmation of what is happening," Obama briefly told reporters as he made his way into a popular restaurant here shortly before he conducted the secure conference call. "I will make a statement when we do." 

Obama was greeted with chants of "four more years" as he shook hands with cheering supporters as he made his way into Nancy's restaurant, though he ended up not staying there for dinner. 

Earnest said the President and First Lady Michelle Obama stopped by one of their "favorite spots on the island to visit with some friends" before heading to a home where White House aide Valerie Jarrett is staying for dinner. 

According to Earnest, early on Sunday Obama received a briefing on the situation in Libya from John Brennan, a top national security council aide, who is on the island with Obama to make sure he is in the loop on major foreign policy matters. 

Obama spent the rest of the day largely in vacation mode, starting with a couple of hours on a local beach with Mrs. Obama and their two daughters, Sasha and Malia. 

After departing the beach separately from his family, the President played golf for the second time during this vacation. Aides say he played less than a full 18 holes with Robert Wolf of investment bank UBS, longtime friend Eric Whitaker, and White House aide Marvin Nicholson. 

Obama and Jarrett then stopped by the home of Brian Roberts , chairman and CEO of MSNBC parent company Comcast, for a reception. A pool report filed by a journalist traveling with Obama said the main Roberts house is a "two-winged mansion, with a big swimming pool and private beach, overlooking Vineyard Sound." 

Aides have said over the last two days that Obama has been getting regular updates on the situation in Libya throughout his vacation, though the President has been careful not to comment publicly so as not to get ahead of events on the ground in Tripoli. 

Separate written statements, however, by Earnest and a State Department official both used the same exact phrase declaring Qaddafi's "days are numbered." 

"The United States continues to communicate closely with our allies, partners, and the TNC," Earnest said in his statement. "We believe that (Qaddafi's) days are numbered, and that the Libyan people deserve a just, democratic and peaceful future." 

The State Department statement noted Secretary Hillary Clinton is also getting regular briefings. 

"We continue efforts to encourage the TNC to maintain broad outreach across all segments of Libyan society and to plan for post-Qadhafi Libya," said the statement by Victoria Nuland. "Qadhafi's days are numbered. If Qadhafi cared about the welfare of the Libyan people, he would step down now." 

In March, Obama joined key NATO allies in leading a limited military operation in Libya while he was on an official trip to Latin America . Obama ended up cutting that foreign trip a few hours short in order to get back to Washington earlier in order to more closely monitor the early stages of the NATO mission, though there are no signs at this point that he is planning to cut this domestic vacation short. 

Back in the spring, critics in both parties suggested there was not a clear rationale for the mission and Congress was not adequately consulted on the mission. 

But in a speech to the nation at the National Defense University, Obama stressed no U.S. ground forces would be used in the conflict and said his administration was compelled to act amid "brutal repression and a looming humanitarian crisis" brought on by Qaddafi, whom he labeled a "tyrant" in the speech.
