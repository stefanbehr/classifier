Following Tuesday's 5.8 magnitude earthquake and mass evacuations across Washington D.C., some hot spots on Capitol Hill saw an increase in customers. 

The bars packed in patrons and offered specials in honor of the earthquake. 

When asked if they were busier than usual, the Capitol Lounge replied, "Oh yeah we ran out of a keg of Yuengling and a keg of Bud Light in 30 minutes." 

Union Pub said they re "packed." The bar offered $12 pitchers, $4 drinks for Tuesday only. 

Hawk N Dove did not offer any specials, but said the manager said they were "packed way more than usual." 

Lounge 201 reported a "bit of a rush" thanks to the evacuation of Capitol office buildings. 

Bullfeathers was "packed all day," since staffers left the Hill. The bar offered a Nationals game special: $2.50-$3.00 domestics bottles. 

Pour House said, "Our main bar was packed at 4:30 and usually it s not." They offered regular Tuesday specials.
