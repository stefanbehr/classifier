It may be 75 degrees and sunny on Martha's Vineyard, but President Obama is bracing for the possible U.S. impact of Hurricane Irene. Tuesday Mr. Obama received updates on the storm as well as FEMA preparation for areas that could be in Irene's track later this week. 

"This morning at the Blue Heron Farm, John Brennan gave the President his daily national security briefing including an update on Hurricane Irene and how FEMA is supporting territorial response activities in Puerto Rico as well as the Virgin Islands where the effects of the storm have already been felt," White House Principal Deputy Press Secretary Josh Earnest said in a statement. 

The hurricane was still south of Grand Turk Island Tuesday morning, but the National Hurricane Center predicts Irene will approach the Carolinas and Chesapeake Bay on Sunday. President Obama is scheduled to return to Washington Saturday. 

Brennan "also updated [the president] on the steps FEMA has taken in coordination with states that could feel the impact of the storm later this week," said Earnest. 

The last hurricane to strike the U.S. was Ike in the summer of 2008 while Obama was running for office. Irene could become a category 3 by Tuesday.
