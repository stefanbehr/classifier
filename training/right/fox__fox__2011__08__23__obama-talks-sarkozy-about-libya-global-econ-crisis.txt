VINEYARD HAVEN, Massachusetts - President Obama spoke by phone today with French President Nicolas Sarkozy about the situation in Libya , according to White House spokesman Josh Earnest. 



A second senior administration official told Fox that the two leaders also discussed "the idea of an international conference in the near future to support the Libyan people" and agreed to stay in close coordination as the crisis unfolds. 

"With regard to Libya , they agreed that Qadhafi and the remnants of his regime needed to accept that their time is up and relinquish power once and for all, and they welcomed the progress that has been made by the Libyan people in pursuing an end to the Qadhafi regime," said Earnest. "At the same time, they agreed to continue to work with allies and partners in the international community to protect the people of Libya and to support a peaceful transition to democracy." 

Earnest added that Sarkozy joined Obama in urging the Transitional National Council to "continue demonstrating its leadership by respecting the rights of the people of Libya, avoiding civilian casualties, protecting the institutions of the Libyan state, and pursuing a transition to democracy that is just and inclusive for all of the people of Libya."In addition, the two leaders also discussed "the global economic situation, the Eurozone crisis and recent market developments," according to Earnest.
