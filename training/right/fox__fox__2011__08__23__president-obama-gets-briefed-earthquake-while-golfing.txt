When the earthquake hit the DC area Tuesday afternoon, President Obama was playing golf on Martha's Vineyard. 

The quake which hit just before 2 o'clock could even be felt on the island located off the coast of Cape Cod. The press corps covering Mr. Obama's vacation could a feel subtle rumble in the work area located on the first floor of a Vineyard Haven hotel. 

Mr. Obama, however, did not feel the earthquake today, according to White House Principal Deputy Press Secretary Josh Earnest. 

The president, who hit the links for the third time since he arrived on the island last Thursday, led a conference call with members of his administration "to discuss the earthquake and status of critical infrastructure." According to a paper statement, Earnest said the president "was told that there are no initial reports of major infrastructure damage, including at airports and nuclear facilities and that there were currently no requests for assistance." 

After the conference call, the president continued his game of golf with long-time pals Eric Whitaker and Vernon Jordan and White House Trip Director Marvin Nicholson. 

The president, who has had frequent briefings during his vacation, has asked officials for regular updates on the situation.
