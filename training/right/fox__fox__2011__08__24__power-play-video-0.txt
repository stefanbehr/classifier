Chris Stirewalt discusses the politics of the nation's weakening economy and an alarming report from the Congressional Budget Office with Fox Business Network's Rich Edson and Fox News contributor Mary Katherine Ham. 

Watch the latest video at FoxNews.com
