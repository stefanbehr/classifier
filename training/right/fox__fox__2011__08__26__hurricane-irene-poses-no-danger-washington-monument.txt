The National Park Service says the Washington Monument is not in any danger from Hurricane Irene. 

A temporary sealant has already been applied to the crack caused by Tuesday's earthquake, says NPS spokesman Bill Line. The sealant will prevent water from seeping in and causing more damage. 

Line emphasizes that the monument has survived hurricanes, high winds, and rainstorms in the past. 

The National Weather Service currently has the District of Columbia and Metropolitan Area under a Tropical Storm Warning.
