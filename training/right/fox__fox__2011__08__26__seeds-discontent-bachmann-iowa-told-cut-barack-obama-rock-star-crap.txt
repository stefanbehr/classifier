Not two weeks removed from cheers surrounding her momentous victory at the Iowa GOP's Straw Poll, there is a growing chorus of criticism about the way Rep. Michele Bachmann has been conducting her campaign in the Hawkeye State. 

Can the " Barack Obama rock star crap." 

That comes from Judd Saul, who helped organize the Blackhawk County Lincoln Day Dinner. The fundraiser, which was the day after the Straw Poll, was Texas Governor Rick Perry's first presidential campaign event in Iowa. 

It was held in Waterloo, a place where she is no stranger. Bachmann proudly states she is from Waterloo and It is also where she officially launched her White House bid. 

But Saul says Bachmann's behavior at the dinner left folks "kinda pissed." 

"(Perry) sat with people, talked with people. (Bachmann) acted like a rock star, refused to eat dinner with us," he said. "If Michele Bachmann is the hometown girl, from Waterloo, she should dine with us." 

Instead, Saul points out Bachmann spent time on her campaign bus, only entering for her turn to speak at the event after she was introduced twice. 

Out in Western Iowa, the chairman of the Pottawattamie County Republican Party , Jeff Jorgenson tells Fox News, "(Bachmann) hasn't been making herself available...as far as retail politics is concerned, she's got a long way to go." 

In large part, Republican ire at Bachmann comes because she is seen as not fully embracing the '"rules" of Iowa campaigning. 

Unlike most other states, Iowans expect candidates, even ones for 'leader of the Free World', to linger at events to talk with every person left in the room. Presidential hopefuls are also expected to take questions from not only the local media, but also everyday Iowans. 

Fox News has found through conversations with Iowa Republican operatives and GOP regulars that Bachmann has developed a reputation as a campaigner who likes friendly photo-ops, autographs and taking pictures with supporters. But she is also frequently late to events and often does not take questions. 

Craig Robinson, founder of the conservative 'blog TheIowaRepublican.com, says Bachmann also needs to keep in mind, the Iowa campaign has changed dramatically. 

"Campaigning in advance of the Straw Poll was easy, especially against a weak field with limited resources. (Bachmann's) now in the big leagues. Mitt is the Red Sox. Perry is the Yankees, and Bachmann is that team that can have a magical year if things go exactly right. They can't take Iowa for granted even after winning the Straw Poll because Perry is licking his chops." 

Even a key Bachmann backer at the Straw Poll says her campaign has to change. 

"The race started over after the straw poll," note Ryan Rhodes, director of the Iowa Tea Party . 

"(Bachmann) needs to work on being more accessible and punctual to events. These are things I'm hearing from Tea Partiers around the state and I hope her campaign irons out these kinks." 

The Bachmann campaign didn't respond to a Fox News request Thursday for its take on the Iowa complaints but did note the congresswoman had to cancel some planned events in the state for House votes back in Washington. 

A campaign representative previously told Fox News that Bachmann talks to crowds before and after campaign events but that some times they have to hustle the candidate off to the next stop. 

Most of the time Bachmann has spent on the campaign trail has been split between South Carolina and Iowa. The time she spent in the Hawkeye State leading up to the Ames straw poll is credited with propelling her to her first-place finish. 

Rhodes says he has made attempts to let the Bachmann campaign know about its alleged Iowa transgressions, but he hasn't heard from anyone with the campaign since the Straw Poll.
