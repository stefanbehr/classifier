Democratic National Committee chair and Florida Rep. Debbie Wasserman Schultz fired back Monday at a 2012 GOP presidential candidate who said the Everglades should be up for discussion as the U.S. looks at oil and natural gas drilling. 

Rep. Michele Bachmann , R-Minn., said Sunday that drilling for oil and natural gas should be considered in the south Florida swamp if it could be done without damaging the environment. Wasserman Schultz called that idea "outrageous." 

"Michele Bachmann's latest proposal to drill in the Florida Everglades is just another example of the Republican Party supporting policies that would only further enrich the special interests, while putting our environment and working families at risk," she said. 

Bachmann insists that all avenues need to be explored for the U.S. to attain energy independence and that domestic drilling is key. 

"The United States needs to be less dependent on foreign sources of energy and more dependent upon American resourcefulness. Whether that is in the Everglades, or whether that is in the eastern Gulf region, or whether that's in North Dakota, we need to go where the energy is," Bachmann said at a campaign event in Sarasota Sunday. 

But Wasserman Schultz argued Monday that any benefit from drilling in the Everglades isn't worth the risk to the environment. And she used the issue to swat at all the Republican presidential candidates on energy even though others have said drilling there isn't an option. 

"Michele Bachmann and the rest of the Republican field may think that if we give Big Oil a few more give-aways and take a few risks, we can drill ourselves to energy independence," her statement read. "Well the Republicans couldn't be more wrong..." 

In 2002, President George W. Bush pushed the federal government to buy back drilling rights in the Everglades to prevent drilling there.
