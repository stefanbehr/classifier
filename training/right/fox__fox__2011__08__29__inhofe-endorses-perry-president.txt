An Oklahoma Senator is endorsing a candidate from next-door Texas in the race for the 2012 GOP presidential nomination. 

Sen. James Inhofe, R-Okla., has given his support to Texas Gov. Rick Perry . 

"I've known Gov. Rick Perry for a long time, and I am endorsing him because I know he is the strongest leader to run against and defeat President Obama," a statement from Inhofe read. "After three years of Obama's liberal agenda, Rick Perry is the right person to get America working again and turn our country in the right direction." 

Inhofe, the sometimes controversial ranking member of the Senate Committee on Environment and Public Works, has been outspoken on environmental issues and what he calls overregulation. Inhofe notes Perry's position against cap and trade legislation and says that the Texas governor will stand up against those who support it. 

"(H)e won't cave in to the extreme environmental activists or the Hollywood crowd and their liberal agenda," Inhofe's statement continued. 

Perry entered the race in mid-August and several polls show him as a frontrunner among the GOP contenders. 

Mitt Romney announced the endorsement of Senator Orrin Hatch, R-Utah, in July, and has also received the endorsement of Sen. Jim Risch, R-Iaho. 

Ron Paul has been endorsed by his son Sen. Rand Paul , R-Ky.
