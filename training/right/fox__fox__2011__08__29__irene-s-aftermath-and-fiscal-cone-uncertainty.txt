The remnants of Hurricane Irene are now pummeling the outskirts of eastern Quebec and Newfoundland. 

But when it comes to emergency recovery funding, Irene's cone of uncertainty is just now cutting a wide swath though Capitol Hill . 

It will take days and even weeks to assess the cost of Irene as communities try to recover from flooding, infrastructure damage and beach erosion. 

But already, officials are deciding where to direct emergency money. 

Bob Josephson of the Federal Emergency Management Agency (FEMA) told the Associated Press that its disaster fund is running low. A monster tornado pulverized much of Joplin, MO in late May. And Josephson says FEMA is now diverting some money designated for Joplin for needy locations along the eastern seaboard. 

The decision raised the ire of both of Missouri's senators. 

"I warned FEMA and assured victims in Joplin that they would not be forgotten after the camera trucks lowered their antennas and rolled out of town," said Sen. Claire McCaskill (D-MO) in a statement. "I will fight to make sure that promise is kept. FEMA should be prepared for all types of disasters and have the resources to respond rapidly and stay until the work is done, and until the community is made whole again." 



Sen. Roy Blunt (R-MO) joined McCaskill in his outrage. 

"Recovery from hurricane damage on the east coast must not come at the expense of Missouri's rebuilding efforts," said Blunt. "If FEMA can't fulfill its promise to our state because we have other disasters, that's unacceptable and we need to take a serious look at how our disaster response policies are funded and implemented." 

Here's the problem: FEMA's coffers have run low all year. Big floods in Tennessee, Mississippi, Nebraska, Kansas Louisiana and major tornadoes in Alabama Missouri exacerbated the situation. 

In addition, Washington is nearing the end of its fiscal year on September 30. Even prior to Irene, FEMA's Immediate Needs Funding dwindled to a paltry $792 million. 

So what to do? 

One option is what's called a "supplemental spending bill." 

Here's how it works: 

Imagine that the budget of the federal government is a pie. It's caved into 12 slices which fund various departments and agencies. Then, the House and Senate have individual Appropriations Subcommittees which decide how much the federal government should spend on each agency and what programs they should fund. For instance in the House, there's the "Agriculture, Rural Development, Food and Drug Administration and Related Agencies Subcommittee" and the "Commerce, Justice, Science and Related Agencies Subcommittee." 

But periodically, Congress and the president determine the nation's spending exceeds what's already allotted. So they craft an ad-hoc supplemental spending bill in the middle of the fiscal year. 

Presidents and lawmakers often concoct these bills to cover natural disasters. Starting after September 11, President George W. Bush submitted multiple emergency supplemental spending bills to Congress, requesting additional funds to foot the bills for wars in Afghanistan and Iraq. 

Over the weekend, a senior House Republican aide was non-committal about the possibility of a supplemental spending bill. 

"We'll discuss that if and when needed," the aide said. 

But others are more certain about what lies ahead for Congress and the president on FEMA. 

"I don't think there's any question there will be a supplemental (spending bill)," said a senior House Democratic aide. 

However, President Obama hasn't asked for anything yet. House Majority Leader Eric Cantor (R-VA) indicated last week that lawmakers will make money available. But unlike in previous years, they also have to find requisite cuts to make up the difference. 

"We're going to find the money," Cantor said on FOX Monday. "We're going to need to make sure there are savings elsewhere." 

Talk like this infuriates Democrats. 

"We don't offset emergencies. What investments do we raid to address other needs?" fumed one House Democratic aide. 

But Cantor spokesman Brad Dayspring notes that the House has already offset some of that money. 

The government's new fiscal year starts October 1. The House approved its annual Department of Homeland Security appropriations bill (which funds FEMA) three months ago. Even though the bill is designed to pay FEMA and Homeland Security programs starting October 1, this legislation injects the near-depleted FEMA disaster fund with $1 billion for the CURRENT fiscal year. Dayspring notes that the House offset that additional spending by transferring one batch of money and rescinding funds from a Department of Energy vehicle program 

But the Senate hasn't yet acted on its version of the legislation. On Monday, the Senate Homeland Security Appropriations Subcommittee announced it would formally write its package early next month. 

In the meantime, lawmakers wait to see what the White House might ask for. 

"We can't respond to an emergency request that doesn't yet exist," said Dayspring. 

Regardless, since the Senate has the benefit of drafting its legislation AFTER Irene, it's increasingly likely it could draw up a measure with a dollar figure well above what the House okayed. It's also important to note who chairs the Senate Homeland Security Appropriations Subcommittee. That's Sen. Mary Landrieu (D-LA), who knows something about disaster relief after hurricanes. Interestingly, Sen. Patrick Leahy (D-VT) is also a member of the same panel. The storm inflicted some of its worst damage in the Green Mountain State. 

All of this creates a cone of uncertainty for lawmakers as they return to Washington. 

For starters, the current political climate has Republicans pushing to hold the bottom line. But there's potential for political peril there. If it becomes evident that significant emergency spending is necessary (and even demanded by some GOP lawmakers who represent states and districts devastated by disasters), voters could view Republicans as callous if they scrap with Democrats about potential offsets. To the contrary, some voters may applaud such a battle if its viewed as fiscally responsible. 

Secondly, Congress reconvenes with some bad news for conservatives and tea party loyalists: the debt ceiling agreement approved earlier this month kicked the overall operating figure for THIS fiscal year (not the one that begins in October) to a level $24 billion HIGHER than what the House approved in the budget released by Rep. Paul Ryan (R-WI). Because of that, it's possible the House will have to engineer some sort of vote that commissions it to operate off the higher figure rather than the number in the Ryan budget. 

This likely to drive those affiliated with the tea party up a wall and could present a challenge to the House Republican leadership. 

Third, it's nearly impossible for the House and Senate to approve all of the FY '12 appropriations bills to keep the government running past September 30. So it's possible the House, Senate and President Obama could again be engaged in hand-to-hand fiscal combat about keeping the government running come October 1. 

Heaping an additional spending bill to cover Irene and other disasters on top of this already gorged menu spells trouble. 

Again, not even a month after averting a federal default and four-and-half months after avoiding a government shutdown, Washington could again stare into the crisis abyss. 

The easy out? 

Lawmakers and the administration could forge a comprehensive bill that keeps the government operating past September 30, includes emergency spending to address Irene and trims spending. But that's going to take a lot of give on all sides. 

And it crashes headlong into one of the most steady dictums in American government: all politics is local. 

Politics becomes local when a raging storm and its influences ravages the districts and states represented by a squadron of House and Senate appropriators who are charged with determining how the government spends its money. 

Senate appropriators who represent Irene-whipped areas include Leahy along with Sens. Barbara Mikulski (D-MD), Jack Reed (D-RI), Frank Lautenberg (D-NJ), Susan Collins (R-ME) and Lindsey Graham (R-SC). 

In the House, the list includes Reps. Frank Wolf (R-VA), Rodney Frelinghuysen (R-NJ), Nita Lowey (D-NY), Jose Serrano (D-NY), Rosa DeLauro (D-CT), Jim Moran (D-VA), John Olver (D-MA), David Price (D-NC), Chakah Fattah (D-PA) and Steve Rothman (D-NJ). 

So just as what's left of Irene spins out to sea, prepare for another storm to bear down on the Beltway coastline. It's a tempest over spending. And no one quite knows the path this gale might take. 

Which leaves Capitol Hill well within a fiscal cone of uncertainty.
