Responding to a request from House Speaker John Boehner , R-Ohio, for the Obama administration to "provide a list of all pending and planned rulemakings with a projected impact on our economy in excess of $1 billion," President Barack Obama tells the speaker that his administration has taken steps to minimize regulatory burdens and costs government wide. 

In a letter to Boehner, Mr. Obama writes that his executive order earlier this year "called for an ambitious Government-wide review of rules now on the books" which the president says is now complete. He adds that the review led to cuts from 26 agencies, generating more than $10 billion in savings over 5 years. 

President Obama replied Tuesday with a list of seven proposed rules with an estimated economic impact of over $1 billion. The various proposed regulations apply to the Environmental Protection Agency and Department of Transportation, ranging in rough costs from $1 billion for DOT hours of service regulations, to as much as $90 billion for ozone air standards. 

Boehner's office answered Obama's letter with a statement Tuesday afternoon bashing the seven regulations. 

"The combined cost of these seven new regulatory actions alone could be more than $100 billion," the statement read. "These costs will be felt by the American people in the form of fewer jobs and slower economic growth." 

In his earlier letter, the speaker requested the specific information from the White House be available to Congress when they return from recess in September, so that "as the House considers legislation requiring a congressional review and approval of any proposed federal government regulation that will have a significant impact on the economy as we continue our efforts to remove impediments to job creation and economic growth for the American people." 



And the House will take up regulation issues the second week of September, as reflected in the congressional schedule issued by House majority leader Eric Cantor , R-Va. "Our regulatory relief agenda will include repeal of specific regulations, as well as fundamental and structural reform of the rule-making system," Cantor stated in a memo Monday. 

Obama explained to Boehner in his correspondence that the rules creating over $1 billion in estimated economic impacts "are in a highly preliminary state" and promises scrutiny for the potentially costly measures. The president concluded the letter to Boehner by saying that he looks forward to working closely on a regulatory system. 

Boehner's office points out 212 smaller regulations it says the administration has in the works and called for more disclosure. 

"Given this new information disclosed today, I believe it is the Administration's responsibility to now make public the detailed cost estimates for all 219 of the new 'economically significant' regulatory actions it has planned, so that the American people can see the total cost of these government rules on private-sector job creation in our country," Boehner's statement said. 



Fox's Chad Pergram contributed to this report.
