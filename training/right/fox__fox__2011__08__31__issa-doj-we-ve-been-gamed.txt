The Justice Department's recent staff reshuffling can be seen as a result of the criticism of the botched gun tracking operation known as Fast and Furious, but it is unlikely to get the operation out of Congress' crosshairs anytime soon. 

"We know we are being gamed and we think the time for the game should be up," House Oversight Committee Chairman Darrell Issa , R-Calif., told Fox News host, Greta Van Susteren . 

Issa, along with Charles Grassley , R-Iowa, has lead the congressional investigation into the ill fated operation, and both he and Grassley agreed that there are still many unanswered questions. 

"We have confidential sources that have shown us why the administration's representatives knew these weapons were going to the cartels. Not mostly, not maybe, but virtually all of em," and that is why Issa says they can't give up on the investigation. 

Issa weighed in on the shakeup at the Department of Justice, which involved the demotion of ATF boss Ken Melson, hoping that some new blood might help the investigation move forward. He is hopeful that under a new U.S. attorney it can begin to go after those who were involved in the scheme. While Melson was cooperative and helpful during the investigation according to Issa, there was a need to have someone with independent eyes get to the bottom of Operation Fast and Furious. 

But, the Oversight and Government reform committee will make sure that a few individuals aren't sacrificial lambs. "Unfortunately, one of the problems is some of the eyes watching are high up in Eric Holder's office and had a lot to do with this happening." 

Issa told Van Susteren that the attorney general reached out to him for a conference call the same day as the staffing shakeup. He declined in order to meet in person next week when he returns to Washington. 

"I do think we need to work jointly to get this investigation wrapped up with some satisfactory conclusions that we are not heading toward right now."
