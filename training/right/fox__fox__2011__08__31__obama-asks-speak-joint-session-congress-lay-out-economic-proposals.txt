President Obama has sent a letter to Senate Majority Leader Harry Reid , D-Nev., and House Speaker John Boehner , R-Ohio, requesting to speak before a joint session of Congress next Wednesday night to lay out his jobs proposals. 

"I respectfully request the opportunity to address a Joint Session of Congress on September 7, 2011, at 8:00 p.m.," the letter reads. "It is my intention to lay out a series of bipartisan proposals that the Congress can take immediately to continue to rebuild the American economy by strengthening small businesses, helping Americans get back to work, and putting more money in the paychecks of the Middle Class and working Americans, while still reducing our deficit and getting our fiscal house in order." 

With Republicans in control of the House and Democrats in control of the Senate, the president will need bipartisan support for any proposals he lays out. After bitter partisan debates led to last minute agreements on government funding and a debt ceiling increase, the president is calling on lawmakers to come together around his new proposals. 

"It is our responsibility to find bipartisan solutions to help grow our economy, and if we are willing to put country before party, I am confident we can do just that," the letter continues. 

But the president asked for the session on what promises to be a partisan night in politics as the 2012 GOP presidential candidates are slated to debate at the Reagan Library in California at the same time that evening. 

The president he must be formally invited by Congress in order to address a joint session and can't just show up. The rule works the same way for the State of the Union. 



Fox News Senior House Producer Chad Pergram contributed to this report.
