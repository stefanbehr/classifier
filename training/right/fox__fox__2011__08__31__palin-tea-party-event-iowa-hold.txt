UPDATE: Tea Party of America founder Ken Crow says Sarah Palin is back on board for the event. Christine O'Donnell has been disinvited. Crow won't say why. 

Aides to former Alaska Gov. Sarah Palin say her Saturday appearance at a rally in Iowa by The Tea Party of America is "on hold." 



The Palin camp says it is a matter of "truthfulness and trust" with the Tea Party organizers of the Sept. 3 event.They do want to go forward with some sort of event because "this is about the people, not the organizers," but finding an alternative to the Indianola Balloon Grounds outside Des Moines that would be large enough to handle big crowds on short notice for Labor Day weekend could be tough. 

Palin insiders say that while this is a Tea Party event, Palin's PAC - SarahPac - is scrambling to find an alternative for a Saturday event. Regardless, Palin will attend a separate event Friday night in Iowa. As an example of the distrust, Palin insiders say that Christine O'Donnell, the Tea Party Express's failed 2010 GOP U.S. Senate candidate in Delaware, was first invited to speak at the Iowa weekend event, then disinvited, than re-invited after somebody with the Tea Party of America or the O'Donnell camp claimed Palin wanted O'Donnell re-invited. 

Palin aides say the governor has not spoken to O'Donnell in over a year and said nothing to the organizers about re-inviting O'Donnell. That, insiders say, is just one example of the problems they are facing with organizers "they can't trust."It has been widely speculated that Palin would announce whether she has decided to run for president this weekend, even though when Palin was asked at the Iowa State Fair less than three weeks ago if she would be ready to make a decision by Labor Day weekend and she said, "I doubt it." Late September into October has been a frequently mentioned timetable. 

After Iowa she plans to go to New Hampshire.
