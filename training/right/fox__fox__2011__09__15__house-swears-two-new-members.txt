The House of Representatives added two new members Thursday, bringing its total membership to 434. 

Reps. Bob Turner, R-N.Y., and Mark Amodei, R-Nev., were sworn in on the floor by House Speaker John Boehner , R-Ohio, shortly after the chamber began its work for the day. Turner and Amodei won special elections in their home states on Tuesday. 

Both members owe their new jobs at least in part to former members' indiscretions. Turner fills the seat formerly held by Anthony Weiner, the New York Democrat who resigned in disgrace after lewd pictures he took of himself surfaced on the internet. Amodei steps into a seat that opened up after a member of the House, Dean Heller, moved to the Senate to replace John Ensign , who resigned from his seat amidst a Senate ethics probe. 

The House is now made up of 242 Republicans and 192 Democrats. That means it'll take 218 "yea" votes to pass a bill under regular order. One seat remains vacant; former Rep. David Wu of Washington resigned in August.
