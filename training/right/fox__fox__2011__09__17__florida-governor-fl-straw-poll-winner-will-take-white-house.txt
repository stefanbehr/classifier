Florida Governor Rick Scott (R) is making a big prediction ahead of next week's Presidency 5 Straw Poll. 

"I personally believe whoever wins that straw poll, they will be the next president of the United States," said Scott on Fox News Saturday. 

But Florida's straw poll hasn't always picked the White House winner, or the GOP nominee for that matter. In 2008, Mitt Romney won the Sunshine State's poll, but lost out to Senator John McCain in the GOP primary. 

A handful of candidate, including Romney, Congresswoman Michele Bachmann and former Utah governor Jon Huntsman have all said they are not actively participating in Florida's straw poll. But Governor Scott says their names will still be on the straw poll ballot. "Even if you say you aren't participating, basically, everyone knows your positions. They're watching the debates. They're going to be watching," stated Scott. Governor Scott has yet to endorse any of the candidates, and says he has no plans right now to do so.
