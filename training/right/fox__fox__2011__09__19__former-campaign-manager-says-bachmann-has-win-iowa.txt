Michele Bachmann has to win in Iowa. Some might say that's right out of the Department of the Political Obvious,' but Ed Rollins, former campaign manager for the Minnesota Republican, says "that was always the strategy." 

"We always knew she had to win Iowa," said Rollins. "Win Iowa, do well in the debates, raise money...it's the Huckabee strategy." 

That's a reference to the political game plan for former Arkansas governor Mike Huckabee in 2008, for whom Rollins was also campaign manager. 

Rollins also disputes characterizations of his appearance on MSNBC Monday, where he's quoted saying Bachmann doesn't have the "ability or resources" to effectively campaign past Iowa for the Republican presidential nomination. 

"What I said was, she has to win Iowa," says Rollins. 

But Bachmann's interim campaign manager and Rollins' replacement Keith Nahigian says he is currently interviewing potential staff for states other than Iowa and just hired someone Arizona. 

Still, Rollins, who serves as an un-paid advisor to the Bachmann campaign, says there is a "stature gap" between Bachmann and Republican rival, Rick Perry. "There's a difference between being a Republican member of Congress and being governor of the most Republican state in America," Rollins said. 

Since Perry's entry, the same day as Bachmann's impressive win at the Iowa Straw Poll, Perry's campaign has catapulted into the top tier while Bachmann's faded in nationwide polls.
