A number of Senate Republicans who voted to support a $6.9 billion disaster aid bill authored by Senate Majority Leader Harry Reid, D-Nev., are now rethinking their position as the House prepares to approve a smaller amount for now, $3.7 billion, attaching it to a must-pass, stopgap spending bill to keep the federal government running beyond September 30. 

Senators Susan Collins of Maine and Roy Blunt of Missouri said they are reviewing what the House will pass Wednesday and what GOP leaders intend to support in an omnibus appropriations bill to come later this year. 

Both senators said, in no uncertain terms, though, that what the House is expected to approve is not enough for this year, so much so, that Blunt is working with Sen. John Hoeven, R-ND, who also voted in favor of the Reid bill, to approve more money in fiscal 2012 for Community Development Block Grants (CDBG). 

Collins said the Hoeven-Blunt effort would add about $500-700 million to the CDBG fund to help rebuild neighborhoods, but both men said the numbers were still in flux. 

Blunt said he intends to talk to his former colleagues in the House, adding, "I need some kind of commitment that we're going to meet these needs." 

Reid said he intends to try to plus up the House bill by $3.2 billion and strip out a House-approve measure to pay for part of the disaster funding through cuts to an Department of Energy loan program, but he will need the GOP coalition to stay on board. 

Not all of the 10 GOP senators who supported him have strayed. Sens. Scott Brown, R-Mass, and Hoeven both said unequivocally that they will support the Reid move. 

If lawmakers do not come to an agreement by September 30, the government will shut down, something Senate GOP Leader Mitch McConnell , R-Ky, predicted would not happen, in the end. 

Contrary to what some Democrats have said, House Republicans have not ruled out adding more money to the fiscal 2012 spending bills that cover disaster aid, though it is not clear when this will happen. 

Jennifer Hing, spokeswoman for the House Appropriations Committee, tells Fox, "We need time to scrub the (Administration's) request (don't want to just rubber stamp the White House number), collect more info, review incoming estimates, etc. Then we will make decisions on additional disaster funding needs over the next two months. This is the first step, not the only step."
