Republican Presidential contender and Texas Congressman Ron Paul says more and more Americans are subscribing to his governing philosophy of limited government, despite his limited presence in the mainstream media. 

An also ran during the 2008 primary cycle, Paul is currently third among announced candidates in the latest Real Clear Politics poll average. But he says the horse race isn't his principal concern. "I have one goal: to change policy, economic policy, foreign policy," the congressman told reporters at a breakfast sponsored by the Christian Science Monitor newspaper Wednesday. "I think it's catching on." 

Paul says he's getting traction with voters without the kind of media exposure that other top-tier candidates get, but that he doesn't take it personally. "I'm not surprised. I don't get too annoyed," he noted. "It's partially my fault. I'm trying to explain my message." He added that his philosophy is hard to explain in sound bites, which he says "holds him back." However, he thinks that his popularity has grown thanks to an aggressive internet following. 

Paul pointed to several instances of the Republican mainstream adopting his position as evidence that his message is catching on. 

In economic policy, he cited a letter from Republican Congressional leaders sent to Federal Reserve Chairman Ben Bernanke Tuesday that cautioned against further efforts to boost the economy through monetary policy. "It should have happened 30 years ago," Paul said, "More quantitative easing will just lead to more disaster." Paul believes that the Federal Reserve, and its ability to print more money, unfairly manipulates free markets. 

He also said that his views in foreign policy are becoming more popular in the Republican base. His anti-war views were very unpopular among GOP primary voters in 2008, but now Paul notes that, "We're having candidates say it's time to take troops out of Afghanistan ." 

While some of his positions remain unpopular, in particular his stance on the United States' role in the 9/11 attacks, Paul said he wouldn't change course in the interest of garnering more electoral support, "If you have the right ideas," he charged, "You win in the end."
