ORLANDO, FLA -- Following Thursday night's Fox News/ Google Presidential debate, front-running Texas Governor Rick Perry and former Massachusetts Governor Mitt Romney locked horns once again, this time at Friday's Conservative Political Action Conference (CPAC) in Florida. 

Perry has been widely criticized among media and political insiders alike as having a bit of an off night at the debate. Romney -- seeming to sensing an opportunity -- pounced on Perry for his stance on the lighting-rod issue of immigration. 



"My friend Governor Perry says if you don't give benefits to illegal immigrants you don't have a heart...if you are opposed to illegal immigration it doesn't mean you don't have a heart, it means you have a heart and a brain," 

Minnesota Congresswoman Michele Bachmann has stumbled in the polls and has been steadily losing traction ever since her Ames straw poll win back in August and the emergence of Perry as a candidate. On Friday, she once again sought to make up ground by brandishing her conservative credentials. 

She told the crowd, "As President of the United States, I will build a fence on our southern border against illegal immigration, and we will not have taxpayer subsidized benefits for illegal immigrants or for their children." 

The Perry campaign has acknowledged the criticisms of the governor's performance Thursday night, and one Perry adviser explained to Fox News Perry "was tired." 

For his part the Texas Governor steered clear of the immigration issue all together on Friday, and instead tweaked Romney for his universal health care plan when he was governor of Massachusetts, pointing to a beacon Hill study and telling the crowd, "Governor Romney's misguided healthcare mandate slowed income growth and cost Massachusetts 18,000 jobs. If Romneycare cost 18,000 jobs, just think what it will do to the rest of this country if Obamacare is applied." 



Perry also insisted debate skills are not what the presidency is all about, and that, "As conservatives, we know that values and vision matter. It's not who is the slickest candidate or slickest debater that we need to elect." 

Friday's CPAC event serves as a precursor to Saturday's critical Republican Party of Florida Presidential straw poll.
