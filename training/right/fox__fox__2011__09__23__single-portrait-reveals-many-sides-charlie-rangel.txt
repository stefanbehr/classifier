A personal, friendly tone oozed through the salutation of the letter dated September 4, 2008. 

"Dear Charlie," began the missive. 

The letter writer noted the addressee had scores of friends and admirers and praised the recipient for his "record of long and highly-decorated service." 

And then suddenly, the manner grew stern. 

"You, along with the Speaker and other leaders of the majority party, have an obligation to help set the pace when it comes to standards of official conduct," scolded the letter. 

On Thursday afternoon, the tenor of House Speaker John Boehner (R-OH) was much more conciliatory than the rebuke he sent to Rep. Charlie Rangel (D-NY) in September 2008. It's tradition to commission a portrait of former chairmen who lead the powerful House Ways and Means Committee. The paintings adorn the walls of the hearing room in the Longworth House Office Building, one of the most-ornate spaces on all of Capitol Hill. And on Thursday, Boehner, along with a host of other key Congressional leaders and hundreds of well-wishers wedged their way into the cavernous room for the unveiling of Rangel's image. About the only person who didn't attend the ceremony was presidential candidate Texas Gov. Rick Perry (R). On Monday, Rangel accidentally crashed a Perry campaign event when he showed up at a Harlem restaurant, not realizing Perry was there. 

In his remarks, John Boehner heaped nothing but praise on Rangel, a stark contrast to the admonishment the Ohio Republican dished out a few years ago. 

"Charlie and I have considered each other friends since I got here," bragged Boehner. "We're proud of your accomplishments." 



Not to be outdone, House Minority Leader Nancy Pelosi (D-CA) gushed about her Democratic colleague who sat just steps away with his three grandsons looking on. 

"Thank you for being an inspiration to us," Pelosi said. 

"Even at times of extraordinary stress, Charlie Rangel has stood very high," declared House Minority Whip Steny Hoyer (D-MD). 

Last fall was a time of extraordinary stress for Rangel. In November, the House Ethics Committee found him guilty of 11 violations. The charges ranged from his improper use of rent-stabilized apartments for campaign purposes, failing to pay income taxes on a vacation home in the Dominican Republic and the use of Congressional stationary to solicit donations for a school of public service in his name at City College of New York. 

Upon the Ethics Committee's findings, the entire House voted 333-77 to discipline Rangel. Then-Speaker Nancy Pelosi summoned Rangel. As Speaker, it was up to Pelosi to confer an ignominious standing on her friend - a status attained by only 22 other House members since the beginning of the republic. 

"Will the gentleman from New York, Mr. Rangel, kindly appear in the well?" Pelosi asked, her voice soft and tentative. 

Pelosi then formally censured Rangel, the highest level of punishment the House has to offer short of expulsion. 

It would be nearly a year before the unveiling of Rangel's portrait in the Ways and Means Committee hearing room. But already, Rangel had joined the dubious ranks of other scandal-plagued former Ways and Means Committee chairmen whose portraits adorn those walls. 

Rep. Wilbur Mills (D-AR) chaired the tax writing committee for 17 years. He stepped aside in 1974 after a drunken escapade with an Argentinean stripper who wound up bobbing around in Washington's Tidal Basin. 

Rep. Dan Rostenkowski (D-IL) served 15 months in jail after using government resources for personal purposes and hiring ghost employees. 

Still, despite his transgressions, Rangel's likeness became part of the Ways and Means Committee collection Thursday. 

"Charr-lee! Charr-lee! Charr-lee! Charr-lee!" chanted the assembled as Rangel rose to speak. 

"My life is a story that anyone can make it," Rangel declared. "From a high school dropout to the chair of this great committee." 

But despite the jubilant atmosphere, even Rangel couldn't escape the ironic juxtaposition of Thursday's fete and his ebb during last year's ethics probe. 

"If anyone wants to know anything about the Ethics Committee, I'm not answering any questions," said Rangel, drawing a laugh from the crowd. "I can't wait to see how the New York Post handles it." 

Congress only salutes chairmen and women of Congressional committees with portraits. Lawmakers sometimes languish for decades without ever seizing the gavel. Rangel served for years as the top Democrat on the elite Ways and Means panel while Democrats toiled in the minority. 

Rangel came to Congress in 1971 when no African Americans chaired committees. But he persevered. The Harlem native who was left for dead on a Korean battlefield graduated to chairman 36 years later when Democrats reclaimed control of the House. 

Rangel's chairmanship was brief. Questions about his conduct began swirling in 2008. In fact, Rangel took the extraordinary step of referring himself to the Ethics Committee in an effort to clear up the controversy. 

It did anything but. 

In March, 2010, Rangel appeared to still control the Ways and Means Committee when Pelosi convened a late night meeting with him in her office to discuss his future. A reporter asked Rangel if he was still chairman as he emerged from the conclave. 

"You bet your life," Rangel snapped. 

But less than 12 hours later, Rangel reversed course, relinquishing his gavel. 

Rep. Sander Levin (D-MI) became chair of the Ways and Means Committee, despite Rangel's contention that he only stepped aside temporarily. Rangel's Congressional limbo was conspicuous as the nameplate designating him as chairman was removed from a Ways and Means office suite in the Capitol. It was never replaced with one bearing Levin's name. 

Republicans seized control of the House in January. And Rangel never got his gavel back. 

Until Thursday night. 

At least on canvas. 

Simmi Knox painted Rangel portrait. It shows Rangel in a dark suit sporting a tie speckled with polka dots. A red handkerchief explodes out of the breast pocket like a party favor. 

In the lower left-hand corner of the painting is a box, revealing seven of Rangel's Army medals: the United Nations Service Medal for Korea, the National Defense Service Medal, the Korean Service Medal, the Korea Defense Service Medal, his Purple Heart, a Bronze Star, and the Marksmanship Badge-Sharpshooter. 

The lower right-hand corner features a stack of books. Jefferson's Manual of House rules, Rangel's autobiography "And I Haven't Had a Bad Day Since," the 2007 Internal Revenue Code and a black booklet on the Ways and Means Committee. 

The medals and books frame the portrait's main attraction. In his hands, Rangel cradles a mahogany gavel. The name "Charles R..." is inscribed on a plate. His full name disappears around the handle's circumference. 

Rangel's ethics woes may have forced Pelosi to take away his gavel last year. But now he's got it back. And others notice. 

"This portrait will be a permanent tribute," said John Boehner at the ceremony. 

A permanent tribute to the man who lost his gavel. Then got it back for posterity. 

Charlie Rangel has always been a figure of extremes. He came from little in Harlem and rose to become a war hero. He was a popular, loved figure in Congress. Then had his colleagues vote to sanction him. 

He went to college even though he came from a family which didn't know anyone who went to college. He became an Assistant U.S. Attorney, a Congressman and then Chairman of the Ways and Means Committee. And then the man in charge of writing tax law failed to pay his taxes. 

Still Thursday afternoon, hundreds of people, Democrats and Republicans alike, poured into a hearing room to laud Rangel and salute him for his time at the helm of the Ways and Means Committee. 

And they got to see him clasp that elusive gavel once more. 

Rangel's portrait will hang alongside former chairmen like Reps. Bill Archer (R-TX) and Robert Doughton (D-NC) - as well as other chairmen with ethics burdens like Wilbur Mills and Dan Rostenkowski. 

The question is how will the public view Rangel? As a tax cheat? Or as a revered Congressman who worked for social justice and is beloved by his colleagues? 

Samuel Butler wrote that "every man's work, whether it be literature or music or pictures or architecture or anything else, is always a portrait of himself." 

And so to is Rangel's portrait. A portrait of himself and all of his dichotomies. Some will spy the depiction of the tax code booklet and recall his tax troubles. Others will observe the military medals and remember his bravery and service. 

Some will see Rangel holding the gavel and note he was the first African American to chair the Ways and Means panel. To others, the gavel is a reminder of what Rangel lost. 

It's a bifurcation. The good and the bad. Just like anyone else. 

And it's all captured on a single canvas.
