ORLANDO, FL- In an effort to regain momentum after Thursday's debate performance that left many Republicans here disappointed, GOP front-runner Governor Rick Perry lashed out at his rivals who have decided to skip Florida's P5 presidential straw poll-- a forum which has now become a significant test for Perry's campaign for the White House . 

"There are a number of candidates who have spurned Florida's straw poll and I think that's a big mistake," Perry said to a group of several hundred GOP delegates who had gathered to hear him speak at an Orlando breakfast event. 

"I have all my hopes on Florida," Perry added. 

The poll is voted on by a convention of over 3,500 GOP delegates from across the battleground state. 

Three Republican presidential contenders have decided not to actively participate in the forum; Mitt Romney , the former governor of Massachusetts, Rep. Michelle Bachmann, R-MN, and John Huntsman, former Utah governor. 

Ahead of the Fox News-Google Debate, many in the Republican Party of Florida meeting in Orlando, expected Perry to easily win this straw poll, but after heated criticism on his illegal immigration stance by his GOP opponents, and his lackluster response some delegates have openly changed their mind on the border state governor. 

During the prayer breakfast Saturday, Perry also seemed to take aim at Romney, when he charged that voters did not want the "slickest candidate." 

"What Americans are looking for isn't the slickest candidate. They are looking for an authentic, principled leader, Perry said. "Think about it. You've seen what happens when our country chooses leaders who emphasize words over deeds."
