In another attempt to brand Mitt Romney a flip-flopper, Rick Perry s campaign has just released a web ad that goes after what it calls Romney shifting support for Obama s education policies. 

The ad called, Romney s Race to the Flop, highlights a Florida town hall where Romney gave some credit to education secretary Arne Duncan and some of Obama s education initiatives, like Race to the Top. It then shows an exchange at the Fox News-Google debate one day later, where the Perry campaign claims Romney backs off what they call a federal takeover of education. 

The Romney campaign responded Tuesday, saying that Romney's full remarks from the town hall reflect his desire to bring education reform back to the state level. 

"This is just another tall tale from Gov. Perry," said Andrea Saul, a spokesperson for the Romney campaign. "Instead of spending time misrepresenting Gov. Romney's words, Gov. Perry should take some time to explain why he thinks conservatives who disagree with him on providing in-state tuition to illegal immigrants 'don't have a heart.'" 

You can see the ad here: 

Watch the latest video at FoxNews.com
