Amid the most recent attacks from the Perry camp accusing Romney of flip-flopping on his view of Obama's stimulus bill, Romney fought back in an exclusive interview with Fox News. 

The Perry ad focuses on one excerpt from Romney's book saying that he believed the stimulus passed in 2009 would "accelerate the timing of the start of the recovery". Following a town hall meeting at Saint Anselm's College in Manchester, New Hampshire, Romney emphatically refuted that notion, saying the quote was taken out of context, that he never backed the Obama stimulus plan, and called Governor Perry little short of a liar. 

"Mr. Perry has a real hard time getting his stories straight. He has proved that when he is talking about TARP, he's proved that when he's talking about Hillary Care, he's proved that when he's talked about his reasons for putting in place a vaccine, he has a hard time with getting the stories straight." 

He went on to say that he thought it was "unusual" that Perry would talk about the stimulus since the Lonestar State's governor accepted "massive" stimulus money to fix the state's budget problems. 

Romney also took aim at Perry over his immigration policy-in particular his DREAM ACT legislation. Perry sparked uproar among his conservative base over his remarks at the debate calling those who did support the program "heartless." Romney has been capitalizing on this perceived political misstep, and told Fox News today that the in-state tuition granted to illegal immigrants is a "$100,000 tax gift" from the people of Texas. He went on to say, "citizens come first and illegal aliens come well behind." 

Governor Rick Perry's ratings have been consistently slipping since his less than stellar debate performance last week in Orlando, and a new Fox News Poll shows Romney reclaiming the frontrunner status with 23 percent to Perry's 19 percent. 

It was Perry's wife who came to her husband's defense this week at a Conservative Club breakfast meeting in Iowa, saying "he's never had a debate class or coach in his life...He's going to be better prepared next time." 

When asked if calls for an expanding Republican field is a concern for the former Massachusetts governor, Romney downplayed the issue saying there will never be 100 percent satisfaction on the part of voters. 

"We are always looking for someone who agrees with us on all issues 100 percent, I don't think that person exists, and I understand I am not perfect, some people don't think I am the ideal guy, but hopefully enough people do." 

In a controversial move, Florida is poised to hold their primary early-as early as January failing to comply with the Republican National Committee calendar. The move which is expected to be officially announced Friday would mean voting could start in as little as four months. Romney responded to news of the calendar shift, saying he thinks there is still ample time for voters to hear from the candidates and doesn't see it as an obstacle for his campaign 

"I don't have any problem with the calendar people work on, my job is to have a message that connects with people, we'll be ready whenever the process starts." 

Watch the latest video at FoxNews.com
