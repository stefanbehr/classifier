President Obama said that he's the "underdog" in the 2012 race in an interview with ABC News' George Stephanopoulos Monday. 

Stephanopoulos asked about a new ABC/Washington Post poll out that said the majority of Americans - 55 percent - believe that Obama will be a one-term president and questioned if that makes Obama the underdog. 

Without hesitation, the president immediately responded "absolutely." 

"[G]iven the economy, there's no doubt that, you know, whatever happens on your watch," he added. 

Noting he responded quickly to that notion, Obama said, "I don't mind. I'm used to being an underdog. And -- and I think that, at the end of the day, though, what people are going to say is, who's got a vision for the future that can actually help ordinary families recapture that American dream?"
