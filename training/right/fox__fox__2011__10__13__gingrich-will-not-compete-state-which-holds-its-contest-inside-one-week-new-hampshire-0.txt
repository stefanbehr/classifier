Former Pennsylvania Sen. Rick Santorum and Minnesota Rep. Michele Bachmann have joined former Utah Gov. Jon Huntsman and former House Speaker Newt Gingrich in pledging to boycott the Nevada caucuses because the state is daring New Hampshire to consider scheduling its first-in-the-nation primary in December. 

The pledge was prompted by New Hampshire activists upset over Nevada for scheduling its caucus on Jan. 14, earlier than New Hampshire's planned primary, traditionally the first in the nation. New Hampshire Secretary of State William Gardner wants Nevada to move its primary back. 

"Nevada's move has potentially forced the other early states to have primary's near Christmas - and that destroys the primary process, Santorum said. I firmly believe that we must protect New Hampshire, the other early primary states and the proven presidential primary process. 

"To be clear, I will not campaign in Nevada nor participate in the Nevada caucus if it doesn't move its primary date." 

Gingrich made a similar pledge earlier. 

"I have always supported New Hampshire's First in the Nation Primary," he said in a statement released Thursday afternoon. "Now, as a candidate for president, I am committed to competing in and maintaining the first in the nation status of the New Hampshire primary . Therefore, I will not compete in a state which holds its contest inside of one week of New Hampshire." 

New Hampshire is being squeezed by Florida's decision to hold its presidential primary on January 31, which forced South Carolina, Nevada and Iowa to all jump up into January. 

New Hampshire's Secretary of State William Gardner wants Nevada to move its primary back from January 14, and is threatening to Hold the Granite State's primary as early as Dec. 6. 

Rick Perry's campaign said late Thursday that it wouldn't commit to a boycot of Nevada but still hoped the party would find a resolution preserving New Hampshire's status. 

Governor Perry respects and supports the long tradition of New Hampshire having the first primary in the nation," his campaign manager, Rob Johnson , said. "The movement of early primaries and caucuses has pitted states against each other and will only hurt the political process." 

Fox News' Jake Gibson contributed to this report.
