Herman Cain's communications director slept a total of two hours Wednesday night, but J.D. Gordon likes it that way. His phone is constantly ringing and, as we spoke Thursday morning, he was trying to fit in a power nap ahead of a busy interview schedule. "Right now we're just trying to stay afloat," he said. 

Since the surge in polls, the Cain campaign has received a flood of calls from the press and volunteers alike. According to Cain's Iowa operation, the number of members in their state Facebook group has increased 80 percent in the last week. 

"We have four people on staff in Iowa. We're the leanest campaign in the state, I would venture to say nationally as well," said Iowa campaign staffer Lisa Lockwood. "Mr. Cain is running a business here, just like he always has. We're lean and strong, and we're exercising very hard to further the metaphor," she said, laughing. 

In total, Herman Cain has just 35 people on the payroll nationally, which has some political operatives in early primary states raising questions about whether the campaign can translate the momentum into something concrete. 



Can Cain Organize in South Carolina? 

"He's surging at the right time nationally but it's becoming noticeable among activists here in South Carolina that the campaign is lacking an organized presence -- partly due to resources and partly as a result of an inexperienced national team," said a top Republican source in South Carolina. 

"Typically in South Carolina you would hire state directors that have been party executives, campaign directors in presidential races. There needs to be a bigger presence at events, parades, and we haven't seen that," he said, although he noted it's not too late for the Cain camp to organize in the state, given Texas Governor Rick Perry's recent "fall-off" in the debates. 

William Head, who is Cain's South Carolina director and currently the state's only paid staff member, said, "We are looking to win South Carolina. I want to stress -- we look to be very competitive. We're bringing on field staff now, should be in place within the end of the week, in just a matter of days." 

The West Columbia headquarters opened its doors on October 6, and Head, a South Carolina native who previously worked for the Georgia Republican party , described the interest in Mr. Cain as a true grassroots movement. "Normally a campaign is doing outreach, reaching out to people," said Head, "People are flooding in; we are almost out of materials." 

"Before, people would say I really like him but I don't feel like he's in position [to win]," said Head, "There's a tipping point in every race. I feel that tipping point is very close." 

Can Cain benefit from early trips to Iowa and New Hampshire? 

For voters in early states, it's often easier to find Herman Cain on television than in their area of the state. 

Cain visited the state of Iowa often before the straw poll, but Trudy Caviness, who chairs the Wapello County Republicans in the Southeast quadrant of the state, says, "Right now I see more activity from other campaigns, even the Romney campaign."It's not just candidate appearances Caviness is referring to, it's also contact from members of the campaign staff. Since the beginning of the year, Herman Cain has made 23 visits to Iowa for a total of 35 days; in New Hampshire, he's clocked 14 visits for a total of 18 days; and in South Carolina, he's made 10 visits for a total of 11 days. 



But Caviness also called Herman Cain a "phenomenon" who may be able to do well in the state, despite not meeting the usual Iowa prerequisite of being on the ground in the first-in-the-nation caucus state early, often and consistently. 

"I think Herman Cain has a lot of support in Iowa, he built a lot of support early on," she said. "I talked to a reporter last March and commented on Herman Cain. The reporter said, I didn't know anybody was listening.' But he spent a lot of time in Iowa at that time. He built something early. And now hearing him more in debates, I think people are getting stronger for him. That's why I think he's a phenomenon. I don't know how much groundwork he needs. We've got so many people who will be for him at the caucuses even though no one is calling asking us Will you be for him?' This will be unusual year." 

"Two people this last week called me to get in touch with the Herman Cain campaign operation," she said. "I know people are excited about him. There's a lot of interest for him." 

A similar level of interest in Herman Cain is developing in what should be Michele Bachmann country. Greg Tagtow, of the county Republicans in Black Hawk County, which includes Bachmann's hometown of Waterloo, said there's been a recent groundswell. 

"The more people hear about him, the more they take interest in him," he said, calling the race wide open, although he did note, "If a candidate hopes to get traction here in Iowa, they have to be in Iowa so people can get up close attention." 

Charlie Spano, who heads up Cain's New Hampshire field operations and is one of two paid staffers there, notes that the candidate has been in the Granite State over twenty times in the past 20 months. Spano is currently planning a state bus tour for Mr. Cain, although the timing is dependent on when New Hampshire settles on a primary date. 

"After the Florida Straw Poll, this has been a rapidly ascending rocket," Spano said. "After the debate last night [Tuesday], we're probably somewhere halfway to the moon right now, and I expect it only to get higher. That's why we're getting more staff. Our volunteer staff has tripled." 

Organizing with the Internet in Iowa 

Iowa staffer Lisa Lockwood says the state team hopes to harness the grassroots energy using digital tools as a way to supplement their organizational leanness. 

"We haven't gotten Cain's schedule yet but we're hoping to do a live Skype with Iowan supporters and county leaders," she said. "We're really excited about that. It's the brain child of a volunteer who worked on the Huckabee campaign last time around. He called us and came to us with the whole event proposal." 

The state operation is also trying to put together a new digital platform, called the Herman Cain Express, that will consolidate the various social networking groups, "not to replace Facebook activity," Lockwood said, "but to bring everyone together in one stop. It helps us track state activity, see who is where, where we have good coverage." 

"It's a big state geographically and that's why virtual grassroots organization is so exciting to us. There are only four of us, and one of us is the phone bank operator. That leaves only three of us to go to county chair meetings and whatnot, so that's why we're trying to gather everyone together in one location online." 

The virtual town hall is still in the works; Lockwood says she hopes to launch the event "very soon." 

"We pitched it to the national campaign, but have to put in place collateral materials before we push the go' button on that, but we're scrambling as fast as we can, and meanwhile we continue to meet with groups and organizations." 

Will the money be there? 

With the deadline to announce third quarter fundraising right around the corner on October 15, J.D. Gordon says he doesn't have the final figure yet, although he says the campaign is in "good shape." He says they don't have as much money as the other candidates, but "we're not in debt." 

It's been reported that Cain is expected to announce fundraising numbers somewhere in the neighborhood of $8 million dollars for the third quarter. 

That would be a significant increase from what he raised in the second quarter, when he reported raising $2.5M and spending $2 million, with $480 thousand cash on hand.
