The U.S. has just foiled a plot where Iranian agents aimed to assassinate the Saudi ambassador while he dined at his favorite Washington restaurant. 

As a result, the State Department issued a worldwide terror alert advising Americans traveling overseas to gird against potential terror attacks. 

On Thursday, a federal court convicted three North Carolina men of plotting attacks abroad as well as the Marine Corps base in Quantico, VA, near Washington. 

Security officials braced the nation for a possible terror strike last month surrounding the ten-year anniversary of September 11th. 

So what should the U.S. be on the lookout for? Where is the next threat coming from? The next bin Laden or al-Awlaki? Something in Yemen? A sleeper cell from Northern Virginia? 

If you ask Defense Secretary Leon Panetta, he'll tell you to look no further than under the Capitol Dome. 

"I think that one of the great national security threats is the dysfunctionality of the Congress and its inability to confront the issues that we face now," said Panetta at a House Armed Services Committee hearing Thursday. 

You thought it was bad enough for Standard Poor's to downgrade the nation's credit worthiness? And now the Secretary of Defense accuses Congress of being a national security threat? 

Remember, national security threats don't always come in the form of IED's, shoulder-fired SA7's or radicalized Muslims in Sana'a. 

After all, it's the performance of the House and Senate which resulted in S P's decision to diminish the nation's credit rating. And it's the same set of circumstances which produced Panetta's stark assessment of the body he once served in for 16 years as a Congressman from California. 

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 



In August, Congress approved a debt ceiling agreement which requires the bipartisan, bicameral Congressional supercommittee to conjure up $1.2 trillion in spending cuts by Thanksgiving. Failure to reach an accord triggers $1.2 trillion in mandatory budget cuts by 2013, most of which could be cleaved from the Pentagon's budget. 

That scenario alarms lawmakers of both parties. But the fundamental paralysis which stymies Congress has political observers wondering how lawmakers can carve a deal. Especially when defense spending is already on the chopping block. 

"It's going to take us to the edge," said Panetta. "And if suddenly on top of that we face additional cuts or if the sequester goes into effect and it doubles the number of cuts, then it'll truly devastate our national defense." 

If there's one new vocabulary word everyone is learning this fall, it's "sequester." 

A judge sometimes sequesters juries, isolating them from outside contact which could potentially taint their deliberations. 

In Congressional terms, "sequestration" is a budgetary tool used to "quarantine" a chunk of money from use. That's what Panetta and others worry will happen if the supercommittee stumbles. It's especially tough if half a trillion of that money comes from defense. 

So what are Congressional hawks doing? Defending defense. 

"If there's failure on the part of the supercommittee, then we will be amongst the first on the floor to nullify that provision," said Sen. John McCain (R-AZ). 

House Armed Services Committee Chairman Buck McKeon (R-CA) was even more stark in what he thought the supercommittee should do if it can't forge an agreement. 

"That's their problem," snapped McKeon, who is already dubious about more than $400 billion in Pentagon spending reductions over the next decade. "We're not accepting any more cuts in defense." 

Tell that to Rep. Barney Frank (D-MA) 

"They're wrong," said Frank. "They need to tell me where else they'll cut." 

Frank has called for defense spending cuts for years. He believes the military is overextended. So he's pushing the supercommittee to consider the Pentagon as a major venue from which to trim spending. Later this month, Frank is scheduled to hold a town hall meeting in Taunton, MA where he'll tell his constituents why he believes the military needs to be in the crosshairs. 

"We're in a zero sum situation," Frank said. "If you don't cut the military, then you cut Medicare. Or you cut Social Security." 

And this is why Panetta fears Congress. He believes this gridlock imperils the United States. After all, Panetta has lived this for most of his career. Panetta was House Budget Committee Chairman during an abbreviated government shutdown over spending priorities in 1990. He was White House Chief of Staff during the partial government shutdowns of 1995 and 1996. At that point, Panetta watched as President Clinton and then-House Speaker Newt Gingrich (R-GA) sparred over spending, which provoked the shutdowns. But Panetta's doesn't need to recall the 1990s to find a Congress at an impasse. The House and Senate failed to agree to spending bills last year. Then lawmakers had to yank an omnibus spending package just before Christmas. That precipitated multiple rounds of spending spats this year, as the government careened to the precipice of full-blown shutdowns. These closures would have dwarfed the shutdowns of 1990s in size and scope. Panetta then watched Congress narrowly approve a pact to raise the debt ceiling. 

It's that deal that created the supercommittee in the first place. 

This comes as formal committee recommendations as to where lawmakers should cut are due before the budget control panel today. 

"The most important thing I believe is that the committee (has) a real chance to succeed and reach compromise. We are all working towards that end," said House Minority Whip Steny Hoyer (D-MD). 

But what happens if there is no package by the supercommittee's statutory November 23? Does the market fall? Do S P, Moody's and Fitch downgrade the U.S.? 

Many would perceive economic, fiscal and monetary instability in the U.S. as a true national security threat. 

So the question is how can the supercommittee narrow the chasms between the sides. 

"This committee has a difficult task to try to get members to come off from positions they've taken in the past," said Hoyer earlier in the week. 

That's the essential challenge facing the supercommittee. 

As Panetta testified Thursday, U.S. Capitol Police arrested eight demonstrators for disrupting the hearing. A team of plainclothes and uniformed officers lined the Congressional hearing room or stood post in the corridor just outside to protect Panetta. 

In his role as Defense Secretary, it's unlikely that Panetta would find safety amid certain factions in Afghanistan, Pakistan or Yemen. Yet by traveling to Capitol Hill Thursday, Panetta stepped directly into the belly of a beast which he fears is a legitimate national security threat. 

"We have met the enemy and he is us," said the comic strip character Pogo. 

And it's now up to Congress to prove Panetta wrong.
