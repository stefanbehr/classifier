The Obama administration is sending approximately 100 U.S. troops into central Africa to provide assistance to regional armies fighting the Lord's Resistance Army (LRA) and to help remove its leader Joseph Kony. According to a letter the president sent to House Speaker John Boehner , R-Ohio, the troops will serve in advisory role in an attempt to help local forces defeat the LRA. He adds that the special operations forces will only engage in combat if they need to defend themselves. 

"... I have authorized a small number of combat-equipped U.S. forces to deploy to central Africa to provide assistance to regional forces that are working toward the removal of Joseph Kony from the battlefield," the president's letter reads. "I believe that deploying these U.S. Armed Forces furthers U.S. national security interests and foreign policy and will be a significant contribution toward counter-LRA efforts in central Africa." 

The White House says the first troops went into Uganda on Wednesday and that over the next month, more will deploy into South Sudan, the Central African Republic and the Republic of the Congo if those nations agree. The president says in the letter that helping defeat Kony and the LRA is in the national security interest of the U.S. and that the mission is permissible within the parameters of the War Powers Act. 

"I believe that deploying these U.S. Armed Forces furthers U.S. national security interests and foreign policy and will be a significant contribution toward counter-LRA efforts in central Africa," the letter reads. 

The administration says the LRA continues to commit atrocities in the region that impact regional security. 

CLICK HERE TO READ THE FULL LETTER
