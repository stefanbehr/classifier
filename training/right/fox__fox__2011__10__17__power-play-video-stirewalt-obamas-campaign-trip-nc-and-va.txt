Chris Stirewalt heads to the big board to give us a rundown of President Obama's three day campaign trip to North Carolina and Virginia. 

In 2008, Obama was the first Democrat to win North Carolina since Jimmy Carter in 1976 and the first Democrat to win Virginia since Lyndon Johnson in 1964. 

However, roughly 127,000 jobs have been lost in North Carolina since 2008, so the president could have an uphill climb in Bus Force One' over the next few days. 

Watch the latest video at FoxNews.com
