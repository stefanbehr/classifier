Every day I get in the queue...To get on the bus that takes me to you.. 

- "Magic Bus" - The Who, 1968 

Lawmakers are a lot The Who's Roger Daltrey Pete Townshend these days. They're standing in the "queue," about to board some sort of "bus" that they hope will finally finish off this year's fervent spending fights once and for all. 

Only the "bus" they're waiting for could either be an "omnibus" or a "minibus." 

These are the two vehicles Congressional leaders are potentially eyeing to fund the government for the rest of 2011 and most of 2012. 

And regardless of which "bus" they choose, it's doubtful many lawmakers will find either one to be magical. 

+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 



It was just a couple of weeks ago when the House and Senate approved yet another stopgap spending bill to keep money flowing to federal agencies and departments, sidestepping a government shutdown. Congress and the White House have danced this elaborate Kabuki on multiple occasions all year long, dodging possible government shutdowns in February, March, April and September. Lawmakers again face what they hope is a final hurdle to wrap up spending bills for this fiscal year. 

But this is far from over. 

Let's start with the way this is supposed to work, but rarely does. 

Both houses of Congress are charged with approving and syncing up 12 separate spending bills to operate the federal government each year. Lawmakers are supposed to wrap this up by October 1, the start of the government's new fiscal year. But to date, the House has only approved six of those spending packages. 

So to avert a government shutdown, lawmakers crafted an interim spending bill, known as a "Continuing Resolution" or "CR," to keep the government's lights on through November 18. 

Congress could certainly approve the remainder of the individual spending bills which operate each sector of the federal government. But that's unlikely. Time is running short and most attention on Capitol Hill is now focused on the rush for the supercommittee to find significant spending cuts and introduce a blueprint by November 23. 

So what options does that leave Congress with to keep the government in business? Either an omnibus spending bill or a minibus spending bill. 

Under the typical legislative process, most bills are like a singular car, transporting a singular driver. And just like a driver snarled in Washington's legendary traffic on the Beltway, a stand-alone bill often finds itself stuck in a Congressional jam as well. 

But an omnibus or minibus spending bill is the parliamentary equivalent of public transportation. They efficiently move a group of legislative items from one point to another together. In a way, they are "busses," just like what The Who sang about. 

An "omnibus" is where the remaining spending items for the year are encased in one gigantic bill. Rank-and-file lawmakers must either vote for or against the entire package. A "minibus" is simply a smaller version of the same where leaders glom together a handful of particular spending bills. 

Either option is speedier than considering each bill unilaterally. But it does impose a "take it or leave it" choice on lawmakers. For instance, a lawmaker could feel so strongly about an individual provision in the State and Foreign Operations bill that he or she may vote no on the entire bill. But what if Congressional leaders knotted the State and Foreign Operations package to the Agriculture spending bill which the lawmaker really likes? Does that lawmaker vote no on the entire anthology? Probably not because he or she doesn't want to jeopardize the agriculture programs they like. 

Congressional leaders have yet to decide whether to drive an omnibus or minibus. House Speaker John Boehner (R-OH) said he was "reluctant to even consider the idea of an omnibus." And in an interview with Roll Call, House Majority Leader Eric Cantor (R-VA) indicated he was "not in favor of omnibuses." 

With good reason. If House Republicans engineer either an omnibus or even a minibus spending bill, they'll find themselves violating the "Pledge to America." That's the electoral document Republicans unveiled last year, promising to run the House much differently than the Democrats. 

One section of the Pledge is titled "Advance Legislative Issues One at a Time." Under that heading, House Republicans vowed the following: 

"We will end the practice of packaging unpopular bills with "must-pass" legislation to circumvent the will of the American people. Instead, we will advance major legislation one issue at a time." 

Does this give heartburn to some conservatives who signed onto the pledge? Will that be enough to prevent lawmakers from voting for the bill if it comes to that? 

Certainly some will balk. And a percentage of conservatives aren't expected to vote for an omnibus or minibus, no matter what. Many of them are still incensed that the spending figure for the new fiscal year, $1.043 trillion, is $24 billion above the budget propounded by Rep. Paul Ryan (R-WI). The total amount of spending ballooned by $24 billion as part of the debt ceiling agreement approved by Congress in August. Conservatives endorsed the Ryan budget plan but many voted against the debt agreement. So anything that doesn't trim spending further is unlikely to garner the votes of skeptical Republicans. 

That means House Republicans may have to court Democrats on an omnibus or minibus. 

There are problems there, too. 

As if the dollar figures aren't enough to scrap over, conservatives are demanding that Congressional leaders attach scores of "riders" to a catch-all spending bill, which could impact policy. Riders are "add-ons" to legislation which order federal agencies and departments to implement certain polices. Many Republican-backed riders include curbs on environmental rules, efforts to defund Planned Parenthood and the Corporation for Public Broadcasting, an erosion of money for the new health care law, the prohibition of needle exchange programs and a call to return to the "Mexico City" language. The "Mexico City" rule prevents Non-Governmental Organizations which receive federal dollars from offering abortion services. 

Republican leaders won't get the help of many Democrats if policy riders like those are left in tact. So the House GOP brass has a decision to make: what riders can it cast aside to court Democratic votes? 

Regardless, no one knows whether lawmakers will elect to catch an omnibus or minibus to finish the spending packages. 

"I think it will all come down to what is doable in the Senate," said Eric Cantor earlier this month. 

And so far, the Senate could be the oval where lawmakers road test one vehicle. 

Senate Majority Leader Harry Reid (D-NV) is now steering a $128 billion minibus through his chamber. This measure blends the Transportation, Housing Urban Development bill with the Commerce-Justice-Science bill and the Agriculture spending measure. If Reid's able to drive this minibus across the finish line, the House could follow suit and combine some bills, but not all, into a similar minibus. 

Still, one House Republican aide hinted that lawmakers may still have to write yet another short-term "CR" to avoid a shutdown if all of the spending bills aren't done by November 18, whether they be in a minibus or an omnibus. 

"It still will be cutting it close," said the aide "There may be a need for a short-term extension." 

The problem is that Cantor recently told reporters that he hoped the House wouldn't have to consider any more CR's, but didn't rule it out. Conservative Republicans may insist on additional, commiserate cuts if the House were to take up another interim measure. And cuts below the $1.043 trillion level for this fiscal year would blow the debt ceiling agreement out the water. 

"I think that would reflect bad faith if they tried to go below that," said a House Democratic aide of the $1.043 trillion spending figure mandated by the debt limit law. 

Either way, lawmakers may soon find themselves at a Congressional bus terminal, deciding which bus to catch: omni or mini. 

This comes as Sen. John McCain (R-AZ) criticized President Obama for his bus tour that focused on fixing the economy. 

"I've never seen an uglier bus than the Canadian one he's traveling around on," McCain said. Which could only be trumped by the looks of an omnibus or minibus spending package to avert a government shutdown in November.
