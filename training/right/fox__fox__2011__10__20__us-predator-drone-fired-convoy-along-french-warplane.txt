A US Defense official confirms to Fox that an armed US Predator drone fired munitions along with French fighter plane - both fired on a "large convoy" leaving Qaddafi's hometown of Sirte. 

A French Defense official said 80 vehicles were in the convoy which was heading toward Bani Walid. 

Witnesses at the scene said Qaddafi ran and hid after the convoy was hit and was later dragged out of his hiding place and shot in the head with a 9 millimeter pistol.
