Bachmann campaign manager Keith Nahigian tells Fox News, "We have a great team in New Hampshire we have not been notified that anyone is leaving the campaign." 

This is in reaction to local reports suggesting all of Bachmann's staffers in the Granite State had resigned. 



Nahigian adds, "We look forward to spending more time in the Granite State between now and the primary. At this time, our main focus is Iowa and we are building on our efforts there. Michele will spend the majority of her time in Iowa, doing what she does better than all the other candidates - retail politics - leading up to the all important caucuses." 

However, FOX News has confirmed that at least one Bachmann staffer in New Hampshire, Caroline Gilger, has quit the Bachmann campaign and is joining the Perry campaign. 

The Bachmann campaign has been aggressively shifting emphasis to Iowa for the last several weeks. That process has stripped the New Hampshire Bachmann campaign of resources. 

Numerous New Hampshire GOP officials who are not affiliated with any campaign say Bachmann staffers were openly discussing a mass resignation a month ago. 

Bachmann spokesperson Alice Stewart says the campaign did not had actual full time staff in NH, rather the campaign has had weekly contracts with 6-10 individuals only during Bachmann's visits to the Granite State. 

Bachmann's best known strategist, Ed Rollins, stepped down a month ago and Bachmann's well known pollster Ed Goeas has reduced his role. 

As recently as last week interim campaign manager Keith Nahigian told GOP officials in NH that Bachmann looked forward to a more aggressive Granite State campaign. At the same time Bachmann insiders admit resources were being shifted out of NH in the theory that without a triumphant come-from-behind win in Iowa's leadoff caucuses, she would likely be unviable in subsequent contests. 

FOX News producer Jake Gibson contributed to this report.
