The folks at WHO-AM in Des Moines like to refer to their radio station as a blow-torch. The station's powerful signal reaches much of Iowa. 

Today, talk-show host Jan Mickelson used that blow-torch to apply some heat to Republican presidential candidate Herman Cain . 

Mickelson told his listeners he concluded Cain was "basically pro-choice" after playing for his audience a clip of Wednesday's Piers Morgan show. In the clip, Morgan repeatedly asks Cain to clarify his position on abortion. 

According to transcripts of the show, Cain said, "I believe that life begins at conception. And abortion under no circumstances." 

Later in the interview when Morgan asks about exceptions for rape and incest, Cain appears to step back from that "abortion under no circumstances" position: 

"I can have an opinion on an issue without it being a directive on the nation. The government shouldn't be trying to tell people everything to do, especially when it comes to social decisions that they need to make." 

Mickelson told his listeners that it was clear Cain took several positions on abortion in the few minutes dedicated to the topic. Mickelson's verdict was that Cain is pro-choice, not pro-life as he insists. 

Additionally, Mickelson tells Fox News that Cain's attempt to clarify his abortion stance on the Morgan show was incoherent. 

Cain yesterday put out a statement trying to further clarify his position. In part it reads, "I understood the thrust of the question to ask whether that I, as president, would simply order' people to not seek an abortion... My answer was focused on the role of the President. The President has no constitutional authority to order any such action by anyone. That was the point I was trying to convey."Cain may well have a lot of damage-control to do in Iowa over this lingering situation. 

Social conservatives dominate the ranks of Iowa Republicans. Many politically active GOP'ers scour candidates' records, looking for inconsistencies on the abortion question. 

Couple that with Mickelson's pronouncement that Cain is "pro-choice" and the fact that Cain has not been in Iowa since August...and the current Iowa front-runner may have a sizable problem on his hands.
