President Obama's growing impatience with Congress' refusal to "pass this bill now" has resulted in a new administrative approach -- bypassing Congress by taking actions that don't require congressional support and insisting that "We Can't Wait" for any more delays on Capitol Hill . 

"We can't wait for Congress to act so we're going to take the steps we can take," White House Communications Director Dan Pfeiffer told reporters on Monday ahead of the president's trip to Las Vegas where he will unveil the first plank -- changes to a home financing program to help owners with underwater mortgages. 

Ahead of that trip, a senior adviser to the president told Fox News that the push is part of something much larger than merely getting the the American Jobs Act through a stalled Congress. 

White House aides are now culling all sorts of stuff to see what kind of presidential authority they have at various Cabinet departments and agencies to see what can we implement without Congress, the adviser said. 

According to the source familiar with the plan, the view in the West Wing is that the American people want to see Congress and the administration do something, and Republicans are on wrong side of the debate. 

But even before White House officials were finished unveiling the plan to reporters participating in a conference call, Republicans were having a field day playing off the new motto. 

House Speaker John Boehner , R-Ohio, fired off a message on his Twitter account: 

"#WeCantWait for POTUS Democrats to work with Republicans to find common ground #4jobs." 

#WeCantWait for the Senate to pass the #Forgotten15 jobs bills passed by the House," tweeted Rep. Peter Roskam, R-Ill. 

"#WeCantWait for a President who doesn t campaign on the taxpayer dime," tweeted the Republican National Committee . 

Acknowledging that recent Fox News and other polling shows the president suffering from a low approval rating when it comes to handling the economy, the adviser who spoke to Fox News suggested that Republicans ought not to be writing home just yet about a failure to cooperate. 

We re in the 40s, they re in the 14s, the aide said. 

White House spokesman Jay Carney added that the actions by the president are not a substitute for congressional action, but the executive order highlights previous moves, and the White House will push Congress to "wake up" and realize action is needed on the economy 

White House officials say that while they take this new tactic, they have not abandoned their efforts to get the American Jobs Act passed through Congress, whether as a whole or piece by piece. The steps the president is announcing will supplement that legislation. Each week, the administration will lay out an executive action aimed at improving the economy. 



The president begins with a trip to Nevada; the state with the highest unemployment rate in the nation, 13.4 percent, as well as the nation's highest number of foreclosures and underwater properties. 



While in Las Vegas on Monday, the president was to announce his plan to allow homeowners with little or no equity in their homes to take advantage of low interest rates and qualify for refinancing that was not previously available. The plan -- an expansion of the Home Affordable Refinancing Program -- would ease refinancing costs and completely remove limits on how far underwater a person can be on his or her mortgage to qualify. 



The administration emphasized this new plan will only apply to those who are current on their payments. Officials say they are trying to reach as many borrowers as possible who could benefit from the lower mortgage rates today, but exactly how many are eligible is uncertain, as is the number of those who would partake in the plan. 



There is no silver bullet, Carney noted, adding that the 125 percent loan-to-value ratio is all too common in Las Vegas and elsewhere but HARP has already helped nearly 1 million homeowners. 

But Rep. Patrick McHenry, R-N.C., said Treasury Department data show that HARP helped 838,000 homeowners to refinance after the administration estimated 4 million to 5 million borrowers would benefit. 

"When it comes to this administration s housing policies, the time to act has long since passed. More than 6 million Americans are behind on their mortgage payments or facing foreclosure and the policies of this president have done more harm than good, McHenry said. 

With the president headed on a Western U.S. tour that includes a fundraiser at the Bellagio Hotel before a trip to California that includes two fundraisers in Los Angeles, Republicans are also driving home the campaign aspect of his trip. 

The RNC released a web video excoriating the president while RNC Chairman Reince Priebus said, "The president is back to doing what he does best-raising money to save his own job. ... Instead of focusing on getting the 14 million unemployed Americans back to work, he's focusing on protecting his own. By the time he returns, President Obama will have done more than 50 fundraisers. It's abundantly clear by Air Force One's travel log where his priorities are."
