Fresh off of new polls placing him third behind Mitt Romney and Herman Cain , GOP presidential hopeful Newt Gingrich told reporters in Washington, D.C. "the odds are pretty good I'll be the nominee." 

The latest Fox News poll released Wednesday shows the former House Speaker has quadrupled his support since August. He now stands in third place with 12 percent -- edging out Rick Perry at 10 percent -- among Republican primary voters. 

"We have the tortoise campaign," the presidential hopeful said. "I've seen several rabbits run by and then fall asleep, so now we just want to keep moving forward." 

Gingrich attributed his rise to his performance at the debates, and said he looked forward to rolling out new proposals for the American public. 

One such initiative concerns "brain science," something Gingrich says is "probably the largest single opportunity to save money in the next 25 years." Gingrich claims "Alzheimer's alone costs one and a half times the current federal debt." His new plan would focus not only on Alzheimer's, but also on autism, Parkinson's, mental health and "systems of learning." 

Another Gingrich proposal in the works concerns the financial future of college students. "We're going to launch a very big project for students to give them the right to have their own personal social security safety account," he said. "We're going to try to go to every campus we can in Iowa, New Hampshire, and South Carolina to start that process. So I think you're going to see a very idea oriented, very positive campaign in the next 30 days." 

While some campaigns like Congresswoman Michele Bachman's are dialing back staff, Gingrich just added three new full-time hires to his New Hampshire operation, and he indicated that more will be announced soon. 

"We're opening offices in New Hampshire, South Carolina, and Iowa," Gingrich said. "And I think we probably have a very good chance to finish in the top two or three in New Hampshire and Iowa. I think we'll probably win South Carolina." 

Gingrich's presidential ambitions suffered a major blow in June when his entire senior staff abandoned him; although they left for a variety of reasons, the turning point was a two-week Greek cruise he decided to take with his wife, which came to symbolize for critics his lack of interest in traditional boots-on-the-ground campaigning in early states. 

And as if to challenge those who questioned his work ethic, Gingrich made a point today to note the amount of time he was spending on the road. 

"We just had two great days in Iowa and a great day in New Hampshire. We're on the road now. Yesterday was the ninth straight day we've done it. So, you know, it's a process of working hard." 

The former House Speaker made a detour to Washington Wednesday to recognize the work of Randy Evans, an Atlanta-based lawyer specializing in government ethics, with whom he has worked over the course of the past three decades. The Republican Lawyers Association was holding an event naming Evans "Lawyer of the Year."
