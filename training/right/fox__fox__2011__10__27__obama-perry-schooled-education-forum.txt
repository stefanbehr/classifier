Two days after President Obama said he would use an executive order to ease student loans, Republican candidate Michele Bachmann slammed him for the move at an educational forum held by Newscorp and the College Board Thursday night in New York. 

Speaking via satellite, she cited Obama's decision as an "abuse of power" that would prompt people to avoid financial responsibility for their loans. In addition, she says that putting the burden of student loan debt onto the taxpayer is a "moral hazard." 

GOP candidates Newt Gingrich , Rick Santorum , and Herman Cain also took part in the forum, and there was consensus among all the presidential hopefuls that education is not a federal issue, but a state one. 

"I believe that if a state wants to help with college education, that they should do that," Herman Cain said. "You have people living within communities within states that are willing to help fund those kinds of programs." 

Cain said he believes that education issues are best tackled at a local level. "The people within the state, the people within the communities" have the resources and should bear the responsibility, he said. The rest of the GOP field was invited to participate, but notably missing were former Massachusetts Governor Mitt Romney and Texas Governor Rick Perry . While Romney went unscathed during the discussions, Perry took heat for comments made by his campaign staff saying he may skip some debates in the fall. 

Former Senator Rick Santorum said that he welcomes the American public "digging deeper" into his own background and finding out where he stands on issues. "I would never skip a debate. I would never skip an opportunity to let the American public know what I think about these issues," he said. He charged that this was Perry attempting to "hide behind 15 to 20 million dollars of slick ads." 

A second wave of criticism for Governor Perry came from former Speaker Newt Gingrich who said that to skip the debates would be "an enormous" mistake. 

"Why would any Republican want to vote for someone who can't stand on the same platform as us, and the thought that he is then going to stand on the platform against Obama? I think it would just define him out of the race."
