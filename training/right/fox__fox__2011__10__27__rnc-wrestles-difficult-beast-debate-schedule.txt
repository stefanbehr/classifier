Sixteen scheduled debates in 12 weeks: that's what Republican presidential hopefuls are facing between mid-November and the end of January. Texas Gov. Rick Perry's campaign has already announced that the former frontrunner may opt out of some of the contests, and other candidates particularly former House Speaker Newt Gingrich , have complained about the "bickering" encouraged by some debate hosts. 

The Republican National Committee had hoped to avoid this situation with an effort to sanction debates and limit the number of candidate confrontations. 

RNC Communications Director Sean Spicer calls the debate schedule a "difficult beast" the party is trying to "get its arms around." 

But the reality may be that the RNC got into the game too late. 

In April of this year, long before the current field took shape, the party proposed having a series of debates that would double as fundraisers for the RNC and provide campaigns the opportunity to limit their exposure by declaring that they would participate in only sponsored events. That plan fell apart and left the candidates to fend for themselves. 

Spicer says the RNC is looking to sanction one debate a month from here on out, but ultimately "it's up to the campaigns" which debates they participate in. 

The Republican Party continues to sanction new debates, including a showdown hosted by Oregon Public Radio on March 19, 2012, but no campaign so far has agreed to limit their participation to sanctioned events.
