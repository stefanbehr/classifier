PELLA, IA -- A forum on the top issue of the 2012 election, in one of the most important states in the GOP primary, drew five candidates and more than 500 Iowans but Hawkeye State front-runners Herman Cain and Mitt Romney didn't show, perhaps leaving an opening for another candidate to make inroads in the first-in-the-nation caucus state. 

Reps. Ron Paul and Michele Bachmann , former House Speaker Newt Gingrich , former Pennsylvania Sen. Rick Santorum and Texas Governor Rick Perry all spoke for fifteen minutes each at a manufacturing jobs forum hosted by Iowa's Republican Gov. Terry Branstad. Gingrich said he couldn't understand how a candidate could miss the forum. 

"You have to go ask the candidates who decided not to be here," Gingrich said in an interview with Fox News. "I don't understand how somebody can think they are going to run for president and not show up to talk about the importance of manufacturing and not offer leadership and come out into the open." 

He then went on to criticize candidates for raising money instead of presenting ideas. 

"I don't think raising money is a substitute for developing issues and talking to people," said Newt Gingrich . 

Neither missing candidate was fundraising instead of traveling to the forum. Cain was in Washington doing a series of TV interviews Tuesday in the wake of revelations he was the focus of a sexual harassment claim in the 1990's. Romney didn't have any public events scheduled Tuesday. 

Cain and Romney's Iowa absence provides a flicker of light for others looking to gain ground in the state. A couple of candidates at the back of the pack see Iowa as a chance to make a move to the top tier and have focused almost exclusively on the Hawkeye State. 

Rick Santorum has visited 90 of Iowa's 99 counties and plans to hit the other nine by the end of the week. He says he respects the role Iowa voters play in the nomination process. 

"I believe that they do play a crucial role in winnowing the field out and really recommending to the rest of the country," Santorum said. "Many of the people who participate in these caucuses have met the candidates, really measured them up, and I encourage them to do that. That's why I've made myself so accessible." 

Michele Bachmann was born in Iowa and hasn't hidden the fact that Iowa is the key to her campaign's success. She pointed to Iowa's support of President Obama in 2008 as an omen of things to come here. 

"We see Iowa as a pathway to success for the country because this state elected Barack Obama and put him on his path to victory as president. Iowa will choose the next president of the United States and I hope to be that candidate." Said Michele Bachmann. 

But she and the others at Tuesday's forum in Pella face an uphill climb. The most recent Des Moines Register poll of Iowa voters shows despite their relative lack of campaigning in Iowa, Cain and Romney neck-and-neck for the lead with the other candidates trailing far behind. 

But the candidates in attendance Tuesday believe time spent in Iowa can lead them to success on Jan. 3 when the state holds its first-in-the-nation caucus.
