Democratic and Jewish organizations are pushing House Speaker John Boehner to return donations from a "Nazi enthusiast" who ran for Congress last year but was defeated in part because of the revelation he wore a German SS uniform while taking part in several Nazi re-enactments. 

Rich Iott, who ran as a Republican for Ohio's 9th Congressional District seat last year against incumbent Democrat Marcy Kaptur, donated $2,400 to Boehner's campaign, reported Washington Jewish Week 

"Clearly Boehner is so desperate for campaign cash or so removed from reality that he is accepting money from a Nazi enthusiast who insults the memory of six million Jews who died during the Holocaust and our nations' veterans who sacrificed to defend our freedom," said Jesse Ferguson of the Democratic Congressional Campaign Committee. 

The National Jewish Democratic Council, a left-leaning group, said Boehner has a "responsibility to repudiate Iott's behavior by returning the money or, better yet, donating it to a Holocaust charity." 

"Boehner-the most powerful Republican in America and the third person in the presidential succession line-owes American Jews and WWII veterans an explanation as to how he can continue to associate himself with Iott, given his widely-known disturbing behavior," said NJDC President and CEO David A. Harris. 

At the time, Iott said his appearance in a Nazi uniform was as a "re-enactor" and that it helped him "bond with his son" who also was a World War II "re-enactor enthusiast." 

FOX News has reached out to Speaker Boehner's office for details on this story, and although they do not deny the contribution, Boehner's communications team has declined to comment.
