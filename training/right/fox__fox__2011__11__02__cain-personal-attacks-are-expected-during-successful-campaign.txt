McLEAN, Va. -- Facing another day of tough questions about more-than-a-decade-old sexual harassment allegations, Republican presidential candidate Herman Cain said Wednesday he is merely in the third stage of a successful campaign -- when critics attack. 

"I just left a meeting and someone made the observation that a successful campaign goes through four stages, and I never heard it put this way but it made a lot of sense," Cain told an audience at a breakfast hosted by the Northern Virginia Technology Council and Consumer Electronic Association. 

"The first phase is they ignore you. And they ignored us for about the first six months of this year. Second phase, they ridicule you. And some of the pundits have described the Cain candidacy for president as entertaining so we did get our share of ridicule. The third phase, they try to destroy you. Well, got a little bit of that this week," Cain said in a tone of understatement, drawing chuckles from the audience. "And I'm going, 'This model makes too much sense.' He pointed out that the fourth phase -- they accept you." 

Cain described how he will survive the vicious nature of the third phase, which he has experienced since news broke over the weekend that two women had reached confidential financial settlements with the National Restaurant Association, which Cain headed from 1996-1999, after complaining about alleged inappropriate behavior by Cain. 

"There are factions that are trying to destroy me, personally, as well as this campaign. But there is a force greater, there is a force at work here that is much greater than those that would try to destroy me and this journey to the White House , and this force is called, 'The voice of the people.' That's why we're doing as well as we are in this campaign thus far." 

What followed was a short moment of silence, after which Cain joked, "Y'all were supposed to applaud." He was rewarded laughter from the audience. 

Cain took the moment to try and win more approval from the crowd. 

"Now I don't want any accountants in here to be offended by this, but I would have expected that from a room full of accountants, and I have gotten that reaction from a room full of accountants, but technology people should have a little bit more soul." 

The generally warm reception toward Cain may be attributable in part to the candidate's professional background. Cain has an undergraduate degree in mathematics and a graduate degree in computer science. After serving as a civilian employee in the U.S. Navy working in ballistics, he joined Coca-Cola as a computer systems analyst.
