Mark Block, Herman Cain's campaign chief of staff, backed off claims that one of Texas Gov. Rick Perry's campaign advisors is behind what he calls a "smear campaign" involving sexual harassments accusations from three women. 

Block first leveled the accusation at Perry strategist Curt Anderson on " Special Report with Bret Baier " Wednesday. Since then, Anderson, in an appearance on FOX News and in multiple interviews, defended himself and praised Cain. 

Watch the latest video at video.foxnews.com 

The Cain campaign alleged Wednesday that Anderson learned of the harassment charges when he worked as a strategist for Cain's unsuccessful 2004 Georgia Senate bid. Anderson denied ever hearing of the allegations and, talking to FOX News' John Scott , said that Politico, the source of the original story, as well as any other publication were free to reveal any comments he has ever made about Cain. 

Cain had doubled down on the accusations against the Perry campaign Wednesday, telling supporters from TeaParty.net: "We've been able to trace it back to the Perry campaign that stirred this up in order to discredit me. The fingerprints of the Rick Perry campaign are all over this, based on our sources." 

Cain was more specific talking to Forbes.com, saying that he briefed Anderson at length in a private meeting. " Those charges were baseless, but I thought he needed to know about them. I don't recall anyone else being in the room when I told him," the Web site quotes Cain as saying. 

But, in an appearance on "America Live with Megyn Kelly " Block said that while all the evidence their campaign had about the incident over the last two weeks pointed to Anderson being the source, the campaign was "absolutely thrilled that he came on your show and said it wasn't, because Mr. Cain has always had the upmost respect for him," Block said. 

Block stopped short of clearing the Perry campaign, adding "I will stand behind what we said yesterday" but is "waiting for all the facts." 

Block claims the Cain campaign has seen its best fundraising day ever since this scandal broke. He says the campaign is "in position to move ahead" and campaign hard in Iowa New Hampshire, South Carolina and Florida.
