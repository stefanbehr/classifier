Sen. John McCain blasted the regulatory agency responsible for overseeing government-backed mortgage giants Fannie Mae and Freddie Mac, calling for the ouster of its head, Edward DeMarco. The Arizona Republican said Thursday that he is prepping legislation to prevent future bonuses like the $12.8 million paid to 10 executives of the two government-sponsored enterprises, money approved by DeMarco, a move McCain called "unconscionable." 

"It's been proven time and time again that Fannie and Freddie are synonymous with outright corruption and fraud and the federal regulator has the audacity to approve $12 million in bonuses to people who make $900,000 per year," McCain charged, as Freddie Mac on Thursday reported a $4.4 billion loss for the third quarter and announced it is seeking $6 billion from the Treasury Department. "This body should be ashamed if we let this happen, especially in these economic times." 

The senator plans to introduce an amendment to the minibus appropriations bill the Senate will take up next week that would, according to a McCain aide, "prevent (bonuses) in the future, so long as (Fannie and Freddie) are in federal conservatorship." 

A spokesman for the Federal Housing Finance Agency (FHFA), where DeMarco is acting director, told Fox, "These are not rewards for the individuals who were running the companies when the problems were created. Fannie Mae and Freddie Mac have more than 5 trillion dollars in mortgage assets, all of which are at risk to taxpayers. It is critical that we protect taxpayers by having highly qualified executives running these companies." 

Politico first reported the $6.46 million in bonuses, paid back in 2010, for the top five officers at Freddie Mac -- including $2.3 million for CEO Charles E. Haldeman Jr., who is stepping down next year -- and $6.33 million for Fannie Mae officials, including $2.37 million for CEO Michael Williams, for meeting modest goals. A second bonus installment for Freddie executives in 2010 has yet to be reported to the Securities and Exchange Commission, Politico reported. 

"Freddie Mac has done a considerable amount on behalf of the American taxpayers to support the housing finance market since entering into conservatorship," Freddie spokesman Michael Cosgrove told Politico earlier this week. "We're providing mortgage funding and continuous liquidity to the market. Together with Fannie Mae, we've funded the large majority of the nation's residential loans. We're insisting on responsible lending." 

The outrage in Congress has been bipartisan, signaling easy-sledding for McCain's amendment. 

On Tuesday, Senate Majority Leader Harry Reid, whose home state of Nevada has been ravaged by the housing crisis, could barely contain his disdain when he learned the news of the bonuses. "A gag reflex in front of all of you would be improper" 

"Why should they be taking bonuses when Fannie and Freddie are in real trouble?" Sen. Orrin Hatch , top Republican on the Finance Committee, asked. "That's what happens when government bureaucrats decide who wins and who loses." 

Fannie and Freddie received a $170 billion taxpayer-funded bailout and were put into conservatorship in 2008. 

Senate Banking Committee Chairman Tim Johnson, D-ND, also announced Thursday that soon his panel will hear testimony from DeMarco, acting director of the Federal Housing Finance Agency (FHFA), on the bonus matter. 

"Hearings are fine. Hearings are great. They're not the answer," McCain said in a Senate floor speech. "The answer is to stop it from happening."
