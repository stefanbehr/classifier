It use to be new fathers would receive cigars to celebrate the birth of a child. But French President and new father Nicolas Sarkozy , 56, received some hazing from President Obama on the recent arrival of his first daughter. 

Standing together in Cannes, France , for the G20 summit on Thursday, Obama joked, "I am confident that Giulia inherited her mother's looks rather than her father's." Sarkozy's wife, Carla Bruni , is a famous former model. 

"Now we share one of the greatest challenges and blessings of life, and that is being fathers to our daughters," Obama said. 

The French president didn't seem at all embarrassed at the fun Obama had at his expense. The host of the G20 summit was gracious and even said he was following in Obama's example. 

"For four years now, (Obama) has been explaining to me that to be a father to daughters is a fantastic experience -- he who has two daughters. So I have listened to him." Sarkozy said.
