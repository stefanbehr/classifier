CANNES, France - President Obama responded to the 80,000 new jobs created in October, saying they indicate the economy is growing, but way too slowly. "If we're going to do something big to jump-start the economy, congress needs to act," Obama said referring to his American Jobs Act. 

The Labor Department reported Friday that the unemployment rate dropped slightly in October, down to 9.0 percent, from 9.1 percent the previous month. 

Delivering a press conference from Cannes, France on Friday, president Obama had a message to congress. "They need to think twice before voting no again on the only proposal that independent economists say would actually make a dent in unemployment." 

Obama says it's hard to ignore that there's an election in just one year. For republican members of congress, that means fighting the Obama administration agenda. For the president, it means getting that unemployment number below 9%. 

Of his jobs agenda, Obama says, "I'm going to keep on pushing it regardless of the politics of it."
