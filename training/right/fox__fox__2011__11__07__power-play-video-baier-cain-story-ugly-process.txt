Bret Baier joined Chris Stirewalt on Monday's Power Play Live where the two discussed the evolving Herman Cain Scandal. 

"Covering this stuff is not fun," said Baier. "It's kind of an ugly process." 

Watch Power Play Live Monday-Friday at 11:30am ET here: http://live.foxnews.com/ 

Watch the latest video at FoxNews.com
