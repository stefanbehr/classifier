Rick Santorum has picked up three noteworthy Iowa endorsements for his bid to capture the 2012 GOP presidential nomination. 

Chuck Laudner (former Executive Director and Interim Chairman of the Iowa RepublicanParty), Lori Jungling (Iowa co-chairman of HuckPAC) and Shane Vander Heart (conservative blogger at CaffeinatedThoughts.com) have all jumped on board the Santorum campaign. 

Laudner's endorsement will almost certainly get the attention of fellow Iowa Republicans. One of Laudner's friends is the currently un-committed Congressman Steve King. 

King's endorsement (should there be one) would be a big Iowa Caucus development given the level of devotion backers have for the western Iowa Congressman. And please note the King mention in this portion Santorum's press release about Laudner's endorsement. 

"Chuck has been a leading voice for the Republican Party in Iowa and for conservative values. Chuck was an influential leader in the campaign to unseat Supreme Court justices that took it upon themselves to redefine marriage, and he has been a loyal and trusted counselor to one of our nation's brightest conservative stars in Congress - Steve King - which speaks volumes for Chuck's ability and principles." 

Laudner says Santorum is a "...rock-ribbed conservative with a long record of fighting on all of the issues important to me. He is running a ground game in Iowa sure to produce results. And I trust him to lead the fight." 

Launder's been active in Iowa campaigns for years and his extensive experience will certainly help Santorum.
