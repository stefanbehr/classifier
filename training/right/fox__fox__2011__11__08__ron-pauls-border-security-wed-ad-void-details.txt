If you land on a conservative political blog (particularly one that covers Iowa), you may see a web ad for Ron Paul's "Declaration of American Sovereignty." 

A mouse click on the ad brings visitors to a website asking them to sign a petition for the American Sovereignty Protection Act which "will end Birthright Citizenship, welfare benefits for illegal immigrants and secure our borders with a high-tech fence, including satellite surveillance and helicopter stations across the border." 

The petition suggests there is proposed legislation by the Texas congressman which would address this list of border security measures. But the House Clerk's Office has no record of it. 

When asked for details about the American Sovereignty Protection Act, the Paul campaign referred Fox News to an amendment to the Constitution introduced by the congressman in 2007. The proposed constitutional change only targeted the elimination of birthright citizenship. 

Now, Paul's presidential campaign website does note the following to-do list of immigration changes: 

* Enforce Border Security - America should be guarding her own borders and enforcing her own laws instead of policing the world and implementing UN mandates. 

* No Amnesty - The Obama Administration's endorsement of so-called "Comprehensive Immigration Reform," granting amnesty to millions of illegal immigrants, will only encourage more law-breaking. 

* Abolish the Welfare State - Taxpayers cannot continue to pay the high costs to sustain this powerful incentive for illegal immigration. As Milton Friedman famously said, you can't have open borders and a welfare state. 

* End Birthright Citizenship - As long as illegal immigrants know their children born here will be granted U.S. citizenship, we'll never be able to control our immigration problem. 

* Protect Lawful Immigrants - As President, Ron Paul will encourage legal immigration by streamlining the entry process without rewarding lawbreakers. 

This list of proposals would likely appeal to conservative voters, who may want to hear more from Congressman Paul and the so-called American Sovereignty Protection Act.
