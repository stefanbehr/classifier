You've heard the beeps and the warnings of the Emergency Alert Systems (EAS) on your televisions. State and local tests pop up on the airwaves all the time, when you least expect them. 

But on Wednesday, November 9 at 2:00 p.m. ET, the federal government will conduct its first-ever nationwide evaluation of the system and the White House wants to get the word out and remind everyone that it is only a test. 

"The National Emergency Alert System is an alert and warning system that can be activated by the president if needed to provide information to the American public during emergencies," White House press secretary Jay Carney told a gaggle of reporters Tuesday. The test will only last about 30 seconds and will be broadcast on television and radio stations across the country. "Just wanted to make sure everybody knows that it's just a test," Carney added. 

Federal Emergency Management Agency (FEMA) and the Federal Communications Commission (FCC), along with many other agencies are teaming up to help spread the word so people know what to expect and are not caught off guard when the test occurs. 

"The various disasters our country has faced this year underscore the need for effective and well-tested emergency alert and warning systems that could be used in a time of real emergency, at a moment's notice," FEMA Administrator Craig Fugate and FCC Chairman Julius Genachowski wrote in a letter last week. 

The federal agencies have already identified areas that need improvement to ensure everyone across the country has access to the warnings. The nationwide test is expected to help agencies figure out how to complete a new, effective and more reliable alert system.
