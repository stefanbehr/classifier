WASHINGTON -- An early morning gunshot that closed off Northwest D.C. streets for several hours ended up not being quite the scare neighbors expected. 

It turns out a U.S. Secret Service officer accidentally fired off his gun near the Russian Embassy. Secret Service spokesman Jim Mackin told Fox News there were no known injuries. 

The inadvertent discharge occurred just before 4 a.m. ET. Officers were on the scene all morning as the investigation took place. 

The officer who is with the Foreign Missions branch of the Secret Service was in a marked vehicle at the time of the shooting. 

As part of standard protocol, the case has been turned over to the Secret Service's Office of Professional Responsibility to determine if there was any wrongdoing.
