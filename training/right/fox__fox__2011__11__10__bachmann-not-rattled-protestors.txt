Michele Bachmann was disappointed in a group of about 20 "Occupy Charleston" protestors who interrupted her foreign policy speech in Mount Pleasant, S.C. on Thursday afternoon. 

"They have the right to do that. I completely understand. But how disrespectful to do that when we were here to honor veterans and to stand behind them and to stand behind our military. And I just hope that some day they truly come to understand and appreciate what s been done for them, how their liberties have been paid for by a very heavy price by the people in that room." 

Just a few minutes into her speech, "Occupy Charleston" participant Max Brewer stood up in a crowd of about 50 and led the chanting. "This will only take a minute. We have a message for Mrs. Bachmann," they shouted in uniform. "You cater to the one percent. You oppose paying hardworking Americans a living wage and refuse to promote realistic solutions to economic problems." 

Bachmann let the protestors chant and it wasn t until the protestors moved close to the podium where she stood that police escorted her off the stage and handcuffed one of the protestors. Bachmann supporters rallied in her defense and started yelling in response "sit down" and "Michele, Michele!" 

Bachmann said that at no point did she fear for her safety and returned to the podium after the protestors had marched off.
