Democrats on the Senate Judiciary Committee took the first steps Thursday to invalidate the federal law that codifies marriage as solely between a man and a woman. The panel passed the measure repealing the Defense of Marriage Act (DOMA) by party-line vote. 

"DOMA was wrong when it passed in 1996 and it is wrong now. There are 131,000 legally married, same-sex couples in this country who are denied more than 1,100 federal rights and protections because of this discriminatory law," said lead Respect for Marriage Act co-sponsor Dianne Feinstein of California. "I don't know how long the battle for full equality will take, but we are on the cusp of change, and today's historic vote in the committee is an important step forward." 

The move would pave the way for same-sex couples to marry ahead of a key election year in which control of both the White House and Senate hangs in the balance, though the measure stands little chance of passage in this Congress. The GOP-controlled House and a filibuster-proof GOP majority in the Senate all but guarantees its demise. 

The law, created in 1996 by President Bill Clinton, has been repealed by six states and the District of Columbia (Vermont, Connecticut, Iowa, New York, New Hampshire, and Massachusetts). Still, more than 10 states have upheld DOMA. 

Sen. Chuck Grassley, R-Iowa, a staunch opponent of the repeal effort, said it was "unfortunate" that the bill was being brought up now with so few days left on the legislative calendar and a need to focus on the nation's bleak jobless outlook. 

Grassley, top Republican on the Judiciary Committee, added, "Marriage as an institution for one man and one woman is about morality for many millions of Americans...It's still the law in the vast majority of our states." The senator said, "To me this debate is about stable families, good environments for raising children, and religious beliefs. It's not about discrimination against anyone. No society has limited marriage to heterosexual couples because of the desire to create second class citizens." 

"I see it as a vote to right an injustice that goes to the core of what we stand for - freedom and equality," retorted committee chairman Patrick Leahy, D-Vt. 

Supporters cited a number of benefits they said would come from allowing same-sex couples to marry, some which would improve the nation's economy, like lower health care costs for businesses and individuals. 

Sen. Orrin Hatch , R-Utah, a senior member of the panel, called on members to respect states rights and predicted that repealing DOMA would give nothing to same-sex couples. 

It's unclear if the bill will receive a vote this year.
