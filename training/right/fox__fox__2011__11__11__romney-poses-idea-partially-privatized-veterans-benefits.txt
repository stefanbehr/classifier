Mauldin, S.C. - Making a rare trip to South Carolina, former Massachusetts Governor Mitt Romney held a roundtable with veterans, where he suggested that a voucher system might solve their frustrations with the bureaucracy of applying for Veterans Affairs benefits after some of them described the necessity of having a personal army of advocates to get what they deserved. 

"When you work in the private sector and you have a competitor, you know if I don't treat this customer right, they're going to leave me and go somewhere else so I better treat them right," said Mitt Romney , as he analyzed their bureaucratic dilemma aloud. "Whereas if you're in the government, they know there's no where else you guys can go, you're stuck." 

He continued with a tone of animation, as if the thought had just come to him, "Sometimes you wonder if there's some kind of way to introduce some, some private sector competition, somebody else that can come in and say, hey each soldier gets X thousand dollars attributed to them, and then they can choose whether they want to go in the government system or a private system with money that follows them, like with what happens in schools in Florida where people have a voucher that goes with them, who knows." 

In his opening remarks, Romney said that while there were opportunities for "cost savings and systems improvement" at the Defense Department, he vowed to reinvest the money into building a stronger military, not to solve the nation's debt problem. 

"I want to take the funds that are being wasted and use them to rebuild our Navy and Air Force, use them to have additional active duty personnel, about a hundred thousand troops, use them to give care to our veterans that our veterans deserve," he said. "And so I will not look to the military as a place to balance our budget." 

Asked how he would handle the influx of soldiers returning from Iraq , Romney emphasized his business background, saying, "The best thing I can do for the soldiers returning home is to get the economy going again and provide the stability and predictability that small business and middle sized business and big business need to make investments." 

Although he was critical of the president's timetable for withdrawal in Iraq , he complimented President Obama for a "good idea" -- providing credit to companies that hire veterans, particularly those who have been out of work for a while or those who are coming home. 

"It's a temporary measure but given the high level of unemployment among veterans, and given the large influx of veterans that will be returning to the country, I think it's an idea that has merit." 

Romney seemed to anticipate questions about the amount of time he plans to spend in South Carolina, which he has visited much less frequently than New Hampshire. In his opening remarks to reporters following the roundtable, Romney said, "I have great hopes for real success here in South Carolina. First I've got to get things going in Iowa of course and New Hampshire, but then comes South Carolina. I'm here today to say hi to many friends and get ready for a debate coming up and you'll see me time and time again in South Carolina." 



Asked about Rick Perry's "oops" moment, Romney declined to speak negatively of his competitor saying, "We all make mistakes, and I'm more concerned about my own than anybody else's and wish Rick the very best. We've had a lot of debates, I think we've had ten already, maybe eleven now, and we may have four or five more to come. So he and I have plenty of time to get our message across in the best way we can."
