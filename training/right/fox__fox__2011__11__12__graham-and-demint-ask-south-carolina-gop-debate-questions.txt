SPARTANBURG, S.C. -- Some presidential debates feature moderator questions, audience questions or even online questions, but at Saturday night's GOP debate in South Carolina, the pair of Republican U.S. senators from the Palmetto State will join the questioning. 

Sen. Jim DeMint , a member of the Committee on Foreign Relations and Sen. Lindsey Graham , a member of the Committee on Armed Services, will each ask questions at the debate. 

Organizers of the debate at Wofford College in Spartanburg are calling it the "Commander-in-Chief Debate" because it will focus on foreign relations and national security issues. Graham is a member of the Air Force Reserve and has made several highly-visible trips to U.S. war zones in recent years. 

It won't be the first time DeMint has questioned some of those on stage. He held a Labor Day candidate forum in Columbia back in September. 

Fox News' Chief Political Correspondent Carl Cameron contributed to this report.
