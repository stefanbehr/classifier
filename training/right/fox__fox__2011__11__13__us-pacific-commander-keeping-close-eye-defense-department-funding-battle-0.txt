The United States Naval Commander for the Pacific region expressed concern Sunday over the looming potential for automatic Defense Department funding cuts should the Congressional Super Committee fail to reach agreement. 

"I know that it's shared broadly that sequester would be a rather Draconian approach to the problem and it would complicate the budgetary approaches that the Department of Defense is scoping right now considerably were it to occur," Admiral Robert F. Willard, Naval Commander of U.S. Pacific Command, told reporters gathered for the APEC summit in Honolulu. 

In an attempt to force the Super Committee to come to agreement on how to cut at least $1.2 Trillion from the budget before its November 23rd deadline, President Obama has warned that across-the-board federal budget cuts will automatically be implemented should the committee fail. 

When asked about any concerns the committee might want to cut too much from the Defense Department, Admiral Willard was not overly concerned, "[W]e're all very aware that we're coming off of a period of long term warfare for the country." 

He added, "I mean we're eventually transitioning from two wars, and we're facing budgetary challenges as a nation that have to be addressed. So the Department of Defense realizing that, is scoping what the those outcomes may mean for the Department of Defense." 

The president has been keeping his distance from Super Committee deliberations, but in a conversation with two of its members while making his way to Hawaii for the APEC summit this weekend, he told them the automatic budget cut trigger was non-negotiable. 

Willard has confidence Washington will work this out. Over the course of two wars during his tenure, he said, the Defense Department has always had the tools it needed, "I have every confidence that in the decisions that our government makes, that our administration makes and that are made in the Pentagon, given the importance of this region to world and the importance of this region to the United States that Pacific Command will continue to be well served and able to carry out its mission of assurance and deterrence where required into the foreseeable future."
