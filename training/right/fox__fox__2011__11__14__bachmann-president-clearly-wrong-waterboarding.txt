Minnesota Rep. Michele Bachmann isn't backing down from her controversial stance on waterboarding despite being called out by President Obama over the weekend as "wrong." 



The Republican presidential candidate said in a GOP candidates' debate on Saturday night that she, along with fellow candidate Herman Cain , would support the reinstatement of waterboarding as an interrogation technique for terror suspects. 

President Obama responded on Sunday 

"They're wrong. Waterboarding is torture," he told reporters at a press conference in Hawaii. "It's contrary to America's traditions. It's contrary to our ideals. It's not who we are." 

"Well, I think the president is clearly wrong," Bachmann shot back on Monday, likening the circumstances under which she would use the technique to those of World War II . 

"President Harry Truman -- who had to make the horrific decision about dropping an atomic bomb on Japan to end World War II -- he said if he had to kill Japanese in order to save one American life, he would," Bachmann told Fox News. "And if, as president of the United States, I believed that we would be able to save 3000 American lives, and stop aircraft from flying into the twin towers, I would utilize waterboarding if it would save those American lives." 

Bachmann also tempered the effects of waterboarding on suspects. 

"No one dies from the use of waterboarding," She said. "It is uncomfortable? Yes, it's uncomfortable. But our worry should not be about the comfort level of a terrorist." 

The Minnesota representative has seen her polls numbers drop since winning the Iowa straw poll back in August, but insists she's not concerned. 

"We have a strong operation," she said. "We're going to continue to get our positive message out all across Iowa, and on January 3rd, that's when the results will come in." 

Bachmann also had choice words for former Penn State defensive coordinator Jerry Sandusky, who is embroiled in a child sex scandal, and assistant coach Mike McQueary, who says he walked in on Sandusky with a boy in the showers at Penn State. 

"I am a small woman, I'm a Christian, but if I had walked in on an event like that, I can guarantee you, that man would wish he'd never been born," she said. "Any human being would stop that form of activity against a little child."
