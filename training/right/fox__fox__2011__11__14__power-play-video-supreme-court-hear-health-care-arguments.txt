Shannon Bream checks in from the Supreme Court where she it was revealed on Monday that the justices will hear arguments on four different issues stemming from national health care reform. The court granted five and a half hours for arguments, the normal time period is only one hour. 

Chris Stirewalt was also joined by former National Republican Campaign Committee direcor of communications Karen Hanretty who asked, "Do you want to be a president running for re-election after wasting two years of your presidency and much political capitol on something the supreme court rules is unconstitutional?" 

Chris countered that if the Supreme Court upholds the law Obama has a great victory, but it will also galvanize the right duyring an election year. 

Watch Power Play Live Monday-Friday at 11:30am ET here: http://live.foxnews.com/ 

Watch the latest video at FoxNews.com
