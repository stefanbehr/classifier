A Bloomberg poll out Tuesday put Ron Paul in a statistical dead heat with Herman Cain , Rick Perry , and Newt Gingrich among likely Iowa caucus goers. But in an indication of Ron Paul's organizational strength, the survey also found that two-thirds of those surveyed said they had been contacted by the Ron Paul campaign through email, direct mail, telephone, door knocking, or by meeting the candidate in person. 

Less than half said they had been contacted by the campaigns of other frontrunners in the state -- only 46 percent had been contacted by Romney, 41 percent by Cain, and 29 percent by Gingrich. 

"The Ron Paul organization has not stopped working for the last four years," observes Trudy Caviness, who is a member of the Iowa Republican State Central Committee. "We've got five people on our state central committee that are Ron Paul supporters." 

By contrast, she says, "The Mitt Romney staff has changed from who they were four years ago whereas the Ron Paul staff, the leadership, is the same. His people have continued to work. And they work a lot with young people." 



The Ron Paul campaign does not have the highest number of paid staffers in Iowa -- there are six -- but the campaign's strength can be better understood by the legions of "dedicated" volunteers, as described by one top Republican in the state who declined to call that dedication "organizational" strength. 

Ron Paul's Deputy National Campaign Manager, Dimitri Kesari, can't give an exact figure on the number of volunteers in Iowa, but says that compared to Ron Paul's 2008 run, "It's more professional this time. The whole team national on down has more campaign, structured experience, more knowledge and know-how." Furthermore, he says, they're focused on doing well in the Hawkeye State. 

A new Iowa State University/Gazette/KCRG poll of 1,256 registered Iowa voters, out Thursday, has Paul once again near the front of the pack at 20.5 percent, second to Cain at 24.5 percent. Romney trails in third place at 16.3 percent. 

"Iowa is a big grassroots state," says Kesari. "Iowans like to see the candidates up close and personal. It's not the biggest TV market -- they want to meet their candidates, their volunteers. Our strategy is to have lots of volunteers. Ron Paul has been traveling across the state meeting people in places big and small." 

Ron Paul has spent 34 days in Iowa, behind Herman Cain's 35 days, Gingrich's 47, Bachmann's 56, and Santorum's 74. 

Kesari scoffs when asked whether Paul has an upper limit of support in the Hawkeye state. 

"Back when Reagan was running against Jimmy Carter, they said he's too conservative, he's from California. One of the biggest things said was that he can't win. There is a similar dynamic here. People say Ron's too conservative, he can't win, he has an upper limit. We're running against Barack Obama . This could be a history making race again."
