Despite a poor economy, Americans are finding ways to make it home for the holidays. 

AAA is forecasting about 42.5 million people will travel more than 50 miles for Thanksgiving, a four percent increase from last year. This is the first increase in holiday travel this year. 

"Driving AAA's projected increase in the number of Thanksgiving travelers is pent-up demand from Americans who may have foregone holiday travel the last three years," said Bill Sutherland, the vice president of AAA Travel Services. "As consumers weigh the fear of economic uncertainty and the desire to create lasting family memories this holiday, more Americans are expected to choose family and friends over frugality." 

According to AAA consumer confidence and consumer comfort surveys, sixty percent of intending travelers feel the economy has either no impact on their travel plans or that things have improved for them. The remaining 40 percent do state an intention to scale back travel plans but in light of the current economic conditions. 

Automobiles remain the dominant mode of transportation, but 3.4 million people are expected to fly during the Thanksgiving holiday despite the 20 percent average increase in airfares from last year. Hotel rates have also increased with travelers spending an average of $145 per night compared to $136 last year. One bright spot... a decrease in daily car rentals. 

The Thanksgiving travel period is from Nov. 23 through Nov. 27. 

For the AAA 2011 Thanksgiving Travel Forecast: http://newsroom.aaa.com/wp-content/uploads/2011/11/Thanksgiving_2011-Final.pdf
