A vote in favor of a balanced budget amendment failed in the House of Representatives Friday, falling short of the two-thirds majority needed to amend the Constitution. The measure got a simple majority with 261 yeas to 165 nays, but with 426 members voting, the bill's sponsors needed 284 yeas, 23 more than the measure got. 

The balanced budget amendment vote in 1995 was 300 yeas to 132 nays, well above the two-thirds requirement to amend the Constitution. In that vote, 72 Democrats voted yes. Today, only 25 voted in the affirmative. In 1995, two Republicans voted no. In Friday's vote four GOPers voted no. 

There were only three members not voting in 1995's vote. On Friday there were eight no-votes: Reps. Karen Bass (D-Calif.), Ted Deutch (D-Fla.), Bob Filner (D-Calif.), Gabrielle Giffords (D-Ariz.), Grace Napolitano (D-Calif.), Devin Nunes (R-Calif.), John Olver (D-Mass.) and presidential candidate Ron Paul (R-Texas). 

Presidential candidate Rep. Michele Bachmann (R-Minn.) was at the Capitol and voted yes. 

Other interesting tidbits from the vote: 

Ten Democrats who voted yes in 1995 remain in the House. Of those, seven switched their vote today. They included House Minority Whip Steny Hoyer (D-Md.), Assistant Minority Whip and supercommittee member Jim Clyburn (D-S.C.), Reps. Rob Andrews (D-N.J.), Jim Moran (D-Va.), Frank Pallone (D-N.J.), Marcy Kaptur (D-Ohio) and Mike Doyle (D-Pa.). 

Reps. Collin Peterson (D-Minn.), Jerry Costello (D-Ill.), Peter DeFazio (D-Ore.) remained consistent. 

Rep. Ralph Hall (R-Texas) voted yes in 1995 as a Democrat and switched parties several years ago. He also voted yes today. 

House Rules Committee Chairman David Dreier (R-Calif.) was the only Republican here in 1995 who voted yes then and voted nay today. 

Reps. Sanford Bishop (D-Ga.) and Bobby Rush (D-Ill.) were the only members still serving who missed the 1995 vote. Bishop voted yes today. Rush voted no.
