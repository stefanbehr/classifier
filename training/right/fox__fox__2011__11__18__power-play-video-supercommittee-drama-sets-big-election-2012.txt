Chris Stirewalt and Texas Rep. Kevin Brady (R) discuss the latest drama surrounding the supercommittee's looming deadline. 

Chris argues that there are no real serious consequences associated with the deadline while Brady says it's an arguement over a "small fraction" of government spending. 

"This sets up a pretty big battle for next November," said Brady. "About laying out what is the vision for America." 



Watch Power Play Live Monday-Friday at 11:30am ET here: http://live.foxnews.com/ 

Watch the latest video at FoxNews.com
