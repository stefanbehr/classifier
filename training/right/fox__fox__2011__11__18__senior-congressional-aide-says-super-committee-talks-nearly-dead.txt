A senior aide with familiarity of the Super Committee talks says negotiations have nearly broken down. 

"Hospice has entered the house," the aide said. "It's just a question of whether it's done tonight or tomorrow or Sunday. But the end is near," adding that "the family has gathered." 

Others are noting that talks and individual conversations are "slowing down." 

There is an indication that there may be some more talks Saturday, but some are even starting to back away from weekend negotiations at this point.
