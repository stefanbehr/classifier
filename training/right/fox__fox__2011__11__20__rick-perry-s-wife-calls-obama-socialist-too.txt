The first lady of Texas agrees with her husband Rick Perry , President Obama's health care policies are socialist. Yes, Anita Perry went there too. "It is socialized medicine. People can call it what they want-- nationalized health care, that is what it is in my opinion," Mrs. Perry told Fox News. 

In fact, Mrs. Perry says her strong feelings about health care reform are why she wanted her husband to run for president. "I don't like socialized medicine and that's the path we're going. We have the greatest health care system, why would we want to change it," Perry says. 

Mrs. Perry's background qualifies her to talk about health care. She was a practicing nurse in Texas for 17 years, working in surgery, pediatrics, intensive care, administration and teaching. She has advocated for health care issues as the governor's wife, and has continued talking about it on the trail, making stops at hospitals and nursing schools. 

When pressed on why she feels Obama's health care policies are socialist, she points to her father, a "bed side" physician who told her, "'the moment we take a dollar from the federal government they will tell us how to start practicing medicine.'" Perry went on to say she agrees with him that "every American should have the choice they want. To have that physician they want." 

Her husband, Texas Gov. Rick Perry is currently running a campaign ad that also uses the "s" word in relation against the President. "Obama's socialist policies are bankrupting America. We must stop him now," says Perry in his fourth, and first negative, ad of his presidential campaign. 

The Perrys have been married for 29 years. And, on a trail littered with stump speeches, Anita Perry has been gaining media attention for her candor. To cover more ground, the Perrys often campaign separately but this week she and her husband are making appearances together ahead of the Thanksgiving holiday.
