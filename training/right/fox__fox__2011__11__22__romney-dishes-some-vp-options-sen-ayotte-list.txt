Presidential candidate Mitt Romney has been pretty quiet about who he might pick as a running mate if he wins the nomination, but Monday said New Hampshire Senator Kelly Ayotte is on the list. 

He said if he becomes the nominee, then he'll start thinking about it, but he has gathered about 15 names. 

"[T]here are terrific Republicans in the Senate, in the House, in governors' offices. You've got extraordinary men and women. We have a very deep bench," Romney said on Fox's Hannity Monday night. 

Romney added that he didn't think that was always the case. 

"I don't know that we've always felt that way. This year is really exceptional and whoever our nominee is will be able to choose a remarkable, superb person to become the vice presidential contender," he said. 

He made the appearance on set with Ayotte, a freshman Senator from the key political state of New Hampshire, who endorsed Romney over the weekend. 

Ayotte's backing was a coveted one with her political star rising and popularity in conservative circles. 

Romney also talked about the early caucus and primary states and his strategy, which some have criticized him for not spending enough time in Iowa. 

"I am going to do what it takes to get the nomination. I will let the tacticians in my office figure out how much money we spend in each state and how many visits we make. But I'll be in Iowa this week. I'm going to be in New Hampshire regularly," he said.
