A former fundraiser to President Barack Obama was sentenced to 10.5 years in federal prison today. Tony Rezko, 56, will end up spending only seven years behind bars because the judge gave him credit for time he's already served. 

In 2008, Rezko was convicted of pay to play politics including fraud, bribery and extorting millions from businesses seeking contracts with the state of Illinois while Rod Blagojevich was Governor. 

Rezko, a real estate developer, was a well known fundraiser in Illinois political circles. For years he donated money to Republicans and Democrats, including Barack Obama during his campaign for Illinois Senate and later for US Senate. Rezko and Obama first met in 1991 and later engaged in a small real estate deal when Obama bought his home in Chicago's Hyde Park neighborhood. Obama later referred to the deal as a mistake. 

Rezko's attorneys hoped the judge would sentence Rezko to the 3.5 years he's already served. Prosecutors say Rezko actually refused to cooperate with them after he was first arrested and charged. 

"Rezko lied point-blank repeatedly over his first 19 interview sessions about his criminal involvement with Blagojevich," according to a court filing. 

Rezko's sentencing of 10.5 years could set a precedent for the upcoming sentencing of convicted Illinois Governor Rod Blagojevich . Blagojevich was found guilty on multiple corruption charges during his re-trial earlier this year. His sentencing is scheduled for Tuesday December 6th in Chicago.
