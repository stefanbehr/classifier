Arizona Congresswoman Gabrielle Giffords (D) held her first constituent event since she was critically wounded in a January 8 shooting. She helped served a traditional Thanksgiving dinner Thursday to airmen as well as military retirees and their families at Davis-Monthan Air Force Base in Tucson. 

Giffords husband, Mark Kelly, a retired Navy captain and former NASA astronaut accompanied her to the midday meal at the dining facility. Over four hundred people were expected to attend the traditional Thanksgiving meal which featured turkey as well as prime rib, ham, shrimp and assorted deserts. 

Base officials had originally contacted the congresswoman s staff to take part in the annual dinner but when staff informed Giffords about the event, she wanted to attend herself. She traveled to her home district of Tucson on Tuesday to spend the holiday with family and friends. 

Giffords has been in Houston since January, undergoing therapy as part of her recovery from head wounds suffered during the shooting which wounded 11 others and killed six people during a meet-and-greet event for the congresswoman. 

Giffords sits on the House Armed Services Committee.
