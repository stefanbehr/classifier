The shopping malls have Black Friday. 

Congress has Black December. 

It's happened again. Christmas is coming. And Congress has a mountain of work on its docket. Which means that December on Capitol Hill could well be described as Capitol Hell. 

August's creation of the supercommittee pitched Congress into near-suspended animation for the end of the summer and first part of fall. All eyes were fixed on the supercommittee to see what sort of deficit slashing scheme it could contrive. Would the supercommittee hit its mark of $1.2 trillion in cuts over ten years? Would it "go big" and exceed the target and float several trillion dollars in spending reductions? Would it fall short? Or would it collapse in abject failure? 

Congress punted on most big ticket items in September, October and November as it waited to taste the legislative lager concocted by supercommittee's brewmasters. Any sort of supercommittee agreement would have drastically influenced how Congress might tackle a host of spending and economic measures. It's likely many of those issues would have been wound into the supercommittee's final product, thus leaving lawmakers anxious to see the outcome. 

In the end, the supercommittee flamed out with nary an agreement. That means the legislative quarantine which gripped Capitol Hill since August has been lifted. 

Congress faces an avalanche of issues which must be resolved before the end of the year. In recent years, it's become de rigueur for Congress to labor on major issues right up until the final hours preceding Christmas. It's quite a track record. Drilling in the Arctic National Wildlife Refuge (ANWR). Legislation to provide benefits for 9-11 first responders. Renewing tax cuts. The Senate approved its version of health care reform in a legendary, pre-dawn vote on Christmas Eve morning in 2009. 

So now Congress stares December squarely between the eyes. Santa's not the only one who's been making a list. And the legislative load is so massive, it's doubtful Santa and could wedge it all in that sleigh of his. Which means Congress could once again find itself toiling right up until Christmas. 



Bah humbug. 

Here's some of what's on the agenda: 

The first issue could be the extension of unemployment insurance, known in Washington shorthand as "UI." 

After two months of steady declines, new claims for unemployment benefits rose last week. A supercommittee plan was expected to address the UI issue. But without any resolution, Congress must decide what to do, especially as unemployment hovers around nine percent. 

"We let everybody down with the supercommittee," says Del. Eleanor Holmes Norton (D-DC), Washington's non-voting delegate to Congress. "Let's begin fresh by giving everybody something we know everybody wants. There can't possibly be any controversy on this one." 

Wanna bet? 

Previous efforts to extend unemployment benefits have launched monumental fights on Capitol Hill. The cost and length of the extension has always been at the apex, igniting a classic skirmish between Democrats and Republicans over which party cares the most about those who are down on their luck. This issue centers on the long-term unemployed who have already burned through six months of state unemployment benefits. However, the federal government would not extend UI for more than 99 weeks. 

It's estimated that 2.1 million people could lose unemployment benefits this winter if Congress doesn't move before the end of the year. 

Next up is a debate over renewing the "payroll tax holiday," also set to run out on December 31. In a speech last week in Manchester, NH, President Obama warned that "if Congress refuses to act, then middle-class families are going to get hit with a tax increase at the worst possible time." 

The government could slap the average family with $1,000 in new taxes if Congress doesn't resolve this issue. In a statement, House Speaker John Boehner (R-OH) says he's "ready to have an honest and fruitful discussion" over a payroll tax extension. 

Here's the problem: Congressional sources indicate that at a time of exploding deficits, some fiscal conservatives may demand that these programs be offset with spending cuts. It's believed that UI and the payroll tax moratorium would cost around $200 billion. 

Demanding offsets is important this fiscal climate. But the optics of voting against the unemployed at Christmas is a different enterprise altogether. You'll recall the firestorm that erupted last August when House Majority Leader Eric Cantor (R-VA) called for spending offsets to mitigate the costs of natural disaster relief efforts. 

Then there are jobs bills. 

Boehner and other GOPers are sure to excoriate Senate Democrats for not advancing some 20 bipartisan-passed House packages designed to boost the economy and return people to work. 

Christmas cookie exchanges are a staple of the holiday season. A new custom of Black December is the annual donnybrook over the "doc fix," sometimes referred to as "SGR" (short for "sustainable growth rate"). 

The "doc fix" is a fiscal patch created by Congress to reimburse physicians who accept patients who are on Medicare. The federal government has always wrestled with what's the appropriate reimbursement for Medicare doctors. For many years now, the equation used to pay the doctors didn't balance, with the costs exceeding the money coming in. Thus, the government made up the difference with the "doc fix." Since 2003, Congress has okayed stopgap measures to keep the doctors flush with cash. Last December, lawmakers approved a one-year fix worth $19 billion. 

It was widely thought that the supercommittee might find a remedy to this ongoing problem, especially if it altered the formula for who was eligible for benefits. But without a deal, rank-and-file lawmakers are on their own. And if Congress fails, doctors could either stop seeing Medicare patients, or see them at a 25 percent loss. 

Believe it or not, Congress has to take action again to avert another government shutdown showdown. 

This could prove to be the thorniest issue of all as Congress has failed to approve most of the annual bills which fund the federal government. Lawmakers passed a slate of three spending bills a few weeks ago and simultaneously funded the rest of the government at through December 16. But to avoid a shutdown, lawmakers may have to lasso together the remaining nine appropriations bills into one gigantic package. That could fund the government through next October. Or, lawmakers could cook up yet another interim spending bill to at least get them through the holidays. 

This creates an interesting dilemma for House Republicans. 

As a part of their Pledge to America campaign document released in the fall of 2010, House GOPers promised voters they would run the House differently when compared to the Democrats. One proviso of the Pledge is titled "Advance Legislative Issues One at a Time." It reads as follows: "We will end the practice of packaging unpopular bills with must-pass' legislation to circumvent the will of the American people. Instead, we will advance major legislation one issue at a time." 

For the record, the Pledge to America also contains a section titled "Read the Bill" which promises a "three-day waiting period on all non-emergency legislation." It goes on to say that "legislation should be understood by all interested parties before it is voted on." The House adopted this as a part of its "rules" package for the 112 Congress in January. 

The "three-day rule" is not precisely three days. Under the GOP's metric, "three days" applies if the legislation has been posted for parts of three days. In other words, the House could release the text of a bill at 11:59 pm on a Monday and conceivably vote on it as early as 12:01 am on that Wednesday. 

Republicans made it clear this year that they didn't want to Velcro big spending measures together and aimed to return the appropriations process to "the regular order." But that clearly hasn't happened. Certainly they can blame Senate Democrats for some of that. But at least in one instance, GOP opposition has prevented House Republicans from moving a bill out of subcommittee to full committee and then to the House floor. 

This means the House GOP may have to swallow hard on the "packaging" promise they made to the voters, particularly if the only way to address some of these major legislative headaches and keep the government open is by marrying them together in a gigantic, omnibus bill at the end of the year. 

One wonders how many catcalls there'll be about "reading the bill" then? 

But the most intense debate this December may be the blame game. Congress hasn't been in session since the supercommittee imploded a few days ago. Many stores opened their doors for Black Friday at midnight. Even though it's still November, Congress formally re-opens its doors for Black December on Tuesday. Like the Long Island Walmart employee who was trampled three years ago in a stampede, Members of Congress could trample over one another in the race to cast aspersions on the other side for the supercommittee's failure. They'll certainly deploy a few rounds of rhetorical pepper spray like the disgruntled Black Friday shopper in Los Angeles. 

And you think this December could be bad? Try next December. 

After all, the supercommittee's penalty for coming up short means that mandatory, arbitrary spending cuts worth $1.2 trillion are scheduled to start in January, 2013. Half of those cuts will come from defense. If Congress doesn't "undo" some of those cuts in the next few months, the legislative hand-to-hand combat on tap for Black December of 2012 could make Black December of 2011 look like a walk through Xanadu.
