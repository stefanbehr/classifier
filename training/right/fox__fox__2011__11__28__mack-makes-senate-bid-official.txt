MIAMI-- U.S. Congressman Connie Mack IV officially announced Monday that he is running in the GOP primary for U.S. Senate. 

" I am going to run for the United States Senate because, frankly, I think the people of Florida have had enough, " Mack said to FOX News host Sean Hannity . 

" The people of the state of Florida want somebody who believes in them, not in somebody who believes in more government, " Mack said in a shot against incumbent Democratic Senator Bill Nelson . 

"It's important that Senator Nelson does not win a third term in the United States Senate. We can't afford him in the state of Florida and in the country and I think I'm best positioned to beat Senator Nelson," Mack said. 

Mack still faces a GOP field that includes former U.S. Senator George LeMieux , state House Majority Leader Adam Hasner, retired Army Colonel Mike McCalister, and businessman Craig Miller. However, a recent Quinnipiac poll showed Mack with a large lead, ahead of his closest opponent by 23 percenage points among GOP primary voters. 

Hasner, who has received endorsements from several influential conservative organizations, like Freedomworks and Red State, quickly issued a statement following Mack's announcement. 

"Mack's record of supporting wasteful Washington spending, billions in earmark spending, and taxpayer-funded embryonic stem cell research stands in stark contrast with what Florida Republicans are looking for in their next United States Senator., " Hasner said, also labeling him an "establishment politician." 

Mack's senior advisor confirmed a month ago to AEHQ that Mack intended to run. However tonight is the first time that Mack has officially said he is entering the race.
