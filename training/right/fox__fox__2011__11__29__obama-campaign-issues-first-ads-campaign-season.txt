The Obama 2012 campaign has made its first ad buy of the election season with two spots aimed at building momentum for the president's re-election bid. The ads began airing Monday on satellite vendors such as DIRECTV. 

Despite the Democratic National Committee's relentless, almost daily admonitions of GOP candidate Mitt Romney's political positions, the new ads don't target any candidates. 

"Call the number on your screen or visit JoinObama.com to help build our campaign in your community," the president implores in one spot. "It's up to you to fight for the values we all share. Don't sit this one out. I'd love to see you out there." 

The effort is a mere toe-dipping into what is likely to be a deep pool of money needed this presidential election cycle. A campaign official tells Fox the ads are just a "tiny buy on national satellite," though officials won't define what "tiny" means. 

The Obama camp doesn't want to bust its budget early in a campaign that still has no single opponent. 

During the last quarterly filings the Obama campaign reported raising $43 Million and Romney's camp claimed around $14 Million. 

While Romney is only the sometimes front-runner, he's been in the DNC's crosshairs from the outset. The organization has released a flurry of ads aimed at portraying Romney as his own worst enemy and a flip-flopper. 

Meanwhile, the Romney camp has fired off its own ad, lambasting the president on the economy and angering Obama supporters, who said a quote used in the spot was out of context and a distortion. The Romney camp used Mr. Obama's 2008 quote of a Senator John McCain aide with no attribution, giving the impression the president had made the statement. 

The Romney campaign suggested the bigger picture of Obama's failure to turn around the economy and his subsequent reluctance to talk about it is the salient point. 

Click below to see the Obama 2012 ads. 

Obama2012spot#1 

Obama 2012spot#2
