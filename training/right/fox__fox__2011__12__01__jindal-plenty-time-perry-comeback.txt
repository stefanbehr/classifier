ORLANDO, FL - Louisiana Governor Bobby Jindal , a top Rick Perry backer, warned not to underestimate the Texas governor from making a comeback in the in the GOP presidential primary race despite new poll numbers that show him lagging behind. 

" Rick is a fighter, " Jindal said in an interview with FOX News Thursday. " There is plenty of time left in the primary campaign season." 

Jindal pointed to this year's mercurial political cycle--- that has seen several GOP front-runners come and go--as the reason why Perry's current trajectory could improve. 

" This has probably been one of the most unpredictable primary seasons on the Republican side. Lots of up and downs and there will probably be a lot more changes between now and when the first votes are cast in Iowa and New Hampshire, so there is a lot of time left," Jindal said of the former front-runner. 

Would he support Perry rivals Newt Gingrich or Mitt Romney if either one became the GOP nominee? 

" I will support whoever our nominee is," Jindal replied." Again, I think it is going to be Rick. I am supporting Gov. Perry, but whoever our nominee is, I think, will provide a clear contrast with current administration." 

Jindal, and 26 other Republican governors, are meeting in Florida for their annual conference. 

Jindal, and New Jersey Gov. Chris Christie , a top Romney surrogate, are one of the few GOP governors congregating here that have endorsed a presidential candidate. 

South Carolina Gov. Nikki Haley confirmed to reporters Wednesday that she would " absolutely" make an endorsement before the Iowa Caucuses on January 3rd. 

However, most governors convening here have yet to endorse, or indicated that they would not support anyone from the 2012 Republican field, until after the primaries.
