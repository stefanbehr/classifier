WASHINGTON, DC- Former Vice President, and 2000 Republican presidential candidate, Dan Quayle will endorse GOP presidential contender Mitt Romney for President at a campaign event in Arizona on Tuesday, according to Republican sources. 

Quayle, a former U.S. Senator from Indiana, served as Vice President under President George H.W. Bush. Romney will be campaigning in Paradise Valley, AZ where Quayle owns a home. 

His son, U.S. Representative Ben Quayle, represents the 3rd Congressional district of Arizona.
