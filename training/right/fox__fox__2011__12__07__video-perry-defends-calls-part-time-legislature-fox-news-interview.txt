GOP presidential candidate and Texas Gov. Rick Perry defended his calls for a part-time federal legislature during an interview with Fox News Chief Political Correspondent Carl Cameron Wednesday. 

Perry was in Washington to speak to the Republican Jewish Coalition and talked to Fox News ahead of his speech. 

See the full interview below. 

Watch the latest video at FoxNews.com
