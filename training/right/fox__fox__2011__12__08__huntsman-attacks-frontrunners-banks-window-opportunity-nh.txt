If Mitt Romney places third in Iowa, GOP hopeful Jon Huntsman said Thursday, all bets are off in New Hampshire. 

"If he takes third in Iowa, you're hobbled going into your best state in New Hampshire, already the gap has closed," Huntsman said when asked what would happen if Mitt Romney places behind Newt Gingrich and Ron Paul in the Hawkeye State. "And that opens a whole window of opportunity for us." 

Calling Newt Gingrich and Ron Paul "unpredictable," Huntsman said the Republican field is in "tremendous flux," especially since independents, not just Republicans, will be able to cast a vote in the Granite State primary. The former Utah governor and U.S. ambassador to China has slowly risen to fourth place in New Hampshire, where he has devoted the majority of his resources and plans to double down until January 10. 

Huntsman was at the National Press Club Thursday delivering what his campaign billed as "his closing argument to voters in the lead-up to the New Hampshire Primary ." Among the many proposals in his seven-point plan, Huntsman called for imposing a fee on banks whose assets exceed a certain percentage of the GDP, as well as placing 12-year term limits in Congress and prohibiting members of Congress and White House staff from lobbying on any issue area for which they had oversight responsibility for four years after leaving their positions. 

"America suffers from a deficit of dollars and jobs," he said. "We also suffer from a deficit of trust -- trust in our institutions of power, from Washington to Wall Street." 

Taking a populist tone that attacked "cronyism" in the nation's capital, Huntsman took on the frontrunners for not having the ability, like President Obama he argued, to restore trust in Washington. 

"Governor Romney will say anything to earn the voters' trust," he said. "We are in this mess because there are already enough people in Washington who make a career out of telling people what they want to hear. Newt Gingrich is a product of that same Washington, who participated in the excesses of our broken and polarized political system. You may not agree with me on every single issue. But you'll always know exactly where I stand, and I will never waver from my conservative convictions." 

Huntsman was even sharper in the question and answer session that followed. "You have a choice between a panderer-in-chief, a lobbyist-in-chief, or a commander-in-chief. I want to be that commander in chief." 

Gingrich has vaulted to first place in New Hampshire polls and, asked about opponent's rise, Huntsman said, "We can live with that, and I think it has legs. December is a chopped up month in terms of holidays, so you look at the timing remaining for people to actually change that dynamic. I don't think that's a dynamic that's easily changed." 

But Huntsman said his chances remain good. "I was just looking at, you know, McCain's numbers four years ago in New Hampshire, you know, low double digits. And then by the end of December...people began to coalesce around candidates. And indeed I think the first week of January, everybody moves. I think we're going to see tremendous moves towards candidates at that point. It may look a whole lot different than it looks today." 

Asked about his fundraising, Huntsman said, "As we go up in the polls, the marketplace works. You know, the contributions follow." 

Huntsman's wife, Mary Kaye, told Fox News after the speech that she sees resemblances between the rise of her husband's candidacy in New Hampshire and his successful campaign to become governor of Utah. 

"Just as when he ran for governor, I watched him do the same thing. It was a slow, steady rise. And he was running against, you know, eight people actually in the race at the time. And he was, what I called back then, the most undervalued stock. And he began to gain traction and became the most valued stock in the end, and I think the same thing's going to happen here."
