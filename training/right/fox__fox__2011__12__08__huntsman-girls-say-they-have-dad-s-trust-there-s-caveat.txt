Jon Huntsman's three oldest daughters, widely known by their Twitter handle @Jon2012girls, enjoy an online reputation of being funny and irreverent, even controversial. But even with their father's trust, they are being closely watched for tweets that could cause the campaign headaches. 

"He's starting to trust me more on this, but um I have parental supervision for all the debates now, which is why we haven't seen too many inappropriate tweets," Liddy Huntsman said. "But we're having fun." 

Liddy was the Huntsman daughter to author the controversial -- and wildly circulated -- tweet one debate night in October: "How does Romney know anything about China ? He's only been there once and that was for the Olympics. Panda express doesn't count." The off-message post gave her father fresh buzz, but reportedly unnerved his staff who hadn't been consulted. 

Mary Anne, Abby, and Liddy were in the audience Thursday for their father's speech at the National Press Club about "restoring trust" in Washington. 

"We're very close," Abby said of their relationship with their father. "We talk about everything, so that's why I don't think -- well we hope we don't embarrass him much -- but you know I think he trusts us and knows that we have his best interests at heart at the end of the day." 

The sisters started authoring the tweets as a way to keep in touch with their friends because, as Liddy explained, "massive emails were not working." 



"Obviously when we started this Twitter account we didn't realize it would go as viral as it did," said Mary Anne. 

The Huntsman girls say they enjoy the feedback and advice they've received via Twitter, which they've sometimes even relayed to their father. And while there are reports that their online escapades are making staffers jittery, the daughters have a different take on what the campaign thinks of them. 

"They're definitely calling us their secret weapons right now," said Mary Anne, who added, "They have been great with letting us do our own thing. They've trusted us." 

"And we're financially friendly," Abby cracked, which drew laughter from her sisters.
