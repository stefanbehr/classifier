Former New Hampshire Governor John Sununu was once dubbed "Bush's Bad Cop" by Time magazine for his reputation to play hardball when he served as President George H. W Bush's Chief of Staff. Thursday, along with his new partner, former Sen. Jim Talent, the two top Mitt Romney surrogates played a rendition of "Bad Cop/ Bad Cop" on Romney rival Newt Gingrich , unleashing on the former Speaker of the House and Iowa front-runner for being an unreliable conservative. 

"The Speaker is running as a reliable and trusted conservative leader. What we're here to say, with reluctance, but clearly, is he is not a reliable or trustworthy conservative leader, " Talent said on the conference call with members of the press. 

The two also thrashed Gingrich for his controversial comments made in May about the budget plan Republican budgetary golden boy Paul Ryan that nearly derailed his campaign. During a an interview on NBC's Meet the Press, Gingrich described the Ryan Plan as "right-wing social engineering." 

"Gingrich's under-cutting of Paul Ryan proves that he's more concerned about Newt Gingrich that he is about conservative principles," Sununu blasted. " Newt Gingrich cares about Newt Gingrich ." 

"What he did to Paul Ryan is a perfect example of the irrational behavior that you don't want in a commander in chief, " Sununu continued. 

Talent, who was a bit reluctant to criticize, also said Democrats would have a field day if Gingrich became the eventual nominee. 

"My concern is that if he is the nominee, and I say this as a Republican, this election is going to be about him and that's exactly what the Democrats want," Talent said. "This is a time for contrast, and contrast on the record, and that's what we are doing." 

Talent, a GOP congressman in the '90s under Gingrich, also took a shot at his leadership while Speaker of The House. 

"We also reached that conclusion after four years that we could not go on with him as our leader and continue accomplishing things," Talent added. "We we're in a situation where we would get up every morning and you would have to check the newspapers...to check what the Speaker had said that day that you were going to have to clean up."
