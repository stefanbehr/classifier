House GOP files revamped Payroll tax bill with Keystone pipeline provision attached 

FOX News has learned that House Republicans have filed a revamped payroll tax bill, adjusted with the Keystone pipeline language attached, with the House Rules Committee. 

The bill should be ready for public viewing soon. 

The bill is HR 3630. House Ways and Means Committee Chairman Dave Camp (R-MI) is the lead sponsor alongside Reps. Dan Lungren (R-CA), Frank Lucas (R-OK), Fred Upton (R-MI) and Ileana Ros-Lehtinen (R-FL). 

Sources say this bill comes in around $175-180 billion. 

The Keystone pipeline is a particular sweetener to the GOP. 

Many of the payfors are spread across 12 distinct areas, many of which were provisions already proposed by the president to the super committee. 

Subsidy payments on 1099's: must repay it. 

About $20 billion would come from spectrum auction. 

This is the preliminary plan: 

The bill is expected to be posted sometime Saturday, go to the Rules Committee on Monday and perhaps be ready for the floor as early as next Tuesday. 

Now this is the scenario where things get interesting: if the House passes this bill and then the omnibus (conceivably Wednesday), the GOP leadership may send everyone home and try to force the Senate to accept the House version of the payroll tax bill. 

That would cheer the conservatives if they can stick it to the Senate and force a confrontation with the President. 

This is exactly what Boehner needs at this time to gin up the conservatives who are seething about how things have gone this year and were luke-warm about voting for the payroll tax bill in the first place.
