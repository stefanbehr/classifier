Santa Claus came to town early this year -- in "secret" fashion -- spreading good tidings of great joy in one unlikely place -- the U.S. Senate. 

Most gift-givers would probably give lawmakers lumps of coal at this point -- and the black gem actually did surface in Monday night's mystery gift exchange so common to many workplaces. 

But senators participating in the "Secret Santa" exchange kept it mostly safe this season, sticking to their inner circle in a bipartisan manner that seemed to vanquish the partisan Scrooginess -- however fleetingly -- that has haunted the corridors of the Capitol all year. 

On Dasher! On Dancer! 

One by one, senators gleefully distributed their holiday gifts, most with a home state theme, some modeling their treasure as if it were a Red Rider BB Gun ("with the compass in the stock and this thing which tells time"). 

Sixty-one members fell under the influence of Yuletide cheer as each unmasked his and her Secret Santa, a creation of Sens. Al Franken , D-Minn., Mike Johanns , R-Neb, and a handful of their colleagues. 

Best in show (after all, these are competitive people) has to go to West Virginia's Joe Manchin who gave his fellow Democrat, Chuck Schumer, a natural resource from The Mountain State paired with a little riddle attached to cloak his identity: "With the Senate in a deadlock with 8 percent approval ratings, what we deserve is a lump of coal ... We should be working in a bipartisan way." 

Schumer quickly sussed out his Secret Santa and bounced out of the gift room giddy with excitement. 

"Look! Carved out of coal!" city-boy Schumer marveled with a wide-eyed smile as he paraded around, a la Vanna White, a tiny donkey and elephant carved from the combustible substance. Reporters, happy to cover something besides the payroll tax, oogled the bounty, noticing that each felt hard and smooth like plastic. 

"That's the firing process after they're polished," Manchin schooled the naive city-dwelling press corps. 

Funny enough, that wasn't the only gift of coal. 

Pulling his gifts from a brown paper sack worthy of Chinese takeout, Manchin proudly showed off an old, gray gym sock. As reporters looked on quizzically, the former governor extracted a baggie with three large, black lumps. 

"Coal!" Manchin hooted enthusiastically. "They have it in Colorado, too!" 

Quite the learning experience for the normally cynical scribes, who couldn't help but share in a bit of the excitement, despite the mocking of some even more cynical staffers. 

Indeed, The Centennial State's Mark Udall was Manchin's Secret Santa, though he made the gift a little easier to swallow with a six-pack of Colorado "Snow Day" beer and hiking socks (Udall is an avid mountain climber). 

For a brief moment, the Mansfield Room just off the Senate floor resembled the living room of Ralphie and Randy -- minus the Zeppelin and pink bunny PJs. Gifts were stacked high on table tops, many certainly in breech of the Secret Santa decree that they be valued under $10, like the $20 Barnes and Noble gift card for Wyoming's John Barrasso. Senators eagerly searched for their loot. 

"Wicker! One of those with an 'er'," cried Sen. Dianne Feinstein, D-Calif., as she revealed the recipient of her gift and revealed a slightly-challenged knowledge of her conservative brethren (she first said Vitter). 

Sen. Roger Wicker of Mississippi is now the proud owner of "Unbroken" by "Seabiscuit" author Lauren Hillenbrand. It's a World War II true story of survival. 

The second prize for the gift of creativity (if not thriftiness) would have to go to Sen. Kent Conrad, D-N.D., though the recipient of his gift likely wouldn't agree. 

"Look, I think we have to all tighten our belts ... because of the European debt crisis," the wonky Banking Committee chairman said after delivering an empty box that once housed popcorn (that coal was looking better and better). Conrad actually re-gifted the box to Louisiana's Mary Landrieu -- Dick Durbin's name apparently was still on the present. 

For Conrad, his gift was a campaign-related promise fulfilled. The senator once told Kay Bailey Hutchison, if she won her race for Texas governor, he'd take on the job as top cop of her Border Patrol. And though KBH lost to Rick Perry, she brought the chairman a token, anyway -- a black and white hat emblazoned with his would-be employer: Texas Border Patrol. Four gold stars adorned the brim. A card attached read simply, "Keeping my promise." 

A little last-minute homework assignment for Secret Santa Schumer resulted in a bottle of the famous Original Anchor Bar buffalo wing sauce for Johanns. The Nebraska Republican's response? "Way cool!" 

Sen. Kirsten Gillibrand waved around her "unbreakable Christmas ornament" - a hand-painted, red and white ball. 

"Important for a household with a 3-year old," the New York mom said. But her Secret Santa kept up the mystery, with no name tag included. Never fear, though. The crafty press corps, exercising some well-honed (albeit rusty, at this point) reporting skills, discovered an artist's signature card inside the box with the area code "251" - Alabama! 

And since this reporter had already spotted Richard Shelby , the senior Alabama senator, with a large, gift-wrapped rectangular box, she deduced Jeff Sessions -- Alabama's junior senator -- had to be the giver. 

Reporters marveled at Jay Rockefeller who said he had shopped for his own gift. The millionaire Dem then returned from the gift room with his mouth full, chocolate staining his lips, carrying a red sack with gifts from the Indiannapolis Motor Speedway courtesy of Dick Lugar. 

"Dick is now responsible for the 25 pounds I'm going to gain," the West Virginia Dem mumbled, still slurping the sweet treat. 

Ohio's Rob Portman , trailing a strong scent of cinnamon, showed off two boxes, one with Moravian (by way of North Carolina) sugar cake and the other with spice cookies from his Secret Santa, Kay Hagan. 

"My wife's gonna love this," said the former Bush administration OMB director. 

If only his experience on the Super Committee had gone so well. Great Lakes beer from Portman to Rockies' dwelling Mike Bennet. "We make beer!" said the Ohio senator to a skeptical press corps. 

From Ben Nelson - a $10 coffee card tucked in a cute little green and red felt mini-stocking for North Dakota's Tim Johnson. 

A bottle of Oregon wine for Sen. David Vitter , R-La. 

From Dick Blumenthal, cookies for Lugar. 

"Merry Christmas!" the Connecticut Dem wished his colleague. 

Macadamia nuts and coffee from Hawaii's Dan Inouye for Ben Nelson. Mela Kalikimaka. 

Dick Durbin bought "Lincoln's War" for fellow Dem Bob Casey. A long note inside clearly touched the Pennsylvania senator, who held the book tightly, saying only, "Oh that's very nice. Very nice." 

A three-pack of salsa from the Lone Star State's John Cornyn to Chris Coons, who often exercises with the Texas Republican. "Very generous but profoundly subversive to my dietary aspirations," joked the Delaware Dem. 

Sen John Hoeven, R-N.D., got ear buds in the shape of bees. 

Pure milk chocolate almond bark to Sen. Amy Klobuchar from Massachusetts Scott Brown , made by his state's famous Trappistine Nuns. 

"Wishing you a wild and scenic Christmas," read the card on the Penguin wrapping paper enveloping the gift from Klobuchar. The Minnesota Dem arrived too late for recipient Jeff Bingaman of New Mexico. No hints at the box's content. 

All in all, the event appeared to be a smashing success. If only the end-of-year legislative process could go so well. 

O Come All Ye Faithful...
