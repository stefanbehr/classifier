While giving a speech at the University of Iowa Medical School on the importance of increasing research related to brain diseases, Newt Gingrich had an up close and personal encounter with Occupy Wall St. Members of the movement were seated amongst the standing room only crowd, interrupting him as he began talking. 

The protesters appeared very organized, as an elderly woman began shouting "mic check!", echoed by about 10-15 others, recreating the "people's mic" occupiers used in New York City in place of a speaker system. 

Reading from a prepared statement she continued, "Mister Speaker we are here to protest your speech today. We object to your callous and arrogant attitude toward poverty and poor people." 

When she was removed from the audience by security, another man picked up where she left off, until he too was taken outside. The remaining occupiers quieted down and listened to Mr Gingrich's speech. 

However, not everyone was impressed, with people in the crowd booing them and one Gingrich supporter standing up and saying, "We came to listen to him, not to you." 

Later, while answering questions from the audience, an older man standing on the side accused Gingrich of taking "shortcuts" to earn millions of dollars through book deals, and declared he had "a PhD in cheating on your wife," a reference to his admitted past indiscretions. 

Bristling at the accusation, Gingrich fired back, "How would you know anything about how I published and sold books...The fact that I happen to write books that people like may bother you but it doesn't mean the books were wrong." 

He then walked out the door, smiling and waving as the crowd applauded.
