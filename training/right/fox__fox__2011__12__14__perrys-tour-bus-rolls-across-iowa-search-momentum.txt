Texas Gov. Rick Perry may get the second chance he's asking Iowa voters to give him. 

As Perry starts his 42-stop bus tour through Iowa, his campaign claims the momentum is building. 

Nationally, Perry's poll numbers are anemic at best. But pollster Scott Rasmussen says he will release a new poll Thursday that shows Perry's poll numbers in Iowa are up slightly. 

Rasmussen tells Fox News that means Perry could capture a lot of undecided voters in the Hawkeye State come January 3rd. 

The campaign is hoping barnstorming across Iowa over the holidays will showcase what Perry spokesman Ray Sullivan calls their best asset-- Perry's "unrivaled ability to personally connect with Republican voters in traditional retail campaign venues." 

Polls and history say 50-percent of the 120,000 likely to caucus in Iowa are undecided, or could change their minds. 

Sullivan insists, "The momentum is real, and we believe our strong Iowa grassroots organization, Iowa media efforts and Perry's retail campaign skills will make us even strong heading into the January 3rd Iowa caucuses ." 

Perry has several ads playing on TV and radio in Iowa. And a strong debate performance at the Fox News debate in Sioux City would be big. 

Herman Cain's exit has changed things up in the battle for Iowa. The undecided element in Iowa includes both evangelicals and homeschoolers. Perry, Bachmann and former Senator Rick Santorum are strong contenders for those votes. 

Congresswoman Michele Bachmann has picked up some of Cain's staff and volunteers. She's planning an 8 day bus tour through all 99 counties. 

Perry is getting some displaced Cain supporters and is pushing hard for evangelicals. 

Santorum who has spent the most time of any candidate in Iowa, is courting homeschoolers aggressively.
