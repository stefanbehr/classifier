Chris Stirewalt checks in from Sioux City, Iowa, the site of Thursday's FOX News GOP presidential debate. 

Doug Heye of the Iowa GOP joins Chris and the two break down the latest action on the campaign trail in the Hakeye State. 

Heye points out that Ron Paul has a strong organization in Iowa and could surprise the experts with a serious showing there. "The race is so fluid," said Heye. "We just don't know." 

Watch Power Play Live Monday-Friday at 11:30am ET here: http://live.foxnews.com/ 

Watch the latest video at FoxNews.com
