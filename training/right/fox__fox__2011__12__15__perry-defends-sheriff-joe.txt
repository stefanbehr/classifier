Governor Rick Perry is coming to the defense of Arizona Sheriff Joe Arpaio , in light of a scathing Justice Department report against the self-proclaimed toughest sheriff in America. Feds claim a 3-year probe into the Maricopa County Sheriff's Office shows Arpaio committed a wide range of civil rights violations against Latinos through racial profiling and discrimination. 

Perry called the charges little more than a political hit job during an interview Thursday with Fox's Neil Cavuto. "I don't know what all the details are but I do know this, that nothing surprises me out of this administration," Governor Perry said. "I would suggest to you that these people are after Sheriff Joe, he is tough, and again when I'm president of the United States you're not gonna see me going after states like Arizona." 

The Perry camp went even further, releasing this statement: "Governor Perry knows Sheriff Arpaio as a dedicated law enforcement professional fighting to keep his neighbors safe in the wake of federal failures to secure the border and deal with border crime. The Justice Department's action smacks of politics, as do so many of the Obama Administration's misguided actions and excuses." 

In addition to the civil rights investigation over his infamous immigration sweeps, a federal grand jury has been looking into whether Arpaio's office abused its power by using excessive force against Latinos. As part of the report, Arpaio must set up new policies against discrimination, improve training, and make other changes that would be monitored by a judge. Arpaio has until January fourth to decide if he wants to comply. 

Perry secured Sheriff Joe's endorsement less than three weeks ago.
