GREENVILLE, South Carolina -- Governor Mitt Romney is clarifying remarks he made earlier today suggesting he didn't know the difference between the government health care programs Medicare and Medicaid prior to running for the Senate in 1994. 

While answering a question about entitlement reform during a rally at Missouri Valley Steel in Sioux City, Iowa Mr. Romney told the audience that at one point he was not entirely clear about the difference between Medicare and Medicaid. "You wonder what Medicaid is, those that are not in all this government stuff," Mr. Romney said. "I have to admit I didn't know all the differences between these things before I got into government. And then I got into it and understood that Medicaid is the health care program for the poor, by and large." 

Medicare provides coverage for Americans over the age of 65, while Medicaid provides care for the poor. Millions use the programs as their primary insurance. 

The Democratic National Committee immediately pounced on the comment, issuing a statement from Congresswoman Debbie Wasserman-Schultz. "One has to wonder how Mitt Romney thinks he can represent American workers, their families and seniors when his concern for the poor and the middle class comes across like an afterthought... just as he was as a Governor, Mitt Romney as president would be a disaster for seniors and folks trying to make ends meet." 

Aboard a chartered plane to Greenville, SC, Romney told reporters he knew the difference between the two programs, but was not familiar with the intricacies of the programs until his Senate run in 1994. 

He went on to say that he had invested in a health care consulting company before running for the Senate. 

The Romney campaign calls the attacks a distraction, "Instead of focusing on out-of-control spending and record unemployment, President Obama and his political machine are focused on campaigning to try and tear down Mitt Romney," said Romney Spokesperson Andrea Saul.
