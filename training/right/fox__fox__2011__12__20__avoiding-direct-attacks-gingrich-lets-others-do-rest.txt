Hiawatha, IA - Newt Gingrich , the self-described Mr. Nice Guy in the race, is trying a gloves-on approach in Iowa but isn't stopping anyone else from making the hits for him. 

With his poll numbers slipping as Mitt Romney and Ron Paul affiliated supporters continuing to flood Iowa airwaves with ads skewering Gingrich, the former House speaker repeatedly called on audience members fed up with negative campaigning to let candidates know "it demeans America and the only person it helps is Barack Obama ." 

He joked, "Every once in a while, I slip, when they get my goat and I can't quite help myself, but overall I think I've done a pretty good job of staying focused on issues." 

As he demonstrably tries to play a clean game, Gingrich isn't playing the fool. An audience member, clearly bothered by the tenor of the attack ads, asked the candidate who was behind them at an apparel printing company Monday. 

Gingrich at first said "I don't know," but as the questioner kept on, Gingrich continued, "Yeah, somebody's super PAC but I don't know which one is which frankly. Restore our - is it restore our future?"He called on his spokesman R.C. Hammond in the audience, "Which one is that?" To which Hammond replied, "I believe it's Romney." 

Gingrich said, "Oh okay. Well that gets to my point. If you see Romney, ask him to take them off the air. It would be nice if candidates were responsible for the things being done by the people who know them personally who are trying to help him get elected. My plea to the people across the state during the next fourteen days is simple, when you see one of these guys, ask them, how can you keep running this negative stuff?" 

And there was another moment in Davenport where Gingrich tried his imperfect method of staying above the fray. 

A reporter in Davenport followed up with Gingrich on his statement that Kim Jong Il's death is an opportunity for voters to consider how "dangerous" it would be for American to elect a isolationist president and asked whether that was directed at Ron Paul's foreign policy position. Gingrich demurred with the answer, "I described my position." 

The reporter loudly insisted that it was a direct aim, and Gingrich responded flatly, "If you want to draw that conclusion, I can't stop you." 

Sure enough, Tuesday morning, the headline in Politico read "Newt Suggests Paul Unprepared."
