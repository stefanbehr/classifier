Rick Perry reached out for a little help from one of his biggest supporters Tuesday, with Louisiana Governor Bobby Jindal joining him on the campaign trail. 

While answering a question during a town hall meeting in Maquoketa, Iowa, Perry forgot about the about the standard deduction he built into his own flat tax plan, saying it would be gone. 

Jindal quickly chimed in to correct him. "You actually keep the standard deduction in your flat tax," said the popular Bayou State Republican. 

"Oh that's right, as a matter of fact we raise it to $12,500, uh, per family I think," Perry replied. "Thank you for correcting me on that governor... not that I ever make a mistake." 

Jindal is stumping with Perry Tuesday and Wednesday as the Texas governor wraps up the first leg of his 42 city Iowa bus tour. 

The popular Louisiana Governor's endorsement was highly coveted in the Republican field and he announced his support for Perry's candidacy back in September.
