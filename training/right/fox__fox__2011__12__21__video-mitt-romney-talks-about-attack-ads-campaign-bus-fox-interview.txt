MANCHESTER, N.H. -- Republican presidential candidate Mitt Romney says he doesn't have much control over the negative attack ads run by the so-called superPACs. 

He's been roiled in a back and forth with rival Newt Gingrich , who has called the ads a smear campaign, but also acknowledged that they've been working. 

Gingrich was surging in the polls a few weeks back, but his support has waned recently. 

A pro-Romney group, Restore our Future, rolled out a new ad today hitting on what they call Newt's baggage, saying he has more than the "airlines" highlighting some of his congressional record as speaker of the house. 

Romney tells Fox News' Chief Political Correspondent in an interview aboard his campaign bus touring through New Hampshire, that Newt hasn't been all positive. 

"Well he's obviously engaged in a pretty negative series of attacks himself . Look a campaign is about pointing out differences, as you know under the law, I cannot direct the advertisements of PACs and I have to keep a distance from those. Could I come out and attack the PACs, sure by why would I come out and attack the ability of people to get to know differences among the candidates." 

Gingrich's drumbeat for the last few weeks has been that he will run a positive campaign, and only respond when attacked directly. 

Romney suggested that maybe he is being a little whiny. 

"Look if someone can't handle the heat in this kitchen, how are they going to handle Obama's hell's kitchen which is coming down the road. We're going to see people point out the differences between us. And I'm a big boy I can handle that I know there's going to be attacks coming my way." 

Asked directly if he's seen the ads. He said "I haven't seen all the ads, no." 

Watch the full interview: 



Watch the latest video at FoxNews.com
