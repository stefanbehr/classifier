In an interview with the "Houston Chronicle", former President George H.W. Bush said he is publically throwing his support behing Mitt Romney . But, Bush's advisors are hesitant to classify it as an "official endorsement," in a sign of respect for the current Texas Gov. Rick Perry , who is also vying for the Republican presidential nomination. 

"I think Romney is the best choice for us," former President Bush told the Texas newspaper. "I like Perry, but he doesn't seem to be going anywhere; he's not surging forward." 

According to the Houston Chronicle, Bush also said he is supporting the former Massachusetts governor because he knew his father , George, for many years---- A former Republican governor of Michigan who ran for president in 1968. 

"I just think he's mature and reasonable - not a bomb-thrower, " Bush said about Mitt Romney . "Stability, experience, principles. He's a fine person." 

"I've got to be a little careful, because I like Perry; he's our governor," the former president added. 

Romney placed a call on Thursday to thank President Bush for his public support, Andrea Saul, Romney's national press secretary told FOX News. 

The former president was also carefully honest about he thought about Romney's chief primary opponent Newt Gingrich , the former Speaker of the House. 

"I'm not his biggest advocate," he said. "I had a conflict with him at one point." 

In 1990, after reneging on his " No New Taxes" promise dues to a arecession, Bush needed Republican lawmakers to stand behind him. Gingrich, then the House Whip, initially said he would support him but in the end was a no-show for an event at the White House . 

"He was there, right outside the Oval Office. I met with all the Republican leaders, all the Democratic leaders," Bush recalled to the Houston Chronicle. "The plan was, we were all going to walk out into the Rose Garden and announce this deal. Newt was right there. Got ready to go out in the Rose Garden, and I said, Where's Gingrich?' Went up to Capitol Hill . He was here a minute ago. Went up there and started lobbying against the thing. 

Pery's campaign tells FOX News that the govenor was not expecting Bush's support-- He supported Kay Bailey Hutchinson in the brutal GOP primary battle against Perry in 2010. 

"Governor Perry has the upmost respect and admiration for President Bush, he always has and always will," Ray Sullivan of the Perry campaign stated. 

*FOX News' Mike Emanuel, and Lexi Stemple contributed to this report.
