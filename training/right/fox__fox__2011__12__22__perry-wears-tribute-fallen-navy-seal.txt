Governor Rick Perry reached out to connect with Iowa voters on a deep level today. 

While in Burlington, the GOP Presidential hopeful shared a personal story all about the band he now wears on his right wrist. It's an ordinary band, but with extraordinary meaning. 

"I wear a little band, as a matter of fact one of your Iowans, who was killed on the 6th day of August this last year," Perry said. 

"He was one of those Navy SEALs who went down in the helicopter, Jon Tumilson, to remind me about my duty to make sure that those young men and women have the support they need as they serve on active duty, but also the support that they need when they come home," Perry continued to explain. 

The wristband, which reads "K.I.A. 8-6-11 Remember J.T.," was given to Perry by members of Tumilson's family at a church service this past Sunday. 

Petty Officer Jon Tumilson died when a rocket-propelled grenade disabled the helicopter he was in. He was one of 30 Americans killed in that crash. The 35 year old was posthumously awarded the Purple Heart. 

More than a 1,000 mourners paid their respects to the fallen Navy Seal in his home town in Iowa. His best friend, a chocolate lab named Hawkeye was among them. Hawkeye's loyalty and love for Tumilson was visibly apparent. Who could forget the heart-wrenching image of Hawkeye laying by Tumilson's casket and refusing to move.
