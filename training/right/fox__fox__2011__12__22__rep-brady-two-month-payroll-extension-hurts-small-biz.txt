GOP payroll tax negotiator, Congressman Kevin Brady (R-Texas) says a two month payroll tax extension will hurt small businesses. 

Brady, Vice Chairman of the House Joint Economic Committee, explains that businesses work on a quarterly basis, so a two month extension amounts to a financial nightmare because it won't allow them to hire more workers. 

Brady, who is hoping to see Democratic counterparts join the negotiating, tells Power Play it's lonely in the Capitol. Brady kids that Congress is doing its best to ruin the Christmas holiday. But, he insists hope springs eternal and while he really wants to be home for Christmas, he'd like Power Play to tell his wife and kids he loves them if he doesn't make it home. 

Watch Power Play every weekday at 11:30ET at live.foxnews.com
