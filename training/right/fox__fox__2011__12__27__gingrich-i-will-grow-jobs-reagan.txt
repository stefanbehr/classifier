Dubuque, Iowa - Continuing to contrast himself against Mitt Romney , the "Massachusetts moderate," Newt Gingrich declared he would grow the economy as president by reining in the government the way he had done with Ronald Reagan and Jack Kemp. 

"In 1982, Reagan was talked into a tax increase, I opposed it. Kemp and I led the fight against it. Reagan said it was a mistake; it was the biggest single mistake of his administration. In 1990, President Bush wanted to raise taxes and I opposed it. We had a huge fight. The majority of Republicans voted no. And this fight still goes on today." 

Gingrich said he had experienced this standoff twice already and had the experience to win the current standoff between tax cutters and tax hikers, putting the front-runner Mitt Romney in the latter category. 

"He's a fine person, I've said that publicly," Gingrich said of the national front-runner whose negative ads have undercut the former House Speaker's position in the polls. "He is a good manager, he is a competent. He did very, very well with the Winter Olympics but there's a huge difference between the philosophy of a supply side conservative in the Kemp-Reagan tradition, and the philosophy of a Massachusetts moderate. I would dramatically cut taxes." 

The former House Speaker has been criticized for taking credit for the achievements of former President Bill Clinton and Gingrich said he "gets to claim half the credit" for the "bipartisan effort." 

"I was effective. I actually got the deal done," Gingrich said. "So it wasn't 112% pure. But it worked. And we got a liberal Democrat to sign welfare reform, we got a liberal Democrat to sign tax cuts, we got a liberal Democrat to sign four consecutive balance budgets. Now I think that's pretty effective conservatism."
