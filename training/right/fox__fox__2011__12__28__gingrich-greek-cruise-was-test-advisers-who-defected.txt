Mason City, Iowa Newt Gingrich offered his own version of campaign history Wednesday, saying the luxury cruise he took in the Greek Isles with his wife which contributed to a major staff exodus from his campaign was part of a grand design to weed out consultants who didn t agree with him because he is a different kind of candidate. 

I wanted to force either they would like to be the advisers to my campaign or they needed to leave because I couldn t be the candidate for their campaign, 

Gingrich told reporters at a media avail. And I think it worked pretty well. Within two hours of their leaving, we were back on track. We have been growing ever since." 

The former House Speaker said he s pretty happy with where the campaign is, although the latest CNN poll of Iowa has Newt Gingrich dropping to fourth place at 14% behind Romney, Paul, and Santorum. 

Gingrich told reporters the couple had been planning the trip to the Greek Isles since January because they needed time to think, but his consultants didn t understand I am a different kind of candidate. I m determined to be positive. I m determined to talk about big ideas We think ideas matter. The consultants found this very mystifying. Very strange. 

In Mt Pleasant a week ago, Gingrich said he nearly dropped out of the race when his top advisers quit but Callista convinced him otherwise, which makes Wednesday s account a dramatic reversal of his version of the story.
