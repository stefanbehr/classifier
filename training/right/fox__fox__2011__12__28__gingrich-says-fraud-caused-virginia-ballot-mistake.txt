Newt Gingrich says the root cause of his failure to qualify for the Virginia primary ballot was a worker who submitted fraudulent signatures. 

The former House Speaker, who had enough of his signatures thrown out by the Republican Party of Virginia that he failed to meet the filing rules, told a woman in Algona that what happened in Virginia "was just a mistake." 

"We hired somebody who turned in false signatures. We turned in 11,100 -- we needed 10,000 -- 1,500 of them were by one guy who frankly committed fraud," Gingrich said, in a moment captured on camera by CNN. 

The Gingrich campaign tells Fox News they are still evaluating their options in Virginia: whether to sue to get on the ballot, which is the strategy Governor Rick Perry s campaign has taken, or whether to force the Virginia Board of Elections to allow write-ins. The Virginia legislature convenes January 11. 

"I would like to be on the ballot or I would like to have the legislature give us the ability to have a write-in campaign," the presidential hopeful told reporters Wednesday morning. "Every poll in Virginia shows me beating Romney in Virginia so I would love to be able to at least have a write-in campaign." 

Both Mitt Romney and Ron Paul exceeded the ten thousand signature quota necessary to get on the Virginia primary ballot. Perry and Gingrich both did too, but not beyond the margin of error, and their efforts were tossed out. 

The former House Speaker even took a detour from the early primary states last week to rally support in Virginia, where Gingrich pre-emptively announced he had gathered enough signatures necessary, proving -- he argued back then -- that he had the organization needed to win the nomination. 

His campaign has confirmed reports that they paid volunteers to gather signatures in states that required large numbers of them; these states were Illinois, Ohio, Tennessee and Indiana, in addition to the state of Virginia. 

Fox News Senior Producer Lexi Stemple contributed to this report.
