Storm Lake, Iowa -- Newt Gingrich routinely fields questions during his campaign stops and during the event showcasing Art Laffer's endorsement, a woman who had been sititing on stage behind him asked Gingrich for clarification about President Obama's country of birth. 

"Why doesn't Barack Obama not have to exactly prove his citizenship as he's going to all of these other countries besides leading our country." She added, "Arizona, if I have my facts correct, they are refusing to put his name on the ballot because he hasn't proven that he is a citizen. Is that true?" 

"No," Gingrich said. He joked, "I thought you were going to ask me whether Donald Trump had citizenship," referring to the attention the reality television star stirred up when he raised questions about President Obama's birthplace, prompting the White House to release his long-form birth certificate. 

"All I can report is the state of Hawaii has certified that he was born there," Gingrich continued. 

Gesturing to his wife beside him, he said, "We both were with a taxi driver one day who showed us the hospital. There is every reason to believe he is a citizen of the United States. The fact that he's already a terrible president, we don't have to go beyond that and try to find something beyond that." 

Some members of the audience began to clap. 

"In all fairness, this is one of those issues where it's a fact: he is the president of the United States. Therefore, at a factual level, citizenship is a moot issue. He is the president. He's not going to lose the presidency over that. He will lose the presidency because all of us will vote him out, which is the American way of doing it."
