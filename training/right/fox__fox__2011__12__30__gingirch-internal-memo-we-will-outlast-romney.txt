Des Moines, Iowa -- Newt Gingrich predicted he would finish maybe third or fourth in Iowa, but he didn't expect the possibility of fifth place. 

A new NBC/Marist poll in Iowa shows Gingrich at 13 percent behind Mitt Romney at 23 percent, Ron Paul at 21 percent, Rick Santorum at 15 percent, and Rick Perry at 14percent. Only Michele Bachmann, also a former frontrunner, is behind him at 6 percent 

Still, even before the NBC poll was released, senior strategist Kevin Kellems circulated an internal memo reassuring the staff that they are "about where we always thought we'd be at this point in the race. In fact, we are in a stronger position than necessary at this stage." 

The memo, obtained by Fox News, underscores what the campaign considers Gingrich's "long-term brand strength" as the reason he will prevail to win the nomination. 

"We have a strategic advantage over the rest of the field, including Romney - due to Newt's long-term brand strength as well as regional strength in South Carolina and Florida," Kellems wrote. "He's been around long enough and has been tested; people know Newt. Note, for example, that he was the only man among the GOP presidential candidates to make Gallup's annual short list of most admired Americans, which was released this week." 

In the days before the Virginia ballot fallout, Newt Gingrich painted an elegant picture of his path to the nomination. 

Gingrich said that after finishing in the top "three or four" in Iowa and "top two" in New Hampshire, he would go on to win South Carolina and Florida. 

"From that point on I think it becomes a pretty easy race," he said. 

But his ability to execute his blueprint for winning was thrown into question after his campaign failed to make Virginia's Super Tuesday primary ballot, and amid polls indicating the former House Speaker's plummet in Iowa -- early signs of a campaign with its wheels falling off -- his campaign spokesperson boarded the press bus Wednesday giving reporters a ballpark figure of Gingrich's 4Q fundraising numbers -- a figure Fox News' Carl Cameron had reported two weeks before. 

It was an effort to change the story line of a campaign in free fall; Gingrich is now facing another test of survival: will he be able to buck a trend that has sent every candidate who has peaked in the GOP race into the single digits? 

The day before the NBC/Marist results, Kellems wrote that the race in Iowa is "still VERY fluid" and questioned Romney's ability to survive an attack similar to what Gingrich has undergone. 

"Governor Romney remains stuck at the same levels he's been at for five years - even after tens of millions in direct expenditures, hiring half of Washington and raising enormous sums for super PAC attacks," he said. "Imagine what Governor Romney's numbers would look like now if anything sizable had been aimed at him? He would crumble, which obviously is not what Republicans are looking for when choosing a nominee to take on Barack Obama and his war chest."
