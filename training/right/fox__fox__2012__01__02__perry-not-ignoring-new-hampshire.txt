Rick Perry is downplaying the perception he is skipping New Hampshire's primary entirely. 

Despite announcing he's heading straight to South Carolina after the Iowa caucuses , Perry and his campaign say they are also making plans to increase his visibility in the Granite State this weekend. 

Perry spokesman Ray Sullivan now says the Texas governor will be at both weekend debates, and the campaign is planning events around them though none have been announced yet. 

Perry and Congressman Michele Bachmann are polling dead last in New Hampshire. A new Suffolk University survey shows both candidates at 2 percent. 

Mitt Romney's commanding lead in New Hampshire, however, continues to grow. The same poll shows him garnering 43 percent of the vote -- up 2 points from a day earlier. 

The Perry campaign says regardless of where he places on Tuesday, Perry sees a path out of the Hawkeye State. 

The GOP hopeful tells Fox he's the only true conservative in the race with a campaign infrastructure needed to win. 

" Rick Santorum and Michele Bachmann don't have a national organization in place, nor the fundraising ability to go forward from out of Iowa and so I'm the only one that has that ability." 

When asked about his electability at an event in Sioux City, Perry said he's confident he can go the distance. 

"This is, let's say mile one of the marathon and I've run a marathon before. We'll see who's still running at mile 21. I finished my marathon, and I expect to finish this marathon as well." 

Watch the latest video at FoxNews.com 

Fox News' Lexi Stemple contributed to this report.
