MUSCATINE, Iowa -- Newt Gingrich on Tuesday called for the redesign of both disability and worker's comp insurance to focus on abilities and rehabilitation, suggesting the current system is "very dangerous" because it subsidizes people who "do nothing." 

Responding to a question about how the federal government could control the cost of Social Security disability while at the same time effectively deliver services to people who need them, Gingrich said reform must be aimed at not just targeting the programs, but approaching differently the recipients who use them. 

"We really start by saying what are your capabilities, not what are your disabilities," Gingrich said. "I mean you talk to wounded warriors who come back home who are now running marathons, who are now skiing, who are now doing an amazing range of things. And you are now seeing people who want to pursue happiness. They do not want to live in dependency." 

Gingrich said the programs need to focus on retraining and asking what "investments" are needed to live a full life. 

"We have to be very clear. This is not a society that subsidizes people who do nothing. It's very dangerous to this culture," he said to applause. 

Gingrich added that the "pursuit of happiness" endowed by "our Creator" as described in the Declaration of Independence requires active pursuit, not handouts. 

The former House speaker repeatedly blasted President Obama and Congress for passing a two-month extension of the payroll tax cut, saying Washington is in "cloud cuckoo land" for not getting a longer extension. 

"Notice, I'm not just picking on Obama, the whole system's a mess," he said. 

Stopping by Elly's Tea, Gingrich talked about the importance of coalition building and said there's "a certain virtue to the fact that I've been around long enough to be a grandfather." 

"These are people who have watched me on TV, they've read my books, they've thought about it and they just think compared to the current mess, it'd be nice to have somebody (in the White House ) who knew what he was doing," Gingrich said.
