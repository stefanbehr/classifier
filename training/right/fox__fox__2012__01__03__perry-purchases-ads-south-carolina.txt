When Rick Perry arrives in South Carolina the day after the Iowa caucuses , there will be Perry campaign ads running on TV. 

The campaign says it has just started buying ads in The Palmetto State. 

Perry s aides insist the campaign is not ignoring New Hampshire, but it is clear the focus is on the race after that. 

Perry s campaign released the South Carolina schedule before announcing any New Hampshire events. He has agreed to two debates this weekend in New Hampshire and the campaign now says it has planned to campaign there as well though it hasn't yet announced any campaign events. 

Perry has said he expects to do well in South Carolina. New Hampshire, on the other hand, isn t looking good. Perry is polling in last place there. 

The Texas governor has repeatedly described this GOP presidential race as a marathon. He has spent the most of any campaign on Iowa advertising, and this South Carolina ad buy signals he will be running until at least the third vote in the season, scheduled for Jan. 21.
