An emotional Rick Perry says he is heading back to Texas to "determine whether there is a path forward." Until tonight's distant fifth place finish in Iowa, the plan was to head straight to South Carolina tomorrow. 

Surrounded by his family, Perry told the crowd at his Iowa Caucus Party site, "With a little prayer and reflection, I'm going to decide the best past forward." 

"But I want to tell you, there's been no greater joy in my life than to be able to share with the people of Iowa and this country that there is a model to take this country forward and it is in the great state of Texas," said Perry. 

Perry indicated it wasn't his ambition to run tonight. And the mood of his campaign staff is a sober one. 

Perry's campaign spent the final days before the Iowa Caucuses deflecting speculation that they were skipping New Hampshire to focus on South Carolina. That speculation started ramping up earlier this week, when his campaign released their South Carolina event schedule without announcing any New Hampshire events. Later in the week, Perry and his staff started saying they were planning campaign events around the two debates in New Hampshire they had committed to be there for. However, there were never any releases sent about those events. 

The question now is whether Perry will endorse anyone.
