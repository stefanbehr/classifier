It's where social media meets foreign policy. 

The State Department hosted its first official Twitter briefing Friday where citizens around the world were given an opportunity to tweet their questions to one of the State Department's 10 official Twitter feeds using the hashtag #AskState. 

According to State Department Press Secretary Victoria Nuland, Secretary of State Hillary Clinton is encouraging employees at the State Department to use new technology and social media as a key part of their foreign policy agenda. 

"We're adopting new approaches to meet diplomatic and development challenges around the globe," Nuland said. "Part of this effort is making sure we are using full use of digital networks and essential technologies to more quickly and directly engage audiences around world." 

During the State Department Twitter briefing, Nuland answered 5 questions from the Arabic, Chinese, Farsi, English and French language twitter accounts. The questions ranged from the influence of NATO, the State Department's role in the massacre against civilians in Sudan and improving their human rights, Iran cutting off the internet and America's ability to maintain its military existence around the world. 

The State Department is not the first government organization to use Twitter to spread their message. The White House held its first Twitter Town Hall with Twitter's co-founder and Executive Chairman Jack Dorsey in June 2010. People were able to tweet their questions about jobs and the economy using the hashtag #AskObama. 

This briefing was a way for the State Department to acknowledge the month of January 2012 as 21st Century Statecraft Month - an initiative to adapt traditional foreign policy to the digital networks and technologies of today's world. 

"Throughout this month we'll be showcasing some of the ways the State Department uses new technology and that our diplomats both in Washington and abroad make direct contact with citizens," said Nuland. 

Each Friday in January the State Department will hold a similar Twitter briefing, and next week Secretary Clinton's senior advisor Alec Ross will participate in a live video chat with journalists and bloggers around the world. The embassy in Port-au-Prince will also hold a Twitter Q A with the people and government of Haiti.
