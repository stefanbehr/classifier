SPARTANBURG, Sc. - Despite a disappointing 5th place finish in the Iowa Caucuses , GOP hopeful Gov. Rick Perry, R-Texas, pushed his campaign forward to South Carolina Sunday, explaining to voters here exactly why he's staying in the race. 

"I have never quit a day in my life, I have never quit in the face of adversity and I'm not just about to quit on the future of America," Perry declared at his first campaign stop since his Iowa defeat. 

"I am gonna stay in this race and stay in this fight because our children and this country are worth the fight." 

Staying viable takes more than sheer will. Several polls now show Perry is a non factor in South Carolina, and he needs money to keep his campaign alive. 

His wife Anita, who reportedly urged her husband to stay in the race after Iowa, is now attacking his GOP rivals and asking supporters for cash. 

"Despite what you read from the pundits and pollsters, the scenario for resurgence is quite plausible," she writes in a new plea for funds. 

"Governor Romney continues to be mired in the low twenties in many polls, and in fact did not improve his percentage in Iowa over four years ago by a single point. At the same time, Senator Santorum is just now receiving media scrutiny. While Rick and I admire Rick Santorum greatly, he can't replicate his performance in Iowa where he camped out for the last year." 

For his part, Governor Perry will now be camping out in South Carolina, and campaigning here every day until the Palmetto State's primary January 21st.
