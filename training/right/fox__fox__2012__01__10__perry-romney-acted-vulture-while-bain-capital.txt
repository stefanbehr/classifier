FORT MILL, SC - Republican presidential hopeful Rick Perry launched a ferocious attack on Mitt Romney Tuesday, calling the ethics of his GOP rival into question and likening the actions he took while CEO of Bain Capital to a vulture feasting on the dead. 

The assault came during a campaign stop in as Fort Mill, South Carolina, when Perry singled out two companies in the Palmetto State that Romney's private equity firm took over. Hundreds of workers ended up losing their jobs at those companies. 

"Rather than trying to restructure and to keep the jobs in South Carolina, they were more interested in making their quick buck," Perry said. "That's the Wall Street mentality, ethics kind of get thrown out the door." 

"They're just vultures," he added. "They're vultures that [are] sitting out there on the tree limb waiting for the company to get sick and then they swoop in, they eat the carcass. They leave with that and they leave the skeleton." 

One of Perry's strongest backers Steve Forbes disagrees with the Texas governor on this one, and has defended Bain as an example how free enterprise works. 

Confronted with Forbes' view, Perry fired back. "We can have our disagreements, but the fact is that Americans are sick and tired of Wall Street taking main street to the cleaners." 

Governor Perry has bypassed the New Hampshire primary , opting instead to focus on South Carolina. Perry refuses to say how strong a finish he needs in the Palmetto State to keep his campaign alive, but he claims he is ready for battle. 

"I'm not gonna play the game of what ifs and what have yous. Our intention is to win," Perry said. "This isn't our Alamo, this is our San Jacinto."
