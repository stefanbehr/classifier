President Obama's reelection team raised $68 million for the campaign and the Democratic National Committee in the last three months of 2011, ending up with a total haul of $224 million for the year. 

"[T]he good news is thanks to thousands of people across this country, we've had a pretty good quarter," Obama Campaign Manager Jim Messina said in a video to supporters. He went on to plead for more cash as the election year begins. 

"[W]e also have a challenge that keeps coming up. Too many Obama supporters think we don't need their money or they don't need to give now," Messina said. 

He lamented pre-campaign speculation that predicted a $1 billion total campaign haul for the president and set fundraising expectations high. Messina told supporters the campaign will not raise that much money. 

Republican frontrunner Mitt Romney raised less than half of the Obama campaign's total in the final quarter of 2011 with a total of around $24 million. But the Obama camp expects political action committees backing the GOP nominee will pour money into the general election. 

The Thursday announcement came just hours after the president attended three fundraisers in Chicago . 

"[T]his will be the last campaign," the president told supporters Wednesday night. "And you know, when you think about what's at stake, I hope you end up feeling that there hasn't been a more important investment to make than the one that needs to be made this year, not just in terms of money but in terms of time and energy and effort and enthusiasm."
