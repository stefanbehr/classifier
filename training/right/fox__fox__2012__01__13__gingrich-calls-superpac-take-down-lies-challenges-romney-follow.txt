As he opened up a new campaign office in Orlando, Newt Gingrich called on Winning Our Future, the pro-Gingrich SuperPAC that has been running ads in South Carolina portraying Mitt Romney as a corporate raider during his time at Bain Capital, to either edit out the inaccuracies or pull them from the airwaves and the Internet. 

"I am calling for the Winning Our Future Super-PAC supporting me to either edit its "King of Bain" advertisement and movie to remove its inaccuracies, or to pull it off the air and off the internet entirely," Gingrich said in a release emailed to reporters. 

He also requested that Romney follow suit. A flood of pro-Romney SuperPAC ads that targeted Gingrich in Iowa have been credited for derailing his candidacy. 

Gingrich has continually asked Romney to call for misleading ads to be removed, particularly the ones rated by fact checkers as "Pants on Fire" and given "four Pinocchios." 

Romney has said he will not honor the request because the SuperPACs are outside of his control. Gingrich, meanwhile, has pointed out that, even though it's illegal for a campaign to directly coordinate with a SuperPAC, they are run by former staffers who could honor any public requests made by the former Massachusetts governor. 

"I've said all along that these SuperPACs ought to have some sense of responsibility," Gingrich said. "The ad for example that Governor Romney is running in Florida right now about me was given four Pinocchios by the Washington Post, which means it was wrong at least four times in 30 seconds, which is not easy. I challenged the governor to speak up. He frankly has been timid and irresponsible." 

A Romney source fired back, telling Fox News, "Speaker Gingrich is just distracting from the fact that the movie he has been touting for days including how well-sourced it is turned out to be full of blatant falsehoods and fabrications. 

Gingrich explained his call comes after learning that fact checkers at the Washington Post awarded four Pinocchios to the 29-minute video entitled "King of Bain," excerpts of which Winning Our Future has been using to generate radio and television ads. 



Rick Tyler, a former long-time aide to the former House speaker, runs the pro-Gingrich SuperPAC and says they are considering the candidate's request and will release a statement soon. 

Fox News Congressional Correspondent Mike Emanuel contributed to this report.
