MIAMI, FL-- Greeted by a frenzied herd of sharp-elbowed Miami media, Newt Gingrich downed two potent Cuban coffees, leaving him "energized" for his speech before a largely Cuban-American crowd at Versailles, the iconic Little Havana restaurant, a traditional stop for GOP presidential candidates on the campaign trail in Florida. In front of this receptive audience, the former Speaker of the House wasted little time in hitting the Communist Castro regime in Cuba. 

" My goal as president would be to create a 'Cuban Spring' which is even more exciting than a ' Arab Spring ', " Gingrich said to strong applause. " If we can worry about Egypt , if we can worry about Syria...surely, Washington can look south 90 miles beyond our shores." 

Gingrich also criticized GOP frontrunner Mitt Romney , repeating his claim that he was not attacking Capitalism when he questioned Romney's claim that he helped create 100,000 jobs while CEO of the venture capitalist firm, Bain Capital. 

"The idea that some candidate can make a claim and then call foul the minute you ask him to prove it is just silly. I mean, if he can't stand up today and defend his claim how is he going to stand up to Obama in the fall? " Gingrich said. " I challenge Governor Romney to release the records , show us the facts. You can't run for president, have half your campaign be about your great achievements, and then hide them." 

Gingrich discounted polls that showed Romney leading the field by double digits and predicted that he would win Florida's January 31st primary, which come 10 days after South Carolina's "First in the South" primary. Gingrich said his state wide organization could go head to head with Romney's formidable ground game but that the former Massachusetts governor would outmatch him in funds. 

" We have thousands of volunteers now and we are growing rapidly. Look, I have never made any dispute about this. Governor Romney has raised millions of dollars on Wall Street. He has far and away the best financed Republican campaign but he doesn't have anything like Obama has, " Gingrich said in reply to a local reporter's question." If Romney thinks money is what matters, then he is going to lose to Obama. I think ideas are what matter." 

" If you remember, Romney outspent McCain 10 to 1 in Florida and lost because after they got through all the ads, he was still Romney. So I think you what you are going to find is that we will surge very rapidly in Florida " Gingrich added.
