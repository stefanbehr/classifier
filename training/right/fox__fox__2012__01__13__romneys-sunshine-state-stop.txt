WEST PALM BEACH, FL-- Following consecutive wins in Iowa, and New Hampshire, presidential candidate Mitt Romney made his first trip to the critical primary state of Florida Thursday--- greeted by a large and exuberant midday crowd, that broke out into spontaneous chants of "Mitt! Mitt! Mitt!"-- the GOP frontrunner, who is leading his opponents by double-digits in recent polls here, aimed all his shots at President Barack Obama while ignoring his Republican primary rivals. 

" It is unacceptable to be critical of our friends. (President Obama) went to the United Nations and criticized Israel for building settlements, " He said , repeating a phrase from earlier speeches, that nonetheless generated loud applause and cheers from the Palm Beach crowd. " He had nothing to say about Hamas' 20,000 rockets into Israel." 

"The President's run out of ideas, and he's running out of excuses, and in 2012, with your help, he's run out of time," Romney added in a stump line, more familiar to supporters in Iowa, and New Hampshire, but new to most in the enthused room of over 400 Sunshine staters, as well as an overflow room next door. 

In a state that has a higher than national average unemployment percentage and that felt the head-on brunt of the housing collapse, Romney also acknowledged, and empathized with some in the crowd who might be suffering through the hardships of a harsh economy. 

" Some people are having trouble with their marriages, losing faith, becoming depressed, " Romney said, while some in the audience nodded. "It's a real tragedy to have people out of work for long periods of time, as we've seen." 

The former Massachusetts governor also encouraged the crowd to remember to send in their primary absentee ballots. According to the Republican Party of Florida, over 400 thousand of these votes have already been issued so far due to requests, while nearly 50 thousand have already been cast--Florida typically has a higher than normal amount of early voters due to their large absentee ballot system. 

Romney also attended two finance events while in Palm Beach on Thursday. One fundraiser organized by the Miami Dolphins owner Stephen Ross was expected to have over 600 people, at $2,500 dollars a plate, according to Romney's Palm Beach county chairmen, as well as the Republican county chairman, Sid Dinerstein. 

"You do the math," Dinerstein said. 

A smaller group of donors, who generated $ 50,000 a piece for his presidential campaign, were also scheduled to dine later with Romney at the home of a wealthy Palm Beach businessman, according to the Palm Beach Post. 

Romney's quick visit to Florida was sandwiched between stops in South Carolina. The Palmetto state holds its GOP primary on January 21st, just ten days before Florida.
