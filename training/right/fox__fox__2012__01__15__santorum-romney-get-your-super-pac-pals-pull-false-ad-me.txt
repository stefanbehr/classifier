Myrtle Beach, S.C.--- GOP presidential contender Rick Santorum called for Republican frontrunner Mitt Romney to demand withdrawal of an ad produced by a pro-Romney super PAC that says the former U.S. senator supported a law that would have allowed convicted felons to vote. 

I hope Governor Romney will have his PAC take down an ad that's running against me in South Carolina saying that I want felons to be able to vote. That's an absolute lie, " Santorum said in an interview with "Fox News Sunday" host Chris Wallace . "Romney should be saying to his PAC, take that ad down, it's false. 

"It gives the impression I want people to be voting from jail. And those are the kinds of things if candidates, when they see their super PACs doing things, whether it's Newt Gingrich or Mitt Romney , they should stand up and say, this is false, I repudiate that and they should take it down," Santorum said. 

The Restore Our Future PAC is not coordinated by Romney nor his campaign, and he has no direct involvement though the organization is made up of former Romney staffers. 

"Santorum pushed for billions in wasteful pork, voting for the Bridge to Nowhere, a teapot museum. Even an indoor rain forest. Santorum voted to raise the debt limit five times, increasing spending and debt by $3 trillion," the narrator in the 30-second TV ad says. "And he even voted to let convicted felons vote. So how will Santorum beat Obama? Obama knows he can't." 

The PAC bought over $2 million dollars worth of ad time in South Carolina for this ad and another that also goes after Gingrich. The Restore Our Future Ad, called " Facts" also is running in Florida media markets.
