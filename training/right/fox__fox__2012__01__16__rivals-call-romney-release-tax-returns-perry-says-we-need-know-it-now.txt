MYRTLE BEACH, SC - Texas Governor Rick Perry is demanding Mitt Romney release his tax records, warning there can't be any surprises should the GOP front-runner become the party's nominee. 

"Anita and I put out our taxes every year since back the 80s, and every candidate up there, they should put their taxes out, including Mitt," Perry insisted during a town hall meeting. 

"You know, November, or excuse me, September and October, is not the time for us to be finding out that whoops, there's something out there that is a problem. We need to know it now." 

Perry has hit Romney on this issue before but never by suggesting it could prevent an October surprise. 

Meanwhile former Pennsylvania Senator Rick Santorum lashed out at Romney while on the Laura Ingraham Radio show. When asked if he thinks the former Massachusetts Gov. is a liar because of some of his attack ads, Santorum responded, " You know liar means--has he perpetrated lies? Yes he has. I'm not going to judge him as a person I am going to judge what his actions are and his actions were not truthful." 

Santorum also called on Romney to release his tax returns, "I don't know why he isn't releasing his tax returns, I think he should. I think it is appropriate that if you are going to run for federal office that you let folks know," said Santorum. "Particularly someone with the enormity of wealth and he keeps going out there talking about how he has all this experience how he's done all these things, and he understands how the economy, well let's see how he managed his own economy." 

Romney, already victorious in both Iowa and New Hampshire, has also pulled ahead of his GOP rivals in South Carolina. 

A Real Clear Politics average of the polls out of the Palmetto State shows Mitt Romney leading with 29.7 percent, Newt Gingrich in second with 22 percent, Ron Paul coming in third with 15 percent and Rick Santorum right behind Paul with 14.3 percent. Perry however is running dead last with 5.7 percent. 

The Texas Governor may also be hinting that the sun may be setting on his presidential campaign. When asked at Monday's town hall meeting to describe his relationship with his wife Anita, the Texas Gov. replied, "If I had to walk away from all this, if she is walking with me, I'll be okay." 

FOX News Producer Jake Gibson contributed to this report.
