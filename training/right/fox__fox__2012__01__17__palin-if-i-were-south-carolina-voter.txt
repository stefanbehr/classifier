Watch the latest video at FoxNews.com 

Sarah Palin is not ready to endorse a candidate for president yet, but she told Fox News Sean Hannity that if she were a South Carolinian, she would vote for Newt Gingrich in order to keep competition alive in the GOP race. 

If I had to vote in South Carolina, in order to keep this thing going, I would vote for Newt and I would want this to continue more debates, more vetting of candidates, because we know the mistake made in our country four years ago was having a candidate who was not vetted to the degree he should have been, she told Hannity from her home in Alaska. 

If front-runner Mitt Romney were to win South Carolina, political analysts say he would effectively wrap up the Republican nomination, and Gingrich has argued that conservatives need to rally around his candidacy rather than split their votes among the other candidates, to prevent Romney from securing the nomination. 

The former Alaska governor - who said her husband, Todd, went rogue last week when he endorsed the former House speaker - seemed to agree with Gingrich s argument that consolidating the vote would benefit the conservative movement. 

I want to see this [race] continue because iron sharpens iron, steel sharpens steel, she said. 

Palin said Gingrich won the Fox News/Wall Street Journal debate Monday in Myrtle Beach because just like South Carolina s own smokin Joe Frazier , he came out there swinging, talking about work, talking about jobs and work ethic and about how government needs to get out of the way in order for all Americans to have a sense of opportunity.
