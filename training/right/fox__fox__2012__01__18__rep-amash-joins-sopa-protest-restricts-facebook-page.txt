Rep. Justin Amash, R-Mich., who posts detailed explanations detailing his reasons for voting yes or no on issues on his Facebook and Twitter accounts, has restricted access to his Facebook page. 

This is in line with the Wikipedia protest of the Stop Online Piracy Act (SOPA). Wikipedia blacked out its site at midnight. 

Here is Amash's message, posted overnight: 

"On Wednesday, January 18, I will join others across the Internet in a 24-hour "blackout" to protest the Stop Online Piracy Act (SOPA) in the U.S. House and the PROTECT IP Act (PIPA) in the U.S. Senate. These bills give the federal government unprecedented power to censor Internet content and will stifle the free flow of information and ideas. In protest, I have changed my profile picture and will temporarily disable your ability to post independent content on my Wall (although you still may comment under this post). Demand that Congress and the President keep the Internet open and free. Please borrow my profile pic, share this message, and contact your Representatives and Senators in Congress to urge them to protect your right to free speech by opposing SOPA and PIPA."
