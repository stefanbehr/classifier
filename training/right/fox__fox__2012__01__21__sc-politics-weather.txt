Weather could factor into the South Carolina Primary. There are three key voting areas that are different in political make-up and geography: the upstate, the midlands and the coastal area. 

Depending on which part of the state voters are in, they are dealing with very different weather conditions. The Upstate and the midlands are dealing with rain, fog and thunderstorms. Meanwhile, along the coast there is sunshine and warmer temperatures. 

The big thing to watch for today will be voter turnout in those areas facing rough weather. For some people, that could keep them from the polls, which would be bad news for Newt Gingrich who is banking on votes from the more socially conservative upstate area. 

There is also rain across the middle of the state, which includes the state capital and a large military population. 

The sunshine along the coast could be good for Romney, who is banking on good turnout there. Storms are expected hit the coastal areas later in the afternoon, which could play into the number of voters that come out later in the day. 

Long story short, the timing of the rain and the intensity could swing today's vote one way or the other.
