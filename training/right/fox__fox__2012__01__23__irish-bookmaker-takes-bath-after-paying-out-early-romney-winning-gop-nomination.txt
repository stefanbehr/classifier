Europe's largest betting company is sweating the results of the weekend's South Carolina Republican primary after it decided Friday to pay out early on bets placed that Mitt Romney would take the GOP nomination over rivals Newt Gingrich , Rick Santorum and Ron Paul . 

"It looks as though we could have jumped the gun too soon by paying out on Romney," said Paddy Power, the face and major shareholder of Irish bookmaker Paddy Power. " Newt Gingrich is continuing to build momentum and if he succeeds we could end up with some very expensive pie on our face." 

A Paddy Power spokesman told FoxNews.com that the company took a bath over Gingrich's late surge in the Palmetto State primary, paying out nearly $26,000 so far on Romney's expected coast to victory. 

"We felt the race for the nomination was over," said the spokesman, who added, "If Gingrich takes Florida, we'll definitely be reaching for the deodorant." 

While Romney remains the favorite for the Republican nomination, the company has cut his odds from 1/14 to 2/5, while Gingrich's odds have been chopped from 6/1 before the South Carolina primary to 2/1 now. 

For next week's Florida primary, Paddy Power put Romney and Gingrich even, with both paying out 5/6 odds for the win. Paul and Santorum both stand at 66/1 to take Florida. 

The spokesman said if Gingrich wins the overall nomination, the company could lose about another $39,000 "and counting." 

Paddy Power, which books all kinds of U.S. political contests, also is taking bets on President Obama's State of the Union speech, listing "we have more work to do" at 8/1 odds to be the first cliche the president utters Tuesday night.
