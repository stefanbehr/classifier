The Obama campaign made political hay out of Republican presidential candidate Mitt Romney's comment late last year about foreclosures, but now Vice President Joe Biden is suggesting Romney's plan might actually work. Biden indicates, however, it may not be the most humane course of action. 

Romney told the Las Vegas Review Journal's editorial board in October, "As to what to do for the housing industry specifically and are there things that you can do to encourage housing: One is, don't try to stop the foreclosure process. Let it run its course and hit the bottom." 

In a radio interview with KIIS-FM Los Angeles Host Ryan Seacrest that aired Monday, the vice president broached the topic this way, "I'm going to make raw political statement here. A guy like Romney is a good guy. He's a decent, honorable man and I admire his family. But, you know, what he doesn't understand is we look at the foreclosure rate in the country like my father did; like somebody going to bed tonight-- and there's millions of them still-- staring at the ceiling wondering whether they're gonna be able to be in that house a month from now. And what can we do to help?" 

Then, shifting from using Romney's name, Biden addressed the more generic "they" saying, "They look at it purely from a purely economic standpoint. The best thing to get the housing market back up is let these foreclosures occur, let the bottom fall out and then start to clean up. It's just, you know, sort of Darwinian, you know, the fittest out there." 

An argument we've heard before from the Obama team. 

But then Biden added, "And they're right. It would be the quickest way to do it." 

That's not to say the vice president thinks Romney's plan is a good one. 

Emphasizing the human aspect of foreclosures, Biden said, "But what they don't understand is there are tens of thousands, hundreds of thousands of people who through no fault of their own are in this spot. So I think in those circumstances, government has a responsibility to give these people a leg up."
