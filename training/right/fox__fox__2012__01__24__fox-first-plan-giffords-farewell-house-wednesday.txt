U.S. Rep. Gabrielle Giffords will submit her formal letter of resignation from the House of Representatives tomorrow, promising, in her letter, I will recover and will return. 

Giffords, who has represented Southern Arizona in the House for five years, will submit letters of resignation tomorrow morning to House Speaker John Boehner and Arizona Gov. Jan Brewer . Her resignation will be effective at the end of the day tomorrow. 

Giffords ends her resignation letter with the words: Every day I am working hard. I will recover and will return and we will work together again for Arizona and for all Americans. 

The complete text of her resignation letter as well as a pdf of the letter will be released tomorrow after it is formally submitted. 

Here is how the process is expected to unfold tomorrow: 

Sometime between 9:45 and 10:30 a.m. (EST), Democratic Leader Nancy Pelosi will ask unanimous consent from her colleagues to speak out of order. Pelosi then will discuss Giffords career in the House. 

Following that, either Boehner or Republican Leader Eric Cantor will make brief remarks about Giffords service followed by Democratic Whip Steny Hoyer. 

Then Rep. Debbie Wasserman Schultz, Giffords closest personal friend in the House, will read the congresswoman s letter of resignation from the well of the House. Wasserman Schultz will be joined by Giffords and a bipartisan group of Giffords colleagues. 

Giffords then will hand her letter of resignation to the speaker. 

The congresswoman s resignation will come just before the House votes on her final piece of legislation: a bill that would impose tough new penalties on smugglers who use ultralight aircraft to illegally bring drugs across the U.S.-Mexico border. 

Giffords resignation letter also will be delivered to the governor tomorrow, who will determine the dates for special primary and general elections to fill Giffords vacant seat until the November general election. 

Giffords was shot in the head and critically wounded on Jan. 8, 2011. Six people were killed and 12 others wounded as Giffords met with her constituents at her Congress On Your Corner event. 

I have more work to do on my recovery, so to do what is best for Arizona, I will step down this week, Giffords said in a video message released Sunday to her constituents. 

In the two-minute video, Giffords looked back on her career and urged her colleagues in Congress to continue her practice of working across party lines for the good of the country. 

A lot has happened over the past year, she said. We cannot change that. But I know on the issues we fought for, we can change things for the better. Jobs, border security, veterans. We can do so much more by working together.
