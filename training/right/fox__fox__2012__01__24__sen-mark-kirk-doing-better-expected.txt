Doctors say US Senator Mark (R-IL) is doing "better than expected" after undergoing brain surgery related to a stroke he suffered over the weekend. Kirk's neurosurgeon, Dr. Richard Fessler, says Kirk even asked for his B lackberry yesterday, a sign he's clearly eager to get back to his work. 

Kirk's breathing tube was removed and he is now breathing on his own. Dr. Fessler says Kirk is talking but his speech is slurred due to partial paralysis on the left side of his face. He also has very little movement in his left arm and leg. 

Kirk recognizes his family members and is responding to basic questions and following commands. "The fact that he's doing all of these things this quickly... is a very good sign," said Dr. Fessler, a neurosurgeon at Northwestern Memorial Hospital in Chicago who previously served on Kirk's health care policy committee. 

Kirk's medical team says at this point they don't know what caused Kirk's stroke and may never know. Dr. Fessler says Sen. Kirk lived a healthy lifestyle and points out Kirk had to pass a Navy physical twice a year. 

Responding to a question that Sen. Kirk suffered from migraines, Dr. Fessler said, "I can't imagine being a politician and not having migraines. I don't think this event had anything to do with either stress or his diet, it's just one of those unfortunate disasters that's happens." 

Kirk's recovery will likely take several months. He will remain in intensive care for four to five weeks to recover from his surgery. 

It will be at least two weeks before the swelling in his brain will decrease to the point that doctors can replace the 4x8 inch section of Kirk's skull that was removed during surgery. Eventually Kirk will be transfered to an outpatient rehabilitation facility where he will undergo intense physical and speech therapy. 

After serving five terms as a US Congressman, Mark Kirk was elected to the Senate in November 2010, during a special election to fill Barack Obama's vacant senate seat.
