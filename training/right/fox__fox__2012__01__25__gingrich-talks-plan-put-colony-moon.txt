COCOA, F.L. - GOP hopeful Newt Gingrich is unveiling a dramatic new vision for America's space program, which he promises to implement should he become president. 

"By the end of my second term, we will have the first permanent base on the moon," Gingrich vowed at a campaign stop on Florida's space coast. "And it will be American." 

The former speaker went even further, calling for a significant increase in the number of trips into orbit. 

"We need to figure out how to do five or eight launches a day, not just one," he insisted. "If we re going to get to the moon permanently, we have to do this. Does that mean I m a visionary? You betcha." 

Mitt Romney has sharply criticized Gingrich over earlier space proposals, specifically pointing to a lunar colony plan to mine minerals from the moon as a waste of money. 

One Gingrich idea Team Romney didn't seem to catch - statehood for Americans living on the moon. 

"At one point early in my career I introduced the Northwest Ordinance for space and I said when we have 13,000 Americans living on the moon they can petition to become a state," Gingrich admitted. 

"I wanted every young American to say to themselves, I could be one of those 13,000. I could be a pioneer." 

That's a plan Gingrich now hopes to revive. 

"I will as president, encourage the introduction of the Northwest Ordinance for space to put a marker down that we want Americans to think boldly about the future and we want Americans to go out and study hard and work hard and together we're going to unleash the American people to build the country we love."
