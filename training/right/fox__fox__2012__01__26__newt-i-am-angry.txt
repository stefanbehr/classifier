MT. DORA, Fl - In a fiery speech before a crowd of tea partiers, Newt Gingrich unloaded on Mitt Romney , accusing his GOP rival of outright hypocrisy for the negative ads now running the Sunshine state. 

"You're watching ads paid for with the money taken from the people of Florida by companies like Goldman's Sachs, recycled back into ads to try to stop you from having a choice in this election. That's what this is all about," Gingrich said. 

"The question you have to ask yourself is, what level of gall does it take to think that we collectively are so stupid that somebody who owns lots of stock in Fannie and Freddie Mac, somebody who owns lots of stock in Goldman Sachs, who is insistently foreclosing on Floridians, who is surrounded by lobbyists who are already protecting Fannie Mae and Freddie Mac, can then build his entire negative campaign in Florida around a series of ads that are just plain false." 

Gingrich went on to call the ads "junk," and admitted they are now getting under his skin. 

"I am angry. But I think I'm angry and every American should be angry. How can somebody run a campaign this dishonest and think he's going to have any credibility running for president?," Gingrich told reporters after his speech. 

"I mean at some level there ought to be a sense of shame for someone being this fundamentally dishonest." 

Team Romney fired back. "This is the type of criticism we have come to expect from President Obama and his friends on the left like Debbie Wasserman Schultz . But it is puzzling to see Speaker Gingrich and his supporters continue their attacks on free enterprise," Romney Spokesperson Andrea Saul said. 

"Unlike President Obama and Speaker Gingrich, Mitt Romney spent his career in business and knows what it will take to turn around our nation's bad economy." 

An average of polls show the former speaker and Romney running neck-and neck in Florida, with the primary now just 5 days away.
