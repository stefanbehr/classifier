Chris Stirewalt hosts an all-star lineup on Power Play Live as they dissect Florida's Primary Election Day. 

Carl Cameron, Juan Williams , Phil Keating and former New York Sen. Alfonse D'Amato weigh in on the latest news from the campaign trail in the Sunshine State. 

Watch Power Play Live Monday-Friday at 11:30am ET here: http://live.foxnews.com/ 

Watch the latest video at FoxNews.com
