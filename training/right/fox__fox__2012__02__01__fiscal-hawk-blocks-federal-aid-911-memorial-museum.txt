Another September 11th-related funding clash is poised to erupt in Congress, as one fiscal hawk senator blocks a bill that would see $20 million in taxpayer funds go to the creation of the 9/11 Memorial Museum. 

Sen. Tom Coburn, R-Okla., who proudly wears the moniker of "Dr. No" due to his opposition to deficit spending, has refused to sanction the legislation in the wake of trillion dollar deficits that are now projected to continue through the current fiscal year and possibly beyond. 

"Dr. Coburn believes we can best honor the heroism and sacrifices of 9/11 by making hard choices and reducing spending on less vital priorities rather than borrowing money," Coburn spokesman John Hart tells Fox. "This funding dispute could be solved in minutes if the sponsors would look at the hundreds of billions of dollars in waste and duplication in the federal government that has been identified by the Government Accountability Office and others. Finding $20 million in savings is the least we can do to demonstrate that Congress also understands the value of service and sacrifice." 

The museum, largely funded by private donations, is designed to honor victims of and educate the public on the terrorists attacks of both September 11, 2001, as well as the February 26, 1993 World Trade Center bombing. Hart said the senator would ideally like to see private funds solely cover the cost of the project, much as happened with the Oklahoma City bombing memorial. 

Senate Appropriations Committee Chairman Dan Inouye, D-Hawaii, sponsored the 9/11 Museum bill in 2011 which he said was "to provide permanent authorization of funds to support the operations and maintenance of the Memorial and Museum." 

The chairman's measure states that through the Interior Secretary, the federal government will take ownership of the site," if approved by the Museum's board, the Governor of the State of New York, the Governor of the State of New Jersey, and the Mayor of New York City." It also stipulates that "all funds appropriated must be matched by non-Federal sources, with the resulting Federal share being about 33% or less of the overall budget of the Memorial and Museum." 

These are politically treacherous waters to navigate for Coburn - deficit reduction verses an emotionally-charged, 9/11-related project, but the senator, who plans to retire at the end of his term, has been here before and rarely shrinks from any fight over the debt. 

Coburn initially opposed federal funding for first-responders injured by the toxic cleanup in the wake of the terrorist attacks. He eventually relented, though, after a vigorous campaign by supporters of the legislation. 

Sen. Chuck Schumer , D-NY, a primary supporter of the federal museum effort, said he hopes his Republican colleague will step aside, once again. 

"To his credit he sat down with us and moved beyond (his objections), and we hope it will happen again," Schumer told reporters Wednesday, adding that federal funds were needed, because after talking with NY Mayor Michael Bloomberg, who is also the project's chairman, "the very generous" private funding is "clearly not enough money. We hope Coburn will relent." 

The September 11th Memorial Museum is already receiving federal funding, though, through grants, and in the wake of damaging reports about golden parachutes for former project employees, Coburn could find support for his blockade. 

The Wall Street Journal broke the news this week that at least one former employee of the memorial received a nearly $300,000 severance payout and that possibly others also scored lucrative packages. According to one report obtained by Fox, at least 10 former employees of the Memorial Museum received pricey payouts. 

But Mayor Bloomberg defended the $300,000 severance deal this week at a news conference in Queens, saying, "It was a long time ago. It's the only person that I've been able to find that had when they were hired for a variety of reasons were given a golden parachute." 

And Bloomberg's spokeswoman, Julie Wood, told Fox Report's Martin Finn Wednesday, "Like other memorials of national significance that receive federal funding, we believe the 9/11 Memorial and Museum deserves these funds. We're grateful for the support of New York's representatives in Washington as well as Sen. Inouye, who recognize what this funding will mean to the family members of victims the millions of visitors from around the country and the world who want to commemorate the tragic attacks." 

Still, this conflict shines a light on the exceedingly difficult situation in which lawmakers find themselves as they attempt to rein in and shrink the nation's dangerously-high debt.
