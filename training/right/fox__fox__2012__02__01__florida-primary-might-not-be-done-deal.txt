TAMPA, Fla. -- Florida's primary might not be over just yet. 

The Newt Gingrich campaign is gearing up to challenge the results of the Florida Republican presidential primary based on the Republican National Committee's own rules which state that no contest can be winner-take-all prior to April 1, 2012. ( See RNC memo .) 

It was assumed that Mitt Romney , who won Tuesday's contest, would gain all 50 of the state's delegates. But the Gingrich campaign plans to challenge Florida's allocation and demand the delegates be divvied up proportionally. ( See Gingrich memo .) 

Fox News has learned exclusively that on Thursday, a Florida Gingrich campaign official will begin the process of trying to have the RNC rules enforced so that the Sunshine State delegates are distributed based on the percentage of the vote each candidate got. 

RNC Chairman Reince Priebus warned Florida Republican Party Chairman Lenny Curry of the violation in a December letter quoting the rule, "...'winner-take-all' states cannot hold a primary or caucus before April 1, 2012." 

Tuesday night's Romney victory in the Sunshine State awarded the former Massachusetts governor all 50 Florida delegates (the state was already docked half its delegates for moving the election up on the calendar). Romney won the primary with 46 percent to Gingrich's 32 percent. Rick Santorum finished third with 13 percent and Ron Paul with 7 percent.
