A House committee released a report Wednesday accusing tanning salons of denying the "the known risks of indoor tanning." 

"Tanning salons should not be putting young women's health at risk by providing them with false and misleading information," said Rep. Henry Waxman, D-Calif., ranking member of the House Committee on Energy and Commerce, which released the report. 

The report found that 90 percent of indoor-tanning salons claimed that indoor tanning did not pose a health risk and more than 50 percent of salons denied it would increase the risk of skin cancer . 

Young-white females are the most frequent indoor tanners. About 40 percent of 16-to-18 year-olds in that age demographic has visited a tanning salon, according to the report. 

Even when tanning salons did tell committee investigators about health risks, they got it wrong. One salon compared tanning beds to "walking to your car" in sunlight and another to "standing in front of the microwave." 

Indoor tanning has been linked to skin cancer , including its deadliest form, melanoma, one of the fastest growing cancers in the United States. 

Tanning beds can emit UVA radiation up to 10 to 15 times more powerful than midday sunlight and the World Health Organization has said the risk of melanoma increases 75 percent when tanning bed usage starts before age 30. 

The FDA is currently considering strengthening regulations, but for now hasn't set any limits on the frequency or duration of indoor tanning sessions after the first week of usage. 

More than 30 states have enacted laws regulating teens indoor tanning use, most requiring parental consent. California is the only state that has banned teens from using tanning beds. 

The report also criticized tanning salons for targeting their ads at teenagers and college students.
