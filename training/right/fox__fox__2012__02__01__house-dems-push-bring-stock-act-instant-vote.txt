Co-sponsors of a House bill to prevent insider trading on Capitol Hill are planning to attempt a procedural maneuver Wednesday to force a floor vote on the legislation. 

The STOCK Act, which prohibits members of Congress and their staff from executing stock trades based on information learned in the halls of the Capitol, currently has 271 cosponsors. And while it only takes 218 members, a simple majority of the House, to move what is known as a "discharge petition" to demand action on the floor, it s tough sledding to get to an up-or-down vote after that. 

There are lots of reasons this discharge petition may be challenging to accomplish. First, it can take up to seven legislative days to schedule. That easily gets to sometime next week. However, by rule, the House can only consider discharge petitions on the second and fourth Mondays of each month. So if the House is not in session on one of those given Mondays, tough cookies. 

Considering that timing, the House would not be able to consider this discharge petition until Feb. 27 at the earliest. 

The historical precedents don t bode well either for the bill's sponsors, Reps. Louise Slaughter , D-N.Y., and Tim Walz, D-Minn. The last successful discharge petition to proceed was the Bipartisan Campaign Reform Act, known as McCain-Feingold, in 2002 -- after a five-year effort. 

All hope isn t lost, though, for proponents of the legislation. The Senate is expected to vote on a similar measure this month. House Majority Leader Eric Cantor , R-Va., said he wants to use the Senate langauge as a template for House action on the legislation. 

Walz suggested moving in an expedited fashion would help lawmakers who agree with the overall thrust of the legislation but want to make additional changes. 

"Go home and tell people that you decided you wanted to hold this up and you didn't want to pass it and find out what happens, Walz said, I think the fear of not moving this thing will outweigh the desire that many legislators have to attach things and make it perfect in their own vision. 

Fox News Chad Pergram contributed to this report .
