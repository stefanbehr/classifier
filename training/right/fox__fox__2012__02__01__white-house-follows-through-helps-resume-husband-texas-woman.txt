The White House has followed through on President Obama's pledge to help out an Ohio woman's unemployed husband, who is trained as an semi-conductor engineer in Texas. 

Jennifer and Darren Wedel said they received a call from a deputy chief of staff on Tuesday and the White House has looked over his r sum and passed it along to potential employers in the Dallas-Fort Worth area, the couple said on Fox News' "Happening Now" Wednesday morning. 

Darren added that they are very "grateful" for that. 

The president had promised as part of Google+ town hall on Monday that he would help them out by reviewing Darren's r sum after sounding surprised he was having trouble finding a job, saying that he's hearing many in the engineering field are in high-demand. 

Jennifer and the president were part of a virtual "hangout" where a handful of individuals peppered the president with questions for about 50 minutes and responding to other YouTube videos. 

The two had several back and forth moments about the r sum and difficulty finding work in the field and how her husband was out of work for three years. 

"I meant what I said: if you send me your husband's r sum , I'd be interested in finding out exactly what's happening right there," Obama said Monday. 

Since Monday, she says that she has received tons of e-mails from other Americans in the same situation her husband is in. 

Jennifer initially asked the president about a capping a visa program and limiting the number of foreigners who can take high-tech jobs here. 

Asked whether this would persuade Jennifer to vote for Obama since she didn't in 2008, she said not necessarily. 

"Not based on a job. We're one American. There are tons of Americans, hundreds, thousands that don't have jobs, we're just one," she told Fox. 

"If he were to do something...maybe reevaluate the [visa] cap, that could possibly sway my vote, but just because he looked at the r sum , we're going to have to think about that one," she said. 

White House Press Secretary Jay Carney was asked Tuesday how the White House would handle the potential for a lot more individuals to flood their r sum s to the White House, Carney said he didn't know but also that they weren't necessarily expecting that to happen. 

But, he said the president likes hearing personal stories. 

"I think it just reflects his deep interest in the individual stories that Americans have about their lives, how they're experiencing the economy in different parts of the country. And he will certainly have that kind of engagement, as he has in the past, going forward," Carney said. 

The Associated Press contributed to this report.
