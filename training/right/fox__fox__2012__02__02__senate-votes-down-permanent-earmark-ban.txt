Sens. Pat Toomey, R-Pa., and Claire McCaskill, D-Mo., led an unsuccessful, 40-59, bipartisan effort to try to permanently ban congressionally directed spending known as earmarks. 

Eight Republicans from the Appropriations Committee joined all of the panel s Democrats to oppose the measure. The conservative Club for Growth said they consider this a "key vote." 

STAND-OUT VOTES: 

Yes : 

Bill Nelson , D-Fla. 

Debbie Stabenow , D-Mich. 

Mark Udall, D-Colo. 

Mark Warner , D-Va. 

Kay Hagan, D-N.C. 

Mitch McConnell, R-Ky., Senate Republican leader, Appropriations Committee member 

No : 

Lamar Alexander , R-Tenn., Appropriations Committee member 

Roy Blunt, R-Mo., Appropriations Committee member 

Thad Cochran , R-Mo., Appropriations Committee member 

Susan Collins, R-Maine, Appropriations Committee member 

John Hoeven, R-N.D., Appropriations Committee member 

Kay Bailey Hutchison, R-Texas, Appropriations Committee member 

James Inhofe, R-Okla. 

Joe Lieberman , D-Conn. 

Richard Lugar, R-Ind. 

Lisa Murkowski, R-Alaska, Appropriations Committee member 

Harry Reid , D-Nev., Senate majority leader 

Pat Roberts , R-Kan. 

Jeff Sessions, R-Ala. 

Richard Shelby, R-Ala., Appropriations Committee member 

Roger Wicker, R-Miss.
