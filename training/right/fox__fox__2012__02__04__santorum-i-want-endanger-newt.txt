MONTROSE, CO - Republican presidential candidate Rick Santorum took a new jab at Newt Gingrich , saying he wants to "endanger" the former Speaker. 

The dig came during a campaign rally in Montrose, CO., as Santorum railed against the federal government's environmental regulations. 

"These are your lands! Oh but they always say we're doing it for your benefit," Santorum decried. 

"We'll make sure that you don't do something to scar the land or you don't do something to endanger a newt. No not that Newt, different newt. I want to endanger that Newt - that's a different story." 

The line drew laughter from the audience but Santorum has been serious in sharpening his attacks on Gingrich in recent weeks. 

The former Pennsylvania senator also targeted Mitt Romney , claiming the former Massachusetts governor will not be able to bully President Obama like he has contenders in the GOP field. 

"If you think he's gonna be able to do to Barack Obama what he's done to a series of Republican candidates so far, which is to go out and spend 5 times as much and beat them up so he is not promoting himself but beating up the other side so he can win a primary here and a primary there, he's not gonna be able to do that."
