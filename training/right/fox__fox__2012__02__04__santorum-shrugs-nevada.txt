DENVER, Colo. - It may be caucus day in Nevada but GOP hopeful Rick Santorum isn't even in the state. 

Asked why he is campaigning in Colorado instead, Santorum claimed he doesn't have the cash to compete in the Silver State. 

"It's a state that very much favors Governor Romney. He's invested about $1 million in the state already. Ron Paul's got close to $1 million in the state," Santorum said. 

"We just don't have those resources. [Nevada] doesn't match up for me as well as some other states do." 

Romney is heavily favored to pull off a victory in Nevada Saturday, but there may still be some glimmers of hope for Santorum. 

Aboard his campaign plane, dubbed "South Vest Airline" by the traveling press, Santorum learned he was tied with Romney in Searchlight, Nev. 

"That may be the highlight of the day," he quipped. "Emphasize may."
