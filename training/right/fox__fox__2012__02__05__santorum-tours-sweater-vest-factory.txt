BIMIDJI, Minn. - Sporting a sweater vest and a smile from ear to ear, GOP presidential hopeful Rick Santorum got a first hand look at the factory that makes what is now a staple of his campaign. 

"It was a complete fashion faux pas," Santorum explained while touring the Bemidji, Minnesota factory that manufactures his signature sweater vest. 

"I was out campaigning one day and forgot that that night I had a speech to give at a Mike Huckabee event which should have required a coat and tie, and I walked out there dressed like this," he explained. 

"I went out there and I guess gave a pretty darn good speech and people started saying hey, maybe it was the sweater vest! And all of a sudden the sweater vest sort of took off and took on a life of its own." 

The senator now says yes to the vest frequently on the campaign trail, and he's not the only one suiting up in sleeveless wool. 

Team Santorum started offering the attire last month to supporters on the campaign website for a $100 donation. 

National Communications Director Hogan Gidley says since then, the campaign has sold about 1,650 sweater vests and raised about $200,000 off of them.
