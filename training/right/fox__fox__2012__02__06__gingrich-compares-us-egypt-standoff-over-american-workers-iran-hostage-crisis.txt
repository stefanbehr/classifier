Golden, Colorado - Newt Gingrich slammed the White House Monday, comparing the escalating diplomatic tensions between the U.S. and Egypt to the Iran hostage crisis of 1979. 

Over the weekend Egypt announced intentions to send nineteen American non-profit workers to trial for charges they used foreign funds to promote unrest, ignoring threats from U.S. officials that $1.3 billion in aid be withheld. 

"We now have the Obama hostage crisis to resemble the Carter hostage crisis," Gingrich said at a rally held just outside Denver. "And you talk about taking back the 1.3 billion --I'd do a lot more than that!" 

Comparing Ronald Reagan's strategy for the Cold War -- "We win, they lose" -- to President Obama's support of the Arab Spring , Gingrich characterized the White House's approach to the Middle East as a "mindless capitulation to forces that are contrary to our entire civilization." 

President Jimmy Carter's handling of the Iran hostage crisis has been widely cited by political analysts as a key reason why he Carter lost his re-election bid to Ronald Reagan in 1980. 

Gingrich has oscillated between preaching the importance of positive campaigning and whacking fellow Republicans on the campaign trail - with the latter practice often eclipsing the former, as what happened during his media avail Saturday night after the Nevada caucuses. But on Monday, fresh off of fundraising and reorganization meetings in Nevada, he saved his harshest attacks for the White House. 

"We've been trying over the last three or four weeks to rethink the whole campaign because I really believe that it's a great disservice to the American people to not have this as a conversation about really big solutions and really big ideas," the candidate said to a medium-sized crowd of approximately 250 people gathered in a hotel ballroom in the city of Golden, home to Coors beer company. 

Gingrich has just two stops in Colorado Monday, his only day of campaigning here before the non-binding state caucuses. Saving up his resources to execute a Super Tuesday strategy, Gingrich raised a little over a million dollars in Las Vegas last week, with an additional $800,000 in commitments.
