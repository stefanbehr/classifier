By Bree Tracey 

Catholics, contraceptives and Capitol Hill . The three factors coming together have made for a bitter battle in Washington, as the Obama administration pushes a requirement that forces religious organizations, including Catholic hospitals, to pay for health insurance covering contraceptives for employees. 

At a GOP press conference Thursday, Rep. Renee Ellmers, R-N.C., a Catholic herself, took a personal approach in explaining her opposition. 

"This is about religious freedom," said Ellmers. "When my father, my father is deceased, but when I was a child he sat me down and he looked me in the eye and he said, 'Renee, some day you will have to fight for your religion.' I didn't understand then what he meant, but it always stuck with me. I understand today because I believe that day has come and we must do everything we possibly can to fight against it." 

All eight Republican congresswomen who spoke at the GOP presser criticized President Obama and health Secretary Kathleen Sebelius for having a "lack of respect" for their religious constitutional rights. Ellmers added a statement that summed up their message saying, "This is not about health care, this is not about women's healthcare, this is about the overreach of government. This is about President Obama having control over every aspect of your life, especially for women, from conception to death." 

Rep. Jerry Nadler, D-N.Y., presented an opposing viewpoint at a House Democratic press conference on Capitol Hill the same day stressing that the mandate allowing access to birth control was not forcing anyone to use it. 

"We're not coercing the Catholic Church to do anything," said Nadler. "It is totally wrong and totally phony." 

Nadler continued to argue that when the church steps out of its role as a church and becomes an employer or a hospital administrator they are subject to the same laws as everybody else. 

"What they are trying to ask for is an exemption from the normal laws in their own. Not as a church, not as a religious institution, but as a hospital or as a college that they cannot have because to get, to allow them to have that is to allow them to impose their religious doctrine on people who may not share their faith and whose choice it is," said Nadler. "We don't coerce them, but they can't coerce other people."
