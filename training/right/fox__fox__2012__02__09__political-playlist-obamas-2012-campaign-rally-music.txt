The Obama campaign released the official soundtrack that will play at President Obama's 2012 election rallies, revealing an eclectic lineup that includes tracks from rock, indie and country acts. 

The campaign posted the playlist the president campaign Facebook page and also Spotify, a streaming music service. You can listen to the 24 songs here . 

The list includes a song performed by Jennifer Hudson for the movie Dreamgirls, "Love you I do." Hudson, like the president, is a Chicago native and has performed at Obama campaign events. 

Also notable on the list is Al Green's "Let's Stay Together," which the president sang at the Apollo Theater last month at a campaign fundraiser. First lady Michelle Obama says her husband likes to serenade her and recently told Jay Leno, "He does have a beautiful voice, and he sings to me all the time." 

She said he also likes Marvin Gaye and Stevie Wonder , who didn't make the cut, but some other iconic names did like James Taylor ("Your Smiling Face"), Aretha Franklin ("The Weight"), REO Speedwagon ("Roll with the Changes" and U2 ("Even Better Than The Real Thing"). 

There's also a decent round of country performers on the soundtrack, including Dierks Bentley ("Home"), Darius Rucker ("Learn to Live" and "This") and Sugarland ("Everyday America" and "Stand Up"). 

Some indie tunes will also be played at his rallies including Arcade Fire's "We Used to Wait" and Florence and the Machine's "You've Got Love." 

The music will be heard before large campaign events, and obviously intended to get the crowed jazzed up before the president makes his re-election campaign pitch. 

Obama's 2008 lineup featured upbeat tempos from the 60s and 70s, fitting to his "Hope and Change" campaign slogan. 

The 2012 one is perhaps slightly more in tune with the realities of the tough economic times. 

Take No Doubt's track that sings: 

"Things can be broken down In this world of ours You don't have to be a famous person Just to make your mark" 

And Dierks Bentley, who belts: 

"From the mountains highTo the wave crashed coastThere's a way to findBetter days I know 

It's been a long hard rideGot a ways to goBut this is still the placeThat we all call home" 

Fox News' resident expert on 60s music and the Beatles, James Rosen (who also goes by the more official title of Chief Washington Correspondent) notes that the Earth, Wind and Fire's song on the list "Got to Get You in My Life," is misidentified. The correct title of the song, originally released in 1996 on the Album Revolver is by the Beatles, and is titled "Got to Get You Into My Life." 

OBAMA'S 2012 RALLY PLAYLIST: Different People No Doubt 

Got To Get You Into My Life - (Live) Earth Wind Fire 

Green Onions - Single/LP Version Booker T. The MG's 

I Got You Wilco 

Keep On Pushing - Single Version The Impressions 

Love You I Do Performed by Jennifer Hudson; Dreamgirls Soundtrack 

No Nostalgia AgesandAges 

Raise Up Ledisi 

Stand Up Sugarland 

This Darius Rucker 

We Used to Wait Arcade Fire 

You've Got The Love Florence + The Machine 

Your Smiling Face James Taylor 

Roll With The Changes REO Speedwagon 

Keep Marchin' Raphael Saadiq 

Tonight's The Kind Of Night Noah And The Whale 

Keep Me In Mind Zac Brown Band 

The Weight Aretha Franklin 

Even Better Than The Real Thing U2 

Home Dierks Bentley 

Everyday America Sugarland 

Learn To Live Darius Rucker 

Let's Stay Together Al Green 

Mr. Blue Sky Electric Light Orchestra
