Rep. Michele Bachmann's former campaign spokeswoman has just signed on with the Rick Santorum campaign. 

Alice Stewart, who was Bachmann's communications director, will join team Santorum as its national press secretary. The move comes after Santorum won three state contests on Tuesday. 

Stewart said in a statement she is "honored and proud" to join up with the former Pennsylvania senator. 

"Senator Santorum has proven himself to be the true, consistent conservative in this race," she said. "Rick's growing momentum is evidence that voters realize he's the only candidate with the conservative record we need to stop Mitt Romney and beat President Obama." 

Santorum, who is rising in national polls, placed second to Romney on Saturday in the presidential straw poll at the Conservative Political Action Conference in Washington. 

Stewart also previously worked as a spokeswoman in the 2008 presidential campaign of former Arkansas Gov. Mike Huckabee .
