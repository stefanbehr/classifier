Sen. John McCain will travel to Egypt to resolve a dispute over the expected prosecution of six U.S. rights workers, who are being prevented from leaving the country. 

In an interview with Fox News' "America's News Headquarters" on Saturday, the Arizona Republican was optimistic that communications would remain fluid between Egypt and the United States. 

"I am hopeful that before we get there, we can have the issue resolved, if not, obviously it will be our highest priority. Americans not being able to leave the country that is supposed to be our ally is an important issue. And so we intend to raise it in our meetings." 

Egyptian activists held strikes in the streets Saturday to mark one year since former President Hosni Mubarak was ousted from office. Since then, Egypt has held parliamentary elections and is reconstructing its infrastructure. While still a tumultuous time in the country, McCain didn't express concern over safety but rather the rapport between the U.S. and Egypt. 

"What I am worried about is the overall relationship between the United States and Egypt. Egypt is the heart and soul of the Arab world" said McCain. 

The ranking member of the Senate Armed Services Committee also called out the Obama administration for its inaction on Syria . 

"I say in all respect, the White House is leading from behind again," he said, acknowledging that troops on the ground may not be a necessary step in taking action to end President Bashar al-Assad's regime. 

"There is a myriad of ways to be of assistance to the people that are being slaughtered," said McCain. "We should again publicly and openly criticize Russia and China for the disgraceful behavior on the issue." 

More than 5,400 people have been killed in Syria since the uprising began in March 2011.
