Several senior Senate Democratic aides have confirmed Democrats have laid aside, for now, their attempt to pay for any extension of a package of benefits - including the payroll tax cut, jobless benefits, and a Medicare "doc fix" -- with a surtax on millionaires, a cornerstone of President Obama's fight against Republicans. 

"To be sure, it will live to see another day," said one senior Senate Dem leadership aide. 

The fight over how to offset another payroll tax holiday has roiled bipartisan House-Senate negotiations. But House Speaker John Boehner , R-Ohio, and his GOP leadership team diffused that fight Monday by offering to extend the tax break with NO offset, and by doing so, they side-lined what had been a potent political weapon used against them. 

But the issue of how to pay for the remaining items in the negotiations, from unemployment insurance benefits to the "doc fix," is still preventing a breakthrough, according to multiple participants. 

Sen. Mike Crapo , R-Idaho, a payroll tax cut conferee, told Fox that though Democrats are now proposing to extend jobless benefits with no offsets, Republicans have not given in. "That's certainly not decided yet," Crapo said. 

Crapo said it was his understanding that negotiations are far from concluding, and Senate Minority Leader Mitch McConnell, R-Ky., confirmed that Tuesday, telling reporters, "The conference appears to be going nowhere." 

Senate Majority Leader Harry Reid, D-Nev., told reporters that though the payroll tax fight had seemingly been resolved, lawmakers "would not leave here" until jobless benefits are extended. 

With just three days to go before a scheduled week-long recess in honor of President's Day, it would appear that lawmakers' coveted time away from Washington could be in jeopardy.
