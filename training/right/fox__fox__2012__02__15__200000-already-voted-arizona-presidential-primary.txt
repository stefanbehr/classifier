Some 200,000 ballots have already been cast for the Arizona's Republican presidential primary that's still two weeks away. Nearly of those votes come from voter-rich Maricopa County, the largest in the state. 

Most of the smaller counties don't yet have an exact tally of returned ballots so a specific statewide figure isn't available but a spokesperson for the Maricopa County Recorder's office says 144,518 Republican votes have been submitted. Nearly 400,000 early ballots were sent out there and voters can still request them until the end of this week. 

Voters have until polls close on Election Day to return ballots. Those who don't request one can also show up on February 28 to vote in person. The early voting period opened on Feb. 2 before Mitt Romney won Nevada and Maine. Perhaps more significantly, it's not clear how many ballots were returned before Rick Santorum's recent sweep of contests in Missouri, Minnesota and Colorado and his subsequent rise in national polls. 

"They came back fast," Pima County Recorder Ann Rodriguez told Fox. So far there, 30,205 Republican ballots have been returned. That's 35 percent of the early ballots sent out. They are sent to voters who requested them for this election or have done so in the past. Nearly 60 percent of Pima County's Republicans will get an early ballot. 

Another county recorder said she's satisfied a couple of thousand ballot requests in recent days suggesting the sluggish turnout seen in other states this election cycle isn't the case in Arizona. 

Four years ago, 541,767 votes were cast in the primary won by native son Sen. John McCain . Romney finished second. The seemingly heavy response so far might actually dilute the impact of next week's debate in Mesa-at least for Arizona voters. 

There are 29 winner-take-all delegates available, but that's half the number Arizona was originally allocated. The Republican National Committee penalized the state for moving up its primary.
