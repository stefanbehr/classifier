Los Angeles, Calif. - Coin by coin, Newt Gingrich slowly filled up his modestly sized coffers in the Golden State this week, at the same time courting leaders of minority groups who -- the campaign says -- could prove to be kingmakers if no candidate is able to wrap up the nomination by the state's primary, June 5. 

"We've exceeded our goal of $2 million dollars since the start of the campaign," Eric Beach, state finance chair said, telling Fox News that he is "very pleased" with the numbers given that it's Gingrich's second trip to the state since he announced his candidacy. 

The campaign has yet to say exactly how much Gingrich raised during this particular four-day swing, which had eight fundraisers on the schedule. The campaign reported raising $660,000 through the end of 2011, a figure dwarfed by the $6.6 million Mitt Romney fundraised that year in California. 

Mike Schroeder, who served as Romney's political director in 2008, disagreed with critics who say Gingrich could have made better use of his time had he been campaigning elsewhere. Gingrich has kept a relatively low profile in California, making minor headlines feeding pandas at the San Diego Zoo and lunching with Condoleezza Rice at the Hoover Institute. 

"What we got out of South Carolina is we got a bump in fundraising but it's all small donor fundraising going to the website," Schroeder said. "That's poll driven. The bigger money fundraising, like if you want to get you know $5,000 checks, $2,000 checks, $20,000 checks...you have to do it here." 

"Super Tuesday is going to tell us everything. If it's still a three or four way race, California is still going to be in play," Schroeder said. A favorable outcome March 6 would move Gingrich forward on his only realistic path to winning the nomination: a prolonged battle for delegates that goes all the way to the convention. 

Schroeder, who as chair of the state Republican party oversaw a change in the way delegates would be awarded in California - from winner-take-all to winner-take-all by congressional district - says that the 126 delegates at play would then play a crucial role deciding the nominee - which is why Gingrich has also been paying attention to ethnic groups during his California trip. He held a meet and greet with Asian Americans Thursday morning, which followed a fundraiser with members of the Hispanic community on Monday. 

"We have 53 congressional districts...If you win a couple Congressional districts, you can get as many as you got in New Hampshire," Schroeder said, adding, "We have districts where there are only 4,000 or 5,000 Republicans in them up in the Bay Area, Los Angeles, and most of them are either Asian, African American, or Hispanic," Schroeder said, predicting that candidates would be "aggressively campaigning to Asians, blacks, and Hispanics in California because they can win significant numbers of delegates." 

At an event hosted by city drug commissioner Howard Winkler and restaurateur Nir Weinblut, two local Jewish leaders, supporters who signed $2,500 checks for the candidate were granted access to a VIP reception; a $500 contribution was the cost of a seat at the luncheon where Gingrich delivered a foreign policy speech that underscored his pledge to protect Israel from the Iranian threat of a "second Holocaust." 

Gingrich also gave a 20-minute interview with Jacob Frankfurter, editor-in-chief of an Orthodox Jewish magazine, who told Fox News he advised the candidate that he was spending too much time beating up his opponents instead of presenting a vision of a "bigger, beautiful tomorrow." 

"As somebody who believes in conservative values and Reaganomics and that entire vision, there's nobody who can articulate that better and understands that better than Newt Gingrich ," said Frankfurter, who isn't supporting any particular candidate. "And in my humble opinion, he hasn't been able to get that message out yet." 

Frankfurter said Gingrich agreed and told him that he'll be shooting films that are "more positive rather than what's wrong with the Obama administration."
