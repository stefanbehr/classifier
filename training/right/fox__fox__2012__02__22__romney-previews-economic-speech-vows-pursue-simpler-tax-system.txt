In a preview of what is being billed as a major economic speech in Detroit later this week, Mitt Romney doubled down on his economic proposals Wednesday. 

Standing before a large and boisterous crowd in Chandler, Ariz., Romney promised to make the tax system "simpler, flatter, and fairer," by reducing individual rates by 20 percent across the board. If implemented, the top rate would drop from 35 percent to 28 percent, the same top rate Americans paid under President Reagan in 1986. 

To prevent an increase in the federal deficit by lower tax receipts, Romney said he would limit deductions and exemptions for higher-income brackets, saving the burden from falling on the middle class. 

"I want to make sure that you understand for middle-income families, the deductibility of home mortgage interest and charitable contributions -- those things will continue," Romney said to loud cheers. "But for high-income folks, we're going to cut back on that, so that we make sure the top 1 percent keeps paying the current share they're paying or more. We want middle-income Americans to be the place we focus our help, because it's middle-income Americans that have been hurt by this Obama economy." 

On an earlier conference call with reporters, members of the campaign expanded on the proposals, saying they would eliminate taxes on overseas profits, get rid of the Alternative Minimum Tax (AMT) for both individuals and corporations, keep tax rates on dividends and capital gains at 15 percent, and eliminate the death tax, which is set to go up to 55 percent in the next year. They said both their individual and corporate tax overhaul would be deficit neutral without raising taxes. 

Romney is set to deliver his economic speech Friday. The new details could appease top Republicans, who have criticized the GOP candidate for not being specific or bold enough in his policy proposals. 

The proposals could also be seen as a response to President Obama's own corporate tax reform, unveiled earlier Wednesday. The president's plan would reduce the overall rate to 28 percent while eliminating dozens of loopholes and exemptions enjoyed by large, multinational corporations. The White House estimates their plan would raise an additional $250 billion in revenues over the next 10 years. 

While the new rates expand on his tax policy unveiled last year, the proposal was still light on details - the Romney campaign did not explain how they would broaden the tax base or what deductions and exemptions they would get rid of for high earners. 

And the Obama campaign was quick to pounce, saying Romney's proposals would increase the federal deficit by $2 trillion over the next decade. " Mitt Romney has already rolled out a budget plan that doesn't add up. By giving large tax cuts to millionaires, billionaires and corporations while increasing defense spending to an arbitrary level, his budget would lead to massive increases in the deficit," the campaign said.
