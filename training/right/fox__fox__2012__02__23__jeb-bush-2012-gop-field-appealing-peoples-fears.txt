Jeb Bush seems a little confused by some of the rhetoric being tossed around by the GOP's 2012 presidential frontrunners in debates and out on the campaign trail. 

"I used to be a conservative and I watch these debates and I'm wondering, I don't think I've changed, but it's a little troubling sometimes when people are appealing to people's fears and emotion rather than trying to get them to look over the horizon for a broader perspective and that's kind of where we are," said the former Florida Governor. "I think it changes when we get to the general election. I hope." 

Bush made the comments while answering questions from the audience after giving a speech in Dallas on Thursday. 

The younger brother of former president George W. Bush also weighed in on the debate Republicans and Democrats are having over the national economy. 

"If you want to narrow the income gap, there are two ways to do it. One, you punish people that are successful and try to narrow it that way and that's the president's approach. Or you equip people that aspire for a better life to give them the tools and then you don't try to manage that, you allow them to pursue those dreams as they see fit. That to me is the better approach and it requires a celebration of success."
