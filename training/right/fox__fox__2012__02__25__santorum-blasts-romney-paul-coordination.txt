SAINT CLAIR SHORES, Mich. - In one of his most pointed attacks to date, Rick Santorum lashed out at Mitt Romney and Ron Paul , accusing both GOP rivals of teaming up together in order to tear him down. 

To help prove his point, Santorum singled out Congressman Paul's campaign spots now flooding the airwaves in the Wolverine State. 

"He's not campaigning in Michigan, yet he's running ads in Michigan against me," Santorum said during a Tea Party rally here. 

"You just have to take a look at what his real objective is. If it's really going after trying to fundamentally change Washington, why is he being the wingman for Mitt Romney all throughout this campaign," he added. "So let's see who's calling who a fake." 

Santorum fired off plenty of shots at Romney too, claiming the former Massachusetts governor's new tax plan looks like something crafted by the Occupy Wall Street movement. 

"He said we're gonna make it revenue neutral, well how are you gonna do that," Santorum asked. 

"Don't worry, we'll limit deductions for the top one percent. The top one percent. Hmm, where have I heard that? We have a Republican running for president who is campaigning as an Occupy Wall Street adherent. What's he going to do? He's going to limit charitable deductions on the top 1 percent," he continued. 

"It's laughable for Governor Romney to suggest that I am not a conservative. It's absolutely laughable for a liberal governor of Massachusetts to suggest that I am not conservative." 

Romney was quick to fire back, with a little help from his friends. 

In a replay of tactics used against Newt Gingrich in Florida, Team Romney is deploying surrogates to Santorum events in Michigan to provide on the spot criticism of the former senator while defending the Romney record. 

This time it was Michigan State Representative Aric Nesbitt, who approached reporters after the event to say, "It's sad to see Senator Santorum trying to run away from his record, but it's to be expected." 

"It's very disappointing seeing and hearing in our own turf of Michigan him trying to sell something that he isn't." 

When asked, Nesbitt admitted Santorum's Tea Party event was not in his district, which is more than 100-miles away. 

*Nick Kalman and Chris Laible wrote this blog
