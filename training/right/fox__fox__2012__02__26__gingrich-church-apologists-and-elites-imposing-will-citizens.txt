CUMMING, Ga. - Newt Gingrich turned the church pulpit into a history class when he addressed the congregation at First Redeemer Church, comparing the struggle of American colonies under British rule to what he sees as the modern day assault of religious freedom in America 

"I don't come here as a religious leader and I don't come here as a saint," Gingrich said as soon as he began talking. "I come here as a citizen who has had a life that at times has fallen short of the glory of God, who has had to seek God's forgiveness and has had to seek reconciliation." As such, Gingrich said, he was speaking in the pulpit "as a historian, where I think I can talk with some credibility and some authority" on the role of God in America 

Listing the demands of the colonists -- no taxation without representation, getting rid of British judges imposing dictatorial power -- Gingrich said America is back "in some ways" at the same state. 

"You have elites in the bureaucracy, elites in the judgeship, frankly elites in the news media, elites in the academic world, and elites in politics -- and they would all like to impose on us an America that none of us believe in," he said to a congregation of over 2,000 people -- standing room only, but smaller than the crowd Rick Santorum drew here last week. 

The White House recently created an uproar when it mandated that religious institutions pay for insurance that includes contraception methods opposed by the Catholic church, and Gingrich used the administration's decision as an example of "the inevitable nature of the left to use government to impose on us their values." 

"Now they'll tell you, well they stand for separation of church and state," Gingrich said. "My answer to that is that they have perverted Thomas Jefferson's words beyond belief." 

"What happens is you go from a request for toleration to the imposition of tyranny, and you do it with remarkable speed," he added. 

Gingrich said that the religious foundation of America is being attacked on two fronts: "We have a secular elitist wing that deeply, deeply disbelieves in America, that wants to create a different country based on a different set of principles," he said. "And we have a radical Islamist one which legitimately and authentically hates us and should." 

He drew a standing ovation for slamming a State Department meeting with the Organization of Islamic countries, which Gingrich said had "the purpose of talking about how to protect Islam from being described inappropriately. I have passionate opposition to the government of the United States lying to us and censoring us as we try to understand those who would kill us." 

Secretary of State Hillary Clinton said after the July 2011 OIC meeting "combating religious intolerance" that the United States remains "focused on interfaith education and collaboration, enforcing anti-discrimination laws, protecting the rights of all people to worship as they choose, and to use some old-fashioned techniques of peer pressure and shaming, so that people don't feel that they have the support to do what we abhor." 

By contrast, Gingrich drew a stark picture of radical Islamists, saying, "Those people who want to kill us want to kill us because from their world view, we are the greatest threat they've ever faced because we represent freedom and freedom is the end of their religion." 

Referring to the burning of churches in Nigeria, Egypt, and Malaysia and the fleeing of Christians from Iraq "that we supposedly liberated," Gingrich said, "I haven't heard any apologies. It's amazing. Saudi Arabia allows no open worship by Christians or Jews, period, and then lectures us. And we don't today in our elites have the nerve to stand up and say this is baloney. We are not going to have a one-sided offensive against our civilization in which we're supposed to accept defeat, we're supposed to accept humiliation, and we're supposed to death of our young men and women, and we have to accept those who are killing us? I don't think so." 

Gingrich drew another standing ovation.
