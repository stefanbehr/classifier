LANSING, Mich. - GOP hopeful Rick Santorum blamed a sole culprit for the 2008 financial meltdown. 

"We went into a recession in 2008 because of gasoline prices," the former senator declared at a campaign rally in Lansing, Michigan. 

"The bubble burst in housing because people couldn't pay their mortgages because we're looking at $4 a gallon gasoline. And look at what happened, economic decline," he added. 

Many economists disagree and say the recession was caused by a combination of greedy bankers and reckless homeowners who took on mortgages they knowingly couldn't afford. 

When foreclosure rates started spiking, banks across the country stopped lending and the credit market froze. Some institutions that invested heavily in toxic mortgage backed securities went belly up. Others got in on a massive bailout from the federal government to stay afloat. 

Santorum's comments could come back to haunt him in Michigan. 

His GOP rival Mitt Romney is now highlighting his own economic experience as a main selling point here, and a recent Public Policy Polling survey shows 69% of likely primary voters say their biggest concern is the economy. Among them, Romney leads Santorum 45%-30%. 

Asked by Fox after the event if he really believed high gas prices alone caused the recession, the former senator backtracked. 

"I think they were a contributing factor to the recession, sure," Santorum said. 

"Obviously there are a lot of factors that go into it but I think that was one of them," he added. 

"They were spiking up until the summer of 2008 and that was a fact."
