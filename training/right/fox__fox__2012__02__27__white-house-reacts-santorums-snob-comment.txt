President Obama made what could be interpreted as a direct response on Monday to Republican presidential contender Rick Santorum's comment over the weekend that the president is a snob for wanting everyone in America to go to college. However, a White House official told reporters Monday that's not necessarily how the president meant it. 

Press Secretary Jay Carney said that while he didn't know for sure the president's intentions, "I think it's a general point he wanted to make. I don't know if it was -- I don't think it was specific to that statement," he said. 

Allowing the president to wade into the fray during a political season in which there is still no clear GOP opponent is a self-prescribed no-no by the White House . The time for politics will come later, they say. 

But Mr. Obama was talking about education at a gathering of governors at the White House when he made an effort to clarify what he meant in singing the praises of higher education. "The jobs of the future are increasingly going to those with more than a high school degree. And I have to make a point here. When I speak about higher education we're not just talking about a four-year degree," the president said. 

"We're talking about somebody going to a community college and getting trained for that manufacturing job that now is requiring somebody walking through the door, handling a million-dollar piece of equipment." 

Democratic Governor Martin O'Malley (MD) seconded Carney's opinion, saying the president didn't appear to be reacting to Santorum. "I didn't hear him referencing that in there," he told Fox. 

"I think the bottom line is we all recognize the more a person learns, the more a person earns," he said. O'Malley added that education can come in various forms. 

If Mr. Obama wasn't reacting to the former Pennsylvania Senator, Carney made it clear to reporters that he was. "I don't think any parent in American who has a child would think it snobbery to hope for that child the best possible education in the future, and that includes college."
