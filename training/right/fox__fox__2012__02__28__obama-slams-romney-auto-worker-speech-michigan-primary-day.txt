President Obama took two swipes at Republican presidential candidate Mitt Romney in front of auto workers in Washington on Tuesday -- on the same day that Romney is fighting for a crucial win in his home state of Michigan.In front of the friendly union crowd, Obama took a more campaign-style approach to his remarks, with a casual delivery and often booming statements in the microphone that pumped up the auto workers. Audience members often interjected his speech with cries of support and chants of "four more years, four more years!" 

Obama made references in separate parts of the speech, to Romney's 2008 New York Times op-ed titled "Let Detroit Go Bankrupt." The president used that exact title, and also repeated a line from the piece, "you can kiss the American automotive industry goodbye." 

In full context, Romney wrote on Nov. 18, 2008, in his position to oppose the auto bailout, "If General Motors, Ford and Chrysler get the bailout that their chief executives asked for yesterday, you can kiss the American automotive industry goodbye. It won't go overnight, but its demise will be virtually guaranteed." 

Obama said to the auto workers, "If we had turned our backs on you, if America had thrown in the towel, GM and Chrysler would have gone under," Obama said. 

"Once proud companies chopped up and sold off for scraps. And all of you - the men and women who built these companies with your own hands - would've been hung out to dry," he added. 

Obama never actually mentions the former Massachusetts governor by name, but it's noteworthy the same day as the Republican primary in the state where the auto industry is not only the economic life for the state, but where Romney is struggling to win. 

The slams come on a primary day where Romney should have a stronger standing in Michigan, given he won there for years ago, it's where he grew up, his father was governor there and also ran a car company. 



Despite the delivery and atmosphere, White House Press Secretary Jay Carney said Obama's remarks to the auto workers were not a campaign speech. 

Romney's rival, former U.S. Sen. Rick Santorum , R-Pa., also opposed the bailout, but Romney is on shakier ground. 

A Michigan loss would be embarrassing and could propel the race more in favor to Santorum, who is behind in delegates but could benefit from momentum leading into the several states casting ballots on Super Tuesday in a week. 

On why he is down, Romney said to reporters, "It's very easy to excite the base with incendiary comments. We've seen throughout the campaign that if you're willing to say really outrageous things that are really accusative and attacking of President Obama, you're going to jump up in the polls." 

"I'm not willing to light my hair on fire to try and get support. I am what I am," Romney told the traveling press. 

In remarks Monday, the president also took an opportunity while talking to a group of governors about education, to clarify his stance that seemed to point to a recent line by Santorum. 

Santorum had suggested Saturday that the president was being a snob in his approach to college, even though he didn't get exactly right the president's sentiment. " The idea that somehow or another that everybody needs to go to college, I just, I think is intellectual snobbery. There are people in this country who you know, who live good strong fulfilling lives without having to go to college or even aspire to go to college and there is nothing wrong with that," Santorum said. 

The president clarified his stance Monday and many political observers took it as a swipe at Santorum. 

"I have to make a point here. When I speak about higher education we're not just talking about a four-year degree. We're talking about somebody going to a community college and getting trained for that manufacturing job that now is requiring somebody walking through the door, handling a million-dollar piece of equipment," Obama said to the a group governors at the White House . 

Tuesday in front of the auto-workers, Obama also referenced "values," which could be a veiled attack to Republicans, or even Santorum, who is most-known in this election race as the values candidate because of his take on social conservative issues. 

"You want to talk about values? Hard work -- that's a value. Looking out for one another -- that's a value. The idea that we're all in it together -- that I am my brother's keeper; I am my sister's keeper -- that is a value," Obama said. 

In addition to Michigan, Arizona is also holding a primary Tuesday, where polling shows Romney in the lead.
