59 delegates up for grabs in the Arizona and Michigan Primaries. Romney is a prohibitive favorite in Arizona, but the outcome in Michigan is not as easy to predict. A victory there could give either candidate big momentum heading into Super Tuesday . 

Digital Politics Editor Chris Stirewalt lays out what should you be looking for as the returns come in tonight. 



Watch the latest video at FoxNews.com
