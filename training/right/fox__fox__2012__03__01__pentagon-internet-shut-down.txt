Fox News has learned that on Thursday at around 10:00 a.m. the military's Defense Information Systems Agency (DISA) shut down access to the internet and blackberry service while they work to fix an unspecified problem. This means no one in the Pentagon has internet and many military downrange, to include combatant commands, don't have internet either. 

DISA, according to its website, is a Defense Department agency that provides command and control support to national-level leaders and joint-war fighters "across the full spectrum of operations." The agency sent out a network wide notification this morning via email explaining that "users are experiencing problems browsing the internet due to a DISA-wide outage." As a result, the memo said, "ALL Blackberry , email web-browsing, and VPN services are affected." 

People we spoke with in the Pentagon are still able to use e-mail on their computers, but were unable to access the internet. 

According to a Pentagon official familiar with network security, this outage is not in response to any time of cyber-attack. This official says if it were an attack, "we'd all know it and DISA would have done what is called a blanket protocol, shutting down all sorts of access until they isolated the source of the attack." 

A spokesman at DISA told Fox so far "there is no indication of an attack" and it's expected the internet will slowly come back online. 

Technicians in the military are working to resolve the problem, which could be affecting as many as 20,000 military and civilian personnel in the Pentagon alone.
