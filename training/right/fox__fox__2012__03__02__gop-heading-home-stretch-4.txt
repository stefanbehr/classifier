Presidential politics ramp up considerably in March as the Republican nomination process heads into the home stretch. 

After two months of nominating contests, just over 300 Republican delegates have been allocated, leaving all contenders far from the 1,144 delegates needed to win the party A As nomination. But in March alone, there are 735 delegates on the line A A more than any other month. (With that said, the delegate count is complicated) 

And no single day in the Republican calendar carries as much significances as March 6 A A Super Tuesday A A when 419 delegates are at stake in 10 contests across the country. There will be primary elections in Ohio, Virginia, Georgia, Massachusetts, Vermont, Oklahoma and Tennessee and caucus meetings to start selecting delegates in Alaska, Idaho, and North Dakota. Wyoming also starts its county conventions on March 6 with 12 delegates at stake, but they run through March 10. 

FOX News will provide AEHQ special coverage starting at 8 pm ET with reporters in Ohio, Georgia, Tennessee, Virginia and Massachusetts. Mitt Romney will be in Massachusetts, Rick Santorum in the state of Ohio, Gingrich in his home state of Georgia and Ron Paul in Idaho. We could see race calls in the states of Virginia, Georgia and Vermont in the 7 pm ET hour. 

The outcome of Super Tuesday will say a great deal about what happens just one week later on March 13 as another 107 delegates are up for grabs in Alabama, Mississippi, Hawaii and American Samoa. There are another 54 delegates on the line in Illinois on March 20, and 25 more later that week with Louisiana A As Saturday, March 24 primary. 

The other weekends in March will all be busy, with Saturday or Sunday events. On March 3, we have the Washington caucuses, which include a straw poll of participants. On March 10, Kansas holds a similar event. On March 18, Puerto Rico Republicans will have caucus meetings and straw polls. 

After 10 months of debates, straw polls and spin, the votes will start to pile up quickly in March, and Super Tuesday is the key to unlocking the prizes later in the month. 

A strong showing in those high-profile contests could start sealing the nomination for the frontrunner, former Massachusetts Gov. Mitt Romney . Conversely, a breakout performance by either former Pennsylvania Sen. Rick Santorum or former House Speaker 

Newt Gingrich on March 6 could slow Romney A As delegate lead and provide momentum to stay in the hunt in what could be a months-long process. Super Tuesday also offers indefatigable delegate hunter Texas Rep. Ron Paul the chance to sock away some more seats on the floor for the GOP convention this August in Tampa, Fla. 

As the Republicans duke it out, President Obama, his campaign and the Democratic Party have continued to capitalize on Republican infighting. Aside from raising millions more for his war chest, expected to be the largest ever, Obama is expected to continue to try to shadow the GOP contest with campaign visits, advertising and speeches all aimed to appeal to independent voters -- a serious vulnerability for his bid A A by painting all the members of the Republican field as extreme and unserious.
