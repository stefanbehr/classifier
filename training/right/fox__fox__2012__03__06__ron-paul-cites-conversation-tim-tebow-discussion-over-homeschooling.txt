Ron Paul discussed a conversation he had with Denver Broncos quarterback Tim Tebow while discussing homeschooling at a campaign event Monday night. 

At a town hall discussion at the Civic Auditorium of Idaho Falls, Idaho, Paul spoke on his pro-homeschooling views, saying arguments that homeschooled children receive inadequate education have been disproven. 

He then mentioned his conversation with Tebow, in reference to an argument that homeschooled kids don't get the chance to participate in sports. 

"You know I was talking to a football player the other day and I think he's rather famous now," he said. "I think it s Tim Tebow , something like that, and of course, of course most people know he was homeschooled and he's doing pretty well for himself." 

The Paul campaign told Fox News Tebow's manager, who is a Paul supporter, arranged a friendly phone call between the Republican candidate and the NFL star. 

On the eve of Idaho's caucus as part of the ' Super Tuesday ' contests, Paul said he enthusiastically believed his campaign 'had to do well tomorrow' in Idaho based on the event's turnout. 

"We expect to do very, very well," he said. "I don't make bold predictions but there's no reason in the world we cannot win this state, so I suspect we are going to do exceptionally well and I thank you, thank so much for all your hard efforts."
