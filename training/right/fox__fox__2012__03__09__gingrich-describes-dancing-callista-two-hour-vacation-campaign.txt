SOUTHAVEN, Miss. - It's not every day that a presidential candidate lets loose on the dance floor. So when reporters posted photos of a tie-less Newt Gingrich dancing with his wife Callista in the wee hours of Thursday morning, curiosities were piqued. 

Fox News' Greta Van Susteren teased Gingrich in an interview Thursday evening: "I am waiting for the video about the Twist, the polka, there are lots of dances that I'm ready to get you on tape doing. So beware." 

"I tell you what, if a video of that shows up, you may find one of the reporters missing," he replied. "No videos. We didn't mind an occasional still shot, but no videos." 

The former House Speaker called the night of dancing in a Jackson, Miss., hotel lounge a "two-hour vacation" after non-stop campaigning Georgia and Alabama . 

"I like dancing with Callista and it was great fun," he said. "Everybody who was with us had a good time. But we even had a couple of reporters who were with us, and it was kind of fun. It was -- I thought it was in the 99 percent off the record. I think you got the 1 percent." 

"You know, Callista warned me that this would make GretaWire," he said with a smile.
