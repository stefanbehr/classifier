Sen. John McCain may have been a sailor at one point, but the former Navy captain doesn't curse like one, he said Sunday, and he sure didn't pick Sarah Palin to be his 2008 vice presidential running mate just because she's a gal. 

McCain, who watched the Phoenix Coyotes defeat the San Jose Sharks Saturday night rather than take in HBO's premiere of "Game Change," the glitzy Hollywoodized version of his presidential bid, said he's a bit taken aback to hear not only about the film's depiction of his decision-making process, but also his choice of verbiage. 

"I have been told I am portrayed as using an exceeding amount of coarse language. I don't use coarse language very often. I have a larger vocabulary than that," he said. 

The Arizona Republican senator told "Fox News Sunday" that it's flat-out fiction in the movie when Woody Harrelson , playing senior adviser Steve Schmidt, tells Ed Harris ' McCain that none of the white, male potential vice presidential options are a game changer. In the film, the McCain character responds, "So find me a woman." 

"I thought that she was best qualified person," McCain said of his selection. "I thought she had the ability to excite our party and the kind of person that I wanted to see succeed in the political arena. She is very effective and successful governor of a state. 

"And what I don't understand even in the tough world of politics, why there continues to be such assaults on a good and decent person, Sarah Palin , a fine family person, a person whose nomination energized our campaign. We were in the lead and they continue to disparage and attack her character and her person," McCain continued. 



"I admire and respect her and proud of our campaign. I'm grateful that she ran with me and I will always be proud of what we did and humbled by the fact that I was able to get the nomination of the Republican Party for president of the United States," he said. 

McCain also shrugged off the book that spawned the movie as "completely biased and with unattributed quotes." 



Asked about the real-live Schmidt's comments that the movie is accurate and that he regrets "playing a part in the process that yielded someone on the ticket that was not prepared to be president," McCain had little explanation for his former adviser's words. 

"I regret that he would make such a statement," McCain said.
