Birmingham, Ala. -- It s hard to imagine any presidential candidate getting more political mileage out of single-cell life forms than Newt Gingrich . 

Algae in a bottle, algae as campaign material, algae as a paper weight (more on that later), Gingrich has managed to take President Obama's so-called "weird" reference to algae and manufacture even weirder hits on Obama. 

But before you think the candidate is making the mistake of sounding "zany" on the stump given how serious his vow is to bring gas prices down to $2.50, it's worth noting that audiences have enjoyed Gingrich s algae-isms in Mississippi and Alabama , so much so that little prompting is necessary for them to get involved in the anti-algae action. 

Saturday night, as he teed up his go-to offensive against Obama's "anti-energy" policy -- "This is what (the president) said: Drilling is not the answer." -- Gingrich was interrupted by a single, muddled yell from the crowd. 

"What's the answer?" Gingrich asked, turning in the direction of the voice. 

"Algae!" a chorus of voices shouted back, prompting a ripple of giggles through the standing-room only crowd. 

The one-time professor now routinely calls for a show of hands from the audience: "How many of you knew the answer was algae?" 

Riffing on the stump, Gingrich has derived an unusual amount of creative material from algae. For the first video in his "Educate Barack Obama " tour, the candidate made an impromptu decision to stop at a Texaco station to record a video for YouTube in front of a gas pump. 

"I don t think the president quite gets it," Gingrich deadpanned, grabbing the nozzle out of the holster and pointing it towards the camera. "There's no algae that's going to come out of this this summer." 

At Henderson's antique car barn in Mobile, Ala., Gingrich exclaimed that there were no "algae cars" in the house. In order to show voters the contrast between "President Algae" (Obama) and "President Drilling" (Gingrich), the candidate said he was considering sending volunteers out in the summer to gas stations and "stand out there with a jar of algae and ask folks how much Obama fill-up they would like and whether or not their car has been redesigned for algae." 

It doesn't end there either. 

"I think we want to make every American learn that the Obama solution is algae," the candidate declared. "Now, I'm actually thinking about getting a little desk set made that you could get at Newt.org that would have sort of a jar of algae and an Obama bumper sticker over here and it would have a drilling rig and a Newt bumper sticker over here and it would (say) $10 and $2.50 and (the words) 'You Choose.'" 

"We're looking for somebody who can manufacture that so if you find him, give us a call," Gingrich said. 

Algae desk set, anyone?
