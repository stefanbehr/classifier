A joint US and Afghan investigation into the Koran burning incident at a U.S. military base outside Kabul late last month is now complete, Pentagon Spokesman Capt. John Kirby told reporters Tuesday. 

Capt. Kirby said there has been no decision to make the report public, but the U.S. is satisfied with its results, he said. 

According to Kirby the report found that "the disposal of religious texts was improper, but not an act of religious malice." He said the report also found that there were written notes inside these books. 

Fox News previously reported prisoners were using the Korans at the detention center's library to pass extremist messages. 

The investigation also recommends the alleged offenders be considered for potential disciplinary action. 

The U.S. military has yet to complete its own investigation into the matter.
