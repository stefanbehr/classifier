BALLWIN, Mo. - Rick Santorum is now claiming the race for the GOP nomination is closer than you might think. 

"We're looking at the rules, we're looking at how things are stacking up, and we're in much better shape in these caucuses and some of these apportioned states or winner take all states which in fact are not winner take all states," Santorum told caucus-goers here. 

"We've got some new delegate math that we're going to be putting out that shows this race is a lot different than what the consensus is." 

According to the latest math from the Associated Press, Mitt Romney has secured 495 delegates. Santorum trails with 252. A total of 1,144 are needed to win the Republican nomination. 

Santorum did not say when his campaign will release their own numbers, but during a later visit to a caucus site in Hazelwood, he did make another claim that caught Team Romney's attention. 

"All of the Midwest is one color. It's our color. We've won every state in the Midwest," Santorum declared. 

Romney spokesperson Andrea Saul has a much different take, and released the following statement in response. 

"As usual, Senator Santorum has no regard for the facts. Mitt Romney beat him in both Michigan and Ohio because of his strong, pro-jobs message - and despite Senator Santorum's desperate attempt to get Obama supporters to throw the primary his way."
