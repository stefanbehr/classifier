Shhh!!! 

Come over here. 

I want to tell you a few dirty, little secrets about this year's budget process. 

And you should hear this as House Budget Committee Chairman Paul Ryan (R-WI) propounds his budget blueprint for the year. 

Here's the first secret: Congressional budgets aren't binding. 

And the second secret? This one's more complex. 

Republicans decided to vacate the topline figure of $1.047 trillion in discretionary spending for fiscal year 2013 as called for in the debt ceiling pact approved last summer. Instead, Ryan's budget aims for a lower total of $1.028 trillion in discretionary spending in the new fiscal year. 

But even if the House GOP approves Ryan's budget with $1.028 in discretionary spending, many familiar with Congress's fiscal machinations believe that the spending figure is like likely to balloon back to $1.047 trillion later this year. 

Pretty good little secret, isn't it? 

How can that be? 

For starters, budgets aren't required in Congress. Like a union shop steward touting how many days a plant has been accident free, House Republicans repeatedly pointed out that the Senate hasn't approved a budget in 1,037 days. If budgets were mandatory, the Senate would have okayed several since then. But they're not. So the Senate hasn't done so. 



Meantime, the GOP-controlled House okayed the Ryan budget plan last spring and is angling to follow suit again this year. 

But all of the mayhem surrounding the budget might not amount to much if the GOP is forced to toss the Ryan budget out the window this fall. The House Republican leadership brass can do that - simply because Congressional budgets don't have to happen. 

Here's the trouble: 

President Obama forged a pact last summer with Congressional Democrats and Republicans to raise the debt ceiling. That measure was called the Budget Control Act (BCA). It set the top spending level at $1.047 trillion for discretionary spending. 

Briefly, a primer on "discretionary spending." 

"Discretionary spending" is all the money Congress isn't compelled to obligate. For example, it's up to Congress to approve a certain amount of dollars for the Pentagon so it may purchase a specific weapon system. With its "discretion," Congress may decide to green light more money one year and less money the next for that weapon system. Congress has the ultimate say (hence, discretion) over whether the government spends that money in that way. 

"Non-discretionary" spending covers compulsory expenditures. In other words, Congress doesn't truly control spending for Social Security, Medicare and Medicaid. Social programs are "entitlements" and are "off-budget." So, the dollars spent on entitlements hinge on which Americans qualify to receive which benefits at which rate. Congress has no say in those programs unless it fundamentally changed the law. 

Entitlements make up the supermajority of government spending. Everything else is "discretionary" which Congress does have a say over. 

And discretionary spending is the $1.028 trillion figure reflected in the Ryan budget. 

However, Democrats are in full-throated protest. They claim Republicans are "breaking the deal" because they're not using $1.047 trillion as the baseline. 

"The Budget Control Act is crystal clear that the spending limits in the resolution should be set at the levels agreed to in the Budget Control Act," argued Senate Budget Committee Chairman Kent Conrad (D-ND). "It doesn't say at a level below the limits set forth in this act.' It says at a level consistent with the limits set forth in this act.'" 

Tell that to House Speaker John Boehner (R-OH). 

"People have limits on credit cards. It doesn't say they're required to spend that much," Boehner said. 

So is $1.047 trillion in the BCA a "cap" as Republicans assert or a hard figure as Democrats suggest? 

"Nobody, in my opinion, misunderstood that we had made a deal, not a deal that was the cap," said House Minority Whip Steny Hoyer (D-MD). "I think what has happened is that they made a deal and they can't keep the deal." 

And why might Republicans conceivably try to "break" a deal? If they didn't have the votes. 

Conservatives and tea party loyalists blistered the House Republican leadership last year for making agreements like the debt ceiling accord and not slashing sufficient spending. In fact, many of the most-conservative voices in the House advocated that the GOP dial the discretionary spending figure all the way down to $931 billion. Still, conservatives jumped on board with Ryan's $1.028 trillion plan. Curtailing the topline figure to $1.028 trillion from $1.047 trillion showed a good faith effort by House GOP leaders to satisfy conservative voices. Secondly, many conservatives liked how Ryan's outline began to tackle entitlement (non-discretionary) spending, the most significant driver of the national debt. Thus, even though conservatives may have preferred deeper spending reductions, adjustments to entitlements sweetened the package for them. 

And how could Republicans scrap the $1.047 trillion level? 

It's simple. 

Budgets produce an inordinate amount of sturm and drang on Capitol Hill . But until they're binding, budgets won't carry most of the Congressional freight. 

What does matter? 

Appropriations. 

Remember, Congress's primary responsibility for federal spending deals with discretionary allocations. The House and Senate Appropriations Committees decide how much every cabinet department and agency get to spend and on what. A figure beneath $1.047 presents a challenge to Congressional appropriators who have to scramble to trim their budgets across the board. 

A few weeks ago, House Appropriations Committee Chairman Hal Rogers (R-KY) wasn't keen on dropping the topline figure from $1.047 trillion. But no fool he, Rogers recognized the challenge of cobbling together a budget which didn't significantly dent spending. 

"Leadership needed a number that the Budget Committee could pass," Rogers said, noting he preferred $1.047 trillion. "But we'll make it work. It's a number we'll have to work with." 

One can decant the problems House Republicans had with the $1.047 trillion figure from the BCA into the comments of Rep. Louie Gohmert (R-TX). Gohmert says the GOP still isn't complying with the Pledge to America, the political covenant that most House Republicans agreed to prior to the 2010 midterm elections. 

"I took the pledge. We said we'd reduce spending by $100 billion. We're not there," said Gohmert. 

Gohmert said he's undecided about voting for the Ryan budget. 

But Rep. Tim Huelskamp (R-KS) is not. 

"I will be voting against the plan," declared Huelskamp, a member of the House Budget Committee. "The Pledge to America promised better. And today, those assurances have not been met." 

The attrition of Huelskamp and potential nay votes from the likes of Gohmert and others probably isn't going to imperil the chances of passing the $1.028 trillion Ryan budget. But a spending figure below $1.047 trillion will make it tougher for both houses of Congress and President Obama to agree to the appropriations bills later this year. 

Remember, budgets don't truly count. But appropriations bills do. And if all 12 of the appropriations bills aren't approved by both the House and the Senate and signed into law by September 30, then the government could face a potential shutdown. 

Of course, Republicans will blame Democrats for pushing for higher spending. But top GOP leaders like John Boehner were clear last year about the potential calamity of a government shutdown. 

So what to do? 

Create what's called a "Continuing Resolution, or "CR" for short, to run the government past September 30 if all or some of the appropriations bills aren't complete. 

Continuing Resolutions are the norm these days in Congress. They take the place of individual appropriations bills when the sides can't bridge an impasse. And in most cases, CR's continue government spending at levels previously agreed to. 

What are "levels previously agreed to?" 

Not $931 billion. Not $1.028 trillion. But $1.047 trillion, the number set forth in the Budget Control Act from August, 2011. 

Remember, under the current rules, budgets don't matter much. But appropriations do. 

Otherwise, some form of a government shutdown could be in the offing just weeks before the 2012 election. 

Here's the irony: Congress actually stands a better chance of approving all 12 of the annual appropriations bills at the $1.047 trillion level. That's because the Budget Control Act demonstrated there were enough votes on both sides of the Capitol to run the government with the higher discretionary spending figure. 

Anything below that could cause problems. 

What did you think of those dirty, little secrets? 

Most Republicans will applaud Ryan for crafting a budget to ratchet down spending and harness entitlements. Some affiliated with the tea party movement will vent that Ryan's budget cuts aren't significant enough. Most Democrats will be apoplectic that the Ryan budget doesn't adhere to the $1.047 trillion threshold. 

Many will embrace the Ryan budget as a bold vision aimed at reducing spending. Democrats will portray it as a nightmare. 

It's really something of a fiscal aspiration. 

In Congress, budgets can be phantasms. Appropriations are real. 

And those are a couple of the dirty, little secrets about this year's budget process.
