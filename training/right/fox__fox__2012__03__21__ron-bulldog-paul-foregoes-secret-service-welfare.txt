If you could have a Secret Service nickname what would it be? 

Republican presidential candidate and Texas Rep. Ron Paul , who opted out of Secret Service protection because he thought it was a "form of welfare," jokingly said on " The Tonight Show with Jay Leno " Tuesday he would like to be called "Bulldog." 

"I would go after the Federal Reserve and all that big spending," Paul said. "I'm an ordinary citizen and I think I should pay for my own protection." 

There were plenty of laughable moments during Paul's interview. Leno compared a picture of Paul in his bathing suit to a recent and unflattering photo of Sen. Rick Santorum lounging around the pool in Puerto Rico without a shirt. 

Leno also played a video with Paul's head superimposed onto a karate fighter, but these antics did not distract Leno from questioning Paul about issues like birth control and how he felt about other presidential candidate's policies. 

Paul admitted he did prescribe birth control and the morning-after pill while practicing as an ob-gyn in Texas. 

"I was also putting myself out of business, all this birth control," said Paul, adding, "They had less babies." 

When asked if he thought women should have the right to choose, Paul replied: "Yes. I think so, but does the infant have a right to choose?" 

He mentioned the idea of abortion is one he hasn't been able to fully accept adding that changing the law didn't create abortions, but the "changing morality of people changed the law." 

As for his fellow presidential candidates, Paul labeled Romney a "flip-flopper" who does it because it's good politics, Santorum as a "fake conservative" whose record doesn't show a candidate desiring less government and Gingrich as "on the moon" regarding his claim to reduce gas prices to $2.50 a gallon. 

Paul also weighed in on whether Gingrich should get out of the race. 

"I'm not telling him what to do, but I think all three should get out of race," he said. 

Tuesday night, Paul finished third in the Illinois Republican Primary with 9 percent of the vote behind Romney and Santorum.
