Interesting at the very end of President Obama's Oklahoma event, a woman was overheard telling the president she was born at same hospital in Hawaii. 

Obama shot back: "Do you have YOUR birth certificate?" 

At an Irish reception with Prime Minister Enda Kenny earlier this week, the president was given a certificate of his Irish heritage. He dropped a joke then too. 

"This will have a special place of honor alongside my birth certificate," he said.
