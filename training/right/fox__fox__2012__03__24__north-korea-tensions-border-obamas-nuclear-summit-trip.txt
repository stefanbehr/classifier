SEOUL, South Korea -- President Obama is attending a three-day stop in Seoul for a Nuclear Security Summit where North Korea's flirtation with weapons borders the talks both literally and figuratively. 

The 53-nation summit is a follow-up to the 2010 one in Washington, where leaders outlined ways to keep the most sensitive nuclear materials from terrorists. Obama pushed for a goal of locking down all nuclear facilities by 2014 and this summit serves as a halfway mark to check on progress. 

This trip is Obama's third visit to South Korea since becoming president. He also most recently hosted a State Dinner for Korean President Myung-bak Lee at the White House late last year. 

The first major event Obama will take part after arrival in Seoul is visiting the demilitarized zone, or DMZ, one of the most fortified strips of land in the world. 

It's the border between North and South Korean and has nearly 30,000 U.S. troops in the area, along with 650,000 South Korean troops up against nearly a million North Korean soldiers. Obama will also meet and address U.S. troops during the DMZ stop. 

The historic area, which is a remnant of the World War II and the Cold War, is a symbolic and physical separation of the two Koreas, has also been visited by former Presidents George W. Bush, Bill Clinton George H.W. Bush and Ronald Reagan. 

Within the last couple weeks North Korea has escalated tensions and fears by announcing it would launch a long-range rocket in April, which would be a violation of U.N. resolutions. The move in part is seen as a way for the new North Korean ruler, Kim Jong-un, to flex his new power after taking over for his father, Kim Jong-il, who died in December. 

North Korea , known for provocation and cover-up, is saying the rocket would launch a satellite to commemorate the 100th anniversary of Jong-un's grandfather and the country's founding ruler, Kim II-sung. 

Recently the U.S. brokered a deal where they provided food to the nation, often facing serious food shortages, in exchange for a nuclear freeze. 

North Korea won't be the only nation on leaders' radars at the summit, as Iran and fears of it stockpiling nuclear materials to create a weapon are also expected to be a big topic. 

Tensions and talk of war and military action has risen in recent weeks, particularly because of Iran's hard-line provocation and verbal attacks against Israel and their continual secrecy to inspectors wanting to find our more about the country's nuclear ambitions, 

They also continue to be evasive to the international community about their intentions. 

Obama has emphasized diplomacy and sanctions against Iran, but also has stated ardent support for Israel and says all options are on the table, including military action. He recently stressed in a speech to a Pro-Israel lobbying group that "loose talk" of war was not helping and even criticized 2012 GOP presidential candidates for too easily throwing around the option. 

The president will also be meeting with other world leaders at the summit including Chinese President Hu Jintao, whom he also hosted a state dinner for in the U.S. and has tried to forge a global and economic relationship with. While they have areas to intersect and find commong ground, the fact that China is North Korea's closest ally could be a point of disagreement. 

"We certainly hope and recommend that China bring all the instruments of power to bear to influence the decision-making in North Korea along the lines that President Obama has advocated, namely to take a path that will bring to North Korea the dignity and the security that they say that they desire," said National Security Asia Director Danny Russell in a briefing ahead of the trip. 

While at the summit, Obama will also meet with Russian President Dmitri Medvedev, Turkish Prime Minister Erdogan and Pakistani Prime Minister Gilani. 

He'll hold and a press conference with President Lee and then also give a speech at Hankuk University of Foreign Studies. 

Obama and Lee are also expected to discuss the recent free-trade deal the two nations forged. 

The president first presented his nuclear security vision in a speech in Prague in 2009. He said, "I state clearly and with conviction America's commitment to seek the peace and security of a world without nuclear weapons. I'm not na ve. This goal will not be reached quickly--perhaps not in my lifetime...it will take patience and persistence...we have to insist, 'yes we can." 

Republicans have charged Obama is too easy to just talk and negotiate and not present a position of strength. He's also been criticized for a recent cuts to a program aimed to get civilian nuclear materials. 

The U.S is about one of 10 nations known or suspected to have a nuclear weapon. The administration says it's been able to get five countries to get rid of their highly enriched uranium. 

The leaders in Seoul are expected to not only talk about nuclear security, but also safety - which came into serious focus after the Japanese accident at Fukushima power plants. 

White House aides say this trip also underscores an administration initiative of engaging in the Asia-Pacific region, which the president emphasized in a trip to Australia and Indonesia last fall.
