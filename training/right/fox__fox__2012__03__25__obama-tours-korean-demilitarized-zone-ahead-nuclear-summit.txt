President Obama became the fourth sitting U.S. president to visit the Korean demilitarized zone, the sliver of land that divides North and South Korea and the most fortified border in the world. 

The zone lies just 35 miles north of Seoul, the South Korean capital. Seoul is the home of more than 10 million people and is hosting world leaders for a nuclear security summit. 

One of the world s biggest and most modern cities, Seoul is in the shadow of a communist North Korea , a country with nuclear weapons and unstable leadership. 

The president looked through binoculars across the volatile border to the north, and asked U.S. and South Korean military leaders to point out exactly where the border is located. 

The demilitarized zone, or DMZ, is an approximately 155 mile-long and just over two miles wide border between North and South Korea . 

The dividing line is mostly enforced by the two nations troops, but as part of a July 1953 armistice that halted the Korean War U.S. troops stand guard at an area on the South Korean side manned by the United Nations . It s been called the world s most volatile border and a place former President Bill Clinton described as the scariest place on earth. 

Before touring the border, President Obama spoke to U.S. and South Korean troops who stand guard there. 

You guys are at Freedom s Frontier, he told troops. The contrast between north and south could not be starker in terms of freedom and prosperity. 

North Korea is exists in a state of secretive, self-imposed isolationism. A remnant of Cold War communism, its leaders have retreated from the world economy, leading to mass poverty. 

Yet North Korea s nuclear arsenal and boisterous leadership have led it to be a central focus of world leaders. Despite White House attempts to focus on other nuclear security issues in Seoul, the trip to the DMZ at a time when new North Korean Leader Kim Jong Un is threatening a non-nuclear rocket launch draws attention to the threat next door. 

The president noted the gravity of the spot on his visit. 

There is something about this spot where there is such a clear line and such an obvious impact you ve had every day, he said. 

Presidents Ronald Reagan, Bill Clinton and George W. Bush also toured the Korean DMZ during their terms.
