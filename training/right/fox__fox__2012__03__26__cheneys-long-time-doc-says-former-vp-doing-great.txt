Dr. Jonathan Reiner, who has treated former Vice President Dick Cheney for many years, says his long-time patient is doing amazingly well following heart transplant surgery on Saturday. 

In a phone conversation, Dr. Reiner said he was amazed by what he saw this morning when visiting Mr. Cheney in the hospital. 

Less than 48 hours after receiving a heart transplant at Inova Fairfax Hospital in Northern Virginia, the 71 year-old former Vice President was out of bed and sitting up in a chair. A family spokesperson told Fox Mr. Cheney was spending time with his wife Lynne and his daughters, talking, and was even able to stand up a day after surgery.Dr. Reiner says it is "absolutely" possible a healthy heart could give him another 10 years, and adds "that was the goal."In terms of risks, Dr. Reiner says organ rejection, infection, and blood clots which can cause strokes are some of the issues his medical team will be monitoring. 

Dr. Reiner praised the former Vice President for not allowing a battery-powered medical pump that helped his heart since July 2010 to keep him from living an active life. The cardiologist says after he recovers Mr. Cheney will find "there is nothing he cannot do" with his transplanted heart. 

The former Vice President did not receive any kind of preferential treatment in terms of receiving a heart - according to Dr. Reiner. In fact, he notes Mr. Cheney waited "almost 21 months", whereas the average is 12 to 18 months. Dr. Reiner says Mr. Cheney never asked for special treatment, and " felt it was important that he wait his turn." 

Dr. Reiner did not want to speculate on how long Mr. Cheney will be in the hospital, but says his medical team is very pleased with his recovery so far. 

Dr. Reiner is the director of the cardiac catheterization laboratory at The George Washington University Hospital and professor of medicine at The George Washington University Medical Center.
