SEOUL, South Korea - President Obama made light Tuesday of the hot microphone moment he had the day earlier with Russian President Medvedev, where he said that after the November election, he'd have more "flexibility" on the issue of missile defense. 

Just as leaders were greeting one another and about to sit down at the opening session of the Nuclear Security Summit , Obama spotted Medvedev, looks over at him, puts his hands over the microphone in front of him with a big smile, and then goes to greet the Russian president. 

Obama was asked about the "flexibility" statement later while making remarks to the press about a nuclear safety agreement, and said "Arms control is extraordinarily complex, very technical, and the only way it gets done is if you can consult and build a strong basis of understanding both between countries as well as within countries." 

The open microphone comments he made Monday after a bilateral meeting with Medvedev happened right as journalists were being let into the room. 

Obama could be heard saying, "On all these issues, but particularly missile defense, this, this can be solved but it's important for him to give me space." 

Medvedev responds in English, "Yeah, I understand. I understand your message about space. Space for you..." 

Obama then brings up a frank statement about timing and what he can get done, "This is my last election. After my election I have more flexibility." 

Medvedev replies, "I understand. I will transmit this information to Vladimir," a reference to the next Russian President-elect Valdimir Putin. 

Building up missile defense in Europe, which the U.S. is for, and Russia is against, has been an issue of contention between the two countries. 

While later explaining the hot mic remarks Tuesday, he also noted the START treaty took them two years to get ratified. 

"I don't think it's any surprise that you can't start that a few months before a presidential and congressional elections in the United States and at a time when they just completed elections in Russia , and they're in the process of a presidential transition where a new president's going to be coming in a little less than two months," he added. 

He downplayed that he was making any new revelation, saying that in speeches he's noted wanting to reduce nuclear stockpiles and a barrier to doing that is building trust and cooperation on missile defense. 

Obama added he thinks they'll do better in 2013 and outlined steps he's taking. 

"[T]he only way I get this stuff done is if I'm consulting with the Pentagon , if I'm consulting with Congress, if I've got bipartisan support, and frankly the current environment is not conducive to those kinds of thoughtful consultations. I think the stories you guys have been writing over the last 24 hours is probably pretty good evidence of that," Obama said. 

Obama has struggled to agree with Republicans because they haven't been happy he hasn't given upgrades on the nuclear arsenal, something he promised two years ago. 

In response to the comments after being public, the White House initially issued a statement Monday and noted other elections were in play and the reality of the situation. 

"Since 2012 is an election year in both countries, with an election and leadership transition in Russia and an election in the United States, it is clearly not a year in which we are going to achieve a breakthrough." 



Watch the latest video at FoxNews.com
