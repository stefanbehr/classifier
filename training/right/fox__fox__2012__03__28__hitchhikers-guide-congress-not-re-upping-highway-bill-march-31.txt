What happens if Congress doesn't pass a highway bill by March 31? 

A lot. 

For starters, there would be an immediate layoff of Federal Highway Administration employees. Federal money to various highway programs around the country would not end immediately. But would generally peter out at the pipeline goes dry. 

Some states working on individual programs could be okay (at least for a while) if they have funds in the pipeline. 

But in many respects, this functions much like the FAA stoppage last summer. Only it's bigger. 

One of the most vexing issues with the 18.4 cent gasoline tax. In this case, March 31 is a deadline much like December 31 is for most tax provisions sunsetting on the books. If there isn't a gas tax on April 1, the federal government has no authority to collect that tax. That's certainly what happens when various tax proposals expire elsewhere in the tax code on January 1. 

Still, some believe that gas taxes could be collected. 

"If you don't think the federal government will figure out a way to collect the gas tax then you're living in la-la land," said Rep. Nick Rahall (D-WV), the top Democrat on the Transportation Committee. 

While the construction sector would be hit the hardest in terms of sudden unemployment and stalled projects, other areas impacted would include public transportation, retail, engineering and local governments. 

In the event Congress failed to pass an extension before current funding runs dry on March 31, there would be a $110 million loss in revenues per day - amounting to over $1 billion within ten days - all of which would be added to the federal deficit. 

Because legislation directly impacting these industries is generally only tacked onto massive measures such as this bill, taken together, this could highlight the importance of roads, bridges and transit for the U.S. economy. 

"There's a supply chain of jobs dependent on this surface transportation bill," said Robyn Boerstling, the director of transportation and infrastructure policy for the National Association of Manufacturers. Just a few of the companies affected would include equipment designers, concrete asphalt makers and traffic safety structure manufacturers. 

The lack of an extension would put a halt to road and bridge work, potentially meaning another year of deterioration and potholes, Transportation for America's David Goldberg said. Consequently, retailers who depend on the speed of shipping are concerned sales could be affected. 

"The condition of the transportation system and its ability to handle cargo quickly and efficiently are vital to retailers' businesses," David French of the National Retail Federation said in a statement last month urging the House to pass the legislation. 

Furthermore, thousands of people involved in construction projects would be left without funding to continue ongoing work just as the spring construction season begins. A shutdown would be a blow for an industry that's already undergone significant job losses. The Bureau of Labor Statistics estimated in January that the construction industry's unemployment rate hovered around 18 percent. 

It would also force a partial shutdown of the Department of Transportation. According to a 2010 estimate, it would require approximately 4,000 furloughs. The lights would go out at agencies such as the Federal Motor Carriers Safety Assistance Program and the Federal Highway Administration, which are responsible for carrying out safety regulations. 

States would also lose the authority to borrow money from the federal government, and they would have to cover the difference. However, few states have the budgets necessary to do so - and those with that ability wouldn't be able to cover it for long. 

A failure to pass the legislation before the month's end would also be especially ill-timed with rising gas prices. When trying to drive less to cut costs, consumers often turn to public transportation - which would lose funding. 

Robert Healy, vice president for government affairs for the American Public Transportation Association, added that transit ridership tends to increase with high unemployment, as the majority of riders use it to commute to work. 

"We believe that this is hardly the time to be cutting funds for public transportation," Healy said. "It's access to jobs." 

Congress may decide to enact an extension lasting only 18 months instead of the intended 5 years due to the roadblocks in the House. However, the uncertainty of whether agencies will receive federal funding could ultimately cost the federal government more money. For example, bonding agencies may charge companies more money if they don't think that programs have enough continuity or if they're not confident the federal government will be able to contribute. 

The brinksmanship could also cause transportation projects, such as bridge or highway repairs, to be delayed in the same way a full-on shutdown would. Contractors may choose to put projects off if they can't anticipate the amount of funding they would receive.
