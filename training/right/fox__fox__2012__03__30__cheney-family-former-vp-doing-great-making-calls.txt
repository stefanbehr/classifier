A spokeswoman for the family of former Vice President Dick Cheney says he is "doing great," less than a week after receiving a heart transplant. 

The spokesperson says Mr. Cheney has been making a lot of phone calls to family and friends. 

During his time at Inova Fairfax Hospital since a transplant operation last Saturday, Mr. Cheney has received a great deal of mail from people all over the country, said spokeswoman Kara Ahern. 

Ahern says the former Vice President has been calling people he doesn't know personally -- "perfect strangers" who have sent in cards and notes wishing him well. 

She tells Fox News, "most of them are shocked when he (Cheney) tells them who is calling." 

Ahern also says Mr. Cheney and his family are very appreciative of all of the well wishes and prayers.
