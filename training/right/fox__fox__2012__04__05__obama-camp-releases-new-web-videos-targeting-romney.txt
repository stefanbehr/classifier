President Obama's campaign released two new web videos this week targeting Mitt Romney -- once again showing that while Romney has not clinched the GOP nomination he's the presumptive opponent for the Obama campaign. 

A new ad, titled "Hold Mitt Romney Accountable," quotes the former governor as saying, "The president said something very revealing yesterday. He said that in a perfect world, the government could spend a lot more money. I disagree." 

The ad claims, "Mitt Romney is lying about President Obama. Again." 

The video follows with a clip of what the president actually said in his last weekly address, "If this were a perfect world, we'd have unlimited resources. No one would ever have to pay any taxes, and we could spend as much as we wanted. But we live in the real world. We don't have unlimited resources." 

A similar campaign video released Wednesday, "Mitt Romney Versus Reality," also compares Romney's take on the president's words with what the president has said. 

In addition to the defensive campaign ads, the Obama camp recently launched two new blogs, "Keeping GOP Honest" and "AttackWatch." As part of the campaign's "Truth Team," posts on both blogs have almost exclusively attacked Romney. 

Leaving the negativity to campaign surrogates, President Obama has taken a civil tone when making reference to his potential opponent. In remarks this week at the Associated Press Luncheon, the president merely took issue with Romney's use of the word "marvelous" to describe Rep. Paul Ryan's proposed budget. 

Romney will be campaigning in Pennsylvania this week before traveling to California. 

President Obama will be attending campaign events in Washington this week.
