Pawlenty Out of Veepstakes 

Former Minnesota Governor Tim Pawlenty (R) has ruled himself out of the VP sweepstakes. 

"I've taken my name off the list for vice president," Pawlenty said Tuesday on Fox News, removing himself from any consideration of being likely Republican presidential nominee Mitt Romney's running mate this fall. 

"I think I can help Mitt Romney in other ways as a volunteer," said Pawlenty. "I'm happy to do that. He will have a lot of great choices." 

Pawlenty highlighted three variables that are important to consider when selecting a running mate. They are "be ready to be president," "compliment and bring strengths to the ticket," and "bring something political to the table in terms of a big state, a swing state, [or] some other consideration that would be helpful to win the election." 

He called Florida Senator Marco Rubio one of the leading candidates because of the Cuban-American's compelling life story and highlighted the fact that the freshman lawmaker hails from a big swing state with "lots of electoral votes." 

Pawlenty also mentioned Ohio Senator Rob Portman, Wisconsin Representative Paul Ryan, New Mexico Governor Susana Martinez, Virginia Governor Bob McDonnell , and South Carolina Governor Nikki Haley as possible VP contenders. 

Pawlenty himself was vetted as a potential running mate for Arizona Senator John McCain in 2008 before McCain selected then Alaska Governor Sarah Palin . 

He also competed against Romney for the 2012 republican presidential nomination, but dropped out of the race in August after a disappointing finish in the Ames Straw Poll in Iowa. 

Pawlenty currently serves as a National Campaign Co-Chair for the Romney campaign.
