Mitt Romney said "of course" he would have ordered Usama Bin Laden killed. 

Romney made the comments while speaking to reporters in Portsmouth, New Hampshire. 

"Even Jimmy Carter would have given that order," added Romney. 

President Obama's re-election campaign has questioned Romney's willingness to take bin Laden out. 

It was one year ago this week that Obama authorized the U.S. military raid in Pakistan that led to bin Laden's death.
