A national security interest group released a web video chastising President Obama for his repeated use of "I" in taking credit for last year's killing of Usama bin Laden . 

Veterans for a Strong America, a non-partisan, non-profit organization of veterans focused on U.S. military issues, posted the video. The group claims that President Obama's recent comment, "I said that I'd go after Bin Laden if we had a clear shot at him and I did," is a political statement. 

The veterans' group accused the president of "spiking the football" over bin Laden's killing, ahead of the upcoming election. 

"Heroes don't seek credit" the video states, while showing shadowy images of a team of Navy SEALs . Navy SEALs carried out the raid on bin Laden's compound in Abbottabad, Pakistan , in May 2011. 

The video is a response to a recent Obama web video, "One Chance," which highlights the president's role in giving the order to execute the raid on bin Laden's compound. 

The campaign ad also questions whether Mitt Romney would have made the same decision if he were president. 

Romney says "of course" he would have. Obama has defended the ad.
