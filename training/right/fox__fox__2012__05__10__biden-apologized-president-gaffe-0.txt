Shortly before President Obama officially reversed his position on same sex marriage, Vice President Biden apologized in the Oval Office for getting ahead of him on the fiery issue, according to senior administration officials. 

The apology, which officials say was generated by the Vice President himself and not forced upon him, came Wednesday shortly before the President did an interview with ABC's Robin Roberts revealing that he had competed his "evolution" and now supports same-sex marriage. 

"The President has been the leader on this issue from day one," said Kendra Barkoff, spokeswoman for Biden. "And the Vice President never intended to distract from that." 

Officials say the President indicated he understood the Vice President had been speaking from the heart Sunday when he all but endorsed same sex marriage on NBC's " Meet the Press ," which caught other administration officials off-guard and sent the White House into damage control mode. 

Senior officials have maintained the President was not angry at his number two, in part because they claim that Obama was already planning to announce his decision before the Democratic National Convention in Charlotte. 

Nevertheless, Biden's comments appeared to box the President in, forcing him to speed up the timetable of his own announcement. 

Dr. Joel Hunter, a spiritual adviser to the President, said he received a phone call from the commander in chief informing him of the change of heart on Wednesday. Hunter says he expressed disappointment and warned the President there could be political backlash in November, especially because of how it dribbled out. 

"First of all it looked from the timing after the Biden announcement that he was being pushed into a decision that was political rather than simply value based," said Hunter.
