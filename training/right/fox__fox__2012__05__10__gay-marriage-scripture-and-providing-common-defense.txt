Which of these things is not like the others? 

The construction of an East Coast ballistic missile shield Aid for Pakistani counterterrorism effortsGay marriageHealth care for military retireesThe Pentagon's budgetThe fate of Air National Guard planes 

President Obama's endorsement of gay marriage dominated Beltway chatter Wednesday afternoon. 

The House Armed Services Committee labored throughout the day Wednesday and into the wee hours of Thursday morning, crafting a massive Pentagon funding measure. But the heat yielded by the gay marriage issue was so incandescent that it cauterized traditional military topics and spurred the feistiest debate of the marathon session. 

Most of these lengthy Armed Services meetings are sprinkled with discussions about MRAPs, JDAMs, engines for the Joint Strike Fighter and troop rotations. 

But the colloquy about providing for the common defense of the United States devolved into a debate about stoning, sin, parsonical rights, "gayness" and canonical interpretations of the Old Testament. 

Perhaps as Sun Tzu said, "all warfare is based on deception." 

The wrangling started over two amendments designed to curb the impact of gay marriage on the periphery of the armed services. 

Rep. Steven Palazzo (R-MS) concocted a provision which would ban the performance of same-sex marriage ceremonies on military installations. A "conscience clause" forged by Rep. Todd Akin (R-MO) would allow military chaplains to refuse to wed gay and lesbian couples. 

This comes just months after Congress formally ended the military's 18-year policy of "Don't Ask, Don't Tell (DADT)." DADT barred gay and lesbian service members from identifying themselves as such and prohibited the armed forces from inquiring about the sexual orientation of those serving the nation. 



"The president has repealed Don't Ask, Don't Tell and is using the military as a prop to promote his gay agenda," declared Akin. 

Palazzo's amendment was up first Wednesday night. 

"Many in Congress heard from a great deal of soldiers that homosexuality is morally wrong," Palazzo said. 

Palazzo's amendment didn't sit well with Rep. Adam Smith (D-WA), the leading Democrat on the House Armed Services panel. 

"This certainly insults gay and lesbian members serving in our military," Smith said. "Members of this committee are looking to turn back the clock and find new ways to discriminate against gay and lesbian service members." 

But those barbs were just the warm-up for the back-and-forth over Akin's amendment which aimed to protect military clergy. 

"People are being asked to do things that they don't agree with," argued Akin. 

Rep. Rob Andrews (D-NJ) asserted that Akin's amendment "does much more mischief than it prevents." 

Rep. Susan Davis (D-CA) suggested that Akin's idea was "inviting" a discriminatory treatment of gay and lesbian service members. 

"I don't think that's something we want to encourage," said Davis. 

Akin countered that the conscience protections were essential "because some of the changes have put a lot of stress in the military." 

But Rep. Loretta Sanchez (D-CA) took particular issue with Akin's amendment. In addition to giving protection to chaplains, Akin's effort shields service members morally opposed to homosexuality. 

Sanchez posed a hypothetical scenario to her colleagues about the consequences service members might face if they didn't, as she put it, "believe in gayness.'" 

Her use of the word "gayness" triggered chortles from the audience and raised the eyebrows of lawmakers and Congressional aides alike, stationed on the hearing room dais. 

Sanchez then dipped into an interpretation of the Scriptures. 

"Let's just say I read the Bible and it says gays should be killed. Stoned," Sanchez began. 

Lawmakers who were slumped in their chairs suddenly sat up, ram-rod straight. Those listening to the proceedings, leafing through copies of Roll Call set the publications aside. Reporters posted in the back of the hearing room began pecking feverishly on laptop keyboards. Rep. Jeff Miller (R-FL) and other GOP members shot steely glances at Sanchez and demanded doctrinal clarification from the Bible. 

Moments later, Sanchez quoted Scripture. 

"If you read Leviticus 20:13, it says man must be put to death if man has sexual relations with not a woman," Sanchez said. 

"That's the Old Testament," protested Rep. Austin Scott (R-GA). 

"It's the Bible!" shot back Sanchez. 

Scott then blasted Sanchez, declaring he was "taken aback by those comments." She tried to get a word in edgewise, inquiring if the Georgia Republican would yield. 

"I am not yielding any more time to you," snapped Scott. "I have heard enough." 

For the record, Leviticus 20:13 in the King James version of the Bible reads "If a man lies with mankind, as he lies with a woman, but of them have committed an abomination. They shall surely be put to death. Their blood shall be upon them." 

The committee voted in favor of Palazzo's amendment, 37-26. The panel also adopted Akin's amendment, 36-25. 

While the gay marriage debate grew testy during head-on exchanges in the Armed Services Committee, many lawmakers either ignored the president's announcement. Or at the very least, tried to tiptoe around it. 

"My personal belief is that marriage is between a man and a woman," declared Senate Majority Leader Harry Reid (D-NV) in a statement. "But in a civil society, I believe that people should be able to marry whomever they want, and it's no business of mine if two men or two women want to get married. The idea that following two, loving, committed people to marry would have any impact on my life, which or on my family's life always struck me as absurd." 

In an interview with Gerri Willis of Fox Business Network, House Speaker John Boehner (R-OH) echoed Reid. 

"I have always believed that marriage was between a man and a woman," Boehner said. "But Republicans here on Capitol Hill are focused on the economy." 

In short, most lawmakers would rather not weigh-in on the gay marriage debate. But perhaps it's no surprise the isuue reared its head in the House Armed Services Committee. After all, the military's wrestled with its policies addressing homosexuality and gender orientation for nearly 20 years now. And it may now be as old hat as conversations about Apache helicopters and Virginia Class submarines.
