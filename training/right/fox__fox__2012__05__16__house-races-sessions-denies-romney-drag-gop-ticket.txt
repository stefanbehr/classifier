Appearing on "Power Play with Chris Stirewalt" today Rep. Pete Sessions (R-TX), chair of the National Republican Campaign Committee, rejected forcefully the idea that Mitt Romney - who was derided by fellow Republicans for the better part of six months, during the GOP primaries, as insufficiently conservative - will be a drag on the Tea Party freshmen and other right-wing House members seeking re-election this fall. 

Still, Sessions acknowledged that his rank and file are "slightly different on some ideas" from Romney. 

"Let's be truthful about this," Sessions told me. "Every single Republican cannot wait to be seen with Mitt Romney onstage, talking about how we will go about changing not just the culture in Washington but reviving the free enterprise system back home. And Mitt Romney is a free enterprise champion. There's not one person in this country that from their front porch can't see Greece today. They understand what four years of this president, four years of Nancy Pelosi -- what she did, and of taxing and spending - has crippled this country. 

"We have the will to win. Republicans can work together. Even if we're slightly different on some ideas, we'll work together and come up with great things for this country. And we're going to go sell this fight, because we have the will to win." 

I asked Sessions about his strategy of tying local Democratic candidates to President Obama, when the president's job approval ratings appear to have stabilized at or near the 50 percent mark, and his personal approval remains high. 

"I like the president also," Sessions said. "I think a lot of people personally like the president. I want to see the president be successful. It's his policies that we disagree with." 



Watch Power Play Live Monday-Friday at 11:30am ET here: http://live.foxnews.com/ 

Watch the latest video at FoxNews.com
