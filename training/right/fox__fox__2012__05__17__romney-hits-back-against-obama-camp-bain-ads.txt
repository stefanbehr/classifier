Mitt Romney called President Obama's campaign "off target" in their vamped up attacks over his Bain Capital record, while talking to conservative radio talk show host Ed Morrissey yesterday. 

This marks the first time Romney responded to President Obama camp's ads characterizing him as a corporate vulture rather than job creator. 

The TV ads- which aired in five swing states this week- features testimonials from employees at steel mill, GSI, acquired by Romney's former private equity firm that shut down in 2001. They tell stories of cut jobs, lost healthcare and reduced pensions after Bain Capital failed to successfully reconstruct the company. 

Romney called the rhetoric "misguided" and pointed to the fact that he had left the company two years prior to the factory's closure. 

"They said, 'Oh gosh, Gov. Romney at Bain Capital closed down a steel factory,'" Romney told Morrissey. "Their problem, of course, is that the steel factory closed down two years after I left Bain Capital. I was no longer there, so that's hardly something that should be on my watch." 

Romney countered by touting the number of jobs Bain Capital investments created, citing 100,000 jobs, while urging Obama to look at his own record of job loss in the auto-manufacturing sector. 

"We were able to help create over 100,000 jobs...On the president's watch, about 100,000 jobs were lost in the auto-industry and auto dealers and auto manufacturers, so he's hardly one to point a finger. 

The Obama camp argued there is no comparison between the President's auto bailout and the record of Romney's former company."The president extended a loan to the auto companies to keep them afloat while they went through a managed bankruptcy," Stephanie Cutter said in a conference call. "That's a stark difference to what Mitt Romney did in the private sector, where he structured deals by purchasing companies, loading them up with debt, often paying himself fees and dividends out of that debt and leaving the companies hanging." 

In the interview, Romney also accused Obama of hypocritically approving the ads on Monday, the same day the president held a fundraiser at the Manhattan home of Tony James, head of the Blackstone Group, another private equity firm that, Romney noted, he and Bain had "previously made investments with." 

"He has no problem going out and doing fundraisers with Bain Capital and private equity people," Romney said of Obama. "The president is just misguided in his effort to try and divide Americans from one another and to try to disparage one part of our economy from the other or one person from another. This is not what America is." 

Obama camp defended the Bain ad, arguing that even though Romney was no longer actively involved in the company, he had a heavy hand in the negotiations as investor and owner that ultimately led to the closure of the factory. They also dismissed the charge that they were criticizing the private equity industry as a whole while speaking to reporters on Monday. 

"No one is challenging Romney's right to run his business as he saw fit, and no one is questioning the private equity industry as a whole. That's not what this is about," Stephanie Cutter of the Obama re-election campaign said on a conference call. "This is about whether Romney's business experience qualifies him to make the right decisions as president.''
