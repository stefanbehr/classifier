The man known for putting Illinois Governor Rod Blagojevich behind bars for corruption is stepping down from his role as US Attorney of Illinois' Northern District, after more than 10 years. 

Outside of the Department of Justice, Patrick Fitzgerald is not a household name but he is arguably one of the most powerful US Attorneys in the country and the longest serving US Attorney in Chicago history. 

Over the past decade in Chicago , Fitzgerald, 51, has compiled an impressive crime-fighting resume of high profile cases, ranging from corruption and international terrorism to organized and violent crime. Most recently, Fitzgerald's team secured a conviction against former Illinois Governor Blagojevich who is now serving a 14 year prison sentence in Colorado. 

In 2003, Fitzgerald served as Special Counsel to the Justice Department during the investigation of the CIA leak case that led to the indictment of I. Lewis " Scooter" Libby , an advisor to Vice President Dick Cheney . Fitzgerald was the lead counsel in the federal trial against " Scooter" Libby , who was eventually convicted for perjury and obstruction of justice. 

At a press conference in Chicago Thursday, Fitzgerald told reporters the decision to leave his job wasn't an easy one. When asked why now Fitzgerald said, "people have terms for a reason. For the office, it's important that there be change... But just because you think you do things that are great doesn't mean someone can come in with a fresh view or a fresh set of eyes and decide to do things differently. And I think it should be change from the top .As for what's next - I don't know, and that's sincere. 

There has been speculation for months that Patrick Fitzgerald could be considered to replace current FBI Director Robert Mueller who is required to step down by September 2013. Fitzgerald said he hasn't spoken to anyone in President Obama's administration about the possibility of leading the FBI. "I'm not presumptuous enough, I know there's been reports that I am on a short -list, I've never seen a short-list and have no reason to believe I'm on it. But I'll just say this: I love public service, I don't know what I am doing next but public service is in my blood and I'd like to find a way to balance public service and whatever I do next and my family obligations." Fitzgerald says, if and when the call comes with an offer in public service he will "think long and hard" about it before making a decision. 

Fitzgerald is also well known for his work in the fight against terrorism. While working at the US Attorney's office in New York City, he helped prosecute cases related to the 1998 bombings of the US embassies in Kenya and Tanzania and 1993 bombing of the World Trade Center. Fitzgerald says the US has made "remarkable progress" in the fight to stop Al-Qaeda. "We shouldn't underestimate them but I think it's remarkable that we've prevented anything close to the scale of 9/11 for 10 years." 

During a lighter moment, Fitzgerald entertained a question about what he would want written on his tombstone. Fitzgerald smirked and said "I don't like thinking about my tombstone, since I know I'll never read it." After a moment, he answered the question: "He tried his best." 

Fitzgerald will leave his position on June 30th. No word yet on who will replace him.
