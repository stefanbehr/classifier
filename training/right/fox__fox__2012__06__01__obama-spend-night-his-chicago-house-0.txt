Chicago, IL - The comforts the White House not to be outdone, sometimes you just need a good night of sleep in your own bed. President Obama will overnight in Chicago Friday night, and the commander-in-chief is staying at his primary residence. 

The president "is looking forward to spending the night in his own house for the first time in quite a while," White House Principal Deputy Press Secretary Josh Earnest told reporters on Air Force One . 

Last month during the NATO summit, the president stayed in a hotel to not further disrupt the city traffic. But Friday the president will stay at his Hyde Park residence alone. The first lady and their daughters are not on the trip and without the White House staff, Mr. Obama may even cook for himself. 

He "might even make himself breakfast in the morning," Earnest said.
