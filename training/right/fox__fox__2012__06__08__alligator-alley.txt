There's an old saying that you should never taunt the alligator until you've crossed the creek. 

Perhaps someone should mention this creed to House Speaker John Boehner (R-OH). The Ohio Republican hadn't even made his way down to the creek bank Thursday morning when he provoked the journalistic reptilia which inhabit the Congressional marsh. 

Boehner hit the door of the House Radio/TV Gallery Studio for his weekly news conference when he let out a loud, audible sigh - seemingly laced with exasperation. The 30 or so reporters who gathered to hear Boehner chortled at the Speaker's irritation. Perhaps they were amused that yet another round of weekly question and answer sessions managed to pique the usually unflappable Boehner. After all, wide-ranging press conferences are challenging. On Thursday, reporters asked Boehner about a contempt of Congress resolution for Attorney General Eric Holder, mandatory year-end spending cuts, the European economic meltdown, the transportation bill, student loans and what to do about leaks from the intelligence community. So it's no surprise that yet another one of these conclaves managed to rankle Boehner. 

But still en route to the lectern, it was Boehner who scored the first question at his own press conference. 

"Would you like to be me?" Boehner asked reporters rhetorically after they noted his sigh. 

Boehner's question hinted at the stresses he faces as Speaker of the House - such as the internecine fights he referees over the debt ceiling or the struggle to gin up enough votes to pass a troubled highway measure. 

But Boehner's "changing spaces" interrogatory was also a brief, "walk in my shoes" moment. In other words, would you like to trade places and undergo an inquisition like this each week? 

"This is the weekly alligator feeding," said Boehner, drawing another laugh from the scribes. "The trick is feeding the alligators and not get bit." 

The alligators who double as Capitol Hill reporters do bite sometimes. And sometimes, those who feed the alligators bite back. 

Less than hour earlier, House Minority Leader Nancy Pelosi (D-CA) stood at the same podium for her weekly version of the "alligator feeding." Of course, the Calfiornia Democrat didn't characterize the session like that. Instead, a smiling Pelosi opened the floor after brief, opening remarks. 

"With that I would be pleased to take any questions," Pelosi said. 

And this being the Congressional Everglades, there's usually a question or two at any news conference which have more bite than the others. 

Liz Harrington of CNS News asked the Calfiornia Democrat about lawsuits various Catholic institutions filed against the Obama Administration over contraception rules in the health care reform law. 

"Those people have a right to sue. But I don't think they are speaking ex cathedra for the Catholic Church and there are people in the Catholic Church, including some of the bishops, who have suggested that some of this may be premature," responded Pelosi. 

Harrington noted Pelosi's Catholic faith and quickly tried to follow up about whether she agreed with the church's teachings. 

This time, the bite came from the dais, not the press swamp. 

"You know what?" replied a curt Pelosi, raising her hand in protest. "I do my religion on Sunday in church and I try to go other days of the week. I don't do it at this press conference." 

There was was lots of alligator biting underway Thursday over at the Rayburn House Office Building. The House Judiciary Committee summoned Eric Holder for a hearing. And much of the snapping back and forth took place not between lawmakers and reporters, but between Members of Congress themselves. This was particularly prominent when House Oversight Committee Chairman Darrell Issa (R-CA) pressed the Attorney General on whether he's been responsive to Congressional inquiries about why Fast and Furious went awry. 

Holder told Issa he had been responsive to Congressional subpoenas. The California Republican cut off Holder as he told the panel about thousands of pages of documents the Department of Justice produced for lawmakers. Issa, who doesn't chair the Judiciary Committee, even wagged a finger at Rep. Sheila Jackson Lee (D-TX) who attempted to intervene on Holder's behalf. 

"The lady is out of order!" scolded Issa. 

Issa then refocused his attention on Holder after the Attorney General reiterated that he felt the Justice Department complied with the subpoenas. 

"No! Mr. Attorney General, you're not a good witness!" lectured Issa. "A good witness answers the questions asked." 

Rep. John Conyers (D-MI) then implored Issa to allow Holder to fully respond to his questions. Issa then conceded "that there was hostility between the Attorney General and myself." 

Despite the sniping between lawmakers, Holder told the hearing that he didn't feel like alligator food. 

"I'm not feeling hostile. I'm pretty calm," said Holder. 

For all of the daily alligator biting, there was none to be found in the House chamber. That's when House Minority Whip Steny Hoyer (D-MD) seized the floor to laud Pelosi on her 25th anniversary in Congress. 

"And now we note - some celebrate, others note - her attaining a quarter of a century of service in this body," beamed Hoyer as lawmakers laughed. 

Boehner followed Hoyer to the well of the chamber to honor his counterpart. 

"The gentle lady and I from California have differing political philosophies. We've had some real battles here on the floor over the 22 years I've served with her. But the gentle lady and I have a very, very workable relationship. We get along with each other fine. We treat each other very nicely and actually have a very warm relationship," said a glowing Boehner. 

A few minutes later, the House resumed a voting sequence. And the triumvirate of Hoyer, Boehner and Pelosi met in the well of the chamber, put their arms around one another and engaged in a brief group hug. 

This was a short respite from the daily alligator biting on Capitol Hill. Democrats against Republicans. Lawmakers against the president. The media against lawmakers. There are lots of alligator feedings in this version of "alligator alley." And as Boehner says, the trick is to not get bitten.
