The White House Press Secretary stopped just short of calling the violence plaguing Syria a civil war and said Russia isn't responsible for deaths there despite a State Department claim that Russia is still providing assault helicopters to the Assad regime. 

Jay Carney initially said long-term arms deals between Russia and Syria enhance Bashar al-Assad's "capacity to wage war" on the Syrian people before stopping mid-sentence to tone down his take on the attacks. 

"This is not new, but it is an issue that we have discussed with the Russians and we, obviously, view with some concern," Carney said at the daily White House press briefing Wednesday. "[I]t enhances Assad's capacity to wage war -- or wage violence upon his own people, which is what he's been doing." 

Asked later to clarify whether Syria is in the midst of a civil war, Carney would only say conditions are quickly becoming more favorable for sectarian civil war. 

"[T]he situation there is deteriorating, it is deteriorating quickly; it is horrific what Assad is doing to his own people; and that the window of opportunity to bring about a transition to a democratic future for Syria is closing and will close, and if it does, the chance for a broader sectarian civil war in Syria will be enhanced greatly," Carney said. 

As he was peppered by more questions about a possible Syrian civil war, Carney added that discussions about terminology are not the issue and that the U.S. is focused on working with allies to pressure Assad's regime out of power. 

Without singling out Russia as Secretary of State Hillary Clinton did earlier Wednesday , Carney said the administration calls on all nations to halt arms sales to Assad's government, something he calls a part of the larger conversation with Russians. Clinton accused Russia of providing assault helicopters to the Syrian military as arms supply contracts between the two nations continue to be fulfilled. 

"The issue of helicopters is part of a broader concern that we've expressed and will continue to express as we discuss with the Russians and others about next steps that need to be taken to help bring about the political transition that is so essential for the future of the Syrian people," Carney said. 

The Russian government responded to Clinton's charge, claiming the U.S. is also sending weapons to Syria, something both the White House and State Department adamantly deny. For months the White House has sought a diplomatic solution to the crisis and has only provided humanitarian assistance to the Syrian resistance. 

Despite claims Assad is getting military assistance from the Russians, Carney wouldn't say Russia is complicit in civilian deaths in Syria. 

"That is not what we're saying. We are simply saying that it is well known that Syria and Russia have an arms-supply relationship that goes back a half century, including providing arms and helicopters," he said. "A change in that relationship will only happen in the context of a larger Russian decision to join the international consensus on Assad's departure."
