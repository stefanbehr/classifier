The Democratic National Committee intends to take its bracketing efforts on the road. 

"Sources say during the Romney bus tour, Democratic truth telling teams will be dispatched for [Romney] events," reports Fox News Chief Political Correspondent Carl Cameron. 

The presumptive Republican Presidential nominee plans to embark on a 5-day bus tour dubbed the "Believe in America: Every Town Counts" tour. 

Mr. Romney will pass through 6 swing states critical to his election this Fall - the tour will begin in New Hampshire on June 15th, then go through Pennsylvania, Ohio, Wisconsin, and Iowa before ending in Michigan on the 19th. 

The choice of states is not by accident either - the campaign believes all 6 of chosen states are up for grabs in November. It will also be the first time Mr. Romney has been to Wisconsin since the state's embattled Republican Governor, Scott Walker , fended off a Democratic challenger in a recall election last week.
