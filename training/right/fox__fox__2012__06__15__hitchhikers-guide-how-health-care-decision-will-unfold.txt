Remember in college when your political science professor would tape the grades on to his office door at the end of the semester? Do you recall the crush of students who would press against the doorway, eyes sweeping up and down the paper for their student ID number and the grade typed next to it. 

How about back in seventh grade when the basketball coach posted the final cut list for the team on locker room door before school? 

A similar ritual will unfold at the Supreme Court in the next few weeks. 

And it will all start with a fleeting scan of the eyes. 

The Supreme Court is due to release its opinion on the health care reform law before the end of the month. It's the most-anticipated decision since Bush v. Gore settled the presidency in 2000 and one of biggest opinions in the past 60 years. The ruling will trigger prodigious media coverage. Members of Congress, law experts, talking heads and pundits will analyze and quarter the opinion ad nauseam. They'll dissect its political impact and what it means for the legacy of President Obama and House Minority Leader Nancy Pelosi (D-CA). 

Expect a week-long media carnival as soon as the Court rules, triggering a major Washington media event. And depending on the day, the decision could trump all other news out of Washington: a contempt of Congress resolution for Attorney General Eric Holder. The fate of funding for transportation programs set to expire at the end of the month. The cost of loans for college students, on schedule to increase in July. Even a verdict in the case of baseball superstar Roger Clemens, on trial for allegedly lying to Congress about performance enhancing drugs. 

No one knows exactly what day the Supreme Court will hand down its decision. But the Court is scheduled to release opinions this Monday the 18th, Thursday the 21st or June 25th. Plus, it's likely the Court could extend the term by a few days. 

But on each of those days, all of the mechanics will start with a rapid scan of the eyes. 

The High Court will post its "grade" on the Affordable Care Act - or determine whether the controversial health care reform law either qualified for the varsity squad or is relegated to intramurals. 

The custom works like this: 

A clutch of reporters will jockey for position in the public affairs office on the north end of the Supreme Court around 9:45 am on days opinions are due. Some scribes will head into the courtroom itself to watch the brief session. But those who want to get the decision out fast will wait in the anteroom of the public affairs suite. 



At a few minutes before 10 am, the Supreme Court's public affairs officers, Kathy Arberg and Patricia Estrada, will enter the room holding large cardboard boxes. Stowed inside are the slip opinions of the case or cases the Supreme Court is rendering judgment on upstairs. Audio from the courtroom is then piped in. 

A gavel raps at precisely 10 am. 

"Oyez, oyez, oyez!" intones the court marshal, signaling the start of the session. 

"Oyez" is a medieval way of saying "shut up and listen." It's pronounced "OH-yay." Not "Oh yeah!." If that were the case, Kool-Aid Man might burst through the wall of the Supreme Court, bound for the Hart Senate Office Building down the street. 

The marshal continues as the swarm of reporters listen in rapt attention. 

"All persons having business before the Honorable, the Supreme Court of the United States, are admonished to draw near and give their attention, for the Court is now sitting. God save the United States and this honorable Court." 

At the end of this declamation, the court almost immediately begins to hand down its opinions. Like clockwork, Arberg, Estrada and their assistants farm out slip opinions to the clutch of reporters as quickly as humanly possible. 

This is truly a feeding frenzy. The reporters can't get their hands on the booklets containing the opinions fast enough. 

And that's where the scanning comes in. 

When the justices assemble this Monday, there will still be 15 outstanding cases on which the Court has yet to render a decision. Some of the opinions are big and will command significant news oxygen by themselves. Expect the reporters to rifle through the opinions in search of FCC v. Fox Television Stations and Arizona v. U.S., to name a few. FCC v. Fox Television Stations focuses on whether the government wields too much control when fining broadcasters for "indecent material aired during live events. Arizona v. U.S evaluates the constitutionality the state's controversial immigration law. 

But what reporters are really searching for is National Federation of Independent Business v. Sebelius/Florida v. Department of Health Human Services . 

All hell will break loose if the press spots that citation. 

Reporters will dash back into the press room and begin tearing through the pages, trying to divine what the Court decided. Twitter will light up with news of the decision. Networks will break into coverage. 

But will everyone know what the opinion says right away? 

Probably not. At least not definitively. And initial news reports might be fragmentary - warning the audience that a decision is in - but no one really knows what it means just yet. 

First of all, the opinion could very well run several hundred pages. It could be exceptionally nuanced. And it could be a fractured opinion. Remember, the justices were asked to rule on four different parts of this case. For instance in Bush v. Gore , the Supreme Court ruled 5-4 on one section but was followed with a parenthetical 7-2 on another. It wouldn't be a surprise if the outcome of this case resembles a tennis score at Wimbledon. 

So, expect news to break that the Supreme Court has handed down the decision. But it may not be immediately evident what the justices decided. 

Then there's the mad dash. 

And you thought the best track and field events would be run at the London Olympics later this summer. 

In addition to a squadron of reporters inside the Supreme Court, a number of TV reporters will assemble outside on the sidewalk. There will be another batch of TV reporters ready from the "Senate Swamp Site." This location sits just off the steps to the U.S. Senate and is hard-wired so the television networks can plug in and go live immediately. The locale also presents a beautiful television shot of the Supreme Court from the distance across the Capitol's grassy lawn. 

It takes four minutes and 30 seconds to walk from the steps of the Supreme Court to the Senate Swamp Site. But someone really booking it can probably make it out the Maryland Avenue exit of the Supreme Court, dash across First Street and then navigate the Congressional lawn in under two minutes. 

If you're watching a television feed emanating from the Senate Swamp Site with the Supreme Court as the backdrop, keep your eyes peeled. You might spy a team of reporters, if not an intern or two, spill out of the court in the rear of the shot and come motor across the grass with the health care opinion in hand. A few networks will immediately hand off to some reporter who arrives panting, who tries to talk on their air while they're bent over when their hands on their thighs like an NBA player in the fourth quarter. Someone else will start to rifle through the booklet to try to glean some sense of the opinion. 

And then there is Congressional reaction. 

The Affordable Care Act was one of the most-controversial pieces of legislation to move through Congress in decades. It elicited wildly passionate views on both sides of the issue. In fact, large groups of protesters massed outside the Capitol and even heckled Democratic lawmakers as they walked across the street to vote on the bill. 

Many on Capitol Hill are anticipating a reprise of 2010 as people from around the country flood the phone lines. Multiple Congressional offices are already planning press conferences. But no one knows when those could be or what lawmakers might say. After all, people have to read and digest the Supreme Court's opinion first. 

At least that's what some are hoping. 

The House Republican Conference sent out a brochure this past week to its members and staff about the looming health care decision. 

"If you fail to plan, you plan to fail," headlines the pamphlet. 

The document reminds offices that the Supreme Court could issue "one of three rulings - fully repealed, partially repealed or upheld." 

The House GOP leadership advises its members that they "should be prepared to communicate with your constituents" and that "Americans will be looking to their representatives for a response to the decision." 

Some Congressional offices began peppering the press this week about how soon they should call a press conference to respond to the opinion. But the House GOP advises its members to take some time to consider what the High Court found. 

"Many of your constituents and the press will ask if you read the entire opinion," says the pamphlet. "Consider scheduling time immediately after the ruling's release to read the decision in its entirety." 

Of course, if the decision comes on June 25, neither the House nor Senate are expected to be in. The Senate might meet later in the day, but not until afternoon. The House is not scheduled to be in at all on the 25th. That could mute any immediate Congressional response. Some argue that might not be a bad thing. Republicans excoriated Democrats for not "reading the bill" when they passed it two years ago. A timeout might give everyone a respite to digest what the High Court decided. 

Certainly, the most active chatter in Washington these days is what the Supreme Court will decide. Will they torpedo the "individual mandate" which requires all Americans to purchase health coverage? Will they uphold the entire law? Will the decision be so muddled that no one is happy and it creates chaos in the health care industry? 

Hard to say. 

Perhaps the best response to that interrogatories comes from that titan of American jurisprudence, 19-year phenom Bryce Harper of the Washington Nationals : "That's a clown question, bro."
