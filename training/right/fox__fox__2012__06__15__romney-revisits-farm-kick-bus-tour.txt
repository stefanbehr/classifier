STRATHAM, NH - Returning to the site where he launched his Presidential campaign one year ago, Mitt Romney Friday echoed the same message he delivered then - the country is broken, and he's the man to fix it. 

"Over the past year, it's become clear that good things begin here," a nostalgic Romney told the enthusiastic crowd gathered at Scamman Farm in southern New Hampshire. "America can do better - and with your help, we will." 

Kicking off a 5-day bus tour, Romney presented himself as the clear alternative to President Obama, and argued Americans "are tired of a detached and distant President who never seems to hear their voices." 

"I hear you," he went on. "And I'll make sure to continue to hear the voices of the people of America when I'm President of the United States." 

The bus tour will take the presumptive Republican nominee through six crucial battleground states in the Northeast and Midwest, focusing heavily on small towns and rural areas. It will be his most sustained stretch of campaigning since clinching the nomination in April, and will present him with the opportunity to meet potential voters in areas he hasn't normally visited. 

"We'll be traveling on what are often called the back roads of America,'" Romney said referring the upcoming trip. "But I think our tour is going to takes us along on what I call the "backbone of America." 

The Obama campaign fired back immediately following the speech, saying the President, not Romney, has presented a plan to restore wealth to the middle class. " Mitt Romney continues to offer nothing but empty and angry rhetoric," Obama campaign spokesperson Lis Smith said in a statement. "America can't afford Romney Economics." 

And while the all too familiar sniping between the two campaigns was limited on Friday, one had only to look up to see it on full display - dueling messages were competing via banners flying behind two planes. One read "Romney for President 2012" while the other, put up by Moveon.org read "Romney's Every Millionaire Counts Tour."
