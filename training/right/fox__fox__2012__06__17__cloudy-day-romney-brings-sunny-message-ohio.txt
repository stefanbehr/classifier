BRUNSWICK, Ohio -- Amid ominous storm clouds on his first stop during the Ohio portion of his bus tour, Mitt Romney offered a brighter vision for America with him at the helm. 

"This is courage, you guys out here wearing garbage bags," Romney said Sunday, referencing the makeshift rain gear several soaked supporters sported during a pancake breakfast in northeast Ohio. "I guess you didn't know the rain was going to come." 

"But it looks like the sun is coming out. I think that's a metaphor for the country," he continued, looking off at blue skies on the horizon. "Three and half years of dark clouds are about to part. It's about to get a little warmer around this country, a little brighter." 

Appearing with Ohio Sen. Rob Portman -- a short-list candidate for VP -- Romney again accused the president of not giving the middle class "a fair shot," saying his policies have weakened their position. 

Over the last several months, Romney has taken great pains to contrast how his handling of the economy would differ from President Obama's. His current five-day bus tour through America's small towns in six crucial swing states is his greatest effort yet to reach rural voters, whom he argues are the "backbone" of America and have struggled under the current administration. 

And, arguably, no place is more important for him on this trip than Ohio. Its electoral votes have been critical in deciding the last several presidential elections -- President Obama carried it four years ago by four points, and the state went President George W. Bush's way in 2000 and 2004. 

Sitting in the so-called "Rust Belt," Ohio has suffered through decades of tough economic times as the automobile, steel and agricultural industries shrank. 

Ohio, however, has recently seen a rebound in growth, with the unemployment level dropping from 10.6 percent in 2009 to 7.3 percent this month, almost a full percentage point lower than the national average. 

As promised Sunday, the sun did come out, which Romney said is an omen of things to come. 

"Things are drying out here," he told the crowd. "I can tell you something else -- the sun is coming out in this country. Our brightest days are ahead. Things are getting better in America as long as we get off the course he's put us on."
