For big-time politicos like David Axelrod, longtime senior adviser to President Obama, boasting about how often the commander-in-chief listens to your advice usually boosts your personal stock. 

Instead, Axelrod likes to tout how often this president ignores him. 

"I often say what I like about him so much is that he listens to me so little," he told Fox News in an interview. 

Axelrod believes highlighting the fact that the president has repeatedly shot down his top political adviser's advice shows that Obama's style as a leader is someone who has demonstrated real courage on top issues -- like the auto bailout -- that were politically unpopular. 

"I was in the room when the president made the decision to intervene to save the American auto industry," recalled Axelrod. "I told him that people in Michigan -- in Michigan -- weren't even in favor of intervention and that you know, it would not be a political popular decision. And he said, 'I'm sure you're right, but if we don't do anything we're going to see a million jobs at least go away.' " 

The story is an Axelrod two-fer: It shows off the president going against the grain, while also reminding everyone that presumptive Republican presidential nominee Mitt Romney opposed the bailout, which has turned out to be more popular over time. 

When asked about the Republican narrative that Romney is more of a CEO than Obama and would offer crisper decision-making to fix the economy, Axelrod is not shy about reminding that the GOP candidate has been known to flip-flop. 

"You have to have good judgment, and then you have to have the courage to make decisions," said Axelrod. "And when you are president of the United States, particularly in challenging times, that means making decisions that aren't always politically popular, that carry with it some political weight." 

Romney supporters like Sen. Rob Portman, R-Ohio, rumored to be on the short list as a possible vice presidential pick, counter the president's leadership problem is that he does not get his hands dirty enough on Capitol Hill. Portman cited the stimulus law and health care reform as major pieces of legislation in which the president left too much of the details to lawmakers. 

"He has not been engaged as other presidents have," Portman told Fox News. "And you think about it, every time there has been a major change like tax reform or Social Security reform or entitlement reforms of any kind, the kind of thing everybody knows has to be done, it is led by the executive. ... So I think it does require presidential leadership and we simply haven't seen that." 

Axelrod dismissed that criticism by noting he was in the room when the Recovry Act was first discussed during the transition in December 2008, and the president-elect laid out the parameters of the legislations during an interest four-hour meeting. "And I would say that 80 percent of what was in the final plan was what was discussed," he recalled. 

As for health care, Axelrod notes the president worked the phones and traveled the country to just barely get enough votes to pass it. 

"So I don't know whether you call that getting your hands dirty, but he certainly has put his shoulder to the wheel to get that done and without that (leadership) it wouldn't have happened," he said. 

Axelrod says from the beginning of the long health care debate, Obama believed the "greatest gamble" would be to do nothing at all because sticking with the status quo could slowly bankrupt the nation in soaring health costs. 

The president faced fierce opposition throughout that fight amid questions about whether he was overreaching, over-regulating and overspending. And while the victory will be tainted if the Supreme Court strikes down the individual mandate in the days ahead, Axelrod suggests it was all worth it. 

"Leadership is not just making the decisions that are easy and popular," he said. "Leadership is making the decisions that are sometimes difficult and fraught with risk -- personal risk, political risk. He's been willing to make those decisions and in all the time I have been with him, the yard stick I have used with him is what he felt is best for the country. And that was the case here. 

Democrat Ed Rendell, author of the new book "Nation of Wusses" that urges political leaders to show more guts, gives Obama enormous credit for courage in the health care fight. 

"That was a case where he was truly not a wuss," Rendell told Fox News. "He led and he was bold." 

Rendell is the former Pennsylvania governor who endorsed Hillary Clinton over Obama in the 2008 primaries, so he does not often read from the White House talking points. And when asked about whether Obama left too much detail to Congress on the stimulus and health care reform, he seems a little conflicted about whether Clinton would have been bolder. 

"If I were President Obama and I had his approval ratings at the time [in 2009], I would have basically written those bills and get Congress to try to pass them with some slight modifications because had the wind at his back," said Rendell. "Whether President Hillary Clinton would have done that, it's hard to speculate. But she probably would have had a different style. But again, it's what works and the President did get stimulus through. He did get health care through. So his opinion ended up at the bottom line to be effective." 

Rendell is less impressed by the President deciding to appoint the Simpson-Bowles commission to deal with the deficit -- but then choosing to ignore its recommendations -- which seems to challenge Axelrod's narrative about political courage. 

"He has acted courageously on a number of things -- Simpson-Bowles he was slow to the mark," Rendell said. "He eventually did come for a Simpson-Bowles type solution when he and John Boehner came this close to the big deal. But he was slow and initial leadership may have helped." 

Axelrod's defense is that even former Sen. Alan Simpson (R-Wyo.) has said that if the President embraced the report it would have sunk like a stone. "Democrats would have picked apart the entitlement reforms," Axelrod said, "and Republicans in the House would have immediately opposed it" over tax hikes. 

Maybe so, but the deficit is still one of those issues that has made the President vulnerable because he promised to cut it in half within four years, when in reality it has soared. 

Axelrod believes on balance the American people will see the President as someone who has tackled many of the tough issues in a job -- here's the poke at Romney again -- where you simply can not be constantly flip-flopping. 

"You don't want someone who is you know reaching for the polls and you don't want someone who is second guessing themselves or wondering about the political calculus when there is so much on the line," said Axelrod. "The country has benefitted in my view from having a President who has been willing to do that."
