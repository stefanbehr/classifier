Reps. Jason Chaffetz, R-Utah, and Adam Schiff, D-Calif., sparred Sunday over whether U.S. Attorney General Eric Holder should be held in contempt regarding the Fast and Furious scandal. 

It was the latest example of how each side is digging in ahead of a possible floor vote this week. 

"The Department of Justice and the White House need to come clean and abide by a properly issued subpoena put out in October and provide the documents necessary so we can solve this problem and make sure it never, ever happens again," Chaffetz told Fox News. 

But Schiff said what started as a "bona-fide investigation" has gone political. 

"The Justice Department recognized after it gave the 9,000 documents, that the attorney general testified nine times and gave an unprecedented level of discovery that the committee wasn't going to take yes for an answer," rebuked Schiff. 

Schiff urged the White House to "stand its ground." 

The Fast and Furious program was launched by The Bureau of Alcohol, Tobacco, Firearms and Explosives out of Arizona and sought to follow guns across the border to drug cartels and weapons smugglers but somehow lost track of them. 

The House Oversight and Government Reform Committee's push for documents pertaining to the case has led up to the current confrontation over contempt proceedings against Holder. 

Chaffetz claimed there are actually 140,000 documents that need to be looked at regarding the program. 

"We deserve to see the documents," Chaffetz said. 

Schiff said he hopes the dispute can be resolved before an anticipated vote on the House floor this coming week. 

"If the attorney general wants to avoid this constitutional conflict and will provide some of the documents that are covered by the privilege, if the other side wants to meet halfway. Unfortunately, I think the other side want to fight more than anything else," he said.
