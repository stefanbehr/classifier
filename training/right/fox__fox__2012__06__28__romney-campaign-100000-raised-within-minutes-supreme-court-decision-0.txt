A Romney campaign aide said Thursday the campaign had raised $3.2 million from more than 20,000 donors, roughly twelve hours after the Supreme Court ruling that allowed President Obama's health care law to largely stand. 

The aide said the campaign has raised $100,000 online organically, just 60 minutes after the high-court ruling. 

Meanwhile, the Democratic Congressional Campaign Committee also moved to raise money off the decision. 

The group sent out an email that in part stated: 

"BREAKING NEWS: The Supreme Court just voted to uphold President Obama's Health Care law. 

Republicans threw everything they had at us, and they lost. With you standing with us, we can carry this momentum into November and win a Democratic majority to keep making progress. 

Please donate $3 or whatever you can right now."
