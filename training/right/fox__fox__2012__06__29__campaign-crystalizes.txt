House Speaker John Boehner (R-OH) had a problem a few days ago. The chatter in Washington was rampant that the Supreme Court would strike down the mandate requiring all Americans to purchase health insurance. In fact, the talk was so serious that it compelled Boehner to pen a memo to his GOP colleagues about tempering their glee should the Supreme Court rule in their favor. 

"There will be no spiking of the ball," Boehner warned. 

But Boehner's priorities quickly shifted from end-zone celebrations on Thursday after the Supreme Court rendered the Affordable Care Act constitutional. 

"Well, we don't have to worry about that," snorted Boehner when asked about the football instructions. 

That's because the High Court's ruling quickly refocused Boehner's attention. He offered a pronouncement on that score Thursday afternoon for the legions of reporters who squeezed into the House Radio-TV Gallery to hear his take on the decision. 

"Elections have consequences," proffered the Ohio Republican. 

That maxim isn't anything people haven't heard before. But in this case, it was more of a battle cry. 



The GOP's nadir was the election of President Obama in 2008. Democrats fortified their majority in the House and climbed within two seats of the crucial 60-vote threshold in the Senate. 

To Boehner, that's precisely the problem because it granted Democrats the authority to pass the health care reform. 

"The election in 2008 clearly had a consequence that most Americans disagree with," said Boehner. He suggested that Republicans sideswiped the Democratic juggernaut with its dramatic capture of the House of Representatives two years later. 

But Boehner was really saying that the Supreme Court's 5-4 ruling helped Republicans unveil their 2012 campaign. 

Sure, there will be plenty of talk about jobs and the economy. Boehner launches every press conference these days with the rhetorical precept "The American people are asking, where are the jobs?'" The GOP easily links sluggish economic growth to the health care law as businesses say the statute could inhibit hiring. 

But in short, "Obamacare" was lightning in a bottle for Republicans at the ballot box in 2010. Raucous town hall meetings. Allegations about "death panels." The rise of the tea party. Rep. Joe Wilson (R-SC) hectoring the president during a Joint Session of Congress. 

Republicans always planned to talk about health care reform in 2012. But now they have a totem. Health care worked wonders for Republicans in 2010. And they think Thursday's ruling gives them plenty of ammo this fall. 

"The election just changed," said Rep. Dave Schweikert (R-AZ) seconds after learning of the ruling while standing in the crowd near the Supreme Court. "The election just rolled back to 2010 because it was driven by Obamcare. It's back in play. The Supreme Court just woke up a sleeping giant." 

And Schweikert was right. The Supreme Court decision immediately ignited the same fervor which stoked people to dress in tri-corner hats in 2010. 

"We will not comply! We will not comply!" chanted a large group of anti-health care reform demonstrators in front of the Court. 

An unidentified woman then commandeered a microphone and wailed at the throng. 

"We will not comply with this unconstitutional tax! So come get me right now!" she implored. 

Minutes later, Rep. Charlie Rangel (D-NY), the deposed Chairman of the Ways and Means Committee who helped craft the bill, walked with a cane to a meeting in the Capitol. The New York Democrat suspected the Supreme Court decision returned health care to center stage. 

"It could be a mobilizing factor for the Romney people," suggested Rangel. 

But at the same time, Rangel thought the judicial victory could boost Democrats, too. 

"It gives us the opportunity to re-sell the bill which we did not do before," he added. 

That's a challenge for Democrats. They struggled to market the measure the first time. So it begs the question how they could re-package it now. Plus, any new marketing could fall on deaf ears because Republicans were ready to pounce. 

"The Supreme Court spoke today, but they won't have the final word," said House Republican Conference Vice Chairwoman Cathy McMorris Rodgers (R-WA). "The American people will have the final word in November." 

Boehner postponed his press conference by a couple of hours Thursday. When it finally started, he stepped aside and let McMorris Rodgers and a group of Republican female lawmakers take the lead. All had backgrounds in medicine. 

"We are more determined than ever we will succeed," predicted Rep. Nan Hayworth (R-NY), a Hudson River Valley ophthalmologist. 

"I came to Washington because of Obamacare," said Rep. Renee Ellmers (R-NC) who is a nurse. 

"Today begins the fight," warned Rep. Ann Marie Buerkle (R-NY), a nurse and health care attorney. "Today begins the true debate on how we are going to reform health care in the United States of America." 

It appeared as though Republicans had again found their groove. While the Supreme Court failed to rule the way the GOP wanted, they believed the justices may have presented them with a chance to recapture some old political magic. 

Of course, Democrats felt the Republicans were already well into campaign gear Thursday. And not just because of health care reform. 

The marquee issue on the House floor Thursday was a plan to slap Attorney General Eric Holder with two contempt of Congress citations for failing to turn over documents related to the gun program known as Fast and Furious. Democrats were apoplectic about the effort. More than 100 of them boycotted one of the votes by walking out of the chamber. 

"Just when you think you have seen it all and that they couldn't possibly go any further over the edge, they come up with something like this," mused House Minority Leader Nancy Pelosi (D-CA). "It's stunning." 

Holder immediately honed in on how the GOP may use the contempt resolutions for their benefit. 

"Today's vote is the regrettable culmination of what became a misguided, and politically motivated, investigation during an election year," he said. 

Just a few months ago, few thought it was possible that the House might vote on contempt for Holder. The death of border patrol agent Brian Terry became a cause c l bre in many quarters of the country. It's especially true in conservative, rural strongholds where firearms are ingrained in the culture. Every House majority engineers a number of roll call votes during a Congress which are designed to make members of the other party sweat. Some of these votes are contrived so campaign committees can tailor campaign commercials around an orchestrated vote if a lawmaker from a swing district votes the "wrong" way. 

That's not entirely what happened here. But Republican political operatives are more than pleased. 

Earlier in the week, Rep. Jim Matheson (D-UT) declared he would vote for contempt. Matheson is a moderate to conservative Democrat. Redistricting placed Matheson in the most-GOP leaning district held by a Democrat. 

Rep. Ben Chandler (D-KY) is another Democrat who voted yes on contempt. He won re-election in 2010 by only 648 votes. 

In all, 17 Democrats voted yea on one contempt citation and 21 on the other. 

One lawmaker who many thought might vote yes was Rep. Heath Shuler (D-NC). Shuler's demonstrated an independent streak and often bucked his party while representing a conservative-leaning district in western North Carolina. But Shuler's leaving Congress at the end of this term. 

"While I strongly believe that the Department of Justice should fully cooperate with Congress to ensure transparency in the Fast and Furious' operation, this motion lacks an enforcement mechanism to make it anything more than politically motivated," said Shuler. "The majority has opted to use this vote as a fundraising mechanism to rally the base. It is no wonder I have opted to retire." 

During the health care debate of 2009 and 2010, Republicans blasted Democrats for never having read the massive health care bill. 

"We have to pass the bill so that you can find out what is in it," Pelosi declared famously. 

On Friday the House is slated to debate and vote on a controversial measure to prevent transportation funding from expiring Saturday night. The House GOP posted the text of the bill online at 4:24 am Thursday. In the Republican's 2010 campaign document known "The Pledge to America," the GOP indicated everyone would get to read bills well ahead of time. 

"We will ensure that bills are debated and discussed in the public square by publishing the text online for at least three days before coming up for a vote in the House of Representatives," the pledge said. "Legislation should be understood by all interested parties before it is voted on." 

The House then incorporated that provision into its rules package at the beginning of this Congress. 

With all of the pandemonium that enveloped Capitol Hill Thursday, very few lawmakers have read the transportation measure. The package is expected to pass with bipartisan support. But whether lawmakers read bills in advance probably won't be a campaign issue in 2012. 

That's because the campaign seemed to crystallize around two other issues on Capitol Hill Thursday. Which means you can expect to hear a lot about health care reform and contempt of Congress in the coming mon
