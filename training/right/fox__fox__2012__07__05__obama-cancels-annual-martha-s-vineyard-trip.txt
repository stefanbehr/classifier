President Obama will forego a summer vaction this election year. 

In the midst of his 2012 campaign, the White House reportedly canceled hotel reservations on Martha's Vineyard, the Massachusetts island where the first family has vacationed the last three summers. A senior administration official confirmed to Fox News: "The first family is not going to Martha's Vineyard." 

It's tradition for the sitting president to escape the Washington, D.C., heat and humidity with a family summer vacation. Martha's Vineyard has been a favorite holiday spot for past presidents including the Clintons. 

But presidents tend to scale back their summer vacation plans during election years. 

Both President Bushes, for instance, opted not to take their traditional vacation to the family compound in Kennebunkport, Maine, when up for election. President George H.W. Bush, in the summer of 1992, instead huddled down with his national secuirty team at Camp David to focus on the situation in Iraq. President Clinton spent a week in Jackson Hole, Wyo., in August of 1996, rather than the more luxiousous Martha's Vineyard vacaton that re-election year. In 2004 while running for re-election, President Geoge W. Bush spent one week in July and one week in August at his Crawford, Texas Ranch. But that was scaled back dramatically from Bush's first three summers in office, in which he took the enitre month of August off in the Lonestar State. 

Mitt Romeny spent a long weekend around the 4th of July at his family summer home in Wolfeboro, N.H., while Obama toook a few days with his family at the presidential retreat, Camp David. The Obamas' only vacation this year was their winter trip to Hawaii for Christmas and New Year's. 

Later this month Romney will venture to London for the opening ceremonies of the Olympic games. His wife Ann's horse is competeing on the U.S. Olympic dressage team. Obama is not attending the summer games but rather sending the first lady to lead the U.S. delegation to London.
