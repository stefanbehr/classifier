In the battleground state of Iowa, there is a stand-off. 

It's between presumptive Republican presidential nominee and the state's Christian conservatives. This fall, they'll need each other and yet they have, at best, a rocky relationship. 

"Romney has said a lot of the right things in this campaign, this election but they look back at his record at his rhetoric in the past and it quite frankly doesn't add up, doesn't match," says Bob Vander Plaats, CEO of the Family Leader which is an influential social conservative organization in Iowa. 

Among the issues Christian conservatives believe Romney has been "soft" on are abortion, same-sex marriage and healthcare. And Vander Plaats was among the doubters. 

He was among a group of social conservatives prior to the Iowa Caucuses who were trying in vain to unite the anti-Romney vote behind a single Republican candidate. 

Of course now, Romney is the GOP flagbarer, up against the Democratic incumbent Barack Obama . If you ask Vander Plaats how he'll vote in November, note how he phrases his answer. 

"I will vote against President Obama." 

Not for Romney but, against Obama. The difference might be the difference between winning a toss-up state like Iowa...or losing it...and maybe the White House , too. 

Conservative syndicated radio talk-show host, Iowan and noted Romney-basher Steve Deace, likens the state's Christian conservative vote to a pie. 

"(Romney's) gonna get seven-percent of the pie," says Deace, "the question how big is that pie?" 

Deace is very well-connected to Christian conservatives around the state and across the country. In his opinion, Romney has blown opportunities to win the enthusiastic support of this key Republican constituency. 

"North Carolina...had...a (same sex) marriage (ban on the ballot) earlier this year. He never went there to campaign for that issue. He never went there to support that issue." 

Efforts to court Christian conservatives have not been by Romney, in person. The campaign did bring in the former campaign manager for Rick Santorum's presidential bid, Mike Biundo. He's been charged with social conservative outreach for the Romney campaign. Biundo has been in touch with Deace and presumably other big-name Iowa Christian conservatives. 

There are encouraging signs for Romney in Iowa. Just this week, the Des Moines Register reported it had seen internal polling numbers which had Romney running as well as Republican Congressman Steve King in the deep red, conservative turf of western Iowa. 

The state GOP chairman A.J. Spiker says, "I think social conservatives will be very much united behind Governor Romney this fall in Iowa." 

Plus, the registered voter edge has dramatically swung. When Obama took Iowa in 2008, there were 110,000 more Democrats registered to vote than Republicans. The advantage now belongs to the Republicans, by 21,000. 

But if Iowa remains a close presidential contest, those currently un-enthused Christian conservatives could be the difference for the state's six Electoral College votes. 

"It's a turnout game now. You need to get this base turned out. And you need to get those independents to come with you as well," says Vander Plaats. 

But Vander Plaats, Deace and others say Christian conservatives cannot be driven in droves to the polls solely by their dislike for Obama's policies. They say the sales pitch needs to come from Romney himself...in person...and preferably...soon.
