by Cristina Marcos 

In a swipe at Mitt Romney , Democratic Rep. Sander Levin announced Wednesday that he'll introduce a bill requiring presidential candidates to release 10 years of tax returns. 

The proposal comes amid growing pressure on Romney to release more than the two years' worth of returns he's pledged. 

"I think that Governor Romney should set the example and release his returns. I think more and more, there is the understanding that he should do so," said Levin, the top Democrat on the tax-writing House Ways and Means Committee. "And if there are problems with it, let's have a discussion about the problems." 

Romney has resisted calls to disclose more of his tax returns. He argued that Democrats would unfairly misrepresent facts about his tax returns, assets and accounts. 

"The opposition research of the Obama campaign is looking for anything they can use to distract from the failure of the president to reignite our economy," Romney said in an interview with National Review Online on Tuesday. "And I'm simply not enthusiastic about giving them hundreds or thousands of more pages to pick through, distort and lie about." 

Levin said that he intends to introduce the bill before the month-long August recess. His amendment to the 1978 Ethics in Government Act would require presidential candidates to disclose the location of each offshore account, the amount of capital gains income from investments, information about compensation agreements and details about their assets. The current law was enacted in the aftermath of the Watergate scandal to set standards for government officials to release their financial history to the public. 

The Michigan Democrat denied that the legislation is directly aimed at disqualifying any particular candidate. 

"It's not a question of qualification. It's a question of information. The public needs to know the activities of a candidate," Levin said. 

But some of the provisions of the measure echo specific issues the Obama campaign has raised, such as Romney's offshore accounts. Romney has also come under scrutiny for the length of his official tenure as CEO at Bain Capital and whether his firm engaged in outsourcing. 

Levin further noted that debate on the issue of Romney releasing his tax returns comes as Congress gears up for overhauling the tax code next year. 

"We're going to have a major battle on tax reform. I think we should have some understanding what has been the past practice of candidates, and with full transparency participate in that debate and lead that debate," Levin said.
