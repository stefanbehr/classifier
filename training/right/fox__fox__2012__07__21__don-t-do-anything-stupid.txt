"However, in the meantime, there is more we can do in Congress." 

Nearly 130 House Republicans penned a letter to House Speaker John Boehner (R-OH) and Majority Leader Eric Cantor (R-VA) this week. The missive's authors are members of the Republican Study Committee (RSC), the most-conservative bloc of GOPers in the House. 

"We appreciate your willingness to schedule a vote on the full repeal of ObamaCare," the letter read. "We should continue efforts to repeal the law in its entirety this year, next year, and until we are successful. However, in the meantime, there is more we can do in Congress." 

The GOP-controlled House has voted 33 times in the past year-and-a-half to repeal, dismantle or defund health care. The House voted 244-185 to again rescind the law just days after the Supreme Court upheld the statute. So what exactly did the Republican Study Committee have in mind? They told Messrs. Boehner Cantor precisely what they were thinking in their letter. 

"In Federalist No. 58, James Madison wrote that the power over the purse may, in fact, be regarded as the most complete and effectual weapon...for obtaining a redress of every grievance.'" 

The Constitution awards Congress broad latitude when it comes to controlling government spending. So that's the "more" that the Republican Study Committee demands in its letter to leadership. 

The letter continued. 

"Since much of the implementation of ObamaCare is a function of the discretionary appropriations process, and since most of the citizens we represent believe that ObamaCare should never go into effect, we urge you not to bring to the House floor in the 112th Congress any legislation that provides or allows funds to implement ObamaCare through the Internal Revenue Service, the Department of Health and Human Services, or any other federal entity," wrote the lawmakers. 

In short, the law may stand. But it's just a skeleton if lawmakers slash all funding. 

In 2011, Republicans aimed to defund the law through the annual Labor-HHS appropriations bill. Known colloquially as "Labor-H" on Capitol Hill , that piece of legislation would fund most of the programs called for in the Affordable Care Act (ACA). But Republicans ran into a problem. They could never extricate that bill from the subcommittee which pays for health care. They didn't have the votes. And if you can't get the votes, you can't do a lot in Congress. 



But things were different this year. 

The panel's chairman, Rep. Denny Rehberg (R-MT), was able to secure enough votes to pry the bill out of the subcommittee this week. Rehberg is locked in a tight Senate contest with Sen. John Tester (D-MT). Rep. Jeff Flake (R-AZ), who is also running for the Senate, and Rep. Cynthia Lummis (R-WY) were opposed to the measure last year because the plan didn't cut enough money. They had reservations again this year. Flake ultimately voted no. But despite her concern about accounting gimmicks in this year's package, Lummis was an eventual yea. She authored an amendment to trim an additional 5.5 percent from the entire bill. But her colleagues defeated that austerity plan. 

So, Rehberg's panel was able to finish a bill that eliminates $123 billion in spending for the ACA over the next five years. The bill's now prepped to go to the full Appropriations Committee next week or the week after next. 

But much to the chagrin of many House conservatives, the spending bill is going nowhere. 

"That bill will never see the floor of the House," opined a senior House GOP source with knowledge of the bill. "There's no way." 

Why? 

Well, much like the issue that stymied that piece of legislation in the subcommittee, it might not get the votes in the full House. 

Why? 

Passing the Labor-HHS spending bill on the House floor could prove to be an onerous task for the GOP. Certainly defunding health care is a touchstone for conservatives. And while this legislation eliminates a chunk of spending, the cuts might not be deep enough for some Republicans who see the package through the same lens as Jeff Flake. Secondly, throughout this Congress, Boehner has repeatedly turned to Democrats to find the necessary votes to lug controversial bills across the finish line. That wouldn't be the case with this bill. Democrats are apoplectic about the health care cuts. That's to say nothing of shaving money to Planned Parenthood, among other things. Third, even if the House okayed the bill, it would die in the Senate and President Obama wouldn't sign it anyway. 

Those circumstances create a problem bigger than some may realize. 

It's one thing to engineer a vote to repeal the health care law as the House did a few weeks ago after the Supreme Court decision. It's quite another not to approve an annual spending bill to fund a major portion of the government by September 30. That's the end of the government's fiscal year. The House and Senate plus the president must agree on all 12 appropriations bills by that date or the government could face a shutdown. 

If members of the Republican Study Committee had their way, they would force this bill to be the legislation which the House sends to the Senate. But it's even more complicated than that. No one on Capitol Hill is under any allusions that the House and Senate are going to jointly pass any or all of the 12 spending bills which run government by the end of September. House and Senate leaders will have to concoct some sort of a stopgap bill to keep government running past then and probably well into 2013. That way, either President Obama or President Romney can wrestle with a new set of lawmakers over funding or defunding various programs - including health care. 

However, even an interim piece of legislation (known as a Continuing Resolution or "CR" in Congress-ese) could face trouble too. The Republican Study Committee told the GOP brass that "we urge you not to bring to the House floor in the 112th Congress any legislation that provides or allows funds to implement ObamaCare." A CR designed to avoid a government shutdown would do just that. Otherwise, there could be a conflagration. So now it's a question of just how much House conservatives really want to dig in their heels. 

"The Speaker is caught in a vice of his own doing," said Rep. Rosa DeLauro (D-CT), the top Democrat on the appropriations panel which funds health care. 

For the record, no one in Congress is talking about a government shutdown. And Republicans know that forcing a shutdown over health care funding in the CR could virtually hand the Democrats the election on a platter. So something's got to give. And despite the RSC's insistence and the GOP trumpeting its 33 votes to bury the ACA, Congress will probably fully fund the law this fall. 

Rep. Tim Huelskamp (R-KS) is one of the most-conservative members of the freshman class. He's often bucked leadership since coming to Washington. Huelskamp voted against averting a government shutdown in the spring of 2011. He can't stand the health care law. But the Kansas Republican realizes his side is in a bind with November looming. 

"We don't want to shut the government down just to win an argument," Huelskamp said. 

He believes the election could go in the GOP's favor at both the presidential and Congressional levels, thus improving chances to repeal or defund the health care law. So while he's not happy Congress didn't defund the ACA, he's willing to bide his time. 

Rep. Steve King (R-IA) has been one of the most-vehement Republican voices to call for the repeal of the ACA. But King says it's all over but the shouting. 

"The die is cast," said King. "Boehner made clear that he didn't want a fight over Obamacare' funding...because the public is not prepared for a shutdown." 

Just not the public. But lawmakers aren't ready for a shutdown either. Which is why one can expect moves in the next several weeks to keep the government humming past September 30 - and presumably fund the health care law. 

"They have to compromise and maybe some reasonable Democrats and Republicans will work this out," said Rep. Norm Dicks (D-WA), the top Democrat on the House Appropriations Committee. 

The RSC was wise to cite Madison's writing about the "power over the purse" in Federalist #58. Congress carries the ultimate spending authority and can slash money for the health care law if it wants to. But perhaps the most astute advice on this front comes not from Madison, but from what your father used to tell you when you were going out with friends on Saturday night back in high school: don't do anything stupid. 

Republicans want to cleave health care reform. But they want to win the election even more. A government shutdown triggered over funding the health care law could have catastrophic consequences for Republicans. The multiple government shutdowns led by former House Speaker Newt Gingrich (R-GA) in the mid-1990s is proof of that. So sticking to their guns over defunding the health care law in the Labor-HHS spending bill or a CR to keep the government operating might fall into the realm of what your father was talking about. 

Republicans might not like this choice. But with the election coming, they don't want to do anything stupid.
