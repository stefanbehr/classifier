By Chad Pergram Cristina Marcos 

House Speaker John Boehner (R-OH) today told Republicans that the defense sequester and the possible tax hike looming early next year are the biggest threats to the U.S. in the so-called "fiscal cliff." 

"There's the threat to our security, posed by the looming defense sequester, which will hit our military with arbitrary cuts that will endanger our security," Boehner said, according to a source in the conference room. "And there's the threat to our economy, posed by the tax hike looming on January 1, that will hit millions, including small businesses." 

Boehner told his caucus that the automatic cuts to defense loomed because President Obama tried to punt the issue. 

"Let's remember why we have the sequester. We have it for one reason: because the President of the United States didn't want to deal with the debt limit again before the presidential election. Because the president didn't want to be inconvenienced, he came up with the sequester," he said. 

Congress is in the initial stages of gearing up for negotiations likely to begin in earnest after the November elections. Automatic cuts of $1.2 trillion split between defense and domestic programs outlined in last summer's deal to raise the debt limit, also known as the Budget Control Act, will begin in January unless lawmakers agree to alternative cuts. A litany of tax cuts, including the payroll tax cut and the Bush-era tax cuts, are also set to expire at the end of the year. 

President Obama has proposed extending the Bush-era tax cuts on Americans' first $250,000 of income. The Senate is expected to hold a key procedural vote on Wednesday on Democratic legislation that echoes the president's position. The Republican-controlled House intends to vote on a measure next week that would extend current rates for all income levels. 

Senate Democrats last week said that they would refuse any fiscal deal to extend tax cuts on the wealthy without any provision to increase revenues. They're trying to avoid a repeat of last year's agreement to raise the debt ceiling, which did not include any new taxes. 

The Speaker further urged Republicans to stay firm on preventing any tax hikes. 

"We have the high ground in this fight, and the Democrats know it," he said. "Let's stay on offense," 

House Minority Whip Steny Hoyer (D-MD) said that "the country would be well-served" if lawmakers could forge an agreement on taxes and the sequester this summer.
