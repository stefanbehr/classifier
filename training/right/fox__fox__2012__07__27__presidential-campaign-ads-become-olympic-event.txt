The opening Olympic ceremony Friday is a time for the host city of London to shine. But stateside, it'll also be a mega-platform for the U.S. presidential campaigns. 

Obama for America unveiled a new ad Friday, which will air during the Olympic Games ceremony. " I Believe " features Americans working in various blue-collar jobs coupled with a recent a speech from President Obama. 

"I believe that the way you grow the economy is from the middle out," Obama says in the ad. The ad was released the same day the Commerce Department announced GDP decreased to 1.5 percent in the second quarter of 2012, down from 2 percent in the first quarter of this year. 

The RNC also released an economy-focused ad Friday, set to run this week during the Olympics. The ad claims that "while Americans waited for help, billions were spent in foreign countries." 

In reference to the $800 billion American Recovery and Reinvestment Act of 2008, the RNC ad continues, "millions went to political insiders, millions more unaccounted for." The ad concludes by asking, "his plan for a second term: do it again. Are you with him?" 

Priorities USA, an Obama-supporting super PAC, released an ad earlier this week highlighting Romney's financial ties to China , Switzerland , The Cayman Islands , and India. The ad was edited from footage of Mitt Romney at the 2002 Olympics in Salt Lake City, Utah. After a copyright complaint from the International Olympics Committee, the ad was pulled. 

Restore Our Future, the pro-Romney super PAC, is also set to run ads during the Olympics. The details of the ads have yet to be released.
