Americans for Prosperity, a conservative political advocacy group concerned with economic policy, will begin airing an ad this week attacking the president's handling of the debt. 

The ad references the president's pledge in 2009 to cut the deficit in half during his first term and his offer for a "one term proposition." This is the first time Americans for Prosperity has released ad directly calling for the defeat of President Obama. 

A spokesman for the Obama campaign responded to the ad by saying, "President Obama has already signed $2 trillion of deficit reduction into law and has proposed a balanced plan to reduce the deficit by more than $4 trillion over the next decade." 

Americans for Prosperity will spend $7 million for the ad for TV time during its first full week of air time. The ad will be aired in 11 swing states and new ads will be released by the group over the next four weeks. 

Mitt Romney and the Republican National Committee jointly released a new ad on Tuesday accusing President Obama of gutting welfare reform by dropping work requirements. The ad claims, "Under Obama's plan, you wouldn't have to work and wouldn't have to train for a job. They just send you your welfare check." 

White House Press Secretary Jay Carney responded to the Romney ad by calling it "outrageous," "dishonest," and "false." Carney went on to say that the attack is "an utter misrepresentation of the president's policy." 

At a rally in Illinois Tuesday, Romney praised the bipartisan accomplishment on welfare reform in the 90s, but warned, " I hope you understand that President Obama in just the last few days has tried to reverse that accomplishment by taking the work requirement out of welfare." Romney promised, "I'll put work back in welfare."
