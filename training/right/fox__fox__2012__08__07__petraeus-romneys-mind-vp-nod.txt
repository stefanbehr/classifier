Stirring the veepstakes pot just a little more, Drudge Report published an "exclusive" story Tuesday claiming President Obama thinks rival Mitt Romney wants to name Gen. David Petraeus as his running mate. 

The White House quickly shot down the claim. Romney, though, did not respond when reporters asked him directly Tuesday whether he met with Petraeus in New Hampshire , as suggested by the Drudge story. 

The report claimed Obama "whispered" to a fundraiser this week that he thinks Romney wants to name Petraeus. "The president wasn't joking," the source said, according to Drudge. 

The report succeeded in prompting a question at Tuesday's White House press briefing. 

White House Press Secretary Jay Carney cautioned the media to be "mindful of your sources." 

"I can say with absolute confidence that such an assertion has never been uttered by the president." Carney said. "And again be mindful of your sources." 

"Drudge is wrong?" a reporter asked, incredulously. 

"Apparently so," Carney said. 

Carney went on to say Petraeus is "currently serving very well" as director of the CIA, a post he assumed after leading U.S. and NATO forces in Afghanistan. 

/* Style Definitions */ 

table.MsoNormalTable 

{mso-style-name:"Table Normal"; 

mso-tstyle-rowband-size:0; 

mso-tstyle-colband-size:0; 

mso-style-noshow:yes; 

mso-style-priority:99; 

mso-style-qformat:yes; 

mso-style-parent:""; 

mso-padding-alt:0in 5.4pt 0in 5.4pt; 

mso-para-margin-top:0in; 

mso-para-margin-right:0in; 

mso-para-margin-bottom:10.0pt; 

mso-para-margin-left:0in; 

line-height:115%; 

mso-pagination:widow-orphan; 

font-size:11.0pt; 

font-family:"Calibri","sans-serif"; 

mso-ascii-font-family:Calibri; 

mso-ascii-theme-font:minor-latin; 

mso-fareast-font-family:"Times New Roman"; 

mso-fareast-theme-font:minor-fareast; 

mso-hansi-font-family:Calibri; 

mso-hansi-theme-font:minor-latin;} 

Romney was coy when asked in an interview with Fox News for an update on the VP process. 

"I'm a week closer than I was a week ago," he said. "I am not going to give you anything on the VP front."
