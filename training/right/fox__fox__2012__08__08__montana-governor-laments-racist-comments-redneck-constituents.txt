Montana Gov. Brian Schweitzer had some curious remarks last month about his own constituents - describing some of them as rednecks with a penchant for making racist comments. 

Schweitzer told the Ohio Democratic Convention , "All over Montana , you can walk into a bar, a caf or even a school or a courthouse and just listen for a while as people talk to each other, and you will hear somebody, before very long, say something outrageously racist about the people who have lived in Montana for 10,000 years." 

"I decided I can't turn the heart of a 45-year-old redneck. But if we start early enough. If we start with a tender child -- so we passed Indian Education for All." 

The governor was trying to stress the importance of that education act, a law that requires children in Montana to learn both American and Indian history. The suggestion was that younger Montanans would be more open to such diversity education. 

The comments came shortly after Schweitzer pointed out that his state is, according to him, entirely white and native American. He's not far off. According to the last census, minorities only make up 8.2 percent of the state, most of them American Indians. 

Schweitzer claims this will be Montana's "walk from Selma to Montgomery" over the next 15 years. 

The governor's comments bring to mind remarks from the late Pennsylvania Rep. John Murtha . Murtha took heat for telling a local newspaper "there's no question Western Pennsylvania is a racist area." 

Schweitzer's office clarified his comments saying in a statement to Fox News, "There are cultural dynamics that exist in many parts of our state and there are stereotypes that are perpetuated about our first Montanans that are unfair and untrue. Governor Schweitzer believes that we all have a lot to gain from learning about each other's cultures, traditions and histories. History has shown this time-and-time again." 

The governor's communications director, Sarah Elliott, also points out that although Montana's constitution requires the state "recognize the distinct and unique cultural heritage of the American Indians and is committed in its educational goals to the preservation of their cultural integrity," the Indian Education for All legislation wasn't passed and implemented until Schweitzer took office. 

The video of Schweitzer's speech was originally posted on liberal blog Plunderbund .
