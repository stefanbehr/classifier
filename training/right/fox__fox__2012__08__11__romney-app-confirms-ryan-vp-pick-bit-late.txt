It's official. "Mitt's choice for VP is Paul Ryan . Spread the word about America's Comeback Team #RomneyRyan2012." 

So said a push notification Saturday morning from the "Mitt's VP" smart phone app Team Romney claimed would break the news. 

But those push notices started coming hours after major networks had already confirmed Ryan was the pick. The two are appearing together Saturday morning in Norfolk, Va., to make the announcement in person. 

The app was used to create buzz surrounding Romney's pick ever since it was rolled out by the campaign on July 31. 

Earlier this week, a teaser was sent out to those who downloaded the smart phone program saying, "Enter for the chance to meet Mitt his VP!" 

That same teaser gave no insight as to when a running mate would be picked. In the end, the app was a bit behind the media.
