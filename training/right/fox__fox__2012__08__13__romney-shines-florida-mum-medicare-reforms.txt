ST. AUGUSTINE, Fla. -- Shooting for the stars, Mitt Romney made his first visit to Florida since announcing his vice presidential pick, tailoring his attacks against President Obama to the space-crazed region. 

"When President Obama was candidate Obama, he promised everyone in this country the moon, but he never got off the launch pad," Romney deadpanned to a crowd of thousands at an outdoor rally at Flagler College. 

Riding high off a successful weekend stumping with his new running mate, the presumptive GOP nominee promised America's best days were ahead, while offering his vision on job creation and fix the housing market, problems near and dear to Floridians. Florida has struggled since the housing market crashed in 2007, with the unemployment rate, at 8.6 percent, stuck above the national average. 

But on the topic of Medicare , a key issue to the millions of Americans over 65 who live in the Sunshine State, Romney was relatively mute. He criticized the president for cutting the program by $700 million, but offered little in the way of his own plan except to say "we want to make sure that we preserve and protect Medicare ." 

The Obama campaign quickly fired back, calling Romney's statement "extremely misleading and hypocritical." 

"The Ryan budget that Mitt Romney promised to sign into law includes the very same savings that Romney attacked today," Obama campaign spokesperson Lis Smith said in a statement. "The Romney-Ryan budget would actually end Medicare as we know it by pushing seniors into the private market and raising their health care costs by thousands of dollars per year." 

As chairman of the House Budget Committee, Ryan unveiled a plan to balance the budget that would slash entitlements and transform the government-sponsored health care plan by offering government payments to either purchase private insurance or stay in the current system. 

Senior adviser Kevin Madden, in a briefing with reporters Sunday, attempted to downplay the potential impact Ryan's reforms could have here. He emphasized Romney, and therefore his policies, is at the top of the ticket, not Congressman Ryan. "Governor Romney's vision for the country is something that Congressman Ryan supports," he said. 

Ryan will travel to Florida this Saturday where aides say he will address the Medicare overhaul directly.
