by Kristin Rudman 

It was time to put the Kiss Cam chatter to rest - and Michelle Obama did just that during her latest appearance on The Tonight Show with Jay Leno . 

The first lady clarified to the comedian during Monday night's episode that she wasn't trying to avoid an intimate moment with her husband during last month's USA vs. Brazil basketball game at the Verizon Center in Washington, D.C. - the first couple appeared on the Kiss Cam, but didn't smooch. 

She simply didn't know they were supposed to kiss, Obama said. 

"The girls and I went somewhere else, we had met him at the game," the first lady said referring to her husband. "I had just walked in and sat down, and I just saw my face on the jumbotron. I'm still a little embarrassed, and I didn't see the Kiss Cam part." 

After some initial booing by fans, the Obamas would face an even tougher critic - their daughter Malia. 

"Malia came over after we got booed for not kissing, and she was just disgusted with us," the first lady said. "She said, 'why didn't you kiss'?" 

The Obamas would soon fix their courtside blunder. 

The first couple received a second attempt to display some public affection. The Kiss Cam returned later in the game, and the president and first lady conceded to the cameras. 

Michelle would later learn that she and her husband pulled off the do-over with a little help from Malia. 

"She orchestrated that second try," the first lady admitted to Leno. "After the second half we came back and she said, 'I've arranged for you to get another chance on the Kiss Cam'." 

However the Obama's eldest daughter didn't stop there. Malia had to ensure that her parents would succeed at their shot at redemption. 

The first lady added that Malia, "Came and sat with us to make sure we didn't mess it up."
