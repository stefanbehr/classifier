For years, one of the songs played to juice up the crowd when Newt Gingrich took the stage was the Rocky III song "Eye of the Tiger" -- that is until the song's co-writer Frankie Sullivan sued him in January for it. 

After a months-long dispute, the former presidential candidate Newt Gingrich has reached a "settlement in principle" with Sullivan's record label Rude Music Inc., whose federal lawsuit alleged Gingrich began illegally using the eighties rock hit without permission in 2009. Rude Music sought a court order prohibiting the former presidential candidate from playing the song as well as unspecified monetary damages over copyright infringement. 

The details of the settlement are currently in the process of being finalized and the terms are confidential. Karl Broun is the Nashville-based entertainment and intellectual property lawyer representing Gingrich in the case. Patrick Millsaps, Broun's partner at the legal firm Hall Booth Smith Slover, served as his chief of staff at the time Sullivan's lawsuit was filed. 

In March, Gingrich and his lawyers requested that the case be dismissed. Among the reasons listed in the court filing was the argument that the song's usage constituted "fair use" under federal copyright laws. 

Gingrich reluctantly bowed out of the campaign trail in May after a protracted battle to win the party nomination and continues to hold over $4 million in campaign debt.
