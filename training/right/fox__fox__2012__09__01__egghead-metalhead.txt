Many have long thought of GOP Vice Presidential Nominee Rep. Paul Ryan (R-WI) as an egghead - a fiscal whiz who understands the federal budget better than anyone in Washington. 

But after Wednesday's speech at the Republican convention, they may now think of Mitt Romney's running mate as a metalhead. 

Political conventions are all about burnishing your party's positions, shaping perceptions and framing your candidates. As Chairman of the House Budget Committee, Ryan's an economic intellectual. He knows all about the Gross Domestic Product, indexation and sovereign debt. Fiscal issues are the crux of the political debates which dominate Washington and American politics these days. But political conventions are about marketing. And when some voters perceive Mitt Romney as detached, the GOP braintrust wants to make sure you know that Paul Ryan is a down-to-earth guy who listens to hard rock. 

On Wednesday afternoon, Ryan toured the convention stage in Tampa and did a walkthrough in preparation for his speech. He didn't answer when asked if he might listen to Led Zeppelin or Rage Against the Machine to pump himself up before his remarks. But later that night GOP convention morphed into a limited version of Headbangers Ball. 

"Tonight is about Ryan, ( Condoleezza) Rice and rock and roll," wrote a senior GOP source shortly before Wednesday's session began. 

And less than a half hour into the program, Night Ranger and Damn Yankees bassist Jack Blades stalked the stage, blaring a song titled "Back in the Game." GE Smith (of Saturday Night Live fame) and his band provided backup with throbbing power chords and cymbal crashes. 

A few hours later, Smith and his crew covered Thin Lizzy's "The Boys Are Back In Town" as Ryan entered the arena for his speech. 



This opened the door for Ryan to talk about what's on his iPod. He noted that Mitt Romney is a generation older than he is - so he wasn't surprised that some of the tunes the GOP nominee dials up can be heard on "hotel elevators." By contrast, Ryan might fit in at a Monsters of Rock show, flashing "metal horns" with his pinky and index finger, Ronnie James Dio style. 

"My playlist starts with AC/DC and ends with Zeppelin," Ryan declared to the rapturous throng on the convention floor. 

All Ryan needed behind him was massive wall of Marshall amps. 

When Ryan concluded, his kids, wife and mother ran out on stage. That queued GE Smith and company to sample a few riffs of "Rock and Roll" from Led Zeppelin IV. 

"He's kind of a metal headbanger," said Rep. Sean Duffy (R-WI) after Ryan concluded. "He loves hard rock." 

Wisconsin Gov. Scott Walker (R) said Ryan's speech demonstrated his human side. 

"A lot of people may have thought he was just about the budget," said Walker. "You saw the Paul Ryan I know. He just didn't read off the TelePrompTer." 

But it's what was on the TelePrompTer which could create problems for Ryan. 

The Wisconsin Republican delivered a rousing speech. He electrified The Tampa Bay Times Forum like Eddie Van Halen playing licks from "Eruption." He modeled himself as a different kind of vice presidential nominee. One who inspired. One with a vision. He addressed the crowd in simple, Reagan-esque language. He told the faithful "we can do this" and implored them to "let's get this done." 

Ryan glossed his image Wednesday. That's what national political candidates are supposed to do at conventions. But it wasn't long until fact-checkers seized on three unforced, scripted errors which could haunt Ryan for the remainder of the campaign. 

Ryan talked about the GM plant that closed in his hometown of Janesville, WI. He made it personal when he said "a lot of guys I went to high school with worked at that GM plant." He turned President Obama's words against him when he referred to a 2008 campaign stop there where then candidate Obama said "this plant will be here for another hundred years." 

Ryan suggested that Mr. Obama didn't follow through on a campaign promise even though the plant's closure was ordered before he took office. 

Democrats have groused about the "Ryan budget" for a year-and-a-half. The Ryan budget is blueprint the vice presidential nominee engineered to guide the nation to a sound fiscal path. Democrats excoriated Ryan for his effort to convert Medicare into a voucher system. But Ryan turned the Democrats' argument around on them Wednesday by saying the president and Congressional Democrats took from Medicare to pay for the health care law. 

"The greatest threat to Medicare is Obamacare and we're going to stop it," Ryan said. "A Romney-Ryan administration will protect and strengthen Medicare." 

Of course, the jury is out on this. Ryan's plan could salvage Medicare in the end. But analysts perceive Medicare changes to be Ryan's Achilles Heel. Democrats are prepped to demonize Ryan's plan for Medicare and turn these comments against him. 

Then there is Bowles-Simpson. 

In 2010, President Obama empanelled the National Commission on Fiscal Responsibility and Reform. It's commonly called Bowles-Simpson after former Clinton White House Chief of Staff Erskine Bowles and former Senate Minority Whip Alan Simpson (R-WY). The panel required yea votes from 14 of the 18 members to ask the House and Senate to consider it. 

As a master of fiscal issues, Republicans tapped Ryan to serve on the committee. 

Bowles-Simpson ultimately scored 11 yes votes and fell short of passage. Ryan was one of the no votes to kill the plan. 

On December 2, 2010, the Congressman's office published a statement explaining his nay vote. 

"It...relies too heavily on tax increases which would stifle the very growth of prosperity that are the essential preconditions of a sustainable fiscal path," read the statement. 

But Ryan also said something else. 

"Regardless of the outcome of Friday's vote, the Fiscal Commission has been a success," said Ryan. "It has been a privilege to serve as an active participant in the Fiscal Commission this past year." 

Yet on Wednesday, the casual viewer wouldn't know that Ryan had a hand at all in killing Bowles-Simpson - or was even at the table. 

"(President Obama) created a bipartisan debt commission. They came back with an urgent report," Ryan said, never hinting he was part of the "they." Ryan said that the president thanked those on the panel, "sent them on their way and then did exactly nothing." 

Certainly Mr. Obama shares some of the blame for the exploding debt. But commentators pounced on Ryan's indifference. After all, Ryan portrayed himself as a quiescent bystander when he was personally hip-deep in the process. 

The casual viewer wouldn't detect these potential blips in Ryan's speech. But those who follow politics did. A few political insiders were stunned that Ryan and his advisers didn't make his speech airtight, inoculating the candidate and the ticket from criticism - particularly after the Congressman delivered a powerful, heartfelt speech that rocked the arena. 

Acumen is a hallmark quality for Ryan. But the characteristics around which Ryan built a reputation on Capitol Hill aren't what's important in a convention speech. Conventions are about tailoring the candidate to the image the campaign wants to portray. 

Out with the egghead. In with the metalhead. The GOP has a vice presidential candidate who can tell you what AC/DC tracks Bon Scott sings and which ones are performed by Brian Johnson. Oh, and he knows something about cutting spending, too. 

Most voters don't know much about the intricacies of the budget. But they do know rock and roll. 

And for this campaign, rock and roll is here to stay.
