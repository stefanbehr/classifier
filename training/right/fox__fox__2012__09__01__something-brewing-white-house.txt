URBANDALE, Iowa -- The citizens' campaign to demand release of the official White House beer recipe started here last month when President Obama attended the Iowa State Fair and bought a round of brews for some supporters who started shouting "Four More Beers" and let slip that he has his own special brew at 1600 Pennsylvania Avenue. 

So it was only natural that as Obama made his third trip to this same battleground in the last three weeks, the White House chose this day to reveal on its blog the recipes for White House Honey Brown Ale and a Honey Porter. 

As the blog -- titled "Ale to the Chief" -- noted, "With public excitement about White House beer fermenting such a buzz, we decided we better hop right to it." 

Online petitions had been demanding light-heartedly that the White House show some "transparency" and reveal all. 

White House officials now say the president bought a home brewing kit for the kitchen and tasked the White House kitchen team with trying to come up with a new brew. They say the White House Honey Brown Ale is the first alcohol brewed or distilled on the White House grounds. (George Washington brewed beer and distilled whiskey at Mount Vernon across the Potomac River in Virginia and Thomas Jefferson made wine down at his Monticello estate). 

On tap now is a third flavor -- Honey Blonde -- that was just added this summer. 

All three brews use honey from the first ever bee hive on the South Lawn, which is part of First Lady Michelle Obama's special garden at the White House to promote healthy eating. 

Beer, unfortunately for many, is not usually considered health food. 

And around the Internet, some critics are having a field day with the White House focus on beer. 

Sen. John Cornyn, R-Texas, took to Twitter to say sarcastically "inquiring minds want to know" about the recipe. 

In response to @JohnCornyn, @walt_gilbert Tweeted: "Any chance we'll see a recipe for economic recovery from the president anytime soon?"
