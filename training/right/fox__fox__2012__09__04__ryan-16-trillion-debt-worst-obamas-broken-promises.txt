CEDAR RAPIDS, Iowa - Moments after the national debt surpassed a record $16 trillion Tuesday, GOP vice presidential nominee Paul Ryan went on the attack. 

"Of all the broken promises from President Obama, this is probably the worst one because this debt is threatening jobs today, it is threatening prosperity today and it is guaranteeing that our children and grandchildren get a diminished future." Ryan said during a rally in Cedar Rapids, Iowa. 

"The problem is, the president keeps kicking the can down the road. No leadership on this issue," he added. 

At this historic figure, every American is now in the hole by more than $50,000 according to the Treasury Department. President Obama did promise to cut the debt in half when he took office, but it has increased by more than $5 trillion during his term. 

Still the Obama campaign is pointing the finger right back at Ryan, saying he's the one who deserves the blame. 

"Congressman Ryan's the last person to lecture on the debt and here's why: he was a rubber stamp in Congress for the policies that turned surpluses into deficits, putting two wars on the credit card, voting for a prescription drug benefit without paying for it, and fighting for tax cuts for the wealthiest Americans when they weren't asking for them," Obama campaign spokesman Danny Kanner said.
