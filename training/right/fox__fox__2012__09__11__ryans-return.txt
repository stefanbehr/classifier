Most House Republicans are enamored with GOP Vice Presidential nominee Rep. Paul Ryan (R-WI). 

But not enough to prevent a swath of those Republicans from voting differently from Ryan on a bill to avoid a government shutdown on Thursday. 

Ryan returns to Capitol Hill this week for the first time since Mitt Romney tapped the Wisconsin Republican to be his running mate. Ryan's visiting the Capitol to vote on a $1.047 trillion stopgap spending bill to forestall a government. shutdown. 

"We just miss him," mused House Majority Whip Kevin McCarthy (R-CA). "Paul was a great choice. It's not very often members know somebody personally and worked with them and worked on their issues." 

To House Republicans, "their issues" are comprised of spending cuts. Romney tapped Ryan for his ticket because cutting spending is "his issue." And yet Ryan and some House Republicans are expected to vote differently on a critical measure known as a Continuing Resolution (or CR for short) to run the government through March 27, 2013. 

"I don't like to speak for other members," said McCarthy, the top vote counter on the GOP side of the aisle. "Ryan's going to support the CR." 

But McCarthy is expecting some defections, too. Past is prologue when it comes to votes on these fiscal issues - particularly packages to keep the government open throughout this Congress. Fifty-four Republicans defected on a plan to avert a shutdown on March 15 of last year. That number spiked to 59 GOP noes on a similar measure to avoid a government shutdown on April 14, 2011. A bill to keep the government open actually failed on September 21 last year when 48 GOPers joined a massive Democratic contingent to vote nay. One-hundred-one Republicans voted no on November 17, 2011. And 86 Republicans voted against another bill to sidestep a shutdown last December 16. 

$1.047 trillion is the amount of discretionary spending mandated by the debt ceiling deal approved by Congress and signed by President Obama in August, 2011. "Discretionary" refers to the amount of money Congress can elect to spend, compared to "non-discretionary" spending for entitlements like Medicare, Medicaid and Social Security. What that means is that lawmakers control how to spend $1.047 trillion for fiscal year 2013, which begins October 1. 



There were big fights earlier this year in Washington over the $1.047 trillion limit. Democrats insisted on adhering to the $1.047 trillion level because that's what all sides agreed to in the debt ceiling pact, commonly called the Budget Control Act or BCA. Republican leaders tried to spin this figure as a "cap" as they felt pressure from conservatives and tea party lawmakers who demanded additional spending cuts. Many thought $1.047 was too high. So they pushed for deeper spending cuts. 

In fact as Chairman of the House Budget Committee, Ryan forged a spending outline of $1.028 trillion. But if Ryan votes yes on the CR, he'll be voting for $19 billion in spending above what he called for in his own spending parameter. 

This is to say nothing of a coalition of tea party loyalists who wanted spending to plummet to a mere $931 billion. 

But that's not going to happen. When announcing the general framework for a deal on the CR in July, House Speaker John Boehner (R-OH), Senate Majority Leader Harry Reid (D-NV) and President Obama all settled on the $1.047 trillion number. And that doesn't sit well with a lot of conservatives who pined for cuts to the marrow. 

"The same people who voted against the debt ceiling (bill) won't support this either," complained one Republican lawmaker who asked not to be identified. "It's the same stuff they carped about last year." 

The "carping" is because Republicans rose to power in the House last year with orders to eliminate spending. They've made some cuts. But they aren't as deep as many wanted. And they surely didn't want to raise the debt ceiling. That's why 66 Republicans abandoned their leadership to cast nay votes on the Budget Control Act and its $1.047 trillion threshold in August, 2011. The CR to keep the government humming this week features that same threshold. 

To be fair, there are some spending reductions in this bill. The Appropriations Committee advertises the "annual rate" for this CR as being $26.6 billion below last year's level." 

But does that matter when rounding up the votes? 

"I don't think so," replied Rep. Lynn Westmoreland (R-GA) when asked if he'd vote for the Continuing Resolution. Westmoreland is one of the 66 Republican noes on the BCA last year. "I was for $1.028." 

But I pointed out that if Ryan votes yea as McCarthy suggested, Westmoreland would be voting against the position of his party's vice presidential nominee - the very man who Republicans revere when it comes to spending and budgetary issues. 

"Paul can explain the budget better than anyone I know. We'll get spending down," said Westmoreland, who then noted he still wanted to "see what's in" the CR before making a final decision. 

Rep. Tom Latham (R-IA) is another one of those 66 GOP nays on the debt ceiling package. 

"I'm still trying to make that determination," said Latham when asked about his vote this week. 

Latham's just not any lawmaker either. He's what's known on Capitol Hill as a "cardinal." That means he chairs one of the 12 appropriations subcommittees which determine how the government spends its money. Latham leads the Transportation Appropriations Subcommittee. He's upset that lawmakers are having to shrink-wrap all 12 of the annual spending bills into one gigantic package just to avoid a government closure. 

"It's disturbing to me that all of the work we've done all year is for naught," said Latham. 

But Latham doesn't think there are problems if lawmakers vote against Ryan. 

"No one wants to see a big shutdown battle," Latham said. "I think there are bigger issues like the economy (that people will look at) with Ryan." 

And that's the flip side of this equation. How will GOP voters, many with ties to the tea party, feel about Ryan voting yes on a bill which conservatives could vote against? 

"CR's are too inside baseball," said Rep. Jo Ann Emerson (R-MO), another "cardinal" who chairs the Financial Services and General Government panel. 

Now, here's a unique factoid about this spending measure. $1.047 comports with the debt ceiling arrangement last summer. As the CBO tallied the cost of this package, it discovered additional spending offsets totaling around $8 billion. That means actual spending clocked in around $1.039 trillion. So to conform with the $1.047 trillion number, one interpretation of the bill means it actually spends $8 billion more than necessary. 

Of course, that flies in the face of the mantra preached by almost every Republican and certainly Paul Ryan. Cut spending. And certainly slash spending when there's an opportunity to do so. But House appropriators note that the pact was set at $1.047 trillion. So discretionary, non-emergency spending enjoys an across-the-board climb of six-tenths of one percentage point in this measure. 

"I'm not sure I'm in favor of the number," said Rep. Scott DesJarlais (R-TN) who said he was still undecided. "It's not good enough." 

DesJarlais is pushing for deeper spending cuts. But realizes this is a process. 

"My biggest problem with the Paul Ryan budget is that it wasn't tough enough," said DesJarlais. 

So at this point, no one knows how many GOP noes this package might get. But Republicans have had to rely on Democrats to vote yes to help keep the government in operation. 

"We'll have to wait and see," said Rep. Norm Dicks (D-WA), the top Democrat on the Appropriations Committee. "It could be exciting." 

Expect analysts to scrutinize this vote more than others because lawmakers risk voting against Paul Ryan. In other words, how can those lawmakers support Ryan as Romney's torch bearer and master of all things fiscal - yet deviate from the Wisconsin Republican on a measure as critical as keeping the government open? 

"I see our members very excited about Paul," said Kevin McCarthy. 

But not all are excited to cast their votes the same as Ryan on Thursday.
