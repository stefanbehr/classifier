DANVILLE, Va. -- Campaigning in the crucial battleground state of Virginia Wednesday, GOP vice presidential hopeful Paul Ryan was quick to pounce on a 1998 audio clip in which then State Senator Barack Obama said he "actually believe[s] in redistribution." 

" Mitt Romney and I are not running to redistribute the wealth, Mitt Romney and I are running to help Americans create wealth," Ryan said during a rally at the Piedmont Precision Machine Company in Danville. 

"Efforts that promote hard work and personal responsibility over government dependency are what have made this economy the envy of the world," he added. 

That 1998 recording -- leaked to the press Tuesday -- comes from a conference held at Loyola University. 

The Romney-Ryan team is hoping it will help get them back on the offensive after backlash from a seperate leaked video showing Mitt Romney saying 47 percent of Americans who support President Obama are "dependent on the government" and "believe they are victims." 

Back in Danville, Ryan made a point that achievement in America ought to be rewarded, not "resented." "We take pride in one another's success. Our job is not to fight over a shrinking pie in redistributed slices, our job as leaders is to grow the pie so that everybody has a better shot at the American dream, and everybody can pick themselves up," he said. 

Team Obama responded, trying to turn the tables. 

"Congressman Ryan talked a lot about redistribution today, which is exactly what he and Mitt Romney are proposing to do if elected," Obama spokesman Danny Kanner said. 

"While President Obama cut taxes for the typical middle class family by $3,600 over his first term, the Romney-Ryan plan would actually raise taxes on the middle class by cutting deductions like those for mortgage interest and charitable contributions in order to cut taxes for millionaires and billionaires."
