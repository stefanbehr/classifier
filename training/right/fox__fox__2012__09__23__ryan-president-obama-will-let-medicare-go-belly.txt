ORLANDO, Fla. -- President Obama will let Medicare go belly up. 

That is the warning GOP vice presidential nominee Paul Ryan delivered to Florida's seniors Saturday -- a voting bloc that could determine not only the outcome of the Sunshine State but also the election. 

" Mitt Romney and I have looked at the issue, worked with Democrats, come up with the best ideas from both parties for a plan to save Medicare," Ryan said during a town hall meeting in Orlando. "President Obama hasn't done a thing. President Obama is obviously complicit with this program going bankrupt." 

It's the latest accusation in a perpetual back-and-forth on the issue. For weeks the Romney-Ryan team has been arguing that Obama "robbed" Medicare of $716 billion to pay for ObamaCare. 

The Obama campaign says that's false and counters by claiming the Republican ticket is proposing a "voucher" program that would force seniors to shell out $6,400 a year. 

Ryan outright dismissed the allegation. 

"You'll hear the word voucher thrown around. The president likes to say it a lot of end Medicare as we know it.' That was rated the lie of the year last year," Ryan told the Orlando crowd. "Voucher is go to your mailbox, here's a check, go buy something. Nobody's proposing that. What we're saying for younger Americans when they become Medicare eligible -- you have a list of guaranteed coverage options to choose from. You can't be denied. You can choose from a list of comprehensive plans including traditional Medicare." 

Besides speaking in front of a ticking debt clock, Ryan also rolled out some new stagecraft here, using a PowerPoint presentation to drive home the message that the rising national debt is crushing America and President Obama has no plan to fix the problem. 

"By the time my three kids are around my age the debt is going to be two-and-a-half times the size of our economy," Ryan said as he pointed to a slide. "The reason we just end this chart there is because the Congressional Budget Office tells us the economy effectively crashes at that point," he continued. "We have a president who has literally not only done nothing but he's made it worse." 

With the national debt surpassing $16 trillion, it's now estimated the average American household is in the hole by more than $140,000. That's roughly a 50 percent increase since President Obama took office, but Democrats say Republicans are just as responsible for the ongoing spending binge in Washington. 

With Florida being one of the biggest prizes in the upcoming election, Ryan used his visit to Orlando to open up another line of attack, blasting President Obama for "dismantling" America's space program. 

"Today, if we want to send an astronaut to the space station we have to pay the Russian to take him there. China may someday be looking down on us from the moon. That's unacceptable," Ryan said. "Mitt Romney and I believe America must lead in space. Mitt Romney and I believe we need a mission for NASA, a mission for a space program, and we also believe that this is an integral part of our national security." 

The Obama campaign says Ryan should get his facts straight. 

"It's probably time for Romney to have a talk with Paul Ryan. Congressman Ryan repeatedly voted against NASA funding. And the Romney-Ryan budget's cuts -- if applied across the board -- would cut funding for space exploration programs by 19 percent," said Obama campaign spokesman Danny Kanner.
