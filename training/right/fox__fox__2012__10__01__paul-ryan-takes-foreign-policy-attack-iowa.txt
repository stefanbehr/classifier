As Mitt Romney renews his effort to paint the Obama administration as weak on foreign policy, running mate Paul Ryan brought that message to Iowa, where on Monday he embarked on a two-day tour through the Hawkeye State. 

"If you turn on the TV today, you can see that the Obama foreign policy is unraveling before our eyes," Ryan told the crowd gathered at Loras College, his grandfather's alma mater. 

Calling the death of four Americans in Benghazi "not just an isolated incident," Ryan said the terrorist attack was "part of a bigger story of the unraveling of this agenda all over the world." 

"We've distanced our ally Israel, we are not advancing our interests in the Middle East ," he said. 

But while Ryan was scornful of the administration's foreign policy weaknesses, he has declined to join the calls led by House Homeland Security Chairman Peter King for U.N. ambassador Susan Rice to resign. Rice has been accused of deliberately mischaracterizing the deadly Benghazi attack as a spontaneous response to a YouTube video rather than an organized terrorist assault. 

"We don't know all the facts about what that woman knew at the time she made those statements," he told Laura Ingrahm on her radio show. 

Ryan will continue barnstorming the Southeast region of Iowa with his family Tuesday, and in Dubuque he waxed nostalgically about his local ties to the region. 

"We wanted to start here in Loras because this is where my grandfather went to college -- Duhawks! " he said in honor of the college mascot. The audience clapped in delight. 

"On the way over here, I had this song ringing in my mind, a song I grew up hearing all the time in southern Wisconsin: Dubuque, Dubuque, da da da da da da da Dubuque, Dubuque.' I know you have that thing in your mind."
