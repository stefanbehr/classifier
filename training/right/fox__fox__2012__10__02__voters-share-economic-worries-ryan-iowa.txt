Burlington, Ia. - Vice President Joe Biden routinely mocks Paul Ryan for his concerns that the federal government is promoting a "culture of dependency," but in Iowa Tuesday, such worries factored prominently into questions the candidate fielded outside the Clinton County Courthouse. 

A woman, who said her husband owns a small business, fretted over former young employees who have chosen unemployment benefits over work. "If they're unemployed, they're not paying into social security ," she said. 

Ryan responded by echoing her sentiments, "If you have youth unemployment in countries like Greece and Spain who are practicing the same kind of failed economics that our president is pushing us toward...you're losing a great generation." 

"Is there any way possible that this 47 percent can pay a nominal fee or something so they feel that they have small ownership in the government, maybe they won't take all the handouts so readily?" another member of the crowd asked, referring to nearly half of Americans who don't pay federal income tax. 

"I got an idea. Let's help them get jobs so they can get a good paycheck so then they're good taxpayers," Ryan replied. "This is the point that Mitt and I have been trying to make, and sometimes the point doesn't get made the right way. We don't want a stagnant economy that fosters dependency." 

The notion of getting people back to work and growing the size of the nation's economic pie is a key aspect of the Romney-Ryan pledge, one that the campaign points to when critics argue their promises to cut tax rates across the board by 20 percent would plunge the country further into debt. For the two-day swing through the Hawkeye State, Paul Ryan's campaign bus rattled through cities near the Iowa-Illinois state line emblazoned with the campaign slogan "More Jobs, More Take-Home Pay." 

So when a woman in the crowd challenged Ryan to provide more specifics about the GOP ticket's proposed reforms, citing his Fox News Sunday interview where he declined to go into the math of their proposed tax cuts -- "Why aren't you more specific?" she pressed -- the candidate ticked through the campaign's 5-point plan to revive the economy. 

"The problem is, it just took me about five minutes to go into all of this with you and when you are on a 30-second TV show, you can't do it as much," he said. 

Ryan did not -- to the glee of opponents -- detail a numerical breakdown, but he did say that tax reform would involve closing loopholes for the wealthiest so "important preferences for middle class taxpayers like, you know, charitable donations, or buying a home, or healthcare" would be preserved. 

Such is the delicate tone Ryan is trying to strike on the campaign trail, somewhere between issuing dire warnings about irresponsible federal policies and delivering sincere vows to protect benefits middle class Americans rely on. 

The voice of a veteran, who said he served in Grenada and Iran, audibly trembled when he told the vice presidential candidate, "I live off of social security all of $1,000 a month, and I cannot understand why this country, who I gave my life for, feels that they should tax me twice for social security." 

"You know IA'm glad you brought that up, sir," Ryan responded, telling the audience he's "voted consistently to repeal that tax because, you know what, you already paid for this. It's your benefit." 

He accused the Democratic ticket of bearing responsibility for the tax on social security benefits. " Joe Biden voted for this tax, President Obama voted a few times to keep this tax," Ryan said. 

"But here's the other point, if we donA't work to prevent Social Security and Medicare from going bankrupt then current seniors get hurt," he continued, returning to his signature reform issues, and difficult ones to solve, at that.
