By Lucas Y. Tomlinson 

Representative Charles Rangel (D-NY), the third longest tenured member of the House of Representatives, told Fox News Thursday he was disappointed with President Obama's performance in the first of three nationally televised presidential debates. "I don't think the president took him very seriously--and he should have," Rangel said. "For the large part the president just looked bored and wanted to get the darn thing over with." 

Rangel added that Obama's performance in Denver on Wednesday night needed to be more aggressive and predicted, "You can bet your life that when Michelle was done with him last night and this morning, Obama will not sleep at the wheel again." 

Rangel told viewers that even northern Manhattan's solidly loyal Democratic supporters were left scratching their heads after President Obama's first debate with his Republican challenger, Gov. Mitt Romney . 

Rangel said he heard about if from residents as he left home Thursday morning. "Congressman Charlie," Rangel imitated. "What happened last night to the president?" 

Rangel credits low expectations for Romney and high expectations for the president as two possible reasons why many pundits believed Romney emerged victorious in the first debate. Congressman Rangel offered an explanation as to why the president appeared so flat in his opening battle against his Republican challenger, "I don't think the president took him very seriously--and he should have. For the large part the president just looked bored and wanted to get the darn thing over with." 

When Rangel was asked how else Obama can improve for next time, Rangel offered three tips: 

"Look up, wake up, be involved."
