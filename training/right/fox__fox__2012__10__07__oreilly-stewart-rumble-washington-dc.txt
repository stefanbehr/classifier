WASHINGTON - FOX News Channel's Bill O'Reilly and "The Daily Show's" Jon Stewart faced off in a spirited and humorous mock debate Saturday night. 

The aptly titled "Rumble in the Air Conditioned Auditorium" took place in front of a sold out crowd of 1,500 on the campus of The George Washington University. It was there the two sparred on a wide range of topics including the economy, Middle East , and entitlement reform. 

The debate began with O'Reilly using visual aids to explain how he believes 20 percent of the country is made up of "slackers" and the country's debt is out of control. 

However, it quickly veered to the humorous side when, before his rebuttal, the 5-foot-7 Stewart used an elevated platform to stand eye-to-eye with the 6-foot-4 O'Reilly. 

Stewart responded, saying the country is facing a debt crisis like we've never seen before and the United States is "weeks from becoming a failed state or worse Greece ." 

"To solve it is to kill Big Bird," said Stewart. 

The comedian was referring to Republican Presidential Nominee Mitt Romney's comments at Wednesday night's first presidential debate - when the former Massachusetts governor said he liked Sesame Street's Big Bird, but would end government funding for the Corporation of Public Broadcasting. 

Stewart also accused former President George W. Bush of taking a financial surplus under the Clinton Administration and turning it into deficit after two wars and a tax cut. O'Reilly said it shouldn't matter what Bush did - "the job now is to get the debt down and we need to cut stuff." 

"We are people who want free things," Stewart said, explaining that the United States was an entitlement nation. 

O'Reilly rebutted that President Obama's policies have made it easier to become more dependent on the government. He said more people are applying for food stamps and welfare because the mindset is if they ask, the government will give it to them. 

Both men surprisingly agreed on some issues, including ending government subsidies for oil companies and taking a tougher stance on Iran and its pursuit of nuclear weapons. 

The "signal he [President Obama] sends to the world is hey, let's have a conversation," O'Reilly remarked. He suggested Obama go on a double date with Israeli Prime Minister Benjamin Netanyahu to send a message to Iran. 

O'Reilly and Stewart also agreed the U.S. mishandled the situation on the release of information regarding the attack on the U.S. consulate in Benghazi, Libya , which led to death of U.S. Ambassador Chris Stevens and three other Americans. 

One of the funniest parts of the night was when O'Reilly was asked during the question-and-answer session who he would like to see as president. 

"I'd have to say Clint Eastwood ," O'Reilly said. "Well why don't we ask him," Stewart replied while getting out of his chair and pretending to berate it, re-enacting Eastwood's speech at the Republican National Convention. 

At a press conference afterwards, O'Reilly and Stewart were asked if they had any advice for the two presidential candidates. O'Reilly said Romney and Obama shouldn't have been watching this [their debate], and Stewart said it was "strictly entertainment" and he and O'Reilly "shouldn't be giving advice." 

O'Reilly and Stewart have squared off many times before in appearances on each other's shows, but this is the first time they have debated each other on stage. 

Half of the net-profits from the debate will be donated to charity. 

The event was planned to be streamed online, but the servers crashed, causing organizers to issue a statement on its Facebook page apologizing for the inconvenience and letting people know the debate would soon be made available on-demand. 

There was no winner or loser in this debate as both men were presented with championship belts at the end of the 90-minute program. But when asked in the press conference who won, Stewart remarked, "I think America won."
