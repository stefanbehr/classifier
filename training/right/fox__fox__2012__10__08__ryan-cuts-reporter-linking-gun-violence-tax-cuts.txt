ROCHESTER, Mich. - Paul Ryan abruptly ended an interview with an ABC affiliate in Flint after the reporter tried to grill the vice presidential candidate on gun violence and tax cuts. 

"Does the country have a gun problem?" WJRT's Terry Camp asked in a one-on-one interview . 

"This country has a crime problem," Ryan responded. After the anchor pressed him on whether he thinks the country has a "gun problem," Ryan, a self-described deer hunter, elaborated, "If you take a look at the gun laws we have, I don't even think President Obama is proposing more gun laws. We have good, strong gun laws." 

Camp challenged him again, and Ryan said: "We have to make sure we enforce our laws. We have lots of laws that aren't being properly enforced. We need to make sure we enforce these laws. But the best thing to help prevent violent crime in the inner cities is to bring opportunity to the inner cities." He credited the work of charities, civic groups, and churches with helping to instill "good discipline, good character" in civil society. 

In an unlikely turn, the WJRT reporter followed up by asking, "And you can do all that by cutting taxes? With a big tax cut?" 

"Those are your words, not mine," Ryan responded curtly. His press secretary Michael Steel interrupted from off-camera, "Thank you very much, sir." 

As Ryan began taking off his mic, he commented, "That was kind of strange, you're trying to stuff words in people's mouths?" 

Camp replied, "Well, I don't know if it's strange." 

Ryan disagreed, "Sounds like you're trying to put answers to questions," after which the camera lens is covered in a video that was posted on the affiliate's website but was later removed. 

Campaign spokesman Brendan Buck characterized Camp's question "weird" and a "strange situation" in which the anchor "embarrassed" himself. 

"The reporter knew he was already well over the allotted time for the interview when he decided to ask a weird question relating gun violence to tax cuts," Buck said in a statement. "Ryan responded as anyone would in such a strange situation. When you do nearly 200 interviews in a couple months, eventually you're going to see a local reporter embarrass himself." 

The Obama campaign used the exchange to skewer Ryan's Michigan trip. 

Congressman Ryan brought both his temper and penchant for dishonesty to Michigan today," campaign spokesman Danny Kanner said in a news release. "After walking out of a local interview when he was pressed about Mitt Romney s tax plan, Ryan told supporters that Romney knows we need a strong auto industry, but Romney would have just 'let Detroit go bankrupt.'" 

Since being named Romney's running mate, Ryan has granted 197 total media interviews; of those, 153 were granted to television reporters and 124 to local outlets, according to the campaign. 

Ryan "seemed himself" after the interview and Camp stayed in the room as cameras filmed Ryan dialing his two-millionth voter contact call in Michigan , according to the pool report. He used his iPhone to make the call, which is the hunting color of blaze orange.
