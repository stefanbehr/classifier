Making a pitch for why his fellow Michiganders should vote for the Romney-Ryan ticket, Kid Rock tapped into the complicated feelings some voters harbor in deciding whether to re-elect President Obama. 

"I am very proud to say that we had elected our first black president; I m sorry--I m sorry he didn t do a better job, I really wish he would have I do, but the facts are the facts," the singer-songwriter said Monday in his introduction of Republican vice presidential pick Paul Ryan . 

Having appeared with Mitt Romney on the campaign trail, Kid Rock threw some more star power behind the Republican candidates as Michigan polls conducted after the first presidential debate show a tightening race. 

Kid Rock joked to the crowd of thousands gathered at Oakland University that he enjoyed Romney's face-off with President Obama so much that "I think I might throw a keg party for the Ryan-Biden debate," which is scheduled for Thursday. 

The Grammy-nominated musician admitted to the audience that it's "a little difficult to put myself in this position knowing it may alienate a few fans." 

"I really believe strongly that it s okay to disagree on politics and the direction of our country without hating one another," he said. "I mean it s no secret that I am embedded in an industry that leans very left."
