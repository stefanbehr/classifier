Under normal circumstances, Rep. Joe Walsh , R-Ill., might be scrambling with three weeks until Election Day. In the three-month span ending Sept. 30, the House freshman collected just over $250,000 for his re-election campaign. 

Walsh's opponent, Democrat Tammy Duckworth , raised almost $1.5 million in the same period and had $750,000 cash on hand heading into October and the final month of the campaign. Advantage Duckworth. 

But Walsh has gotten a nearly million-dollar boost. 

Starting Sept. 17, a Missouri-based PAC started buying up large chunks of TV ad time in Illinois to urge for Walsh's re-election and to attack Duckworth. Between TV ads and direct mail. the spending binge was $932,000, all from Now or Never PAC. 

And yet no one seemed to know anything about them, other than they liked Walsh. Republican-affiliated groups in Washington were stumped. 

"I've never heard of them," one GOP operative said. 

Calls and e-mails to the PAC's spokesman and some of its Missouri donors were not returned. 

In filings with the Federal Elections Commission, it appears Now or Never began as a group of Missouri business men interested in helping state treasurer Sarah Steelman win the Show-Me State's GOP Senate primary in August. But when Steelman finished third, Now or Never went quiet. 

But on the same day Now or Never started its TV ad buys to help Walsh, the PAC received a $1 million contribution from Americans for Limited Government. Eleven days later, another donation from ALG came in for a little less, $950,000. 

Why would ALG, a conservative group that advocates for a more limited federal government and reduced spending, go to the trouble funneling almost $2 million through a Missouri PAC instead of doing it directly themselves? 

We got this from ALG communications director Richard Manning by e-mail: "Now or Never PAC does an impressive job of fighting for free market principles, which are in alignment with Americans for Limited Government, and we are proud to support it." 

But according to its most recent FEC filing, after Now or Never PAC paid some bills left over from its Missouri Senate campaign efforts, it submitted new paperwork to form a Super PAC and has put its financial muscle behind one candidate, Joe Walsh.
