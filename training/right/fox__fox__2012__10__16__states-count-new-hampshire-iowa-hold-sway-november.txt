Voters across the country are hearing a lot about big swing states like Ohio and Florida. But when the votes are counted, will it be the smallest numbers that add up to a big win? 

The following is the second in a two-part series on swing states with a relatively small number of electoral votes. 

NEW HAMPSHIRE 

New Hampshire and Iowa both enjoy center stage in presidential politics, establishing early frontrunners, but the spotlight fades after caucus and primary votes are cast. This year is different. These critical first-voting states are political battlegrounds, drawing further attention from both sides in an intensely competitive battle for the White House. 

Though the Granite State has just four electoral votes, professor Andy Smith, the director of the University of New Hampshire Survey Center, says they are critical. 

"Those four votes count," he said. "Each electoral vote counts. If Al Gore had spent a little bit more time here, he could have taken some of the votes away that Ralph Nader got and would have been president in 2000." 

At the Red Arrow Diner, in Manchester, a popular stop on the campaign trail for would-be presidents, customers comfortably talk politics over lunch. 

"The economy, jobs," said Frank Devine, of Merrimack, when asked what tops his election year concerns. "I don't think we can take another four years of the direction we're headed in." 

"I'm voting for Romney because we need a change," Devine said. "He's a successful business man. He knows how to make businesses grow so why can't he make the economy grow?" 

From the diner to Manchester's busy main drag, New Hampshire's savvy voters are paying attention. Some are still weighing their choice. Others have made up their minds. 

"I'm voting for Barack Obama, and I'm voting for him because I feel that overall he best represents my feelings in terms of the economy, foreign relations and education," said Betsy Holmes, of New Boston. 

"I'm probably still undecided," said Brian Urick, of Concord. "To be honest, I'm not really thrilled with either candidate. They don't strike me as followers-through on commitments based on track record. So it will probably be a gut feeling when I go into the booth." 

Said Smith: "We're seeing that Republicans are still very angry. They're more motivated than Democrats. The key for the Obama campaign is whether or not they can get the more marginal Democrats, the less partisan Democrats to show up to vote on election day. So far it looks like they've got a slight advantage, but that motivation could change over the last month." 

The numbers are shifting. According to the latest WMUR Granite State poll (conducted by the UNH Survey Center and released on Oct. 9,) the race is getting closer. President Obama continues to lead Mitt Romney but the margin is shrinking. Now 6 percentage points divide the candidates compared to 15 percentage points the week prior. 

www.unh.edu/survey-center 

IOWA 

This once reliably red state has six electoral votes. In five of the last six elections, voters here have supported the Democratic nominee, including Obama in 2008. 

While Iowa has more social conservatives, many here share Granite Stater's frugality. With an economy dependent on agriculture, they know that it's good to save for a rainy day, something so many prayed for during the 2012 drought. 

"It's been extremely stressful because we didn't know when the next rain could come," said Larry Sailer. He considers himself to be an average Iowa farmer, growing roughly 400 acres of corn and soy beans. He says Mother Nature is just half the battle. Shifting regulations and unpredictability create greater challenges. 

"I think everybody's frustrated with Washington right now," said Sailer, who believes new leadership is needed. "I think we've had so much gridlock from the last administration (that) I think we need a change. And I think Mitt Romney's got a pretty good business sense. I think he knows what he's doing and I think he can do it." 

A common thread ties together the participants at the American Quilters Society Quilt Show in Des Moines, but their political leanings make-up a vibrant patchwork of opinions. 

"I'm voting for President Obama," said Mary Shotwell, of Des Moines. "I'm really concerned about a couple of things, one having to do with what happens for women if President Romney is elected. Women and health care especially. I also have a belief that Obama is much more concerned about the middle and lower classes. And those are the folks that really need some assistance at this point. More jobs. Better jobs. Better health care and better opportunities and certainly better education. And (I) have haven't heard anything yet from candidate Romney that would lead me to believe he was that concerned about them." 

Mark Caraher, who works for an Iowa-based manufacturer of quilting machine, ran a popular booth at the event. 

"I most likely will be voting for Mitt Romney," he said. "I like some of the standards he stands for. Some of the things on the Democratic platform as far as abortion and some of these things, I don't really personally agree with. So I like the stance of some of the Republican platforms. I think Mitt Romney will do a great job trying to rebound the economy from where we're at." 

Fox News' Andrew Fone contributed to this report.
