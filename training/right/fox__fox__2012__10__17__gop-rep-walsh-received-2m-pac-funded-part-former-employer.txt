Illinois Republican Rep. Joe Walsh has gotten re-election help from a political action committee that received roughly $2 million from the congressman's former employer -- Americans for Limited Government. 

According to papers filed with the Federal Election Commission , the group on Sept. 17 gave the first of two contributions totaling $1.95 million to Missouri-based Now or Never PAC. The same day, Now or Never started buying large blocks of TV ad time for commercials praising Walsh and attacking Democratic challenger Tammy Duckworth . 

Walsh's campaign says it was unaware of the situation and did not seek assistance in a tough battle with Duckworth. 

Coordinating or communicating with third party organizations is illegal for political campaigns. 

"We just learned about this yesterday," said Walsh campaign spokesman Justin Roth. "Congressman Walsh ended his association with them 10 years ago, and the campaign has had no contact with any (PAC), pro-Joe or anti-Joe." 

In addition, the Walsh campaign tells Fox News the congressman left the group in about 2002 and has since had no contact. 

The conservative, Washington-area non-profit group pushes for reducing the size and scope of the federal government. 

Now or Never also produced and distributed direct mail to voters in the 8th congressional district of Illinois. But mostly, the PAC was involved in television ads in support of Walsh. The total spent to help Walsh was $1.8 million, accounting for most of the money Now or Never received from ALG. 

ALG also is denying any effort to assist Walsh. 

"It's not like there's a special relationship," said ALG communications director Richard Manning. "We have no relationship with the guy." 

Manning says he was aware Walsh was an early ALG employee but unaware of what role he played. 

Regarding the nearly $2 million in contributions, Manning said ALG was approached by Now or Never PAC. 

"We liked their promotion of limited government principles," he explained. 

He also insists ALG was not made aware of Now or Never plans to back Walsh. 

Duckworth has gotten help from House Majority PAC. The political action committee, fueled in large part by labor unions, has been running a clever, three-for-one attack ad on the Chicago airwaves. 

The spot features footage of Walsh talking in a town hall setting and portrays him as a loud-talking, House member who voted against mortgage relief for distressed borrowers and for tax breaks for millionaires. 

The ad also charges that fellow Illinois Republicans Reps. Judy Biggert, and Bob Dold voted with Walsh on these issues. The spot is part of a $2.4 million ad buy.
