NAPLES, Fla. -- Vice President Biden lit into Paul Ryan with a violent analogy during his campaign stop Thursday in Las Vegas. 

"Ryan has written a book called 'The Young Guns' with two other members of the House ... Republican leaders in the House," the vice president said. "You had, unfortunately, the bullets are aimed at you." 

The comment drew an immediate retort from the Romney campaign, which earlier in the day had lamented the "negative" tone in the closing weeks of the race. 

Ryan spokesman Brendan Buck called Biden's "over-the-top" rhetoric "disappointing but not surprising." 

"In the absence of a vision or plan to move the country forward, the vice president is left only with ugly political attacks beneath the dignity of the office he occupies," Buck said in a statement. 

Critics pointed to Biden's assertion, "I always say what I mean," during the vice presidential debate as a backdrop to Thursday's attack. 

Earlier in the day, Paul Ryan had blasted President Obama for trying to "disqualify his opponent with a sea of negativity," casting the attacks as a ploy to "distract" voters from learning about their actual agenda. 

"President Obama is not telling you what his second term plan would be," Ryan told the crowd of over 2,000 people, gathered in the town square of Ocala, Fla. "He's not saying that he's offering anything new. All he's offering is four more years of the same." 

Biden's comment marked an escalation in the rhetoric. Obama supporters -- as well as the president himself -- have also been tweaking Romney for his comment during Tuesday's debate that he sought out "binders full of women" candidates while filling out his cabinet as Massachusetts governor. In his first stop in Florida for a two-day swing, Ryan appeared to subtly rebut critics by fielding five of his six questions from women in the audience. 

"The best way to help women, the best way to help people that are out of luck, the best way to help the 5.5 million people who the government doesn't even measure in unemployment statistics because they just stopped trying for work, the best way to help the 23 million people struggling to find a job is to get this economy growing," Ryan said. 

Stoppping by a fundraiser in Naples, which raised $1.2 million dollars, Paul Ryan warned the crowd the Democratic ticket has gotten "even more desperate" because the polls are "going in the wrong direction for them." 

"Now it's a war on women, tomorrow it's going to be a war on left-landed Irishmen or something like that," Ryan joked, noting he's "right-handed by the way." 

He continued, "They're trying to appeal to people's sense of envy, anxiety, and fear. Hope and change has become attack and blame. We're not going to fall for it. I don't think the country is going to fall for it."
