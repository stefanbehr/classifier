COLORADO SPRINGS, Colo. - On the eve of the third and final presidential debate which will focus on foreign policy, GOP vice presidential hopeful Paul Ryan may have given a preview of what's to come from his running-mate. 

During a campaign rally in the battleground state of Colorado, Ryan berated President Obama for getting caught on a live microphone just before a global nuclear summit back in March, when he assured former Russian President Dmitry Medvedev that he needed "space" and would have "more flexibility" to deal with contentious issues like missile defense after the presidential election. 

"When you see your president whispering to the president of Russia, when he thinks no one is listening ... it begs the question: how much more does he want to give away," Ryan said. 

The Wisconsin congressman then shifted focus to the military, blaming President Obama for the massive looming cuts to the defense budget set to kick in at the end of the year. 

"Of all the areas where Mitt Romney and I are very different than Barack Obama and Joe Biden , we are not going to gut our military," Ryan pledged. "We need the strongest military that the world has ever seen." 

Ryan and Romney have proposed increasing military spending and have also repeatedly spoken out against the so-called "sequestration" plan which would slash hundreds of billions of defense dollars if a deal to lower the deficit cannot be forged between Congress and the White House . 

Ryan voted for this, though he claims he never supported the military cuts. He does however claim that President Obama has proven himself to be inept as a leader and as a commander in chief. Accusations he repeated here Sunday night. 

"The president himself acknowledged on TV a few weeks ago that he cant change Washington from the inside," Ryan said. "Isn't that why we have presidents? If he can't change Washington, then I say we change presidents," he continued. 

"If he can't tackle our problems like a debt crisis, if he can't get our economy growing, if he can't honor the men and women who put the uniform on by giving us a strong military, then I say we change presidents." 

Ryan's speech Sunday was part of a two-day campaign swing through Colorado, where polls show Governor Romney and President Obama locked in a virtual dead heat. 

Millions of Americans however will soon be turning their attention to another battleground state; Florida, where President Obama and Governor Romney will duke it out in their last face-to-face meeting Monday night. 

Their highly anticipated foreign policy debate will be divided into the following segments: America's role in the world, hotbed regions like Afghanistan and Pakistan, increasing tensions between Iran and Israel, the changing Middle East, and the rise of China.
