128th Fundraiser 



As we all recover from the weekend's WHCD festivities, President Obama will hold his 127 th  and 128 th  fundraisers of the reelection campaign with Bill Clinton tonight. The White House claims the president is focused on governing but between fundraising and swing state travel the spin is getting old. Can't wait to hear what new line Carney has in the works for tomorrow's WH briefing... 



Today, Obama Will Attend Two Fundraisers With Former President Bill Clinton.  "Former President Bill Clinton will join President Barack Obama on Sunday to raise money for the Democrat s re-election campaign, the first time the two U.S. political heavyweights have campaigned together in 2012. ... The two men will appear together at a fundraising reception and then a dinner, both hosted by Terry McAuliffe, a close friend of the Clintons." ("Bill Clinton, Obama Team Up For Re-Election Fundraisers,"  Reuters , 4/28/12) These Events Will Bring The Numbers Of Fundraisers Obama Has Attended Since Announcing His   more
