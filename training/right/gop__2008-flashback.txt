2008 Flashback 



REALLY? 

DAVID AXELROD, on NBC's live Super Tuesday special:  "It's hard to unite a party when 80 or 90 percent of the messages you send are negative messages about your opponent. I think he's got some serious problems." 

BRIAN WILLIAMS:  " Do you have any  real  doubts about the GOP's ability to unite behind a single nominee?" 

AXELROD:  "I've never actually seen a race quite like this. ... 

FLASHBACK 



CLAIMS THAT LONG OPPOSING PARTY PRIMARY WOULD AID JOHN MCCAIN IN 2008 

Headlines: 

The New York Times : "A Present For McCain As The Other Side Fights."  "At the moment, Republicans can savor protracted warfare between Senators Hillary Rodham Clinton and Barack Obama. As the Democratic rivals trade attacks, Mr. McCain, already the presumptive Republican nominee, has crept ahead of both in national polls." (John Harwood, "A Present For McCain As The Other Side Fights,"  The New York Times , 3/24/08) 

AFP : "Democratic Dead-Heat 'Not Good News' Says Dean"  ("Democratic Dead-Heat Not Good News   more
