An Open Letter to Jake Tapper (From the DNC) 





Dear Jake, 

Regarding the DNC's recent ad, you  wrote the following  about our claim that Mitt Romney previously supported the stimulus: "In other words: he didn't. Romney saying he supports the concept of stimulus is not the same thing as saying he supports President Obama's stimulus bill. The DNC got greedy here. The use of that Romney quote is deceptive and false." If only you had dug a little deeper, scratched beyond the surface--or just opened your eyes--you would have noticed that Mitt Romney is, in fact, such a serial flip-flopper that it is often hard to pin him down on any issue--but on this one, we did, and you are wrong. 

If you had only done your due diligence, you might have learned that Mitt Romney expressed his support for the Recovery Act on more than the one occasion. In fact, one week after the President-elect released his plan, Mitt Romney had the following to say on Morning Joe: 

"The president s plan for economic recovery, including a stimulus bill which includes a very healthy dose of tax reductions, is something which I think showed a willingness to actually listen to some of his own economic advisers that have pointed out in their research that tax reductions have a bigger economic stimulus impact than spending money on infrastructure does. That's encouraging." 

We know you are always in such a hurry to hit "post" that you rarely have time to finish your fact checking, but this is not even a close call. Jake, if you really believe that Mitt Romney hasn't been trying to mislead voters on this and other issues--well, you just haven't been paying attention. 

But, don't take our word for it; we'll let the actual events and timeline speak for itself: 

ON JANUARY 9, 2009, PRESIDENT-ELECT OBAMA RELEASED HIS AMERICAN RECOVERY AND REINVESTMENT PLAN THAT MIRRORED THE FINAL RECOVERY ACT BILL 

January 9, 2009: President-Elect Obama Introduced The "American Recovery And Reinvestment Plan" That Assumed A Package "Slightly Over" $775 Billion That Included "Substantial Investments In Infrastructure, Education, Health, And Energy" As Well As "Increases In Food Stamps And Expansions Of Unemployment Insurance" And "State Fiscal Relief" And The "Making Work Pay" Tax Cut And "Business Investment Incentives."  "Estimating the aggregate employment effects of the proposed American Recovery and Reinvestment Plan involves several steps. The first is to specify a prototypical package. We have assumed a package just slightly over the $775 billion currently under discussion. It includes a range of measures, all of which have been discussed publicly. Among the key components are: * Substantial investments in infrastructure, education, health, and energy. * Temporary programs to protect the most vulnerable from the deep recession, including increases in food stamps and expansions of unemployment insurance. * State fiscal relief designed to alleviate cuts in healthcare, education, and prevent increases in state and local taxes. * Business investment incentives. * A middle-class tax cut along the lines of the Making Work Pay tax cut that the President-elect proposed during the campaign." [The Job Impact Of The American Recovery And Reinvestment Plan, Presidential Transition,  1/09/09 ] 

ONE WEEK LATER: ROMNEY PRAISED THE AMERICAN RECOVERY AND REINVESTMENT PLAN AS "ENCOURAGING," SAYING IT "INCLUDES A HEALTHY DOSE OF TAX REDUCTIONS" 

JANUARY 16, 2009: Romney Praised Obama's Proposed Stimulus As "Encouraging" For Having A "Healthy Dose Of Tax Reductions."  Romney said, "Well, Mika, I don t think I m going to top what we heard from Leader Boehner, but I can tell you that, in my view, the president s willingness, his rhetoric to say, look, he s going to reach across the aisle, he wants to seek the input from members of our party--that s a very encouraging sign. The president s plan for economic recovery, including a stimulus bill which includes a very healthy dose of tax reductions, is something which I think showed a willingness to actually listen to some of his own economic advisers that have pointed out in their research that tax reductions have a bigger economic stimulus impact than spending money on infrastructure does. That's encouraging." [Morning Joe, 1/16/09] 

FEBRUARY 17, 2009: PRESIDENT OBAMA SIGNED THE AMERICAN RECOVERY AND REINVESTMENT ACT  

February 17, 2009: President Obama Signed The American Recovery And Reinvestment Act, Which Was Estimated To Cost $787 Billion And Included Investments In Infrastructure, Education, Health, And Energy, As Well As Increases In Unemployment Insurance, Support For Low-Income Families, State Fiscal Relief, And Tax Relief For Working Families And Businesses.  "On Feb. 13, 2009, Congress passed the American Recovery and Reinvestment Act of 2009 at the urging of President Obama, who signed it into law four days later. A direct response to the economic crisis, the Recovery Act has three immediate goals...The Recovery Act intended to achieve those goals by: Providing $288 billion in tax cuts and benefits for millions of working families and businesses. Increasing federal funds for entitlement programs, such as extending unemployment benefits, by $224 billion. Making $275 billion available for federal contracts, grants and loans...In addition to offering financial aid directly to local school districts, expanding the Child Tax Credit, and underwriting the computerization health records, the Recovery Act is targeted at infrastructure development and enhancement. For instance, the Act provides for the weatherizing of 75 percent of federal buildings and more than one million private homes. Construction and repair of roads and bridges as well as scientific research and the expansion of broadband and wireless service are also projects that the Recovery Act is funding. While many of Recovery Act projects are focused on jumpstarting the economy, others, especially those involving infrastructure improvements, are expected to contribute to economic growth for many years." [Recovery.gov, accessed 11/29/2011 ]  

###
