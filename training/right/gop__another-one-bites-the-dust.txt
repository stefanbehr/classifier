Another One Bites The Dust 



Less than a week after Obama's "independent review" of his green energy investments, yet another solar technology firm has filed for bankruptcy. Energy Conversion Devices Inc. which received a $13 million taxpayer-funded stimulus tax credit, filed for Chapter 11 protection Tuesday. 

Energy Conversions Devices Inc, A Michigan Based Solar-Technology Company Filed For Bankruptcy Protection.  "Energy Conversion Devices Inc., a pioneering Michigan-based solar-technology company, filed for bankruptcy protection Tuesday with a plan to slash its debt and sell its business at a court-supervised auction. The Auburn Hills, Mich., company filed for Chapter 11 protection in U.S. Bankruptcy Court in Detroit after it was unable to come to terms on an out-of-court deal with its convertible bondholders, according to Michael E. Schostak, director of business development at Energy Conversion Devices." (Patrick Fitzgerald, "Energy Conversion Devices Files For Chapter 11,"  The Wall Street Journal , 2/14/12) 

Energy Conversions Devices Received A $13 Million Stimulus Tax Credit.  "ECD says it's pushing ahead with plans to use the $13 million stimulus tax credit it received to upgrade other parts of its Auburn Hills operations to produce a new, more efficient line of solar cells. When ECD announced the Department of Energy award, it said the $42 million project would create about 600 additional jobs in Michigan." (Joseph B. White, "Green Jobs That Can Be Outsourced,  The Wall Street Journal's  Washington Wire ,   8/11/10) 

The Company Listed Assets Of $318 Million And Debt Of $349 Million.  "In the company s most recent quarterly regulatory filing in November, it listed consolidated assets of $318 million and debt of $349 million. That debt includes about $263 million in convertible bonds coming due in June of next year." (Patrick Fitzgerald, "Energy Conversion Devices Files For Chapter 11,"  The Wall Street Journal , 2/14/12) 

The Company Has Lost Money For Years And Recently Its Stock Closed At $1.46 A Share.  "The business has lost money for years. In 2011 the company posted a net loss of $306.4 million on, or $6.40 a share, on revenue from product sales and licensing of $232.5 million. The previous year it lost even more, $457 million, or $10.75 a share, on revenue of $254 million. The company s stock, which trades on Nasdaq, closed at $1.46 share Monday." (Patrick Fitzgerald, "Energy Conversion Devices Files For Chapter 11,"  The Wall Street Journal , 2/14/12)
