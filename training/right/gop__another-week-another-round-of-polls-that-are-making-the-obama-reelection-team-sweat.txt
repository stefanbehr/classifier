Another Week, Another Round Of Polls That Are Making The Obama Reelection Team Sweat 



NATIONALLY, OBAMA IS SEEING WIDESPREAD DISAPPROVAL 

73 Percent Of Americans Think The Nation Is On The Wrong Track.  ( Reuters/Ipsos Poll , 1055 A, MoE 3%, 8/4-8/11) 

54 Percent Of Americans Disapprove Of Obama.  ( CNN/ORC Poll , 1008 A, MoE 4.5%, 8/5-7/11) 

Obama's Political Standing Is Weakened As Opposition Grows In Vital Swing Voting Groups.  "Obama's political standing is weaker in the aftermath of the fierce debt negotiations, especially among liberals. There's also stronger opposition than support for a second term among swing voting groups who backed him in 2008, including independents and women." (Scott Clement, "Obama Reelect Numbers Soften, Poll Syas,"  The Washington Post's  " Behind The Numbers ," 8/11/11) 

60 Percent Of Americans Disapprove Of The Way Obama Is Handling Unemployment.  ( CNN/ORC Poll , 1008 A, MoE 3%, 8/5-7/11) 68 Percent Of Independents Disapprove Of The Way Obama Is Handing Unemployment.  ( CNN/ORC Poll , 1008 A, MoE 4.5%, 8/5-7/11) 

64 Percent Of Americans Disapprove Of The Way Obama Is Handling The Economy.  ( CNN/ORC Poll , 1008 A, MoE 3%, 8/5-7/11) Only 25  Percent Of Independents Approve Of   The Way Obama Is Handling The Economy, 72 Percent Disapprove.  ( CNN/ORC Poll , 1008 A, MoE 4.5%, 8/5-7/11) 

67 Percent Of Americans Are Not Confident That Obama Is Making The Right Decisions About The Nation's Economic Future.  ( Washington Post Poll , 601A, MoE 4.5%, 8/9/11) 

65 Percent Of Americans Disapprove Of The Way Obama Is Handling The Deficit.  ( CNN/ORC Poll , 1008 A, MoE 3%, 8/5-7/11) 72 Percent Of Independents Disapprove Of The Way Obama Is Handing The Federal Budget Deficit.  ( CNN/ORC Poll , 1008 A, MoE 4.5%, 8/5-7/11) 

AND STATE-WIDE POLLS ARE NOT BRINGING OBAMA ANY RELIEF 

Obama's Poll Numbers Are Surprisingly Low In New York: 

Obama "Might Need To Start Taking A Few More Campaign Trips To New York--And Not Just To Raise Cash."  (Geoff Earle, "NY Turns Negative On Bam,"  New York Post   8/12/11) 

After Carrying New York with 63 Percent Of The Vote In 2008, Obama's Approval Rating Drops To A Record Low Of 45 Percent. " A stunning new survey gives the president a negative approval rating in the Empire State for the first time, with just 45 percent approval and 49 percent disapproval among voters, according to the latest Quinnipiac University poll. That s a sharp turnaround from June, when Obama s New York popularity was a healthy 57-38. In the 2008 presidential election, Obama carried New York with 63 percent of the vote." (Geoff Earle, "NY Turns Negative On Bam,"  New York Post   8/12/11) 

'The Debt-Ceiling Hullabaloo Devastated Obama s Numbers Even In True-Blue New York,' Said Maurice Carroll, Director Of The Quinnipiac University Polling Institute."  (Geoff Earle, "NY Turns Negative On Bam,"  New York Post   8/12/11) 

He's Also Slipping In The Hawkeye State: 

While Candidate Obama Carried Iowa With 55 Percent Of The Vote In 2008, Today 52 Percent Of Voters Disapprove Of The Job He Is Doing As President And Just 43 Percent Say He Is Doing A "Good Job."  "Iowans helped catapult President Barack Obama into the White House in 2008, but he's likely to get a cooler reception when he visits the Hawkeye State next week. Fifty-two percent of Iowa voters disapprove of the job Mr. Obama is doing as president, compared with 43% who think he's doing a good job, according to a poll taken earlier this month by The Tarrance Group, a Republican polling firm. That's a pretty steep drop since 2008, when the president won the state, 55%-44%." (Patrick O'Connor, "Iowans Cool On Obama, "  The Wall Street Journal  8/10/11) "Perhaps More Troubling For Mr. Obama: Nearly Twice As Many Independent Voters Disapprove Of His Performance In Office Than Those Who Approve Of It, 61%-32%."  (Patrick O'Connor, "Iowans Cool On Obama, "  The Wall Street Journal  8/10/11)
