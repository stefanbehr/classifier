Buh-Bye Blue Hues 





Gallup: "More States Move To GOP In 2011."  (Jeffrey Jones, "More States Move To GOP In 2011,"  Gallup , 2/2/12) 

"Democrats Have Lost Their Solid Political Party Affiliation Advantage In 18 States Since 2008, While Republicans Have Gained A Solid Advantage In 6 States."  (Jeffrey Jones, "More States Move To GOP In 2011,"  Gallup , 2/3/12) 

Since 2008, 12 States Have Shifted To Either Solidly Republican Or Leaning Republican.  "A total of 17 states were either solidly Republican or leaning Republican in their residents party affiliation in 2011, up from 10 in 2010 and 5 in 2008." (Jeffrey Jones, "More States Move To GOP In 2011,"  Gallup , 2/3/12) While Obama Has Lost 17 Solidly Or Leaning Democrat States.  "Meanwhile, 19 states including the District of Columbia showed a solid or leaning Democratic orientation, down from 23 in 2010 and 36 in 2008." (Jeffrey Jones, "More States Move To GOP In 2011,"  Gallup , 2/3/12) 

Gallup: "Clearly, President Obama Faces A Much Less Favorable Environment As He Seeks A Second Term In Office Than He Did When He Was Elected President."  (Jeffrey Jones, "More States Move To GOP In 2011,"  Gallup , 2/3/12) 

  

And No Matter How He Spins It, Obama's Approval Ratings Are Solidly Depressed 

"Overall, Obama Averaged 44% Job Approval In His Third Year In Office, Down From 47% In His Second Year."  (Jeffrey Jones, "Obama Approval Above 50% In 10 States And D.C. In 2011,"  Gallup , 1/31/12) 

National Journal:  Rocky Terrain: Obama s Electoral College Map Grows Steeper."  (Ronald Brownstein, "Rocky Terrain: Obama's Electoral College Map Grows Steeper,"  National Journal,   2/2/12) 

To Win In 2012, Obama Faces The "Daunting Prospect" Of Carrying States Where His Approval Rating Stands At 43.7 Percent Or Above.  "In sum then, Obama in 2010 could reach an Electoral College majority by carrying states where his approval rating stood at least at 46.6 percent, something that would be difficult but hardly impossible. To reach a majority based on the 2011 results, he d need to carry states where his approval stood at 43.7 percent or above. That s a much more daunting prospect." (Ronald Brownstein, "Rocky Terrain: Obama's Electoral College Map Grows Steeper,"  National Journal,   2/2/12) Obama "Favored At Least Somewhat" In States That Produce Only 215 Electoral Votes.  "In 2011, the states in which Obama s approval rating exceeds 50 percent-enough to make him a clear favorite-have a combined total of 159 Electoral College votes. His rating stands between 47 percent and 49.9 percent in states with another combined 56 Electoral College votes. That means he s favored at least somewhat in states with 215 Electoral College votes." (Ronald Brownstein, "Rocky Terrain: Obama's Electoral College Map Grows Steeper,"  National Journal,   2/2/12) 

USA Today's  David Jackson: "Obama Saw Higher 2011 Approval Ratings In Only Three States."  "Throughout the nation, Obama saw higher 2011 approval ratings in only three states: Wyoming, Connecticut and Maine. But there were declines of less than 1 percentage point in Massachusetts, Wisconsin, Minnesota, New Jersey, Arizona, West Virginia, Michigan and Georgia." (David Jackson, "Obama s Approval Rating Above 50% In Only 10 States,"  USA Today , 1/31/12) 

ABC News'  Devin Dwyer: Despite "Spin," Obama "Deeply Under Water In His Bid For A Second Term."  "No matter how much President Obama's team of re-election strategists tries to spin it, voters across the country soured on Obama in 2011, compounding a decline in his job approval ratings that have him deeply under water in his bid for a second term." (Devin Dwyer, "Obama Approval In Swing States Took Hit in 2011,"  ABC News'  Political Punch , 1/31/12) 

National Journal's  Josh Kraushaar: Numbers Suggest Obama "Campaign Has Been Doing A Lot Of Bluffing."  "President Obama s re-election team has spun multiple pathways to an electoral vote majority, but a glance at his state-by-state approval ratings throughout 2011 suggests the campaign has been doing a lot of bluffing." (Josh Kraushaar, "Obama Struggling In Battleground States,"  National Journal , 1/31/12) 

Across The Board Democrats Are Less Enthused About 2012 

Gallup: "Republicans More Likely to Be Extremely Enthusiastic About Voting Next November."  (Frank Newport, "Romney Ties Obama In Swing States; Gingrich Trails,"  Gallup,  1/30/12) Nationally, 38 Percent Of Republicans Say They Are "Extremely" Enthusiastic About The 2012 Presidential Election, Compared To Just 25 Percent Of Democrats.  (Frank Newport, "Romney Ties Obama In Swing States; Gingrich Trails,"  Gallup,  1/30/12) 

In Swing States, 35 Percent Of Republicans Say They Are "Extremely" Enthusiastic About The 2012 Presidential Election, Compared To Just 23 Percent Of Democrats.  (Frank Newport, "Romney Ties Obama In Swing States; Gingrich Trails,"  Gallup,  1/30/12)
