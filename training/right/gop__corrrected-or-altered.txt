Corrrected or Altered? 



Corrected or altered? Guess we'll just have to take the White House at their word that people "applauded" instead of laughing at the President's claims that he's created jobs. This begs the question - why doesn't the "most transparent administration in history" put videos online along with the transcript so we can see for ourselves? Guess we'll file that request right after 1) DNC events held at the White House 2) fundraising totals for DNC and Obama For America events and 3) how much the DNC reimburses the government for Obama fundraising travel. 

Updated Transcript: 

     As a consequence of that swift, decisive, and sometimes difficult period, we were able to take an economy that was shrinking by about 6 percent and create an economy that is now growing, and has grown steadily now over many consecutive quarters.   Over the last 15 months we've created over 2.1 million private sector jobs. (Laughter.)    (Applause.)*   We have an auto industry that, for the first time in a very long time is profitable, and the Big Threeautomakers actually gaining market share, and not only gaining market share, but also gaining market share in the cars of the future so that they're actually competing in compact cars and sub-compact cars and electric cars and hybrids. 



OBAMA'S WHITE HOUSE CLAIMS TO BE TRANSPARENT ABOUT ANY DNC RELATED EVENTS, BUT THIS ADMINISTRATION IS AS CLEAR AS MUD 



Carney Said The White House Is "Very Transparent" About Any DNC Related Events.  "Carney said Friday the White House is 'very transparent' about its DNC-related events." (Josh Gerstein, "Donor Meeting At White House Draws Fire,"  Politico ,  6/20/11) But Hasn't Said Who Attended The Blue Room Meeting.  "By press time, the White House did not respond to questions about DNC members and the White House staff who attended the event." (Josh Gerstein, "Donor Meeting At White House Draws Fire,"  Politico ,  6/20/11) The White House Logs Are "Missing The Names Of Thousands Of Visitors To The White House, Including Lobbyists," And Donors.  "Similarly, the logs are missing the names of thousands of other visitors to the White House, including lobbyists, government employees, campaign donors, policy experts and friends of the first family, according to an investigation by the Center for Public Integrity." (Viveca Novak and Fred Schulte, "White House Visitor Logs Leave Out Many,"  Politico ,  4/13/11) 

And The White House Hasn't Answered  Politico's  Question: "How Many DNC-Organized Events Has The President Taken Part In That Weren't Listed On His Schedule?"  "The topic didn't come up during White House press secretary Jay Carney's briefing. We wonder: How many DNC-organized events has the president taken part in that weren't listed on his schedule?" ("What About The Wall St. Meeting,"  Politico ,  6/15/11) 

  

Obama Won't Disclose Fundraising Totals For Each DNC/Obama For America Fundraising Event 

  

"Pres. Obama s Re-Election Campaign Won t Be Disclosing How Much Money Is Taken In At The Individual Fundraising Events Attended By Pres. Obama. Neither Will The Democratic National Committee."  (Mark Knoller, "Obama Campaign Won t Reveal Fundraising Numbers,"   CBS News ,  4/19/11) 

"Pres. Obama Often Trumpets That His White House Is More Transparent And Open Than Any Other. But It s Clear He Does Not Apply That Approach To His Political Fund-Raising."  (Mark Knoller, "Obama Campaign Won t Reveal Fundraising Numbers,"   CBS News ,  4/19/11)   

  Other Re-Election Campaigns Have Disclosed The Totals From Individual Events.  "The Obama Campaign is not the first to adopt such a policy, although Pres. Bush s re-election operation in 2004 freely disclosed how much money was taken in at individual events." (Mark Knoller, "Obama Campaign Won t Reveal Fundraising Numbers,"   CBS News ,  4/19/11) 



Or How Much The DNC Reimburses The Government For The Costs Of Air Force One 

  

"Obama Is Entitled To Use Air Force One For Political Trips" But Must Reimburse Taxpayers For "A Relatively Small Portion Of The $100,000-Per-Hour Operating Costs."  "Being president cuts into a candidate s fundraising time, but it also facilitates travel. As chief executive, Mr. Obama is entitled to use Air Force One for political trips, though his campaign must reimburse the government for a relatively small portion of the $100,000-per-hour operating costs of flying aboard the 747-version of Air Force One." (Mark Knoller, "Obama Seeks To Bring In Big Bucks For 2012 Campaign,"  CBS ,  4/4/11) 

"For Two-Years, CBS News Has Been Trying To Get The White House To Disclose The Costs Of Political Travel By The President And The Amount The Government Is Reimbursed By The Candidates And Political Committees For Whom He Was Raising Money. For Two Years, The White House Has Declined To Disclose The Data."  (Mark Knoller, "Obama Seeks To Bring In Big Bucks For 2012 Campaign,"  CBS ,  4/4/11) 



An Official Event Allows The Political Costs Of The Trip To Decrease, But We Don't Know By How Much Since The White House Has Not Disclosed Its Formula.  "By doing an 'official' event during the same trip as political fundraisers, the costs the DSCC and the Nelson campaign must pay to reimburse the government for Mr. Obama s trip to Miami is significantly reduced, although the White House has yet to disclose the formula it uses to arrive at the amounts to be reimbursed." (Mark Knoller, "Obama To Raise $1M In Florida For Democratic Campaigns,"  CBS News ,  3/4/11)   





From:  White House Press Office  [mailto:noreply@messages.whitehouse.gov]   

Sent:  Tuesday, June 21, 2011 4:04 PM 

Subject:  CORRECTED: Remarks by the President at a DNC Event 



THE WHITE HOUSE  

Office of the Press Secretary 

For Immediate Release                                    June 20, 2011 

REMARKS BY THE PRESIDENT 

AT DNC EVENT 

Mandarin Oriental Hotel 

Washington, D.C. 

  

Please see below for a correction (marked with an asterisk) to the transcript.  

9:06 P.M. EDT 



THE PRESIDENT:  Thank you.  Thank you, everybody.  Thank you.  (Applause.)  Thank you, everybody.  Please have a seat, have a seat. 

It is wonderful to see all of you.  I ve got a lot of friends in the room here.  People who knew me before anybody could pronounce my name.  (Laughter.)  People who knew me before I had gray hair.  (Laughter.)  It is wonderful to see those of you who've been friends for a long time, and it's wonderful to see new friends here as well. 

What I d like to do is to make some very brief remarks at the top and then have a chance to take a few questions, because that will give us a chance to have a dialogue, and you might have some suggestion that we haven't thought of.  And it's one of the great things about these kinds of events is people here have so much expertise in so many different areas that it's a wonderful thing for me to be able to pick your brain as well as just you guys hearing me chatter. 

We are obviously going through one of the toughest periods in American history.  We went through the worst financial crisis since the Great Depression, and immediately after being elected, I had to take a series of very difficult steps to rescue ourselves from the brink.  We had lost 4 million jobs in the six months before I was sworn in; lost another 4 million during the period probably six months after I was elected.  And so as a consequence, we had to do some things that we didn't expect we would have to do, just to save the economy stabilize the financial system, make sure that states and local governments didn't have to lay off police officers and cops and firefighters. We had to save an auto industry.  I never expected to be a automobile executive.  (Laughter.)  

As a consequence of that swift, decisive, and sometimes difficult period, we were able to take an economy that was shrinking by about 6 percent and create an economy that is now growing, and has grown steadily now over many consecutive quarters.  Over the last 15 months we've created over 2.1 million private sector jobs. (Laughter.)  (Applause.)*  We have an auto industry that, for the first time in a very long time is profitable, and the Big Threeautomakers actually gaining market share, and not only gaining market share, but also gaining market share in the cars of the future so that they're actually competing in compact cars and sub-compact cars and electric cars and hybrids. 

And so I m extraordinarily proud of the economic record that we were able to produce over the first two and a half years, but having said all that, the economy is still so tough for so many people around the country.  The hole that was dug was so deep.  And most importantly, the reasons that I decided to run for President in the first place still had not been fully addressed, because the fact is, is that even before this financial crisis, wages and incomes had flat-lined for most Americans.  Those at the very top had seen themselves do very well, but the bottom 95 percent, the bottom 90 percent, they were treading water at a time when their cost of health care and cost of college education, cost of groceries, cost of gasoline all were going up. And that was before the crisis hit.  And now they've got to worry about homes that have lost value and businesses that are just barely getting by.  

And so although we've made a turn in a positive direction, the underlying structural challenges that we face remain.  And so the reason that 2012 is important is because I did not just run for President to get us back to where we were; I ran for President originally to move us to where we need to be. 

And what that means is that what we've begun we had to finish.  We've begun to reform our education system, and thanks to programs like Race to the Top, we're not just putting more money into the schools.  We are saying to schools and states and local school districts, if you reform, if you get rid of the dogmas of the left or the right and you focus on student achievement and how to get the best possible teachers at the front of the classroom and we're rewarding excellence and we are holding ourselves accountable, you know what, there's no reason why we can't make sure that we have the highest proportion of college graduates in the world and make sure that every single one of our young people are equipped to compete in a 21st-century economy.  (Applause.) 

We have begun the process of changing how we think about energy in this country -- made the largest investment in clean energy in our history through the Recovery Act; have stood up entire industries like advanced battery manufacturing; invested in making sure that wind power and solar power and biothermal energy, that all of these things are being developed and researched right here in the United States of America. 

But the fact of the matter is, is that we are still way too dependent on foreign oil and the fuels of the past.  And so part of our unfinished business is making sure that we are getting electric cars on our roads and that we are not only tapping into traditional energy sources here in the United States of America but we re also becoming more energy-efficient.  We re at the cutting edge of a clean energy revolution that could not only free ourselves from dependence on foreign oil and clean up our environment, but also produce jobs right here in the United States of America.  Our job is not finished when it comes to energy policy. 

We re not done when it comes to rebuilding our infrastructure.  America has always had the best stuff.  We had the best roads, we had the best ports, we had the best airports. People would travel from around the world to marvel at the infrastructure we had built.  We can t claim to have the best anymore.  You go to airports in Beijing or Singapore that put a lot of our airports to shame.  High-speed rail networks all through Europe that could be built here in the United States of America. 

And so imagine what we could do putting people back to work right now doing the work that America needs to be done.  We started.  We made the largest investment in infrastructure since Dwight Eisenhower was President through the Recovery Act, but we ve still got $2 trillion worth of repairs to be made.  And think about all those unemployed construction workers out there that could be working right now rebuilding America for the future and not just the old traditional infrastructure, the new infrastructure a smart grid that would help us become more energy-efficient and get energy from wind farms or solar panels to the places where it s needed most; making sure that we ve got the best broadband and 4G and 5G and so that we have the best communication networks in the world. 

We started, but we haven t finished.  We ve started reforming our health care system, and I could not be prouder of the work that we did on the health care act but we now have to implement it, because health care costs are still going up too fast for families, for businesses, and for governments, state and federal, that are paying the bills. 

And so this is a matter not only of making sure that 30 million Americans never again have to go bankrupt because somebody in their family gets sick.  It s also making sure that we re getting a better bang for our health care dollar; that instead of taking five tests, you take one test and it s emailed to five doctors; that we make certain that preventive medicine is in place so that people aren t getting amputated because of diabetes they re not getting diabetes in the first place. 

Those are the changes that we initiated through the Affordable Care Act, but we ve got to finish the job.  The same is true when it comes to financial reform making sure that we never go through the financial meltdown that we went through again. but also, at the same time, that we re looking after consumers and protecting them for the first time in a very long time, whether it s getting a mortgage or taking out a credit card.  Our job is not finished. 

We ve made tremendous progress on a whole host of social issues, from ending don t ask, don t tell so that every American can serve their country regardless of who they love, to making sure that we ve got equal pay for equal work, to making sure that we ve got national service so that our young people can use their talents to help rebuild America. 

But our job is not finished.  We still have work to do on immigration reform, where we have to once again be a nation of laws and a nation of immigrants; one that welcomes the strength that comes from talented people from all around the world wanting to be here, but also making sure that we re doing it in orderly way. 

And we sure have got a lot of work to do on the international front.  When I came into office, we had two active wars.  By the end of this year, one war will be done.  And we will be transitioning in Afghanistan to turn over more and more security to the Afghan people. 

But there s also enormous challenges and opportunities to all that s happening in the Arab world right now.  And it requires us to articulate clearly what we stand for, what our values are, to reject isolationism, but it also requires us to recognize that us having influence in these affairs is going to have less to do with our firepower and more to do with our ideas and our example, our economic engagement, the quality of our diplomacy.  We ve still got more work to do. 

So the bottom line is this.  Back in 2008, on election night, in Grant Park it was a nice night in Chicago I said to people, this is not the end, this is the beginning.  We ve got a steep climb ahead of us to get to that summit where we want to be, where every single American knows that if they work hard, if they re doing the right thing, if they re carrying out their responsibilities, they have a chance at the American Dream. 

We re just part of the way up that mountain.  And the only way we re going to get all the way up that mountain is if we are as engaged, as motivated, as involved, as excited, working as hard as we were in 2008.  And that may be a little bit of challenge because, let s face it, back in 2008, I was new.  (Laughter.)  

 Now I m gray.  (Laughter.)  I ve got dings and dents.  The old posters are all faded.  (Laughter.)  People make fun of hope and change.  And some folks have said, well, change didn t happen as fast as I wanted, or it s not exactly as I expected, or why can t he just change the minds of all those Republicans.  (Laughter.) 

 The thing is, change is never easy because we live in a democracy.  And that s what s wonderful about this country, is we argue it out and ideas are tested and sometimes we lurch this way or that way and mistakes are made.  But our general trajectory has always been to advance prosperity and equality and opportunity. 

And so this process, as difficult as it has been, has also been invigorating.  And I ve never had more confidence in the possibilities of this great American experiment, partly because I get a chance to see and talk to Americans from every walk of life.  And we are a good, decent people.  And as hard as things have been, we are resilient and we come back.  

And so if you re willing to join with me in what will be my last campaign (laughter) if you re willing to dig deep and talk to your friends and neighbors and coworkers and recognize, yes, we re a little older, we ve matured a little bit, but that that fundamental project of delivering the American Dream for that next generation, that s just as urgent and as vital as ever, then I m confident not only will we win in 2012 more importantly, we ll get a little further up that mountain.  That s our job. 

So, thank you very much, everybody.  Thank you.  Thank you.  (Applause.) 

END             9:23 P.M. EDT
