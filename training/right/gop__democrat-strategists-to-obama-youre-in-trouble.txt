Democrat Strategists To Obama: You're In Trouble 



It's not every day we highlight comments from James Carville, Bob Shrum, Joe Trippi, Doug Schoen and Mark Penn but we wanted to make sure you didn't miss their comments recently.    

Democrat Strategist James Carville And Pollsters Stan Greenberg And Erica Seifert: Write "That Their Party's Current Frame For 2012 Race Is Not Effective" 

http://www.politico.com/blogs/burns-haberman/2012/06/dems-call-for-fresh-message-warn-of-impossible-headwind-125911.html 

President Barack Obama and his party may face an "impossible headwind in November" unless it shifts to a more forward-looking economic message aimed squarely at the middle class, three Democratic strategists warn in a memo out this week. Pollsters Stan Greenberg and Erica Seifert, of Greenberg Quinlan Rosner, and Democratic strategist James Carville write in a research document for Democracy Corps that their party's current frame for the 2012 race is not effective. Based on focus groups in Ohio and Pennsylvania, the strategists argue that voters are simply not convinced that the economy is on the move and it's a mistake to try and tell them otherwise. 

  

Democrat Strategist Bob Shrum: If It's A Referendum Election, Obama Loses 

http://www.youtube.com/watch?v=OWl5e8SwACE 

SHRUM: "So I think we ought to just face reality here. If you did what I think the Governor is suggesting, and maybe he s not, and you just let this be a referendum, I don t think the president could win because the truth of the matter is, he may have created over 4.3 million jobs, he may have saved General Motors, but the country is still not back to where it needs to be."  (CBS's "Face The Nation," 6/3/12) 



Democrat Strategist Joe Trippi: "Handing Your Opponents A Gaffe Is Never Good - But It's Made Worse At A Time When Anxiety About The Economy Is Surging Again" 

http://www.foxnews.com/opinion/2012/06/11/new-threats-to-obama-reelection-bid/#ixzz1xaMmbPn0 

As someone who wants to see President Obama win re-election, nothing is gained by denying that three things from last week his comments on the private sector, the Wisconsin results, and the campaigns' fundraising reports hurt his re-election prospects. They did. While it's clear that President Obama's comment that "the private sector is doing fine" was meant relative to the decline in jobs in the public sector, you can bet that the full context of his remarks won't make it into the attacks ads based on the comment that Romney and his Super PACs have already begun releasing. Handing your opponents a gaffe is never good but it's made worse at a time when anxiety about the economy is surging again.  

  

Former Clinton Pollster Doug Schoen: Carville, Greenberg And Seifert "Couldn't Be More Correct" 

http://www.buzzfeed.com/zekejmiller/clintonites-hit-the-panic-button-for-obama 

Former Clinton pollster and strategist Doug Schoen -- brought in by Clinton to replace Greenberg in a rightward tack after the 1994 midterms -- echoed the memo s conclusions in an email to BuzzFeed. They are absolutely correct. [Democrats] must talk about the future. [I] may have a different view of the message than they have, but they couldn t be more correct. [Democrats] must talk outcomes and benefits to win, he said. 



Democrat Pollster Mark Penn: "Said Obama Needs More Than Just A New Message" 

http://www.buzzfeed.com/zekejmiller/clintonites-hit-the-panic-button-for-obama 

But pollster Mark Penn, Schoen s former partner and a member of Clinton s inner circle in the White House and later a force on Hillary Clinton s presidential campaign, said Obama needs more than just a new message -- but also a new economic plan. I think that the president needs a new economic plan that takes the country into the 21st century global economy, a plan with emphasis on education, infrastructure, innovation, and growing exports. A plan that creates new economy jobs for a country that wants to move  forward, he told BuzzFeed, adding that most of the messages [in the memo] are too much about raising taxes and raising spending in a public that has changed quite dramatically from 1992.
