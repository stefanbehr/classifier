Democrats' Problem With Real Economic Data 



The DNC attempts to conduct economic analysis in their pre-buttal to tonight's GOP presidential primary debate at the Reagan Library, claiming that cutting our $1.3 trillion deficit, capping spending, and balancing the budget would cost 9.5 million jobs. The  Democrats claim their "analysis" is valid because it is based on the "conservative Romer-Bernstein rule of thumb  that equates 1 percent of GDP to 1 million jobs." 

Using that same "conservative  Romer-Bernstein rule of thumb" led Obama and Democrats to make their infamous claim that Obama's $825 billion stimulus would keep unemployment below 8 percent. 

We all know how that turned out:  the unemployment rate increased from 8.2 percent to 9.1 percent. We've had a post WWII-record 31 months of unemployment above 8 percent. 

And today we learned from The Washington Post's Fact Checker that the Obama claim that Democrats signed the biggest middle-class tax cut is just not true, receiving the maximum four Pinocchio's (i.e. a really big lie). When asked for outside validation of their claim, the Obama White House cited a report that ... cited White House talking points. 

But with Obama's dismal record on the economy - 2.4 million jobs lost, 9.1 percent unemployment, $4 trillion added to the national debt - why worry about credibility? 

### 

DNC PRE-BUTTAL DNC Report: Tea Party Economic Plan [Cut, Cap and Balance] Endorsed By GOP Presidential Field Would Cost 9.5 Million Jobs If Fully Phased In By 2012 This new report uses the conservative Romer-Bernstein rule of thumb that equates 1 percent of GDP to 1 million jobs. The Tea Party economic plan endorsed by the GOP presidential field, euphemistically referred to as Cut, Cap and Balance, might make for great sound bites at a Tea Party Express rally, but based our analysis, it would be devastating to American workers and the middle class. http://my.democrats.org/DNC-report 

Obama's Economic Advisors - Christina Romer and Jared Bernstein - Predicted That The Stimulus Would Keep The Unemployment Rate Below 8 Percent With Stimulus. (Christina Romer and Jared Bernstein, The Job Impact Of The American Recovery And Reinvestment Plan, 1/9/09) 

Since President Obama Took Office The Nation Has Lost 2.4 Million Jobs. (Bureau Of Labor Statistics, BLS.gov, Accessed 9/6/11) 

Since President Obama Took Office, The Unemployment Rate Has Increased From 7.8 Percent To 9.1 Percent. (Bureau Of Labor Statistics, BLS.gov, Accessed 9/6/11) 

Since President Obama's $825 Billion Stimulus Bill Passed, The Unemployment Rate Has Increased From 8.2 Percent To 9.1 Percent. (Bureau Of Labor Statistics, BLS.gov, Accessed 9/6/11) 

Since Obama Took Office, The National Debt Has Increased By $4 Trillion. (Department Of The Treasury, "Debt To The Penny And Who Holds It," TreasuryDirect.gov , Accessed 9/6/11)
