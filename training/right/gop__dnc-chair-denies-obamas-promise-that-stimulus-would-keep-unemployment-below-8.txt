DNC Chair Denies Obama's Promise That Stimulus Would Keep Unemployment Below 8% 



Yesterday, DNC Chair Debbie Wasserman Schultz denied that President Obama promised his stimulus would keep unemployment below 8%. 



Fox News' Shepard Smith:  "He did promise that unemployment would never get to where it is now if we spent all that money and of course here it is." 

DNC Chair Rep. Wasserman Schultz:  "Oh come on, promised?" 

Smith:  "Well no - he did." 

Rep. Wasserman Schultz:  "No no no no no." 

Smith:  "He did. What would you call it?" 

But Obama promised that his $825 billion stimulus would create or save 3.5 million job, even releasing a graphic illustrating how his stimulus would keep unemployment below 8%.  However, since the stimulus was passed, unemployment has been above 8% for 35 straight months and the nation has lost over a million jobs. 

Click To View Obama s Stimulus Gap
