DNC Convention Fact Check 



DEMOCRATS ARE PROMISING A "DIFFERENT" CONVENTION 

Today, DNC Chair Rep. Debbie Wasserman Schultz In Charlotte, NC, Said, " We Will Make This The First Convention In History That Does Not Accept Any Funds From Lobbyists, Corporations, Or Political Action Committees." "This convention will be different. It will not look like any other that we've ever seen. President Obama set a high bar that we will meet. We will make this the first convention in history that does not accept any funds from lobbyists, corporations, or political action committees. This will be the first modern political convention funded by the grassroots, funded by the people. Does that make our job a little harder, it sure does. But we're committed to funding this convention in a way that represents the values of our party and our president and puts the American people in charge." (Debbie Wasserman Schultz, Remarks At DNC Kick Off, Charlotte, NC, 9/6/11) 

On Behalf Of The DNC, Michelle Obama Promised "A Different Convention, For A Different Time. " The donor policy explains a mysterious line in the announcement e-mail from first lady Michelle Obama that the party had until now refused to explain. 'We will finance this convention differently than it's been done in the past, and we will make sure everyone feels closely tied in to what is happening in Charlotte,' Obama wrote in announcing the site of the convention Tuesday. 'This will be a different convention, for a different time.'" (Molly Ball, "Democratic National Committee: No Corporate Cash At Charlotte Convention," Politico , 2/4/11) 

The DNC's Contract States That Contributions From For-Profit Companies Or Federal Lobbyists Will Be Prohibited . "The contract states that 'monetary contributions from any incorporated for-profit entity' to the host committee for the convention will be prohibited, along with monetary or in-kind contributions from 'political organizations' and 'individuals registered as federal lobbyists.' Lobbyists may not serve on the convention host committee, either." (Molly Ball, "Democratic National Committee: No Corporate Cash At Charlotte Convention," Politico , 2/4/11) As Are Contributions From Companies Who Received "TARP Or Other Bailout Funds." "Also banned are in-kind contributions of goods and services from entities that received 'TARP or other bailout funds, unless those funds have been repaid in full to the U.S. government.'" (Molly Ball, "Democratic National Committee: No Corporate Cash At Charlotte Convention," Politico , 2/4/11) 

BUT THE ONLY DIFFERENCE IS BETWEEN DEMOCRATS' RHETORIC AND REALITY 

"The New Restrictions, However, Don t Apply To The Host Committee Itself." (Jim Morrill, "A Year Left To Sell The Democratic National Convention," The Charlotte Observer , 9/6/11) 

"Corporations Also Are Permitted To Give In-Kind Contributions, Such As Telecommunications Equipment Or Vehicles." (Jim Morrill, "A Year Left To Sell The Democratic National Convention," The Charlotte Observer , 9/6/11) 

Duke Energy CEO Is Raising Up To $15 Million For The Host Committee, And Will Be taking Corporate Checks. "Duke Energy CEO Jim Rogers, who co-chairs the committee, is quietly raising up to $15 million for the committee, in part from corporate contributions. That s on top of the $37 million in noncorporate contributions for the convention itself. Among other things, the money will go toward organizing and hosting events for the expected 35,000 visitors." (Jim Morrill, "A Year Left To Sell The Democratic National Convention," The Charlotte Observer , 9/6/11) 

A For-Profit Company, Which Has Spent Millions On Lobbying Efforts During The Obama Administration, Guaranteed A Line Of Credit For The DNC's Convention Funds 

Duke Energy Is Guaranteeing The DNC's $10 Million Line Of Credit For The 2012 Convention. "Duke Energy Corp., whose CEO is leading the fundraising for the Democratic National Convention, is guaranteeing a $10 million line of credit for the event." (Jim Morril, "Duke Guaranteeing $10 Million Line Of Credit For DNC," The Charlotte Observer , 3/12/11) 

"The Credit Line Was Required By The Democratic National Committee." (Jim Morril, "Duke Guaranteeing $10 Million Line Of Credit For DNC," The Charlotte Observer , 3/12/11) 

 "The Credit Line From Fifth Third Bank Is Apparently The First Time Such An Arrangement Has Been Used By Any Democratic Convention Organizers." (Jim Morril, "Duke Guaranteeing $10 Million Line Of Credit For DNC," The Charlotte Observer , 3/12/11) 

And Stockholders Will "Be On The Line" If The DNC's Host Committee Defaults. "A Duke spokesman said stockholders, not rate-payers, would be on the line if the convention s host committee defaults." (Jim Morril, "Duke Guaranteeing $10 Million Line Of Credit For DNC," The Charlotte Observer , 3/12/11) 

Duke Energy Has Spent Over $15.5 Million On Federal Lobbying Efforts Since Obama Has Been In Office. (Center For Responsive Politics, opensecrets.org , Accessed 9/6/11) 

That Line Of Credit Came From A Bank That Took Federal Bailout Money 

The Line Of Credit From Fifth Third Was Agreed To On The Same Day The Bank Offered To Repay Its Bailout Money. "Fifth Third, a bank that entered the Charlotte market in 2008, agreed to the letter of credit on Jan. 19, according to a letter included in the final contract. That was the same day the bank announced a public stock offering to repay its federal bailout, or TARP money." (Jim Morril, "Duke Guaranteeing $10 Million Line Of Credit For DNC," The Charlotte Observer , 3/12/11)
