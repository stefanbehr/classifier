DNC's "Strongest Organization" Pushing Iowans Away From Democratic Party 



DNC Chairwoman Debbie Wasserman Schultz and Team Obama are touting that they've got the strongest organization in Iowa, but new voter registration statistics released today show that Iowans continue to flee Obama and the Democratic Party. 

SHOT: 

@DWStweets:  Talking to Shep Smith about how President Obama Democrats have the strongest organization in Iowa. #iacaucus Photo:  http://t.co/x1f6nOE9    

CHASER: 

New voter registration statistics released by the Iowa Secretary of State today show  another 1,252 Iowans left the Democratic Party  in December  while Iowa  Republicans saw their 34 th  straight month of voter registration gains .  ( Iowa Secretary of State ,  Accessed 1/3/2012) 

Graph of Iowa New Voter Registration:  http://www.gop.com/images/IowaNewVoterReg.gif 

###
