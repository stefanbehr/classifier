DOE LOAN RECIPIENT E-MAILS SHOW DIRECT WHITE HOUSE LINKS TO INFLUENCE OVER LOAN PROGRAMS 



After Obama proclaimed stimulus dollars wouldn't be doled out as political favors and months of administration officials testifying that DOE loans were awarded based on merit, it is becoming more evident that political influence was rampant within the process. 

Today, the House Oversight and Government Reform Subcommittee on Regulatory Affairs, Stimulus Oversight and Government Spending held a hearing on the DOE loan guarantees for renewable energy projects where they grilled executives of various clean energy companies who received loans. In an exchange between Committee Chairman Jim Jordan (R-OH) and John Woolard, President and CEO of BrightSource Energy, Inc., e-mails show a direct contact between BrightSource and White House officials discussing the loan process. 



Click To Watch
