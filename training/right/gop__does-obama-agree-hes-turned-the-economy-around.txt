Does Obama Agree He's Turned the Economy Around? 



As the President heads to North Carolina where unemployment at 9.7 percent is higher than the national average, does Barack Obama agree with Debbie Wasserman Schultz's assessment that they've been able to "turn this economy around?" 



DNC CHAIR DEBBIE WASSERMAN SCHULTZ:  "[W]e were able to, under President Obama s leadership,  turn this economy around." 

NBC's DAVID GREGORY:  "Whoa, whoa, let me just stop you there.  Clearly, the economy has not been turned around, I mean, you just saw those numbers ... Americans don t believe that s the case ." 

OBAMA'S ECONOMIC REALITY 

The Associated Press : "Employers Added 54,000 Jobs In May, Fewest In 8 Months; Unemployment Rate Rose To 9.1 Pct." ("Employers Added 54,000 Jobs In May, Fewest In 8 Months; Unemployment Rate Rose To 9.1 Pct."  The Associated Press , 6/3/11) North Carolina Is Suffering From 9.7 Percent Unemployment.  (Bureau of Labor Statistics, BLS.gov, Accessed 6/13/11) Since President Obama's $831 Billion Stimulus Bill Passed The Unemployment Rate Has Increased From 8.2 Percent To 9.1 Percent.  (Bureau Of Labor Statistics, BLS.gov, Accessed 6/3/11) The Average Duration Of Unemployment Increased To A Record 39.7 Weeks.  (Bureau Of Labor Statistics, BLS.gov, Accessed 6/3/11) 

  

Washington Post-ABC News Poll: "Americans' Disapproval Of How [Obama] Is Handling The Nation's Economy And The Deficit Has Reached New Highs."  "The public opinion boost President Obama received after the killing of Osama bin Laden has dissipated, and Americans' disapproval of how he is handling the nation's economy and the deficit has reached new highs, according to a new Washington Post-ABC News poll." (Dan Balz and Jon Cohen, "Obama Loses Bin Laden Bounce; Romney On The Move Among GOP Contenders,"  The Washington Post , 6/7/11) 

Washington Post-ABC News Poll: Nine In 10 Americans "Rate The Economy In Negative Terms And Six In 10 "Say The Economy Has Not Started To Recover."  "By 2 to 1, Americans say the country is pretty seriously on the wrong track, and nine in 10 continue to rate the economy in negative terms. Nearly six in 10 say the economy has not started to recover, regardless of what official statistics may say, and most of those who say it has improved rate the recovery as weak." (Dan Balz and Jon Cohen, "Obama Loses Bin Laden Bounce; Romney On The Move Among GOP Contenders,"  The Washington Post , 6/7/11)
