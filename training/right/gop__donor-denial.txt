Donor Denial 







Jay Carney: Obama's Contributions Are "Not From Huge Donors At All."   "The fact of the matter is that the President's support as demonstrated by contributions, is - comes demonstratively from small, you know, people who just contributed a little bit. They are not from huge donors at all."  (White House Press Secretary Jay Carney,  Press Briefing,  5/22/12)  

FACT CHECK: OBAMA IS RELYING ON HIS BUNDLERS TO A "GREATER DEGREE" THAN IN 2008 

"In All, 532 Bundlers Were On The Obama Team's List As Of March 31, The End Of The First Quarter, Up From 445 At The End Of 2011." (Jennifer Epstein, "Chopra, Jean-Georges Join Obama Big Cash List,"  Politico,  4/20/12) At Minimum, These Bundlers Have Brought In At Least $106.4 Million Or About 32.4 Percent Of The Obama Campaign's Total Haul.  (Federal Election Commission,  fec.gov,  Accessed 5/22/12; Obama For America And Obama Victory Fund 2012 Volunteer Fundraisers,"  barackobama.com , Accessed 5/22/12) 

"These Latest Bundler Figures Show That The Wealthy, Well-Connected Individuals Who Typically Become Bundlers Are Rallying To Obama s Aid To A Greater Degree Than They Did In His First Bid For The Oval Office." (Russ Choma, "Obama Bundlers Pick Up The Super PAC Slack,"  Center For Responsive Politics ,   "Open Secrets,"  4/23/12)  

Obama Has Been "Rapidly" Increasing His Bundlers, "Doubling The Amount Of Financiers Who Have Brought In At Least $500,000."  "President Obama's reelection campaign has been rapidly increasing the number of big money 'bundlers' collecting checks for his reelection, doubling the number of financiers who have brought in at least $500,000." (T.W. Farnam, "Big Money In A Big Way For Obama's Reelection Campaign,"  The Washington Post,  4/21/12) 

"Of This Year's Bundlers, 117 Are In The Top Echelon, Raising At Least $500,000 Each, Nearly Double The Number The Campaign Reported At The End Of The 2011 And More Than Double The 47 Who Reached That Level In 2008."  (T.W. Farnam, "Big Money In A Big Way For Obama's Reelection Campaign,"  The Washington Post,  4/21/12) "Of The Individuals Who Were Previously Listed As Bundlers, 115 Raised Enough Cash To Bump Up To Higher Fundraising Tiers, Including 45 Who Advanced To The $500,000-And-Up Level."  (Aaron Mehta, "Obama's 'Bundlers' Hauled In More Than $33 Million Last Quarter,"  iWatch News,  4/20/12) 
