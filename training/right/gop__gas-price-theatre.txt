Gas Price Theatre 



Good morning, more evidence today's Rose Garden event is just another gimmick - a shiny object to distract from the fact that Obama doesn't have a solution for gas prices. In advance of his ceremony, administration officials held a conference call where they punted on the actual impact on gas prices and left it up to outside analysts to detangle.  Instead of more Rose Garden ceremonies, maybe Obama should consider having an actual energy policy that will help with Americans' pain at the pump. 



Yahoo News's Olivier Knox live tweeted the call: 

@OKnox:  White House punts on whether today s Pres Obama announcement re: oil speculation would have any impact on gas prices. (cont d) 

@OKnox:   We would leave that to outside analysts to disentangle," senior administration officials tells reporters on conference call. 



@OKnox:  White House also refused to describe impact/extent of enforcement of laws re: oil speculation over past year. 



@OKnox:  In short: White House won t describe extent of   more
