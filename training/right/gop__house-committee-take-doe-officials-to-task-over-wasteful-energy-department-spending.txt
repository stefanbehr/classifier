HOUSE COMMITTEE TAKE DOE OFFICIALS TO TASK OVER WASTEFUL ENERGY DEPARTMENT SPENDING 





Today, the Subcommittee on Oversight and Investigations of the House Energy and Commerce Committee held a hearing on budget and spending concerns at the DOE. Director of the Office of the Budget of the U.S. Department of Energy Christopher Johns, DOE Inspector General Gregory Friedman and Director of Natural Resources and Environment Frank Rusco were grilled on wasteful spending practices rampant within the Department of Energy. 

Highlights of the Hearing: Congressman Joe Barton (D-TX) grilled Budget Director Johns on his lack of knowledge of how many cars the Department of Energy owns. IG Friedman called the estimated 60 million the department spends on travel a year "paltry" in comparison to that of DOE contractors. Director Rusco remarks that the DOE Loan Guarantee Program is "less willing" to accept the GAO's recommendations to curb wasteful spending practices. 

Inspector General Friedman Estimates a "Paltry" $60 Million A Year Is Spent On Travel By DOE Employees. 

http://www.youtube.com/watch?v=qqI2f7rhL2M 

The Budget Director Johns Doesn't Have A Clue How Many Vehicles The DOE Owns. 

http://www.youtube.com/watch?v=0ORE41TtVoQ 

IG Friedman would not comment on the over 300 active investigations within the DOE. 

http://www.youtube.com/watch?v=mcjzh74VuoA feature=youtu.be 

IG Friedman Says the states and federal government were not fully prepared to address the stimulus. 

http://www.youtube.com/watch?v=VoDTrdwkksc feature=youtu.be 

Director Rusco says DOE loan guarantee program is one of many programs within the DOE that are less willing to accept the GAO's recommendations 

http://www.youtube.com/watch?v=l0S1yo946U8 feature=youtu.be
