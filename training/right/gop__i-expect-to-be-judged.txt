"I Expect To Be Judged..." 





"On Tuesday, The President Will Visit Interstate Moving Services In Springfield, Virginia, To Announce First Of Their Kind Fuel Efficiency Standards For Work Trucks, Buses, And Other Heavy Duty Vehicles."  (Lynn Sweet, "Obama Aug. 8, 2011 Week Ahead. Fuel Standards, Iftar Dinner, Fund-Raiser, Green Bay Packers,"  Chicago Sun-Times'  "The Scoop From Washington,"  8/8/11) 

THE LAST TIME HE WAS IN SPRINGFIELD, VA, OBAMA TOLD AMERICANS TO JUDGE HIM BY THE RESULTS OF HIS STIMULUS 

A Week Before The Stimulus Was Signed Into Law, Obama Promised His Plan Would Provide Jobs For Struggling Americans 

OBAMA TALKS:  "As President, I Expect To Be Judged And Should Be Judged By The Results Of This Program."  (President Barack Obama,  Remarks on Economic Stabilization , Springfield, VA, 2/11/09) 

THE RESULTS:  Since The Stimulus Passed, The Nation Has Lost 1.6 Million Jobs And The Unemployment Rate Has Increased From 8.2 Percent To 9.1 Percent.  (Bureau Of Labor Statistics,  BLS.gov , Accessed 8/5/11) 

Obama's Economic Advisors Predicted That The Stimulus Would Keep The Unemployment Rate Below 8 Percent With Stimulus.  (Christina Romer and Jared Bernstein,  The Job Impact Of The American Recovery And Reinvestment Plan,  1/9/09) *          "The Jobless Rate Has Stayed Above 8% For 30 Straight Months, The Longest Stretch Of High Unemployment Since The Great Depression In The 1930s."  (Jeffry Bartash, "U.S. Economy Gains 117, 000 Jobs In July,"  Market Watch , 8/5/11) 

"The Jobs Report Friday Showed 44.4% Of Unemployed Americans, Or 6.2 Million People, Were Out Of Work For More Than Six Months In July."  (Luca di Leo and Jeffrey Sparshott, "Payrolls Grow As Unemployment Ticks Down,"  The Wall Street Journal , 8/5/11) 

OBAMA TALKS:  Obama Promised "400,000 Jobs Rebuilding Our Roads, Our Railways, Our Dangerously Deficient Dams, Bridges, And Levees."  "So across the country, States need help. And with my plan, help is what they will get. My plan contains the largest investment increase in our Nation s infrastructure since President Eisenhower created the national highway system half a century ago. We ll invest more than $100 billion and create nearly 400,000 jobs rebuilding our roads, our railways, our dangerously deficient Dams, Bridges, and Levees." (President Barack Obama,  Remarks On Economic Stabilization , Springfield, VA, 2/11/09) 

THE RESULTS:   Since The Stimulus Was Passed, America Has Lost 923, 000 Jobs In Construction.  (Bureau Of Labor Statistics,  BLS.gov , Accessed 8/5/11) Since The Stimulus Was Passed, America Has Lost 643, 000 Jobs In Manufacturing.  (Bureau Of Labor Statistics,  BLS.gov , Accessed 8/5/11) 

Former Biden Chief Of Staff Ron Klain: The Stimulus' Infrastructure Spending Was "Hardly A Game Changer."  "Yes, infrastructure projects create jobs. But even by the administration's own estimate, the number of jobs created or saved by $25 billion in Recovery Act spending on roads was a mere 150,000 over a two-year period. That isn't a trivial number, but it's hardly a game changer for an economy that needs to create 5 million jobs each year just to keep the unemployment rate constant." (Ron Klain, Op-Ed, "Forget About Hoover Dam And Other Job-Growth Lessons,"  Bloomberg , 6/14/11) 

OBAMA CONTINUES TO IGNORE THE ECONOMY'S DOWNWARD SPIRAL, TALKING ABOUT CAFE STANDARDS WHEN AMERICANS WANT ANSWERS ABOUT JOBS 

Americans Are Concerned With Their Economic Situation, Not CAFE Standards 

"According To The Poll, 60 Percent Now Say That The Economy Is Still In A Downturn And Getting Worse. That s Up 24 Points From April, When A Plurality Believed That Things Had Stabilized."  (Paul Steinhuaser, "CNN Poll: Economic Pessimism Skyrockets,"  CNN Political Ticker , 8/8/11) The Jump In Economic Pessimism Is Across The Board A Majority Of Every Major Demographic And Political Subgroup Thinks The Economy Is In A Downturn And Getting Worse.   (Paul Steinhuaser, "CNN Poll: Economic Pessimism Skyrockets,"  CNN Political Ticker , 8/8/11) 

"When Asked About The State Of Their Finances Since President Obama Took Office, Almost Half Of Likely Voters -- 48 Percent -- Said Theirs Had Worsened."  (Elise Viebeck, "The Hill Poll: One In 3 Voters Say The US Has Passed Its Peak,"  The Hill,   8/1/11) "More Than One Third Of Likely Voters Believe America's Best Days Are Over, According To A New Poll For The Hill."  (Elise Viebeck, "The Hill Poll: One In 3 Voters Say The US Has Passed Its Peak,"  The Hill,   8/1/11) 

70 Percent Of Likely Voters Don't See Their Economic Position Getting Better.  "Overall, 37 percent of likely voters said they expect their economic position to remain the same, while 33 percent expect it to get worse, though women, at 40 percent, were slightly more likely to foresee stability than men, at 34 percent." (Elise Viebeck, "The Hill Poll: One In 3 Voters Say The US Has Passed Its Peak,"  The Hill,   8/1/11) 

As July's Jobs Numbers Show, The Nation's Unemployment Remains Bleak 

Ezra Klein: "This Is Terrible - It's A Terrible Jobs Number After A Terrible Three Years."  (MSNBC's "Morning Joe," 8/5/11) 

The Wall Street Journal : The Unemployment Rate "Still Leaves Almost 14 Million Americans Who Would Like To Work Without A Job."  "The unemployment rate, which is obtained from a separate household survey, dropped to 9.1% last month from 9.2% in June. That still leaves almost 14 million Americans who would like to work without a job." (Luca di Leo and Jeffrey Sparshott, "Payrolls Grow As Unemployment Ticks Down,"  The Wall Street Journal , 8/5/11) 

The Associated Press : "The Economy Needs Twice As Many Net Jobs Per Month To Rapidly Reduce Unemployment."  "Still, the economy needs twice as many net jobs per month to rapidly reduce unemployment. The rate has topped 9 percent in every month except two since the recession officially ended in June 2009." ("Unemployment Rate Dips To 9.1% As U.S. Economy Adds 117,000 Jobs In July,"  The Associated Press , 8/5/11) AP: "The Report Follows A String Of Gloomy Data That Shows The Economy Has Weakened."  ("Unemployment Rate Dips To 9.1% As U.S. Economy Adds 117,000 Jobs In July,"  The Associated Press , 8/5/11)
