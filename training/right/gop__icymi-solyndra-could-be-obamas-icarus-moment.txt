ICYMI: Solyndra Could Be Obama's Icarus Moment 



Solyndra Could Be Obama's Icarus Moment 

Albuquerque Journal 

Michael Coleman 

September 25, 2011 

http://www.abqjournal.com/main/2011/09/25/politics/solyndra-could-be-obamas-icarus-moment.html#ixzz1Z415AjXr   

"Just when it appeared things couldn't get much worse for President Obama, the Solyndra scandal reared its ugly head this month.  

"The California-based solar panel manufacturer filed for bankruptcy after pocketing a $528 million loan from the Department of Energy. Not only are taxpayers on the hook for the half-billion dollars, but it appears the Obama administration pressured the Office of Management and Budget to speed its loan review so Vice President Joe Biden could trumpet the company at a 2010 press event.  

"The White House clearly wanted to use Solyndra as a poster child for the clean-energy jobs the president promised as part of the 2009 economic stimulus package. Instead, it looks like Solyndra will become congressional Republicans' poster child for wasteful government spending and a potentially major election-year headache for Obama ... 

"That became clear last week when House Republicans - some of whom had lobbied Obama for similar DOE loans to companies in their own districts - moved to slash $100 million from the DOE loan program that funded the Solyndra deal.  

"Rep. Darrell Issa, the chairman of the House Oversight Committee and a fierce and formidable Obama critic, hosted a hearing Thursday he billed as "How Obama's Green Energy Agenda Is Killing Jobs." Ironically, Issa wrote to Energy Secretary Steven Chu to lobby for federal aid to electric-car maker Aptera Motors in California last year, claiming the firm would create jobs, according to the San Francisco Chronicle.  

"A spokesman for Issa told Bloomberg news the Aptera application - pending three years - was different from Solyndra's. "In the entire time that Aptera's application has been pending, Solyndra was able to obtain taxpayer backing and go bankrupt, leaving taxpayers on the hook," Issa spokesman Frederick Hill told Bloomberg. "Most applicants for federal programs don't, in fact, receive the VIP treatment Solyndra did."  

"As a side note, conservative websites are having a field day with reports that George Kaiser, a major Solyndra investor and Obama fundraiser, in 2008 had a series of meetings at the White House around the same time the deal was being finalized.  

"The Solyndra debacle not only led to the demise of more than 1,000 American jobs, but it could also cripple the DOE loan program that aims to prop up the still-fledgling clean-energy industry ..." 



Click here to read the full article :  http://bit.ly/pHhkvm  
