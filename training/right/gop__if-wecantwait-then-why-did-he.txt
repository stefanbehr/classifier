If #WeCantWait... Then Why Did He? 







Watch The Video Here 

CHUCK TODD:   "You are saying you can t wait. Then why have you waited? Why haven t you been why didn t you start doing this six weeks ago as you guys were pushing the jobs bill? Do it simultaneously if this stuff is going to work, even around the margins why wait as long as you have?" (MSNBC's " Daily Rundown ," 10/27/11) 

Especially When Obama Had Control Of The White House, The Senate And The House Of Representatives For Two Years    

"Mr. Obama Has Been The Least Obstructed President Since LBJ In 1965 Or FDR In 1933, Which Is How We Got Here."  "The larger political subtext of Mr. Obama s speech is that if Congress doesn t pass his plan, he ll then campaign against Republicans as obstructionist. Thus his speech mantra that Congress should 'pass it right away.' This ignores that Mr. Obama has been the least obstructed President since LBJ in 1965 or FDR in 1933, which is how we got here." (Editorial, "The Latest Jobs Plan,"  The Wall Street Journal ,  9/9/11)  

Obama Had "The Biggest Senate Majority In Decades" When He Came Into Office . "In 2008, President Barack Obama swept into the White House, and Senate Democrats eventually picked up nine seats, giving the new commander in chief the biggest Senate majority in decades." (John Bresnahan And Manu Raju, "Obama A Drag On Senate Democrats?,"  Politico ,  10/20/11) 

Chuck Todd: "They Got The White House, Filibuster-Proof Senate, A Very Powerful And Large Majority In The House. It s On Their Hands."  CHUCK TODD: "Well, I don t think they re going to do it down the line, Meredith. I think that could be their strategy now from here on out. Sort of they re going to wash their hands of this. They re going to say, `Hey, this is the no-excuses this is the no-excuses agenda for the Democrats. They got the White House, filibuster-proof Senate, a very powerful and large majority in the House. It s on their hands. " (NBC's "Today Show," 7/1/09) 

Roland Martin: "Democrats Control The House, Control The Senate, Control The White House, They Can t Look To Anybody Else."  ROLAND MARTIN: "The Democrats this is in their plank. It has been in their plank for a number of years. There is no way in the world that they are going to allow that this moment when they are so close to slip through their fingers because they cannot blame the Republicans. They will have to accept the blame themselves. Democrats control the House, control the Senate, control the White House, they can t look to anybody else. They ve got to suck it up." (CNN's "Anderson Cooper 360," 9/10/09)
