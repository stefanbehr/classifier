In Case You Missed It: "Follower In Chief" 





The Weekly Standard 

Fred Barnes 

June 27, 2011 

"We've had strong presidents and weak presidents, skillful presidents and incompetent presidents, mediocre presidents and just plain poor presidents. Barack Obama stands alone as the first president who simply declines to lead. 



"On almost every major issue since he took office in January 2009, Obama has dumped responsibility on someone else, merely paid lip service, or let the issue quietly fade away. Just this year, the issues that have gotten the no-leadership treatment from Obama include: the deficit, the debt, Medicare, Social Security, Medi-caid, energy, corporate taxes, medical liability, immigration, and Libya. 



"The president set his pattern of negligible leadership early on in his administration. Rather than draft his own proposals on economic stimulus, health care, cap and trade, and Wall Street reform--his top priorities--he delegated the job to Democrats in Congress. 



"Even Jimmy Carter, one of our weakest presidents, didn't do this. And strong presidents, like Lyndon Johnson and Ronald Reagan, never considered deferring to Congress in that way. They followed the traditional practice of drafting specific legislation--two major tax bills and a military buildup in Reagan's case, civil rights and Medicare in LBJ's--and pressing Congress to ratify their recommendations. 



"Why is Obama so leadership averse? For one thing, it gives him flexibility since he's not tied irrevocably to what congressional Democrats come up with. And it limits his accountability. He's free to attack Republican proposals without attaching himself to an alternative that Republicans could attack." 

... 

Read The Full Article At:   http://www.weeklystandard.com/print/articles/follower-chief_574848.html
