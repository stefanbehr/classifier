In The Headlines....Democrats Criticizing The Top Of Their Ticket 



FYI, banner morning for Democrats criticizing Barack Obama and his policies that are leaving this nation worse off. Check out the run down below: 

NJ: "Manchin On The Fence About Obama Vote" 

http://www.nationaljournal.com/daily/manchin-on-the-fence-about-obama-vote-20120419 

Sen. Joe Manchin, D-W.Va., who has done more than any other Democrat up for reelection this year to distance himself from President Obama, said he does not know if he will vote for Obama or presumptive GOP nominee Mitt Romney in November. "I'll look at the options," Manchin said this week. The last three years "have made it pretty rough" for his state, he said. 

WS: "Endangered Democrat: This Isn t About The Presidential Race " 

http://www.weeklystandard.com/blogs/endangered-democrat-isnt-about-presidential-race_640456.html 

On MSNBC Thursday, Senator Jon Tester (D-Mont.) was asked whether President Barack Obama being at the top of the ticket in November would be a drag on Tester s reelection chances. Watch the video below: The president isn t particularly doing well in a state like Montana, Tester said. But this isn t about the presidential race. This is about a U.S. Senate race in Montana. 

The Hill: "Democrats Expressing Buyers' Remorse On Obama s Health Law" 

http://thehill.com/blogs/healthwatch/politics-elections/222719-democrats-buyers-remorse-on-obama-health-care-law 

An increasing number of Democrats are taking potshots at President Obama's healthcare law ahead of a Supreme Court decision that could overturn it. The public grievances have come from centrists and liberals and reflect rising anxiety ahead of November's elections. "I think we would all have been better off -- President Obama politically, Democrats in Congress politically, and the nation would have been better off -- if we had dealt first with the financial system and the other related economic issues and then come back to healthcare," said Rep. Brad Miller (D-N.C.), who is retiring at the end of this Congress. "It did hurt us, there's no doubt about it. The climate out there was really ugly because of it," said Rep. Norm Dicks (D-Wash.), who is also retiring at the end of this Congress.  But Dicks argues the party isn't likely to suffer much more because of the law, regardless of the Supreme Court's actions. "It's just not an issue. It's all the economy," Dicks said. "We paid a big price two years ago. But we've already paid it." 

  NYT: "Democrats Joining G.O.P. On Pipeline" 

http://www.nytimes.com/2012/04/20/us/politics/democrats-join-gop-on-pipeline-vote.html 

President Obama is finding himself increasingly boxed in on the Keystone pipeline fight as more Congressional Democrats are joining Republicans in backing the project, which has strong labor support and could generate significant numbers of jobs in economically hard-hit states. On Wednesday, the House passed a short-term transportation bill that included a provision that would pave the way for the construction of the next stage of the oil pipeline, a measure that Mr. Obama has said he would veto. The bill passed 293 to 127, with 69 Democrats supporting it. It is the fourth time the House has passed a measure to expedite the project; one failed narrowly in the Senate only after Mr. Obama personally lobbied some Democrats to vote no. With the House vote, Mr. Obama finds himself, for the first time in his presidency, threatening a veto on a significant piece of legislation that enjoys the support of an increasing number of Democrats, as well as the vast majority of Republicans in Congress. Representative Dennis Cardoza, a California Democrat who voted for the House measure, said he would be happy to vote to override a veto if needed. He said: "I think the president has made a very serious mistake here. I'm still supporting the president. But we have to do what's right." But Democrats like Senator Bob Casey of Pennsylvania say they would support a highway bill with a Keystone pipeline provision. "I would vote for it, yes," Mr. Casey said.
