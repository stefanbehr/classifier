Just More Words: "Major Speeches" By The Numbers 





TODAY, OBAMA WILL DELIVER ANOTHER "MAJOR SPEECH" ON THE ECONOMY WITH ZERO NEW PROPOSALS 

49,928: 

Number Of Words In Obama's Past "Major Speeches" On The Economy.  ( The White House , Accessed 6/13/12) 

10: 

Number Of "Major Speeches" Obama Has Made On The Economy.  ( The White House , Accessed 6/13/12) 

0: 

Number Of New Economic Proposals In Today's "Major" Economic Speech. (Carol Lee and Laura Meckler, "Obama To Revisit Economic Debate,"  The Wall Street Journal , 6/11/12) 

AFTER NEARLY FOUR YEARS OF OBAMA'S POLICIES, AMERICANS ARE NOT DOING FINE 

$15.7 Trillion: 

Current National Debt ($ 15,744,360,700,064.85 ).  ( U.S. Treasury Department , Accessed 6/13/12) 

$5.11 Trillion: 

Added To The National Debt Since Obama Took Office.  ( U.S. Treasury Department , Accessed 6/13/12)   

23.2 Million: 

Number Of Americans Who Are Unemployed, Underemployed, Or Marginally Attached To The Workforce.  ( Bureau of Labor Statistics , Accessed 6/13/12)   

12.7  Million: 

Unemployed Americans.  ( Bureau of Labor Statistics , Accessed 6/13/12)   

8.1 Million: 

Americans Working Part-Time For Economic Reasons.  ( Bureau of Labor Statistics , Accessed 6/13/12)   

6.3 Million: 

Number Of Americans That Fell Into Poverty Since Obama Took Office.   ( U.S. Census Bureau , 9/13/11) 

5.4 Million: 

Americans Unemployed 27 Weeks Or Longer.  ( Bureau of Labor Statistics , Accessed 6/13/12) 

1,042,000: 

Construction Jobs Lost Since Obama Took Office.  ( Bureau of Labor Statistics , Accessed 6/13/12) 

552,000: 

Jobs Lost Since Obama Took Office.  ( Bureau of Labor Statistics , Accessed 6/13/12) 

$50,995: 

Your Share Of The National Debt.  ( U.S. Treasury Department  Accessed 6/13/12; U.S. Census Bureau , Accessed 6/4/12) 

$4,300: 

Decline In Median Household Income Since Obama Took Office.  ( Bloomberg , 4/30/12) 

15.1%: 

Americans Living In Poverty.  ( U.S. Census Bureau , 9/13/11) 

8.2%: 

Unemployment Rate.  ( Bureau of Labor Statistics , Accessed 6/13/12) 

AND OHIOANS ARE LOOKING FOR SOLUTIONS, NOT EMPTY WORDS 

431,318: 

Number Of Unemployed Ohioans.  ( Bureau of Labor Statistics , Accessed 6/8/12) 

286,878: 

Number Of Ohioans That Have Fallen Into Poverty Between 2008 And 2010. ("Poverty: 2007 And 2008,"  U.S. Census Bureau , 10/11; "Poverty: 2009 And 2010," U.S. Census Bureau , 10/11) 

66,200: 

Ohio Jobs Lost Since Obama Took Office.  ( Bureau of Labor Statistics , Accessed 6/13/12) 

$45,090: 

The Median Household Income Of An Ohio Family, Down $2,898, Or 6%, From 2008.  (U.S. Census, Household Income For States: 2009 And 2010 , 9/11; U.S. Census,   Household Income For States: 2007 And 2008 , 9/09) 

$27,713: 

The Amount Of Debt The  Average Student Graduating From A Four-Year College Institution In Ohio Has $27,713 In Debt.  ( Project On Student Debt , Accessed 5/4/12) 

55%: 

The Increase In The Number Of Ohioans Receiving Food Stamps Since Obama Took Office.  ("Public Assistance Receipt In The Past 12 Months For Households: 2009 And 2010"  USDA Food And Nutrition Service , 10/11) 

$3.582: 

The Average Price Per Gallon Of Gas In Ohio, Up From $1.903 When Obama Took Office.  ("Current State Averages,"  Fuel Gauge Report , 1/16/09; "Current State Averages,"  Fuel Gauge Report , 6/11/12) 

The Associated Press : "The President Can t Tell Voters About A Grand Economic Comeback Story Because There Isn t One To Tell."  (Ken Thomas, "Obama Doesn't Emphasize Issues He Fought Hard For,"  The Associated Press , 6/12/12) 

Obama Will Deliver A "Framing" Speech, With No New Policy Proposals Because "His Economic Team Made Clear They Don't See Many Fresh Options."  "Mr. Obama s address in Cleveland, described by his aides as a 'framing' speech, isn t expected to include any major new proposals. While some of his political advisers had pushed for that, his economic team made clear they don t see many fresh options, particularly when Congress hasn t passed the bulk of a jobs bill that the president unveiled nine months ago, according to people familiar with the discussions." (Carol Lee and Laura Meckler, "Obama To Revisit Economic Debate,"  The Wall Street Journal , 6/11/12) Obama Still Believes That A Second Stimulus Contains His "Best Ideas For Spurring The Economy, And There Are No Alternative Policies Waiting In The Wings."  "People familiar with the speech say the White House believes those proposals still represent Mr. Obama s best ideas for spurring the economy, and there are no alternative policies waiting in the wings. The speech will mark Mr. Obama s first 2012 campaign event that is neither a large rally nor a fundraiser. About 1,000 people are expected to attend the event at Cuyahoga Community College, people familiar with the event said." (Carol Lee and Laura Meckler, "Obama To Revisit Economic Debate,"  The Wall Street Journal , 6/11/12) It's Unclear If Obama's Speech Will Include Any Details On What He Would Do In A Second Term.  "Recent signs that the economy could be faltering spawned an intense internal debate among the president s aides over the contents of the speech, people familiar with the discussions said, and put its details in flux. One option was for Mr. Obama to draw a sharper picture of what he would concentrate on if he won a second term, said people familiar with the discussions. It is unclear if Mr. Obama s speech will include such a focus." (Carol Lee and Laura Meckler, "Obama To Revisit Economic Debate,"  The Wall Street Journal , 6/11/12) 

"Privately, Senior Obama Advisers Say They Are No Longer Expecting Much Economic Improvement Before The Election."  "For the White House, it was just the latest entry in the when-it-rains-it-pours ledger. This has been one of the worst stretches of the Obama presidency. In Washington, there is a creeping sense that the bottom has fallen out and that there may be no second term. Privately, senior Obama advisers say they are no longer expecting much economic improvement before the election." (Dana Milbank, Op-Ed, "Pileup At The White House,"  The Washington Post , 6/11/12) "The Reality Is The Ideas Coming From Obama Are Likely To Be The Ones He Has Already Proposed."  "The 'president is continuing to work with his team on potential new ideas' to jumpstart the economy, White House spokesman Jay Carney said Monday when pressed about the sagging rate of job growth. The reality is the ideas coming from Obama are likely be the ones he has already proposed. There is no money and no political appetite for bolder ideas." (Andrew Taylor, "Few Options Left For Obama On Economy Before Election Day As Gridlock Takes Hold,"  The Associated Press , 6/5/12)
