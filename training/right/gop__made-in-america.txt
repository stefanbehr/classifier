Made In America? 



In another example of President Obama's campaign double-talk, the Campaigner-In-Chief says he wants manufacturing products to be stamped "Made In America." @markknoller  - Pres Obama says he wants key manufacturing products such as wind turbines to bear the proud stamp of Made in America . 

Yet in an effort to appease the Union Bosses that support his campaign, his Administration continues to hamper companies like Boeing that do exactly that.  Even Obama's nominee for Commerce Secretary has said the President's labor board made the wrong judgment in bringing a lawsuit against Boeing for expanding production in a right-to-work state.  Secretary Of Commerce Nominee John Bryson: I think it's not the right judgment...we thought we were doing the right thing for the country and we looked hard at maintaining the jobs in Washington and expanding the jobs elsewhere for the benefit of the country and  never thought for example of putting those jobs outside the U.S. (Committee On Commerce, Science,  Transportation, U.S. Senate, Hearing, 6/21/11) 

Perhaps the President should change his stump speech to clarify that his chief concern is that products are "made by the unions that fund his campaign" and not that they are "Made In America." 







###
