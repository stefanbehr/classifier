More Debt As He Dithers 





"The White House Has Put The Final Touches On A Nifty New Website Offering Guidelines On How Parents Can Reinforce Money Lessons With Their Kids, Starting As Young As Age Three."   (Dan Kadlec, "What President Obama Wants You To Tell Your Kids About Money - And When,"  Time , 5/22/12) 



A FEW OF THE LESSONS FROM OBAMA'S  MONEY AS YOU GROW 

Lesson 1: YOU NEED MONEY To Buy Things 

Activities To Help President Obama Reach This Milestone Discuss Who Will Pay For Obama's Record Trillion Dollar Deficits 

President Obama Has Racked Up The Three Largest Deficits In U.S. History.  "The U.S budget deficit for fiscal year 2011 is $1.299 trillion, the second largest shortfall in history. The nation only ran a larger deficit for the 2009 fiscal year, which included the dramatic collapse of financial markets and a huge bailout effort by the government."(Erik Wasson, "Treasury Announces 2011 Deficit Is Second Highest In History,"  The Hill's   On The Money , 10/14/11) FY2009: The Federal Budget Deficit Was $1.413 Trillion, The Highest In U.S. History.  ("Monthly Budget Review: November 2011,"  Congressional Budget Office , 11/7/11) FY2011: The Federal Budget Deficit Was $1.299 Trillion, The Second Highest In U.S. History.  ("Monthly Budget Review: November 2011,"  Congressional Budget Office , 11/7/11) FY2010: The Federal Budget Deficit Was $1.294 Trillion, The Third Highest In U.S. History.  ("Monthly Budget Review: November 2011,"  Congressional Budget Office , 11/7/11) 

The CBO Projects The Deficit At The End Of Obama's First Term Will Be $1.253 Trillion, Obama's Fourth Straight Trillion Deficit.   ("An Anlysis Of The President's 2013 Budget,"  Congressional Budget Office , 3/16/12) 

Lesson 4: There's A Difference Between THINGS YOU WANT And Things You Need 

Activities To Help President Obama Reach This Milestone Explain Why Obama Spent His Time Pursuing His Liberal Wish List Rather Than Concentrating On The Economy 

Obama Has "A Scattershot Record (At Best) Of Focusing On The Main Concern Of Main Street: Joblessness."  "Obama arrives at his reelection campaign not merely with a weak performance on Wall Street crime enforcement and reform but also with a scattershot record (at best) of focusing on the main concern of Main Street: joblessness." (Frank Rich, "Obama's Original Sin,"  New York Magazine , 7/3/11) After The Stimulus, Obama "Pivoted Too Quickly" Away From The Economy.  "And second, as soon as the stimulus package was completed, they pivoted too quickly to addressing climate change and health care." (Charlie Cook, "Owning It Democrats Are In A Corner On Their Ownership Of The Economy And What They Can Do To Help Turn It Around,"   National Journal ,  6/6/11) Obama Insisted On Pursuing Health Care Reform Despite Warnings From Advisors "That Such An Initiative Would Distract Attention From The Urgent Need To Focus On The Economy And Jobs."  "When it comes to Mr. Obama himself Mr. Scheiber draws a portrait of a president with a messianic streak, whose 'determination to change the course of history' made him reluctant to accept Mr. Geithner's suggestion that his signature achievement would be preventing another Great Depression. Instead the president insisted on pursuing his vision of health care reform in his first year in office even though many of his advisers were warning that such an initiative would distract attention from the urgent need to focus on the economy and jobs." (Michiko Kakutani, "Obama's Economists, Not Stimulating Enough,"  The New York Times , 2/27/12) 

Lesson 5: You Need To MAKE CHOICES About How To Spend You Money 

Activities To Help President Obama Reach This Milestone Explain Why The Senate Has Failed To Pass A Budget For Over Three Years Discuss Why The President's Budget Has Not Received A Single Vote 

President Obama "Has Been All-Too-Willing To Avoid Making Tough Decisions."  "One of President Obama s political weaknesses in his first term has been that he s all-too-willing to avoid making tough decisions, hesitant to expend political capital for potential long-term gain.  Throughout his first term in office, he s had a cautious governing style, and has avoided taking on some of his party s core constituencies." (Josh Kraushaar, "Obama Trying To Have It Both Ways,"  National Journal , 11/30/11) 

Senate Democrats Have Not Passed A Budget For 1,120 Days, Since April 29, 2009.   (S. Con. Res. 13,  Roll Call 173 ; D 53-3, R 0-40, I 2-0, 4/29/09) Sen. Harry Reid (D-NV) Said Last Year That "It Would Be Foolish For Us To Do A Budget At This Stage."  "Senate Majority Leader Harry Reid said it would be 'foolish' for Democrats to propose their own federal budget for 2012, despite continued attacks from Republicans that the party is ducking its responsibility to put forward a solution to the nation s deficit problems. 'There s no need to have a Democratic budget in my opinion,' Reid said in an interview Thursday. 'It would be  foolish for us to do a budget at this stage.'" (Lisa Mascaro, "Harry Reid: 'Follish' For Democrats To Offer Budget Plan,"  Los Angeles Times , 5/20/11) 

White House Press Secretary Jay Carney Says The White House Has "No Opinion" On Whether The Senate Should Pass A Budget.  ABC NEWS' JAKE TAPPER:  " The White House has no opinion about whether or not the Senate should pass a budget? The president's going to introduce one. The Fed chair says not having one is bad for growth. But the White House has no opinion about whether - " JAY CARNEY:  " I have no opinion -- the White House has no opinion on Chairman Bernanke's assessment of how the Senate ought to do its business." (White House Press Briefing, 2/8/12) Watch The Video 

"President Obama s Budget Suffered A Second Embarrassing Defeat Wednesday, When Senators Voted 99-0 To Reject It."  (Stephan Dinan, "Obama Budget Defeated 99-0 In Senate,"  The Washington Times , 5/16/12) "Coupled With The House s Rejection In March, 414-0, That Means Mr. Obama s Budget Has Failed To Win A Single Vote In Support This Year."  (Stephan Dinan, "Obama Budget Defeated 99-0 In Senate,"  The Washington Times , 5/16/12) 

Lesson 8: Putting Your Money In A Savings Account Will PROTECT It And Pay You Interest 

Activities To Help President Obama Reach This Milestone Discuss How The President Has No Plan To Save The Entitlement Programs That Americans Depend On 

"Medicare And Social Security Are On A Fast Track To Deep Fiscal Problems, Trustees For The Two Programs Warned Monday."   (Sam Baker, "Medicare, Social Security Funds Running Out Quickly, Trustees Say,"  The Hill , 4/23/12) Social Security And Medicare Trustees: Social Security Is Predicted To "Run Dry" By 2033, Three Years Earlier Than Last Year's Projection.  "The trustees who oversee Social Security say the program s trust funds will now run dry in 2033. Medicare s finances have stabilized but the program s hospital insurance fund is still projected to run out of money in 2024." (Stephen Ohlemacher, "Poor Economy Worsens Social Security's Finances,"  The Associated Press , 4/23/12) 

Los Angeles Times : Obama "Offers No Real Solution To The United State's Long-Term Fiscal Problems."   "The day after the Greek Parliament approved another round of deep spending cuts in the face of violent protests, President Obama released a budget proposal for the coming fiscal year that offers no real solution to the United States long-term fiscal problems." (Editorial, "What About The U.S. Debt?"  Los Angeles Times , 2/14/12) The Washington Post:  "The Final Budget Of His First Term Does Not Reflect The Leadership On Issues Of Debt And Deficit That Mr. Obama Once Vowed."   (Editorial, "Obama's Budget Falls Short, But It Beats Many Alternatives,"  The Washington Post , 2/13/12) 

Lesson 9: You Should SAVE AT LEAST A DIME For Every Dollar You Receive 

Activities To Help President Obama Reach This Milestone If It Is A Good Idea To Save A Dime For Every Dollar, Why Is Obama Borrowing 34 Cents For Every Dollar He Spends? 

The Federal Government Will Borrow $1.25 Trillion Dollars To Pay For Obama's $3.65 Trillion In Spending; 34 Cents For Every Dollar Spent.  ("Updated Budget Projections: Fiscal Years 2012 To 2022,"  Congressional Budget Office , 3/13/12) 

Lesson 12: A CREDIT CARD IS LIKE A LOAN; If You Don't Pay Your Bill In Full Every Month, You'll Be Charged Interest And Owe More Than You Originally Spent 

Activities To Help President Obama Reach This Milestone Discuss How Obama's Massive Debt Will Cost Future Generation Hundreds Of Billions Of Dollars In Interest Every Year 

In FY2012, The United States Will Pay $224 Billion In Interest On Its $15.7 Trillion Debt.  ("Updated Budget Projections: Fiscal Years 2012 To 2022,"  Congressional Budget Office , 3/13/12) Through 2022, Taxpayers Will Pay Over $4.7 Trillion In Interest To Finance Obama's Binge Spending.  ("An Analysis Of The President's 2013 Budget,"  Congressional Budget Office , 3/16/12) 

Lesson 15: Your First Paycheck May Seem Smaller Than Expected Since MONEY IS TAKEN OUT FOR TAXES 

Activities To Help President Obama Reach This Milestone Obama Is "Intrigued" By The Idea Of Making Your Paycheck Even Smaller, Discuss How This Will Effect You? 

Obama's FY2013 Budget Would Raise Taxes On 27 Percent Of U.S. Households According To A Study By The Tax Policy Center.  "President Barack Obama's 2013 budget plan would raise taxes for 27 percent of U.S. households in 2013, far more than the administration estimates, according to a nonpartisan study. The study released today comes from the Tax Policy Center, a research group in Washington that analyzes proposals from presidential candidates in both parties. Obama focuses the tax increases in his 2013 budget on corporations and the top 2 percent of individual taxpayers. The result in the center's study stems from the fact that taxpayers in all income brackets own parts of corporations." (Richard Rubin, "Obama Budget Raises Taxes For 27% Of Households, Report Says,"  Bloomberg , 3/21/12) 

Obama Has Said He Is "Agnostic" On Raising Taxes On Those Making Less Than $250,000 As Part Of A Plan To Reduce The Deficit.  "President Barack Obama said he is 'agnostic' about raising taxes on households making less than $250,000 as part of a broad effort to rein in the budget deficit.  Obama, in a Feb. 9 Oval Office interview, said that a presidential commission on the budget needs to consider all options for reducing the deficit, including tax increases and cuts in spending on entitlement programs such as Social Security and Medicare." (Rich Miller, "Obama 'Agnostic' On Deficit Cuts, Won't Prejudge Tax Increases,"  Bloomberg, 2/11/10) President Obama Was "Intrigued By [The] Elegance" Of Allowing All Of The Bush Tax Cuts To Expire In Order To Cut The Deficit.  "In November 2009, Orszag would tout an idea that divided the economic team and inspired contempt in the political shop: extending for one or two years George W. Bush's middle class cuts, which were scheduled to expire in 2011, then letting them lapse unless Congress found a way to offset their costs. During a meeting with Obama in the Oval Office, he casually outlined the proposal. The obvious defect was that it would be likely to break the president's campaign pledge to oppose tax increases on the middle class. Nevertheless, Obama was intrigued by its elegance as a deficit-cutting maneuver, according to two people in the room. He also liked the idea of forcing Republicans to grapple with the costs of Bush's policies. Only later did the politicos revolt--the vice president, for one, was apoplectic--and the president lost interest." (Noam Scheiber,  The Escape Artists,   2012, p. 154-155) 

Lesson 20: Always Consider Two Factors Before Investing: THE RISKS AND THE ANNUAL EXPENSES 

Activities To Help President Obama Reach This Milestone Discuss How Obama's Energy Department Ignored Warnings About Its Risky Investments And Cost Taxpayers Millions Of Dollars 

Obama's Clean Energy Investments Are "A Case Study Of What Can Go Wrong When A Rigid Government Bureaucracy Tries To Play Venture Capitalist."  "The Obama administration's vaunted initiative to catalyze the U.S. clean-energy industry -- under attack for betting half a billion dollars on the solar-panel manufacturer Solyndra, which closed last month -- has become a case study of what can go wrong when a rigid government bureaucracy tries to play venture capitalist and jump-start a nascent, fast-changing market." (Steven Mufson And Carol D. Leonning, "Some Clean-Energy Firms Found US Loan-Guarantee Program A Bad Bet,"  The Washington Post ,  9/26/11) The Washington Post:  "The Problem Is That Bureaucrats Are More Likely To Bet Wrong Because They Are Generally Not Full-Time Investment Experts And Have No Skin In The Game Themselves."  (Editorial, "Solyndra: A Bad Bet Obama Should Regret,"  The Washington Post ,  10/6/11) 

Government Accountability Office:  "The Administration Didn't Do Its Due Diligence" When Celebrating The $535 Million In Stimulus Funds For Solyndra.  "It s not his statements the administration will regret; it s the loan guarantees. The President was celebrating $535 million in federal promises from the Department of Energy to the solar startup. The administration didn t do its due diligence, says the Government Accountability Office. 'There s a consequence if you don t follow a rigorous process that s transparent,' Franklin Rusco of GAO told the website iWatch News."(Scott McGrew, "Solyndra Filing A Disaster For Obama,"  NBC Bay Area News , 8/31/11) Solyndra Was One Of Five Companies The Energy Department Failed To Properly Assess, Leaving The GAO "Greatly Concerned."  "Government auditors are questioning whether the administration may have made other bad bets on clean energy. Frank Rusco, a Government Accountability Office director who helped lead a review of the Solyndra loan and the Energy Department's loan guarantee program, said GAO remains 'greatly concerned' by its 2010 finding that the agency agreed to back with loans five companies without properly assessing their failure risks. The companies were not identified in the report, but the agency has since acknowledged that Solyndra was one." (Carol Leonnig, "After Solyndra Failure, Auditors Wonder What Other Bad Bets Obama Officials Made,  The Washington Post , 9/1/11)
