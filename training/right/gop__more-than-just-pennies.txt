More Than Just Pennies 





Obama:  "And today I am proud to announce the government has been completely repaid for the investments we made under my watch by Chrysler." (President Barack Obama,  Remarks At Chrysler Plant , Toledo, OH, 6/3/11) CHASER - 



ABC News' Jake Tapper:  "POTUS says Chrysler has repaid every dime it owes American people. Doesnt mention $1.3 billion that taxpayers will likely lose on the deal." ( Jake Tapper Twitter , Accessed 6/3/11) 

"The Government Is Expected To Lose About $1.3 Billion On Its Bailout Loans To Chrysler Once The Fiat Deal Concludes."  (James Healy, "It's Official: Fiat Now Tops 50% Ownership Of Chrysler,"  USA Today , 6/3/11)
