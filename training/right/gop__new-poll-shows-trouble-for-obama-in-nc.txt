New Poll Shows Trouble for Obama in NC 



Good Evening  - Let's put this new Elon University/Charlotte Observer poll into perspective. A majority of North Carolinians disapprove of President Obama's handling of the economy. Additionally, President Obama still has an uphill climb when it comes to overall job performance with about 48% disapproving while only 45% approve. Maybe that's why President Obama is  running scared  in North Carolina. In the poll, conducted Feb. 26-March 1 by Elon University in cooperation with the Observer and other media outlets in North Carolina, about 51 percent of those surveyed said they disapprove of Obama s handling of the economy, while 43 percent approve. About 48 percent said they disapprove of the president s overall job performance, while about 45 percent said they approve. 



Obama s N.C. approval inches up, but challenges remain, new poll shows 

Charlotte Observer 

Tim Funk and Jim Morrill 

http://goo.gl/U1qOm
