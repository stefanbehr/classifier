Not Made In The U.S.A. 



After Obama campaigned on making electric cars here in the United States, his administration handed out a $529 million federal loan to a company that manufactures its cars in Europe. 

FLASHBACK TO ONE YEAR AGO TODAY: Obama Said He Wants Electric Cars To Be Made In The USA,  Not In Europe: 

Obama Said He Wants Electric Cars Made Right Here In The United States With American Workers, Not In Europe Or Asia.  "I believe in tax breaks for companies like the one that I just visited, companies that are investing here in the United States, small businesses, American manufacturers, clean energy companies. I don t want solar panels and electric cars made in Europe or Asia. I want them made right here in the United States with American workers. That s the choice in this election." (President Barack Obama, Remarks At A Democratic Congressional Campaign Committee Reception,  Providence , RI, 10/25/10) Obama:  "We see a future where we invest in American innovation and American ingenuity; where we export more goods so we create more jobs here at home; where we make it easier to start a business or patent an invention; where we build a homegrown, clean energy industry  because I don't want to see new solar panels or electric cars or advanced batteries manufactured in Europe or Asia. I want to see them made right here in the U.S. of A by American workers." (President Barack Obama, Remarks On The Economy, Cleveland, OH, 9/8/10) 





Click To Watch Obama Say He Doesn't Want Cars Made In Europe 

But Obama Handed Out $529 Million In Taxpayer Dollars To Company Making Cars In Europe:  

An Electric Car Company That Received $529 Million In Federal Loan Guarantees Manufactures Its Cars In Finland.  "A $529 million federal loan guarantee to an electric car company manufacturing automobiles in Finland is drawing more unwanted attention to Energy Department loans. A report by ABC News on Thursday said Fisker, a startup electric car company, is making cars in Finland after receiving the loan because it could not find a contractor in America to actually manufacture its electric vehicles." (Keith Laing, "Energy Defends Loans For Electric Cars,"  The Hill ,  10/21/11)
