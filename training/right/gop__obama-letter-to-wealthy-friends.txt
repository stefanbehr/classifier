Obama Letter to Wealthy Friends 



Dear Wealthy Friends-- 

I never said Hope and Change would be easy.  I also never said it would be cheap. 

As you know, my friend Jon Corzine is on Capitol Hill today testifying about how he lost $1.2 billion in Americans' hard-earned money.  And, unfortunately, that means one of my top campaign bundlers is now out of commission.  

So, with the critical year-end fundraising deadline approaching, I have an important question for you: Will you consider becoming an  Obama Bundler  today? 

There's something in it for you too.  After all, it is the Holiday Season. 

Here's a sampling of some of my top bundlers who raised $500,000 for my campaign and what they were able to experience since my election. Ex-MF Global CEO  Jon Corzine  (My "Wall Street Guy"):  A Personal Call From Joe For Economic Advice AND Political Connections That Helped Delay Regulations Venture Capitalist  Steve Westly :  Direct Access To My Personal Advisor, Valerie Jarrett Tech Investor  Steve Spinner :  A Plush Administration Job Overseeing A Half-Billion Dollar Loan For Solyndra     

But wait there's more.  Other bundlers have received: Coveted ambassadorships Millions in stimulus funds Lucrative White House staff positions Seats on my Jobs Council Visits to the White House Blue Room And much, much more... 

Just imagine what you can get in my second term when I don't have a re-election image to worry about. 

Now, you may have heard that our campaign relies on small donors and "everyday Americans."   But let me be clear: we need someone to actually pay the bills.  So, please, sign up to be an Obama Bundler today. 

Thanks, 

Barack  

P.S.  I'm really going to need a new Treasury Secretary.  And it doesn't look like Jon C.'s going to work out.  (Hint, hint.) 



###
