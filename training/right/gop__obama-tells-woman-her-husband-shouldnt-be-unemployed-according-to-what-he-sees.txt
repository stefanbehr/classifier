Obama Tells Woman Her Husband Shouldn't Be Unemployed According To What He Sees 



What Obama Sees And Hears About The Economy... 

  



Obama:  "Jennifer, can I ask you what kind of engineer your husband is?" 

Jennifer:  "He's a semiconductor engineer." 

Obama:  "It is interesting to me - and I meant what I said, if you send me your husband's resume, I'd be interested in finding out exactly what's happening right there, because the word we're getting is that somebody in that kind of high-tech field, that kind of engineer, should be able to find something right away. And the H1B should be reserved only for those companies who say they cannot find somebody in that particular field so that wouldn't necessarily apply, if in fact, there were a lot of highly skilled American engineers in that position, so I - I -I'd be interested to know - I-I-I will follow up on this." 

(President Barack Obama, "Google+ Town Hall," 1/30/12) 

What America Sees And Hears About The Economy... 

Since President Obama Took Office, The Semiconductors And Electronic Components Manufacturing Sector Has Lost 21,500 Jobs, 5.25 Percent Of The Sector.  ( Bureau Of Labor Statistics , Accessed 1/30/12)
