Obama White House Called Out For "Programmed Answer" On The Economy 



On NBC's Meet The Press, host David Gregory played a clip of President Obama from July 14, 2009 saying his job was to "get this economy back on its feet." Obama said it's a job he gladly accepted and even declared, "Give it to me." 

Gregory then asked his guest, Obama White House Office Of Management and Budget Director Jacob Lew, if Obama bears responsibility for the weak economy. Lew answered with the usual White House talking points, which Gregory promptly called out as a "programmed answer." 

See below for the transcript and video: 

NBC'S DAVID GREGORY: "Is it the President's failed leadership that has brought us to this moment? Does he bear responsibility?" 

JACOB LEW: "I think the President has shown enormous leadership from the first day in office. He inherited an economy where the bottom was falling out. He stabilized it through dramatic actions without which millions of Americans would be looking for work who are working today. We still have a lot work to do, the economy is not growing fast enough. We've made a lot of proposals, we want Congress to act on things like trade agreements and patent reforms, things that are up there on the Hill right now which would help the economy. We think that it's important to extend the payroll tax and unemployment insurance and we want to work together on things like infrastructure and other things that are necessary for the future. There's a lot of work to be done." 

GREGORY: "But Mr. Lew, that's a programmed answer. The reality is, he said, my job is to get the economy back on its feet, that's my job. The reality is, it's not. And this budget deal has fallen apart. The President is trying to lead but he's not accomplishing it."  

(NBC's " Meet The Press ," 7/17/11) 

Click Here To Watch
