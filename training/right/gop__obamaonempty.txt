#ObamaOnEmpty 



SHOT:  Steny Hoyer: Gas prices could hurt Obama 

House Minority Whip Steny Hoyer says he's concerned that rising gas prices will hurt President Barack Obama and fellow Democrats politically.The House's No. 2 Democrat said Tuesday that much like former President George W. Bush was blamed for high gas prices during the last administration, Obama could face political pain as consumers continue doling out more cash to fill up their cars. "They're not at historic high levels at this point in time," Hoyer, referring to current gas prices, told reporters on Tuesday. "But they are high, and yes I'm concerned, and yes I'm concerned about the impact it has on the administration." The average gas price is $3.72 per gallon, according to the AAA. Republicans have pounced, trying to pin escalating prices on the Democratic White House and its energy policies. 

CHASER:   Obama's approval rating falls sharply 

President Obama's approval rating has fallen sharply this week, hitting its worst margin since early January, according to  Gallup's three-day rolling average.  Only 43 percent of those surveyed said they approve of the job the president is doing, compared to 50 percent who disapprove. 

 ###
