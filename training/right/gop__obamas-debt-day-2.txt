Obama's Debt Day 2 



Today the RNC is again messaging on the deficit and debt with an op-ed in the Union Leader from Chairman Priebus, a series of conference calls in battleground states and a research piece talking about what Obama's failure to address the debt means for future generations of Americans. Yesterday we kicked off the week of joint messaging with a web video  "Empty Promises: Debt and Deficits" . - Kirsten 



Reince Priebus Union Leader Op-Ed: President Obama's Massive Debt Threatens Our Freedom.   "I don't want to wake up four years from now and find out that our children's future was mortgaged by another mountain of debt." That was then-candidate Obama in 2008. But four years later, a "mountain of debt" is exactly what we're waking up to -- all thanks to President Obama. We all know the national debt poses a grave threat to our economy, our national security, and ultimately our freedom. But President Obama ignored that reality and made matters worse. He broke his promises to control   more
