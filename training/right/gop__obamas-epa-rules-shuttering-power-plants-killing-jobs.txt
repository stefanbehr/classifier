Obama's EPA Rules Shuttering Power Plants, Killing Jobs 



Today,  The Toledo Blade  Reports That FirstEnergy Will Close Three Ohio Power Plants Among Others Due To Regulations Imposed By The Environmental Protection Agency (EPA), Threatening Over 500 Jobs.  "The Akron-based utility which owns Toledo Edison said it was shutting the facility because of air emission standards being imposed by the U.S. Environmental Protection Agency. In all, FirstEnergy said it would also shut plants in Eastlake, Ashabula, and Cleveland in Ohio, as well as in Adrian, Pa., and Williamsport, Md. The plants collectively produce 2,689 megawatts of electricity and employ 529 workers." ("FirstEnergy To Shutter Bay Shore Coal-Fired Plant,"  The Toledo Blade , 1/26/12) 

THE FACTS 

Dayton Daily News:  "With An Estimated $9.6 Billion Price Tag, The Rules Rank Among The Most Expensive In The EPA's History."  (Steve Bennish, "EPA Rules To Force Old Coal Plants To Adapt, Close,"  Dayton Daily News , 1/6/12) 

Ohio Filed Suit Challenging The EPA's Cross-State Air Pollution Rule.  "A U.S. federal appeals court on Friday issued a last-minute order to delay the January 1 implementation of stricter federal limits on pollution from coal-fired plants, providing a temporary win for utilities worried about the cost of implementation. Other states challenging the cross state rule were Louisiana, Kansas, Nebraska, Alabama, Florida, Oklahoma, South Carolina, Virginia, Georgia, Indiana, Michigan, Mississippi, Ohio And Wisconsin."   (Eileen O'Grady, "Court Delays EPA Rule On Coal Plants,"  Reuters , 12/30/11)
