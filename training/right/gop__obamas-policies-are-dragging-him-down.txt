Obama's Policies Are Dragging Him Down 



Nationally, Romney Is Up 

CBS/  The New York Times  Poll: If The Presidential Election Were Held Today, 46 Percent Of Registered Voters Say They Would Vote For Romney, While 43 Percent Say They Would Vote For Obama.  ( CBS/ The New York Times Poll ,  562 RV, MoE 4%, 5/11 -13/12) Among Women Voters, 46 Percent Say They Would Vote For Mitt Romney, While 44 Percent Say They Would Vote For Obama.  ( CBS/ The New York Times Poll ,  562 RV, MoE 4%, 5/11 -13/12) Among Independent Voters, 43 Percent Say They Would Vote For Romney While 36 Percent Say They Would Vote For Obama.  ( CBS/ The New York Times Poll ,  562 RV, MoE 4%, 5/11 -13/12) 

If The Presidential Election Were Held Today, 46 Percent Of Americans Say They Would Vote For Romney, Compared To 42 Percent Who Say They Would Vote For Obama.  ( The Economist /YouGov Poll,  1000 R, MoE 4%, 5/12-14/12) 

Americans Don't Approve Of Obama's Handling Of Economic Issues 

54 Percent Of Americans At Least Somewhat Disapprove Of Obama's Handling Of The Economy, While Just 38 At Least Somewhat Approve.  ( The Economist /YouGov Poll,  1000 R, MoE 4%, 5/12-14/12) 41 Percent Of Americans Say They Strongly Disapprove Of Obama's Handling Of The Economy, Compared To Just 14 Percent Who Say They Strongly Approve.  ( The Economist /YouGov Poll,  1000 R, MoE 4%, 5/12-14/12) 

67 Percent Of Americans Say It Is In Bad Shape, While Just 32 Percent Of Americans Say The Economy Is In Good Shape.   ( CBS/ The New York Times Poll ,  615 A, MoE 4%, 5/11 -13/12) 63 Percent Say The Economy Is Staying The Same Or Getting Worse, While Just 36 Percent Of Americans Say The Economy Is Getting Better.  ( CBS/ The New York Times Poll ,  615 A, MoE 4%, 5/11 -13/12) 

57 Percent Of Americans At Least Somewhat Disapprove Of Obama's Handling Of The Deficit, While Just 33 At Least Somewhat Approve.  ( The Economist /YouGov Poll,  1000 R, MoE 4%, 5/12-14/12) 42 Percent Of Americans Say They Strongly Disapprove Of Obama's Handling Of The Deficit, Compared To Just 11 Percent Who Say They Strongly Approve.  ( The Economist /YouGov Poll,  1000 R, MoE 4%, 5/12-14/12) 

54 Percent Of Americans At Least Somewhat Disapprove Of Obama's Handling Of Taxes, While Just 35 Percent At Least Somewhat Approve.  ( The Economist /YouGov Poll,  1000 R, MoE 4%, 5/12-14/12) 35 Percent Of Americans Say They Strongly Disapprove Of Obama's Handling Of Social Security Compared To Just 14 Percent Who Say They Strongly Approve.  ( The Economist /YouGov Poll,  1000 R, MoE 4%, 5/12-14/12) 

Concern Over The Economy Is At Historic Levels, Painting A Dire Picture For Obama's Reelection Prospects 

Gallup: "National Mood A Drag On Obama s Re-Election Prospects."  (Lydia Saad, "National Mood A Drag On Obama's Re-Election Prospects,"  Gallup , 5/16/12) 



"Several Indicators Of Economic And Political Climate" Are At Levels That Could Be "Troublesome" For Obama's Reelection Campaign.  "Some six months before voters head to the polls to choose the next president of the United States, Gallup finds several indicators of the economic and political climate holding steady at levels that could be troublesome for President Barack Obama." (Lydia Saad, "National Mood A Drag On Obama's Re-Election Prospects,"  Gallup , 5/16/12) 

Americans' Concern About The Economy Is "Greater Today Than For Any President Seeking Re-election Since Jimmy Carter."  "The extent of Americans concern about the economy as evident in their top-of-mind mentions of it as the nation s 'most important problem' is greater today than for any president seeking re-election since Jimmy Carter in 1980." (Lydia Saad, "National Mood A Drag On Obama's Re-Election Prospects,"  Gallup , 5/16/12) 

Only 24% Of Americans Are Currently Satisfied With The "Way Things Are Going In The United States," "Most Similar To The 20% Recorded In May 1992 During George H.W. Bush's Term.  "Perhaps the broadest indicator of the public s mood comes from Gallup s satisfaction measure, which asks Americans if they are satisfied or dissatisfied with 'the way things are going in the United States at this time.' The 24% of Americans currently satisfied is most similar to the 20% recorded in May 1992 during George H.W. Bush s first and only term. Bush was also the only sitting president of the last four to lose his re-election bid." (Lydia Saad, "National Mood A Drag On Obama's Re-Election Prospects,"  Gallup ,  5/16/12) 

Obama's Policies Take A Toll On The Swing States 

Romney Is Already Catching Obama In The Swing States 

Rasmussen Reports: Romney Leads Obama 51-43 Percent In North Carolina.  ( Rasmussen Reports , 500 LV, MoE 4.5%, 5/14/12) 

Marquette University Poll: Among Likely Voters, Romney And Obama Are Tied At 46 Percent In Wisconsin.   ( Marquette University , 604 LV, MoE 4.1%, 5/9-5/12/12) 49 Percent Of Registered Wisconsin Voters Disapprove Of President Obama's Job Performance While 45 Percent Approve.  ( Marquette University , 704 RV, MoE 3.8%, 5/9-5/12/12) 

In Washington, Voters Dislike ObamaCare 

SurveyUSA Poll:  46 Percent Of Washington Voters Oppose ObamaCare While 39 Percent Support It.  ( SurveyUSA , 557 RV, MoE 4.2%, 5/8-5/10/12) 47 Percent Of Washington Independent Voters Oppose ObamaCare While 35 Percent Support It.   ( SurveyUSA , 557 RV, MoE 4.2%, 5/8-5/10/12) 

Across Swing States, Voters Are Unhappy With Obama's Handling Of The Housing And Mortgage Crisis 

Pennsylvania 

Public Policy Polling: 48 Percent Of Pennsylvania Voters Disapprove Of President Obama's Handling Of The Housing And Mortgage Crisis While Only 38 Percent Approve.  ( Public Policy Polling , 600 V, MoE 4%, 4/30-5/2/12) 48 Percent Of Pennsylvania Independent Voters Disapprove Of President Obama's Handling Of The Housing And Mortgage Crisis While Only 26 Percent Approve.  ( Public Policy Polling , 600 V, MoE 4%, 4/30-5/2/12) 

Florida 

Public Policy Polling: 50 Percent Of Florida Voters Disapprove Of President Obama's Handling Of The Housing And Mortgage Crisis While Only 36 Percent Approve.  ( Public Policy Polling , 600 V, MoE 4%, 4/30-5/2/12) 49 Percent Of Florida Independent Voters Disapprove Of President Obama's Handling Of The Housing And Mortgage Crisis While Only 28 Percent Approve.  ( Public Policy Polling , 600 V, MoE 4%, 4/30-5/2/12) 

Nevada 

Public Policy Polling: 54 Percent Of Nevada Voters Disapprove Of President Obama's Handling Of The Housing And Mortgage Crisis While Only 34 Percent Approve.  ( Public Policy Polling , 600 V, MoE 4%, 4/30-5/2/12) 70 Percent Of Nevada Independent Voters Disapprove Of President Obama's Handling Of The Housing And Mortgage Crisis While Only 21 Percent Approve.  ( Public Policy Polling , 600 V, MoE 4%, 4/30-5/2/12)
