Obama's "Uphill Climb" 





Voters In A Dozen Key Battleground States Are More "Downbeat" About Obama Compared To The Rest Of The Nation.  "For President Obama, the path to a second term is going to be an uphill climb. While Americans across the nation are downbeat about the economy and the future, a special USA TODAY/Gallup Poll finds that voters in a dozen key battleground states for the 2012 election are in an even deeper funk about their lives, Obama s tenure and the nation s politics." (Susan Page, "Swing States Poll: Obama's Path To 2 nd  Term An Uphill Climb."  USA Today ,  11/3/11)  

Among Swing State Residents, Obama's Job Approval Rating Is Just 40 Percent, "Well Below" Previous Presidents Who Have Won Second Terms.  (Susan Page, "Swing States Poll: Obama's Path To 2 nd  Term An Uphill Climb."  USA Today ,  11/3/11)  "His Support Sags Especially Low Among Men And Among White, Non-Hispanic Voters. Only About One-Third Of Those Groups Say They Approve Of The Job Obama Is Doing As President."  (Susan Page, "Swing States Poll: Obama's Path To 2 nd  Term An Uphill Climb."  USA Today ,  11/3/11) 

Swing States Have Been The "Hardest Hit" By Obama's Failed Economic Policies.  "The underlying perils for the president are particularly pronounced in these battlegrounds, presumably because they are in parts of the country that have been hit hardest by the nation s economic troubles. Four of the states have unemployment rates in double digits, well above the national average of 9.1%." (Susan Page, "Swing States Poll: Obama's Path To 2 nd  Term An Uphill Climb."  USA Today ,  11/3/11) By Nearly 4 To 1, Voters In Swing States Aren't Satisfied With The Direction The Country Is Headed.   "By nearly 4 to 1, those surveyed aren t satisfied with the way things are going in the United States. That could signal trouble for incumbents in general and the president in particular as voters increasingly hold him responsible for the country s economic troubles." (Susan Page, "Swing States Poll: Obama's Path To 2 nd  Term An Uphill Climb."  USA Today ,  11/3/11) 

60 Percent Of Swing State Voters Say They Aren't Better Off Than They Were Three Years Ago.  "By 60% to 37%, those in swing states say they and their families aren t better off than they were three years ago -- a version of the question Republican challenger Ronald Reagan posed to devastating effect against Democratic President Jimmy Carter in 1980." (Susan Page, "Swing States Poll: Obama's Path To 2 nd  Term An Uphill Climb."  USA Today ,  11/3/11)  Among Residents In Other States, 54 Percent Say They Aren't Better Off Than They Were Three Years Ago.  "Residents in swing states are more likely than those elsewhere to say their families lives have taken a negative turn. Americans in other states also are dispirited, but not to the same degree: 44% say they re better off; 54% say they aren t." (Susan Page, "Swing States Poll: Obama's Path To 2 nd  Term An Uphill Climb."  USA Today ,  11/3/11) 

By More Than 2 to 1, Swing State Republicans Say They Are "Extremely Enthusiastic" About The 2012 Presidential Election.  "By more than 2 to 1, Republicans in swing states are more likely than Democrats to say they are 'extremely enthusiastic' about voting for president next year -- an important test of whether supporters will be willing to volunteer their time, contribute money and vote. The enthusiasm gap between Republicans and Democrats and the intense opposition to Obama among Republicans loom as major challenges for the president." (Susan Page, "Swing States Poll: Obama's Path To 2 nd  Term An Uphill Climb."  USA Today , 11/3/11) 

And Obama's Poll Numbers In Swing States Aren't Encouraging 

PENNSYLVANIA 

Just 38 Percent Of Independent Voters Approve Of Obama's Handling Of His Job As President.  ( Franklin Marshall College Poll,  419 RV, MoE 4.3%, 10/24 - 30/11) Even Among Democrats, Just 53 Percent Approve Of Obama's Handling Of His Job As President.  ( Franklin Marshall College Poll,  419 RV, MoE 4.3%, 10/24 - 30/11)   

Just 37 Percent Of Pennsylvania Voters Approve Of Obama's Handling Of His Job As President.  ( Franklin Marshall College Poll,  419 RV, MoE 4.3%, 10/24 - 30/11) Of Those Who Approve Of Obama's Handling Of The Presidency, Just 9 Percent Say He Is Doing And "Excellent Job," Compared To 28 Percent Who Say He Just Doing A "Good Job."  ( Franklin Marshall College Poll,  419 RV, MoE 4.3%, 10/24 - 30/11) 

Only 42 Percent Of Pennsylvania Voters Say Obama Deserves Reelection.  ( Franklin Marshall College Poll,  419 RV, MoE 4.3%, 10/24 - 30/11) 

WISCONSIN 

Public Policy Polling President Dean Debnam: Obama Is "Persistently Unpopular" In Wisconsin.  "'Wisconsin is looking more and more like it might return to the swing state column next year,' said Dean Debnam, President of Public Policy Polling. 'Obama is persistently unpopular there and the recall elections over the summer showed how closely divided the state is.'" ( Public Policy Polling,  1,170 RV, MoE 2.9%, 10/20 - 23/11)  

"Obama Continues To Be Unpopular In Wisconsin With Only 44% Of Voters Approving Of Him To 51% Who Disapprove." ( Public Policy Polling,  1,170 RV, MoE 2.9%, 10/20 - 23/11)  Among Independents, Only 36 Approve Of Obama's Job Performance . "It s yet another competitive state at the Presidential level where his standing with independents is abysmal- he s at 36/57 with them." ( Public Policy Polling,  1,170 RV, MoE 2.9%, 10/20 - 23/11)  

FLORIDA 

Only 41 Percent Of Florida Voters Approve Of The Job Obama Is Doing As President.  ( Suffolk University Poll,  800 RV, MoE 3.5%, 10/26 - 30/11) 

48 Percent Of Florida Voters Say They Have An "Unfavorable" Opinion Of Obama, Compared To 45 Percent Who Say They Have An "Favorable" Opinion Of Him.  ( Suffolk University Poll,  800 RV, MoE 3.5%, 10/26 - 30/11) 

Only 20 Percent Of Florida Voters Think The Country Is Headed In The Right Direction.  ( Suffolk University Poll,  800 RV, MoE 3.5%, 10/26 - 30/11) Compared To 66 Percent Who Say Its Headed In The Wrong Direction.  ( Suffolk University Poll,  800 RV, MoE 3.5%, 10/26 - 30/11) 

NORTH CAROLINA 

North Carolina "Continues To Look Like A Toss Up For 2012."  "North Carolina continues to look like a toss up for 2012, with Barack Obama unpopular but none of his prospective Republican opponents setting the world on fire either."   ( Public Policy Polling,  615 RV, MoE 4%, 10/27-31/11)  

Only 45 Percent Of North Carolina Voters Approve Of Obama's Job Performance.  "45% of voters in the state approve of Obama with 50% disapproving, numbers pretty much in line with where he s been for the last 3 months." ( Public Policy Polling,  615 RV, MoE 4%, 10/27-31/11)  "Obama s Big Issue Continues To Be With Independents, Only 38% Of Whom Think He s Doing A Good Job To 56% Who Give Him Poor Marks."  ( Public Policy Polling,  615 RV, MoE 4%, 10/27-31/11)       

  

The President Suffers From Widespread Voter Discontent    

TEXAS 

57 Percent Of Texans Disapprove Of Obama.  ( University of Texas / Texas Tribune Poll,  800 A, MoE 3.46%, 10/11) Only 33 Percent Approve.  ( University of Texas / Texas Tribune Poll,  800 A, MoE 3.46%, 10/11) 

61 Percent Of Texans Disapprove Of Obama's Handling Of The Economy, 53 Percent Disapproving "Strongly."  ( University of Texas / Texas Tribune Poll,  800 A, MoE 3.46%, 10/11) 

75 Percent Of Texans Think The Country Is On The Wrong Track.  ( University of Texas / Texas Tribune Poll,  800 A, MoE 3.46%, 10/11 

MONTANA 

Only 32 Percent Of Montana Residents Approve Of The Job Obama Is Doing, While Nearly 58 Percent Disapprove.  (Mary Picket, "MSU Billings Poll: Montanans Disapprove Of Obama's Job Performance,"  The Billings Gazette ,  11/1/11) 

More Than 67 Percent Of Montana Residents Say Obama Is Doing A "Bad Job" On The Economy, While Just 21 Percent Say He Is Doing A Good Job.  (Mary Picket, "MSU Billings Poll: Montanans Disapprove Of Obama's Job Performance,"  The Billings Gazette ,  11/1/11)
