"Panoramic Displeasure" 





In An Allstate/ National Journal  Heartland Monitor Poll, Disapproval Of Obama Hit An All-Time High, As His Approval Rating Fell To An All-Time Low.   "Just 44 percent of those surveyed said they approved of Obama's performance as president--his lowest rating in the 10 Heartland Monitor polls conducted since April 2009. Likewise, the share of adults disapproving of his performance also reached a high at 50 percent." (Ronald Brownstein, "Obama's Performance Rating Slips Again,"  National Journal ,  10/14/11) 

Obama's Approval Numbers Show "Panoramic Displeasure" As His Ratings Among Independents "Cratered" And "Tumbled" With Women, Youth Voters, And Hispanics.  "The assessments of Obama's job performance tell a story of similarly panoramic displeasure. Just 35 percent of independents said they approved, by far his worst showing in the Heartland Monitor poll. Among whites, his ratings cratered to 34 percent, also a new low. Even among white women with a college education--consistently Obama's strongest group in the white electorate--his ratings tumbled below 40 percent. Among adults under 30 and Hispanics, two other cornerstones of his 2008 coalition, Obama managed no more than about 50 percent approval." (Ronald Brownstein, "Obama's Performance Rating Slips Again,"  National Journal ,  10/14/11) "Nearly Four In 10 Adults Overall (And Almost Half Of Whites) Said They Strongly Disapproved Of His Performance. Equal Numbers Of Each Group Said They Definitely Intend To Vote Against Him In 2012."  (Ronald Brownstein, "Obama's Performance Rating Slips Again,"  National Journal ,  10/14/11) 

According To Gallup, Obama Is Down Eight Points To A Generic Republican For The Second Month In A Row.  "U.S. registered voters, by 46% to 38%, continue to say they are more likely to vote for the Republican presidential candidate than for Barack Obama in the 2012 presidential election. The generic Republican led by the same eight-percentage-point margin in September, and also held a lead in July."    (Jeffrey M. Jones, "'Generic' Republican Continues To Lead Obama In 2012 Vote,"  Gallup ,  10/14/11) "The Eight-Point Lead For The Republican Candidate Persists, 50% To 42%, When Taking Into Account The Leanings Of Undecided Voters."  (Jeffrey M. Jones, "'Generic' Republican Continues To Lead Obama In 2012 Vote,"  Gallup ,  10/14/11) "Independents Currently Favor The Republican Candidate By 43% To 30%."  (Jeffrey M. Jones, "'Generic' Republican Continues To Lead Obama In 2012 Vote,"  Gallup ,  10/14/11) 

"Equally Ominous For The President: 70 Percent Of Those Polled In The New Survey Said That The Country Was On The Wrong Track."  (Ronald Brownstein, "Obama's Performance Rating Slips Again,"  National Journal ,  10/14/11) 

46 Percent Of Americans Think The Economy Will "Deteriorate" In The Next Year.  "But in the new poll, fully 46 percent said they expected the economy to deteriorate over the next year, a big jump from the 32 percent who took that gloomy view last May." (Ronald Brownstein, "Obama's Performance Rating Slips Again," National Journal ,  10/14/11) 

Less Than A Third Think That Obama's Policies Have Helped Create Opportunities For Them To Get Ahead.  "Just 31 percent of those polled said that his agenda has increased 'opportunity for people like you to get ahead,' while 37 percent said it has decreased their opportunities and 26 percent said it has had no effect." (Ronald Brownstein, "Obama's Performance Rating Slips Again,"  National Journal ,  10/14/11) 

AND OBAMA'S NOT GETTING BETTER MARKS IN THE STATES 

West Virginia 

Obama's Approval Rating Has Dropped To 28 Percent In West Virginia.  "One would think that a man who's approval/disapproval numbers are 32%/62% could not drop any lower. After all, West Virginia is a state where 55% of the voters are registered Democratic and President Obama is a Democrat. But somehow, President Obama has managed to drop from 32%/62% in September down to 28%/63% in October, polls by Public Policy Polling in the last 2 months show."(Don Surber, "Obama Slips In W.VA."  The Daily Mail,  10/13/11) Among Independents, Just 16 Percent Approve Of Obama's Job Performance.  ( Public Policy Polling,  932 LV, MoE 3.2%, 9/30 - 10/2/11) 



Michigan 

Just 38 Percent Of Michigan Voters Give Obama A Positive Job Approval Rating, While 61 Percent Disapprove Of His Job Performance.  ("Mich. Voters Give Obama, Snyder Poor Job Ratings,"  The Associated Press   10/10/11) 

46 Percent Of Michigan Voters Say They Have A Favorable Opinion Of Obama.  ("Mich. Voters Give Obama, Snyder Poor Job Ratings,"  The Associated Press   10/10/11) 

    

Virginia 

Just 45 Percent Of Virginia Voters Say They Have A Favorable Opinion Of Obama.  ( Quinnipiac University Polling , 1459 RV, MoE 2.6%, 10/3 - 9/11) Among Independents, Just 40 Percent Say They Have A Favorable Opinion Of Obama.  ( Quinnipiac University Polling , 1459 RV, MoE 2.6%, 10/3 - 9/11) 

Only 45 Percent Of Virginia Voters Approve Of The Way Obama Is Handling His Job As President And 43 Percent Say They Agree With His Policies.  ( Quinnipiac University Polling , 1459 RV, MoE 2.6%, 10/3 - 9/11) Among Independents, Only 38 Percent Give Obama A Positive Job Approval Rating And 37 Percent Agree With His Policies.  ( Quinnipiac University Polling , 1459 RV, MoE 2.6%, 10/3 - 9/11) 

Only 44 Percent Of Virginia Voters Believe That Obama Deserves Reelection.  ( Quinnipiac University Polling , 1459 RV, MoE 2.6%, 10/3 - 9/11) 



Iowa 

Just 42 Percent Of Iowa Voters Approve Of The Job Obama Is Doing As President.  ( Marist/NBC News Polling , 2,836 RV, MoE 1.8%, 10/3 - 5/11) 

21 Percent Of Iowa Voters Think The Country Is Headed In The Right Direction.  ( Marist/NBC News Polling , 2,836 RV, MoE 1.8%, 10/3 - 5/11)   

New Hampshire 

38 Percent Of New Hampshire Voters Approve Of The Way Obama Is Handling His Job As President.  ( Marist/NBC News Polling , 2,218 RV, MoE 2.1%, 10/3 - 5/11) 

Just 19 Percent Of New Hampshire Voters Think The Country Is Going In The Right Direction.  ( Marist/NBC News Polling , 2,218 RV, MoE 2.1%, 10/3 - 5/11) 

New Jersey 

A Record-Low Of 47 Percent Of New Jersey Voters Say They Have A Favorable Opinion Of Obama.  ( Quinnipiac University Polling,  1,186 RV, MoE 2.9%, 10/5-10/11) 

52 Percent Of New Jersey Voters Disapprove Of The Way Obama Is Handling His Job As President, While Just A Record-Low Of 43 Percent Approve.  ( Quinnipiac University Polling,  1,186 RV, MoE 2.9%, 10/5-10/11) 

"Voters In New Jersey Are Giving President Barack Obama His Lowest Approval Rating Ever, While Independents Are Going Thumbs Down On Him By A Margin Of Almost 2-To-1, A New Survey Shows."  (Tim Mak, "Poll: New Low For Obama In NJ,"  Politico,  10/13/11) Among Independents, 60 Percent Disapprove Of The Way Obama Is Handling His Job As President.  ( Quinnipiac University Polling,  1,186 RV, MoE 2.9%, 10/5-10/11) Just 40 Of Independent New Jersey Voters Percent Say They Have A Favorable Opinion Of Obama.  ( Quinnipiac University Polling,  1,186 RV, MoE 2.9%, 10/5-10/11) Only 37 Percent Of New Jersey Independent Voters Believe Obama Deserves To Be Reelected.  ( Quinnipiac University Polling,  1,186 RV, MoE 2.9%, 10/5-10/11)
