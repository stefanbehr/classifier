Poor Ratings Across The Board 





Gallup's Three Day Rolling Average: Obama Approval At 38%, Disapproval At 53%.  ( Gallup , 1,500 A, MoE 3%, 10/4-6/11) 

Obama's "Job Approval Rating Averaged 41% In September, Tied With August For the Lowest Monthly Approval Average Of His Administration."  (Frank Newport and Clancy Bertane, "Obama's September Approval Rating Remains At Term-Low 41%,"  Gallup,   10/7/11) Among Independents, Just 30 Percent Approve Of Obama's Job Performance.  (Frank Newport and Clancy Bertane, "Obama's September Approval Rating Remains At Term-Low 41%,"  Gallup,   10/7/11) 

55 Percent Of Americans Disapprove Of Obama's Handling Of The Presidency, His Highest Disapproval Rating Since Taking Office.   ( Quinnipiac Poll , 2118 RV, MoE 2.1%, 9/27-10/3/11) 

64 Percent Of Americans Disapprove Of Obama's Handling Of The Economy, Only 32 Percent Approve Tying The Lowest Approval Rating Of His Presidency.  ( Quinnipiac Poll , 2118 RV, MoE 2.1%, 9/27-10/3/11) "And If Obama Wins Re-Election In 2012, Only 29 Percent Surveyed Believe The Economy Will Recover."  (Mackenzie Weinger, "Pollster: 'Trend Isn't Good' For Obama,"  Politico ,  10/6/11) 

"'The Trend Isn't Good For President Barack Obama,' Peter Brown, Assistant Director Of The Quinnipiac University Polling Institute, Said In A Press Release."  (Mackenzie Weinger, "Pollster: 'Trend Isn't Good' For Obama,"  Politico ,  10/6/11) "Brown Said In The Press Release That Obama 'Is Stuck At A Politically Unhealthy Level For Someone Who Wants To Be Re-Elected.'"  (Mackenzie Weinger, "Pollster: 'Trend Isn't Good' For Obama,"  Politico ,  10/6/11) 

  AND IN THE STATES, "IT'S NOT GOING TO BE EASY" FOR OBAMA 



Illinois 

President Obama's Job Approval Rating Has Fallen Below 50 Percent Among Registered Voters In Illinois."  (Mike Flannery, "President Obama s Job Approval Rating Drops Below 50% In Home State Of Illinois,"  Fox Chicago,   10/3/11) 

  "It's A Sign Of Potential Trouble In The State Where He Launched His Political Career."  "As he seeks re-election in 2012, it's a sign of potential trouble in the state where he launched his political career, won a series of landslide victories and still maintains a home." (Mike Flannery, "President Obama s Job Approval Rating Drops Below 50% In Home State Of Illinois,"  Fox Chicago,   10/3/11) 

  

Illinois Voters "Were Split Almost Evenly" On Obama's Job Performance.  "Illinois's registered voters were split almost evenly on the President's job performance: 49.81 percent approved while 46.23 percent disapproved, a statistical tie with a margin of error: +/- 2.3." (Mike Flannery, "President Obama s Job Approval Rating Drops Below 50% In Home State Of Illinois,"  Fox Chicago,   10/3/11) 

  

"Independent Voters Have Turned Negative On Mr. Obama's Job Performance: 43 Percent Approved, 52 Percent Disapproved."  (Mike Flannery, "President Obama s Job Approval Rating Drops Below 50% In Home State Of Illinois,"  Fox Chicago,   10/3/11) 

North Carolina 

Just 44 Percent Of North Carolina Voters Approve Of The Job Obama Is Doing As President.  ( Public Policy Polling,  760 RV, MoE 3.6%, 9/30-10/3/11) Among Independents, Just 35 Percent Approve Of Obama's Job Performance.  ( Public Policy Polling,  760 RV, MoE 3.6%, 9/30-10/3/11) 

  

Ohio 

"In Ohio, Just 42% Approved Of The Job He Was Doing And 51% Said The President Should Not Be Reelected."  (Michael Muskal, "Pennsylvania, Ohio Voters Unhappy With Obama, Quinnipiac Polls Say,"  Los Angeles Times,   9/28/11) 

Director Of University Of Virginia's Center For Politics Larry Sabato: "It's Not Going To Be Easy" For Obama In Ohio.   'I would never say he can t win,' Larry Sabato, director of the University of Virginia s Center for Politics, said when asked about Ohio, a state where Hillary Clinton beat Obama in the 2008 Democratic presidential primary. But he added, 'It s not going to be easy.' (Stephen Koff, "Ohio Looking Like Key State -again--In 2012 Presidential Election,"  The Plain Dealer,   9/28/11) 

A Record High Of 53 Percent Of Ohio Voters Disapprove Of Obama's Handling Of His Job As President, While Just 42 Percent Approve.  ( Quinnipiac University Polling,  1301 RV, MoE 2.7, 9/20-25/11) Among Independents, Just 38 Percent Approve Of Obama's Job Performance.  ( Quinnipiac University Polling,  1301 RV, MoE 2.7, 9/20-25/11) 

Just 43 Percent Of Ohio Voters Say Obama Deserves To Be Reelected, The Lowest Level 2011 ( Quinnipiac University Polling,  1301 RV, MoE 2.7, 9/20-25/11) Among Independents, Only 37 Percent Believe Obama Deserves Reelection.  ( Quinnipiac University Polling,  1301 RV, MoE 2.7, 9/20-25/11) 

Only 23 Percent Of Ohio Democrats Say They Are More Enthusiastic About The 2012 Presidential Election Than In Previous Years, Compared To 51 Percent Of Republicans.  ( Quinnipiac University Polling,  1301 RV, MoE 2.7, 9/20-25/11) 

  

Pennsylvania 

A Record High Of 49 Percent Of Pennsylvania Voters View Obama Unfavorably, While Just 45 Percent View Him Favorably.  ( Quinnipiac University Polling,  1370 RV, MoE 2.7, 9/21-26/11) 

54 Percent of Pennsylvania Voters Disapprove Of The Way Obama Has Handled His Job As President, While Just 43 Percent Approve.  ( Quinnipiac University Polling,  1370 RV, MoE 2.7, 9/21-26/11) Among Independents, 57 Percent Disapprove Of Obama's Job Performance.  ( Quinnipiac University Polling,  1370 RV, MoE 2.7, 9/21-26/11) 

Just 44 Percent Of Pennsylvania Voters Believe Obama Deserves To Be Reelected.  ( Quinnipiac University Polling,  1370 RV, MoE 2.7, 9/21-26/11) 

Polls Continue To Show That Obama Will Have A "Tough" Time Repeating His 2008 Victories In Pennsylvania And Ohio.  "President Obama continues to have trouble in Pennsylvania and Ohio as a majority of voters in those battleground states said that he does not deserve to be reelected, according to two Quinnipiac University polls released on Wednesday. Obama carried both states in 2008, but the latest polls show that he would have a tough, though not impossible, time repeating in 2012." (Michael Muskal, "Pennsylvania, Ohio Voters Unhappy With Obama, Quinnipiac Polls Say,"  Los Angeles Times,   9/28/11)
