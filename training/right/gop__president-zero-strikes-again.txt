President Zero Strikes Again! 



"The House On Wednesday Night Unanimously Rejected An Alternative Budget Proposal Based On President Obama s 2013 Budget Plan, Dispatching It In A 0-414 Rout."  (Pete Kasperowicz, "0-414 Vote: House Clobbers Budget Proposal Based On Obama's 2013 Plan,"  The Hill , 3/28/12) In An Attempt To Downplay "An Ugly-Looking Vote" The White Hosue Called The Vote On Their Budget A "Gimmick."  "The vote came just hours after the White House cast the pending vote as a political 'gimmick,' an apparent attempt to downplay what many expected to be an ugly-looking vote for the White House." (Pete Kasperowicz, "0-414 Vote: House Clobbers Budget Proposal Based On Obama's 2013 Plan,"  The Hill , 3/28/12) 

0-97: Last Year, Obama's FY2012 Budget Was Unanimously Defeated In The Senate 

Last Year, Obama's FY2012 Budget Was Voted Down In The Senate 0-97.  "Obama s budget plans have a poor track record in Congress over the last year. In May 2011, 97 senators voted against a motion to take up his 2012 budget plan -- no senator voted in favor of the motion." (Pete Kasperowicz, "0-414 Vote: House Clobbers Budget Proposal Based On Obama's 2013 Plan,"  The Hill , 3/28/12)  "The Senate s Answer To Not Having A Budget: Vote No On Every Other Plan." " They were unable to produce one last year, when they held both chambers of Congress and it s not clear that any of their divisions over spending and taxing have been bridged. The Senate s answer to not having a budget: Vote no on every other plan." (Jonathan Allen,  Politico Huddle , 5/26/11) 

0: Senate Democrats Have Not Passed A Budget For Over 1,000 Days And That's A-OK With The White House 

Senate Democrats Have Not Passed A Budget For 1,066 Days, Since April 29, 2009.   (S. Con. Res. 13,  Roll Call 173 ; D 53-3, R 0-40, I 2-0, 4/29/09) 

White House Press Secretary Jay Carney Says The White House Has "No Opinion" On Whether The Senate Should Pass A Budget.  ABC NEWS' JAKE TAPPER:  " The White House has no opinion about whether or not the Senate should pass a budget? The president's going to introduce one. The Fed chair says not having one is bad for growth. But the White House has no opinion about whether - " JAY CARNEY:  " I have no opinion -- the White House has no opinion on Chairman Bernanke's assessment of how the Senate ought to do its business." (White House Press Briefing, 2/8/12)
