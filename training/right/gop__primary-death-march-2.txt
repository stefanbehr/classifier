Primary Death March? 



Good afternoon, much has been said/written recently about the tenor of the GOP primary and the comparison of the 2012 primary to 2008. Today David Axelrod weighed in calling the GOP primary a "death march." Turns out that is the exact phrase Obama used to describe his own campaign in '08 speaking to weary primary supporters. So either Axe is wrong or he's spinning himself in circles and into the realm of the nonsensical tele-conference pundit? 



SHOT:  In March 2012, Axelrod Calls Republican Primary A "Death March" 

@aburnspolitico:  Axelrod s take on Romney campaign: tactical victories in a kind of death march here (Alexander Burns,  Tweet , 3/7/12) 



CHASER:  In March 2008, Obama And Axelrod Used The Same Phrase To Describe Their Own Primary 

In March 2008, Obama Said The Democratic Primary Was "Like A Bataan Death March."  OBAMA: "For those of you who are just weary of the primary, and feeling kind of ground down or that it's like a Bataan death march, I just want everybody to know   more
