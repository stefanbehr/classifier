Right Diagnosis, Wrong Prescription 



Now That His Economic Policies Have Repeatedly Failed, Obama Says That "The Mess Has Been Bigger" Than He Anticipated: 

CNN's Wolf Blitzer:  "When you took office, you said if I don't have this done in three years then it s going to be a one-term proposition, meaning you're going to be a one term president. Do you remember that?" 

President Barack Obama:  "Well, here's what I remember. When I came into office, I knew I was going to have a big mess to clean up. And frankly, the mess has been bigger than I think a lot of people anticipated at the time." 

(CNN's "The Situation Room," 8/16/11) 

However, In 2008, He Described The Economic Crisis As One Of "Historic Proportions" And The Worst Since The Great Depression: 

Obama In October 2008: "We Meet Here At A Time Of Great Uncertainty. Our Economy Is In Crisis."  (President Barack Obama,  Remarks , La Crosse, WI, 10/1/08) 

Obama In November 2008: "We Face The Worst Economic Crisis Since The Great Depression."  "We face the worst economic crisis since the Great Depression. 760,000 workers have lost their jobs this year. Businesses and families can t get credit. Home values are falling, and pensions are disappearing. Wages are lower than they ve been in a decade, at a time when the costs of health care and college have never been higher." (President Barack Obama,  Democratic Radio Address , Washington, DC, 11/1/08) 

Obama In November 2008: "We Are Facing An Economic Crisis Of Historic Proportions."  "The news this past week, including this morning s news about Citigroup, has made it even more clear that we are facing an economic crisis of historic proportions. Our financial markets are under stress. New home purchases in October were the lowest in half a century. Recently, more than half a million jobless claims were filed, the highest in eighteen years and if we do not act swiftly and boldly, most experts now believe that we could lose millions of jobs next year." (President Barack Obama,  Remarks Announcing Members Of The Economic Team , Chicago, IL, 11/24/08)
