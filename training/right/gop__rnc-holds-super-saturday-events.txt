RNC Holds Super Saturday Events 



On Saturday August 4 the RNC will hold Super Saturday events in battleground states across the country. Events and details are as follows:  

COLORADO 

Thronton 

What: Adams County Victory Super Saturday 

Who: Congressional Candidate Joe Coors 

When: 10:00AM MT 

Where: 2200 E. 104th Ave., Thornton, CO 80229 

Greenwood Village 

What: Arapahoe County Victory Super Saturday 

Who: COGOP Chairman Ryan Call 

When: 1:00PM MT 

Where: 5950 S. Willow Dr., Suite 305, Greenwood Village, CO 80111 

Castle Rock 

What: Douglas County Victory Super Saturday 

Who: Congressman Cory Gardner (6:00PM MT), CO Speaker of the House Frank McNulty (11:00AM MT) 

When: 11:00AM MT; 6:00PM MT 

Where: 413 Wilcox St., Castle Rock, CO 80104 

Fort Collins 

What: Fort Collins County Victory Super Saturday 

Who: Congressman Cory Gardner 

When: 3:00PM MT 

Where: 700 S. College Ave., Suite B, Fort Collins 

Lakewood 

What: Jefferson County Victory Super Saturday 

Special Guest: National Committeeman-elect Mike Kopp 

When: 9:00AM MT 

Where: 13949 W. Colfax Ave., Suite #250, Lakewood, CO 80401 

Grand Junction 

What: Mesa County Victory Super Saturday 

Who: State Senator Steve King 

When: 11:30AM MT 

Where: 1114 N. 1st St., Grand Junction, CO 81501 

Pueblo 

What: Pueblo County Victory Super Saturday 

Who: Romney Pueblo County Chairwoman Vera Ortegon 

When: 1:30PM MT 

Where: 108 Lincoln St., Pueblo, CO 

Pueblo 

What: Weld County Victory Super Saturday 

Who: Congressman Cory Gardner 

When: 1:45PM MT 

Where: 809 9th St., Pueblo, CO 81004 



FLORIDA 



Fort Meyers 

What: Fort Meyers Super Saturday Event 

Who: Local Republican Volunteers 

When: Saturday, August 4, 2012 at 12:00 PM to 2:00 PM ET 

Location: Fort Myers Romney Victory Office, 17595 S. Tamiami Trail, Suite 203, Fort Meyers, FL 33908 

Gainesville 

What: Gainesville Super Saturday Event 

Who: Local Republican Volunteers 

When: Saturday, August 4, 2012 at 12:00 PM to 2:00 PM EDT 

Location: Gainesville Romney Victory Office, 1722 NW 80th Blvd, Gainesville, FL 32606 

Jacksonville 

What: Jacksonville Super Saturday Event 

Who: Local Republican Volunteers 

When: Saturday, August 4, 2012 at 12:00 PM to 2:00 PM EDT 

Location: Jacksonville Romney Victory Office, 4963 Beach Blvd., Jacksonville, FL 32207 

Miami 

What: Miami Super Saturday Event 

Who: Local Republican Volunteers 

When: Saturday, August 4, 2012 at 12:00 PM to 2:00 PM EDT 

Location: Hialeah Romney Victory Office, 3800 W 12th Ave., Suite 1, Hialeah, FL 33012 

Fort Lauderdale 

What: Fort Lauderdale Super Saturday Event 

Who: Local Republican Volunteers 

When: Saturday, August 4, 2012 at 12:00 PM to 2:00 PM EDT 

Location: Fort Lauderdale Romney Victory Office, 2817 East Oakland Park Blvd Suite 200-A, Fort Lauderdale, FL 33306 

Orlando 

What: Orlando Super Saturday Event 

Who: Local Republican Volunteers 

When: Saturday, August 4, 2012 at 12:00 PM to 2:00 PM EDT 

Location: Orlando Romney Victory Office, 234 South Semoran Blvd., Orlando, FL 32807 

Melbourne 

What: Melbourne Super Saturday Event 

Who: Local Republican Volunteers 

When: Saturday, August 4, 2012 at 12:00 PM to 2:00 PM EDT 

Location: Melbourne Romney Victory Office, 6767 North Wickham Road, Suite B-101, Melbourne, FL 32940 

Panama City 

What: Panama City Super Saturday Event 

Who: Local Republican Volunteers 

When: Saturday, August 4, 2012 at 12:00 PM to 2:00 PM EDT 

Location: Panama City Romney Victory Office, 1607 Lisenby Ave, Suite C, Panama City, FL 32405 

Pensacola 

What: Pensacola Super Saturday Event 

Who: Local Republican Volunteers 

When: Saturday, August 4, 2012 at 12:00 PM to 2:00 PM EDT 

Location: Pensacola Romney Victory Office, 4455 Bayou Blvd, Suite C, Pensacola, FL 32503 

Tallahassee 

What: Tallahassee Super Saturday Event 

Who: Local Republican Volunteers 

When: Saturday, August 4, 2012 at 12:00 PM to 2:00 PM EDT 

Location: Tallahassee Romney Victory Office, 420 E. Jefferson Street, Tallahassee, FL 32301 

Tampa 

What: Tampa Super Saturday Event 

Who: Local Republican Volunteers 

When: Saturday, August 4, 2012 at 12:00 PM to 2:00 PM EDT 

Location: Tampa Romney Victory Office, 4465 Gandy Blvd., Tampa, FL 33611 

West Palm Beach 

What: West Palm Beach Super Saturday Event 

Who: Local Republican Volunteers 

When: Saturday, August 4, 2012 at 12:00 PM to 2:00 PM EDT 

Location: Boca Raton Romney Victory Office, 1962 NE 5th Ave., Boca Raton, FL 33431 



IOWA 

Coralville 

What: Super Saturday Event 

Who: Local Republican Volunteers 

When: Saturday, August 4, 2012 at 8:30 AM and 3:00 PM CST 

Location: Coralville Victory Office, 89 2nd Street, Unit 8, Coralville, IA 52241 

Davenport 

What: Super Saturday Event 

Who: Local Republican Volunteers 

When: Saturday, August 4, 2012 at 10:00 AM CST 

Location: Scott County GOP Headquarters, 311 W. Kimberly Rd., Davenport, IA 52806 

Urbandale 

What: Super Saturday Event 

Who: Local Republican Volunteers 

When: Saturday, August 4, 2012 at 9:00 AM CST 

Location: Urbandale Victory Office, 2775 86th Street, Urbandale, IA 50322 



MICHIGAN 

Monroe 

What: Super Saturday Event and Press Availability with Bus 

Who: Rep. Tim Walberg 

When: Saturday, August 4, 2012 

Location: Monroe Victory Center, 40 S Monroe St, Monroe, MI 48161 

Livingston 

What: Super Saturday Event and Press Availability with Bus 

Who: Rep. Mike Rogers 

When: Saturday, August 4, 2012 

Location: Livingston Victory Center, 2554 E Grand River Ave, Howell, MI 48843 

Orion 

What: Super Saturday Event and Press Conference with Bus 

Who: Rep. Mike Rogers 

When: Saturday, August 4, 2012 

Location: Oakland - Lake Orion Victory Center, 3048 W Clarkston Rd, Lake Orion, MI, 48362 



NEVADA 



Las Vegas 

What: Veterans for Romney Coalition Rollout and Battle Born Blitz 

Who: Special Guest: Congressman Steve Pearce and Major Sean Fellows 

When: Saturday August 4 at 12:00 PM 

Location: 9640 West Tropicana Avenue, Suite 106 Las Vegas, NV 89147 

What: Centennial Hills Office Opening
 

Who: Special Guest: Congressional Candidate Danny Tarkanian 

When: Saturday August 4 at 10:00 AM 

Location: 6955 North Durango Drive, Suite #111 Las Vegas, NV 89149 

Reno 

What: Reno Battle Born Blitz 

Who: Volunteers 

When: Saturday August 4, at 1:00 PM 

Location:
 3702 South Virginia Street, Suite G1
 Reno, NV 89502 

Carson City 

What: Carson City Battle Born Blitz
 

Who: Volunteers 

When: Saturday August 4 at 1:00 PM 

Location: 2049 California Street, Suite #2
 Carson City, NV 89701 

Elko 

What: Elko Battle Born Blitz 

Who: Volunteers 

When: Saturday August 4 at 12:00 PM 

Location: 2140 Idaho Street Elko, NV 89108 



NEW HAMPSHIRE 

Nashua 

What: Super Saturday Event Media Availability 

Who: Local Republican Volunteers 

When: Saturday, August 4, 2012 at 10:00 AM ET 

Location: Nashua Victory Office, 427 Amherst Street, Nashua, NH 03063 



NEW MEXICO 

  

Rio Rancho 

What: Rio Rancho Super Saturday Event 

Who: U.S. Senate Candidate Heather Wilson, New Mexico Republican Volunteers 

When: Saturday, August 4, 2012 at 9:00 AM to 10:00 AM MST 

Location: Rio Rancho Victory Office, 3188 Southern BLVD, Suite B, Rio Rancho, NM 

Albuquerque 

What: Albuquerque Super Saturday Event 

Who: Lt. Gov. John Sanchez, Congressional Candidate Janice Arnold-Jones, New Mexico Republican Volunteers 

When: Saturday, August 4, 2012 at 10:00 AM to 11:00 AM MST 

Location: Albuquerque Victory Office, 1400 W. 2nd Street, Suite A, Roswell, NM 88201 

Santa Fe 

What: Santa Fe Super Saturday Event 

Who: Lt. Gov. John Sanchez, New Mexico Republican Volunteers 

When: Saturday, August 4, 2012 at 12:00 PM to 1:00 PM MST 

Location: Santa Fe Victory Office, 1587 Pacheco Street, Santa Fe NM 87505 



NORTH CAROLINA 

Cornelius 

What: North Mecklenburg County Victory Office Opening and GOP Unity Event 

Who: Republican National Committee Chairman Reince Priebus, North Carolina Republican Party Chairman Robin Hayes, Tennessee Republican Party Chairman Chris Devaney, Georgia Republican Party Vice Chair Bert Guy, South Carolina Republican Party Vice Chairwoman Lin Bennet, North Carolina Speaker of the House Thom Tillis 

When: Saturday, August 4 at 12:00 PM EDT 

Location: North Mecklenburg County Victory Center, 8301-9 Magnolia Estates Dr., Cornelius, NC, 28031 



OHIO 



Youngstown 

What: Volunteer Breakfast and Grand Opening of the Youngstown Victory Center 

Who: Congressman Bill Johnso 

When: Saturday, August 4 at 9:30 AM ET 

Location: Youngstown Victory Center, 621 Boardman Canfield Road, Youngstown, Ohio, 44512 

Grove City 

What: Grove City Victory Office Opening with Congressman Steve Stivers 

Who: Congressman Steve Stivers 

When: Saturday, August 4 at 10:00 AM ET 

Location: Grove City Victory Center, 2065-A Stringtown Road, Grove City, Ohio 43123 

Steubenville 

What: Steubenville Victory Office Opening with Senator Pat Toomey 

Who: Senator Pat Toomey 

When: Saturday, August 4 at 11:45 AM ET 

Location: Steubenville Victory Center, 100 Mall Drive, B-8, Steubenville, Ohio 43952 

Centerville 

What: Volunteer Appreciation Event with Senator Rob Portman 

Who: Senator Rob Portman 

When: Saturday, August 4 at 3:45 PM ET 

Location: Centerville Victory Center, 7720 Paragon Road, Centerville, Ohio 45459 

Westwood 

What: Volunteer Appreciation Event with Senator Rob Portman 

Who: Senator Rob Portman 

When: Saturday, August 4 at 6:00 PM ET 

Location: Westwood Victory Center, 2300 Montana Ave, Ste 420, Cincinnati, Ohio 45211 

Lewis Center 

What: Volunteer Appreciation Event with Olympic Gold Medalist, Derrek Parrk 

Who: Olympic Gold Medalist Derek Parra, Speedskating 

When: Saturday, August 4 at 7:00 PM ET 

Location: OhioHealth Chiller North, 8144 Highfield Drive, Lewis Center, Ohio 43035 



PENNSYLVANIA 

Erie 

What: Erie County Victory Office Opening and Super Saturday Rally 

Who: U.S. Congressman Mike Kelly, former U.S. Congressman Phil English, Diana Irey Vaughan, Candidate for Pennsylvania Treasurer 

When: Saturday, August 4 at 9:30 AM ET 

Location: Erie County Victory Office, 2206 West 15th Street, Erie, PA 16505 

Doylestown 

What: Bucks County Victory Office Opening and Super Saturday Rally 

Who: U.S. Congressman Mike Fitzpatrick, Anne Chapman, Candidate for State House (PA-31) 

When: Saturday, August 4 at 10:00 AM ET 

Location: Bucks County Victory Office, 115 North Broad Street, Doylestown, PA 18901 

Rochester 

What: Beaver County Victory Office Opening and Super Saturday Rally 

Who: U.S. Congressional Candidate Keith Rothfus (PA-12), Diana Irey Vaughan, Candidate for Pennsylvania Treasurer 

When: Saturday, August 4 at 12:30 PM ET 

Location: Beaver County Victory Office, 426 Adams Street, Suite 2, Rochester, PA 15074 



VIRGINIA 

Leesburg 

What: Super Saturday Kickoff 

Who: Gov. Bob McDonnell, Gov. Scott Walker, Rep. Frank Wolf 

When: Saturday, August 4, 2012 at 9:00 AM ET 

Location: Loudoun Victory Office, 18 Royal Street, SE, Leesburg, VA 

Arlington 

What: Romney for President Virginia Headquarters Grand Opening 

Who: Gov. Scott Walker, RNC Political Director Rick Wiley 

When: Saturday, August 4, 2012 at 11:00 AM ET 

Location: Romney for President Virginia Headquarters, 3811 Fairfax Drive, Suite 750, Arlington, VA 

Richmond 

What: Super Saturday Kickoff 

Who: RPV Chairman Pat Mullins 

When: Saturday, August 4, 2012 at 9:00 AM ET 

Location: Chesterfield North Victory Office , 9503 Hull Street Road, Suite D, Richmond, VA 

Bristow 

What: Super Saturday Kickoff 

Who: Rep. Rob Wittman 

When: Saturday, August 4, 2012 at 9:00 AM ET 

Location: Prince William West Victory Office, 10286 Bristow Center Drive, Suite G5, Bristow, VA 

Sterling 

What: Super Saturday Kickoff 

Who: Rep. Frank Wolf 

When: Saturday, August 4, 2012 at 9:30 AM ET 

Location: Sterling Victory Office, 21430 Cedar Drive, Suite #118, Sterling, VA 

Springfield 

What: Super Saturday Kickoff 

Who: Susan Allen, Fairfax County Supervisor Pat Herrity 

When: Saturday, August 4, 2012 at 10:00 AM ET 

Location: Springfield Victory Office, 6320 Augusta Drive, Suite 900, Springfield, VA 

Roanoke 

What: Super Saturday Kickoff 

Who: Rep. Bob Goodlatte 

When: Saturday, August 4, 2012 at 10:30 AM ET 

Location: Roanoke Victory Office, 2706 Ogden Road, SW, Roanoke, VA 

Chesapeake 

What: Super Saturday Kickoff 

Who: Gov. George Allen 

When: Saturday, August 4, 2012 at 10:30 AM ET 

Location: Chesapeake Victory Office, 124 S. Battlefield Boulevard, Chesapeake, VA 

Norfolk 

What: Super Saturday Kickoff 

Who: Gov. George Allen 

When: Saturday, August 4, 2012 at 12:00 PM ET 

Location: Norfolk Victory Office, 193 W. Ocean View Avenue, Norfolk, VA 

Harrisonburg 

What: Super Saturday Kickoff 

Who: Rep. Bob Goodlatte 

When: Saturday, August 4, 2012 at 12:30 PM ET 

Location: Harrisonburg Victory Office, 182 Neff Avenue, Suite S13, Harrisonburg, VA 

Newport News 

What: Super Saturday Kickoff 

Who: Gov. George Allen 

When: Saturday, August 4, 2012 at 1:30 PM ET 

Location: Newport News Victory Office, 12715 Wawick Boulevard, Newport News, VA 



WISCONSIN   

Waukesha 

What: Small Business Presser/Obama Birthday 

Who: Local Volunteers 

When: Saturday, August 4, 2012 

Location: Waukesha Victory Center, 1701 Pearl Street, Waukesha, WI 53186 

Eau Claire 

What: Small Business Presser/Obama Birthday 

Who: Local Volunteers 

When: Saturday, August 4, 2012 

Location: Eau Claire Victory Center, 2237 Brackett Avenue, Eau Claire, WI 54701 

 ###
