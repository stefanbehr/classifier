RNC Weekend Messaging Memo From Sean Spicer-ObamaCare 



MEMO 

FROM: Sean Spicer, RNC Communications Director @seanspicer 

TO:  Elyse Marcellino 

RE: Weekend Messaging Memo ObamaCare 





Next Friday marks the two-year anniversary of ObamaCare. On March 23, 2010, President Obama s signature legislation became law. 

At the RNC, we re marking the anniversary with a thorough examination of the act s effects. The results are not pretty. ObamaCare s not passing this biannual checkup. It s costing families, hurting business, and busting the budget. 

Over the next two weeks, expect to hear that message loud and clear from the RNC. Working with our allies on the Hill and in key battleground states, we re spearheading an aggressive offense to draw attention to the failures and unpopularity of ObamaCare. 

We will launch an advertising campaign across multiple media platforms. We will deploy our surrogates to drive the message on the national, state, and local levels. And we will empower activists with a grassroots awareness campaign. 

Obama   more
