Rushing To Regulate 



Note: While Republicans are fighting to make critical reforms to the Consumer Financial Protection Bureau (CFPB) that was created by the Dodd-Frank bill, President Obama has hit the campaign trail highlighting this new regulatory body as one of his signature accomplishments. Yet, if it was so important, why did he wait a whole year to appoint a director. Nearly one year ago, then-White House press secretary Robert Gibbs remarked that the director of the CFPB was of "tremendous importance." Yet, President Obama has finally chosen to nominate a permanent director less than a week before an agency with an "unprecedented lack of accountability" and the ability to "govern millions of transactions" is to open. Given the administration's haphazard and evasive organization of the CFPB, is it any wonder that main street businesses and community banks are struggling to come to terms with the uncertainty that Obama and Dodd-Frank are saddling the economy with? 

One Year Ago, The White House Was Stressing The Importance Of The CFPB 

Gibbs: "Again, There's A Number Of Positions That The President Will Be Looking At. And Obviously The Consumer Office Is - The Consumer Bureau Is One Of Tremendous Importance." (Press Briefing By Press Secretary Robert Gibbs, Washington D.C., 7/21/10) 

Gibbs: "Obviously There Are Several Positions That This Legislation Created. Obviously One Of Those Important Jobs Are The Office - The Consumer Bureau."   (Press Briefing By Press Secretary Robert Gibbs, Washington D.C., 7/21/10) 

Yet They've Waited Until The Last Minute To Nominate A Director 

"Former Ohio Attorney General Richard Cordray Has Been Picked By President Obama To Lead The Consumer Financial Protection Bureau." (Nathan Koppel, "Obama Taps Former Jeopardy Champ For Consumer Watchdog Post," The Wall Street Journal's "Law Blog" , 7/18/2011) 

The Head Of The CFPB Will "Create An Administrative Structure From Scratch" And Be Responsible For "Writing Regulations That Will Govern Millions Of Transactions."   "The leader of the new consumer financial protection bureau will need to hire hundreds, maybe even thousands of people, create an administrative structure from scratch, and oversee what is likely to be a long and arduous process of writing regulations that will govern millions of transactions." (Neil Irwin, "Is Elizabeth Warren Really The Best Pick To Run The New Consumer Finance Agency?," The Washington Post , 7/19/10) 

The CFPB Was Designed By Elizabeth Warren To Have An "Unprecedented Lack Of Accountability." "This unprecedented lack of accountability is by Ms. Warren s design. The bureau was the Harvard professor s idea, and she lobbied the Obama Administration and Congress to make it part of the 2010 Dodd-Frank financial reform. That law calls it an 'independent bureau,' akin to an independent agency like the Securities and Exchange Commission. But that s deceptive." (Editorial, "President Warren's Empire," The Wall Street Journal,  3/16/11) 

Dodd-Frank Mandates A Wave Of "Voluminous Regulation" 

"[T]he Dodd-Frank Financial Law Calls For Literally Hundreds Of New Rules By Dozens Of Agencies, And Two Entirely New Agencies."  (Editorial, "Obama's Rules Revelation,"  The Wall Street Journal , 1/19/11) 

"To Date, The Total  Estimated Compliance Costs  From Dodd-Frank Remained At $1.26 Billion, But Of The 140 Major Rulemakings, Only 26 Contain Quantified Cost Estimates." (Sam Batkins, "The Week In Regulation: July 5-8, 2011," American Action Network , 7/8/11) 

Dodd-Frank Could Lead To The End Of Community Banking. "The comprehensive financial reform agreed upon by the House and Senate on Friday, along with all the new regulations of the past year, could signal the end of community banking."  (Sarah Wallace, Op-Ed, "The End Of Community Banking," The Wall Street Journal, 6/29/10) "We Will Start To See More Small Community Bank Failures And Mergers Because Of Voluminous Regulation." "What does all this mean for our customers? Less credit will be available, costs will increase, and we will be less able to make loans to regular people who were creditworthy in the past. This is the perfect storm for the small retail banking customer. We will start to see more small community bank failures and mergers because of voluminous regulation." (Sarah Wallace, Op-Ed, "The End Of Community Banking," The Wall Street Journal, 6/29/10)
