Support For ObamaCare Drops Again 



President Obama Told " 60 Minutes" That One Of His Major Accomplishments Is Health Care 

STEVE KROFT: "Why do you think you deserve to be reelected? What have you accomplished?" 

PRESIDENT OBAMA: "Not only saving this country from a great depression, not only saving the auto industry, but putting in place a system in which we re going to start lowering health care costs and you re never going to go bankrupt because you get sick or somebody in your family gets sick." (CBS' "60 Minutes," 12/11/11) 

But New Poll Shows Only 29% Of Americans Agree  

Polls Show That Voters Don't Support ObamaCare, One Of Obama's "Signature Accomplishments." "Heading into his re-election campaign, the president faces a conflicted public that does not support his steering of the economy, the most dominant issue for Americans, or his reforms to health care, one of his signature accomplishments." (Ken Thomas and Jennifer Agiesta, "AP-GfK Poll: More Than Half Say Obama Should Lose," The Associated Press , 12/16/11) 

Half Of Americans Oppose ObamaCare While Only 29% Support It. "The poll found unpopularity for last year s health care reform bill, one of Obama s major accomplishments. About half of the respondents oppose the health care law and support for it dipped to 29 percent from 36 percent in June." (Ken Thomas and Jennifer Agiesta, "AP-GfK Poll: More Than Half Say Obama Should Lose," The Associated Press , 12/16/11) 

  Even Among Democrats, The Health Care Law Has "Tepid Support" With Just 50 Percent Expressing Support. "Even among Democrats, the health care law has tepid support. Fifty percent of Democrats supported the health care law, compared with 59 percent of Democrats last June." (Ken Thomas and Jennifer Agiesta, "AP-Gfk Poll: More Than Half Say Obama Should Lose," The Associated Press, 12/16/11) 

"Only About A Quarter Of Independents Back The Law." (Ken Thomas and Jennifer Agiesta, "AP-Gfk Poll: More Than Half Say Obama Should Lose," The Associated Press, 12/16/11)
