The White House Position On Taxes According To Schumer 



We don't normally circulate Chuck Schumer quotes but in this case, we couldn't help but notice that according to him the White House today is now firmly in support of raising taxes on the "middle class." Just FYI... 

The White House Today:  We Still Support Increasing Taxes On Those Making $250,000 And Above... 

Watch The Whole Exchange: 

  

CNN's Brianna Keilar:  "Someone like Senator Schumer looks at families - you know, he represents a state where money doesn't go as far as it would go in other states, if you're outside the New York metropolitan area. If you have a family in the New York City area, $250,000 thereabouts, does the president think that's a wealthy family?" 

White House Press Secretary Jay Carney:  "The president's position has not changed." (White House Press Briefing, 10/7/11) 

Sen. Chuck Schumer (D-NY):  $250,000 Is "Firmly In The Middle Class"... 

Sen. Chuck Schumer (D-NY) Does Not Support Obama's Tax On Incomes Over $250,000 Because That Is "Firmly In The Middle Class" And "Not Rich."  "But the president also wanted to raise taxes on families making more than $250,000 a year by limiting income-tax deductions they can take. That didn't sit well with rank-and-file Democrats. Schumer said families that earn $250,000 or $300,000 a year aren't rich enough to deserve extra taxes. He described them as 'firmly in the middle class. They are not rich, and in large parts of the country, that kind of income does not get you a big home or lots of vacations or anything else that's associated with wealth in America,' he said." (S.A. Miller, "Million-Dollar Idea,"  New York Post , 10/6/11) Schumer: Setting Line At $250,000 Effects Small-Business Owners And Dual-Income Couples Living In High-Cost Urban Areas.  "'There are lots of people who either make $250,000 or are close' to it, Schumer said, particularly small-business owners and dual-income couples living in high-cost urban areas. 'If we're able to draw a very clear line -- people above a million dollars should pay their fair share -- it's much easier to win that argument,' said Schumer, who is also in charge of political messaging for Democrats hoping to maintain control of the Senate." (Lori Montgomery, "Democrats Shift The Definition Of 'Rich' In Battle Over Taxes," The Washington Post , 10/5/11)
