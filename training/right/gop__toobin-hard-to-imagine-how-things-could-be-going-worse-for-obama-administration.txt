Toobin: Hard To Imagine How Things Could Be Going Worse For Obama Administration 





CNN's Fredricka Whitfield : "Alright arguments just ended in the portion of the health care reform debate Health Care Reform Act that pertains to individual mandates. Our Kate Bolduan and Jeff Toobin just outside the U.S. Supreme Court on the steps there. So Jeff you first, what s your assessment?" 

CNN Senior Legal Analyst Jeff Toobin : "This still looks like a train wreck for the Obama Administration, and it may also be a plane wreck. This entire law is now in serious trouble. It also seems that the individual mandate is doomed. I mean, Anthony Kennedy spent much of this morning talking about if we strike down the individual mandate, how should we handle the rest of the law? Now, it is less clear that they are going to strike down the whole law. There does seem to be some controversy in the court about that. Certainly there are some members of the court, Antonin Scalia, Justice Alito, who want to strike down the entire law, but it seemed almost a foregone conclusion today that they were going to strike down the individual mandate, and the only question is does the whole law go out the window with it?" 

Whitfield : "Oh, my goodness. Okay, so I have got about 20 seconds or so left. How might this impact arguments later on this afternoon, Jeff?" 

Toobin : "Well, it s hard to imagine how things could be going much worse for the Obama Administration, but now they re going to be dealing with the Medicaid portion, and they may decide to get rid of that as well." 

Whitfield : "Okay. All right. Jeffrey Toobin, thanks so much. Kate Bolduan, we ll be talking to you on the other side of the break. Much more of the Newsroom continues with my colleague Don Lemon." ( CNN's "Newsroom ," 3/28/12)
