Voters: Can't Afford Four More Years 





Obama's Job Approval Is "Below The Historical Threshold" For Reelection  

51 Percent Of Americans Say They Disapprove Of Obama's Job Performance, Compared To 41 Percent Who Say They Approve. "President Obama's job approval rating has ticked down to 41 percent in Gallup s latest daily tracking poll, with his disapproval at 51 percent." (Jonathan  Easley, "Obama Back To 51 Percent Disapprove In Latest Gallup Polling," The Hill, 12/7/11) "Obama Hasn't Polled Higher Than 45 Percent Since Late July," Suggesting "He Has His Work Cut Out For Him." "But the Gallup polling suggests Obama has his work cut out for him. While his approval numbers show about 40 percent of voters are with him steadily, he hasn t polled higher than 45 percent since late July, when he managed a 46 percent approval rating." (Jonathan  Easley, "Obama Back To 51 Percent Disapprove In Latest Gallup Polling," The Hill, 12/7/11) 

"Forty-Three Percent Say Obama Deserves To Be Re-Elected, While 55% Say He Does Not." (Frank Newport, "Record High Anti-Incumbent Sentiment Toward Congress," Gallup , 12/9/11) Obama's Job Approval Is "Below The Historical Threshold" For Incumbent Presidents Who Won Reelection. "Gallup trends show that incumbent presidents since World War II who won their re-election bids had 48% or higher job approval ratings in the last Gallup rating before the election. Obama is thus currently below the historical threshold."(Frank Newport, "Record High Anti-Incumbent Sentiment Toward Congress," Gallup , 12/9/11) 



And The Voters In States Obama Won In 2008 Are Echoing His National Poll Numbers 

OHIO 

55 Percent Of Ohio Voters Say They Disapprove Of Obama's Job Performance, Compared To 41 Percent Who Say They Approve. ( Quinnipiac University Polling, 1,437 RV, MoE 2.6%, 11/28 - 12/5/11) 53 Percent Of Ohio Voters Say That Obama Does Not Deserve Reelection, Compared To 42 Percent Who Say He Deserves It. ( Quinnipiac University Polling, 1,437 RV, MoE 2.6%, 11/28 - 12/5/11) 

Only 42 Percent Of Ohio Voters Say They Have A Favorable Opinion Of Obama, Compared To 52 Percent Who Say They Have An Unfavorable Opinion. ( Quinnipiac University Polling, 1,437 RV, MoE 2.6%, 11/28 - 12/5/11) 

Just 19 Percent Of Ohio Voters Say They Are Satisfied With The Direction The Country Is Headed, With 2 Percent Saying "Very Satisfied" And 17 Percent Saying "Somewhat Satisfied." ( Quinnipiac University Polling, 1,437 RV, MoE 2.6%, 11/28 - 12/5/11) 

PENNSYLVANIA 

Obama's Showing In Pennsylvania Will "Foretell His Chances Across The Electoral Map." "This time, amid a sluggish economic recovery and high unemployment, the race is shaping up to be much closer -- so close that Obama s showing in one state might foretell his chances across the electoral map. That would be Pennsylvania." (Peter Nicholas, "President Obama s Road To Reelection Runs Through Pennsylvania," Los Angeles Times , 12/5/11)   "Strategists Say A Loss In Pennsylvania Would All But Doom The President s Reelection Hopes." (Peter Nicholas, "President Obama s Road To Reelection Runs Through Pennsylvania," Los Angeles Times , 12/5/11) 

52 Percent Of Pennsylvania Voters Say They Disapprove Of The Job Obama Is Doing, Compared To 43 Percent Who Say They Disapprove. ( Quinnipiac University Polling, 1,453 RV, MoE 2.6% 11/28 - 12/5/11) 49 Percent Of Pennsylvania Voters Say That Obama Does Not Deserve To Be Reelected, While 47 Percent Say He Does Deserves It. ( Quinnipiac University Polling, 1,453 RV, MoE 2.6% 11/28 - 12/5/11) 

46 Percent Of Pennsylvania Voters Say They Have A Favorable Opinion Of Obama, Compared To 47 Percent Who Say They Have An Unfavorable Opinion. ( Quinnipiac University Polling, 1,453 RV, MoE 2.6% 11/28 - 12/5/11) 

Just 18 Percent Of Ohio Voters Say They Are Satisfied With The Direction The Country Is Headed, With 1 Percent Saying "Very Satisfied" And 17 Percent Saying "Somewhat Satisfied." ( Quinnipiac University Polling, 1,453 RV, MoE 2.6% 11/28 - 12/5/11) 

FLORIDA 



Florida Voters "Continue To Sour" On Obama. "While he carried Florida in 2008, voters in the SunshineState, which continues to have a higher unemployment rate than the national average during the Obama administration, continue to sour on the president." (Kevin Derby, "Obama's Feeling The Heat In Florida," Sunshine State News, 12/8/11) 



54 Percent Of Florida Voters Say They Disapprove Of The Job Obama Is Doing As President, Compared To 41 Percent Who Say They Disapprove. ( Quinnipiac University Polling, 1,226 RV, MoE 2.8% 11/28 - 12/5/11)  51 Percent Of Florida Voters Say That Obama Does Not Deserve Reelection, Compared To 44 Percent Who Say He Does Deserve It. ( Quinnipiac University Polling, 1,226 RV, MoE 2.8% 11/28 - 12/5/11)  

48 Percent Of Florida Voters Say They Have An Unfavorable Opinion Of Obama While 47 Percent Say They Have A Favorable Opinion Of Him. ( Quinnipiac University Polling, 1,226 RV, MoE 2.8% 11/28 - 12/5/11)  

Just 22 Percent Of Ohio Voters Say They Are Satisfied With The Direction The Country Is Headed, With 3 Percent Saying "Very Satisfied" And 19 Percent Saying "Somewhat Satisfied." ( Quinnipiac University Polling, 1,226 RV, MoE 2.8% 11/28 - 12/5/11)   

  

COLORADO  

50 Percent Of Colorado Voters Disapprove Of Obama's Job Performance As President, While Just Say 45 Percent Say They Approve. ( Public Policy Polling, 793 RV, MoE 3.5%, 12/1 - 4/11) 



Colorado Lieutenant Democratic Governor Joe Garcia: "Clearly The President Has A Lot Of Ground To Make Up." "'Clearly the president has a lot of ground to make up,' said Colorado Lieutenant Governor Joe Garcia, a Democrat and a Latino, in an interview in his office at the state capitol." (Jeff Mason, "In Hispanic-Heavy Colorado, Obama Must Work To Win," Reuters, 12/8/11) 

  

NORTH CAROLINA 

50 Percent Of North Carolina Voters Say They Disapprove Of Obama's  Job Performance Compared To 47 Percent Who Say They Approve. ( Public Policy Polling, 865 RV, MoE 3.3%, 12/1 - 4/11) Among Independent Voters, Just 43 Percent Say They Approve Of Obama's Job Performance While 51 Percent Say They Disapprove. ( Public Policy Polling, 865 RV, MoE 3.3%, 12/1 - 4/11) 



NEW HAMPSHIRE 



Just 40 Percent Of New Hampshire Voters Say They Approve Of Obama's Job Performance, Compared To 52 Percent Who Say They Disapprove. ( NBC News/Marist Poll, 2,263 RV, MoE 2.1%, 11/28 - 11/30/11) Among Independents, 40 Percent Say They Approve Of Obama's Job Performance. ( NBC News/Marist Poll, 2,263 RV, MoE 2.1%, 11/28 - 11/30/11)
