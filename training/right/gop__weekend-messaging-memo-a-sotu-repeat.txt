Weekend Messaging Memo - A SOTU Repeat 



MEMO 

FROM: Sean Spicer, RNC Communications Director 

TO: Interested Parties 

RE: Weekend Messaging Memo - A SOTU Repeat 



Sitcoms are known for their reruns. State of the Union addresses usually aren't. But if President Obama's speech Tuesday night sounded like a repeat, you were right. He'd said much of it before. 

Within minutes of the president's conclusion, the RNC released a  video , "Familiar Rhetoric, Failed Record," pairing excerpts of Tuesday's speech with similar or identical excerpts from the 2011 and 2010 State of the Union speeches. Within two days, the video had 600,000 YouTube views. If you haven't watched it, you need to. 

It struck a chord, proving yet again what we've long been saying: Attacking Obama with his own words resonates remarkably well. Our ad, crisp and straightforward, was played and replayed on cable news, on conservative radio, and, of course, across the Internet.  Bill O'Reilly called it "a brutal ad." Rush Limbaugh declared it a "great video." 

And it was simply Obama's words. 

The president's greatest liability in this election year is the heap of promises, now broken, that he made in years past. With  broken promises  on every issue from the economy to energy, healthcare to housing, the deficit, the debt, and White House ethics, the president's new promises are not believable. He has zero credibility. 

As Chairman Priebus  wrote  in POLITICO on Tuesday, "Americans demand a president who can come to Congress with a plan and purpose. Obama arrives with a speech and a slogan." The country is hungering for true leadership on jobs and the economy, and the president offers only more words--and recycled old words at that. 

Slogans are not solutions. The president has offered plenty of the former--We Can't Wait, Winning the Future, An America Built to Last, Better Together, Hope and Change, Yes We Can--but none of the latter. He has no workable solutions for the debt, the deficit, or job creation. 

In fact, the policies Obama has pursued have done more to exacerbate our problems than fix them. Blocking the Keystone pipeline is the latest example. He killed thousands of jobs and a reliable, affordable energy source. Obama says he cares about jobs and energy security, but the rhetoric doesn't match reality. 

Finally, the State of the Union was also notable for what the president did not say. He only barely mentioned Obamacare, his signature legislative achievement. He's conceded the fact that its failure has made it unpopular and politically toxic. He also dodged discussing the debt he's created; only 2.8 percent of the speech addressed the issue. 

Then there's jobs. As a Columbus Dispatch  editorial  noted Thursday, "The big fact missing in President Barack Obama's State of the Union speech on Tuesday was this one: 13 million Americans remain unemployed. About 10 million remain underemployed." In 2012, the state of our union is defined by our persistent unemployment problem and the millions of families suffering as a result. A president afraid to acknowledge that reality is a president unwilling to address the problem head on. 

Holding Obama accountable to his own record is the key to defeating him in 2012. Using his words against him is the most effective line of attack for the GOP, and it leaves Democrats defenseless. They can't argue with the president's own words. 

As the president crisscrossed the country this week on a three-day, five-state campaign tour, the RNC bracketed him with this message at each stop--Iowa, Arizona, Nevada, Colorado, and Michigan. Wherever he goes, from now until Election Day, voters will hear that message. We will hold him accountable for his record, using his own words. 

The State of the Union, like the Obama presidency, was a thorough disappointment, lacking in fresh ideas and full of stale rhetoric. But you don't have to take the GOP's word for it. Just  listen to Obama's . 

 ###
