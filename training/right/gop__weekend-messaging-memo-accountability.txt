Weekend Messaging Memo: Accountability 



MEMO 

FROM: RNC Communications Director Sean Spicer @seanspicer 

TO: Interested Parties 

RE: Weekend Messaging Memo: Accountability 



President Obama came to office promising to hold himself to a higher standard, to be open and honest with the American people and to transcend the Washington politics-as-usual. His record, however, shows he has done the opposite.  

The last few weeks have produced a series of revelations showing a troubling pattern of behavior in the Obama White House and the Obama campaign. But when will President Obama take responsibility? He hasn t walked the walk when it comes to his own staff and Super PAC s shameful and unethical behavior.  

President Obama says the buck stops with him. By staying silent on these issues, he is signaling that he gives his approval. 

There s plenty he needs to answer for, and here are four of the most recent and most egregious examples:  

1. Recently released emails show former Deputy Chief of Staff Jim Messina used a personal email account to email pharmaceutical lobbyists about, among other things, his plans to roll Pelosi. The emails also revealed top staff arranged meetings with industry insiders at the Caribou Coffee shop across the street from the White House to keep them off visitor logs. So White House staff, including Messina, actively worked to hide their questionable activities from the American people, despite all their talk of transparency. 

2.  Right before coming to the Obama White House, David Plouffe accepted a $100,000 speaking gig with a company who had ties to sponsors of terrorism. Profiting from terrorists allies is as absurd as it is reprehensible, and common sense would tell anyone that 100 grand for two hours of work should raise red flags. As an outside adviser and soon-to-be White House staffer, did Plouffe clear this speech with the administration? 

3. Emails uncovered last week revealed nearly every official in President Obama s inner circle knew at some point the Solyndra loan deal was headed for disaster, but the Obama administration still green-lighted a loan restructuring that put taxpayers on the hook for hundreds of millions more. And despite months of denials, the White House was directly involved in the fateful decision. 

4. This week, Team Obama sunk to a new low. The pro-Obama Super PAC Priorities USA attempted to exploit a woman s death for political gain. The Obama campaign insisted they knew nothing of the Super PAC s ad or the story told by the man featured in the ad, Joe Soptic. But in May, they hosted Mr. Soptic on a campaign call featuring Deputy Campaign Manager Stephanie Cutter. ( Click here for the audio .) Nevertheless, Cutter and Traveling Press Secretary Jen Psaki looked reporters in the eye and  told repeated, deliberate lies . 

There are three things we can learn from this pattern:   

1. President Obama s talk of hope and change was pure hypocrisy. He has now undermined the very justification of his 2008 candidacy. He promised to change Washington and the culture of American politics. Instead, Washington is divided; his White House is neither transparent nor accountable to the American people; and his team is running a shameful and dirty campaign like only a Chicago politician could. 

2. The Super PAC ad proves once again the Obama campaign is desperate to run on anything but the president s record, no matter how despicable. Their sole mission is to distract the American people from 42 months of unemployment above 8 percent, the nearly $16 trillion national debt, a slowing economy, rising prices and shrinking wages.  

Their desire for political victory is so great that they are willing to distort the truth and lie to the American people. It s an insult to the millions of families barely getting by in this economy. 

Back in 2007, President Obama called upon his opponents to own up to outside group expenditures on their behalf. [Y]ou can t say yesterday, you don t believe in em, and today, you re having three-quarters of a million dollars being spent for you. You can t just talk the talk, he said at a campaign event. 

I don t just talk the talk; I walk the walk, he added. But as of today, the president has yet to comment on the Super PAC ad. 

3. The ease with which the campaign and the administration have misrepresented themselves raises the appropriate question: What else are they lying about?  

What other insider deals and crony capitalism have they allowed? What else are they hiding on Solyndra? Did they ever intend to live up to their promise of being the most transparent administration in history ? How incestuous is their relationship with the Obama Super PAC s activities? ( See our web video .) 

While we may never know the answer to those questions, we do know one thing: This is not the Hope and Change Americans voted for. 

Americans deserve a substantive campaign and a serious debate focused on jobs, the economy, and strengthening the middle class, not one rooted in undignified character assaults.  

  

###
