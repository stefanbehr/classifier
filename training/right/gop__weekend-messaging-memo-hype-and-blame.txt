Weekend Messaging Memo Hype and Blame 



MEMO 

FROM: Sean Spicer, RNC Communications Director 

TO: Interested Parties 

RE: Weekend Messaging Memo - Hype and Blame 







Tomorrow, President Obama will visit Virginia and Ohio to "kickoff" his campaign. But we already know what to expect from him in the two battleground states:  hype and blame. 

After more than a year of campaign-style "official" events and a record number of fundraisers, the campaign announced this so-called "kickoff" on a conference call last week. But they did so without offering any rationale for the candidacy. There was no vision for a second term or defense of the Obama record.  

That's because they have nothing positive to run on no successful incumbent, no impressive record, and no thriving economy. So they have resorted to distraction and division, diversions and excuses. 

They threw out "hope and change" along with the '08 bumper stickers. Now, the campaign is all about hype and blame. 

It is a "campaign about nothing," completely "devoid of   more 

\
