Weekend Messaging Memo Say Anything Obama 



MEMO 

FROM: Sean Spicer, RNC Communication Director @seanspicer 

TO: Interested Parties 

RE: Weekend Messaging Memo Say Anything Obama 



This week, a hot microphone gave voters a chilling look at how President Obama really operates. 

By now, we know the story. At an international summit Monday, Obama leaned over to the outgoing Russian president to ask for space, promising flexibility after the election. 

Flexibility for what? Disregarding public opinion? 

In effect, he was telling President Medvedev what he hoped voters would never find out: that he will say anything to get reelected, but he doesn t mean any of it. 

America s Campaigner-in-Chief has indeed proven he will say and do whatever is politically necessary. In 2008, he deployed phony rhetoric more unabashedly than any politician in history. It may have worked then. It won t in 2012. 

That s because four years ago, Obama was an empty canvas. He could be anything to anybody: The post-partisan reformer to the disenchanted   more
