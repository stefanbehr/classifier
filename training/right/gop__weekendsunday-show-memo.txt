Weekend/Sunday Show Memo 



MEMO 

FROM: Sean Spicer, RNC Communications Director (@SeanSpicer) 

TO: Interested Parties 

RE: Weekend/Sunday Show Memo 



With the president's much over-hyped speech in Osawatomie, Kansas, this week, the Democrats unveiled their 2012 election strategy: run away from their record. Run far, far away. 

Like the speech itself, their strategy for re-election will be a sloppy amalgamation of raw populism, negative campaigning, misleading attacks, and--as always--a healthy dose of empty slogans. 

As such, they've tacitly admitted their failure. An incumbent only turns to such tactics when his record is an embarrassment. And no modern president has had to face re-election with a record as embarrassing as Barack Obama's. 

Thus, our mission at the RNC is simple: hold President Obama accountable for his record.  

Week in Review 

This past week alone provided plenty for which to hold them accountable: 

Top administration officials  made more disparaging  comments about Israel. 

Attorney General Eric Holder's testimony before Congress on Operation Fast and Furious highlighted the sheer  incompetence  of the administration. (See also: Chairman Priebus'  post on RedState.com  and our video " A Fast and Furious Cover-Up ") 

Obama's "Wall Street Guy," Jon Corzine, testified that his company, MF Global, lost $1.2 billion and noted, "I simply do not know where the money is." (See video: " Obama Day " on Capitol Hill) 

And, of course, there was the Kansas speech--short on policy solutions and long on political shots. As Chairman Priebus  writes  in the  Wichita Eagle  today, Teddy Roosevelt would be ashamed. 

The Week Ahead 

On Saturday, December 10 and on Thursday, December 15, the Republican candidates for president will participate in two more primary debates where they will continue to offer substantive solutions to the damage the Obama presidency has caused our economy. Each debate is a highly visible reminder of the numerous ways Democrats have failed our country. 

Jon Corzine, the man Obama once considered as a candidate for Treasury Secretary, will testify before Congress again next week. 

And next Friday will see President Obama attempting to shore up support among Jewish voters--one of the many constituencies in his 2008 coalition whose support is rapidly waning. 

These events and others will provide perfect openings to keep the spotlight where it must remain through 2012: on the failed Obama record. 

That includes: His Disastrous Policies - Obamacare, the stimulus, over-regulation His Lack of Leadership - the debt ceiling, the supercommittee, the fiscal commission His Alienated Constituencies - Hispanics, the Jewish community, young people His Incompetent Cabinet - Holder (Fast and Furious), Chu (Solyndra), Geithner (credit downgrade) His Political Allies - Jon Corzine, Ambassador Gutman, Big Labor His Broken Promises - deficit reduction, low unemployment, ethics reform 

Of course, that's just a sampling. 

As any independent Obama voter will tell you, this is not what they expected in 2008. Millions of swing voters thought, understandably, that the idea of hope and change was strong enough to overcome Obama's weak resume. 

It wasn't. As we seek to bring those voters back into the Republican fold, our job--this week and every week until November 2012--is to continually make the logical, methodical case that President Obama's record does not warrant a second term. 

The proof: Democrats' refusal to talk about that very record. 



###
