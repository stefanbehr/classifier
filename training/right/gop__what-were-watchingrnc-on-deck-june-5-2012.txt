What We're Watching...RNC On Deck June 5, 2012 



It's all Wisconsin all the time today in a state even  Jim Messina  is acknowledging is now a November battleground. Obama won the state by a high water 14 points in 2008 but flashback to 2004 with Bush losing by just 11,000 votes and significant statewide wins in 2010 and you can see why Team Obama is worried. Then add in the fact that Obama snubbed Wisconsin Democrats in their time of need by literally flying over the state without stopping to help and it isn't hard to understand why Wisconsin will be difficult for Obama. Note, last night Obama was  called out by RNC Chairman Priebus  on Twitter for his "boldness" and Axelrod is already starting the spin on Twitter. 

The Washington Post has a front page story today about the GOP attacking Obama for being  out of touch  with his celebrity appearances and Anna Wintour videos on the same day as a devastating jobs report. While Romney continues to make gains in the polls, Team Obama is trying to soothe supporters who are "scared   more
