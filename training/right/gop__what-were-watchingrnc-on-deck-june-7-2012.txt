What We're Watching...RNC On Deck June 7, 2012 



Today the House will vote on repealing the medical device tax in Obamacare that is hurting job creators across the country. The RNC is hosting an 11 a.m. ET conference call with Reps. Paulsen, Gingrey and NuVasive Chairman and CEO Lukianov to talk about the importance of repealing the tax. See call details below. Obama is in Nevada today for yet another event on student loans. This time he's resorting to a memo just 7 months after signing an executive order that simply moved the goal posts on the same issue. See the RNC Research piece  "The Obama Economic Plan: Sign A Memo" . This after admitting voters are  wondering  if he can rebuild the economy at his event last night and  being greeted in Nevada by a CBS story  on Obama facing difficulties overcoming with the Nevada economy. 

Last night DNC Chairwoman Debbie Wasserman Schultz admitted Obama employed  "his entire machinery" in Wisconsin  as she struggled to explain the loss and it's clear Bill Clinton is putting them on defense   more
