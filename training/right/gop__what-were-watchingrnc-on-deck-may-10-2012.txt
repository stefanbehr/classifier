What We're Watching...RNC On Deck May 10, 2012 



President Obama is headed to Hollywood to hobnob with celebrities while the middle class continues to be squeezed by Obama's policies. For three and a half years we've had a celebrity president and based on new polling in Ohio and Florida where an incumbent president is tied and his approval remains below 50 percent, Americans agree that it hasn't worked out as everyone had hoped. RNC Chairman Reince Priebus has a  blog post  on Red State discussing Obama's celebrity risk and RNC Research has a good piece  "A Day In The Life."  The Wall Street Journal has a good article analyzing the West Virginia primary results where a prison inmate won 40 percent of the vote against Obama. It's all about Obama's energy policies and we're guessing it doesn't stop in WV - we'll be watching VA, PA and OH. 

A couple takeaways from yesterday's news - it's  clear politics  drove the president's decision with an immediate fundraising email from the campaign and a Biden mistake drove the timing. Today the RNC   more
