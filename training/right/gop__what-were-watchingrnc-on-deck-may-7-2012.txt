What We're Watching...RNC On Deck May 7, 2012 



President Obama kicked off his campaign Saturday after a full year of campaigning on the taxpayer dime. A couple things we learned: people just aren't that into him with  smaller crowds  than 2008, he's  moving the goal posts  now saying it's not a question of being better off four years later and even Obama and his supporters agree that  Americans aren't satisfied  after 3.5 years of Obama in the White House. Overall a  Failure to Launch . Now the Obama campaign is up on the airwaves with a new ad that tries to employ his "Forward" slogan even though they spend much of the ad looking backward. See the RNC response and  research piece  below. 



This week we'll be focused on Obama and his policies leaving the American middle class feeling squeezed after years of broken promises. See today's  research piece  and stay tuned for more.  There are two new polls out today showing Governor Mitt Romney in a great position coming out of the Republican primary. Not only does Romney have more Republican   more
