Where Is Debbie On The Debt Ceiling Talks? 



With  The Hill  reporting that the Democrat leadership is at odds over the White House's proposed Medicare cuts, where does the head of the DNC and Congresswoman from Florida, Debbie Wasserman Schultz, stand on the debt ceiling issue? Does she think Obama's call for Medicare cuts would be "death trap" for seniors? And if so, then what does she have to say to Obama who admitted yesterday that his own party's do-nothing plan will bankrupt Medicare? 

DNC Chair Rep. Debbie Wasserman Schultz Said Rep. Paul Ryan's Budget Outline "Would   Literally Be A Death Trap For Seniors." 

"No longer would Medicare be a guarantee of health insurance coverage. Instead Medicare would become little more than a discount card. This plan would literally be a death trap for seniors." (Rep. Debbie Wasserman Schultz,  Remarks At Press Conference , 4/5/11) 

Obama Admits His Own Party's Plan Will Bankrupt Medicare 

OBAMA: "What I've tried to explain to them is, number one, if you look at the numbers, Medicare in particular will run out of money, and we will not be able to sustain that program no matter how much taxes go up. I mean, it s not an option for us to just sit by and do nothing. And if you re a progressive who cares about the integrity of Social Security and Medicare and Medicaid, and believes that it is part of what makes our country great, that we look after our seniors and look after the most vulnerable, then we have an obligation to make sure that we make those changes that are required to make it sustainable over the long term." (President Barack Obama, Remarks At Press Conference, Washington, D.C., 7/11/11) 





  

###
