Why Won't the President Take Questions? 



MEMO 

FROM: RNC Communications Director Sean Spicer @seanspicer 

TO: Interested Parties 

RE: Why Won't the President Take Questions?   



It's been almost eight weeks since President Obama last took questions from the White House press corps. Since then, a lot has happened, and the American people are demanding answers on a growing list of issues. 

When will President Obama quit ducking and dodging? When will he hold himself accountable? 



Here are just some of the questions Americans have for President Obama and that he has yet to answer: 

1) Why did you cut $700 billion from Medicare? 

Even as he talks Medicare on the campaign trail, the president has not explained why he robbed Medicare and the seniors who depend on it to bankroll Obamacare. 

2) Do you condemn the Obama SuperPAC's desperate and despicable ad campaign? 

As a candidate, then-Senator Obama promised to "walk the walk" and denounce independent organizations that ran indefensible ads on his behalf. Yet when the Obama SuperPAC Priorities USA produced an ad attempting to exploit a woman's death for political gain, he remained silent. He previously gave his blessing to the SuperPAC and allows his cabinet and top staff to fundraise for it, but he lacks the courage to take responsibility for their appalling behavior. 

3) How do you explain the July increase in unemployment and slowing GDP growth? 

Last month, unemployment increased to 8.3 percent, marking the 42nd straight month of unemployment above 8 percent. We learned in July that GDP growth slowed, meaning the economy is losing steam. Yet the president cannot say why four more years of the same failed policies will turn around this dangerous trend. 

4) Why is your plate too full for your own Jobs Council? 

President Obama has not convened his Jobs Council in over seven months. The White House says he's too busy, but he has found time for 130 political fundraisers since the last meeting of what he claimed was not a "show council." (He has attended more than 200 fundraisers since April 2011.) So much for making jobs a "number one" priority. 

5) Did you approve of David Plouffe's profiting from a sponsor of terrorism? 

Right after announcing his return to the Obama White House, David Plouffe accepted a $100,000 speaking gig with a company who had ties to sponsors of terrorism. It hardly seems responsible to give someone a high level security clearance after exhibiting such poor judgment. 

6) Can you explain to business owners your "You didn't build that" comment? 

Small businesses are struggling in the Obama economy especially in the wake of Obamacare. Entrepreneurs and innovators are rightfully outraged that the president would denigrate their hard work and attack them both with his policies and his words. 

7) Do you condone your staff using personal email accounts to conduct government business? 

We learned recently that former Deputy Chief of Staff Jim Messina used a private account to email lobbyists about "rolling Pelosi" during Obamacare negations. This seems highly hypocritical for the self-proclaimed "most transparent administration in history." 

8  )  Why didn't you stop the restructuring of Solyndra's loan? 

Nearly everybody in the president's inner circle knew Solyndra was headed for disaster. But the White House and the Administration approved of a loan restructuring plan that put taxpayers on the hook for hundreds of millions more. The president has not explained how he let this happen. 

9) Why did you invoke executive privilege on the Fast and Furious scandal?    

 Americans deserve answers on how this failed operation turned into a tragedy. But the president is hypocritically impeding transparency and accountability. 

10) Can you reconcile the conflicting responses to national security leaks? 

Keeping America safe and secure is of paramount importance, but the president has yet to see fit to answer the charges, from Dianne Feinstein no less, that sensitive information has been leaked by his administration for political gain. 

Surely President Obama can find time to answer ten simple questions. Or is running from his record the official platform of the Obama campaign? 





###
